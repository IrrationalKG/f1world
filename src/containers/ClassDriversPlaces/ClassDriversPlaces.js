/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Segment,
  Flag,
  Icon,
  Table,
  Header,
  Message,
  Image,
  Item,
  Statistic,
  Grid,
  Divider,
  Label,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchClassDrivers } from "../../actions/ClassDriversActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ClassDriversPlaces.less";

class ClassDriversPlaces extends Component {
  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchClassDrivers(props.match.params.year, Cookies.get("lang"));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;
    if (year !== nextYear) {
      props.fetchClassDrivers(nextYear, Cookies.get("lang"));
    }
  }

  renderClassDriversPlaces() {
    const elements = [];
    const { classDrivers } = this.props;
    const { classPlaces } = classDrivers.data;
    const { props } = this;
    const { year } = props.match.params;
    classPlaces.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.place}>
          <Table.Cell data-title="Miejsce" className="cell-default-size">
            {item.isClassified == "1" ? `${item.place}.` : "-"}
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.team?.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPlaces.map((e) => (
            <Table.Cell
              data-title={e.gp}
              key={e.gp}
              className={
                e.place <= 3 || e.points > 0
                  ? `cell-${e.place} cell-default-size`
                  : "cell-default-size"
              }
            >
              {e.grid == "1" ? (
                <div className="gp-pp">
                  <FormattedMessage id={"app.table.header.polepos"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
              <NavLink to={`/gp-result/${e.alias}/${year}`}>{e.place}</NavLink>
              {e.bestLap == "1" ? (
                <div className="gp-bl">
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
            </Table.Cell>
          ))}
          <Table.Cell data-title="Wygrane" className="cell-default-size">
            {item.wins === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className="cell-default-size">
            {item.podium === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className="cell-default-size">
            {item.pp === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.pp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenie" className="cell-default-size">
            {item.bestLap === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.bestLap}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="cell-default-size bold">
            {item.pointsClass === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {Math.round(item.pointsClass * 100) / 100}
              </NavLink>
            )}
            {item.points !== item.pointsClass && (
              <small>
                <NavLink
                  to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {" ("}
                  {item.points === null
                    ? String.fromCharCode(160)
                    : Math.round(item.points * 100) / 100}
                  {")"}
                </NavLink>
              </small>
            )}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassDriversPlacesMobile() {
    const items = [];
    const { classDrivers } = this.props;
    const { classPlaces } = classDrivers.data;
    const { year } = this.props.match.params;
    classPlaces.forEach((item, idx) => {
      const picReplace = "/build/images/drivers/driver_no_profile.jpg";
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const link = `/driver/${item.alias}`;
      const element = (
        <Grid.Row key={item.id}>
          <Grid.Column width={4}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="image-link">
                <Image
                  size="small"
                  src={filename}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={10}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      {item.isClassified == "1" ? `${item.place}.` : "-"}
                      {". "}
                      <Flag name={item.country} />
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.team?.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </Item.Description>
                  <Item.Extra>
                    {item.gpPlaces.map((e) => (
                      <span key={e.gp} className="key-value-box">
                        <div className="key-value-box-header">
                          {e.nameShort.toUpperCase()}
                        </div>
                        <div
                          className={
                            e.place <= 10
                              ? `key-value-box-value cell-${e.place}`
                              : "key-value-box-value"
                          }
                        >
                          <div>
                            {e.place === "" ? (
                              String.fromCharCode(160)
                            ) : (
                              <NavLink to={`/gp-result/${e.alias}/${year}`}>
                                {e.place}
                              </NavLink>
                            )}
                            {e.grid == "1" && (
                              <Label
                                attached="top left"
                                color="red"
                                size="mini"
                              >
                                &nbsp;
                                <FormattedMessage
                                  id={"app.table.header.polepos"}
                                />
                                &nbsp;
                              </Label>
                            )}
                            {e.bestLap == "1" && (
                              <Label
                                attached="top right"
                                color="green"
                                size="mini"
                              >
                                &nbsp;
                                <FormattedMessage
                                  id={"app.table.header.bestlaps"}
                                />
                                &nbsp;
                              </Label>
                            )}
                          </div>
                        </div>
                      </span>
                    ))}
                  </Item.Extra>
                  <Divider></Divider>
                  <Item.Extra>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.wins"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.wins === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.wins}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.podiums"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.podium === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.podium}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.polepos"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.pp === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.pp}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.bestLap === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.bestLap}
                          </NavLink>
                        )}
                      </div>
                    </span>
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right">
              <Statistic.Value>
                {" "}
                {item.pointsClass === null ? (
                  String.fromCharCode(160)
                ) : (
                  <NavLink
                    to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {Math.round(item.pointsClass * 100) / 100}
                  </NavLink>
                )}
                {item.points !== item.pointsClass && (
                  <small>
                    <NavLink
                      to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                    >
                      {" ("}
                      {item.points === null
                        ? String.fromCharCode(160)
                        : Math.round(item.points * 100) / 100}
                      {")"}
                    </NavLink>
                  </small>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  render() {
    const { classDrivers } = this.props;
    if (!classDrivers.data || classDrivers.loading) {
      return (
        <section
          id="class-drivers-details"
          name="class-drivers-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { props } = this;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { regulations, gp } = props.classDrivers.data;

    const linkPrev = `/classification/drivers-places/${prevYear}`;
    const linkNext = `/classification/drivers-places/${nextYear}`;

    if (gp.length === 0) {
      return (
        <section
          id="class-drivers-details"
          name="class-drivers-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.drivers.places.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.drivers.places.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                {year < 1950 && (
                  <FormattedMessage
                    id={"app.page.drivers.places.info1"}
                    values={{ year }}
                  />
                )}
                {year >= 1950 && (
                  <FormattedMessage
                    id={"app.page.drivers.places.info2"}
                    values={{ year }}
                  />
                )}
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }
    return (
      <section
        id="class-drivers-details"
        name="class-drivers-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.drivers.places.title"}
              values={{ season: year }}
            />
          }
          subtitle={
            <FormattedMessage id={"app.page.drivers.places.subtitle"} />
          }
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.page.drivers.places.header"} />}
              type="secondary"
            />
            <div className="hideForDesktop">
              <Grid columns={3} divided="vertically">
                {this.renderClassDriversPlacesMobile()}
              </Grid>
            </div>
            <div className="hideForMobile">
              <Segment basic className="overflow">
                <Table
                  basic="very"
                  celled
                  className="responsive-table center-aligned"
                >
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>#</Table.HeaderCell>
                      <Table.HeaderCell className="left">
                        <FormattedMessage id={"app.table.header.driver"} />
                      </Table.HeaderCell>
                      {gp.map((item) => (
                        <Table.HeaderCell
                          data-title={item.gp}
                          key={item.gp.toLowerCase().split(" ").join("_")}
                        >
                          {item.nameShort}
                          <br />
                          <Flag name={item.name} />
                        </Table.HeaderCell>
                      ))}
                      <Table.HeaderCell>
                        <FormattedMessage id={"app.table.header.wins"} />
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <FormattedMessage id={"app.table.header.podiums"} />
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <FormattedMessage id={"app.table.header.polepos"} />
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <FormattedMessage id={"app.table.header.points"} />
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{this.renderClassDriversPlaces()}</Table.Body>
                </Table>
              </Segment>
              <Segment padded basic>
                <Message>
                  <p>
                    <FormattedMessage id={"app.page.drivers.places.legend"} />
                  </p>
                </Message>
              </Segment>
              <Regulations regulations={regulations}></Regulations>
            </div>
          </Segment>
        </div>
      </section>
    );
  }
}

ClassDriversPlaces.propTypes = {
  fetchClassDrivers: PropTypes.func.isRequired,
  classDrivers: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ classDrivers }) {
  return { classDrivers };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchClassDrivers }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassDriversPlaces);
