// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TEAM_EVENTS_REQUEST = "FETCH_TEAM_EVENTS_REQUEST";
export const FETCH_TEAM_EVENTS_SUCCESS = "FETCH_TEAM_EVENTS_SUCCESS";
export const FETCH_TEAM_EVENTS_ERROR = "FETCH_TEAM_EVENTS_ERROR";

function requestData() {
  return {
    type: FETCH_TEAM_EVENTS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TEAM_EVENTS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TEAM_EVENTS_ERROR,
    payload: error,
  };
}

export function fetchTeamEvents(
  page,
  team,
  event,
  pos,
  driver,
  gp,
  circuit,
  engine,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_team_events.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          page: page,
          team: team,
          event: event,
          pos: pos,
          driver: driver,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
