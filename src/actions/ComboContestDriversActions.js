// const request = require('../json/combo_contest_drivers.json');
//
// export const FETCH_COMBO_CONTEST_DRIVERS_SUCCESS = 'FETCH_COMBO_CONTEST_DRIVERS_SUCCESS';
//
// export function fetchComboContestDrivers() {
//   return {
//     type: FETCH_COMBO_CONTEST_DRIVERS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_COMBO_CONTEST_DRIVERS_REQUEST = 'FETCH_COMBO_CONTEST_DRIVERS_REQUEST';
export const FETCH_COMBO_CONTEST_DRIVERS_SUCCESS = 'FETCH_COMBO_CONTEST_DRIVERS_SUCCESS';
export const FETCH_COMBO_CONTEST_DRIVERS_ERROR = 'FETCH_COMBO_CONTEST_DRIVERS_ERROR';

function requestData() {
  return {
    type: FETCH_COMBO_CONTEST_DRIVERS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_CONTEST_DRIVERS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_CONTEST_DRIVERS_ERROR,
    payload: error
  };
}

export function fetchComboContestDrivers() {
  const url = `${ROOT_URL}/get_combo_contest_drivers.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
