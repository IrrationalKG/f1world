<?
header('Access-Control-Allow-Origin: *');

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie id
// $query="SELECT id_gp id FROM gp WHERE circuit_alias='$id'";
// $result = mysqli_query($dbhandle,$query);
// $id;
// while($r = mysqli_fetch_assoc($result)) {
//   $id = $r["id"];
// }

// aktualny rok
$query="SELECT max(season) maxYear FROM gp_season";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}
// $prevYear = $currentYear-1;

// min pozycja
if ($year) {
  $query="SELECT circuit_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
  AND gp_season.season=$year
  AND sort=(SELECT min(sort) FROM gp_season GROUP BY season desc limit 1)
  ORDER BY season desc LIMIT 1";
}else{
  $query="SELECT distinct circuit_alias alias FROM gp order by circuit LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT circuit_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
  AND gp_season.season=$year
  AND sort=(SELECT max(sort) FROM gp_season GROUP BY season desc limit 1)
  ORDER BY season desc LIMIT 1";
}else{
  $query="SELECT distinct circuit_alias alias FROM gp order by circuit DESC LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
// $maxPos;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
  // $maxPos = $r["sort"];
}

// poprzedni tor
if ($year) {
  $query="SELECT circuit_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
  AND sort=(SELECT sort FROM gp_season WHERE id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') AND season=$year)-1 AND season=$year";
}else{
  $query="SELECT distinct circuit_alias alias FROM gp order by circuit";
}
$result = mysqli_query($dbhandle,$query);
$prevId=$max;
$prevIdTmp=$prevId;
while($r = mysqli_fetch_assoc($result)) {
  if ($year) {
    $prevId = $r["alias"];
  }else{
    if ($r["alias"] == $id) {
      $prevId = $prevIdTmp;
    }else{
      $prevIdTmp = $r["alias"]; 
    }
  }
}

// nastepny tor
if ($year) {
  $query="SELECT circuit_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
  AND sort=(SELECT sort FROM gp_season WHERE id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') AND season=$year)+1 AND season=$year";
}else{
  $query="SELECT distinct circuit_alias alias FROM gp order by circuit";
}
$result = mysqli_query($dbhandle,$query);
$nextId = $min;
$getNextId=false;
while($r = mysqli_fetch_assoc($result)) {
  if ($year) {
    $nextId = $r["alias"];
  }else{
    if ($r["alias"] == $id) {
      $getNextId = true;
      continue;
    }
    if ($getNextId == true){
      $nextId = $r["alias"];
      $getNextId = false;
    }
  }
}

// informacje o torze
if ($lang=='pl') {
  $query="SELECT gp.name,";
}else{
  $query="SELECT gp.name_en as name,";
}
$query.="gp.circuit_alias alias,gp.name_alias countryAlias,gp.id_gp id, gp.circuit, gp.picture, gp.track, gp.laps, gp.length, gp.distance, lower(gp.name_short) countryShort,
gp.country_code countryCode, gp.lat, gp.lng, gp.build, gp_season.date raceDate, gp.internet,'' prevId, '' nextId
FROM gp, gp_season WHERE gp.id_gp=gp_season.id_gp AND gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by gp_season.season DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$circuitItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["prevId"]=$prevId;
   $r["nextId"]=$nextId;
   $circuitItems = $r;
}

/**
* Statystyki
**/
$statsItems;
$statsDriverItems;

//zwyciestwa
$query="SELECT COUNT(*) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
drivers.country_code countryCode from drivers_gp_results,drivers,gp
where drivers.id_driver=drivers_gp_results.id_driver and drivers_gp_results.id_gp=gp.id_gp
and race_pos=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["wins"] = $r;
}

//punkty
// $query="SELECT SUM(race_points) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
// drivers.country_code countryCode from drivers_gp_results,drivers,gp
// where drivers.id_driver=drivers_gp_results.id_driver and drivers_gp_results.id_gp=gp.id_gp
// and race_points>0 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$query="SELECT SUM(COALESCE(race_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) as amount, drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) as name, drivers.alias,
drivers.country_code countryCode 
from drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_points>0 
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint AND drivers_sprint_results.sprint_points>0 
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver 
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
where gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') 
group by drivers.id_driver
having amount>0
order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["points"] = $r;
}

//podium
$query="SELECT COUNT(*) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
drivers.country_code countryCode from drivers_gp_results,drivers,gp
where drivers.id_driver=drivers_gp_results.id_driver and drivers_gp_results.id_gp=gp.id_gp
and race_pos<4 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["podium"] = $r;
}

//pole positions
$query="SELECT COUNT(*) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
drivers.country_code countryCode from drivers_gp_starting_grid,drivers,gp
where drivers.id_driver=drivers_gp_starting_grid.id_driver and drivers_gp_starting_grid.id_gp=gp.id_gp
and is_pp=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["polepos"] = $r;
}

//najlepsze okrazenia
$query="SELECT COUNT(*) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
drivers.country_code countryCode from drivers_gp_results,drivers,gp
where drivers.id_driver=drivers_gp_results.id_driver and drivers_gp_results.id_gp=gp.id_gp
and race_best_lap=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["bestlaps"] = $r;
}

//liczba grand prix
$query="SELECT COUNT(*) as amount,drivers.id_driver idDriver,CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) name, drivers.alias,
drivers.country_code countryCode from drivers_gp_results,drivers,gp
where drivers.id_driver=drivers_gp_results.id_driver and drivers_gp_results.id_gp=gp.id_gp
and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by drivers.id_driver order by amount desc,surname LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsDriverItems["starts"] = $r;
}

$statsItems["drivers"] = $statsDriverItems;

$statsTeamItems;

//zwyciestwa
$query="SELECT COUNT(*) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and drivers_gp_results.id_gp=gp.id_gp
and race_pos=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["wins"] = $r;
}

//punkty
$query="SELECT SUM(COALESCE(drivers_gp_results.race_team_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) as amount, lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_team_points>0 AND drivers_gp_results.excluded_from_team_class=0 
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint AND drivers_sprint_results.sprint_points>0 
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
where gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') 
group by teams.team having amount>0
order by 1 desc LIMIT 1";
// SELECT SUM(race_points) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
// from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and drivers_gp_results.id_gp=gp.id_gp
// and race_points>0 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
// order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["points"] = $r;
}

//podium
$query="SELECT COUNT(*) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and drivers_gp_results.id_gp=gp.id_gp
and race_pos<4 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["podium"] = $r;
}

//pole positions
$query="SELECT COUNT(*) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_starting_grid,teams,gp where teams.id_team=drivers_gp_starting_grid.id_team and drivers_gp_starting_grid.id_gp=gp.id_gp
and is_pp=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["polepos"] = $r;
}

//najlepsze okrazenia
$query="SELECT COUNT(*) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and drivers_gp_results.id_gp=gp.id_gp
and race_best_lap=1 and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["bestlaps"] = $r;
}

//liczba grand prix
$query="SELECT COUNT(DISTINCT race_date) as amount,lower(teams.team) team,teams.name,teams.country_code countryCode, teams.alias_name alias
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and drivers_gp_results.id_gp=gp.id_gp
and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') group by teams.team,teams.name,teams.country_code
order by amount desc,name LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsTeamItems["starts"] = $r;
}

$statsItems["teams"] = $statsTeamItems;

/**
* Wykresy
*/
// zwyciestwa na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.circuit_alias='$id' AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitWins=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitWins[] = $r;
}
$statsItems["driversCircuitWins"]=$driversCircuitWins;

// punkty na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, SUM(COALESCE(race_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) as y 
from drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_points>0 
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint AND drivers_sprint_results.sprint_points>0 
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver 
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
where gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') 
group by drivers.id_driver
having y>0
order by 2 desc,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPoints[] = $r;
}
$statsItems["driversCircuitPoints"]=$driversCircuitPoints;

// podium na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.circuit_alias='$id'
AND drivers_gp_results.race_pos>0 AND drivers_gp_results.race_pos<4
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPodium=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPodium[] = $r;
}
$statsItems["driversCircuitPodium"]=$driversCircuitPodium;

// pole position na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_starting_grid.is_pp) y
FROM drivers_gp_starting_grid
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_starting_grid.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_starting_grid.id_gp
WHERE gp.circuit_alias='$id'
AND drivers_gp_starting_grid.is_pp=1
GROUP BY drivers_gp_starting_grid.id_driver
ORDER BY 2 DESC,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPolepos=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPolepos[] = $r;
}
$statsItems["driversCircuitPolepos"]=$driversCircuitPolepos;

// naj okrążenie na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_best_lap) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.circuit_alias='$id'
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitBestlaps=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitBestlaps[] = $r;
}
$statsItems["driversCircuitBestlaps"]=$driversCircuitBestlaps;

// starty na torze - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.circuit_alias='$id'
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC,surname
LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driversCircuitStarts=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitStarts[] = $r;
}
$statsItems["driversCircuitStarts"]=$driversCircuitStarts;

// zwyciestwa na torze - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results,teams,gp
WHERE drivers_gp_results.id_team=teams.id_team
AND gp.id_gp=drivers_gp_results.id_gp
AND gp.circuit_alias='$id'
AND race_pos=1
GROUP BY teams.team ORDER BY 2 DESC,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitWins=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitWins[] = $r;
}
$statsItems["teamsCircuitWins"]=$teamsCircuitWins;

// punkty na torze - zespoły
// $query="SELECT UPPER(teams.team) name, SUM(drivers_gp_results.race_team_points) y
// FROM drivers_gp_results,teams,gp
// WHERE drivers_gp_results.id_team=teams.id_team
// AND gp.id_gp=drivers_gp_results.id_gp
// AND gp.circuit_alias='$id'
// AND excluded_from_team_class=0
// GROUP BY teams.team ORDER BY 2 DESC LIMIT 10";
$query="SELECT UPPER(teams.name) name, SUM(COALESCE(drivers_gp_results.race_team_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) as y 
from drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_team_points>0 AND drivers_gp_results.excluded_from_team_class=0 
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint AND drivers_sprint_results.sprint_points>0 
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
where gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') 
group by teams.team having y>0
order by 2 desc,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitPoints[] = $r;
}
$statsItems["teamsCircuitPoints"]=$teamsCircuitPoints;

// podium na torze - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results,teams,gp
WHERE drivers_gp_results.id_team=teams.id_team
AND gp.id_gp=drivers_gp_results.id_gp
AND gp.circuit_alias='$id'
AND race_pos>0 AND race_pos<4
GROUP BY teams.team ORDER BY 2 DESC,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitPodium=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitPodium[] = $r;
}
$statsItems["teamsCircuitPodium"]=$teamsCircuitPodium;

// pole position na torze - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_starting_grid.is_pp) y
FROM drivers_gp_starting_grid,teams,gp
WHERE drivers_gp_starting_grid.id_team=teams.id_team
AND gp.id_gp=drivers_gp_starting_grid.id_gp
AND gp.circuit_alias='$id'
AND is_pp=1
GROUP BY teams.team ORDER BY 2 DESC,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitPolepos=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitPolepos[] = $r;
}
$statsItems["teamsCircuitPolepos"]=$teamsCircuitPolepos;

// naj okrążenie na torze - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_best_lap) y
FROM drivers_gp_results,teams,gp
WHERE drivers_gp_results.id_team=teams.id_team
AND gp.id_gp=drivers_gp_results.id_gp
AND gp.circuit_alias='$id'
AND drivers_gp_results.race_best_lap=1
GROUP BY teams.team ORDER BY 2 DESC,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitBestlaps=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitBestlaps[] = $r;
}
$statsItems["teamsCircuitBestlaps"]=$teamsCircuitBestlaps;

// starty na torze - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(DISTINCT drivers_gp_results.race_date) y
FROM drivers_gp_results,teams,gp
WHERE drivers_gp_results.id_team=teams.id_team
AND gp.id_gp=drivers_gp_results.id_gp
AND gp.circuit_alias='$id'
GROUP BY teams.team ORDER BY 2 DESC,name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamsCircuitStarts=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsCircuitStarts[] = $r;
}
$statsItems["teamsCircuitStarts"]=$teamsCircuitStarts;

$circuitItems["stats"]=$statsItems;

/**
* Kariera
**/
$careerItems;

//sezony
$query="SELECT MAX(SUBSTRING(race_date,1,4)) as maxYear,MIN(SUBSTRING(race_date,1,4)) as minYear
from drivers_gp_results where id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["seasons"] = $r["minYear"]." - ".$r["maxYear"];
}

//liczba gp
$query="SELECT COUNT(DISTINCT race_date) as races from gp,drivers_gp_results
where gp.id_gp=drivers_gp_results.id_gp and gp.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["races"] = $r["races"];
}

//zwyciezcy gp
$query="SELECT gp.id_gp id,race_date raceDate, SUBSTRING(race_date,1,4) season,";
if ($lang=='pl') {
  $query.="gp.name gp,";
}else{
  $query.="gp.name_en gp,";
}
$query.="gp.name_alias gpAlias,gp.country_code gpCountryCode, gp.name_short gpNameShort,
drivers.alias,drivers.id_driver idDriver,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode,
COALESCE(drivers_gp_results.race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0) points,
drivers_gp_results.race_time raceTime, drivers_gp_results.sprint_points,
(SELECT CONCAT(ppDrivers.name,' ',ppDrivers.surname) FROM drivers_gp_starting_grid, drivers ppDrivers WHERE 
 drivers_gp_starting_grid.id_driver=ppDrivers.id_driver AND 
 drivers_gp_starting_grid.is_pp=1 AND drivers_gp_starting_grid.season=drivers_gp_results.season AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp) ppDriver,
 (SELECT ppDrivers.alias FROM drivers_gp_starting_grid, drivers ppDrivers WHERE 
 drivers_gp_starting_grid.id_driver=ppDrivers.id_driver AND 
 drivers_gp_starting_grid.is_pp=1 AND drivers_gp_starting_grid.season=drivers_gp_results.season AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp) ppDriverAlias,
 teams_models.team_name modelName,
drivers_gp_results.race_add_info,
gp_season.length,
drivers_gp_results.race_laps laps,
gp_season.distance,
gp_season.version,
gp.circuit_alias circuitAlias
from drivers_gp_results
LEFT JOIN gp ON drivers_gp_results.id_gp=gp.id_gp
LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team 
LEFT JOIN drivers ON drivers_gp_results.id_driver=drivers.id_driver
LEFT JOIN drivers_sprint_results ON drivers_gp_results.id_driver=drivers_sprint_results.id_driver
and drivers_gp_results.id_team=drivers_sprint_results.id_team
and drivers_gp_results.id_gp=drivers_sprint_results.id_gp
and drivers_gp_results.season=drivers_sprint_results.season
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_drivers_gp=drivers_gp_results.id_drivers_gp
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_results.id_gp AND gp_season.season=drivers_gp_results.season
WHERE drivers_gp_results.race_pos=1 and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_date desc";
$result = mysqli_query($dbhandle,$query);
$winners = array();
while($r = mysqli_fetch_assoc($result)) {
  $winners[] = $r;
}
$careerItems["winners"] = $winners;

//pierwsza grand prix
$query="SELECT SUBSTRING(race_date,1,4) as year, race_date raceDate, drivers_gp_results.id_gp id, gp.name_alias alias
from drivers_gp_results,drivers,gp where gp.id_gp=drivers_gp_results.id_gp AND drivers_gp_results.id_driver=drivers.id_driver and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')
and race_pos=1 order by race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstGP"] = $r;
}

//ostatnia grand prix
$query="SELECT SUBSTRING(race_date,1,4) as year, race_date raceDate, drivers_gp_results.id_gp id, gp.name_alias alias
from drivers_gp_results,drivers,gp where gp.id_gp=drivers_gp_results.id_gp AND drivers_gp_results.id_driver=drivers.id_driver and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')
and race_pos=1 order by race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastGP"] = $r;
}

//pierwsze zwyciestwo
$query="SELECT gp.id_gp id,race_date raceDate, SUBSTRING(race_date,1,4) year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode
from drivers_gp_results,gp,teams,drivers where drivers_gp_results.id_gp=gp.id_gp
and drivers_gp_results.id_team=teams.id_team and drivers_gp_results.id_driver=drivers.id_driver
and race_pos=1 and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstWin"] = $r;
}

//ostatnie zwyciestwo
$query="SELECT gp.id_gp id,race_date raceDate, SUBSTRING(race_date,1,4) year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode
from drivers_gp_results,gp,teams,drivers where drivers_gp_results.id_gp=gp.id_gp
and drivers_gp_results.id_team=teams.id_team and drivers_gp_results.id_driver=drivers.id_driver
and race_pos=1 and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastWin"] = $r;
}

//pierwsze kwalifikacje
$query="SELECT SUBSTRING(qual_date,1,4) as year, qual_date qualDate, drivers_pp_results.id_gp id, gp.name_alias alias
from drivers_pp_results,drivers,gp where gp.id_gp=drivers_pp_results.id_gp AND drivers_pp_results.id_driver=drivers.id_driver and drivers_pp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')
and qual_pos=1 order by qual_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstQual"] = $r;
}

//ostatnie kwalifikacje
$query="SELECT SUBSTRING(qual_date,1,4) as year, qual_date qualDate, drivers_pp_results.id_gp id, gp.name_alias alias
from drivers_pp_results,drivers,gp where gp.id_gp=drivers_pp_results.id_gp AND drivers_pp_results.id_driver=drivers.id_driver and drivers_pp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id')
and qual_pos=1 order by qual_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastQual"] = $r;
}

//pierwsze pole position
$query="SELECT gp.id_gp id, drivers_gp_starting_grid.season year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode
from drivers_gp_starting_grid,gp,teams,drivers where drivers_gp_starting_grid.id_gp=gp.id_gp
and drivers_gp_starting_grid.id_team=teams.id_team and drivers_gp_starting_grid.id_driver=drivers.id_driver
and is_pp=1 and drivers_gp_starting_grid.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by year LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstPP"] = $r;
}

//ostatnie pole position
$query="SELECT gp.id_gp id, drivers_gp_starting_grid.season year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode
from drivers_gp_starting_grid,gp,teams,drivers where drivers_gp_starting_grid.id_gp=gp.id_gp
and drivers_gp_starting_grid.id_team=teams.id_team and drivers_gp_starting_grid.id_driver=drivers.id_driver
and is_pp=1 and drivers_gp_starting_grid.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by year desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastPP"] = $r;
}

//pierwsze najlepsze okrazenia
$query="SELECT gp.id_gp id,race_date raceDate, SUBSTRING(race_date,1,4) year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode, race_best_lap_time bestLapTime
from drivers_gp_results,gp,teams,drivers where drivers_gp_results.id_gp=gp.id_gp
and drivers_gp_results.id_team=teams.id_team and drivers_gp_results.id_driver=drivers.id_driver
and race_best_lap=1 and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstBestLap"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstBestLap"] = $r;
}

//ostatnie najlepsze okrazenia
$query="SELECT gp.id_gp id,race_date raceDate, SUBSTRING(race_date,1,4) year,gp.name gp,drivers.id_driver idDriver,drivers.alias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,teams.alias_name teamAlias,
teams.team idTeam, TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team, teams.country_code teamCountryCode, race_best_lap_time bestLapTime
from drivers_gp_results,gp,teams,drivers where drivers_gp_results.id_gp=gp.id_gp
and drivers_gp_results.id_team=teams.id_team and drivers_gp_results.id_driver=drivers.id_driver
and race_best_lap=1 and drivers_gp_results.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastBestLap"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastBestLap"] = $r;
}

//rekord toru
$query="SELECT drivers_gp_results.id_gp id,race_date raceDate,
drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,drivers.alias,
teams.team idTeam,TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.country_code teamCountryCode, race_best_lap_time time,teams.alias_name teamAlias
from drivers_gp_results,drivers,teams
where drivers_gp_results.id_driver=drivers.id_driver and drivers_gp_results.id_team=teams.id_team
and race_best_lap=1 and id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by race_best_lap_time LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["bestLap"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["bestLap"] = $r;
}

//rekord kwalifikacji
$query="SELECT drivers_pp_results.id_gp id,qual_date raceDate,
drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,drivers.alias,
teams.team idTeam,TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.country_code teamCountryCode, qual_time time,teams.alias_name teamAlias
from drivers_pp_results,drivers,teams
where drivers_pp_results.id_driver=drivers.id_driver and drivers_pp_results.id_team=teams.id_team
and qual_pos=1 and qual_completed=1 and id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') order by qual_time LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["bestQualTime"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["bestQualTime"] = $r;
}

// wersje toru
$query="SELECT distinct version, length, MIN(season) minSeason, MAX(season) maxSeason,";
if ($lang=='pl') {
  $query.="info";
}else{
  $query.="info_en info";
}
$query.=" FROM gp_season 
WHERE id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$id') 
group by version order by version";
$result = mysqli_query($dbhandle,$query);
$circuitVersions=array();
while($r = mysqli_fetch_assoc($result)) {
  $circuitVersions[] = $r;
}
$circuitItems["versions"]=$circuitVersions;

$circuitItems["career"]=$careerItems;

$circuitItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $circuitItems;

print json_encode($response);
mysqli_free_result($result);
?>
