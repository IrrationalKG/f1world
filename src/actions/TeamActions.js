// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TEAM_REQUEST = "FETCH_TEAM_REQUEST";
export const FETCH_TEAM_SUCCESS = "FETCH_TEAM_SUCCESS";
export const FETCH_TEAM_ERROR = "FETCH_TEAM_ERROR";

function requestData() {
  return {
    type: FETCH_TEAM_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TEAM_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TEAM_ERROR,
    payload: error,
  };
}

export function fetchTeam(id, year, lang, selectedSeason) {
  const url = `${ROOT_URL}/get_team_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
          selectedSeason: selectedSeason,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
