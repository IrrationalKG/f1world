// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GP_INFO = "FETCH_GP_INFO";
export const FETCH_GP_INFO_SUCCESS = "FETCH_GP_INFO_SUCCESS";
export const FETCH_GP_INFO_ERROR = "FETCH_GP_INFO_ERROR";

function requestData() {
  return {
    type: FETCH_GP_INFO,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GP_INFO_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GP_INFO_ERROR,
    payload: error,
  };
}

export function fetchGPInfo(year, lang) {
  const url = `${ROOT_URL}/get_gp_info.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
