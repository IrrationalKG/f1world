// const request = require('../json/combo_players.json');
//
// export const FETCH_COMBO_PLAYERS_SUCCESS = 'FETCH_COMBO_PLAYERS_SUCCESS';
//
// export function fetchComboPlayers() {
//   return {
//     type: FETCH_COMBO_PLAYERS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_COMBO_PLAYERS_REQUEST = 'FETCH_COMBO_PLAYERS_REQUEST';
export const FETCH_COMBO_PLAYERS_SUCCESS = 'FETCH_COMBO_PLAYERS_SUCCESS';
export const FETCH_COMBO_PLAYERS_ERROR = 'FETCH_COMBO_PLAYERS_ERROR';

function requestData() {
  return {
    type: FETCH_COMBO_PLAYERS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_PLAYERS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_PLAYERS_ERROR,
    payload: error
  };
}

export function fetchComboPlayers() {
  const url = `${ROOT_URL}/get_combo_players.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
