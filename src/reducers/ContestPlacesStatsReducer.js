import { FETCH_CONTEST_PLACES_STATS_REQUEST, FETCH_CONTEST_PLACES_STATS_SUCCESS, FETCH_CONTEST_PLACES_STATS_ERROR } from '../actions/ContestPlacesStatsActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CONTEST_PLACES_STATS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CONTEST_PLACES_STATS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CONTEST_PLACES_STATS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
