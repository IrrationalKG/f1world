/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Segment,
  Flag,
  Icon,
  Table,
  Header,
  Message,
  Image,
  Item,
  Statistic,
  Grid,
  Divider,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchClassTeams } from "../../actions/ClassTeamsActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ClassTeamsPoints.less";

class ClassTeamsPoints extends Component {
  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchClassTeams(props.match.params.year, Cookies.get("lang"));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;
    if (year !== nextYear) {
      props.fetchClassTeams(nextYear, Cookies.get("lang"));
    }
  }

  renderClassTeamsPoints() {
    const elements = [];
    const { props } = this;
    const { classPoints } = props.classTeams.data;
    const { year } = props.match.params;
    classPoints.forEach((item) => {
      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.place}>
          <Table.Cell data-title="Miejsce" className="cell-default-size">
            {item.points !== "0" && `${item.place}.`}
            {item.points === "0" && "-"}
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.name} - ${
                      item.engine ? item.engine : item.team
                    }`}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />{" "}
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPoints.map((e) => (
            <Table.Cell
              data-title={e.gp}
              key={e.gp}
              className={
                e.teamPoints > 0
                  ? `cell-default-size cell-points`
                  : "cell-default-size"
              }
            >
              <NavLink to={`/gp-result/${e.alias}/${year}`}>
                {e.teamPoints}
              </NavLink>
            </Table.Cell>
          ))}
          <Table.Cell data-title="Wygrane" className="cell-default-size">
            {item.wins === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className="cell-default-size">
            {item.podium === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className="cell-default-size">
            {item.pp === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.pp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenie" className="cell-default-size">
            {item.bestLap === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {item.bestLap}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="cell-default-size bold">
            {item.pointsClass === null ? (
              String.fromCharCode(160)
            ) : (
              <NavLink
                to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
              >
                {Math.round(item.pointsClass * 100) / 100}
              </NavLink>
            )}
            {item.points !== item.pointsClass && (
              <small>
                <NavLink
                  to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {" ("}
                  {item.points === null
                    ? String.fromCharCode(160)
                    : Math.round(item.points * 100) / 100}
                  {")"}
                </NavLink>
              </small>
            )}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsPointsMobile() {
    const items = [];
    const { classTeams } = this.props;
    const { classPoints } = classTeams.data;
    const { year } = this.props.match.params;
    classPoints.forEach((item, idx) => {
      const picReplace = "/build/images/teams/team_no_profile.jpg";
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      }
      const link = `/team/${item.alias}`;
      const element = (
        <Grid.Row key={item.id}>
          <Grid.Column width={4}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="image-link">
                <Image
                  size="small"
                  src={filename}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={10}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      {idx + 1}
                      {". "}
                      <Flag name={item.country} />
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.modelName.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </Item.Description>
                  <Item.Extra>
                    {item.gpPoints.map((e) => (
                      <span key={e.gp} className="key-value-box">
                        <div className="key-value-box-header">
                          {e.nameShort.toUpperCase()}
                        </div>
                        <div className="key-value-box-value">
                          {e.teamPoints === "" ? (
                            String.fromCharCode(160)
                          ) : (
                            <NavLink to={`/gp-result/${e.alias}/${year}`}>
                              {e.teamPoints}
                            </NavLink>
                          )}
                        </div>
                      </span>
                    ))}
                  </Item.Extra>
                  <Divider></Divider>
                  <Item.Extra>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.wins"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.wins === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.wins}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.podiums"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.podium === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.podium}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.polepos"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.pp === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.pp}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.bestLap === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                          >
                            {item.bestLap}
                          </NavLink>
                        )}
                      </div>
                    </span>
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right">
              <Statistic.Value>
                {item.pointsClass === null ? (
                  String.fromCharCode(160)
                ) : (
                  <NavLink
                    to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {Math.round(item.pointsClass * 100) / 100}
                  </NavLink>
                )}
                {item.points !== item.pointsClass && (
                  <small>
                    <NavLink
                      to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                    >
                      {" ("}
                      {item.points === null
                        ? String.fromCharCode(160)
                        : Math.round(item.points * 100) / 100}
                      {")"}
                    </NavLink>
                  </small>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  render() {
    const { props } = this;
    if (!props.classTeams.data || props.classTeams.loading) {
      return (
        <section
          id="class-teams-details"
          name="class-teams-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { regulations, gp } = props.classTeams.data;

    const linkPrev = `/classification/teams-points/${prevYear}`;
    const linkNext = `/classification/teams-points/${nextYear}`;

    if (gp.length === 0) {
      return (
        <section
          id="class-teams-details"
          name="class-teams-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.teams.points.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.teams.points.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                {year < 1950 && (
                  <FormattedMessage
                    id={"app.page.drivers.points.info1"}
                    values={{ year }}
                  />
                )}
                {year >= 1950 && (
                  <FormattedMessage
                    id={"app.page.drivers.points.info2"}
                    values={{ year }}
                  />
                )}
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }
    return (
      <section
        id="class-teams-details"
        name="class-teams-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.teams.points.title"}
              values={{ season: year }}
            />
          }
          subtitle={<FormattedMessage id={"app.page.teams.points.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          {year < 1958 && (
            <Segment padded basic>
              <Message>
                <Message.Header>
                  <FormattedMessage id={"app.message.header"} />
                </Message.Header>
                <Message.List>
                  <Message.Item>
                    <FormattedMessage
                      id={"app.page.teams.places.no.classification"}
                      values={{ year: year }}
                    />
                  </Message.Item>
                </Message.List>
              </Message>
            </Segment>
          )}
          {year >= 1958 && (
            <Segment basic>
              <SectionPageHeader
                title={<FormattedMessage id={"app.page.teams.points.header"} />}
                type="secondary"
              />
              <div className="hideForDesktop">
                <Grid columns={3} divided="vertically">
                  {this.renderClassTeamsPointsMobile()}
                </Grid>
              </div>
              <div className="hideForMobile">
                <Segment basic className="overflow">
                  <Table
                    basic="very"
                    celled
                    className="responsive-table center-aligned"
                  >
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>#</Table.HeaderCell>
                        <Table.HeaderCell className="left">
                          <FormattedMessage id={"app.table.header.team"} />
                        </Table.HeaderCell>
                        {gp.map((item) => (
                          <Table.HeaderCell
                            data-title={item.gp}
                            key={item.gp.toLowerCase().split(" ").join("_")}
                          >
                            {item.nameShort}
                            <br />
                            <Flag name={item.name} />
                          </Table.HeaderCell>
                        ))}
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.wins"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.podiums"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.polepos"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.bestlaps"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.points"} />
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>{this.renderClassTeamsPoints()}</Table.Body>
                  </Table>
                </Segment>
                <Segment padded basic>
                  <Message>
                    <p>
                      <FormattedMessage id={"app.page.teams.points.legend"} />
                    </p>
                  </Message>
                </Segment>
                <Regulations regulations={regulations}></Regulations>
              </div>
            </Segment>
          )}
        </div>
      </section>
    );
  }
}

ClassTeamsPoints.propTypes = {
  fetchClassTeams: PropTypes.func.isRequired,
  classTeams: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ classTeams }) {
  return { classTeams };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchClassTeams }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassTeamsPoints);
