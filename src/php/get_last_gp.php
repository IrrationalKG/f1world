<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

// pobranie id gp
$query="SELECT DISTINCT gp.id_gp FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp and gp.name_short='$id' AND gp_season.season='$year'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id_gp"];
}

//zwycięzca
$query="select drivers_gp_results.race_pos position,drivers.alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,drivers.photo,drivers_gp_results.race_time time,teams.name team from drivers_gp_results,drivers,teams where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and id_gp=$id and race_pos=1";
$result = mysqli_query($dbhandle,$query);
$winner=array();
while($r = mysqli_fetch_assoc($result)) {
   $winner = $r;
}

//drugie miejsce
$query="select drivers_gp_results.race_pos position,drivers.alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,drivers.photo,drivers_gp_results.race_time time,teams.name team from drivers_gp_results,drivers,teams where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and id_gp=$id and race_pos=2";
$result = mysqli_query($dbhandle,$query);
$second=array();
while($r = mysqli_fetch_assoc($result)) {
   $second = $r;
}

//trzecie miejsce
$query="select drivers_gp_results.race_pos position,drivers.alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,drivers.photo,drivers_gp_results.race_time time,teams.name team from drivers_gp_results,drivers,teams where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and id_gp=$id and race_pos=3";
$result = mysqli_query($dbhandle,$query);
$third=array();
while($r = mysqli_fetch_assoc($result)) {
    $third = $r;
}

//wyscig
$query="select drivers_gp_results.race_pos position,drivers.alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,drivers.photo,"; 
if ($lang=='pl') {
   $query.="drivers_gp_results.race_time time,drivers_gp_results.race_add_info info,";
 }else{
   $query.="(CASE 
  WHEN drivers_gp_results.race_time LIKE '+1 %' THEN REPLACE(drivers_gp_results.race_time,'okrąż.', 'lap')
 	WHEN drivers_gp_results.race_time LIKE '*%' THEN REPLACE(drivers_gp_results.race_time,'okrąż.', 'lap')
  ELSE REPLACE(drivers_gp_results.race_time,'okrąż.', 'laps')
  END) time, drivers_gp_results.race_add_info_en info,";
 } 
$query.="drivers_gp_results.race_points points, teams.name team, teams.engine from drivers_gp_results,drivers,teams where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and id_gp=$id";
$result = mysqli_query($dbhandle,$query);
$gpResults=array();
while($r = mysqli_fetch_assoc($result)) {
   $gpResults[] = $r;
}

//kwalifikacje
$query="SELECT drivers_pp_results.qual_pos position, drivers.alias, drivers.id_driver id, CONCAT(drivers.name, ' ', drivers.surname) name, drivers.country_code country, drivers_pp_results.qual_time time,";
if ($lang=='pl') {
   $query.="drivers_pp_results.qual_add_info info,";
}else{
   $query.="drivers_pp_results.qual_add_info_en info,";
} 
$query.="teams.name team, teams.engine
FROM drivers_pp_results, drivers, teams
WHERE drivers.id_driver = drivers_pp_results.id_driver AND teams.id_team = drivers_pp_results.id_team AND season = '$year' AND id_gp = $id";
$result = mysqli_query($dbhandle,$query);
$qualResults=array();
while($r = mysqli_fetch_assoc($result)) {
   $qualResults[] = $r;
}

$query="select gp.circuit_alias alias,gp.id_gp id,";
if ($lang=='pl') {
  $query.="gp.name,";
}else{
  $query.="gp.name_en as name,";
}  
$query.="lower(gp.name_short) country,gp.country_code countryCode,gp.circuit,name_alias nameAlias,'' winner,'' second,'' third, gp_season.sort round from gp, gp_season where gp.id_gp=$id AND gp.id_gp=gp_season.id_gp AND gp_season.season=${year}";
$result = mysqli_query($dbhandle,$query);
$gpItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["winner"]=$winner;
   $r["second"]=$second;
   $r["third"]=$third;
   $r["gpResults"]=$gpResults;
   $r["qualResults"]=$qualResults;
   $gpItems = $r;
}

// Response
$response = $gpItems;


print json_encode($response);
mysqli_free_result($result);
?>
