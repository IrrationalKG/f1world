<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT name,alias FROM stats_copy WHERE type='champs' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$champsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $champsItems[] = $r;
}
$statsItems["champions"]=$champsItems;

$query="SELECT name,alias FROM stats_copy WHERE type='wins' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$winsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $winsItems[] = $r;
}
$statsItems["wins"]=$winsItems;

$query="SELECT name,alias FROM stats_copy WHERE type='race' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$raceItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $raceItems[] = $r;
}
$statsItems["race"]=$raceItems;

$query="SELECT name,alias FROM stats_copy WHERE type='qual' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$qualItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $qualItems[] = $r;
}
$statsItems["qual"]=$qualItems;

$query="SELECT name,alias FROM stats_copy WHERE type='points' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$pointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $pointsItems[] = $r;
}
$statsItems["points"]=$pointsItems;

$query="SELECT name,alias FROM stats_copy WHERE type='bestlaps' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$bestlapsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $bestlapsItems[] = $r;
}
$statsItems["bestlaps"]=$bestlapsItems;

$query="SELECT name,alias FROM stats_copy WHERE type='podium' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$podiumItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $podiumItems[] = $r;
}
$statsItems["podium"]=$podiumItems;

$query="SELECT name,alias FROM stats_copy WHERE type='grandprix' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$grandprixItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $grandprixItems[] = $r;
}
$statsItems["grandprix"]=$grandprixItems;

$query="SELECT name,alias FROM stats_copy WHERE type='circuits' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$circuitsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $circuitsItems[] = $r;
}
$statsItems["circuits"]=$circuitsItems;

$query="SELECT name,alias FROM stats_copy WHERE type='misc' ORDER BY sort asc";
$result = mysqli_query($dbhandle,$query);
$miscItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $miscItems[] = $r;
}
$statsItems["misc"]=$miscItems;

$statsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $statsItems;

print json_encode($response);
mysqli_free_result($result);
?>
