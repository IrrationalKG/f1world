// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_CIRCUITS_REQUEST = "FETCH_COMBO_CIRCUITS_REQUEST";
export const FETCH_COMBO_CIRCUITS_SUCCESS = "FETCH_COMBO_CIRCUITS_SUCCESS";
export const FETCH_COMBO_CIRCUITS_ERROR = "FETCH_COMBO_CIRCUITS_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_CIRCUITS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_CIRCUITS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_CIRCUITS_ERROR,
    payload: error,
  };
}

export function fetchComboCircuits(
  empty,
  driver,
  team,
  gp,
  engine,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_circuits.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          driver: driver,
          team: team,
          gp: gp,
          engine: engine,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
