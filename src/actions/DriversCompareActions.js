// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_DRIVERS_COMPARE_REQUEST = "FETCH_DRIVERS_COMPARE_REQUEST";
export const FETCH_DRIVERS_COMPARE_SUCCESS = "FETCH_DRIVERS_COMPARE_SUCCESS";
export const FETCH_DRIVERS_COMPARE_ERROR = "FETCH_DRIVERS_COMPARE_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVERS_COMPARE_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVERS_COMPARE_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVERS_COMPARE_ERROR,
    payload: error,
  };
}

export function fetchDriversCompare(driver1, driver2, season, lang) {
  const url = `${ROOT_URL}/get_drivers_compare.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          driver1: driver1,
          driver2: driver2,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
