<?
header('Access-Control-Allow-Origin: *');

$season=$_GET['season'];
if ($season=='-') $season=$_POST['season'];
if ($season=='-') $season=null;

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// zespoly
$query="SELECT teams.id_team, teams.name, teams.engine FROM teams_class LEFT JOIN teams ON teams.id_team=teams_class.id_team WHERE season='$season' ORDER BY sort";
$result = mysqli_query($dbhandle,$query);
$drivers;
$driversItems;
while($r = mysqli_fetch_assoc($result)) {
   $team = $r["id_team"];
   $drivers = '';
   $drivers["team"] = $team;
   $drivers["name"] = $r["name"];
   $drivers["engine"] = $r["engine"];

   $query2="SELECT drivers.alias,drivers.id_driver id,drivers.name,drivers.surname, drivers.country, drivers.country_code countryCode,
   (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE race_date>='$season-01-01' AND race_date<='$season-12-31'
   AND id_driver = drivers.id_driver
   AND race_pos < (SELECT race_pos FROM drivers_gp_results d2 WHERE d2.race_date>='$season-01-01' AND d2.race_date<='$season-12-31'
   AND d2.id_driver = (SELECT id_driver FROM drivers_class WHERE id_team=$team AND season=$season AND id_driver!=drivers.id_driver order by sort ASC LIMIT 1)
   AND d2.id_gp=drivers_gp_results.id_gp)) race,
   (SELECT COUNT(drivers_pp_results.id_drivers_pp) FROM drivers_pp_results WHERE qual_date>='$season-01-01' AND qual_date<='$season-12-31'
   AND id_driver = drivers.id_driver
   AND qual_pos < (SELECT qual_pos FROM drivers_pp_results d3 WHERE d3.qual_date>='$season-01-01' AND d3.qual_date<='$season-12-31'
   AND d3.id_driver = (SELECT id_driver FROM drivers_class WHERE id_team=$team AND season=$season AND id_driver!=drivers.id_driver order by sort ASC LIMIT 1)
   AND d3.id_gp=drivers_pp_results.id_gp)) qual
   FROM drivers, drivers_class
   WHERE drivers.id_driver = drivers_class.id_driver
   AND drivers_class.id_team=$team
   AND drivers_class.season=$season
   AND drivers_class.sort<=20
   ORDER BY drivers_class.sort";
   $result2 = mysqli_query($dbhandle,$query2);
   $i = 1;

   while($r2 = mysqli_fetch_assoc($result2)) {
     $drivers["driver".$i] = $r2;
     $i+=1;
   }
   $driversItems[] = $drivers;


}

$driversCompareItems["drivers"]=$driversItems;
$driversCompareItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $driversCompareItems;

print json_encode($response);
mysqli_free_result($result);
?>
