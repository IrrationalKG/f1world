<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;


include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

// Nazwa GP
$gpname="";
$query="select name gpname from gp where gp.name_short='$gp'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_row($result)) {
   $gpname = $r[0];
}

// sezony typowania
$query="SELECT count(distinct sezon) seasons,uczestnik id from typy where is_deleted=0 group by uczestnik";
$result = mysqli_query($dbhandle,$query);
$seasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$seasons[$tmp_id] = $r["seasons"];
}
// zwyciestwa w rundach
$query="SELECT count(*) first,uczestnik id FROM typy WHERE place=1 and is_deleted=0 GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$first = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$first[$tmp_id] = $r["first"];
}
// drugie miejsca w rundach
$query="SELECT count(*) second,uczestnik id FROM typy WHERE place=2 and is_deleted=0 GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$second = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$second[$tmp_id] = $r["second"];
}
// trzecie miejsca w rundach
$query="SELECT count(*) third,uczestnik id FROM typy WHERE place=3 and is_deleted=0 GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$third = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$third[$tmp_id] = $r["third"];
}

// zwyciestwa w sezonie
$query="SELECT count(*) first,uczestnik id FROM typy WHERE place=1 and is_deleted=0 and sezon=$year GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$classFirst = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$classFirst[$tmp_id] = $r["first"];
}
// drugie miejsca w sezonie
$query="SELECT count(*) second,uczestnik id FROM typy WHERE place=2 and is_deleted=0 and sezon=$year GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$classSecond = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$classSecond[$tmp_id] = $r["second"];
}
// trzecie miejsca w sezonie
$query="SELECT count(*) third,uczestnik id FROM typy WHERE place=3 and is_deleted=0 and sezon=$year GROUP by uczestnik";
$result = mysqli_query($dbhandle,$query);
$classThird = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id = $r["id"];
	$classThird[$tmp_id] = $r["third"];
}

// Wyniki rundy
$query="SELECT uczestnik id,alias,SUBSTRING(name,1,30) name,surname,short_name,suma points,pkt_gp pointsgp,
'' seasons,'' first,'' second,'' third
from typy,users where users.id_user=typy.uczestnik and sezon= '$year' and gp='$gp' and is_deleted=0
order by suma desc,pkt_gp desc,surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$resultsItems = array();
while($r = mysqli_fetch_assoc($result)) {
   $r["seasons"] = !isset($seasons[$r["id"]]) ? 0 : $seasons[$r["id"]];
   $r["first"] = !isset($first[$r["id"]]) ? 0 : $first[$r["id"]];
   $r["second"] = !isset($second[$r["id"]]) ? 0 : $second[$r["id"]];
   $r["third"] = !isset($third[$r["id"]]) ? 0 : $third[$r["id"]];
   $resultsItems[] = $r;
}

$results = array();
$results['id'] = "results";
$results['season'] = $year;
$results['gp'] = $gp;
$results['gpname'] = $gpname;
$results['items'] = $resultsItems;

// Klasyfikacja zwykła
$query="SELECT id_user id,CONCAT(name,' ',surname) name, alias, place, diffrence diff, points,'' pointPlaces,'' first,'' second,'' third
FROM competition_places,users
WHERE users.id_user=competition_places.user AND competition_places.season='$year'
AND round = (SELECT MAX(round) FROM competition_places WHERE season='$year')
ORDER BY place, competition_places.points DESC,users.surname LIMIT 10";
// $query="SELECT uczestnik id,alias,short_name,name,surname,SUM(suma) points,'' totalPoints,'' first,'' second,'' third
// from typy,users where users.id_user=typy.uczestnik and sezon= '$year' and is_deleted=0 group by uczestnik
// order by points desc, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$classItems = array();
while($r = mysqli_fetch_assoc($result)) {
   $r["first"] = !isset($classFirst[$r["id"]]) ? 0 : $classFirst[$r["id"]];
   $r["second"] = !isset($classSecond[$r["id"]]) ? 0 : $classSecond[$r["id"]];
   $r["third"] = !isset($classThird[$r["id"]]) ? 0 : $classThird[$r["id"]];
   $classItems[] = $r;
}

$class = array();
$class['id'] = "class";
$class['season'] = $year;
$class['items'] = $classItems;

// Klasyfikacja GP
$query="SELECT id_user id,CONCAT(name,' ',surname) name, alias, place, diffrence diff, points,'' pointPlaces,'' first,'' second,'' third
FROM competition_places_gp,users
WHERE users.id_user=competition_places_gp.user AND competition_places_gp.season='$year'
AND round = (SELECT MAX(round) FROM competition_places_gp WHERE season='$year')
ORDER BY place, competition_places_gp.points DESC,users.surname LIMIT 10";
// $query="SELECT uczestnik id,alias,short_name,name,surname,SUM(pkt_gp) points,'' pointPlaces,'' first,'' second,'' third
// from typy,users where users.id_user=typy.uczestnik and sezon= '$year' and is_deleted=0 group by uczestnik
// order by points desc,surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$classGPItems = array();
while($r = mysqli_fetch_assoc($result)) {
  $r["first"] = !isset($classFirst[$r["id"]]) ? 0 : $classFirst[$r["id"]];
  $r["second"] = !isset($classSecond[$r["id"]]) ? 0 : $classSecond[$r["id"]];
  $r["third"] = !isset($classThird[$r["id"]]) ? 0 : $classThird[$r["id"]];
  $classGPItems[] = $r;
}

$classgp = array();
$classgp['id'] = "class-gp";
$classgp['season'] = $year;
$classgp['items'] = $classGPItems;

// Response
$response[0] = $results;
$response[1] = $class;
$response[2] = $classgp;


print json_encode($response);
mysqli_free_result($result);
?>
