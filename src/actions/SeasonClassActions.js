// const request = require('../json/season_class.json');
//
// export const FETCH_SEASON_CLASS_SUCCESS = 'FETCH_SEASON_CLASS_SUCCESS';
//
// export function fetchSeasonClass() {
//   return {
//     type: FETCH_SEASON_CLASS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_SEASON_CLASS = 'FETCH_SEASON_CLASS';
export const FETCH_SEASON_CLASS_SUCCESS = 'FETCH_SEASON_CLASS_SUCCESS';
export const FETCH_SEASON_CLASS_ERROR = 'FETCH_SEASON_CLASS_ERROR';

function requestData() {
  return {
    type: FETCH_SEASON_CLASS
  };
}

function receiveData(data) {
  return {
    type: FETCH_SEASON_CLASS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_SEASON_CLASS_ERROR,
    payload: error
  };
}

export function fetchSeasonClass(year) {
  const url = `${ROOT_URL}/get_season_class.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url, {
      params: {
        year: year
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
