/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React from 'react';
import styles from './NotFound.less';

export default function() {
  return (
    <section id="not-found">
      The page you are looking for was not found.
    </section>
  );
}
