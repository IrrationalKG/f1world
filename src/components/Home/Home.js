/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint camelcase: ["error", {allow: ["UNSAFE_componentWillMount"]}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Loader, Grid, Segment } from "semantic-ui-react";
import PropTypes from "prop-types";
import Trivia from "../../containers/Trivia/Trivia";
import NewsCarousel from "../../containers/NewsCarousel/NewsCarousel";
import LastGP from "../../containers/LastGP/LastGP";
import NextGP from "../../containers/NextGP/NextGP";
import SeasonClass from "../../containers/SeasonClass/SeasonClass";
import Contest from "../../containers/Contest/Contest";
import GPCalendar from "../../containers/GPCalendar/GPCalendar";
import DriversCompare from "../../containers/DriversCompare/DriversCompare";
import DriversCompareTeammates from "../../containers/DriversCompareTeammates/DriversCompareTeammates";
import RecordsWidget from "../../containers/RecordsWidget/RecordsWidget";
import StatsWidget from "../../containers/StatsWidget/StatsWidget";
import GPCalendarWidget from "../../containers/GPCalendarWidget/GPCalendarWidget";
import { BrowserView, MobileOnlyView, TabletView } from "react-device-detect";
import styles from "./Home.less";

class Home extends Component {
  render() {
    const { props } = this;
    const { params } = props;
    if (!props.params.data || props.params.loading) {
      return (
        <section id="home">
          <div className="full-height">
            <Loader active inline="centered">
              Ładowanie danych...
            </Loader>
          </div>
        </section>
      );
    }
    const {
      currentGPYear,
      lastGPYear,
      lastGP,
      lastGPAlias,
      lastGPId,
      nextGPYear,
      nextGP,
      nextGPAlias,
      nextGPId,
      contestRound,
    } = params.data;

    return (
      <section id="home">
        <Trivia />
        <BrowserView>
          <GPCalendarWidget year={currentGPYear} />
          <Grid stackable centered>
            <Grid.Column
              mobile={16}
              tablet={4}
              computer={3}
              className="left-sidebar"
            >
              {currentGPYear && <SeasonClass year={currentGPYear} />}
              {currentGPYear && (
                <DriversCompareTeammates year={currentGPYear} />
              )}
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={10}
              className="main-content"
            >
              <Segment basic>
                <StatsWidget />
                <NewsCarousel />
                {lastGPYear && lastGP && (
                  <Contest
                    year={lastGPYear}
                    gp={lastGP}
                    lastGPAlias={lastGPAlias}
                    nextGPAlias={nextGPAlias}
                    round={contestRound}
                  />
                )}
                {currentGPYear && <GPCalendar year={currentGPYear} />}
                <RecordsWidget />
                <DriversCompare />
              </Segment>
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={4}
              computer={3}
              className="right-sidebar"
            >
              {nextGPYear && nextGPId && (
                <NextGP year={nextGPYear} id={nextGPId} />
              )}
              {lastGPYear && lastGPId && (
                <LastGP year={lastGPYear} id={lastGPId} />
              )}
            </Grid.Column>
          </Grid>
        </BrowserView>
        <TabletView>
          <Grid stackable centered>
            <Grid.Column
              mobile={16}
              tablet={4}
              computer={3}
              className="left-sidebar"
            >
              {currentGPYear && <SeasonClass year={currentGPYear} />}
              {currentGPYear && (
                <DriversCompareTeammates year={currentGPYear} />
              )}
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={8}
              computer={10}
              className="main-content"
            >
              <Segment basic padded="very">
                <StatsWidget />
                <NewsCarousel />
                {lastGPYear && lastGP && (
                  <Contest
                    year={lastGPYear}
                    gp={lastGP}
                    lastGPAlias={lastGPAlias}
                    nextGPAlias={nextGPAlias}
                    round={contestRound}
                  />
                )}
                {currentGPYear && <GPCalendar year={currentGPYear} />}
                <RecordsWidget />
                <DriversCompare />
              </Segment>
            </Grid.Column>
            <Grid.Column
              mobile={16}
              tablet={4}
              computer={3}
              className="right-sidebar"
            >
              {nextGPYear && nextGPId && (
                <NextGP year={nextGPYear} id={nextGPId} />
              )}
              {lastGPYear && lastGPId && (
                <LastGP year={lastGPYear} id={lastGPId} />
              )}
            </Grid.Column>
          </Grid>
        </TabletView>
        <MobileOnlyView>
          <StatsWidget />
          <NewsCarousel className="main-content" />
          {lastGPYear && lastGPId && <LastGP year={lastGPYear} id={lastGPId} />}
          {nextGPYear && nextGPId && <NextGP year={nextGPYear} id={nextGPId} />}
          {currentGPYear && <SeasonClass year={currentGPYear} />}
          {currentGPYear && <DriversCompareTeammates year={currentGPYear} />}
          {currentGPYear && <GPCalendar year={currentGPYear} />}
          {lastGPYear && lastGP && (
            <Contest
              year={lastGPYear}
              gp={lastGP}
              lastGPAlias={lastGPAlias}
              nextGPAlias={nextGPAlias}
              round={contestRound}
            />
          )}
          <RecordsWidget />
          <DriversCompare />
        </MobileOnlyView>
      </section>
    );
  }
}

Home.propTypes = {
  params: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  lastGPYear: PropTypes.string,
  lastGP: PropTypes.string,
  nextGPYear: PropTypes.string,
};

function mapStateToProps({ params }) {
  return { params };
}

export default connect(mapStateToProps)(Home);
