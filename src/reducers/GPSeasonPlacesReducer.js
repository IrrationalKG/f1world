import { FETCH_GPSEASON_PLACES_REQUEST, FETCH_GPSEASON_PLACES_SUCCESS, FETCH_GPSEASON_PLACES_ERROR } from '../actions/GPSeasonPlacesActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_GPSEASON_PLACES_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_GPSEASON_PLACES_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_GPSEASON_PLACES_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
