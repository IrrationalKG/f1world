/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Segment, Flag, Loader, Image, Table, Divider } from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchDriversCompare } from "../../actions/DriversCompareActions";
import { fetchComboDrivers } from "../../actions/ComboDriversActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriversCompare.less";

class DriversCompare extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectDriver1Value: { value: "-" },
      selectDriver2Value: { value: "-" },
      selectSeasonValue: { value: "2024", label: "2024" },
    };
    this.handleDriver1Change = this.handleDriver1Change.bind(this);
    this.handleDriver2Change = this.handleDriver2Change.bind(this);
    this.handleSeasonChange = this.handleSeasonChange.bind(this);
  }

  componentDidMount() {
    const { props, state } = this;
    props.fetchComboDrivers(
      0,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    props.fetchDriversCompare(
      null,
      null,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
  }

  UNSAFE_componentWillUpdate(nextProps) {
    const { props } = this;

    if (
      nextProps.driversCompare.data !== null &&
      props.driversCompare.data !== nextProps.driversCompare.data
    ) {
      this.setState({
        selectDriver1Value: {
          value: nextProps.driversCompare.data.driver1.id,
          label: nextProps.comboDrivers.data?.find(
            (e) => e.value == nextProps.driversCompare.data.driver1.alias
          )?.label,
        },
        selectDriver2Value: {
          value: nextProps.driversCompare.data.driver2.id,
          label: nextProps.comboDrivers.data?.find(
            (e) => e.value == nextProps.driversCompare.data.driver2.alias
          )?.label,
        },
        selectSeasonValue: {
          value: nextProps.driversCompare.data.season,
          label:
            nextProps.comboSeasons.data?.find(
              (e) => e.value == nextProps.driversCompare.data.season
            )?.label || "Wszystkie sezony",
        },
      });
    }
  }

  handleDriver1Change(event) {
    const { props, state } = this;
    this.setState({
      selectDriver1Value: {
        value: event.value,
        label: props.comboDrivers.data?.find((e) => e.value == event.value)
          ?.label,
      },
    });
    props.fetchDriversCompare(
      event.value,
      state.selectDriver2Value.value,
      state.selectSeasonValue.value
    );
  }

  handleDriver2Change(event) {
    const { props, state } = this;
    this.setState({
      selectDriver2Value: {
        value: event.value,
        label: props.comboDrivers.data?.find((e) => e.value == event.value)
          ?.label,
      },
    });

    props.fetchDriversCompare(
      state.selectDriver1Value.value,
      event.value,
      state.selectSeasonValue.value
    );
  }

  handleSeasonChange(event) {
    const { props } = this;
    this.setState({
      selectSeasonValue: {
        value: event.value,
        label: props.comboSeasons.data?.find((e) => e.value == event.value)
          ?.label,
      },
    });
    props.fetchDriversCompare(null, null, event.value);
  }

  renderDriversGPResultsCompare() {
    const items = [];
    const elements = [];
    const { props } = this;
    const { driver1, driver2, season } = props.driversCompare.data;

    let drivers1Count = 0;
    let drivers2Count = 0;
    const gpResults = driver1.gpResults || [];
    gpResults.forEach((item, idx) => {
      let driver1Class = "";
      let driver2Class = "";

      if (item.place && driver2?.gpResults[idx].place) {
        driver1Class =
          parseInt(item.place) < parseInt(driver2?.gpResults[idx].place)
            ? "cell-better"
            : "cell-worse";
        driver2Class =
          parseInt(item.place) > parseInt(driver2?.gpResults[idx].place)
            ? "cell-better"
            : "cell-worse";
      }

      if (parseInt(item.place) < parseInt(driver2?.gpResults[idx].place)) {
        drivers1Count += 1;
      }
      if (parseInt(item.place) > parseInt(driver2?.gpResults[idx].place)) {
        drivers2Count += 1;
      }
      const element = (
        <Table.Row key={idx}>
          <Table.Cell className={driver1Class}>
            <h4>
              {item.place ? (
                item.completed == "1" ? (
                  <NavLink
                    to={`/driver-events/race-places/${driver1.alias}/-/${item.alias}/-/-/-/-/${season}/${item.place}/`}
                  >
                    {item.place}
                  </NavLink>
                ) : (
                  <>
                    <div>
                      <NavLink
                        to={`/driver-events/incomplete/${driver1.alias}/-/${item.alias}/-/-/-/-/${season}/-/`}
                      >
                        -
                      </NavLink>
                    </div>
                    <small className="vexry">{item.info}</small>
                  </>
                )
              ) : (
                ""
              )}
            </h4>
          </Table.Cell>
          <Table.Cell>
            <Flag name={item.name} />
            <b>
              <NavLink to={`/gp-result/${item.alias}/${season}`}>
                {item.gp.toUpperCase()}
              </NavLink>
            </b>
          </Table.Cell>
          <Table.Cell className={driver2Class}>
            <h4>
              {driver2?.gpResults[idx].place ? (
                driver2?.gpResults[idx].completed == "1" ? (
                  <NavLink
                    to={`/driver-events/race-places/${driver2.alias}/-/${driver2?.gpResults[idx].alias}/-/-/-/-/${season}/${driver2?.gpResults[idx].place}/`}
                  >
                    {driver2?.gpResults[idx].place}
                  </NavLink>
                ) : (
                  <>
                    <NavLink
                      to={`/driver-events/incomplete/${driver2.alias}/-/${driver2?.gpResults[idx].alias}/-/-/-/-/${season}/-/`}
                    >
                      <div>-</div>
                    </NavLink>
                    <small className="very">
                      {driver2?.gpResults[idx].info}
                    </small>
                  </>
                )
              ) : (
                ""
              )}
            </h4>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });

    items.push(
      <Table.Row key="total">
        <Table.Cell
          className={
            drivers1Count > drivers2Count ? "cell-better" : "cell-worse"
          }
        >
          <h4>{drivers1Count}</h4>
        </Table.Cell>
        <Table.Cell>
          <b>
            {" "}
            <FormattedMessage
              id={"app.page.home.drivers.compare.whos.better"}
            />
          </b>
        </Table.Cell>
        <Table.Cell
          className={
            drivers2Count > drivers1Count ? "cell-better" : "cell-worse"
          }
        >
          <h4>{drivers2Count}</h4>
        </Table.Cell>
      </Table.Row>
    );
    items.push(elements);
    return items;
  }

  render() {
    const { props } = this;
    if (!props.driversCompare.data) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    const { driver1, driver2, drivers } = props.driversCompare.data;
    const {
      isLoadingExternally,
      multi,
      ignoreCase,
      ignoreAccents,
      clearable,
      selectDriver1Value,
      selectDriver2Value,
      selectSeasonValue,
    } = this.state;
    const picDriver1 = `../build/images/drivers/driver_${driver1.id}_profile.jpg`;
    const picDriver2 = `../build/images/drivers/driver_${driver2.id}_profile.jpg`;

    const linkDriver1 = `/driver/${driver1.alias}`;
    const linkDriver2 = `/driver/${driver2.alias}`;

    return (
      <section id="drivers-compare" name="drivers-compare">
        <div className="drivers-compare-header">
          <h2>
            <FormattedMessage id={"app.page.home.drivers.compare.title"} />
          </h2>
          <h3>
            <FormattedMessage id={"app.page.home.drivers.compare.subtitle"} />
          </h3>
        </div>
        <div className="drivers-compare-content">
          <Segment basic textAlign="center">
            <div className="hideForDesktop">
              <div className="combo-drivers">
                <Select
                  ref={(ref) => {
                    this.comboSeasons = ref;
                  }}
                  isLoading={isLoadingExternally}
                  id="season"
                  name="season"
                  multi={multi}
                  ignoreCase={ignoreCase}
                  ignoreAccents={ignoreAccents}
                  value={selectSeasonValue}
                  onChange={this.handleSeasonChange}
                  options={props.comboSeasons.data}
                  clearable={clearable}
                  labelKey="label"
                  valueKey="value"
                  placeholder="Sezon"
                  noResultsText={
                    <FormattedMessage id={"app.placeholder.no.results"} />
                  }
                  className="react-select-container"
                  classNamePrefix="react-select"
                  aria-label="driver1"
                />
              </div>
              <div className="combo-drivers">
                <Select
                  ref={(ref) => {
                    this.comboDriver1 = ref;
                  }}
                  isLoading={isLoadingExternally}
                  id="driver1"
                  name="driver1"
                  multi={multi}
                  ignoreCase={ignoreCase}
                  ignoreAccents={ignoreAccents}
                  value={selectDriver1Value}
                  onChange={this.handleDriver1Change}
                  options={drivers}
                  clearable={clearable}
                  labelKey="label"
                  valueKey="value"
                  placeholder="Kierowca 1"
                  noResultsText={
                    <FormattedMessage id={"app.placeholder.no.results"} />
                  }
                  className="react-select-container"
                  classNamePrefix="react-select"
                  aria-label="season"
                />
              </div>
              <div className="combo-drivers">
                <Select
                  ref={(ref) => {
                    this.comboDriver2 = ref;
                  }}
                  isLoading={isLoadingExternally}
                  id="driver2"
                  name="driver2"
                  multi={multi}
                  ignoreCase={ignoreCase}
                  ignoreAccents={ignoreAccents}
                  value={selectDriver2Value}
                  onChange={this.handleDriver2Change}
                  options={drivers}
                  clearable={clearable}
                  labelKey="label"
                  valueKey="value"
                  placeholder="Kierowca 2"
                  noResultsText={
                    <FormattedMessage id={"app.placeholder.no.results"} />
                  }
                  className="react-select-container"
                  classNamePrefix="react-select"
                  aria-label="driver2"
                />
              </div>
            </div>
            <Table basic celled unstackable fixed>
              <Table.Body>
                <Table.Row className="vertical-top-aligned">
                  <Table.Cell>
                    <div className="combo-drivers hideForMobile">
                      <Select
                        ref={(ref) => {
                          this.comboDriver1 = ref;
                        }}
                        isLoading={isLoadingExternally}
                        id="driver1"
                        name="driver1"
                        multi={multi}
                        ignoreCase={ignoreCase}
                        ignoreAccents={ignoreAccents}
                        value={selectDriver1Value}
                        onChange={this.handleDriver1Change}
                        options={drivers}
                        clearable={clearable}
                        labelKey="label"
                        valueKey="value"
                        placeholder="Kierowca 1"
                        noResultsText={
                          <FormattedMessage id={"app.placeholder.no.results"} />
                        }
                        className="react-select-container"
                        classNamePrefix="react-select"
                        aria-label="driver1"
                      />
                    </div>
                    <div className="center-aligned">
                    <Divider hidden fitted></Divider>
                      <NavLink to={linkDriver1}>
                        {driver1.picture === "1" ? (
                          <Image
                            size="medium"
                            centered
                            src={picDriver1}
                            alt={`${driver1.name} ${driver1.surname}`}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        ) : (
                          <Image
                            size="small"
                            centered
                            src="/build/images/drivers/driver_no_profile.jpg"
                            alt="no photo"
                          />
                        )}
                      </NavLink>
                      <h3>
                        <NavLink to={linkDriver1}>
                          <Flag name={driver1.countryCode} /> {driver1.name}{" "}
                          {driver1.surname}
                        </NavLink>
                      </h3>
                    </div>
                  </Table.Cell>
                  <Table.Cell>
                    <div className="combo-drivers hideForMobile">
                      <Select
                        ref={(ref) => {
                          this.comboSeasons = ref;
                        }}
                        isLoading={isLoadingExternally}
                        id="season"
                        name="season"
                        multi={multi}
                        ignoreCase={ignoreCase}
                        ignoreAccents={ignoreAccents}
                        value={selectSeasonValue}
                        onChange={this.handleSeasonChange}
                        options={props.comboSeasons.data}
                        clearable={clearable}
                        labelKey="label"
                        valueKey="value"
                        placeholder="Sezon"
                        noResultsText={
                          <FormattedMessage id={"app.placeholder.no.results"} />
                        }
                        className="react-select-container"
                        classNamePrefix="react-select"
                        aria-label="season"
                      />
                    </div>
                    <div className="center-aligned">
                      <div className="versus">VS</div>
                    </div>
                  </Table.Cell>
                  <Table.Cell>
                    <div className="combo-drivers hideForMobile">
                      <Select
                        ref={(ref) => {
                          this.comboDriver2 = ref;
                        }}
                        isLoading={isLoadingExternally}
                        id="driver2"
                        name="driver2"
                        multi={multi}
                        ignoreCase={ignoreCase}
                        ignoreAccents={ignoreAccents}
                        value={selectDriver2Value}
                        onChange={this.handleDriver2Change}
                        options={drivers}
                        clearable={clearable}
                        labelKey="label"
                        valueKey="value"
                        placeholder="Kierowca 2"
                        noResultsText={
                          <FormattedMessage id={"app.placeholder.no.results"} />
                        }
                        className="react-select-container"
                        classNamePrefix="react-select"
                        aria-label="driver2"
                      />
                    </div>
                    <div className="center-aligned">
                      <Divider hidden fitted></Divider>
                      <NavLink to={linkDriver2}>
                        {driver2.picture === "1" ? (
                          <Image
                            size="medium"
                            centered
                            src={picDriver2}
                            alt={`${driver2.name} ${driver2.surname}`}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        ) : (
                          <Image
                            size="small"
                            centered
                            src="/build/images/drivers/driver_no_profile.jpg"
                            alt="no photo"
                          />
                        )}
                      </NavLink>
                      <h3>
                        <NavLink to={linkDriver2}>
                          <Flag name={driver2.countryCode} /> {driver2.name}{" "}
                          {driver2.surname}
                        </NavLink>
                      </h3>
                    </div>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>{driver1.stats.seasons}</h4>
                  </Table.Cell>
                  <Table.Cell>
                    <FormattedMessage id={"app.stats.f1.sesons"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>{driver2.stats.seasons}</h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>{driver1.stats.seasonsNo}</h4>
                  </Table.Cell>
                  <Table.Cell>
                    <FormattedMessage id={"app.stats.seasons"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>{driver2.stats.seasonsNo}</h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.bestResult}
                      {!driver1.stats.bestResult && "-"}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.best.result"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.bestResult}
                      {!driver2.stats.bestResult && "-"}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.starts}
                      {driver1.stats.starts !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.starts}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.races"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.starts}
                      {driver2.stats.starts !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.starts}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.wins}
                      {driver1.stats.wins !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.wins}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.wins"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.wins}
                      {driver2.stats.wins !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.wins}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.points}
                      {driver1.stats.points !== "0.00" && (
                        <small>
                          {" ("}
                          {driver1.ranking.points}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.points"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.points}
                      {driver2.stats.points !== "0.00" && (
                        <small>
                          {" ("}
                          {driver2.ranking.points}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.podium}
                      {driver1.stats.podium !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.podium}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.podium"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.podium}
                      {driver2.stats.podium !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.podium}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.polepos}
                      {driver1.stats.polepos !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.polepos}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.polepos"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.polepos}
                      {driver2.stats.polepos !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.polepos}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.bestlaps}
                      {driver1.stats.bestlaps !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.bestlaps}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.bestlaps"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.bestlaps}
                      {driver2.stats.bestlaps !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.bestlaps}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.completed}
                      {driver1.stats.completed !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.completed}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.completed"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.completed}
                      {driver2.stats.completed !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.completed}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.incomplete}
                      {driver1.stats.incomplete !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.incomplete}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.incomplete"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.incomplete}
                      {driver2.stats.incomplete !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.incomplete}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <h4>
                      {driver1.stats.disqualifications}
                      {driver1.stats.disqualifications !== "0" && (
                        <small>
                          {" ("}
                          {driver1.ranking.disqualifications}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                  <Table.Cell>
                    {" "}
                    <FormattedMessage id={"app.stats.disq"} />
                  </Table.Cell>
                  <Table.Cell>
                    <h4>
                      {driver2.stats.disqualifications}
                      {driver2.stats.disqualifications !== "0" && (
                        <small>
                          {" ("}
                          {driver2.ranking.disqualifications}
                          {".)"}
                        </small>
                      )}
                    </h4>
                  </Table.Cell>
                </Table.Row>
                {selectSeasonValue.value && (
                  <>
                    <Table.Row>
                      <Table.Cell className="drivers-compare-season-results">
                        <h4>
                          {driver1.name} {driver1.surname}
                        </h4>
                      </Table.Cell>
                      <Table.Cell className="drivers-compare-season-results">
                        <h4>
                          <FormattedMessage
                            id={"app.page.home.drivers.compare.race.places"}
                          />{" "}
                          {selectSeasonValue.value}
                        </h4>
                      </Table.Cell>
                      <Table.Cell className="drivers-compare-season-results">
                        <h4>
                          {" "}
                          {driver2.name} {driver2.surname}
                        </h4>
                      </Table.Cell>
                    </Table.Row>
                    {this.renderDriversGPResultsCompare()}
                  </>
                )}
              </Table.Body>
            </Table>
          </Segment>
        </div>
      </section>
    );
  }
}

DriversCompare.propTypes = {
  fetchDriversCompare: PropTypes.func.isRequired,
  fetchComboDrivers: PropTypes.func.isRequired,
  fetchComboSeasons: PropTypes.func.isRequired,
  driversCompare: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  comboDrivers: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ driversCompare, comboDrivers, comboSeasons }) {
  return { driversCompare, comboDrivers, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchDriversCompare, fetchComboDrivers, fetchComboSeasons },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(DriversCompare);
