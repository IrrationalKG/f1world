/*global __dirname*/
const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const AssetsPlugin = require("assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");

module.exports = {
  mode: "production",
  // devtool: 'source-map',
  entry: [
    "./src/index.js" // Your apps entry point
  ],
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/build/",
    filename: "[name]-chunk.[hash].js",
    chunkFilename: "[name]-chunk.[chunkhash].js"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      "../../theme.config$": path.join(
        __dirname,
        "src/styles/semantic-ui/theme.config"
      )
    }
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        uglifyOptions: {
          output: {
            comments: /@license/i
          }
        },
        extractComments: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
      cacheGroups: {
        semantic: {
          test: /[\\/]node_modules[\\/](semantic-ui-react|semantic-ui-less)[\\/]/,
          name: "semantic",
          chunks: "all",
          priority: 1
        },
        vendors: {
          test: /[\\/]node_modules[\\/]((?!(jquery)).*)[\\/]/,
          name: "vendors",
          chunks: "all",
          reuseExistingChunk: true
        },
        common: {
          name: "common",
          minChunks: 2,
          chunks: "all",
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-react", "@babel/preset-env"]
          }
        }
      },
      {
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader"
          },
          {
            loader: "less-loader"
          }
        ],
        test: /\.(less)$/
      },
      {
        enforce: "pre",
        test: /\.(less|scss|css)$/,
        use: {
          loader: "alias-resolve-loader",
          options: {
            alias: {
              "../../theme.config": path.join(
                __dirname,
                "src/styles/semantic-ui/theme.config"
              ),
              "../../themes/default/assets/images": path.join(
                __dirname,
                "src/images"
              )
            }
          }
        }
      },
      {
        use: [
          {
            loader: "file-loader",
            // options: {
            //   name: "fonts/[name].[ext]"
            // }
            options: {
              name: "fonts/[name].[ext]",
              context: "./src"
            }
          }
        ],
        test: /\.(woff|woff2|eot|ttf|otf)$/
      },
      // kopiuje wszystkie includowane pliki graficzne z katalogu src do build
      {
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]",
              context: "./src"
            }
          }
        ],
        test: /\.(png|svg|jpe?g|gif|webp)$/i
      } // ,
      // {
      //   use: [
      //     {
      //       loader: 'json-loader',
      //       options: {
      //         name: '[path][name].[ext]',
      //         context: './src'
      //       }
      //     }
      //   ],
      //   test: /\.(json)$/
      // }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "index.ejs"
    }),
    new FaviconsWebpackPlugin({
      logo: "./src/icons/favicon.jpg",
      prefix: "icons/"
    }),
    new CopyWebpackPlugin(
      [
        { context: "src/images", from: "*/*/*.*", to: "images" },
        { context: "src/images", from: "*/*.*", to: "images" },
        { context: "src/images", from: "*.*", to: "images" },
        { from: "src/json/*.json", to: "json" },
        { context: "src/styles/fonts", from: "*.*", to: "fonts" }
      ],
      {
        copyUnmodified: true
      }
    ),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),
    new MiniCssExtractPlugin({
      filename: "css-bundle.[hash].css",
      chunkFilename: "[name].css-chunk.[hash].css"
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new AssetsPlugin(),
    new BundleAnalyzerPlugin({
      analyzerMode: "static",
      generateStatsFile: false
    })
  ]
};
