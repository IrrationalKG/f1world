/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  GridRow,
  Loader,
  Header,
  Segment,
  Item,
  Flag,
  Icon,
  Table,
  Statistic,
  Divider,
  Popup,
  Message,
  Label,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import Regulations from "../Regulations/Regulations";
import { fetchGPResult } from "../../actions/GPResultActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPResult.less";

class GPResult extends Component {
  constructor(props) {
    super(props);
    const { formatMessage } = this.props.intl;

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectSeason: {
        value: props.match.params.year,
      },
      selectEvent: {
        value: "race",
        label: formatMessage({ id: "app.stats.race" }),
      },
    };

    this.onSeasonChange = this.onSeasonChange.bind(this);
    this.onEventChange = this.onEventChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPResult(
      props.match.params.year,
      props.match.params.gpId,
      Cookies.get("lang")
    );
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    const { gpId, year } = props.match.params;
    const nextId = nextProps.match.params.gpId;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectSeason: {
        value: nextYear,
        label: nextProps.gpresult.data?.seasons
          ?.map((s) => {
            return { value: s, label: s };
          })
          .find((e) => e.value == nextYear)?.label,
      },
      selectEvent: {
        value: "race",
        label: formatMessage({ id: "app.stats.race" }),
      },
    });

    if (gpId !== nextId || year !== nextYear) {
      props.fetchGPResult(nextYear, nextId, Cookies.get("lang"));
    }
  }

  onSeasonChange(event) {
    const { props } = this;
    const { alias } = props.gpresult.data;

    props.history.push({ pathname: `/gp-result/${alias}/${event.value}` });
    this.setState({
      selectSeason: event.value,
    });
  }

  onEventChange(event) {
    this.setState({
      selectEvent: {
        value: event.value,
        label: event.label,
      },
    });
  }

  renderDriversPoints() {
    const items = [];
    const { props } = this;
    const { drivers } = props.gpresult.data.results.points;
    const { formatMessage } = this.props.intl;

    drivers.forEach((item, idx) => {
      const picReplace = "/build/images/drivers/driver_no_profile.jpg";
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      }
      const link = `/driver/${item.driverAlias}`;
      const element = (
        <Item key={item.idDriver}>
          <NavLink to={link}>
            <Image
              size="small"
              src={filename}
              alt={item.driver}
              className="cell-photo"
              onError={(e) => {
                e.target.src = picReplace;
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {idx + 1}
                {". "}
                <Flag name={item.driverCountryCode} />
                {item.driver}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.team}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              {item.excluded === "0" && (
                <Statistic.Value>{item.points}</Statistic.Value>
              )}
              {item.excluded === "1" && (
                <Popup
                  trigger={
                    <Statistic.Value>
                      {"("}
                      {item.points}
                      {")"}
                    </Statistic.Value>
                  }
                  content={formatMessage({
                    id: "app.page.gpresults.driver.points.excluded",
                  })}
                />
              )}
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderDriversPointsMobile() {
    const items = [];
    const { props } = this;
    const { drivers } = props.gpresult.data.results.points;
    const { formatMessage } = this.props.intl;

    drivers.forEach((item, idx) => {
      const picReplace = "/build/images/drivers/driver_no_profile.jpg";
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      }
      const link = `/driver/${item.driverAlias}`;
      const element = (
        <Grid.Row key={item.idDriver}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="item-photo">
                <Image
                  size="tiny"
                  src={filename}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      {idx + 1}
                      {". "}
                      <Flag name={item.driverCountryCode} />
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>{item.team}</Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right" inverted>
              {item.excluded === "0" && (
                <Statistic.Value>{item.points}</Statistic.Value>
              )}
              {item.excluded === "1" && (
                <Popup
                  trigger={
                    <Statistic.Value>
                      {"("}
                      {item.points}
                      {")"}
                    </Statistic.Value>
                  }
                  content={formatMessage({
                    id: "app.page.gpresults.driver.points.excluded",
                  })}
                />
              )}
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsPoints() {
    const items = [];
    const { props } = this;
    const { teams } = props.gpresult.data.results.points;
    const { formatMessage } = this.props.intl;

    teams.forEach((item, idx) => {
      const picReplace = "/build/images/teams/team_no_profile.jpg";
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;
      }
      const link = `/team/${item.teamAlias}`;
      const element = (
        <Item key={`${item.name} - ${item.engine}`}>
          <NavLink to={link}>
            <Image
              size="small"
              src={filename}
              alt={`${item.name} - ${item.engine}`}
              className="cell-photo"
              onError={(e) => {
                e.target.src = picReplace;
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {idx + 1}
                {". "}
                <Flag name={item.teamCountryCode} />
                {item.name} {item.engine}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.modelName}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              {item.excluded === "0" && (
                <Statistic.Value>{item.points}</Statistic.Value>
              )}
              {item.excluded === "1" && (
                <Popup
                  trigger={
                    <Statistic.Value>
                      {"("}
                      {item.points}
                      {")"}
                    </Statistic.Value>
                  }
                  content={formatMessage({
                    id: "app.page.gpresults.driver.points.excluded",
                  })}
                />
              )}
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsPointsMobile() {
    const items = [];
    const { props } = this;
    const { teams } = props.gpresult.data.results.points;
    const { formatMessage } = this.props.intl;

    teams.forEach((item, idx) => {
      const picReplace = "/build/images/teams/team_no_profile.jpg";
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;
      }
      const link = `/team/${item.teamAlias}`;
      const element = (
        <Grid.Row key={`${item.name} - ${item.engine}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="item-photo">
                <Image
                  size="tiny"
                  src={filename}
                  alt={`${item.name} - ${item.engine}`}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      {idx + 1}
                      {". "}
                      <Flag name={item.teamCountryCode} />
                      {item.name} {item.engine}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>{item.modelName}</Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right" inverted>
              {item.excluded === "0" && (
                <Statistic.Value>{item.points}</Statistic.Value>
              )}
              {item.excluded === "1" && (
                <Popup
                  trigger={
                    <Statistic.Value>
                      {"("}
                      {item.points}
                      {")"}
                    </Statistic.Value>
                  }
                  content={formatMessage({
                    id: "app.page.gpresults.driver.points.excluded",
                  })}
                />
              )}
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderRaceResult() {
    const items = [];
    const { props } = this;
    const { race } = props.gpresult.data.results;
    const { formatMessage } = this.props.intl;

    let showNotClassifiedHeader = false;
    race.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;

      const picCar = `/build/images/teams/models/team_${item.idTeam.toLowerCase()}_${item.model
        ?.toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      if (item.completed == 0 && !showNotClassifiedHeader) {
        showNotClassifiedHeader = true;
        const element = (
          <Table.Row key="nk">
            <Table.Cell data-title="Niesklasyfikowani" colSpan="11">
              <FormattedMessage
                id={"app.page.gpresults.driver.not.classified"}
              />
            </Table.Cell>
          </Table.Row>
        );
        items.push(element);
      }
      const diff = item.gridPlace - item.place;
      const cellBorder = item.coDriver != 0 ? " cell-top-border" : "";
      const cellDiff =
        diff != 0 ? (diff > 0 ? "race-position-up" : "race-position-down") : "";
      const firstCellBorder =
        item.coDriver != 0 ? " first-cell-top-border" : "";
      const element = (
        <Table.Row key={`${item.place}_${item.driverAlias}`}>
          <Table.Cell
            data-title="Miejsce"
            className={`no-wrap${firstCellBorder}`}
          >
            {item.completed === "1" && item.coDriver == 0 && `${item.place}.`}
            {item.completed === "1" && item.coDriver == 1 && ""}
            {item.completed === "0" && item.disq == 1 && (
              <FormattedMessage id={"app.table.header.disq"} />
            )}
            {item.completed === "0" &&
              item.disq == 0 &&
              item.coDriver == 0 &&
              "-"}
            {item.completed === "0" &&
              item.disq == 0 &&
              item.coDriver == 1 &&
              ""}
          </Table.Cell>
          <Table.Cell
            data-title="+/-"
            className={`center no-wrap${cellBorder} ${cellDiff}`}
          >
            {item.gridPlace && diff > 0 && (
              <Icon color="green" name="caret up" />
            )}
            {item.gridPlace && diff < 0 && (
              <Icon color="red" name="caret down" />
            )}
            {item.gridPlace && diff == 0 && <>-</>}
            {item.gridPlace && <>{diff != 0 ? diff : ""}</>}
          </Table.Cell>
          <Table.Cell data-title="Numer" className={`no-wrap${cellBorder}`}>
            <div>
              {!item.number || item.number == "0" ? (
                ""
              ) : (
                <Label basic> # {item.number} </Label>
              )}
            </div>
          </Table.Cell>
          <Table.Cell
            data-title="Kierowca"
            className={`left no-wrap${cellBorder}`}
          >
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.driverAlias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>{" "}
                  {item.bestLap == "1" && (
                    <Popup
                      trigger={
                        <sup>
                          <FormattedMessage id={"app.table.header.bestlaps"} />
                        </sup>
                      }
                    >
                      <span className="block">
                        <FormattedMessage id={"app.stats.bestlap"} />:{" "}
                        {item.bestLapTime}
                      </span>
                    </Popup>
                  )}
                </div>
                <div>
                  <small>
                    {" "}
                    <FormattedMessage id={"app.table.header.polepos"} />:{" "}
                    {item.gridPlace ? item.gridPlace : "-"}
                  </small>
                </div>
                {item.sharedDrive != "" && (
                  <div>
                    <small className="cell-info">
                      <Icon name="linkify" size="small" />{" "}
                      {`${formatMessage({
                        id: "app.stats.shared.drive",
                      })}: ${item.sharedDrive}`}
                    </small>
                  </div>
                )}
              </div>
            </div>
          </Table.Cell>
          <Table.Cell
            data-title="Zespół"
            className={`left no-wrap${cellBorder}`}
          >
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.team} - ${
                      item.engine ? item.engine : item.team
                    }`}
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>{item.team}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell
            data-title="Silnik"
            className={`left no-wrap${cellBorder}`}
          >
            {item.engine ? item.engine : item.team}
          </Table.Cell>
          <Table.Cell
            data-title="Model"
            className={`left no-wrap${cellBorder}`}
          >
            <Popup
              trigger={
                <span>
                  {item.model
                    ? `${item.model
                        ?.replace("_", "/")
                        .replace("???", "nieznany")
                        .replace("??", "nieznany")
                        .replace("?", "nieznany")}`
                    : "nieznany"}
                </span>
              }
            >
              <Image
                src={picCar}
                alt={`${item.name} ${item.engine} ${item.model?.replaceAll(
                  "???",
                  ""
                )} ${item.season}`}
                className="car-model-photo"
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_car.jpg";
                }}
              />
            </Popup>
          </Table.Cell>
          <Table.Cell
            data-title="Opony"
            className={`left no-wrap${cellBorder}`}
          >
            {item.tyre}
          </Table.Cell>
          <Table.Cell data-title="Czas" className={`left${cellBorder}`}>
            {item.time}
            {item.info !== "" && (
              <div>
                <small className="cell-info">{item.info}</small>
              </div>
            )}
            {item.sprintPoints > 0 && (
              <div>
                <small>
                  {formatMessage({
                    id: "app.stats.points.sprint.part1",
                  }) +
                    item.sprintPoints +
                    formatMessage({
                      id: "app.stats.points.sprint.part2",
                    }) +
                    item.sprintPos +
                    formatMessage({
                      id: "app.stats.points.sprint.part3",
                    })}
                </small>
              </div>
            )}
          </Table.Cell>
          <Table.Cell data-title="Okrąż." className={`${cellBorder}`}>
            {item.laps}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={`${cellBorder} bold`}>
            {item.points > 0 && item.excluded === "0" && (
              <Statistic.Value>{item.points}</Statistic.Value>
            )}
            {item.points > 0 && item.excluded === "1" && (
              <Popup
                trigger={
                  <Statistic.Value>
                    {"("}
                    {item.points}
                    {")"}
                  </Statistic.Value>
                }
                content={formatMessage({
                  id: "app.page.gpresults.driver.points.excluded",
                })}
              />
            )}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderRaceResultMobile() {
    const items = [];
    const { props } = this;
    const { race } = props.gpresult.data.results;
    const { formatMessage } = this.props.intl;

    let showNotClassifiedHeader = false;
    race.forEach((item) => {
      if (item.completed == 0 && !showNotClassifiedHeader) {
        showNotClassifiedHeader = true;
        const element = (
          <Grid.Row key="nk">
            <Grid.Column width={16}>
              <div className="table-not-classified">
                {" "}
                <FormattedMessage
                  id={"app.page.gpresults.driver.not.classified"}
                />
              </div>
            </Grid.Column>
          </Grid.Row>
        );
        items.push(element);
      }
      const diff = item.qualPlace - item.place;

      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const element = (
        <Grid.Row key={`${item.place}_${item.driverAlias}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink
                to={`/driver/${item.driverAlias}`}
                className="item-photo"
              >
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src = driverPicReplace;
                  }}
                />
                <Segment basic textAlign="center">
                  {item.coDriver == 0 && (
                    <small>
                      <FormattedMessage id={"app.table.header.polepos"} />
                      {": "}
                      {item.gridPlace ? (
                        <>
                          {item.gridPlace}
                          {""}
                        </>
                      ) : (
                        <>{<FormattedMessage id={"app.stats.ns"} />}</>
                      )}{" "}
                    </small>
                  )}
                  <br />
                  {item.gridPlace && diff > 0 && (
                    <>
                      <Icon color="green" name="caret up" />
                      <small>{diff}</small>
                    </>
                  )}
                  {item.gridPlace && diff < 0 && (
                    <>
                      <Icon color="red" name="caret down" />
                      <small>{diff}</small>
                    </>
                  )}
                  {item.gridPlace && diff == 0 && (
                    <>
                      <Icon color="grey" name="arrows alternate horizontal" />
                      <small>{diff}</small>
                    </>
                  )}
                </Segment>
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.completed === "1" && `${item.place}.`}
                    {item.completed === "0" && item.disq == 1 && (
                      <FormattedMessage id={"app.table.header.disq"} />
                    )}
                    {item.completed === "0" && item.disq == 0 && ""}{" "}
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      <Flag name={item.driverCountryCode} />
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.team} {item.engine} {item.model?.replace("_", "/")}
                    </NavLink>
                  </Item.Description>
                  {item.tyre && (
                    <Item.Description>{item.tyre}</Item.Description>
                  )}
                  <Item.Description>
                    {item.time}
                    {item.completed == 1 && (
                      <>
                        {" ("}
                        {item.laps}
                        {")"}
                      </>
                    )}
                  </Item.Description>
                  <Item.Description>
                    {item.sharedDrive != "" && (
                      <div>
                        <small className="cell-info">
                          <Icon name="linkify" size="small" />{" "}
                          {`${formatMessage({
                            id: "app.stats.shared.drive",
                          })}: ${item.sharedDrive}`}
                        </small>
                      </div>
                    )}
                  </Item.Description>
                  <Item.Description>
                    {item.sprintPoints > 0 && (
                      <div>
                        <small>
                          {formatMessage({
                            id: "app.stats.points.sprint.part1",
                          }) +
                            item.sprintPoints +
                            formatMessage({
                              id: "app.stats.points.sprint.part2",
                            }) +
                            item.sprintPos +
                            formatMessage({
                              id: "app.stats.points.sprint.part3",
                            })}
                        </small>
                      </div>
                    )}
                  </Item.Description>
                  <Item.Extra>
                    {item.info !== "" && (
                      <div>
                        <small>{item.info}</small>
                      </div>
                    )}
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            {item.points > 0 && (
              <Statistic floated="right">
                <Statistic.Value>
                  {item.excluded === "0" && (
                    <Statistic.Value>{item.points}</Statistic.Value>
                  )}
                  {item.excluded === "1" && (
                    <Popup
                      trigger={
                        <Statistic.Value>
                          {"("}
                          {item.points}
                          {")"}
                        </Statistic.Value>
                      }
                      content={formatMessage({
                        id: "app.page.gpresults.driver.points.excluded",
                      })}
                    />
                  )}
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            )}
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderGrid() {
    const items = [];
    const { props } = this;
    const { grid } = props.gpresult.data.results;
    grid.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;

      const picCar = `/build/images/teams/models/team_${item.idTeam.toLowerCase()}_${item.model
        ?.toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const rowClassName =
        item.notStarted == 1
          ? "unclassified"
          : item.fromPits == 1 || item.fromBackGrid == 1
          ? "pit-lane"
          : "";

      const element = (
        <Table.Row
          key={`${item.place}_${item.driverAlias}`}
          className={rowClassName}
        >
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Kierowca" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.driverAlias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>
                </div>
                <div>
                  {!item.number || item.number == "0"
                    ? ""
                    : item.number.split(", ").map((item) => {
                        return (
                          <Label basic key={item}>
                            {" "}
                            # {item}{" "}
                          </Label>
                        );
                      })}
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.team} - ${
                      item.engine ? item.engine : item.team
                    }`}
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>{item.team}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Silnik" className="left no-wrap">
            {item.engine ? item.engine : item.team}
          </Table.Cell>
          <Table.Cell data-title="Model" className="left no-wrap">
            <Popup
              trigger={
                <span>
                  {item.model
                    ? `${item.model
                        ?.replace("_", "/")
                        .replace("???", "nieznany")
                        .replace("??", "nieznany")
                        .replace("?", "nieznany")}`
                    : "nieznany"}
                </span>
              }
            >
              <Image
                src={picCar}
                alt={`${item.name} ${item.engine} ${item.model?.replaceAll(
                  "???",
                  ""
                )} ${item.season}`}
                className="car-model-photo"
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_car.jpg";
                }}
              />
            </Popup>
          </Table.Cell>
          <Table.Cell data-title="Opony" className="left no-wrap">
            {item.tyre}
          </Table.Cell>
          <Table.Cell data-title="Info" className="left">
            {item.info !== "" && (
              <div>
                <small className="cell-info">{item.info}</small>
              </div>
            )}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderGridMobile() {
    const items = [];
    const { props } = this;
    const { grid } = props.gpresult.data.results;
    grid.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const element = (
        <Grid.Row key={`${item.place}_${item.driverAlias}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink
                to={`/driver/${item.driverAlias}`}
                className="item-photo"
              >
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src = driverPicReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.place}.{" "}
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      <Flag name={item.driverCountryCode} />
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.team} {item.engine} {item.model?.replace("_", "/")}
                    </NavLink>
                  </Item.Description>
                  {item.tyre && (
                    <Item.Description>{item.tyre}</Item.Description>
                  )}
                  <Item.Extra>
                    {item.info !== "" && (
                      <div>
                        <small className="cell-info">{item.info}</small>
                      </div>
                    )}
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderQualResult() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { qual } = props.gpresult.data.results;
    qual.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;

      const picCar = `/build/images/teams/models/team_${item.idTeam?.toLowerCase()}_${item.model
        ?.toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const rowClassName =
        item.notQualified == 1 || item.notStarted == 1 ? "unclassified" : "";

      const element = (
        <Table.Row
          key={`${item.place}_${item.driverAlias}`}
          className={rowClassName}
        >
          <Table.Cell data-title="Miejsce">
            {item.completed === "1" && `${item.place}.`}
            {item.completed === "0" && "-"}
          </Table.Cell>
          {year >= 2006 && (
            <Table.Cell
              data-title="Segment"
              className={`qual-stage-${item.segment.toLowerCase()}`}
            >
              {item.segment}
            </Table.Cell>
          )}
          <Table.Cell data-title="Kierowca" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.driverAlias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>
                </div>
                <div>
                  {!item.number || item.number == "0"
                    ? ""
                    : item.number.split(", ").map((item) => {
                        return (
                          <Label basic key={item}>
                            {" "}
                            # {item}{" "}
                          </Label>
                        );
                      })}
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.team} - ${
                      item.engine ? item.engine : item.team
                    }`}
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>{item.team}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Silnik" className="left no-wrap">
            {item.engine ? item.engine : item.team}
          </Table.Cell>
          <Table.Cell data-title="Model" className="left no-wrap">
            <Popup
              trigger={
                <div>
                  {item.model
                    ? `${item.model
                        ?.replace("_", "/")
                        .replace("???", "nieznany")
                        .replace("??", "nieznany")
                        .replace("?", "nieznany")}`
                    : "nieznany"}
                </div>
              }
            >
              <Image
                src={picCar}
                alt={`${item.name} ${item.engine} ${item.model?.replaceAll(
                  "???",
                  ""
                )} ${item.season}`}
                className="car-model-photo"
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_car.jpg";
                }}
              />
            </Popup>
          </Table.Cell>
          <Table.Cell data-title="Opony" className="left no-wrap">
            {item.tyre}
          </Table.Cell>
          <Table.Cell data-title="Czas" className="left">
            {item.time}
          </Table.Cell>
          <Table.Cell data-title="Info" className="left">
            {item.info !== "" && (
              <div>
                <small className="cell-info">{item.info}</small>
              </div>
            )}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderQualResultMobile() {
    const items = [];
    const { props } = this;
    const { qual } = props.gpresult.data.results;
    qual.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const element = (
        <Grid.Row key={`${item.place}_${item.driverAlias}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink
                to={`/driver/${item.driverAlias}`}
                className="item-photo"
              >
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src = driverPicReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.completed === "1" && `${item.place}.`}
                    {item.completed === "0" && "-"}{" "}
                    <Flag name={item.driverCountryCode} />
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.team} {item.engine} {item.model?.replace("_", "/")}
                    </NavLink>
                  </Item.Description>
                  {item.tyre && (
                    <Item.Description>{item.tyre}</Item.Description>
                  )}
                  <Item.Extra>
                    {item.info !== "" && (
                      <div>
                        <small className="cell-info">{item.info}</small>
                      </div>
                    )}
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>{item.time}</Item.Header>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderSprintResult() {
    const items = [];
    const { props } = this;
    const { sprint } = props.gpresult.data.results;
    sprint.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;

      const picCar = `/build/images/teams/models/team_${item.idTeam.toLowerCase()}_${item.model
        ?.toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const element = (
        <Table.Row key={`${item.place}_${item.driverAlias}`}>
          <Table.Cell data-title="Miejsce">
            {item.completed === "1" && `${item.place}.`}
            {item.completed === "0" &&
              item.info.startsWith("dyskwalifikacja") && (
                <FormattedMessage id={"app.table.header.disq"} />
              )}
            {item.completed === "0" &&
              !item.info.startsWith("dyskwalifikacja") &&
              "-"}
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.driverAlias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>
                </div>
                <div>
                  {!item.number || item.number == "0" ? (
                    ""
                  ) : (
                    <Label basic> # {item.number} </Label>
                  )}
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="left no-wrap">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.team} - ${
                      item.engine ? item.engine : item.team
                    }`}
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>{item.team}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Silnik" className="left no-wrap">
            {item.engine ? item.engine : item.team}
          </Table.Cell>
          <Table.Cell data-title="Model" className="left no-wrap">
            <Popup
              trigger={
                <span>{`${item.model
                  ?.replace("_", "/")
                  .replace("?", "")}`}</span>
              }
            >
              <Image
                src={picCar}
                alt={`${item.name} ${item.engine} ${item.model?.replaceAll(
                  "???",
                  ""
                )} ${item.season}`}
                className="car-model-photo"
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_car.jpg";
                }}
              />
            </Popup>
          </Table.Cell>
          <Table.Cell data-title="Opony" className="left no-wrap">
            {item.tyre}
          </Table.Cell>
          <Table.Cell data-title="Czas" className="left">
            {item.time}
            {item.info !== "" && (
              <div>
                <small className="cell-info">{item.info}</small>
              </div>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="bold">
            {item.points > 0 && (
              <Statistic.Value>{item.points}</Statistic.Value>
            )}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderSprintResultMobile() {
    const items = [];
    const { props } = this;
    const { sprint } = props.gpresult.data.results;
    sprint.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const element = (
        <Grid.Row key={`${item.place}_${item.driverAlias}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink
                to={`/driver/${item.driverAlias}`}
                className="item-photo"
              >
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src = driverPicReplace;
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.completed === "1" && `${item.place}.`}
                    {item.completed === "0" &&
                      item.info.startsWith("dyskwalifikacja") &&
                      "DS. "}
                    {item.completed === "0" &&
                      !item.info.startsWith("dyskwalifikacja") &&
                      ""}{" "}
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      <Flag name={item.driverCountryCode} />
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.team} {item.engine} {item.model?.replace("_", "/")}
                    </NavLink>
                  </Item.Description>
                  {item.tyre && (
                    <Item.Description>{item.tyre}</Item.Description>
                  )}
                  <Item.Description>{item.time}</Item.Description>
                  <Item.Extra>
                    {item.info !== "" && (
                      <div>
                        <small>{item.info}</small>
                      </div>
                    )}
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            {item.points > 0 && (
              <Statistic floated="right">
                <Statistic.Value>
                  <Statistic.Value>{item.points}</Statistic.Value>
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            )}
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderEntrants() {
    const items = [];
    const { props } = this;
    const { entrants } = props.gpresult.data.results;
    entrants.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;

      const teamPicReplace = "/build/images/teams/team_no_logo.jpg";

      const element = (
        <Grid.Column
          key={item.idDriver}
          textAlign="center"
          className="menu-box-container"
        >
          <Grid>
            <Grid.Row>
              <Grid.Column verticalAlign="top">
                <Segment basic>
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    <Image
                      centered
                      src={driverPic}
                      alt={`${item.driver}`}
                      onError={(e) => {
                        e.target.src = driverPicReplace;
                      }}
                    />
                  </NavLink>
                  <Label attached="top left" color="red">
                    &nbsp;
                    {!item.number || item.number == "0"
                      ? ""
                      : item.number.split(", ").map((item) => {
                          return <span key={item}>#{item} </span>;
                        })}
                    &nbsp;
                  </Label>
                  <Segment basic>
                    {!item.id_drivers_pp && (
                      <Label attached="bottom" color="red">
                        <FormattedMessage id={"app.stats.qual.not.started"} />
                      </Label>
                    )}
                    {!item.id_drivers_gp && (
                      <Label attached="bottom" color="red">
                        <FormattedMessage id={"app.stats.race.not.started"} />
                      </Label>
                    )}
                  </Segment>
                </Segment>
                <Segment basic>
                  {item.teamAlias.split(", ").map((teamAlias, index) => {
                    return (
                      <NavLink to={`/team/${teamAlias}`} key={index}>
                        <Image
                          src={`/build/images/teams/team_${
                            item.idTeam.split(", ")[index]
                          }_logo.jpg`}
                          alt={`${item.team.split(", ")[index]}`}
                          onError={(e) => {
                            e.target.src = teamPicReplace;
                          }}
                        />
                      </NavLink>
                    );
                  })}
                </Segment>
                <Segment basic className="menu-box-flex">
                  <Divider hidden fitted></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <Flag name={item.driverCountryCode} /> {item.driver}
                      <Header.Subheader>
                        {item.team.split(", ").map((item) => {
                          return <div key={item}>{item} </div>;
                        })}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider></Divider>
                  <small className="bold">{item.modelName}</small>
                  <div>
                    <small>
                      {item.engine ? item.engine : item.team} {item.model}
                    </small>
                  </div>
                  <Divider hidden fitted></Divider>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      );
      items.push(element);
    });
    return items;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpresult.data || props.gpresult.loading) {
      return (
        <section
          id="gpresult-details"
          name="gpresult-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { year } = props.match.params;
    const {
      name,
      alias,
      circuit,
      circuitAlias,
      prevId,
      nextId,
      raceDate,
      countryShort,
      sprint,
      seasons,
      regulations,
    } = props.gpresult.data;
    const { race, bestLap } = props.gpresult.data.results;

    const linkPrev = `/gp-result/${prevId}/${year}`;
    const linkNext = `/gp-result/${nextId}/${year}`;

    const picReplace = "/build/images/drivers/driver_no_profile.jpg";
    const winnerPic = `/build/images/drivers/driver_${race[0]?.idDriver}_profile.jpg`;
    const secondPic = `/build/images/drivers/driver_${race[1]?.idDriver}_profile.jpg`;
    const thirdPic = `/build/images/drivers/driver_${race[2]?.idDriver}_profile.jpg`;
    const countryPic = `/build/images/countries/${countryShort}.jpg`;
    const picCircuit = `/build/images/circuits/circuit_${circuitAlias}_profile.jpg`;

    let eventsComboOptions = [];
    eventsComboOptions.push({
      value: "race",
      label: formatMessage({ id: "app.stats.race" }),
    });
    eventsComboOptions.push({
      value: "grid",
      label: formatMessage({ id: "app.stats.grid" }),
    });
    eventsComboOptions.push({
      value: "qual",
      label: formatMessage({ id: "app.stats.qual" }),
    });
    if (sprint == "1") {
      eventsComboOptions.push({
        value: "sprint",
        label: formatMessage({ id: "app.stats.sprint" }),
      });
    }
    eventsComboOptions.push({
      value: "entrants",
      label: formatMessage({ id: "app.stats.entrants" }),
    });

    if (race.length === 0) {
      return (
        <section
          id="gpresult-details"
          name="gpresult-details"
          className="section-page"
        >
          <SectionPageBanner
            label={raceDate}
            title={name}
            subtitle={circuit}
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                {name} <FormattedMessage id={"app.page.gpresults.info"} />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }
    return (
      <section
        id="gpresult-details"
        name="gpresult-details"
        className="section-page"
      >
        <SectionPageBanner
          label={raceDate}
          title={name}
          subtitle={circuit}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <Segment basic padded>
                  <Select
                    ref={(ref) => {
                      this.comboContestSeason = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="seasons"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectSeason}
                    onChange={this.onSeasonChange}
                    options={seasons
                      .map((s) => {
                        return { value: s, label: s };
                      })
                      .sort((a, b) => b.value - a.value)}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={formatMessage({
                      id: "app.placeholder.select.season",
                    })}
                    noResultsText={
                      <FormattedMessage id={"app.placeholder.no.results"} />
                    }
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </Segment>
                <Grid centered>
                  <Grid.Column mobile={8} tablet={16} computer={16}>
                    <Image
                      className="gpresult-country-photo"
                      centered
                      src={countryPic}
                    />
                  </Grid.Column>
                  <Grid.Column mobile={8} tablet={16} computer={16}>
                    <Image
                      className="gpresult-circuit-photo"
                      centered
                      src={picCircuit}
                      alt={circuit}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/circuits/circuit_no_profile.jpg";
                      }}
                    />
                  </Grid.Column>
                </Grid>
                {race.length > 0 && (
                  <div className="gpresult-podium">
                    <div>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.podium"} />}
                        type="tertiary"
                      />
                      <Grid centered className="stats-box">
                        <Grid.Column mobile={8} tablet={16} computer={16}>
                          <Segment textAlign="center" basic>
                            <NavLink to={`/driver/${race[0].driverAlias}`}>
                              <Image
                                centered
                                src={winnerPic}
                                alt={race[0].driver}
                                onError={(e) => {
                                  e.target.src = picReplace;
                                }}
                              />
                            </NavLink>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column
                          mobile={8}
                          tablet={16}
                          computer={16}
                          className="info-box"
                        >
                          <Segment padded basic textAlign="center">
                            <Header size="huge" inverted>
                              1
                            </Header>
                            <Header size="large" textAlign="center" inverted>
                              <NavLink to={`/driver/${race[0].driverAlias}`}>
                                <Flag name={race[0].driverCountryCode} />
                                {race[0].driver}
                              </NavLink>
                              <Header.Subheader>
                                {race[0].team}
                              </Header.Subheader>
                              <small>{race[0].time}</small>
                            </Header>
                          </Segment>
                        </Grid.Column>
                      </Grid>
                      <Divider hidden fitted></Divider>
                      <Grid centered className="stats-box">
                        <Grid.Column mobile={8} tablet={16} computer={16}>
                          <Segment textAlign="center" basic>
                            <NavLink to={`/driver/${race[1].driverAlias}`}>
                              <Image
                                centered
                                src={secondPic}
                                alt={race[1].driver}
                                onError={(e) => {
                                  e.target.src = picReplace;
                                }}
                              />
                            </NavLink>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column
                          mobile={8}
                          tablet={16}
                          computer={16}
                          className="info-box"
                        >
                          <Segment padded basic textAlign="center">
                            <Header size="huge" inverted>
                              2
                            </Header>
                            <Header size="large" inverted>
                              <NavLink to={`/driver/${race[1].driverAlias}`}>
                                <Flag name={race[1].driverCountryCode} />
                                {race[1].driver}
                              </NavLink>
                              <Header.Subheader>
                                {race[1].team}
                              </Header.Subheader>
                              <small>{race[1].time}</small>
                            </Header>
                          </Segment>
                        </Grid.Column>
                      </Grid>
                      <Divider hidden fitted></Divider>
                      <Grid centered className="stats-box">
                        <Grid.Column mobile={8} tablet={16} computer={16}>
                          <Segment textAlign="center" basic>
                            <NavLink to={`/driver/${race[2].driverAlias}`}>
                              <Image
                                centered
                                src={thirdPic}
                                alt={race[2].driver}
                                onError={(e) => {
                                  e.target.src = picReplace;
                                }}
                              />
                            </NavLink>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column
                          mobile={8}
                          tablet={16}
                          computer={16}
                          className="info-box"
                        >
                          <Segment padded basic textAlign="center">
                            <Header size="huge" inverted>
                              3
                            </Header>
                            <Header size="large" inverted>
                              <NavLink to={`/driver/${race[2].driverAlias}`}>
                                <Flag name={race[2].driverCountryCode} />
                                {race[2].driver}
                              </NavLink>
                              <Header.Subheader>
                                {race[2].team}
                              </Header.Subheader>
                              <small>{race[2].time}</small>
                            </Header>
                          </Segment>
                        </Grid.Column>
                      </Grid>
                    </div>
                  </div>
                )}
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered>
                  <Grid.Column mobile={16} tablet={11} computer={12}>
                    <Segment basic padded>
                      <div className="buttons">
                        <NavLink
                          to={`/gp-result/${alias}/${year}`}
                          className="primary active"
                        >
                          <FormattedMessage id={"app.button.gpresults"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.season"} />
                        </NavLink>
                        <NavLink
                          to={`/circuit/${circuitAlias}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.circuit.info"} />
                        </NavLink>
                      </div>
                      <div className="gpresult-filters">
                        <Segment basic textAlign="left">
                          <Select
                            ref={(ref) => {
                              this.combboEvents = ref;
                            }}
                            name="events"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={state.selectEvent}
                            onChange={this.onEventChange}
                            options={eventsComboOptions}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={formatMessage({
                              id: "app.placeholder.select.event",
                            })}
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </Segment>
                      </div>
                      {state.selectEvent.value == "race" && (
                        <div className="gpresult-race">
                          <div className="hideForDesktop">
                            <Segment basic>
                              <Grid columns={3} divided="vertically">
                                {this.renderRaceResultMobile()}
                              </Grid>
                            </Segment>
                          </div>
                          <div className="hideForMobile">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.gpresults.race.title"}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic className="overflow">
                              <Table
                                basic="very"
                                celled
                                className="responsive-table center-aligned"
                              >
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>#</Table.HeaderCell>
                                    <Table.HeaderCell>+/-</Table.HeaderCell>
                                    <Table.HeaderCell>#</Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.driver"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.team"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.engine.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.model.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.tires"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.time"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <FormattedMessage
                                        id={"app.table.header.laps"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <FormattedMessage
                                        id={"app.table.header.points"}
                                      />
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                  {this.renderRaceResult()}
                                </Table.Body>
                              </Table>
                            </Segment>
                          </div>
                          <Segment basic>
                            <Message>
                              <Message.Header>
                                <FormattedMessage id={"app.stats.bestlap"} />
                              </Message.Header>
                              <div>
                                {bestLap?.map((bl) => {
                                  return (
                                    <p key={bl.driver} className="block">
                                      {bl.driver} {bl.team}
                                      {" - "}
                                      {bl.time}
                                    </p>
                                  );
                                })}
                              </div>
                            </Message>
                          </Segment>
                        </div>
                      )}
                      {state.selectEvent.value == "grid" && (
                        <div className="gpresult-grid">
                          <div className="hideForDesktop">
                            <Segment basic>
                              <Grid columns={3} divided="vertically">
                                {this.renderGridMobile()}
                              </Grid>
                            </Segment>
                          </div>
                          <div className="hideForMobile">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.gpresults.grid.title"}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic className="overflow">
                              <Table
                                basic="very"
                                celled
                                className="responsive-table center-aligned"
                              >
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>#</Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.driver"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.team"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.engine.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.model.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.tires"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.info"}
                                      />
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>{this.renderGrid()}</Table.Body>
                              </Table>
                            </Segment>
                          </div>
                          {(year == 2022 || year == 2021) && sprint == "1" && (
                            <Segment basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage
                                    id={"app.message.header.warning"}
                                  />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.gpresults.info2"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                        </div>
                      )}
                      {state.selectEvent.value == "qual" && (
                        <div className="gpresult-qual">
                          <div className="hideForDesktop">
                            <Segment basic>
                              <Grid columns={3} divided="vertically">
                                {this.renderQualResultMobile()}
                              </Grid>
                            </Segment>
                          </div>
                          <div className="hideForMobile">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.gpresults.qual.title"}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic className="overflow">
                              <Table
                                basic="very"
                                celled
                                className="responsive-table center-aligned"
                              >
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>#</Table.HeaderCell>
                                    {year >= 2006 && (
                                      <Table.HeaderCell>Q</Table.HeaderCell>
                                    )}
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.driver"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.team"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.engine.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.model.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.tires"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.time"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.info"}
                                      />
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                  {this.renderQualResult()}
                                </Table.Body>
                              </Table>
                            </Segment>
                          </div>
                        </div>
                      )}
                      {state.selectEvent.value == "sprint" &&
                        sprint === "1" && (
                          <div className="gpresult-sprint">
                            <div className="hideForDesktop">
                              <Segment basic>
                                <Grid columns={3} divided="vertically">
                                  {this.renderSprintResultMobile()}
                                </Grid>
                              </Segment>
                            </div>
                            <div className="hideForMobile">
                              <SectionPageHeader
                                title={
                                  <FormattedMessage
                                    id={"app.page.gpresults.sprint.title"}
                                  />
                                }
                                type="secondary"
                              />
                              <Segment basic className="overflow">
                                <Table
                                  basic="very"
                                  celled
                                  className="responsive-table center-aligned"
                                >
                                  <Table.Header>
                                    <Table.Row>
                                      <Table.HeaderCell>#</Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.driver"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.team"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.engine.long"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.model.long"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.tires"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell className="left">
                                        <FormattedMessage
                                          id={"app.table.header.time"}
                                        />
                                      </Table.HeaderCell>
                                      <Table.HeaderCell>
                                        <FormattedMessage
                                          id={"app.table.header.points"}
                                        />
                                      </Table.HeaderCell>
                                    </Table.Row>
                                  </Table.Header>
                                  <Table.Body>
                                    {this.renderSprintResult()}
                                  </Table.Body>
                                </Table>
                              </Segment>
                            </div>
                          </div>
                        )}
                      {state.selectEvent.value == "entrants" && (
                        <div className="gpresult-entrants">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.gpresults.entrants.title"}
                              />
                            }
                            type="tertiary"
                          />
                          <Grid centered>
                            <GridRow columns={2} only="mobile">
                              {this.renderEntrants()}
                            </GridRow>
                            <GridRow columns={3} only="tablet">
                              {this.renderEntrants()}
                            </GridRow>
                            <GridRow columns={6} only="computer">
                              {this.renderEntrants()}
                            </GridRow>
                          </Grid>
                        </div>
                      )}
                    </Segment>
                  </Grid.Column>
                  <Grid.Column
                    mobile={16}
                    tablet={5}
                    computer={4}
                    className="right-sidebar"
                  >
                    <div className="gpresult-points">
                      <Grid stackable centered columns="equal" padded>
                        <Grid.Row columns={1}>
                          <Grid.Column className="drivers-container">
                            <Segment basic className="drivers-list">
                              <SectionPageHeader
                                title={
                                  <FormattedMessage
                                    id={
                                      "app.page.gpresults.stats.drivers.title"
                                    }
                                  />
                                }
                              />
                              <div className="hideForDesktop info-box">
                                <Segment basic>
                                  <Grid columns={3} divided="vertically">
                                    {this.renderDriversPointsMobile()}
                                  </Grid>
                                </Segment>
                              </div>
                              <div className="hideForMobile info-box">
                                <Item.Group divided unstackable>
                                  {this.renderDriversPoints()}
                                </Item.Group>
                              </div>
                            </Segment>
                          </Grid.Column>
                          <Grid.Column className="teams-container">
                            <Segment basic className="teams-list">
                              <SectionPageHeader
                                title={
                                  <FormattedMessage
                                    id={"app.page.gpresults.stats.teams.title"}
                                  />
                                }
                              />
                              {year >= 1958 && (
                                <>
                                  <div className="hideForDesktop info-box">
                                    <Segment basic>
                                      <Grid columns={3} divided="vertically">
                                        {this.renderTeamsPointsMobile()}
                                      </Grid>
                                    </Segment>
                                  </div>
                                  <div className="hideForMobile info-box">
                                    <Item.Group divided unstackable>
                                      {this.renderTeamsPoints()}
                                    </Item.Group>
                                  </div>
                                </>
                              )}
                              {year < 1958 && (
                                <Message icon className="inverted">
                                  <Icon name="warning circle" />
                                  <Message.Content>
                                    <Message.Header>
                                      <FormattedMessage
                                        id={"app.message.header"}
                                      />
                                    </Message.Header>
                                    <p>
                                      <FormattedMessage
                                        id={"app.page.gpresults.season.info"}
                                        values={{ season: year }}
                                      />
                                    </p>
                                  </Message.Content>
                                </Message>
                              )}
                            </Segment>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                      <Segment basic padded>
                        <Message className="inverted">
                          <Message.Header>
                            <FormattedMessage
                              id={"app.message.header.warning"}
                            />{" "}
                          </Message.Header>
                          <p>
                            <FormattedMessage
                              id={"app.page.gpresults.season.info2"}
                            />
                          </p>
                        </Message>
                      </Segment>
                      <Regulations
                        regulations={regulations}
                        inverted
                      ></Regulations>
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

GPResult.propTypes = {
  fetchGPResult: PropTypes.func.isRequired,
  gpresult: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  gpId: PropTypes.string,
  year: PropTypes.string,
};

function mapStateToProps({ gpresult }) {
  return { gpresult };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGPResult }, dispatch);
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPResult)
);
