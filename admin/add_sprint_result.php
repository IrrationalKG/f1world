<?
include("dbinfo.inc.php");
mysql_connect(localhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query="SELECT * FROM drivers where show_driver=1 order by surname";
$result_d=mysql_query($query);
$num_d=mysql_numrows($result_d);

$query="SELECT * FROM teams where show_team=1 order by name";
$result_t=mysql_query($query);
$num_t=mysql_numrows($result_t);

$query="SELECT * FROM gp order by name";
$result_c=mysql_query($query);
$num_c=mysql_numrows($result_c);

mysql_close();

?>
<html>
<head>
<title>Sprint Result</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<form method="post" action="insert_sprint_result.php">
  <p>Add Sprint Result<br>
    GP:
    <select name="id_gp">
	<?
	$i=0;
	while ($i < $num_c) {
		$id_gp=mysql_result($result_c,$i,"id_gp");
		$name=mysql_result($result_c,$i,"name");
		$circuit=mysql_result($result_c,$i,"circuit");
		?>
		<option value="<? echo "$id_gp"; ?>"><? echo "$name"; ?>&nbsp;(<? echo "$circuit"; ?>)</option>
		<?
		++$i;
	}
	?>
    </select>
    <br>
    Sprint Date (yyyy-mm-dd):
    <input type="text" name="sprint_date" maxlength="10" size="10">
    <br>
    Driver:
    <select name="id_driver">
	<?
	$i=0;
	while ($i < $num_d) {
		$id_driver=mysql_result($result_d,$i,"id_driver");
		$name=mysql_result($result_d,$i,"name");
		$surname=mysql_result($result_d,$i,"surname");
		?>
		<option value="<? echo "$id_driver"; ?>"><? echo "$surname"; ?>&nbsp;<? echo "$name"; ?></option>
		<?
		++$i;
	}
	?>
    </select>
    <br>
    Team:
    <select name="id_team">
	<?
	$i=0;
	while ($i < $num_t) {
		$id_team=mysql_result($result_t,$i,"id_team");
		$name=mysql_result($result_t,$i,"name");
		$engine=mysql_result($result_t,$i,"engine");
		?>
		<option value="<? echo "$id_team"; ?>"><? echo "$name $engine"; ?></option>
		<?
		++$i;
	}
	?>

    </select>
    <br>
    Sprint Position:
    <input type="text" name="sprint_pos" maxlength="2" size="2">
    <br>
    Sprint Points:
    <input type="text" name="sprint_points" maxlength="4" size="4">
    <br>
    Sprint Time:
    <input type="text" name="sprint_time">
    <br>
    Sprint completed:
    <select name="sprint_completed">
      <option value="0">false</option>
      <option value="1">true</option>
    </select>
    <br>
    Sprint additional info: <br>
	<select name="sprint_add_info" multiple>
		<option value=""></option>
		<option value="wypadnięcie z toru">wypadnięcie z toru</option>
		<option value="kolizja">kolizja</option>
		<option value="aw. silnika">aw. silnika</option>
    	<option value="aw. skrzyni biegów">aw. skrzyni biegów</option>
    	<option value="aw. koła">aw. koła</option>
		<option value="aw. opony">aw. opony</option>
		<option value="aw. elektryki">aw. elektryki</option>
		<option value="aw. elektroniki">aw. elektroniki</option>
		<option value="aw. hydrauliki">aw. hydrauliki</option>
		<option value="aw. sprzęgła">aw. sprzęgła</option>
		<option value="aw. hamulców">aw. hamulców</option>
		<option value="aw. zawieszenia">aw. zawieszenia</option>
		<option value="aw. układu napędowego">aw. układu napędowego</option>
		<option value="aw. układu paliwowego">aw. układu paliwowego</option>
		<option value="aw. układu kierowniczego">aw. układu kierowniczego</option>
		<option value="aw. układu wydechowego">aw. układu wydechowego</option>
		<option value="aw. spojlera">aw. spojlera</option>
		<option value="aw. mechaniki">aw. mechaniki</option>
		<option value="aw. turbo">aw. turbo</option>
		<option value="aw. wtrysku">aw. wtrysku</option>
		<option value="aw. bolidu">aw. bolidu</option>
		<option value="aw. alternatora">aw. alternatora</option>
		<option value="aw. akumulatora">aw. akumulatora</option>
		<option value="aw. dyferencjału">aw. dyferencjału</option>
		<option value="aw. zapłonu">aw. zapłonu</option>
		<option value="aw. radiatora">aw. radiatora</option>
		<option value="aw. przepustnicy">aw. przepustnicy</option>
		<option value="aw. pompy paliwowej">aw. pompy paliwowej</option>
		<option value="aw. pompy wodnej">aw. pompy wodnej</option>
		<option value="pożar bolidu">pożar bolidu</option>
		<option value="wyciek wody">wyciek wody</option>
		<option value="wyciek paliwa">wyciek paliwa</option>
		<option value="wyciek oleju">wyciek oleju</option>
		<option value="brak paliwa">brak paliwa</option>
		<option value="utrata mocy">utrata mocy</option>
		<option value="ciśnienie oleju">ciśnienie oleju</option>
		<option value="przegrzanie bolidu">przegrzanie bolidu</option>
		<option value="aw. nadwozia">aw. nadwozia</option>
		<option value="złe samopoczucie kierowcy">złe samopoczucie kierowcy</option>
		<option value="wypadek śmiertelny">wypadek śmiertelny</option>
		<option value="wycofany">wycofany</option>
		<option value="niesklasyfikowany">niesklasyfikowany</option>
    	<option value="dyskwalifikacja">dyskwalifikacja</option>
	</select>
    <br>
	Sprint additional info EN: <br>
	<select name="sprint_add_info_en" multiple>
		<option value=""></option>
		<option value="accident">accident</option>
		<option value="collision">collision</option>
		<option value="engine">engine</option>
    	<option value="gearbox">gearbox</option>
    	<option value="wheel">wheel</option>
		<option value="puncture">puncture</option>
		<option value="electrics">electrics</option>
		<option value="electronics">electronics</option>
		<option value="hydraulics">hydraulics</option>
		<option value="clutch">clutch</option>
		<option value="brakes">brakes</option>
		<option value="suspension">suspension</option>
		<option value="transmission">transmission</option>
		<option value="fuel system">fuel system</option>
		<option value="steering">steering</option>
		<option value="exhaust">exhaust</option>
		<option value="spoiler">spoiler</option>
		<option value="mechanics">mechanics</option>
		<option value="turbo">turbo</option>
		<option value="fuel injection">fuel injection</option>
		<option value="car failure">car failure</option>
		<option value="alternator">alternator</option>
		<option value="battery">battery</option>
		<option value="diferrential">diferrential</option>
		<option value="ignition">ignition</option>
		<option value="radiator">radiator</option>
		<option value="throttle">throttle</option>
		<option value="fuel pump">fuel pump</option>
		<option value="water pump">water pump</option>
		<option value="car fire">car fire</option>
		<option value="water leak">water leak</option>
		<option value="fuel leak">fuel leak</option>
		<option value="oil leak">oil leak</option>
		<option value="out of fuel">out of fuel</option>
		<option value="power loss">power loss</option>
		<option value="oil pressure">oil pressure</option>
		<option value="overheating">overheating</option>
		<option value="chassis">chassis</option>
		<option value="driver ill">driver ill</option>
		<option value="fatal accident">fatal accident</option>
		<option value="withdrawn">withdrawn</option>
		<option value="unclassified">unclassified</option>
    	<option value="disqualification">disqualification</option>
	</select>
	<br>
    <input type="submit" name="Submit" value="Submit">
  </p>
  </form>
</body>
</html>
