// PHP
import axios from "axios";
import { ROOT_URL } from '../config/config';

export const FETCH_DRIVER_TEAMMATES_REQUEST = "FETCH_DRIVER_TEAMMATES_REQUEST";
export const FETCH_DRIVER_TEAMMATES_SUCCESS = "FETCH_DRIVER_TEAMMATES_SUCCESS";
export const FETCH_DRIVER_TEAMMATES_ERROR = "FETCH_DRIVER_TEAMMATES_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVER_TEAMMATES_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVER_TEAMMATES_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVER_TEAMMATES_ERROR,
    payload: error,
  };
}

export function fetchDriverTeammates(id) {
  const url = `${ROOT_URL}/get_driver_teammates.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
