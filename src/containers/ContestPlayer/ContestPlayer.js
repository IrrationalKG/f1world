/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderAwards"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Segment,
  Item,
  Label,
  Statistic,
  Table,
  Flag,
  Popup,
  Icon,
  Message,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchContestPlayer } from "../../actions/ContestPlayerActions";
import { fetchComboContestPlayerSeasons } from "../../actions/ComboContestPlayerSeasonsActions";
import { fetchContestAwards } from "../../actions/ContestAwardsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ContestPlayer.less";

class ContestPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectedYear: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchContestAwards();
    props.fetchComboContestPlayerSeasons(props.match.params.playerId);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchContestPlayer(
      props.match.params.playerId,
      props.match.params.year,
      Cookies.get("lang")
    );
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { playerId, year } = props.match.params;
    const nextPlayerId = nextProps.match.params.playerId;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectedYear: {
        value: nextYear,
        label: nextProps.comboContestPlayerSeasons.data?.find(
          (e) => e.value == nextYear
        )?.label,
      },
    });

    if (nextProps.contestPlayer.data != null) {
      this.setState({
        selectedYear: {
          value: nextProps.contestPlayer.data.season,
          label: nextProps.comboContestPlayerSeasons.data?.find(
            (e) => e.value == nextProps.contestPlayer.data.season
          )?.label,
        },
      });
    }

    if (playerId !== nextPlayerId || year !== nextYear) {
      props.fetchContestPlayer(nextPlayerId, nextYear, Cookies.get("lang"));
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({
      pathname: `/contest/player/${props.match.params.playerId}/${event.value}`,
    });
    this.setState({
      selectedYear: event,
    });
  }

  renderGPRates() {
    const elements = [];
    const { props } = this;
    const { season, rates } = props.contestPlayer.data;
    let cnt = 0;
    rates.forEach((item) => {
      cnt += 1;

      let ppEmpty = true;
      let m1Empty = true;
      let m2Empty = true;
      let m3Empty = true;
      let m4Empty = true;
      let m5Empty = true;
      let m6Empty = true;
      let m7Empty = true;
      let m8Empty = true;
      let m9Empty = true;
      let m10Empty = true;

      let ppColor = "";
      let m1Color = "";
      let m2Color = "";
      let m3Color = "";
      let m4Color = "";
      let m5Color = "";
      let m6Color = "";
      let m7Color = "";
      let m8Color = "";
      let m9Color = "";
      let m10Color = "";

      if (item.result !== null) {
        if (item.pp === item.result.pp) {
          ppColor = "red";
          ppEmpty = false;
        } else {
          ppColor = "";
          ppEmpty = true;
        }

        if (item.m1 === item.result.m1) {
          m1Color = "red";
          m1Empty = false;
        } else if (
          item.m1 === item.result.m2 ||
          item.m1 === item.result.m3 ||
          item.m1 === item.result.m4 ||
          item.m1 === item.result.m5 ||
          item.m1 === item.result.m6 ||
          item.m1 === item.result.m7 ||
          item.m1 === item.result.m8 ||
          item.m1 === item.result.m9 ||
          item.m1 === item.result.m10
        ) {
          m1Color = "grey";
          m1Empty = false;
        }

        if (item.m2 === item.result.m2) {
          m2Color = "red";
          m2Empty = false;
        } else if (
          item.m2 === item.result.m1 ||
          item.m2 === item.result.m3 ||
          item.m2 === item.result.m4 ||
          item.m2 === item.result.m5 ||
          item.m2 === item.result.m6 ||
          item.m2 === item.result.m7 ||
          item.m2 === item.result.m8 ||
          item.m2 === item.result.m9 ||
          item.m2 === item.result.m10
        ) {
          m2Color = "grey";
          m2Empty = false;
        }

        if (item.m3 === item.result.m3) {
          m3Color = "red";
          m3Empty = false;
        } else if (
          item.m3 === item.result.m1 ||
          item.m3 === item.result.m2 ||
          item.m3 === item.result.m4 ||
          item.m3 === item.result.m5 ||
          item.m3 === item.result.m6 ||
          item.m3 === item.result.m7 ||
          item.m3 === item.result.m8 ||
          item.m3 === item.result.m9 ||
          item.m3 === item.result.m10
        ) {
          m3Color = "grey";
          m3Empty = false;
        }

        if (item.m4 === item.result.m4) {
          m4Color = "red";
          m4Empty = false;
        } else if (
          item.m4 === item.result.m1 ||
          item.m4 === item.result.m2 ||
          item.m4 === item.result.m3 ||
          item.m4 === item.result.m5 ||
          item.m4 === item.result.m6 ||
          item.m4 === item.result.m7 ||
          item.m4 === item.result.m8 ||
          item.m4 === item.result.m9 ||
          item.m4 === item.result.m10
        ) {
          m4Color = "grey";
          m4Empty = false;
        }

        if (item.m5 === item.result.m5) {
          m5Color = "red";
          m5Empty = false;
        } else if (
          item.m5 === item.result.m1 ||
          item.m5 === item.result.m2 ||
          item.m5 === item.result.m3 ||
          item.m5 === item.result.m4 ||
          item.m5 === item.result.m6 ||
          item.m5 === item.result.m7 ||
          item.m5 === item.result.m8 ||
          item.m5 === item.result.m9 ||
          item.m5 === item.result.m10
        ) {
          m5Color = "grey";
          m5Empty = false;
        }

        if (item.m6 === item.result.m6) {
          m6Color = "red";
          m6Empty = false;
        } else if (
          item.m6 === item.result.m1 ||
          item.m6 === item.result.m2 ||
          item.m6 === item.result.m3 ||
          item.m6 === item.result.m4 ||
          item.m6 === item.result.m5 ||
          item.m6 === item.result.m7 ||
          item.m6 === item.result.m8 ||
          item.m6 === item.result.m9 ||
          item.m6 === item.result.m10
        ) {
          m6Color = "grey";
          m6Empty = false;
        }

        if (item.m7 === item.result.m7) {
          m7Color = "red";
          m7Empty = false;
        } else if (
          item.m7 === item.result.m1 ||
          item.m7 === item.result.m2 ||
          item.m7 === item.result.m3 ||
          item.m7 === item.result.m4 ||
          item.m7 === item.result.m5 ||
          item.m7 === item.result.m6 ||
          item.m7 === item.result.m8 ||
          item.m7 === item.result.m9 ||
          item.m7 === item.result.m10
        ) {
          m7Color = "grey";
          m7Empty = false;
        }

        if (item.m8 === item.result.m8) {
          m8Color = "red";
          m8Empty = false;
        } else if (
          item.m8 === item.result.m1 ||
          item.m8 === item.result.m2 ||
          item.m8 === item.result.m3 ||
          item.m8 === item.result.m4 ||
          item.m8 === item.result.m5 ||
          item.m8 === item.result.m6 ||
          item.m8 === item.result.m7 ||
          item.m8 === item.result.m9 ||
          item.m8 === item.result.m10
        ) {
          m8Color = "grey";
          m8Empty = false;
        }

        if (item.m9 === item.result.m9) {
          m9Color = "red";
          m9Empty = false;
        } else if (
          item.m9 === item.result.m1 ||
          item.m9 === item.result.m2 ||
          item.m9 === item.result.m3 ||
          item.m9 === item.result.m4 ||
          item.m9 === item.result.m5 ||
          item.m9 === item.result.m6 ||
          item.m9 === item.result.m7 ||
          item.m9 === item.result.m8 ||
          item.m9 === item.result.m10
        ) {
          m9Color = "grey";
          m9Empty = false;
        }

        if (item.m10 === item.result.m10) {
          m10Color = "red";
          m10Empty = false;
        } else if (
          item.m10 === item.result.m1 ||
          item.m10 === item.result.m2 ||
          item.m10 === item.result.m3 ||
          item.m10 === item.result.m4 ||
          item.m10 === item.result.m5 ||
          item.m10 === item.result.m6 ||
          item.m10 === item.result.m7 ||
          item.m10 === item.result.m8 ||
          item.m10 === item.result.m9
        ) {
          m10Color = "grey";
          m10Empty = false;
        }
      }

      const element = (
        <Table.Row key={`gpitem-${item.id}`}>
          <Table.Cell data-title="#">{cnt}.</Table.Cell>
          <Table.Cell data-title="Grand Prix" className="left">
            <div className="bold-uppercase">
              <Flag name={item.countryCode} />
              {item.gp}
            </div>
            <p>
              <small>{item.date}</small>
            </p>
          </Table.Cell>
          {(season === "2005" ||
            season === "2006" ||
            season === "2007" ||
            season === "2008" ||
            season === "2009" ||
            season === "2010" ||
            season === "2011" ||
            season === "2012" ||
            season === "2013" ||
            season === "2014" ||
            season === "2015" ||
            season === "2016" ||
            season === "2017" ||
            season === "2018" ||
            season === "2019" ||
            season === "2020" ||
            season === "2021" ||
            season === "2022" ||
            season === "2023" ||
            season === "2024" ||
            season === "2025") && (
            <Table.Cell data-title="Pole Position">
              {item.pp === "" && "-"}
              {ppEmpty && item.pp}
              {!ppEmpty && (
                <Label
                  htmlFor="pp"
                  color={ppColor}
                  key={`gpitem-${item.id}-pp`}
                  size="large"
                >
                  {item.pp}
                </Label>
              )}
            </Table.Cell>
          )}
          <Table.Cell data-title="1.">
            {item.m1 === "" && "-"}
            {m1Empty && item.m1}
            {!m1Empty && (
              <Label
                htmlFor="m1"
                color={m1Color}
                key={`gpitem-${item.id}-m1`}
                size="large"
              >
                {item.m1}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="2.">
            {item.m2 === "" && "-"}
            {m2Empty && item.m2}
            {!m2Empty && (
              <Label
                htmlFor="m2"
                color={m2Color}
                key={`gpitem-${item.id}-m2`}
                size="large"
              >
                {item.m2}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="3.">
            {item.m3 === "" && "-"}
            {m3Empty && item.m3}
            {!m3Empty && (
              <Label
                htmlFor="m3"
                color={m3Color}
                key={`gpitem-${item.id}-m3`}
                size="large"
              >
                {item.m3}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="4.">
            {item.m4 === "" && "-"}
            {m4Empty && item.m4}
            {!m4Empty && (
              <Label
                htmlFor="m4"
                color={m4Color}
                key={`gpitem-${item.id}-m4`}
                size="large"
              >
                {item.m4}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="5.">
            {item.m5 === "" && "-"}
            {m5Empty && item.m5}
            {!m5Empty && (
              <Label
                htmlFor="m5"
                color={m5Color}
                key={`gpitem-${item.id}-m5`}
                size="large"
              >
                {item.m5}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="6.">
            {item.m6 === "" && "-"}
            {m6Empty && item.m6}
            {!m6Empty && (
              <Label
                htmlFor="m6"
                color={m6Color}
                key={`gpitem-${item.id}-m6`}
                size="large"
              >
                {item.m6}
              </Label>
            )}
          </Table.Cell>
          {(season === "2005" ||
            season === "2006" ||
            season === "2007" ||
            season === "2008" ||
            season === "2009" ||
            season === "2010" ||
            season === "2011" ||
            season === "2012" ||
            season === "2013" ||
            season === "2014" ||
            season === "2015" ||
            season === "2016" ||
            season === "2017" ||
            season === "2018" ||
            season === "2019" ||
            season === "2020" ||
            season === "2021" ||
            season === "2022" ||
            season === "2023" ||
            season === "2024" ||
            season === "2025") && (
            <Table.Cell data-title="7.">
              {item.m7 === "" && "-"}
              {m7Empty && item.m7}
              {!m7Empty && (
                <Label
                  htmlFor="m7"
                  color={m7Color}
                  key={`gpitem-${item.id}-m7`}
                  size="large"
                >
                  {item.m7}
                </Label>
              )}
            </Table.Cell>
          )}
          {(season === "2005" ||
            season === "2006" ||
            season === "2007" ||
            season === "2008" ||
            season === "2009" ||
            season === "2010" ||
            season === "2011" ||
            season === "2012" ||
            season === "2013" ||
            season === "2014" ||
            season === "2015" ||
            season === "2016" ||
            season === "2017" ||
            season === "2018" ||
            season === "2019" ||
            season === "2020" ||
            season === "2021" ||
            season === "2022" ||
            season === "2023" ||
            season === "2024" ||
            season === "2025") && (
            <Table.Cell data-title="8.">
              {item.m8 === "" && "-"}
              {m8Empty && item.m8}
              {!m8Empty && (
                <Label
                  htmlFor="m8"
                  color={m8Color}
                  key={`gpitem-${item.id}-m8`}
                  size="large"
                >
                  {item.m8}
                </Label>
              )}
            </Table.Cell>
          )}
          {(season === "2010" ||
            season === "2011" ||
            season === "2012" ||
            season === "2013" ||
            season === "2014" ||
            season === "2015" ||
            season === "2016" ||
            season === "2017" ||
            season === "2018" ||
            season === "2019" ||
            season === "2020" ||
            season === "2021" ||
            season === "2022" ||
            season === "2023" ||
            season === "2024" ||
            season === "2025") && (
            <Table.Cell data-title="9.">
              {item.m9 === "" && "-"}
              {m9Empty && item.m9}
              {!m9Empty && (
                <Label
                  htmlFor="m9"
                  color={m9Color}
                  key={`gpitem-${item.id}-m9`}
                  size="large"
                >
                  {item.m9}
                </Label>
              )}
            </Table.Cell>
          )}
          {(season === "2010" ||
            season === "2011" ||
            season === "2012" ||
            season === "2013" ||
            season === "2014" ||
            season === "2015" ||
            season === "2016" ||
            season === "2017" ||
            season === "2018" ||
            season === "2019" ||
            season === "2020" ||
            season === "2021" ||
            season === "2022" ||
            season === "2023" ||
            season === "2024" ||
            season === "2025") && (
            <Table.Cell data-title="10.">
              {item.m10 === "" && "-"}
              {m10Empty && item.m10}
              {!m10Empty && (
                <Label
                  htmlFor="m10"
                  color={m10Color}
                  key={`gpitem-${item.id}-m10`}
                  size="large"
                >
                  {item.m10}
                </Label>
              )}
            </Table.Cell>
          )}
          <Table.Cell data-title="Punkty">
            {item.points === null ? "-" : item.points}
          </Table.Cell>
          <Table.Cell data-title="Premia">
            {item.bonus === null ? "-" : item.bonus}
          </Table.Cell>
          <Table.Cell data-title="Suma">
            {item.sum === null ? "-" : item.sum}
          </Table.Cell>
          <Table.Cell data-title="Pkt GP">
            {item.pointsGP === null ? "-" : item.pointsGP}
          </Table.Cell>
          <Table.Cell
            data-title="Miejsce"
            className={item.place < 4 ? `cell-${item.place} bold` : "bold"}
          >
            {item.place === null ? "-" : item.place}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderGPRatesMobile() {
    const { props } = this;
    const { season, rates } = props.contestPlayer.data;
    if (rates.length > 0) {
      const classItems = [];
      let place = 0;
      rates.forEach((item) => {
        place += 1;
        let ppEmpty = true;
        let m1Empty = true;
        let m2Empty = true;
        let m3Empty = true;
        let m4Empty = true;
        let m5Empty = true;
        let m6Empty = true;
        let m7Empty = true;
        let m8Empty = true;
        let m9Empty = true;
        let m10Empty = true;

        let ppColor = "empty";
        let m1Color = "empty";
        let m2Color = "empty";
        let m3Color = "empty";
        let m4Color = "empty";
        let m5Color = "empty";
        let m6Color = "empty";
        let m7Color = "empty";
        let m8Color = "empty";
        let m9Color = "empty";
        let m10Color = "empty";

        if (item.result !== null) {
          if (item.pp === item.result.pp) {
            ppColor = "exact";
            ppEmpty = false;
          } else {
            ppColor = "empty";
            ppEmpty = false;
          }

          if (item.m1 === item.result.m1) {
            m1Color = "exact";
            m1Empty = false;
          } else if (
            item.m1 === item.result.m2 ||
            item.m1 === item.result.m3 ||
            item.m1 === item.result.m4 ||
            item.m1 === item.result.m5 ||
            item.m1 === item.result.m6 ||
            item.m1 === item.result.m7 ||
            item.m1 === item.result.m8 ||
            item.m1 === item.result.m9 ||
            item.m1 === item.result.m10
          ) {
            m1Color = "top10";
            m1Empty = false;
          }

          if (item.m2 === item.result.m2) {
            m2Color = "exact";
            m2Empty = false;
          } else if (
            item.m2 === item.result.m1 ||
            item.m2 === item.result.m3 ||
            item.m2 === item.result.m4 ||
            item.m2 === item.result.m5 ||
            item.m2 === item.result.m6 ||
            item.m2 === item.result.m7 ||
            item.m2 === item.result.m8 ||
            item.m2 === item.result.m9 ||
            item.m2 === item.result.m10
          ) {
            m2Color = "top10";
            m2Empty = false;
          }

          if (item.m3 === item.result.m3) {
            m3Color = "exact";
            m3Empty = false;
          } else if (
            item.m3 === item.result.m1 ||
            item.m3 === item.result.m2 ||
            item.m3 === item.result.m4 ||
            item.m3 === item.result.m5 ||
            item.m3 === item.result.m6 ||
            item.m3 === item.result.m7 ||
            item.m3 === item.result.m8 ||
            item.m3 === item.result.m9 ||
            item.m3 === item.result.m10
          ) {
            m3Color = "top10";
            m3Empty = false;
          }

          if (item.m4 === item.result.m4) {
            m4Color = "exact";
            m4Empty = false;
          } else if (
            item.m4 === item.result.m1 ||
            item.m4 === item.result.m2 ||
            item.m4 === item.result.m3 ||
            item.m4 === item.result.m5 ||
            item.m4 === item.result.m6 ||
            item.m4 === item.result.m7 ||
            item.m4 === item.result.m8 ||
            item.m4 === item.result.m9 ||
            item.m4 === item.result.m10
          ) {
            m4Color = "top10";
            m4Empty = false;
          }

          if (item.m5 === item.result.m5) {
            m5Color = "exact";
            m5Empty = false;
          } else if (
            item.m5 === item.result.m1 ||
            item.m5 === item.result.m2 ||
            item.m5 === item.result.m3 ||
            item.m5 === item.result.m4 ||
            item.m5 === item.result.m6 ||
            item.m5 === item.result.m7 ||
            item.m5 === item.result.m8 ||
            item.m5 === item.result.m9 ||
            item.m5 === item.result.m10
          ) {
            m5Color = "top10";
            m5Empty = false;
          }

          if (item.m6 === item.result.m6) {
            m6Color = "exact";
            m6Empty = false;
          } else if (
            item.m6 === item.result.m1 ||
            item.m6 === item.result.m2 ||
            item.m6 === item.result.m3 ||
            item.m6 === item.result.m4 ||
            item.m6 === item.result.m5 ||
            item.m6 === item.result.m7 ||
            item.m6 === item.result.m8 ||
            item.m6 === item.result.m9 ||
            item.m6 === item.result.m10
          ) {
            m6Color = "top10";
            m6Empty = false;
          }

          if (item.m7 === item.result.m7) {
            m7Color = "exact";
            m7Empty = false;
          } else if (
            item.m7 === item.result.m1 ||
            item.m7 === item.result.m2 ||
            item.m7 === item.result.m3 ||
            item.m7 === item.result.m4 ||
            item.m7 === item.result.m5 ||
            item.m7 === item.result.m6 ||
            item.m7 === item.result.m8 ||
            item.m7 === item.result.m9 ||
            item.m7 === item.result.m10
          ) {
            m7Color = "top10";
            m7Empty = false;
          }

          if (item.m8 === item.result.m8) {
            m8Color = "exact";
            m8Empty = false;
          } else if (
            item.m8 === item.result.m1 ||
            item.m8 === item.result.m2 ||
            item.m8 === item.result.m3 ||
            item.m8 === item.result.m4 ||
            item.m8 === item.result.m5 ||
            item.m8 === item.result.m6 ||
            item.m8 === item.result.m7 ||
            item.m8 === item.result.m9 ||
            item.m8 === item.result.m10
          ) {
            m8Color = "top10";
            m8Empty = false;
          }

          if (item.m9 === item.result.m9) {
            m9Color = "exact";
            m9Empty = false;
          } else if (
            item.m9 === item.result.m1 ||
            item.m9 === item.result.m2 ||
            item.m9 === item.result.m3 ||
            item.m9 === item.result.m4 ||
            item.m9 === item.result.m5 ||
            item.m9 === item.result.m6 ||
            item.m9 === item.result.m7 ||
            item.m9 === item.result.m8 ||
            item.m9 === item.result.m10
          ) {
            m9Color = "top10";
            m9Empty = false;
          }

          if (item.m10 === item.result.m10) {
            m10Color = "exact";
            m10Empty = false;
          } else if (
            item.m10 === item.result.m1 ||
            item.m10 === item.result.m2 ||
            item.m10 === item.result.m3 ||
            item.m10 === item.result.m4 ||
            item.m10 === item.result.m5 ||
            item.m10 === item.result.m6 ||
            item.m10 === item.result.m7 ||
            item.m10 === item.result.m8 ||
            item.m10 === item.result.m9
          ) {
            m10Color = "top10";
            m10Empty = false;
          }
        }
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <Flag name={item.countryCode} />
                {item.gp}
                <p>
                  <small>{item.date}</small>
                </p>
              </Item.Header>
              <Item.Meta>
                {"Punkty: "}
                {item.points}
                {" Premia: "}
                {item.bonus}
                {" Suma: "}
                {item.sum}
                {" Punkty GP: "}
                {item.pointsGP}
              </Item.Meta>
              <Item.Extra>
                {(season === "2005" ||
                  season === "2006" ||
                  season === "2007" ||
                  season === "2008" ||
                  season === "2009" ||
                  season === "2010" ||
                  season === "2011" ||
                  season === "2012" ||
                  season === "2013" ||
                  season === "2014" ||
                  season === "2015" ||
                  season === "2016" ||
                  season === "2017" ||
                  season === "2018" ||
                  season === "2019" ||
                  season === "2020" ||
                  season === "2021" ||
                  season === "2022" ||
                  season === "2023" ||
                  season === "2024" ||
                  season === "2025") && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${ppColor}`}>
                      <FormattedMessage id={"app.table.header.polepos"} />
                    </div>
                    <div className="key-value-box-value">
                      {item.pp === "" && "-"}
                      {ppEmpty && item.pp}
                      {!ppEmpty && item.pp}
                    </div>
                  </span>
                )}
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m1Color}`}>
                    <FormattedMessage id={"app.table.header.p1"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m1 === "" && "-"}
                    {m1Empty && item.m1}
                    {!m1Empty && item.m1}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m2Color}`}>
                    <FormattedMessage id={"app.table.header.p2"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m2 === "" && "-"}
                    {m2Empty && item.m2}
                    {!m2Empty && item.m2}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m3Color}`}>
                    <FormattedMessage id={"app.table.header.p3"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m3 === "" && "-"}
                    {m3Empty && item.m3}
                    {!m3Empty && item.m3}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m4Color}`}>P4</div>
                  <div className="key-value-box-value">
                    {item.m4 === "" && "-"}
                    {m4Empty && item.m4}
                    {!m4Empty && item.m4}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m5Color}`}>P5</div>
                  <div className="key-value-box-value">
                    {item.m5 === "" && "-"}
                    {m5Empty && item.m5}
                    {!m5Empty && item.m5}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m6Color}`}>P6</div>
                  <div className="key-value-box-value">
                    {item.m6 === "" && "-"}
                    {m6Empty && item.m6}
                    {!m6Empty && item.m6}
                  </div>
                </span>
                {(season === "2005" ||
                  season === "2006" ||
                  season === "2007" ||
                  season === "2008" ||
                  season === "2009" ||
                  season === "2010" ||
                  season === "2011" ||
                  season === "2012" ||
                  season === "2013" ||
                  season === "2014" ||
                  season === "2015" ||
                  season === "2016" ||
                  season === "2017" ||
                  season === "2018" ||
                  season === "2019" ||
                  season === "2020" ||
                  season === "2021" ||
                  season === "2022" ||
                  season === "2023" ||
                  season === "2024" ||
                  season === "2025") && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${m7Color}`}>P7</div>
                    <div className="key-value-box-value">
                      {item.m7 === "" && "-"}
                      {m7Empty && item.m7}
                      {!m7Empty && item.m7}
                    </div>
                  </span>
                )}
                {(season === "2005" ||
                  season === "2006" ||
                  season === "2007" ||
                  season === "2008" ||
                  season === "2009" ||
                  season === "2010" ||
                  season === "2011" ||
                  season === "2012" ||
                  season === "2013" ||
                  season === "2014" ||
                  season === "2015" ||
                  season === "2016" ||
                  season === "2017" ||
                  season === "2018" ||
                  season === "2019" ||
                  season === "2020" ||
                  season === "2021" ||
                  season === "2022" ||
                  season === "2023" ||
                  season === "2024" ||
                  season === "2025") && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${m8Color}`}>P8</div>
                    <div className="key-value-box-value">
                      {item.m8 === "" && "-"}
                      {m8Empty && item.m8}
                      {!m8Empty && item.m8}
                    </div>
                  </span>
                )}
                {(season === "2010" ||
                  season === "2011" ||
                  season === "2012" ||
                  season === "2013" ||
                  season === "2014" ||
                  season === "2015" ||
                  season === "2016" ||
                  season === "2017" ||
                  season === "2018" ||
                  season === "2019" ||
                  season === "2020" ||
                  season === "2021" ||
                  season === "2022" ||
                  season === "2023" ||
                  season === "2024" ||
                  season === "2025") && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${m9Color}`}>P9</div>
                    <div className="key-value-box-value">
                      {item.m9 === "" && "-"}
                      {m9Empty && item.m9}
                      {!m9Empty && item.m9}
                    </div>
                  </span>
                )}
                {(season === "2010" ||
                  season === "2011" ||
                  season === "2012" ||
                  season === "2013" ||
                  season === "2014" ||
                  season === "2015" ||
                  season === "2016" ||
                  season === "2017" ||
                  season === "2018" ||
                  season === "2019" ||
                  season === "2020" ||
                  season === "2021" ||
                  season === "2022" ||
                  season === "2023" ||
                  season === "2024" ||
                  season === "2025") && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${m10Color}`}>
                      P10
                    </div>
                    <div className="key-value-box-value">
                      {item.m10 === "" && "-"}
                      {m10Empty && item.m10}
                      {!m10Empty && item.m10}
                    </div>
                  </span>
                )}
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right">
                <Statistic.Value>{item.place}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.place"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderAwards(awardsContent, type) {
    const { formatMessage } = this.props.intl;
    const awardsItems = [];
    if (awardsContent) {
      awardsContent.forEach((award) => {
        let content = `${award.place}. ${formatMessage({
          id: "app.stats.wins.season",
        })} ${award.season}`;
        if (type === "gp") {
          content = `${award.place}. ${formatMessage({
            id: "app.stats.wins.season",
          })} ${award.season} (GP)`;
        }
        let awardColor = "";
        switch (award.place) {
          case "1":
            awardColor = "yellow";
            break;
          case "2":
            awardColor = "grey";
            break;
          case "3":
            awardColor = "brown";
            break;
          default:
            return "";
        }
        let awardItem;
        const key = `${type}_${award.season}_${award.place}`;
        if (award.season === "-1") {
          content = `${award.place}. ${formatMessage({
            id: "app.stats.wins.alltime",
          })}`;
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="trophy" size="big" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        } else {
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="star" size="big" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        }
        awardsItems.push(awardItem);
        return awardsItems;
      });
    }
    return awardsItems;
  }

  renderAwardsNormal(id) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[0];
      const awardsContent = data[id];
      return this.renderAwards(awardsContent, "normal");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderAwardsGP(id) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[1];
      const awardsContent = data[id];
      return this.renderAwards(awardsContent, "gp");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderPlayerCareerMobile() {
    const elements = [];
    const { props } = this;
    const { seasons } = props.contestPlayer.data.career;
    seasons.forEach((item) => {
      const winGPClass = item.placeGP === "1" ? "win" : "";
      const secondGPClass = item.placeGP === "2" ? "second" : "";
      const thirdGPClass = item.placeGP === "3" ? "third" : "";
      const element = (
        <Grid.Row key={item.season}>
          <Grid.Column width={12}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <FormattedMessage id={"app.stats.season"} />{" "}
                    {item.season >= 2004 && (
                      <NavLink to={`/contest/class/${item.season}`}>
                        {item.season}
                      </NavLink>
                    )}
                    {item.season < 2004 && item.season}
                  </Item.Header>
                  <Item.Extra>
                    {item.season >= 2004 && (
                      <>
                        <span className="key-value-box">
                          <div className="key-value-box-header">TYP</div>
                          <div className="key-value-box-value">
                            {item.rounds}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                          <div className="key-value-box-value">{item.wins}</div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.second}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.third}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.podium}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">TOP 10</div>
                          <div className="key-value-box-value">
                            {item.top10}
                          </div>
                        </span>
                      </>
                    )}
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.points"} />
                      </div>
                      <div className="key-value-box-value">{item.points}</div>
                    </span>
                    {item.season >= 2004 && (
                      <>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.points"} />{" "}
                            GP
                          </div>
                          <div className="key-value-box-value">
                            {item.pointsGP !== "0" && item.pointsGP}
                            {item.pointsGP === "0" && "-"}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div
                            className={`key-value-box-header ${winGPClass} ${secondGPClass} ${thirdGPClass}`}
                          >
                            MIEJ. GP
                          </div>
                          <div className="key-value-box-value">
                            {item.placeGP === "1" && item.placeGP}
                            {item.placeGP === "2" && item.placeGP}
                            {item.placeGP === "3" && item.placeGP}
                            {item.placeGP !== "1" &&
                              item.placeGP !== "2" &&
                              item.placeGP !== "3" &&
                              item.pointsGP !== "0" &&
                              item.placeGP}
                            {item.pointsGP === "0" && "-"}
                          </div>
                        </span>
                      </>
                    )}
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={4}>
            <Statistic floated="right">
              <Statistic.Value>
                {item.place === "1" && <div className="win">{item.place}</div>}
                {item.place === "2" && (
                  <div className="second">{item.place}</div>
                )}
                {item.place === "3" && (
                  <div className="third">{item.place}</div>
                )}
                {item.place !== "1" &&
                  item.place !== "2" &&
                  item.place !== "3" &&
                  item.place}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.place"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { contestPlayer } = props;
    const { formatMessage } = this.props.intl;

    if (!contestPlayer.data || contestPlayer.loading) {
      return (
        <section
          id="player-details"
          name="player-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { id, season, name, stats } = contestPlayer.data;
    const {
      seasons,
      classPlaces,
      classPlacesGP,
      roundPoints,
      roundPointsGP,
      roundPlaces,
      seasonsPlaces,
      seasonsPlacesGP,
      seasonsPoints,
      seasonsPointsGP,
    } = contestPlayer.data.career;

    const seasonPlacesData = [];
    seasonsPlaces.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return seasonPlacesData.push(value);
    });

    const seasonPlacesGPData = [];
    seasonsPlacesGP.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return seasonPlacesGPData.push(value);
    });

    const configAllSeasonsPlaces = {
      chart: {
        type: "line",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        reversed: true,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: true,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:10px"><b>{point.key}</b></span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
        // backgroundColor: '#FFFFFF'
      },
      plotOptions: {
        area: {
          fillOpacity: 0.5,
        },
        series: {
          borderWidth: 0,
          label: {
            enabled: false,
          },
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({
            id: "app.stats.class.standard.short",
          }),
          color: "#cbb973",
          data: seasonPlacesData,
        },
        {
          name: formatMessage({
            id: "app.stats.class.gp.short",
          }),
          color: "#b1b1b1",
          data: seasonPlacesGPData,
        },
      ],
    };

    const seasonPointsData = [];
    seasonsPoints.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return seasonPointsData.push(value);
    });

    const seasonPointsGPData = [];
    seasonsPointsGP.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return seasonPointsGPData.push(value);
    });

    const configAllSeasonsPoints = {
      chart: {
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: true,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:10px"><b>{point.key}</b></span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          label: {
            enabled: false,
          },
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({
            id: "app.stats.class.standard.short",
          }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: seasonPointsData,
        },
        {
          name: formatMessage({
            id: "app.stats.class.gp.short",
          }),
          color: "#b1b1b1",
          maxPointWidth: 20,
          data: seasonPointsGPData,
        },
      ],
    };

    const roundPlacesData = [];
    roundPlaces.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return roundPlacesData.push(value);
    });

    const configRoundPlaces = {
      chart: {
        type: "line",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
        labels: {
          skew3d: true,
        },
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        reversed: true,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: true,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:10px"><b>GP {point.key}</b></span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0"></td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          label: {
            enabled: false,
          },
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({
            id: "app.stats.place",
          }),
          color: "#cbb973",
          data: roundPlacesData,
        },
      ],
    };

    const roundPointsData = [];
    roundPoints.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return roundPointsData.push(value);
    });

    const roundPointsGPData = [];
    roundPointsGP.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return roundPointsGPData.push(value);
    });

    const configRoundPoints = {
      chart: {
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: true,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:10px"><b>GP {point.key}</b></span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          label: {
            enabled: false,
          },
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({
            id: "app.stats.class.standard.short",
          }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: roundPointsData,
        },
        {
          name: formatMessage({
            id: "app.stats.class.gp.short",
          }),
          color: "#b1b1b1",
          maxPointWidth: 20,
          data: roundPointsGPData,
        },
      ],
    };

    const classPlacesData = [];
    classPlaces.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return classPlacesData.push(value);
    });

    const classPlacesGPData = [];
    classPlacesGP.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return classPlacesGPData.push(value);
    });

    const configSeasonClassPlaces = {
      chart: {
        type: "line",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        reversed: true,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: true,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:10px"><b>GP {point.key}</b></span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          label: {
            enabled: false,
          },
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({
            id: "app.stats.class.standard.short",
          }),
          color: "#cbb973",
          data: classPlacesData,
        },
        {
          name: formatMessage({
            id: "app.stats.class.gp.short",
          }),
          color: "#b1b1b1",
          data: classPlacesGPData,
        },
      ],
    };

    return (
      <section
        id="player-details"
        name="player-details"
        className="section-page"
      >
        <SectionPageBanner
          title={name}
          subtitle={
            <FormattedMessage id={"app.page.contest.player.subtitle"} />
          }
        />
        <div className="section-page-content player-details-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <div className="info-box">
                  <SectionPageHeader
                    title={<FormattedMessage id={"app.stats.class"} />}
                    type="tertiary"
                  />
                  <Segment padded basic textAlign="center">
                    <Statistic size="large" inverted>
                      <Statistic.Value>
                        {stats.allTimePlace}
                        {"./"}
                        {stats.allTimePoints}{" "}
                        <small className="very">
                          <FormattedMessage id={"app.stats.pts"} />
                        </small>
                      </Statistic.Value>
                      <Statistic.Label>
                        <FormattedMessage id={"app.stats.class.alltime"} />
                      </Statistic.Label>
                    </Statistic>
                    <Segment padded basic textAlign="center">
                      <Statistic inverted>
                        <Statistic.Value>
                          {stats.currentSeasonPlace}
                          {"./"}
                          {stats.currentSeasonPoints}{" "}
                          <small className="very">
                            <FormattedMessage id={"app.stats.pts"} />
                          </small>
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.class.standard"} />{" "}
                          {season}
                        </Statistic.Label>
                      </Statistic>
                      <br />
                      <Statistic inverted>
                        <Statistic.Value>
                          {stats.currentSeasonPlaceGP}
                          {"./"}
                          {stats.currentSeasonPointsGP}{" "}
                          <small className="very">
                            <FormattedMessage id={"app.stats.pts"} />
                          </small>
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.class.gp"} />{" "}
                          {season}
                        </Statistic.Label>
                      </Statistic>
                    </Segment>
                    <Segment padded basic textAlign="center">
                      {this.renderAwardsNormal(id)}
                      {this.renderAwardsGP(id)}
                    </Segment>
                  </Segment>
                  <SectionPageHeader
                    title={<FormattedMessage id={"app.stats.statistics"} />}
                    type="tertiary"
                  />
                  <Segment padded basic>
                    <Item.Group divided>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              {stats.seasons}
                              {" ("}
                              {stats.seasonsNo}
                              {")"}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.wins"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.standard.short"}
                              />
                              {": "}
                              {stats.seasonsWins}
                            </span>
                          </Item.Meta>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.gp.short"}
                              />
                              {": "}
                              {stats.seasonsWinsGP}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.2nd"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.standard.short"}
                              />
                              {": "}
                              {stats.seasonsSecond}
                            </span>
                          </Item.Meta>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.gp.short"}
                              />
                              {": "}
                              {stats.seasonsSecondGP}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.3rd"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.standard.short"}
                              />
                              {": "}
                              {stats.seasonsThird}
                            </span>
                          </Item.Meta>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.gp.short"}
                              />
                              {": "}
                              {stats.seasonsThirdGP}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.podium"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.standard.short"}
                              />
                              {": "}
                              {stats.seasonsPodium}
                            </span>
                          </Item.Meta>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.gp.short"}
                              />
                              {": "}
                              {stats.seasonsPodiumGP}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.top10"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.standard.short"}
                              />
                              {": "}
                              {stats.seasonsTop10}
                            </span>
                          </Item.Meta>
                          <Item.Meta>
                            <span>
                              <FormattedMessage
                                id={"app.stats.class.gp.short"}
                              />
                              {": "}
                              {stats.seasonsTop10GP}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                    </Item.Group>
                  </Segment>
                </div>
                <Segment textAlign="right" basic size="mini">
                  ID: {id}
                </Segment>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered>
                  <Grid.Column mobile={16} tablet={11} computer={12}>
                    <Segment basic padded>
                      <div className="player-stats-content">
                        <Segment basic>
                          <Grid stackable>
                            <Grid.Row textAlign="center" columns={5}>
                              <Grid.Column>
                                <Statistic size="large">
                                  <Statistic.Value>
                                    {stats.wins}
                                  </Statistic.Value>
                                  <Statistic.Label>
                                    <FormattedMessage id={"app.stats.wins"} />
                                  </Statistic.Label>
                                </Statistic>
                              </Grid.Column>
                              <Grid.Column>
                                <Statistic size="large">
                                  <Statistic.Value>
                                    {stats.second}
                                  </Statistic.Value>
                                  <Statistic.Label>
                                    <FormattedMessage id={"app.stats.2nd"} />
                                  </Statistic.Label>
                                </Statistic>
                              </Grid.Column>
                              <Grid.Column>
                                <Statistic size="large">
                                  <Statistic.Value>
                                    {stats.third}
                                  </Statistic.Value>
                                  <Statistic.Label>
                                    <FormattedMessage id={"app.stats.3rd"} />
                                  </Statistic.Label>
                                </Statistic>
                              </Grid.Column>
                              <Grid.Column>
                                <Statistic size="large">
                                  <Statistic.Value>
                                    {stats.podium}
                                  </Statistic.Value>
                                  <Statistic.Label>
                                    <FormattedMessage id={"app.stats.podium"} />
                                  </Statistic.Label>
                                </Statistic>
                              </Grid.Column>
                              <Grid.Column>
                                <Statistic size="large">
                                  <Statistic.Value>
                                    {stats.top10}
                                  </Statistic.Value>
                                  <Statistic.Label>
                                    <FormattedMessage id={"app.stats.top10"} />
                                  </Statistic.Label>
                                </Statistic>
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </Segment>
                      </div>
                      <div className="player-seasons-content">
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.contest.player.subtitle"}
                            />
                          }
                          type="secondary"
                        />
                        <div className="hideForDesktop">
                          <Segment basic>
                            <Grid columns={2} divided="vertically">
                              {this.renderPlayerCareerMobile()}
                            </Grid>
                          </Segment>
                        </div>
                        <div className="hideForMobile">
                          <Segment basic className="overflow">
                            <Table
                              basic="very"
                              celled
                              className="responsive-table center-aligned"
                            >
                              <Table.Header>
                                <Table.Row>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.season"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.rounds"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.wins.long"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.p2"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.p3"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.podiums"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.top10"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.max.points"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.avg.points"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.points.gp"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.place.gp"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.points"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.place"}
                                    />
                                  </Table.HeaderCell>
                                </Table.Row>
                              </Table.Header>
                              <Table.Body>
                                {seasons.map((item) => (
                                  <Table.Row key={item.season}>
                                    <Table.Cell data-title="Sezon">
                                      {item.season >= 2004 && (
                                        <NavLink
                                          to={`/contest/class/${item.season}`}
                                        >
                                          {item.season}
                                        </NavLink>
                                      )}
                                      {item.season < 2004 && item.season}
                                    </Table.Cell>
                                    <Table.Cell data-title="Typowania">
                                      {item.season >= 2004 && item.rounds}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Wygrane">
                                      {item.season >= 2004 && item.wins}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Drugie miejsca">
                                      {item.season >= 2004 && item.second}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Trzecie miejsca">
                                      {item.season >= 2004 && item.third}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Podium">
                                      {item.season >= 2004 && item.podium}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Top 10">
                                      {item.season >= 2004 && item.top10}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Maksymalny wynik">
                                      {item.season >= 2004 && item.maxPoints}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Średni wynik">
                                      {item.season >= 2004 && item.avgPoints}
                                      {item.season < 2004 && "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Punkty GP">
                                      {item.season >= 2004 &&
                                        item.pointsGP !== "0" &&
                                        item.pointsGP}
                                      {(item.season < 2004 ||
                                        item.pointsGP === "0") &&
                                        "-"}
                                    </Table.Cell>
                                    <Table.Cell
                                      data-title="Miejsce GP"
                                      className={
                                        item.placeGP < 4
                                          ? `cell-${item.placeGP}`
                                          : ""
                                      }
                                    >
                                      {item.season >= 2004 &&
                                        item.pointsGP !== "0" &&
                                        item.placeGP}
                                      {(item.season < 2004 ||
                                        item.pointsGP === "0") &&
                                        "-"}
                                    </Table.Cell>
                                    <Table.Cell data-title="Punkty">
                                      {item.points}
                                    </Table.Cell>
                                    <Table.Cell
                                      data-title="Miejsce"
                                      className={
                                        item.place < 4
                                          ? `cell-${item.place}`
                                          : ""
                                      }
                                    >
                                      {item.place}
                                    </Table.Cell>
                                  </Table.Row>
                                ))}
                              </Table.Body>
                            </Table>
                          </Segment>
                        </div>
                      </div>
                      <Segment padded basic>
                        <Message>
                          <Message.Header>
                            <FormattedMessage id={"app.message.header"} />
                          </Message.Header>
                          <p>
                            <FormattedMessage
                              id={"app.page.contest.player.info"}
                            />
                          </p>
                        </Message>
                      </Segment>
                      <div className="filters">
                        <Segment basic>
                          <Select
                            ref={(ref) => {
                              this.comboContestPlayerSeasons = ref;
                            }}
                            isLoading={isLoadingExternally}
                            name="seasons"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={state.selectedYear}
                            onChange={this.onInputChange}
                            options={props.comboContestPlayerSeasons.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={formatMessage({
                              id: "app.placeholder.select.season",
                            })}
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </Segment>
                      </div>
                      <div className="gprates">
                        <SectionPageHeader
                          title={`${formatMessage({
                            id: "app.stats.season",
                          })} ${season}`}
                          type="secondary"
                        />
                        <Segment basic className="player-gprates-mobile">
                          <Item.Group divided unstackable>
                            {this.renderGPRatesMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment
                          basic
                          className="player-gprates-desktop overflow"
                        >
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                {(season === "2005" ||
                                  season === "2006" ||
                                  season === "2007" ||
                                  season === "2008" ||
                                  season === "2009" ||
                                  season === "2010" ||
                                  season === "2011" ||
                                  season === "2012" ||
                                  season === "2013" ||
                                  season === "2014" ||
                                  season === "2015" ||
                                  season === "2016" ||
                                  season === "2017" ||
                                  season === "2018" ||
                                  season === "2019" ||
                                  season === "2020" ||
                                  season === "2021" ||
                                  season === "2022" ||
                                  season === "2023" ||
                                  season === "2024" ||
                                  season === "2025") && (
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </Table.HeaderCell>
                                )}
                                <Table.HeaderCell>1</Table.HeaderCell>
                                <Table.HeaderCell>2</Table.HeaderCell>
                                <Table.HeaderCell>3</Table.HeaderCell>
                                <Table.HeaderCell>4</Table.HeaderCell>
                                <Table.HeaderCell>5</Table.HeaderCell>
                                <Table.HeaderCell>6</Table.HeaderCell>
                                {(season === "2005" ||
                                  season === "2006" ||
                                  season === "2007" ||
                                  season === "2008" ||
                                  season === "2009" ||
                                  season === "2010" ||
                                  season === "2011" ||
                                  season === "2012" ||
                                  season === "2013" ||
                                  season === "2014" ||
                                  season === "2015" ||
                                  season === "2016" ||
                                  season === "2017" ||
                                  season === "2018" ||
                                  season === "2019" ||
                                  season === "2020" ||
                                  season === "2021" ||
                                  season === "2022" ||
                                  season === "2023" ||
                                  season === "2024" ||
                                  season === "2025") && (
                                  <Table.HeaderCell>7</Table.HeaderCell>
                                )}
                                {(season === "2005" ||
                                  season === "2006" ||
                                  season === "2007" ||
                                  season === "2008" ||
                                  season === "2009" ||
                                  season === "2010" ||
                                  season === "2011" ||
                                  season === "2012" ||
                                  season === "2013" ||
                                  season === "2014" ||
                                  season === "2015" ||
                                  season === "2016" ||
                                  season === "2017" ||
                                  season === "2018" ||
                                  season === "2019" ||
                                  season === "2020" ||
                                  season === "2021" ||
                                  season === "2022" ||
                                  season === "2023" ||
                                  season === "2024" ||
                                  season === "2025") && (
                                  <Table.HeaderCell>8</Table.HeaderCell>
                                )}
                                {(season === "2010" ||
                                  season === "2011" ||
                                  season === "2012" ||
                                  season === "2013" ||
                                  season === "2014" ||
                                  season === "2015" ||
                                  season === "2016" ||
                                  season === "2017" ||
                                  season === "2018" ||
                                  season === "2019" ||
                                  season === "2020" ||
                                  season === "2021" ||
                                  season === "2022" ||
                                  season === "2023" ||
                                  season === "2024" ||
                                  season === "2025") && (
                                  <Table.HeaderCell>9</Table.HeaderCell>
                                )}
                                {(season === "2010" ||
                                  season === "2011" ||
                                  season === "2012" ||
                                  season === "2013" ||
                                  season === "2014" ||
                                  season === "2015" ||
                                  season === "2016" ||
                                  season === "2017" ||
                                  season === "2018" ||
                                  season === "2019" ||
                                  season === "2020" ||
                                  season === "2021" ||
                                  season === "2022" ||
                                  season === "2023" ||
                                  season === "2024" ||
                                  season === "2025") && (
                                  <Table.HeaderCell>10</Table.HeaderCell>
                                )}
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.points"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.bonus"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.points.gp"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.place"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderGPRates()}</Table.Body>
                          </Table>
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={5} computer={4}>
                    <SectionPageHeader
                      title={<FormattedMessage id={"app.stats.history"} />}
                    />
                    <div className="player-charts-content">
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.places"} />}
                        type="quaternary"
                      />
                      <ReactHighcharts config={configAllSeasonsPlaces} />
                    </div>
                    <div className="player-charts-content">
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.points"} />}
                        type="quaternary"
                      />
                      <ReactHighcharts config={configAllSeasonsPoints} />
                    </div>
                    <SectionPageHeader
                      title={`${formatMessage({
                        id: "app.stats.season",
                      })} ${season}`}
                    />
                    <div className="player-charts-content">
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.class"} />}
                        type="quaternary"
                      />
                      <ReactHighcharts config={configSeasonClassPlaces} />
                    </div>
                    <div className="player-charts-content">
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.places"} />}
                        type="quaternary"
                      />
                      <ReactHighcharts config={configRoundPlaces} />
                    </div>
                    <div className="player-charts-content">
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.points"} />}
                        type="quaternary"
                      />
                      <ReactHighcharts config={configRoundPoints} />
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

ContestPlayer.propTypes = {
  fetchContestAwards: PropTypes.func.isRequired,
  fetchContestPlayer: PropTypes.func.isRequired,
  contestPlayer: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboContestPlayerSeasons: PropTypes.func.isRequired,
  comboContestPlayerSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  params: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  playerId: PropTypes.string,
  history: PropTypes.object,
  awards: PropTypes.object,
};

function mapStateToProps({
  contestPlayer,
  params,
  awards,
  comboContestPlayerSeasons,
}) {
  return {
    contestPlayer,
    params,
    awards,
    comboContestPlayerSeasons,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchContestPlayer,
      fetchContestAwards,
      fetchComboContestPlayerSeasons,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(ContestPlayer)
);
