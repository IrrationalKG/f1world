// const request = require('../json/drivers_compare_teammates.json');
//
// export const FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS = 'FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS';
//
// export function fetchDriversCompareTeammates() {
//   return {
//     type: FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_DRIVERS_COMPARE_TEAMMATES_REQUEST = 'FETCH_DRIVERS_COMPARE_TEAMMATES_REQUEST';
export const FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS = 'FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS';
export const FETCH_DRIVERS_COMPARE_TEAMMATES_ERROR = 'FETCH_DRIVERS_COMPARE_TEAMMATES_ERROR';

function requestData() {
  return {
    type: FETCH_DRIVERS_COMPARE_TEAMMATES_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVERS_COMPARE_TEAMMATES_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVERS_COMPARE_TEAMMATES_ERROR,
    payload: error
  };
}

export function fetchDriversCompareTeammates(season) {
  const url = `${ROOT_URL}/get_drivers_compare_teammates.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url, {
      params: {
        season: season
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
