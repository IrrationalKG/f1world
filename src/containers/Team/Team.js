/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderCountriesFlags"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  GridColumn,
  Loader,
  Segment,
  Item,
  Flag,
  Statistic,
  Table,
  Header,
  Divider,
  Message,
  Label,
  Popup,
  Tab,
  TabPane,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import ChartStatsBar from "../../components/ChartStatsBar/ChartStatsBar";
import { fetchTeam } from "../../actions/TeamActions";
import { fetchComboTeams } from "../../actions/ComboTeamsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Team.less";

class Team extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: {
        value: props.match.params.teamId,
      },
      selectSeason: {
        value: "",
        label: "",
      },
      driversSort: {
        column: "gp",
        direction: null,
      },
      gpSort: {
        column: "gp",
        direction: null,
      },
      circuitSort: {
        column: "gp",
        direction: null,
      },
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSeasonChange = this.onSeasonChange.bind(this);
    this.onDriversSort = this.onDriversSort.bind(this);
    this.onGPSort = this.onGPSort.bind(this);
    this.onCircuitSort = this.onCircuitSort.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchTeam(
      props.match.params.teamId,
      props.match.params.year,
      Cookies.get("lang")
    );
    if (!props.match.params.year) {
      props.fetchComboTeams(
        0,
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        Cookies.get("lang")
      );
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { teamId } = props.match.params;
    const nextTeamId = nextProps.match.params.teamId;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextTeamId,
        label: nextProps.comboTeams.data?.find((e) => e.value == nextTeamId)
          ?.label,
      },
      selectSeason: {
        value: nextProps.team.data?.selectedSeason,
        label: nextProps.team.data?.selectedSeason,
      },
    });

    if (teamId !== nextTeamId) {
      props.fetchTeam(nextTeamId, nextYear, Cookies.get("lang"));
    }
  }

  onSeasonChange(event) {
    const { props } = this;

    this.setState({
      selectSeason: {
        value: event.value,
        label: props.team.data?.seasons
          ?.map((s) => {
            return { value: s.season, label: s.season };
          })
          .find((e) => e.value == event.value)?.label,
      },
    });
    props.fetchTeam(
      props.match.params.teamId,
      props.match.params.year,
      Cookies.get("lang"),
      event.value
    );
  }

  onInputChange(event) {
    const { props } = this;
    if (props.match.params.year) {
      if (event.value) {
        props.history.push({
          pathname: `/team/${props.match.params.year}/${event.value}`,
        });
      } else {
        props.history.push({
          pathname: `/teams-stats-list/${props.match.params.year}`,
        });
      }
    } else {
      if (event.value) {
        props.history.push({ pathname: `/team/${event.value}` });
      } else {
        props.history.push({
          pathname: `/teams-stats-list/a`,
        });
      }
    }
    this.setState({
      selectValue: event,
    });
  }

  onDriversSort(value) {
    let direction = this.state.driversSort.direction;
    if (this.state.driversSort.column == value) {
      direction =
        this.state.driversSort.direction == null
          ? "descending"
          : this.state.driversSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      driversSort: {
        column: value,
        direction: direction,
      },
    });
  }

  onGPSort(value) {
    let direction = this.state.gpSort.direction;
    if (this.state.gpSort.column == value) {
      direction =
        this.state.gpSort.direction == null
          ? "descending"
          : this.state.gpSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      gpSort: {
        column: value,
        direction: direction,
      },
    });
  }

  onCircuitSort(value) {
    let direction = this.state.circuitSort.direction;
    if (this.state.circuitSort.column == value) {
      direction =
        this.state.circuitSort.direction == null
          ? "descending"
          : this.state.circuitSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      circuitSort: {
        column: value,
        direction: direction,
      },
    });
  }

  renderTeamSeasonsSummary() {
    const elements = [];
    const { props } = this;
    const { seasons, alias, drivers, carModels, engines } = props.team.data;
    let sumGP = 0;
    let sumStarts = 0;
    let sumQuals = 0;
    let sumWins = 0;
    let sumSecond = 0;
    let sumThird = 0;
    let sumPodium = 0;
    let sumPolepos = 0;
    let sumBestlaps = 0;
    let sumPointsPlaces = 0;
    let sumCompleted = 0;
    let sumIncomplete = 0;
    let sumDisq = 0;
    let sumPoints = 0;
    let sumPointsClass = 0;
    seasons.forEach((item) => {
      sumGP += parseInt(item.gp, 0);
      sumStarts += parseInt(item.starts, 0);
      sumQuals += parseInt(item.qual, 0);
      sumWins += parseInt(item.wins, 0);
      sumSecond += parseInt(item.second, 0);
      sumThird += parseInt(item.third, 0);
      sumPodium += parseInt(item.podium, 0);
      sumPolepos += parseInt(item.polepos, 0);
      sumBestlaps += parseInt(item.bestlaps, 0);
      sumPointsPlaces += parseInt(item.pointsPlaces, 0);
      sumCompleted += parseInt(item.completed, 0);
      sumIncomplete += parseInt(item.incomplete, 0);
      sumDisq += parseInt(item.disq, 0);
      sumPoints += parseFloat(item.points, 1);
      sumPointsClass += parseFloat(item.pointsClass, 1);
    });

    const element = (
      <Table.Row key="summary" className="summary">
        <Table.Cell data-title="Razem" className="bold">
          <FormattedMessage id={"app.table.header.total"} />
        </Table.Cell>
        <Table.Cell data-title="Silniki">
          <div className="hideMobile hideTablet">
            <Popup
              position="bottom left"
              trigger={
                <NavLink to={`/team-events/gp/${alias}/-/-/-/-/-/-/-/1`}>
                  {engines.length}
                </NavLink>
              }
              flowing
              hoverable
            >
              <Grid className="team-season-summary-grid">
                {engines.map((item, idx) => (
                  <GridColumn
                    key={idx}
                    className="team-season-summary-grid-column"
                  >
                    <Segment basic padded textAlign="center">
                      <NavLink to={`/team-events/gp/${alias}/-/-/-/-/-/-/-/1`}>
                        <Image
                          size="large"
                          src={`/build/images/engines/engine_${item.engine
                            ?.toLowerCase()
                            .replaceAll(".", "_")
                            .replaceAll(" ", "_")}.jpg`}
                          alt={item.engine}
                          centered
                          onError={(e) => {
                            e.target.src =
                              "/build/images/engines/engine_no_profile.jpg";
                          }}
                        />
                      </NavLink>
                      <Header as="h3">{item.engine}</Header>
                      <div>
                        <small>
                          {item.seasonMin == item.seasonMax
                            ? item.seasonMax
                            : `${item.seasonMin} - ${item.seasonMax}`}
                        </small>
                      </div>
                      <div>
                        <Statistic>
                          <Statistic.Value>{item.starts}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.gp"} />
                          </Statistic.Label>
                        </Statistic>
                        <div className="buttons no-padding">
                          <div>
                            <NavLink
                              className="secondary"
                              to={`/team-events/gp/${alias}/-/-/-/-/-/-/-/1`}
                            >
                              <FormattedMessage id={"app.button.history"} />
                            </NavLink>
                          </div>
                        </div>
                      </div>
                    </Segment>
                  </GridColumn>
                ))}
              </Grid>
            </Popup>
          </div>
          <div className="hideDesktop">
            <NavLink to={`/team-events/gp/${alias}/-/-/-/-/-/-/-/1`}>
              {engines.length}
            </NavLink>
          </div>
        </Table.Cell>
        <Table.Cell data-title="Bolidy">
          <div className="hideMobile hideTablet">
            <Popup
              position="bottom left"
              trigger={
                <NavLink to={`/team-models/${alias}`}>
                  {carModels.length}
                </NavLink>
              }
              flowing
              hoverable
            >
              <Grid className="team-season-summary-grid">
                {carModels.map((item, idx) => (
                  <GridColumn
                    key={idx}
                    className="team-season-summary-grid-column"
                  >
                    <Segment basic padded textAlign="center">
                      <NavLink
                        to={`/team-model-events/gp/${item.idTeamModel}/-/-`}
                      >
                        <Image
                          size="large"
                          src={`/build/images/teams/models/team_${item.team.toLowerCase()}_${item.model
                            .toLowerCase()
                            .replace("???", "3")
                            .replace("??", "2")
                            .replace("?", "1")}.jpg`}
                          alt={`${item.name} ${item.model.replaceAll(
                            "???",
                            ""
                          )} ${item.season}`}
                          centered
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_car.jpg";
                          }}
                        />
                      </NavLink>
                      <Header as="h3">
                        {item.engine ? item.engine : item.name}{" "}
                        {item.model?.replace("_", "/").replace("?", "")}
                      </Header>
                      <div>
                        <small>
                          {item.seasonMin == item.seasonMax
                            ? item.seasonMax
                            : `${item.seasonMin} - ${item.seasonMax}`}
                        </small>
                      </div>
                      <div>
                        <Statistic>
                          <Statistic.Value>{item.starts}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.gp"} />
                          </Statistic.Label>
                        </Statistic>
                      </div>
                      <div className="buttons no-padding">
                        <div>
                          <NavLink
                            className="secondary"
                            to={`/team-model-events/gp/${item.idTeamModel}/-/-`}
                          >
                            <FormattedMessage id={"app.button.history"} />
                          </NavLink>
                        </div>
                      </div>
                    </Segment>
                  </GridColumn>
                ))}
              </Grid>
            </Popup>
          </div>
          <div className="hideDesktop">
            <NavLink to={`/team-models/${alias}`}>{carModels.length}</NavLink>
          </div>
        </Table.Cell>
        <Table.Cell data-title="Kierowcy">
          <div className="hideMobile hideTablet">
            <Popup
              position="bottom left"
              trigger={<a href="#team-drivers">{drivers.length}</a>}
              flowing
              hoverable
            >
              <Grid className="team-season-summary-grid">
                {drivers.map((driver, idx) => (
                  <GridColumn
                    key={idx}
                    className="team-season-summary-grid-column"
                  >
                    <Segment basic padded textAlign="center">
                      <NavLink to={`/driver/${driver.alias}`}>
                        <Image
                          size="large"
                          src={`/build/images/drivers/driver_${driver.id}_profile.jpg`}
                          alt={`${driver.name} ${driver.surname}`}
                          centered
                          onError={(e) => {
                            e.target.src =
                              "/build/images/drivers/driver_no_profile.jpg";
                          }}
                        />
                      </NavLink>
                      <Header as="h3">
                        <div className="no-wrap">
                          <Flag name={driver.country} />
                          {driver.name}
                        </div>
                        <div className="no-wrap">{driver.surname}</div>
                      </Header>
                      <div>
                        <small>
                          {driver.seasonMin == driver.seasonMax
                            ? driver.seasonMax
                            : `${driver.seasonMin} - ${driver.seasonMax}`}
                        </small>
                      </div>
                      <Divider hidden fitted></Divider>
                      <div>
                        <Statistic>
                          <Statistic.Value>{driver.seasons}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Statistic.Label>
                        </Statistic>
                      </div>
                      <div className="buttons no-padding">
                        <div>
                          <NavLink
                            className="primary"
                            to={`/driver/${driver.alias}`}
                          >
                            <FormattedMessage id={"app.button.profile"} />
                          </NavLink>
                        </div>
                        <div>
                          <NavLink
                            className="secondary"
                            to={`/team-events/gp/${alias}/${driver.alias}/-/-/-/-/-/-/1/`}
                          >
                            <FormattedMessage id={"app.button.history"} />
                          </NavLink>
                        </div>
                      </div>
                    </Segment>
                  </GridColumn>
                ))}
              </Grid>
            </Popup>
          </div>
          <div className="hideDesktop">
            <a href="#team-drivers">{drivers.length}</a>
          </div>
        </Table.Cell>
        <Table.Cell data-title="Grand Prix">
          {sumGP === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/gp/${alias}`}>
              {sumGP === null ? String.fromCharCode(160) : sumGP}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Kwalifikacje">
          {sumQuals === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/qual/${alias}`}>
              {sumQuals === null ? String.fromCharCode(160) : sumQuals}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Wyścigi">
          {sumStarts === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/starts/${alias}`}>
              {sumStarts === null ? String.fromCharCode(160) : sumStarts}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Wygrane">
          {sumWins === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/wins/${alias}`}>
              {sumWins === null ? String.fromCharCode(160) : sumWins}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Drugie miejsca">
          {sumSecond === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/second/${alias}`}>
              {sumSecond === null ? String.fromCharCode(160) : sumSecond}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Trzecie miejsca">
          {sumThird === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/third/${alias}`}>
              {sumThird === null ? String.fromCharCode(160) : sumThird}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Podium">
          {sumPodium === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/podium/${alias}`}>
              {sumPodium === null ? String.fromCharCode(160) : sumPodium}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Pole Position">
          {sumPolepos === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/polepos/${alias}`}>
              {sumPolepos === null ? String.fromCharCode(160) : sumPolepos}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Naj. okrążenia">
          {sumBestlaps === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/bestlaps/${alias}`}>
              {sumBestlaps === null ? String.fromCharCode(160) : sumBestlaps}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Ukończone wyścigi">
          {sumCompleted === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/completed/${alias}`}>
              {sumCompleted === null ? String.fromCharCode(160) : sumCompleted}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Nieukończone wyścigi">
          {sumIncomplete === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/incomplete/${alias}`}>
              {sumIncomplete === null
                ? String.fromCharCode(160)
                : sumIncomplete}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Punkty">
          {sumPoints === 0 ? (
            0
          ) : (
            <NavLink to={`/team-events/points/${alias}`}>
              {sumPoints === 0
                ? String.fromCharCode(160)
                : Math.round(sumPoints * 100) / 100}
              {sumPoints !== sumPointsClass && (
                <div>
                  <small>
                    {" ("}
                    {sumPointsClass === 0
                      ? String.fromCharCode(160)
                      : Math.round(sumPointsClass * 100) / 100}
                    {")"}
                  </small>
                </div>
              )}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Miejsce"></Table.Cell>
      </Table.Row>
    );
    elements.push(element);

    const elements2 = [];
    seasons.forEach((item, idx) => {
      const element2 = (
        <Table.Row key={idx}>
          <Table.Cell data-title="Sezon" className="bold">
            <div className="hideMobile hideTablet">
              <Popup
                position="right center"
                trigger={
                  <NavLink to={`/gp-season/${item.season}`}>
                    {item.season}{" "}
                  </NavLink>
                }
                flowing
                hoverable
              >
                <div className="team-season-summary-grid-title">
                  <FormattedMessage id={"app.stats.season"} /> {item.season}
                </div>
                <Grid className="team-season-summary-grid">
                  {item.gpResults.map((gp, idx) => (
                    <GridColumn
                      key={idx}
                      className="team-season-summary-grid-column"
                    >
                      <Segment basic padded textAlign="center">
                        <div>
                          <Segment basic>
                            <NavLink
                              to={`/gp-result/${gp.alias}/${item.season}`}
                            >
                              <Image
                                size="large"
                                src={`/build/images/circuits/circuit_${gp.circuitAlias}_profile.jpg`}
                                alt={gp.gp}
                                onError={(e) => {
                                  e.target.src =
                                    "/build/images/circuits/circuit_no_profile.jpg";
                                }}
                              />
                              <Label
                                attached="top left"
                                color="yellow"
                                size="huge"
                              >
                                {idx + 1}
                              </Label>
                              <Label
                                attached="bottom"
                                color="yellow"
                                size="medium"
                              >
                                {gp.raceDate}
                              </Label>
                            </NavLink>
                          </Segment>
                          <Header as="h3">
                            <Flag name={gp.name} /> {gp.gp}
                          </Header>
                          <div>
                            <small>{gp.circuit}</small>
                          </div>
                        </div>
                        <div>
                          <Statistic>
                            <Statistic.Value>
                              {item.season > 1957 &&
                                (gp.raceStarted == 1 ? (
                                  gp.points == gp.classPoints ? (
                                    gp.points
                                  ) : (
                                    `(${gp.points})`
                                  )
                                ) : (
                                  <FormattedMessage id={"app.stats.nq"} />
                                ))}
                              {item.season <= 1957 && "-"}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.points"} />
                            </Statistic.Label>
                          </Statistic>
                        </div>
                        <div className="buttons no-padding">
                          <NavLink
                            className="secondary"
                            to={`/gp-result/${gp.alias}/${item.season}`}
                          >
                            <FormattedMessage id={"app.button.results"} />
                          </NavLink>
                        </div>
                      </Segment>
                    </GridColumn>
                  ))}
                </Grid>
              </Popup>
            </div>
            <div className="hideDesktop">
              <NavLink to={`/gp-season/${item.season}`}>{item.season} </NavLink>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Silniki">
            <div className="hideMobile hideTablet">
              {item.engines.map((item, idx) => (
                <Popup
                  key={idx}
                  position="right center"
                  trigger={
                    <NavLink
                      to={`/team-events/gp/${item.alias}/-/-/-/${item.engine}/-/-/${item.season}/1`}
                    >
                      <Image
                        size="mini"
                        src={`/build/images/engines/engine_${item.engine
                          ?.toLowerCase()
                          .replaceAll(".", "_")
                          .replaceAll(" ", "_")}.jpg`}
                        alt={item.engine}
                        centered
                        onError={(e) => {
                          e.target.src =
                            "/build/images/engines/engine_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                  }
                  flowing
                  hoverable
                >
                  <Grid className="team-season-summary-grid">
                    <GridColumn
                      key={idx}
                      className="team-season-summary-grid-column"
                    >
                      <Segment basic padded textAlign="center">
                        <NavLink
                          to={`/team-events/gp/${item.alias}/-/-/-/${item.engine}/-/-/${item.season}/1`}
                        >
                          <Image
                            size="large"
                            src={`/build/images/engines/engine_${item.engine
                              ?.toLowerCase()
                              .replaceAll(".", "_")
                              .replaceAll(" ", "_")}.jpg`}
                            alt={item.engine}
                            centered
                            onError={(e) => {
                              e.target.src =
                                "/build/images/engines/engine_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                        <Header as="h3">{item.engine}</Header>
                        <div>
                          <small>
                            {item.seasonMin == item.seasonMax
                              ? item.seasonMax
                              : `${item.seasonMin} - ${item.seasonMax}`}
                          </small>
                        </div>
                        <div>
                          <Statistic>
                            <Statistic.Value>{item.starts}</Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.gp"} />
                            </Statistic.Label>
                          </Statistic>
                        </div>
                        <div className="buttons no-padding">
                          <div>
                            <NavLink
                              className="secondary"
                              to={`/team-events/gp/${item.alias}/-/-/-/${item.engine}/-/-/${item.season}/1`}
                            >
                              <FormattedMessage id={"app.button.history"} />
                            </NavLink>
                          </div>
                        </div>
                      </Segment>
                    </GridColumn>
                  </Grid>
                </Popup>
              ))}
            </div>
            <div className="hideDesktop">
              <NavLink
                to={`/team-events/gp/${item.alias}/-/-/-/${item.engine}/-/-/${item.season}/1`}
              >
                {item.engines.length}{" "}
              </NavLink>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Bolidy">
            <div className="hideMobile hideTablet">
              <Popup
                position="right center"
                trigger={
                  <NavLink
                    to={`/team-model-events/gp/-/${item.season}/-/${alias}`}
                  >
                    {item.carModels.length}
                  </NavLink>
                }
                flowing
                hoverable
              >
                <Grid className="team-season-summary-grid">
                  {item.carModels.map((item, idx) => (
                    <GridColumn
                      key={idx}
                      className="team-season-summary-grid-column"
                    >
                      <Segment basic padded textAlign="center">
                        <NavLink
                          to={`/team-model-events/gp/${item.idTeamModel}/${item.season}/-`}
                        >
                          <Image
                            size="large"
                            src={`/build/images/teams/models/team_${item.team.toLowerCase()}_${item.model
                              .toLowerCase()
                              .replace("???", "3")
                              .replace("??", "2")
                              .replace("?", "1")}.jpg`}
                            alt={`${item.name} ${item.model.replaceAll(
                              "???",
                              ""
                            )} ${item.season}`}
                            centered
                            onError={(e) => {
                              e.target.src =
                                "/build/images/teams/team_no_car.jpg";
                            }}
                          />
                        </NavLink>
                        <Header as="h3">
                          {item.engine ? item.engine : item.name}{" "}
                          {item.model?.replace("_", "/").replace("?", "")}
                        </Header>
                        <Statistic>
                          <Statistic.Value>{item.starts}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.gp"} />
                          </Statistic.Label>
                        </Statistic>
                        <div className="buttons no-padding">
                          <div>
                            <NavLink
                              className="secondary"
                              to={`/team-model-events/gp/${item.idTeamModel}/${item.season}/-`}
                            >
                              <FormattedMessage id={"app.button.history"} />
                            </NavLink>
                          </div>
                        </div>
                      </Segment>
                    </GridColumn>
                  ))}
                </Grid>
              </Popup>
            </div>
            <div className="hideDesktop">
              <NavLink to={`/team-model-events/gp/-/${item.season}/-/${alias}`}>
                {item.carModels.length}
              </NavLink>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Kierowcy">
            <div className="hideMobile hideTablet">
              <Popup
                position="right center"
                trigger={<div className="hover">{item.drivers.length}</div>}
                flowing
                hoverable
              >
                <Grid className="team-season-summary-grid">
                  {item.drivers.map((driver, idx) => (
                    <GridColumn
                      key={idx}
                      className="team-season-summary-grid-column"
                    >
                      <Segment basic padded textAlign="center">
                        <NavLink to={`/driver/${driver.alias}`}>
                          <Image
                            size="large"
                            src={`/build/images/drivers/driver_${driver.id}_profile.jpg`}
                            alt={`${driver.name} ${driver.surname}`}
                            centered
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                        <Header as="h3">
                          <div className="no-wrap">
                            <Flag name={driver.countryCode} />
                            {driver.name}
                          </div>
                          <div className="no-wrap">{driver.surname}</div>
                        </Header>
                        <div>
                          <Statistic>
                            <Statistic.Value>
                              {driver.place ? (
                                driver.place == 1 ? (
                                  <FormattedMessage id={"app.stats.wc"} />
                                ) : (
                                  driver.place
                                )
                              ) : (
                                <FormattedMessage id={"app.stats.nc"} />
                              )}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.place"} />
                            </Statistic.Label>
                          </Statistic>
                        </div>
                        <div className="buttons no-padding">
                          <div>
                            <NavLink
                              className="primary"
                              to={`/driver/${driver.alias}`}
                            >
                              <FormattedMessage id={"app.button.profile"} />
                            </NavLink>
                          </div>
                          <div>
                            <NavLink
                              className="secondary"
                              to={`/team-events/gp/${alias}/${driver.alias}/-/-/-/-/-/${item.season}/1/`}
                            >
                              <FormattedMessage id={"app.button.history"} />
                            </NavLink>
                          </div>
                        </div>
                      </Segment>
                    </GridColumn>
                  ))}
                </Grid>
              </Popup>
            </div>
            <div className="hideDesktop">
              <div className="hover">{item.drivers.length}</div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix">
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje">
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścigi">
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Wygrane"
            className={item.wins > 0 ? "cell-1" : ""}
          >
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Drugie miejsca"
            className={item.second > 0 ? "cell-2" : ""}
          >
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Trzecie miejsca"
            className={item.third > 0 ? "cell-3" : ""}
          >
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium">
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Ukończone wyścigi">
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Nieukończone wyścigi">
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty">
            {item.season > 1957 &&
              (item.points === 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                >
                  {item.points === null
                    ? String.fromCharCode(160)
                    : Math.round(item.points * 100) / 100}
                  {item.points !== item.pointsClass && (
                    <div>
                      <small>
                        {" ("}
                        {item.pointsClass === null
                          ? String.fromCharCode(160)
                          : Math.round(item.pointsClass * 100) / 100}
                        {")"}
                      </small>
                    </div>
                  )}
                </NavLink>
              ))}
            {item.season < 1958 && "-"}
          </Table.Cell>
          <Table.Cell
            data-title="Miejsce"
            className={`bold cell-${item.place < 4 && item.place}`}
          >
            <NavLink to={`/classification/teams-points/${item.season}`}>
              {item.season > 1957 &&
                item.place === "1" &&
                item.season < 2025 && <FormattedMessage id={"app.stats.wc"} />}
              {item.season > 1957 &&
                item.place !== "1" &&
                (item.place == "NK" ? (
                  <FormattedMessage id={"app.stats.nc"} />
                ) : (
                  item.place
                ))}
              {item.season == 2025 && item.place === "1" && item.place}
              {item.season < 1958 && "-"}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements2.push(element2);
    });

    elements.push(elements2);
    return elements;
  }

  renderTeamSeasonDetails() {
    const elements = [];
    const { props } = this;
    const { seasonDetails } = props.team.data;
    let sumGP = 0;
    let sumStarts = 0;
    let sumQuals = 0;
    let sumWins = 0;
    let sumSecond = 0;
    let sumThird = 0;
    let sumPodium = 0;
    let sumPolepos = 0;
    let sumBestlaps = 0;
    let sumPointsPlaces = 0;
    let sumCompleted = 0;
    let sumIncomplete = 0;
    let sumDisq = 0;
    let sumPoints = 0;
    let sumPointsClass = 0;

    const { formatMessage } = this.props.intl;

    seasonDetails.forEach((item) => {
      const element = (
        <Segment basic key={item.season}>
          <Segment basic>
            <div className="box-image-name">
              <Statistic>
                <Statistic.Value>
                  <NavLink to={`/classification/teams-points/${item.season}`}>
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place === "1" && (
                        <FormattedMessage id={"app.stats.wc"} />
                      )}
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place != "1" &&
                      item.place != "NK" &&
                      item.place}
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place != "1" &&
                      item.place === "NK" && (
                        <FormattedMessage id={"app.stats.nc"} />
                      )}
                    {item.season == 2025 && item.place === "1" && item.place}
                    {item.season < 1957 && "-"}
                  </NavLink>
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.place"} />
                </Statistic.Label>
              </Statistic>
              <Image
                size="tiny"
                src={`/build/images/teams/team_${item.id.toLowerCase()}_profile_logo.jpg`}
                alt={item.name}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
              <div>
                <div>
                  <Flag name={item.country} />{" "}
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.fullname ? (
                      item.fullname
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
            <Divider></Divider>
            <div className="team-season-car-container">
              {item.carModels.map((e) => {
                return (
                  <div key={e.model} className="team-season-car-model">
                    <div className="box-image-name">
                      {e.engines.split(", ").map((engine, idx) => {
                        return (
                          engine != item.name.trim() && (
                            <Image
                              key={idx}
                              size="tiny"
                              src={`/build/images/engines/engine_${engine
                                ?.toLowerCase()
                                .replaceAll(".", "_")
                                .replaceAll(" ", "_")}.jpg`}
                              alt={engine}
                              onError={(e) => {
                                e.target.src =
                                  "/build/images/teams/team_no_profile.jpg";
                              }}
                            />
                          )
                        );
                      })}
                      <div>
                        <div className="bold-uppercase">{e.engines}</div>
                        <div>
                          <small>
                            {e.model?.replace("_", "/").replace("?", "")}
                          </small>
                        </div>
                      </div>
                    </div>
                    <Divider hidden fitted></Divider>
                    <NavLink
                      to={`/team-model-events/gp/${e.idTeamModel}/${item.season}/-`}
                    >
                      <Image
                        centered
                        src={`/build/images/teams/models/team_${e.team.toLowerCase()}_${e.model
                          .toLowerCase()
                          .replace("???", "3")
                          .replace("??", "2")
                          .replace("?", "1")}.jpg`}
                        alt={`${item.name} ${e.model.replaceAll("???", "")} ${
                          item.season
                        }`}
                        className="team-season-car-model-photo"
                        onError={(e) => {
                          e.target.src = "/build/images/teams/team_no_car.jpg";
                        }}
                      />
                    </NavLink>
                    <div className="bold-uppercase">
                      <small>
                        {e.engines}{" "}
                        {e.model?.replace("_", "/").replace("?", "")}
                      </small>
                    </div>
                    <div>
                      <small>
                        {e.teamsNames ? (
                          e.teamsNames
                        ) : (
                          <FormattedMessage id={"app.stats.private"} />
                        )}
                      </small>
                    </div>
                  </div>
                );
              })}
            </div>
            <Divider></Divider>
            <div className="team-season-stats overflow">
              <Table basic="very" celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell className="no-wrap no-background">
                      {item.gp === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.gp === null
                            ? String.fromCharCode(160)
                            : item.gp}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.qual === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.qual === null
                            ? String.fromCharCode(160)
                            : item.qual}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.starts === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.starts === null
                            ? String.fromCharCode(160)
                            : item.starts}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.wins === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.wins === null
                            ? String.fromCharCode(160)
                            : item.wins}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.second === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.second === null
                            ? String.fromCharCode(160)
                            : item.second}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.third === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.third === null
                            ? String.fromCharCode(160)
                            : item.third}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.podium === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.podium === null
                            ? String.fromCharCode(160)
                            : item.podium}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.polepos === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.polepos === null
                            ? String.fromCharCode(160)
                            : item.polepos}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.bestlaps === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.bestlaps === null
                            ? String.fromCharCode(160)
                            : item.bestlaps}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.completed === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.completed === null
                            ? String.fromCharCode(160)
                            : item.completed}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.incomplete === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.incomplete === null
                            ? String.fromCharCode(160)
                            : item.incomplete}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.disq === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/disq/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.disq === null
                            ? String.fromCharCode(160)
                            : item.disq}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.season > 1957 &&
                        (item.points === 0 ? (
                          0
                        ) : (
                          <NavLink
                            to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                          >
                            {item.points === 0
                              ? String.fromCharCode(160)
                              : Math.round(item.points * 100) / 100}
                            {item.points !== item.pointsClass && (
                              <small>
                                {" ("}
                                {item.pointsClass === 0
                                  ? String.fromCharCode(160)
                                  : Math.round(item.pointsClass * 100) / 100}
                                {")"}
                              </small>
                            )}
                          </NavLink>
                        ))}
                      {item.season < 1958 && "-"}
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </div>
            {item.starts > 0 ? (
              <div className="team-season-gp-results">
                <Segment basic textAlign="center" className="overflow">
                  <span className="gp-key-value-box">
                    <div className="gp-key-value-box-value-empty"></div>
                    <div className="gp-key-value-box-value-empty"></div>
                    <div className="gp-key-value-box-value-empty"></div>
                    {item.teamDrivers.map((driver, idx) =>
                      idx == 0 ? (
                        <div
                          key={idx}
                          className="gp-key-value-box-value-first bold-uppercase left-aligned"
                        >
                          <small>
                            <NavLink to={`/driver/${driver.alias}`}>
                              {driver.place ? (
                                driver.place
                              ) : (
                                <FormattedMessage id={"app.stats.nc"} />
                              )}
                              . {driver.surname}
                            </NavLink>
                          </small>
                        </div>
                      ) : (
                        <div
                          key={idx}
                          className="gp-key-value-box-value bold-uppercase left-aligned"
                        >
                          <small>
                            <NavLink to={`/driver/${driver.alias}`}>
                              {driver.place ? (
                                driver.place
                              ) : (
                                <FormattedMessage id={"app.stats.nc"} />
                              )}
                              . {driver.surname}
                            </NavLink>
                          </small>
                        </div>
                      )
                    )}
                    {item.season > 1957 && (
                      <div className="gp-key-value-box-value-points">
                        <small>
                          <FormattedMessage id={"app.table.header.points"} />
                        </small>
                      </div>
                    )}
                    <div className="gp-key-value-box-value-empty">
                      {String.fromCharCode(160)}
                    </div>
                  </span>
                  {item.gpResults.map((gp, idx) => (
                    <span key={idx} className="gp-key-value-box">
                      {gp.sprint == "1" ? (
                        <div className="gp-key-value-box-value-sprint">
                          <small>
                            <FormattedMessage id={"app.table.header.sprint"} />
                          </small>
                        </div>
                      ) : (
                        <div className="gp-key-value-box-value-empty">
                          {String.fromCharCode(160)}
                        </div>
                      )}
                      <div className="gp-key-value-box-header-top">
                        {idx + 1}
                      </div>
                      <div className="gp-key-value-box-header">
                        <Popup
                          content={`${gp.gp} - ${gp.raceDate}`}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <NavLink
                              to={`/gp-result/${gp.alias}/${item.season}`}
                            >
                              <small>{gp.nameShort}</small>
                              <br />
                              <Flag name={gp.name} />
                            </NavLink>
                          }
                        />
                      </div>
                      {item.teamDrivers.map((driver) => (
                        <div
                          key={driver.id}
                          className={
                            driver.gpResults[idx].place == "1" ||
                            driver.gpResults[idx].place == "2" ||
                            driver.gpResults[idx].place == "3"
                              ? `gp-key-value-box-value cell-${driver.gpResults[idx].place}`
                              : "gp-key-value-box-value"
                          }
                        >
                          <small>
                            {driver.gpResults[idx].completed == 1 ? (
                              <>
                                <Popup
                                  content={`${formatMessage({
                                    id: "app.stats.qual",
                                  })}: ${driver.gpResults[idx].qual}.`}
                                  position="bottom center"
                                  wide="very"
                                  trigger={
                                    <span>{driver.gpResults[idx].place}</span>
                                  }
                                />
                              </>
                            ) : driver.gpResults[idx].qual ? (
                              driver.gpResults[idx].notQualified == 1 ? (
                                <>
                                  <Popup
                                    content={`${formatMessage({
                                      id: "app.stats.qual",
                                    })}: ${driver.gpResults[idx].qual}. - ${
                                      driver.gpResults[idx].qualInfo
                                    }`}
                                    position="bottom center"
                                    wide="very"
                                    trigger={
                                      <span>
                                        <FormattedMessage id={"app.stats.nq"} />
                                      </span>
                                    }
                                  />
                                </>
                              ) : driver.gpResults[idx].notStarted == 1 ? (
                                <FormattedMessage id={"app.stats.ns"} />
                              ) : (
                                "-"
                              )
                            ) : (
                              String.fromCharCode(160)
                            )}
                          </small>
                          {driver.gpResults[idx].info && (
                            <>
                              <Popup
                                content={`${driver.gpResults[idx].info}`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                      ))}
                      {item.season > 1957 && (
                        <div
                          className={
                            gp.points > 0
                              ? `gp-key-value-box-value-points-scored`
                              : "gp-key-value-box-value-points"
                          }
                        >
                          {gp.points == gp.classPoints ? (
                            <small>
                              {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                            </small>
                          ) : (
                            <>
                              <small className="red">{gp.points}</small>
                              <Popup
                                content={formatMessage({
                                  id: "app.stats.points.excluded",
                                })}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                          {gp.sprintPoints > 1 && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={
                                  formatMessage({
                                    id: "app.stats.points.sprint.part1",
                                  }) +
                                  gp.sprintPoints +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part2",
                                  }) +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part4",
                                  })
                                }
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                      )}
                      {gp.bestLap == "1" ? (
                        <div className="gp-key-value-box-value-bl">
                          <small>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </small>
                        </div>
                      ) : (
                        <div className="gp-key-value-box-value-empty">
                          {String.fromCharCode(160)}
                        </div>
                      )}
                    </span>
                  ))}
                </Segment>
              </div>
            ) : (
              <>
                <Divider hidden></Divider>
                <Segment basic>
                  <Message>
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <p>
                      <FormattedMessage id={"app.page.team.charts.grid.info"} />
                    </p>
                  </Message>
                </Segment>
              </>
            )}
          </Segment>
          <Segment basic>
            <Divider hidden></Divider>
            <SectionPageHeader
              title={
                <FormattedMessage
                  id={"app.page.team.season.details.drivers.header"}
                  values={{ count: item.teamDrivers.length }}
                />
              }
              type="quinary"
            />
            {item.teamDrivers.map((driver) => {
              const gpPlacesData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseInt(item.place, 0);
                return gpPlacesData.push(value);
              });

              const gpPointsData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseFloat(item.points, 0);
                return gpPointsData.push(value);
              });

              const gpPointsClassData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseFloat(item.classPoints, 0);
                return gpPointsClassData.push(value);
              });

              const configGPResults = {
                chart: {
                  height: 250,
                },
                title: {
                  text: "",
                },
                subtitle: {
                  text: " ",
                },
                xAxis: {
                  type: "category",
                },
                yAxis: [
                  {
                    title: {
                      text: "",
                    },
                    min: 0,
                    allowDecimals: false,
                    reversed: true,
                    tickInterval: 1,
                  },
                  {
                    title: {
                      text: "",
                    },
                    opposite: true,
                    min: 0,
                    allowDecimals: false,
                  },
                ],
                legend: {
                  enabled: true,
                },
                credits: {
                  enabled: false,
                },
                tooltip: {
                  headerFormat:
                    "<b>" +
                    formatMessage({ id: "app.stats.gp" }) +
                    ": {point.key}</b><br/>",
                  shared: false,
                  useHTML: true,
                },
                plotOptions: {
                  column: {
                    grouping: false,
                  },
                  series: {
                    borderWidth: 0,
                    label: {
                      enabled: false,
                    },
                    dataLabels: {
                      enabled: true,
                      format: "{point.y}",
                    },
                    cursor: "pointer",
                  },
                },
                series: [
                  {
                    yAxis: 1,
                    type: "column",
                    name: formatMessage({ id: "app.stats.points" }),
                    color: "#b1b1b1",
                    maxPointWidth: 20,
                    data: gpPointsData,
                    stack: "Punkty",
                  },
                  {
                    yAxis: 1,
                    type: "column",
                    name: formatMessage({ id: "app.stats.points.class" }),
                    color: "#cbb973",
                    maxPointWidth: 20,
                    data: gpPointsClassData,
                    stack: "Punkty",
                    dataLabels: {
                      inside: true,
                    },
                  },

                  {
                    type: "line",
                    name: formatMessage({ id: "app.stats.places" }),
                    color: "#000000",
                    maxPointWidth: 20,
                    data: gpPlacesData,
                    label: {
                      onArea: false,
                    },
                  },
                ],
              };

              const gpPlacesBySeasonData = [];
              driver.gpPlaces
                ?.filter((e) => e.season == item.season)
                .sort((a, b) => a.name - b.name)
                .map((item) => {
                  const value = { name: "", y: 0 };
                  value.name = item.name;
                  value.y = parseInt(item.y, 0);
                  return gpPlacesBySeasonData.push(value);
                });

              const ppPlacesBySeasonData = [];
              driver.ppPlaces
                ?.filter((e) => e.season == item.season)
                .sort((a, b) => a.name - b.name)
                .map((item) => {
                  const value = { name: "", y: 0 };
                  value.name = item.name;
                  value.y = parseInt(item.y, 0);
                  return ppPlacesBySeasonData.push(value);
                });

              const configPlacesBySeason = {
                chart: {
                  height: 200,
                  type: "column",
                },
                title: {
                  text: "",
                },
                subtitle: {
                  text: " ",
                },
                xAxis: {
                  type: "category",
                },
                yAxis: {
                  min: 0,
                  allowDecimals: false,
                  tickInterval: 1,
                  title: {
                    text: "",
                  },
                },
                legend: {
                  enabled: true,
                },
                credits: {
                  enabled: false,
                },
                tooltip: {
                  headerFormat:
                    "<b>{series.name}</b></br>" +
                    formatMessage({ id: "app.stats.place" }) +
                    " {point.key}: <b>{point.y:f}</b>",
                  pointFormat: "",
                  footerFormat: "",
                  shared: false,
                  useHTML: true,
                },
                plotOptions: {
                  series: {
                    borderWidth: 0,
                    dataLabels: {
                      enabled: true,
                      format: "{point.y}",
                    },
                    cursor: "pointer",
                    events: {
                      click: function (event) {
                        if (
                          event.point.series.name.startsWith("Pola startowe")
                        ) {
                          props.history.push({
                            pathname: `/driver-events/grid-places/${driver.alias}/${item.alias}/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                          });
                        } else {
                          props.history.push({
                            pathname: `/driver-events/race-places/${driver.alias}/${item.alias}/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                          });
                        }
                      },
                    },
                  },
                },
                series: [
                  {
                    name: formatMessage({ id: "app.stats.race" }),
                    color: "#cbb973",
                    maxPointWidth: 20,
                    data: gpPlacesBySeasonData,
                  },
                  {
                    name: formatMessage({ id: "app.stats.grid" }),
                    color: "#b1b1b1",
                    maxPointWidth: 20,
                    data: ppPlacesBySeasonData,
                  },
                ],
              };

              return (
                <div key={driver.id}>
                  <Segment basic>
                    <div className="box-image-name">
                      <Statistic>
                        <Statistic.Value>
                          <NavLink
                            to={`/classification/drivers-points/${item.season}`}
                          >
                            {driver.place ? (
                              driver.place
                            ) : (
                              <FormattedMessage id={"app.stats.nc"} />
                            )}
                          </NavLink>
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.place"} />
                        </Statistic.Label>
                      </Statistic>
                      <div>
                        <NavLink to={`/driver/${driver.alias}`}>
                          <Image
                            size="tiny"
                            src={`/build/images/drivers/driver_${driver.id}_profile.jpg`}
                            alt={`${driver.name} ${driver.surname}`}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </div>
                      <div>
                        <div>
                          <Flag name={driver.countryCode} />{" "}
                          <NavLink to={`/driver/${driver.alias}`}>
                            {driver.name} {driver.surname}
                          </NavLink>
                        </div>
                        <div>
                          <small>{driver.teamName}</small>
                        </div>
                      </div>
                    </div>
                    <Table basic="very" celled>
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.gp"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.gp.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.qual"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.qual.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.race"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.race.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.p1"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.p1.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.p2"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.p2.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.p3"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.p3.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.podiums"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.podiums.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.polepos"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.polepos.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.bestlaps"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.bestlaps.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.compl"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.compl.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.inc"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.inc.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.disq"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.disq.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <Popup
                              trigger={
                                <div>
                                  <FormattedMessage
                                    id={"app.table.header.points"}
                                  />
                                </div>
                              }
                              flowing
                              hoverable
                            >
                              <FormattedMessage
                                id={"app.table.header.points.long"}
                              />
                            </Popup>
                          </Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        <Table.Row>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Grand Prix`}
                            className="no-wrap no-background"
                          >
                            {driver.gp === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/gp/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.gp === null
                                  ? String.fromCharCode(160)
                                  : driver.gp}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Kwalifikacje`}
                            className="no-wrap"
                          >
                            {driver.qual === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/qual/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.qual === null
                                  ? String.fromCharCode(160)
                                  : driver.qual}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Wyścigi`}
                            className="no-wrap no-background"
                          >
                            {driver.starts === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/starts/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.starts === null
                                  ? String.fromCharCode(160)
                                  : driver.starts}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Wygrane`}
                            className="no-wrap"
                          >
                            {driver.p1 === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/wins/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.p1 === null
                                  ? String.fromCharCode(160)
                                  : driver.p1}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Drugie miejsca`}
                            className="no-wrap"
                          >
                            {driver.p2 === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/second/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.p2 === null
                                  ? String.fromCharCode(160)
                                  : driver.p2}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Trzecie miejsca`}
                            className="no-wrap"
                          >
                            {driver.p3 === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/third/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.p3 === null
                                  ? String.fromCharCode(160)
                                  : driver.p3}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Podium`}
                            className="no-wrap"
                          >
                            {driver.pd === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/podium/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.pd === null
                                  ? String.fromCharCode(160)
                                  : driver.pd}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Pole Position`}
                            className="no-wrap"
                          >
                            {driver.pp === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/polepos/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.pp === null
                                  ? String.fromCharCode(160)
                                  : driver.pp}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Naj. okrążenia`}
                            className="no-wrap"
                          >
                            {driver.bl === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/bestlaps/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.bl === null
                                  ? String.fromCharCode(160)
                                  : driver.bl}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Ukończone wyścigi`}
                            className="no-wrap"
                          >
                            {driver.compl === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/completed/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.compl === null
                                  ? String.fromCharCode(160)
                                  : driver.compl}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Nieukończone wyścigi`}
                            className="no-wrap"
                          >
                            {driver.incompl === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/incomplete/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.incompl === null
                                  ? String.fromCharCode(160)
                                  : driver.incompl}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Dyskwalifikacje`}
                            className="no-wrap"
                          >
                            {driver.dsq === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/disq/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.dsq === null
                                  ? String.fromCharCode(160)
                                  : driver.dsq}
                              </NavLink>
                            )}
                          </Table.Cell>
                          <Table.Cell
                            data-title={`Kierowca ${driver.id} - Punkty`}
                            className="no-wrap"
                          >
                            {driver.points === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/points/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                              >
                                {driver.points === 0
                                  ? String.fromCharCode(160)
                                  : driver.points}
                              </NavLink>
                            )}
                            {driver.points != driver.pointsClass && (
                              <small> ({driver.pointsClass})</small>
                            )}
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    </Table>
                    <Divider hidden></Divider>
                  </Segment>
                  <Segment basic textAlign="center" className="overflow">
                    <Divider hidden></Divider>
                    <span className="gp-key-value-box">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div className="gp-key-value-box-value-first">
                        <Popup
                          content={formatMessage({
                            id: "app.stats.qual",
                          })}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <small>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </small>
                          }
                        />
                      </div>
                      <div className="gp-key-value-box-value">
                        <Popup
                          content={formatMessage({
                            id: "app.stats.grid",
                          })}
                          position="bottom center"
                          wide="very"
                          trigger={<small>PS</small>}
                        />
                      </div>
                      <div className="gp-key-value-box-value">
                        <Popup
                          content={formatMessage({
                            id: "app.stats.race",
                          })}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <small>
                              <FormattedMessage id={"app.table.header.race"} />
                            </small>
                          }
                        />
                      </div>
                      <div className="gp-key-value-box-value-points">
                        <small>
                          <FormattedMessage id={"app.table.header.points"} />
                        </small>
                      </div>
                      <div className="gp-key-value-box-value-empty">
                        {String.fromCharCode(160)}
                      </div>
                    </span>
                    {driver.gpResults.map((gp, idx) => (
                      <span key={idx} className="gp-key-value-box">
                        {gp.sprint == "1" ? (
                          <div className="gp-key-value-box-value-sprint">
                            <small>
                              <FormattedMessage
                                id={"app.table.header.sprint"}
                              />
                            </small>
                          </div>
                        ) : (
                          <div className="gp-key-value-box-value-empty">
                            {String.fromCharCode(160)}
                          </div>
                        )}
                        <div className="gp-key-value-box-header-top">
                          {idx + 1}
                        </div>
                        <div className="gp-key-value-box-header">
                          <Popup
                            content={`${gp.gp} - ${gp.raceDate}`}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <NavLink
                                to={`/gp-result/${gp.alias}/${item.season}`}
                              >
                                <small>{gp.nameShort}</small>
                                <br />
                                <Flag name={gp.name} />
                              </NavLink>
                            }
                          />
                        </div>
                        <div className="gp-key-value-box-value">
                          {gp.modelName ? (
                            <Popup
                              content={`${
                                gp.teamName ? (
                                  gp.teamName
                                ) : (
                                  <FormattedMessage id={"app.stats.private"} />
                                )
                              } ${gp.modelName}`}
                              position="bottom center"
                              wide="very"
                              trigger={
                                <Image
                                  size="mini"
                                  src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                                  alt={gp.team}
                                  className="center-aligned"
                                  onError={(e) => {
                                    e.target.src =
                                      "/build/images/teams/team_no_profile.jpg";
                                  }}
                                />
                              }
                            />
                          ) : (
                            <Image
                              size="mini"
                              src={`/build/images/teams/team_no_profile.jpg`}
                              alt={gp.team}
                              className="center-aligned"
                            />
                          )}
                        </div>
                        <div className="gp-key-value-box-value">
                          <small>
                            {gp.qual ? gp.qual : String.fromCharCode(160)}
                          </small>
                          {gp.qualInfo && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.qualInfo}`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.grid == "1"
                              ? `gp-key-value-box-value cell-${gp.grid}`
                              : "gp-key-value-box-value"
                          }
                        >
                          <small>
                            {gp.grid
                              ? gp.grid
                              : gp.qual
                              ? "-"
                              : String.fromCharCode(160)}
                          </small>
                          {gp.gridInfo && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.gridInfo}`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.place == "1" ||
                            gp.place == "2" ||
                            gp.place == "3"
                              ? `gp-key-value-box-value cell-${gp.place}`
                              : "gp-key-value-box-value"
                          }
                        >
                          <small>
                            {gp.completed == 1 ? (
                              gp.place
                            ) : gp.qual ? (
                              gp.notQualified == 1 ? (
                                <FormattedMessage id={"app.stats.nq"} />
                              ) : gp.notStarted == 1 ? (
                                <FormattedMessage id={"app.stats.ns"} />
                              ) : (
                                "-"
                              )
                            ) : (
                              String.fromCharCode(160)
                            )}
                          </small>
                          {gp.info && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.info} (${formatMessage({
                                  id: "app.stats.lap",
                                })} ${gp.laps})`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.points > 0
                              ? `gp-key-value-box-value-points-scored`
                              : "gp-key-value-box-value-points"
                          }
                        >
                          {gp.excluded == null || gp.excluded == "0" ? (
                            <small>
                              {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                            </small>
                          ) : (
                            <>
                              <small className="red">{gp.points}</small>
                              <Popup
                                content={
                                  <FormattedMessage
                                    id={"app.stats.points.excluded"}
                                  />
                                }
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                          {gp.sprintPoints > 1 && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={
                                  formatMessage({
                                    id: "app.stats.points.sprint.part1",
                                  }) +
                                  gp.sprintPoints +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part2",
                                  }) +
                                  gp.sprintPos +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part3",
                                  })
                                }
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        {gp.bestLap == "1" ? (
                          <div className="gp-key-value-box-value-bl">
                            <small>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </small>
                          </div>
                        ) : (
                          <div className="gp-key-value-box-value-empty">
                            {String.fromCharCode(160)}
                          </div>
                        )}
                      </span>
                    ))}
                  </Segment>
                  {driver.starts > 0 ? (
                    <Segment basic>
                      <Divider hidden></Divider>
                      <SectionPageHeader
                        title={
                          <FormattedMessage
                            id={"app.page.team.season.details.chart1.header"}
                          />
                        }
                        type="quaternary"
                      />
                      <Divider hidden></Divider>
                      <ReactHighcharts config={configGPResults} />
                      <SectionPageHeader
                        title={
                          <FormattedMessage
                            id={"app.page.team.season.details.chart2.header"}
                          />
                        }
                        type="quaternary"
                      />
                      <Divider hidden></Divider>
                      <ReactHighcharts config={configPlacesBySeason} />
                    </Segment>
                  ) : (
                    <Segment basic>
                      <Message>
                        <Message.Header>
                          <FormattedMessage id={"app.message.header"} />
                        </Message.Header>
                        <p>
                          <FormattedMessage
                            id={"app.page.driver.charts.grid.info"}
                          />
                        </p>
                      </Message>
                    </Segment>
                  )}
                  <Divider></Divider>
                </div>
              );
            })}
          </Segment>
        </Segment>
      );
      elements.push(element);
      sumGP += parseInt(item.gp, 0);
      sumStarts += parseInt(item.starts, 0);
      sumQuals += parseInt(item.qual, 0);
      sumWins += parseInt(item.wins, 0);
      sumSecond += parseInt(item.second, 0);
      sumThird += parseInt(item.third, 0);
      sumPodium += parseInt(item.podium, 0);
      sumPolepos += parseInt(item.polepos, 0);
      sumBestlaps += parseInt(item.bestlaps, 0);
      sumPointsPlaces += parseInt(item.pointsPlaces, 0);
      sumCompleted += parseInt(item.completed, 0);
      sumIncomplete += parseInt(item.incomplete, 0);
      sumDisq += parseInt(item.disq, 0);
      sumPoints += parseFloat(item.points, 1);
      sumPointsClass += parseFloat(item.pointsClass, 1);
    });

    return elements;
  }

  renderTeamSeasonDetailsMobile() {
    const elements = [];
    const { props } = this;
    const { seasonDetails } = props.team.data;
    const { formatMessage } = this.props.intl;

    seasonDetails.forEach((item) => {
      const element = (
        <Segment basic key={item.season}>
          <Segment basic>
            <div className="box-image-name">
              <Statistic size="mini">
                <Statistic.Value>
                  <NavLink to={`/classification/teams-points/${item.season}`}>
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place === "1" && (
                        <FormattedMessage id={"app.stats.wc"} />
                      )}
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place != "1" &&
                      item.place != "NK" &&
                      item.place}
                    {item.season > 1957 &&
                      item.season < 2025 &&
                      item.place != "1" &&
                      item.place === "NK" && (
                        <FormattedMessage id={"app.stats.nc"} />
                      )}
                    {item.season == 2025 && item.place === "1" && item.place}
                    {item.season < 1957 && "-"}
                  </NavLink>
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.place"} />
                </Statistic.Label>
              </Statistic>
              <Image
                size="tiny"
                src={`/build/images/teams/team_${item.id.toLowerCase()}_profile_logo.jpg`}
                alt={item.name}
                className="driver-season-team-logo"
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
              <div>
                <div>
                  <Flag name={item.country} />{" "}
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.fullname ? (
                      item.fullname
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Segment>
          <Segment basic>
            <Divider></Divider>
            {item.carModels.map((e) => (
              <Segment basic key={e.model}>
                <div className="box-image-name">
                  {e.engines.split(", ").map((engine, idx) => {
                    return (
                      engine != item.name.trim() && (
                        <Image
                          key={idx}
                          size="tiny"
                          src={`/build/images/engines/engine_${engine
                            ?.toLowerCase()
                            .replaceAll(".", "_")
                            .replaceAll(" ", "_")}.jpg`}
                          alt={engine}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_profile.jpg";
                          }}
                        />
                      )
                    );
                  })}
                  <div>
                    <div className="bold-uppercase">{e.engines}</div>
                    <div>
                      <small>
                        {e.model?.replace("_", "/").replace("?", "")}
                      </small>
                    </div>
                  </div>
                </div>
                <Divider hidden fitted></Divider>
                <NavLink
                  to={`/team-model-events/gp/${e.idTeamModel}/${item.season}/-`}
                >
                  <Image
                    centered
                    src={`/build/images/teams/models/team_${e.team.toLowerCase()}_${e.model
                      .toLowerCase()
                      .replace("???", "3")
                      .replace("??", "2")
                      .replace("?", "1")}.jpg`}
                    alt={`${item.name} ${e.model.replaceAll("???", "")} ${
                      item.season
                    }`}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_car.jpg";
                    }}
                  />
                </NavLink>
                <Segment basic padded>
                  <div className="bold-uppercase">
                    {e.engines} {e.name}{" "}
                    {e.model?.replace("_", "/").replace("?", "")}
                  </div>
                  <div>
                    <small>
                      {e.teamsNames ? (
                        e.teamsNames
                      ) : (
                        <FormattedMessage id={"app.stats.private"} />
                      )}
                    </small>
                  </div>
                </Segment>
                <Divider></Divider>
              </Segment>
            ))}
          </Segment>
          <Segment basic className="overflow">
            <Divider hidden fitted></Divider>
            <table className="basic-table">
              <thead>
                <tr>
                  <th>
                    <FormattedMessage id={"app.table.header.gp"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.qual"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.race"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p1"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p2"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p3"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.podiums"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.polepos"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.bestlaps"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.compl"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.inc"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.disq"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.points"} />
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="Grand Prix">
                    {item.gp === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.gp === null ? String.fromCharCode(160) : item.gp}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Kwalifikacje">
                    {item.qual === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.qual === null
                          ? String.fromCharCode(160)
                          : item.qual}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyścigi">
                    {item.starts === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.starts === null
                          ? String.fromCharCode(160)
                          : item.starts}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P1">
                    {item.wins === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.wins === null
                          ? String.fromCharCode(160)
                          : item.wins}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P2">
                    {item.second === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.second === null
                          ? String.fromCharCode(160)
                          : item.second}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P3">
                    {item.third === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.third === null
                          ? String.fromCharCode(160)
                          : item.third}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Podium">
                    {item.podium === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.podium === null
                          ? String.fromCharCode(160)
                          : item.podium}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Pole Position">
                    {item.polepos === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.polepos === null
                          ? String.fromCharCode(160)
                          : item.polepos}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Naj. okrążenia">
                    {item.bestlaps === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.bestlaps === null
                          ? String.fromCharCode(160)
                          : item.bestlaps}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyś. ukończone">
                    {item.completed === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.completed === null
                          ? String.fromCharCode(160)
                          : item.completed}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyś. nieukończone">
                    {item.incomplete === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.incomplete === null
                          ? String.fromCharCode(160)
                          : item.incomplete}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Dyskwalifikacje">
                    {item.disq === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/team-events/disq/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.disq === null
                          ? String.fromCharCode(160)
                          : item.disq}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Punkty">
                    {item.season > 1957 &&
                      (item.points === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.points === 0
                            ? String.fromCharCode(160)
                            : Math.round(item.points * 100) / 100}
                          {item.points !== item.pointsClass && (
                            <small>
                              {" ("}
                              {item.pointsClass === 0
                                ? String.fromCharCode(160)
                                : Math.round(item.pointsClass * 100) / 100}
                              {")"}
                            </small>
                          )}
                        </NavLink>
                      ))}
                    {item.season < 1958 && "-"}
                  </td>
                </tr>
              </tbody>
            </table>
          </Segment>
          <Segment basic className="overflow">
            <Divider hidden></Divider>
            <span className="gp-key-value-box">
              <div className="gp-key-value-box-value-empty"></div>
              <div className="gp-key-value-box-value-empty"></div>
              <div className="gp-key-value-box-value-empty"></div>
              {item.teamDrivers.map((driver, idx) =>
                idx == 0 ? (
                  <div
                    key={idx}
                    className="gp-key-value-box-value-first bold-uppercase left-aligned"
                  >
                    <small>
                      <NavLink to={`/driver/${driver.alias}`}>
                        {driver.place ? (
                          driver.place
                        ) : (
                          <FormattedMessage id={"app.stats.nc"} />
                        )}
                        . {driver.surname}
                      </NavLink>
                    </small>
                  </div>
                ) : (
                  <div
                    key={idx}
                    className="gp-key-value-box-value bold-uppercase left-aligned"
                  >
                    <small>
                      <NavLink to={`/driver/${driver.alias}`}>
                        {driver.place ? (
                          driver.place
                        ) : (
                          <FormattedMessage id={"app.stats.nc"} />
                        )}
                        . {driver.surname}
                      </NavLink>
                    </small>
                  </div>
                )
              )}
              {item.season > 1957 && (
                <div className="gp-key-value-box-value-points">
                  <small>
                    <FormattedMessage id={"app.table.header.points"} />
                  </small>
                </div>
              )}
              <div className="gp-key-value-box-value-empty">
                {String.fromCharCode(160)}
              </div>
            </span>
            {item.gpResults.map((gp, idx) => (
              <span key={idx} className="gp-key-value-box">
                {gp.sprint == "1" ? (
                  <div className="gp-key-value-box-value-sprint">
                    <small>
                      <FormattedMessage id={"app.table.header.sprint"} />
                    </small>
                  </div>
                ) : (
                  <div className="gp-key-value-box-value-empty">
                    {String.fromCharCode(160)}
                  </div>
                )}
                <div className="gp-key-value-box-header-top">{idx + 1}</div>
                <div className="gp-key-value-box-header">
                  <Popup
                    content={`${gp.gp} - ${gp.raceDate}`}
                    position="bottom center"
                    wide="very"
                    trigger={
                      <NavLink to={`/gp-result/${gp.alias}/${item.season}`}>
                        <small>{gp.nameShort}</small>
                        <br />
                        <Flag name={gp.name} />
                      </NavLink>
                    }
                  />
                </div>
                {item.teamDrivers.map((driver) => (
                  <div
                    key={driver.id}
                    className={
                      driver.gpResults[idx].place == "1" ||
                      driver.gpResults[idx].place == "2" ||
                      driver.gpResults[idx].place == "3"
                        ? `gp-key-value-box-value cell-${driver.gpResults[idx].place}`
                        : "gp-key-value-box-value"
                    }
                  >
                    <small>
                      {driver.gpResults[idx].completed == 1 ? (
                        <>
                          <Popup
                            content={`${formatMessage({
                              id: "app.stats.qual",
                            })}: ${driver.gpResults[idx].qual}.`}
                            position="bottom center"
                            wide="very"
                            trigger={<span>{driver.gpResults[idx].place}</span>}
                          />
                        </>
                      ) : driver.gpResults[idx].qual ? (
                        driver.gpResults[idx].notQualified == 1 ? (
                          <>
                            <Popup
                              content={`${formatMessage({
                                id: "app.stats.qual",
                              })}: ${driver.gpResults[idx].qual}. - ${
                                driver.gpResults[idx].qualInfo
                              }`}
                              position="bottom center"
                              wide="very"
                              trigger={
                                <span>
                                  <FormattedMessage id={"app.stats.nq"} />
                                </span>
                              }
                            />
                          </>
                        ) : driver.gpResults[idx].notStarted == 1 ? (
                          <FormattedMessage id={"app.stats.ns"} />
                        ) : (
                          "-"
                        )
                      ) : (
                        String.fromCharCode(160)
                      )}
                    </small>
                    {driver.gpResults[idx].info && (
                      <>
                        <Popup
                          content={`${driver.gpResults[idx].info} (${
                            gp.laps
                          } ${formatMessage({
                            id: "app.stats.lap",
                          })})`}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <Label
                              attached="top right"
                              color="blue"
                              size="small"
                            >
                              !
                            </Label>
                          }
                        />
                      </>
                    )}
                  </div>
                ))}
                {item.season > 1957 && (
                  <div
                    className={
                      gp.points > 0
                        ? `gp-key-value-box-value-points-scored`
                        : "gp-key-value-box-value-points"
                    }
                  >
                    {gp.points == gp.classPoints ? (
                      <small>
                        {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                      </small>
                    ) : (
                      <>
                        <small className="red">{gp.points}</small>
                        <Popup
                          content={formatMessage({
                            id: "app.stats.points.class",
                          })}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <Label
                              attached="top right"
                              color="blue"
                              size="small"
                            >
                              !
                            </Label>
                          }
                        />
                      </>
                    )}
                    {gp.sprintPoints > 1 && (
                      <>
                        {String.fromCharCode(160)}
                        <Popup
                          content={
                            formatMessage({
                              id: "app.stats.points.sprint.part1",
                            }) +
                            gp.sprintPoints +
                            formatMessage({
                              id: "app.stats.points.sprint.part2",
                            }) +
                            formatMessage({
                              id: "app.stats.points.sprint.part4",
                            })
                          }
                          position="bottom center"
                          wide="very"
                          trigger={
                            <Label
                              attached="top right"
                              color="blue"
                              size="small"
                            >
                              !
                            </Label>
                          }
                        />
                      </>
                    )}
                  </div>
                )}
                {gp.bestLap == "1" ? (
                  <div className="gp-key-value-box-value-bl">
                    <small>
                      <FormattedMessage id={"app.table.header.bestlaps"} />
                    </small>
                  </div>
                ) : (
                  <div className="gp-key-value-box-value-empty">
                    {String.fromCharCode(160)}
                  </div>
                )}
              </span>
            ))}
          </Segment>
          <Segment basic textAlign="left">
            <Divider hidden></Divider>
            <SectionPageHeader
              title={
                <FormattedMessage
                  id={"app.page.team.season.details.drivers.header"}
                  values={{ count: item.teamDrivers.length }}
                />
              }
              type="quinary"
            />
            <Divider hidden></Divider>
            {item.teamDrivers.map((driver) => {
              const gpPlacesData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseInt(item.place, 0);
                return gpPlacesData.push(value);
              });

              const gpPointsData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseFloat(item.points, 0);
                return gpPointsData.push(value);
              });

              const gpPointsClassData = [];
              driver.gpResults.map((item) => {
                const value = { name: "", y: 0 };
                value.name = item.nameShort;
                value.y = parseFloat(item.classPoints, 0);
                return gpPointsClassData.push(value);
              });

              const configGPResults = {
                chart: {
                  height: 250,
                },
                title: {
                  text: "",
                },
                subtitle: {
                  text: " ",
                },
                xAxis: {
                  type: "category",
                },
                yAxis: [
                  {
                    title: {
                      text: "",
                    },
                    min: 0,
                    allowDecimals: false,
                    reversed: true,
                    tickInterval: 1,
                  },
                  {
                    title: {
                      text: "",
                    },
                    opposite: true,
                    min: 0,
                    allowDecimals: false,
                  },
                ],
                legend: {
                  enabled: true,
                },
                credits: {
                  enabled: false,
                },
                tooltip: {
                  headerFormat:
                    "<b>" +
                    formatMessage({
                      id: "app.stats.gp",
                    }) +
                    ": {point.key}</b><br/>",
                  shared: false,
                  useHTML: true,
                },
                plotOptions: {
                  column: {
                    grouping: false,
                  },
                  series: {
                    borderWidth: 0,
                    label: {
                      enabled: false,
                    },
                    dataLabels: {
                      enabled: true,
                      format: "{point.y}",
                    },
                    cursor: "pointer",
                  },
                },
                series: [
                  {
                    yAxis: 1,
                    type: "column",
                    name: formatMessage({ id: "app.stats.points" }),
                    color: "#b1b1b1",
                    maxPointWidth: 20,
                    data: gpPointsData,
                    stack: "Punkty",
                  },
                  {
                    yAxis: 1,
                    type: "column",
                    name: formatMessage({
                      id: "app.stats.points.class",
                    }),
                    color: "#cbb973",
                    maxPointWidth: 20,
                    data: gpPointsClassData,
                    stack: "Punkty",
                    dataLabels: {
                      inside: true,
                    },
                  },

                  {
                    type: "line",
                    name: formatMessage({
                      id: "app.stats.places",
                    }),
                    color: "#000000",
                    maxPointWidth: 20,
                    data: gpPlacesData,
                    label: {
                      onArea: false,
                    },
                  },
                ],
              };

              const gpPlacesBySeasonData = [];
              driver.gpPlaces
                ?.filter((e) => e.season == item.season)
                .sort((a, b) => a.name - b.name)
                .map((item) => {
                  const value = { name: "", y: 0 };
                  value.name = item.name;
                  value.y = parseInt(item.y, 0);
                  return gpPlacesBySeasonData.push(value);
                });

              const ppPlacesBySeasonData = [];
              driver.ppPlaces
                ?.filter((e) => e.season == item.season)
                .sort((a, b) => a.name - b.name)
                .map((item) => {
                  const value = { name: "", y: 0 };
                  value.name = item.name;
                  value.y = parseInt(item.y, 0);
                  return ppPlacesBySeasonData.push(value);
                });

              const configPlacesBySeason = {
                chart: {
                  height: 200,
                  type: "column",
                },
                title: {
                  text: "",
                },
                subtitle: {
                  text: " ",
                },
                xAxis: {
                  type: "category",
                },
                yAxis: {
                  min: 0,
                  allowDecimals: false,
                  tickInterval: 1,
                  title: {
                    text: "",
                  },
                },
                legend: {
                  enabled: true,
                },
                credits: {
                  enabled: false,
                },
                tooltip: {
                  headerFormat:
                    "<b>{series.name}</b></br>" +
                    formatMessage({
                      id: "app.stats.places",
                    }) +
                    " {point.key}: <b>{point.y:f}</b>",
                  pointFormat: "",
                  footerFormat: "",
                  shared: false,
                  useHTML: true,
                },
                plotOptions: {
                  series: {
                    borderWidth: 0,
                    dataLabels: {
                      enabled: true,
                      format: "{point.y}",
                    },
                    cursor: "pointer",
                    events: {
                      click: function (event) {
                        if (
                          event.point.series.name.startsWith("Pola startowe")
                        ) {
                          props.history.push({
                            pathname: `/driver-events/grid-places/${driver.alias}/${item.alias}/-/-/${selectedSeason}/${event.point.name}/`,
                          });
                        } else {
                          props.history.push({
                            pathname: `/driver-events/race-places/${driver.alias}/${item.alias}/-/-/${selectedSeason}/${event.point.name}/`,
                          });
                        }
                      },
                    },
                  },
                },
                series: [
                  {
                    name: formatMessage({ id: "app.stats.race" }),
                    color: "#cbb973",
                    maxPointWidth: 20,
                    data: gpPlacesBySeasonData,
                  },
                  {
                    name: formatMessage({ id: "app.stats.grid" }),
                    color: "#b1b1b1",
                    maxPointWidth: 20,
                    data: ppPlacesBySeasonData,
                  },
                ],
              };

              const picDriver = `/build/images/drivers/driver_${driver.id}_profile.jpg`;
              return (
                <Segment basic key={driver.id}>
                  <Grid columns={2} divided="vertically" key={driver.id}>
                    <Grid.Row>
                      <Grid.Column width={3}>
                        <Segment basic padded textAlign="center">
                          <NavLink to={`/driver/${driver.alias}`}>
                            <Image
                              size="tiny"
                              src={picDriver}
                              alt={`${driver.name} ${driver.surname}`}
                              onError={(e) => {
                                e.target.src =
                                  "/build/images/drivers/driver_no_profile.jpg";
                              }}
                            />
                          </NavLink>
                          <Statistic size="mini">
                            <Statistic.Value>
                              <NavLink
                                to={`/classification/drivers-points/${item.season}`}
                              >
                                {driver.place ? (
                                  driver.place
                                ) : (
                                  <FormattedMessage id={"app.stats.nc"} />
                                )}
                              </NavLink>
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.place"} />
                            </Statistic.Label>
                          </Statistic>
                        </Segment>
                      </Grid.Column>
                      <Grid.Column width={13}>
                        <Item.Group>
                          <Item>
                            <Item.Content verticalAlign="middle">
                              <Item.Header>
                                <Flag name={driver.countryCode} />{" "}
                                <NavLink to={`/driver/${driver.alias}`}>
                                  {driver.name} {driver.surname}
                                </NavLink>
                              </Item.Header>
                              <Item.Description>
                                {driver.teamName}
                              </Item.Description>
                            </Item.Content>
                          </Item>
                        </Item.Group>
                        <Segment basic textAlign="center" className="overflow">
                          <table className="basic-table">
                            <thead>
                              <tr>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.gp"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.qual"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.race"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.p1"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.p2"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.p3"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.podiums"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.polepos"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.bestlaps"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.compl"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.inc"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.disq"}
                                  />
                                </th>
                                <th>
                                  <FormattedMessage
                                    id={"app.table.header.points"}
                                  />
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td data-title="Grand Prix">
                                  {driver.gp === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/gp/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.gp === null
                                        ? String.fromCharCode(160)
                                        : driver.gp}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Kwalifikacje">
                                  {driver.qual === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/qual/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.qual === null
                                        ? String.fromCharCode(160)
                                        : driver.qual}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Wyścig">
                                  {driver.starts === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/starts/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.starts === null
                                        ? String.fromCharCode(160)
                                        : driver.starts}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="P1">
                                  {driver.p1 === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/wins/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.p1 === null
                                        ? String.fromCharCode(160)
                                        : driver.p1}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="P2">
                                  {driver.p2 === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/second/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.p2 === null
                                        ? String.fromCharCode(160)
                                        : driver.p2}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="P3">
                                  {driver.p3 === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/third/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.p3 === null
                                        ? String.fromCharCode(160)
                                        : driver.p3}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Podium">
                                  {driver.pd === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/podium/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.pd === null
                                        ? String.fromCharCode(160)
                                        : driver.pd}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Pole Position">
                                  {driver.pp === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/polepos/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.pp === null
                                        ? String.fromCharCode(160)
                                        : driver.pp}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Naj. okrążenia">
                                  {driver.bl === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/bestlaps/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.bl === null
                                        ? String.fromCharCode(160)
                                        : driver.bl}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Wyś. ukończone">
                                  {driver.compl === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/completed/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.compl === null
                                        ? String.fromCharCode(160)
                                        : driver.compl}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Wyś. nieukończone">
                                  {driver.incompl === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/incomplete/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.incompl === null
                                        ? String.fromCharCode(160)
                                        : driver.incompl}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Dyskwalifikacje">
                                  {driver.dsq === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/disq/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.dsq === null
                                        ? String.fromCharCode(160)
                                        : driver.dsq}
                                    </NavLink>
                                  )}
                                </td>
                                <td data-title="Punkty">
                                  {driver.points === 0 ? (
                                    0
                                  ) : (
                                    <NavLink
                                      to={`/driver-events/points/${driver.alias}/${item.alias}/-/-/${item.season}/1`}
                                    >
                                      {driver.points === 0
                                        ? String.fromCharCode(160)
                                        : Math.round(driver.points * 100) / 100}
                                      {driver.points !== driver.pointsClass && (
                                        <small>
                                          {" ("}
                                          {driver.pointsClass === 0
                                            ? String.fromCharCode(160)
                                            : Math.round(
                                                driver.pointsClass * 100
                                              ) / 100}
                                          {")"}
                                        </small>
                                      )}
                                    </NavLink>
                                  )}
                                  {item.season < 1958 && "-"}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </Segment>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                  <Segment basic textAlign="center" className="overflow">
                    <Divider hidden></Divider>
                    <span className="gp-key-value-box">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div className="gp-key-value-box-value-first">
                        <Popup
                          content={<FormattedMessage id={"app.stats.qual"} />}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <small>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </small>
                          }
                        />
                      </div>
                      <div className="gp-key-value-box-value">
                        <Popup
                          content={<FormattedMessage id={"app.stats.grid"} />}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <small>
                              <FormattedMessage id={"app.table.header.grid"} />
                            </small>
                          }
                        />
                      </div>
                      <div className="gp-key-value-box-value">
                        <Popup
                          content={<FormattedMessage id={"app.stats.race"} />}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <small>
                              <FormattedMessage id={"app.table.header.race"} />
                            </small>
                          }
                        />
                      </div>
                      <div className="gp-key-value-box-value-points">
                        <small>
                          <FormattedMessage id={"app.table.header.points"} />
                        </small>
                      </div>
                      <div className="gp-key-value-box-value-empty">
                        {String.fromCharCode(160)}
                      </div>
                    </span>
                    {driver.gpResults.map((gp, idx) => (
                      <span key={idx} className="gp-key-value-box">
                        {gp.sprint == "1" ? (
                          <div className="gp-key-value-box-value-sprint">
                            <small>
                              <FormattedMessage
                                id={"app.table.header.sprint"}
                              />
                            </small>
                          </div>
                        ) : (
                          <div className="gp-key-value-box-value-empty">
                            {String.fromCharCode(160)}
                          </div>
                        )}
                        <div className="gp-key-value-box-header-top">
                          {idx + 1}
                        </div>
                        <div className="gp-key-value-box-header">
                          <Popup
                            content={`${gp.gp}`}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <NavLink
                                to={`/gp-result/${gp.alias}/${item.season}`}
                              >
                                <small>{gp.nameShort}</small>
                                <br />
                                <Flag name={gp.name} />
                              </NavLink>
                            }
                          />
                        </div>
                        <div className="gp-key-value-box-value">
                          {gp.modelName ? (
                            <Popup
                              content={`${
                                gp.teamName ? (
                                  gp.teamName
                                ) : (
                                  <FormattedMessage id={"app.stats.private"} />
                                )
                              } ${gp.modelName}`}
                              position="bottom center"
                              wide="very"
                              trigger={
                                <Image
                                  size="mini"
                                  src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                                  alt={gp.team}
                                  className="center-aligned"
                                  onError={(e) => {
                                    e.target.src =
                                      "/build/images/teams/team_no_profile.jpg";
                                  }}
                                />
                              }
                            />
                          ) : (
                            <Image
                              size="mini"
                              src={`/build/images/teams/team_no_profile.jpg`}
                              alt={gp.team}
                              className="center-aligned"
                            />
                          )}
                        </div>
                        <div className="gp-key-value-box-value">
                          <small>
                            {gp.qual ? gp.qual : String.fromCharCode(160)}
                          </small>
                          {gp.qualInfo && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.qualInfo}`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.grid == "1"
                              ? `gp-key-value-box-value cell-${gp.grid}`
                              : "gp-key-value-box-value"
                          }
                        >
                          <small>
                            {gp.grid
                              ? gp.grid
                              : gp.qual
                              ? "-"
                              : String.fromCharCode(160)}
                          </small>
                          {gp.gridInfo && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.gridInfo}`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.place == "1" ||
                            gp.place == "2" ||
                            gp.place == "3"
                              ? `gp-key-value-box-value cell-${gp.place}`
                              : "gp-key-value-box-value"
                          }
                        >
                          <small>
                            {gp.completed == 1 ? (
                              gp.place
                            ) : gp.qual ? (
                              gp.notQualified == 1 ? (
                                <FormattedMessage id={"app.stats.nq"} />
                              ) : gp.notStarted == 1 ? (
                                <FormattedMessage id={"app.stats.ns"} />
                              ) : (
                                "-"
                              )
                            ) : (
                              String.fromCharCode(160)
                            )}
                          </small>
                          {gp.info && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={`${gp.info} (${formatMessage({
                                  id: "app.stats.lap",
                                })} ${gp.laps})`}
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        <div
                          className={
                            gp.points > 0
                              ? `gp-key-value-box-value-points-scored`
                              : "gp-key-value-box-value-points"
                          }
                        >
                          {gp.excluded == null || gp.excluded == "0" ? (
                            <small>
                              {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                            </small>
                          ) : (
                            <>
                              <small className="red">{gp.points}</small>
                              <Popup
                                content={
                                  <FormattedMessage
                                    id={"app.stats.points.excluded"}
                                  />
                                }
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                          {gp.sprintPoints > 1 && (
                            <>
                              {String.fromCharCode(160)}
                              <Popup
                                content={
                                  formatMessage({
                                    id: "app.stats.points.sprint.part1",
                                  }) +
                                  gp.sprintPoints +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part2",
                                  }) +
                                  gp.sprintPos +
                                  formatMessage({
                                    id: "app.stats.points.sprint.part3",
                                  })
                                }
                                position="bottom center"
                                wide="very"
                                trigger={
                                  <Label
                                    attached="top right"
                                    color="blue"
                                    size="small"
                                  >
                                    !
                                  </Label>
                                }
                              />
                            </>
                          )}
                        </div>
                        {gp.bestLap == "1" ? (
                          <div className="gp-key-value-box-value-bl">
                            <small>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </small>
                          </div>
                        ) : (
                          <div className="gp-key-value-box-value-empty">
                            {String.fromCharCode(160)}
                          </div>
                        )}
                      </span>
                    ))}
                  </Segment>
                  {driver.starts > 0 ? (
                    <>
                      <Segment basic>
                        <Divider hidden></Divider>
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.team.season.details.chart1.header"}
                            />
                          }
                          type="quaternary"
                        />
                        <Divider hidden></Divider>
                        <ReactHighcharts config={configGPResults} />
                      </Segment>
                      <Segment basic>
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.team.season.details.chart2.header"}
                            />
                          }
                          type="quaternary"
                        />
                        <Divider hidden></Divider>
                        <ReactHighcharts config={configPlacesBySeason} />
                      </Segment>
                    </>
                  ) : (
                    <Segment basic>
                      <Message>
                        <Message.Header>
                          <FormattedMessage id={"app.message.header"} />
                        </Message.Header>
                        <p>
                          <FormattedMessage
                            id={"app.page.driver.charts.grid.info"}
                          />
                        </p>
                      </Message>
                    </Segment>
                  )}
                  <Divider></Divider>
                </Segment>
              );
            })}
          </Segment>
        </Segment>
      );
      elements.push(element);
    });
    return elements;
  }

  renderTeamDrivers() {
    const elements = [];
    const { props } = this;
    const { drivers, driversStats } = props.team.data;
    const { alias } = props.team.data;

    const items = [...drivers];
    switch (this.state.driversSort.direction) {
      case "descending":
        items.sort(
          (a, b) =>
            b[this.state.driversSort.column] - a[this.state.driversSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) =>
            a[this.state.driversSort.column] - b[this.state.driversSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName =
        driversStats.bestDriverByGP[0]?.amount === item.gp
          ? "cell-1"
          : "" + driversStats.bestDriverByGP[1]?.amount === item.gp
          ? "cell-2"
          : "" + driversStats.bestDriverByGP[2]?.amount === item.gp
          ? "cell-3"
          : "";

      const startsClassName =
        driversStats.bestDriverByStarts[0]?.amount === item.starts
          ? "cell-1"
          : "" + driversStats.bestDriverByStarts[1]?.amount === item.starts
          ? "cell-2"
          : "" + driversStats.bestDriverByStarts[2]?.amount === item.starts
          ? "cell-3"
          : "";

      const qualClassName =
        driversStats.bestDriverByQualStarts[0]?.amount === item.qual
          ? "cell-1"
          : "" + driversStats.bestDriverByQualStarts[1]?.amount === item.qual
          ? "cell-2"
          : "" + driversStats.bestDriverByQualStarts[2]?.amount === item.qual
          ? "cell-3"
          : "";

      const winsClassName =
        driversStats.bestDriverByWins[0]?.amount === item.wins
          ? "cell-1"
          : "" + driversStats.bestDriverByWins[1]?.amount === item.wins
          ? "cell-2"
          : "" + driversStats.bestDriverByWins[2]?.amount === item.wins
          ? "cell-3"
          : "";

      const secondClassName =
        driversStats.bestDriverBySecondPlaces[0]?.amount === item.second
          ? "cell-1"
          : "" + driversStats.bestDriverBySecondPlaces[1]?.amount ===
            item.second
          ? "cell-2"
          : "" + driversStats.bestDriverBySecondPlaces[2]?.amount ===
            item.second
          ? "cell-3"
          : "";

      const thirdClassName =
        driversStats.bestDriverByThirdPlaces[0]?.amount === item.third
          ? "cell-1"
          : "" + driversStats.bestDriverByThirdPlaces[1]?.amount === item.third
          ? "cell-2"
          : "" + driversStats.bestDriverByThirdPlaces[2]?.amount === item.third
          ? "cell-3"
          : "";

      const podiumClassName =
        driversStats.bestDriverByPodium[0]?.amount === item.podium
          ? "cell-1"
          : "" + driversStats.bestDriverByPodium[1]?.amount === item.podium
          ? "cell-2"
          : "" + driversStats.bestDriverByPodium[2]?.amount === item.podium
          ? "cell-3"
          : "";

      const poleposClassName =
        driversStats.bestDriverByPolepos[0]?.amount === item.polepos
          ? "cell-1"
          : "" + driversStats.bestDriverByPolepos[1]?.amount === item.polepos
          ? "cell-2"
          : "" + driversStats.bestDriverByPolepos[2]?.amount === item.polepos
          ? "cell-3"
          : "";

      const bestlapsClassName =
        driversStats.bestDriverByBestlaps[0]?.amount === item.bestlaps
          ? "cell-1"
          : "" + driversStats.bestDriverByBestlaps[1]?.amount === item.bestlaps
          ? "cell-2"
          : "" + driversStats.bestDriverByBestlaps[2]?.amount === item.bestlaps
          ? "cell-3"
          : "";

      const completedClassName =
        driversStats.bestDriverByCompleted[0]?.amount === item.completed
          ? "cell-1"
          : "" + driversStats.bestDriverByCompleted[1]?.amount ===
            item.completed
          ? "cell-2"
          : "" + driversStats.bestDriverByCompleted[2]?.amount ===
            item.completed
          ? "cell-3"
          : "";

      const incompleteClassName =
        driversStats.bestDriverByIncompleted[0]?.amount === item.incomplete
          ? "cell-1"
          : "" + driversStats.bestDriverByIncompleted[1]?.amount ===
            item.incomplete
          ? "cell-2"
          : "" + driversStats.bestDriverByIncompleted[2]?.amount ===
            item.incomplete
          ? "cell-3"
          : "";

      const disqClassName =
        driversStats.bestDriverByDisq[0]?.amount === item.disq
          ? "cell-1"
          : "" + driversStats.bestDriverByDisq[1]?.amount === item.disq
          ? "cell-2"
          : "" + driversStats.bestDriverByDisq[2]?.amount === item.disq
          ? "cell-3"
          : "";

      const pointsClassName =
        driversStats.bestDriverByPoints[0]?.amount === item.points
          ? "cell-1"
          : "" + driversStats.bestDriverByPoints[1]?.amount === item.points
          ? "cell-2"
          : "" + driversStats.bestDriverByPoints[2]?.amount === item.points
          ? "cell-3"
          : "";

      const picDriver = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.alias}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={picDriver}
                    alt={item.driver}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/driver/${item.alias}`}>
                    <Flag name={item.country} /> {item.driver}
                  </NavLink>
                </div>
                <div>
                  <small>
                    {item.seasonMin == item.seasonMax
                      ? item.seasonMax
                      : `${item.seasonMin} - ${item.seasonMax}`}{" "}
                    ({item.seasons}{" "}
                    <FormattedMessage id={"app.stats.seasons"} />)
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/gp/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/qual/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/starts/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/wins/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/second/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/third/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/podium/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/polepos/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/bestlaps/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/completed/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/incomplete/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/disq/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            <NavLink
              to={`/team-events/points/${alias}/${item.alias}/-/-/-/-/-/-/1`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : Math.round(item.points * 100) / 100}
              {item.points !== item.pointsClass && (
                <div>
                  <small>
                    {" ("}
                    {item.pointsClass === null
                      ? String.fromCharCode(160)
                      : Math.round(item.pointsClass * 100) / 100}
                    {")"}
                  </small>
                </div>
              )}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });

    return elements;
  }

  renderTeamDriversMobile() {
    const elements = [];
    const { props } = this;
    const { drivers, driversStats } = props.team.data;
    const { alias } = props.team.data;

    drivers.forEach((item, idx) => {
      const gpClassName =
        driversStats.bestDriverByGP[0]?.amount === item.gp
          ? "cell-1"
          : "" + driversStats.bestDriverByGP[1]?.amount === item.gp
          ? "cell-2"
          : "" + driversStats.bestDriverByGP[2]?.amount === item.gp
          ? "cell-3"
          : "";
      const startsClassName =
        driversStats.bestDriverByStarts[0]?.amount === item.starts
          ? "cell-1"
          : "" + driversStats.bestDriverByStarts[1]?.amount === item.starts
          ? "cell-2"
          : "" + driversStats.bestDriverByStarts[2]?.amount === item.starts
          ? "cell-3"
          : "";

      const qualClassName =
        driversStats.bestDriverByQualStarts[0]?.amount === item.qual
          ? "cell-1"
          : "" + driversStats.bestDriverByQualStarts[1]?.amount === item.qual
          ? "cell-2"
          : "" + driversStats.bestDriverByQualStarts[2]?.amount === item.qual
          ? "cell-3"
          : "";

      const winsClassName =
        driversStats.bestDriverByWins[0]?.amount === item.wins
          ? "cell-1"
          : "" + driversStats.bestDriverByWins[1]?.amount === item.wins
          ? "cell-2"
          : "" + driversStats.bestDriverByWins[2]?.amount === item.wins
          ? "cell-3"
          : "";

      const secondClassName =
        driversStats.bestDriverBySecondPlaces[0]?.amount === item.second
          ? "cell-1"
          : "" + driversStats.bestDriverBySecondPlaces[1]?.amount ===
            item.second
          ? "cell-2"
          : "" + driversStats.bestDriverBySecondPlaces[2]?.amount ===
            item.second
          ? "cell-3"
          : "";

      const thirdClassName =
        driversStats.bestDriverByThirdPlaces[0]?.amount === item.third
          ? "cell-1"
          : "" + driversStats.bestDriverByThirdPlaces[1]?.amount === item.third
          ? "cell-2"
          : "" + driversStats.bestDriverByThirdPlaces[2]?.amount === item.third
          ? "cell-3"
          : "";

      const podiumClassName =
        driversStats.bestDriverByPodium[0]?.amount === item.podium
          ? "cell-1"
          : "" + driversStats.bestDriverByPodium[1]?.amount === item.podium
          ? "cell-2"
          : "" + driversStats.bestDriverByPodium[2]?.amount === item.podium
          ? "cell-3"
          : "";

      const poleposClassName =
        driversStats.bestDriverByPolepos[0]?.amount === item.polepos
          ? "cell-1"
          : "" + driversStats.bestDriverByPolepos[1]?.amount === item.polepos
          ? "cell-2"
          : "" + driversStats.bestDriverByPolepos[2]?.amount === item.polepos
          ? "cell-3"
          : "";

      const bestlapsClassName =
        driversStats.bestDriverByBestlaps[0]?.amount === item.bestlaps
          ? "cell-1"
          : "" + driversStats.bestDriverByBestlaps[1]?.amount === item.bestlaps
          ? "cell-2"
          : "" + driversStats.bestDriverByBestlaps[2]?.amount === item.bestlaps
          ? "cell-3"
          : "";

      const completedClassName =
        driversStats.bestDriverByCompleted[0]?.amount === item.completed
          ? "cell-1"
          : "" + driversStats.bestDriverByCompleted[1]?.amount ===
            item.completed
          ? "cell-2"
          : "" + driversStats.bestDriverByCompleted[2]?.amount ===
            item.completed
          ? "cell-3"
          : "";

      const incompleteClassName =
        driversStats.bestDriverByIncompleted[0]?.amount === item.incomplete
          ? "cell-1"
          : "" + driversStats.bestDriverByIncompleted[1]?.amount ===
            item.incomplete
          ? "cell-2"
          : "" + driversStats.bestDriverByIncompleted[2]?.amount ===
            item.incomplete
          ? "cell-3"
          : "";

      const disqClassName =
        driversStats.bestDriverByDisq[0]?.amount === item.disq
          ? "cell-1"
          : "" + driversStats.bestDriverByDisq[1]?.amount === item.disq
          ? "cell-2"
          : "" + driversStats.bestDriverByDisq[2]?.amount === item.disq
          ? "cell-3"
          : "";

      const picDriver = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/driver/${item.alias}`}>
                <Image
                  size="tiny"
                  src={picDriver}
                  alt={item.driver}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/drivers/driver_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="tiny">
                <Statistic.Value>
                  <NavLink
                    to={`/team-events/points/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : Math.round(item.points * 100) / 100}
                  </NavLink>
                </Statistic.Value>
                {item.points !== item.pointsClass && (
                  <Statistic.Label>
                    <small>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </small>
                  </Statistic.Label>
                )}
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}. <Flag name={item.country} />
                    <NavLink to={`/driver/${item.alias}`}>
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.seasonMin == item.seasonMax
                      ? item.seasonMax
                      : `${item.seasonMin} - ${item.seasonMax}`}
                  </Item.Description>
                  <Item.Description>
                    <Segment basic className="overflow">
                      <table className="basic-table">
                        <thead>
                          <tr>
                            <th>
                              <FormattedMessage id={"app.table.header.gp"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.race"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p1"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p2"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p3"} />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.podiums"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.polepos"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.compl"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.inc"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.disq"} />
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td
                              data-title="Grand Prix"
                              className={`${gpClassName}`}
                            >
                              {item.gp === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/gp/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.gp === null
                                    ? String.fromCharCode(160)
                                    : item.gp}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Kwalifikacje"
                              className={`${qualClassName}`}
                            >
                              {item.qual === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/qual/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.qual === null
                                    ? String.fromCharCode(160)
                                    : item.qual}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyścigi"
                              className={`${startsClassName}`}
                            >
                              {item.starts === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/starts/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.starts === null
                                    ? String.fromCharCode(160)
                                    : item.starts}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P1" className={`${winsClassName}`}>
                              {item.wins === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/wins/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.wins === null
                                    ? String.fromCharCode(160)
                                    : item.wins}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="P2"
                              className={`${secondClassName}`}
                            >
                              {item.second === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/second/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.second === null
                                    ? String.fromCharCode(160)
                                    : item.second}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P3" className={`${thirdClassName}`}>
                              {item.third === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/third/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.third === null
                                    ? String.fromCharCode(160)
                                    : item.third}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Podium"
                              className={`${podiumClassName}`}
                            >
                              {item.podium === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/podium/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.podium === null
                                    ? String.fromCharCode(160)
                                    : item.podium}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Pole Position"
                              className={`${poleposClassName}`}
                            >
                              {item.polepos === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/polepos/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.polepos === null
                                    ? String.fromCharCode(160)
                                    : item.polepos}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Naj. okrążenia"
                              className={`${bestlapsClassName}`}
                            >
                              {item.bestlaps === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/bestlaps/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.bestlaps === null
                                    ? String.fromCharCode(160)
                                    : item.bestlaps}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. ukończone"
                              className={`${completedClassName}`}
                            >
                              {item.completed === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/completed/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.completed === null
                                    ? String.fromCharCode(160)
                                    : item.completed}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. nieukończone"
                              className={`${incompleteClassName}`}
                            >
                              {item.incomplete === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/incomplete/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.incomplete === null
                                    ? String.fromCharCode(160)
                                    : item.incomplete}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Dyskwalifikacje"
                              className={`${disqClassName}`}
                            >
                              {item.disq === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/disq/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                                >
                                  {item.disq === null
                                    ? String.fromCharCode(160)
                                    : item.disq}
                                </NavLink>
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Segment>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderTeamGrandPrix() {
    const elements = [];
    const { props } = this;
    const { gp, gpStats } = props.team.data;
    const { alias } = props.team.data;

    const items = [...gp];
    switch (this.state.gpSort.direction) {
      case "descending":
        items.sort(
          (a, b) => b[this.state.gpSort.column] - a[this.state.gpSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) => a[this.state.gpSort.column] - b[this.state.gpSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName = gpStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        gpStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName = gpStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName = gpStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        gpStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        gpStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        gpStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        gpStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        gpStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      const completedClassName =
        gpStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        gpStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName = gpStats.disq?.amount === item.disq ? "cell-1" : "";

      const pointsClassName =
        gpStats.points?.amount === item.points ? "cell-1" : "";

      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Table.Row key={item.alias}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/gp-stats/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={gpPic}
                    alt={item.circuitAlias}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/gp-stats/${item.alias}`}>{item.name} </NavLink>
                </div>
                <div>
                  <small>
                    {item.circuits.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/gp/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/qual/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/starts/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/wins/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/second/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/third/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/podium/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/polepos/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/bestlaps/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/completed/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/incomplete/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/disq/${alias}/-/${item.alias}/-/-/-/-/-/1`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            <NavLink
              to={`/team-events/points/${alias}/-/${item.alias}/-/-/-/-/-/1`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : Math.round(item.points * 100) / 100}
              {item.points !== item.pointsClass && (
                <div>
                  <small>
                    {" ("}
                    {item.pointsClass === null
                      ? String.fromCharCode(160)
                      : Math.round(item.pointsClass * 100) / 100}
                    {")"}
                  </small>
                </div>
              )}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });

    return elements;
  }

  renderTeamGrandPrixMobile() {
    const elements = [];
    const { props } = this;
    const { gp, gpStats } = props.team.data;
    const { alias } = props.team.data;

    const gpSort = gp.sort((a, b) => b.starts - a.starts);
    gpSort.forEach((item, idx) => {
      const gpClassName = gpStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        gpStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName = gpStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName = gpStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        gpStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        gpStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        gpStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        gpStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        gpStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      const completedClassName =
        gpStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        gpStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName = gpStats.disq?.amount === item.disq ? "cell-1" : "";

      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/gp-stats/${item.alias}`}>
                <Image
                  size="tiny"
                  src={gpPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="tiny">
                <Statistic.Value>
                  <NavLink
                    to={`/team-events/points/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : Math.round(item.points * 100) / 100}
                  </NavLink>
                </Statistic.Value>
                <Statistic.Label>
                  {item.points !== item.pointsClass && (
                    <div>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </div>
                  )}
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/gp-stats/${item.alias}`}>
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.circuits.split(", ").map((e) => (
                      <div key={e} className="team-grand-prix-circuits">
                        {e}
                      </div>
                    ))}
                  </Item.Description>
                  <Item.Description>
                    <Segment basic className="overflow">
                      <table className="basic-table">
                        <thead>
                          <tr>
                            <th>
                              <FormattedMessage id={"app.table.header.gp"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.race"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p1"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p2"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p3"} />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.podiums"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.polepos"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.compl"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.inc"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.disq"} />
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td
                              data-title="Grand Prix"
                              className={`${gpClassName}`}
                            >
                              {item.gp === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/gp/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.gp === null
                                    ? String.fromCharCode(160)
                                    : item.gp}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Kwalifikacje"
                              className={`${qualClassName}`}
                            >
                              {item.qual === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/qual/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.qual === null
                                    ? String.fromCharCode(160)
                                    : item.qual}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyścig"
                              className={`${startsClassName}`}
                            >
                              {item.starts === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/starts/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.starts === null
                                    ? String.fromCharCode(160)
                                    : item.starts}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P1" className={`${winsClassName}`}>
                              {item.wins === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/wins/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.wins === null
                                    ? String.fromCharCode(160)
                                    : item.wins}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="P2"
                              className={`${secondClassName}`}
                            >
                              {item.second === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/second/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.second === null
                                    ? String.fromCharCode(160)
                                    : item.second}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P3" className={`${thirdClassName}`}>
                              {item.third === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/third/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.third === null
                                    ? String.fromCharCode(160)
                                    : item.third}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Podium"
                              className={`${podiumClassName}`}
                            >
                              {item.podium === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/podium/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.podium === null
                                    ? String.fromCharCode(160)
                                    : item.podium}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Pole Position"
                              className={`${poleposClassName}`}
                            >
                              {item.polepos === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/polepos/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.polepos === null
                                    ? String.fromCharCode(160)
                                    : item.polepos}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Naj. okrążenia"
                              className={`${bestlapsClassName}`}
                            >
                              {item.bestlaps === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/bestlaps/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.bestlaps === null
                                    ? String.fromCharCode(160)
                                    : item.bestlaps}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. ukończone"
                              className={`${completedClassName}`}
                            >
                              {item.completed === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/completed/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.completed === null
                                    ? String.fromCharCode(160)
                                    : item.completed}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. nieukończone"
                              className={`${incompleteClassName}`}
                            >
                              {item.incomplete === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/incomplete/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.incomplete === null
                                    ? String.fromCharCode(160)
                                    : item.incomplete}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Dyskwalifikacje"
                              className={`${disqClassName}`}
                            >
                              {item.disq === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/disq/${alias}/-/${item.alias}/-/-/-/-/-/1`}
                                >
                                  {item.disq === null
                                    ? String.fromCharCode(160)
                                    : item.disq}
                                </NavLink>
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Segment>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderTeamCircuits() {
    const elements = [];
    const { props } = this;
    const { circuits, circuitsStats } = props.team.data;
    const { alias } = props.team.data;

    const items = [...circuits];
    switch (this.state.circuitSort.direction) {
      case "descending":
        items.sort(
          (a, b) =>
            b[this.state.circuitSort.column] - a[this.state.circuitSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) =>
            a[this.state.circuitSort.column] - b[this.state.circuitSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName = circuitsStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        circuitsStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName =
        circuitsStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName =
        circuitsStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        circuitsStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        circuitsStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        circuitsStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        circuitsStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        circuitsStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      const completedClassName =
        circuitsStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        circuitsStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName =
        circuitsStats.disq?.amount === item.disq ? "cell-1" : "";

      const pointsClassName =
        circuitsStats.points?.amount === item.points ? "cell-1" : "";

      const gpArray = item.grandPrix.split(",");
      const circuitPic = `/build/images/circuits/circuit_${item.alias.toLowerCase()}_profile.jpg`;
      const element = (
        <Table.Row key={item.alias}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Tor" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/circuit/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={circuitPic}
                    alt={item.circuitAlias}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/circuit/${item.alias}`}>{item.name} </NavLink>
                </div>
                <div>
                  <small>
                    {item.grandPrix.split(", ").map((e, idx) => (
                      <div key={e}>{gpArray[idx]}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/gp/${alias}/-/-/${item.alias}/-/-/-/-/1/`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/qual/${alias}/-/-/${item.alias}/-/-/-/-/1/`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/starts/${alias}/-/-/${item.alias}/-/-/-/-/1/`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/wins/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/second/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/third/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/podium/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/polepos/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/bestlaps/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/completed/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/incomplete/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/team-events/disq/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            <NavLink
              to={`/team-events/points/${alias}/-/-/${item.alias}/-/-/-/-/1`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : Math.round(item.points * 100) / 100}
              {item.points !== item.pointsClass && (
                <div>
                  <small>
                    {" ("}
                    {item.pointsClass === null
                      ? String.fromCharCode(160)
                      : Math.round(item.pointsClass * 100) / 100}
                    {")"}
                  </small>
                </div>
              )}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderTeamCircuitsMobile() {
    const elements = [];
    const { props } = this;
    const { circuits, circuitsStats } = props.team.data;
    const { alias } = props.team.data;

    const circuitsSort = circuits.sort((a, b) => b.starts - a.starts);
    circuitsSort.forEach((item, idx) => {
      const gpClassName = circuitsStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        circuitsStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName =
        circuitsStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName =
        circuitsStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        circuitsStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        circuitsStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        circuitsStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        circuitsStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        circuitsStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      const completedClassName =
        circuitsStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        circuitsStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName =
        circuitsStats.disq?.amount === item.disq ? "cell-1" : "";

      const circuitPic = `/build/images/circuits/circuit_${item.alias.toLowerCase()}_profile.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/circuit/${item.alias}`}>
                <Image
                  size="tiny"
                  src={circuitPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="tiny">
                <Statistic.Value>
                  <NavLink
                    to={`/team-events/points/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : Math.round(item.points * 100) / 100}
                  </NavLink>
                </Statistic.Value>
                <Statistic.Label>
                  {item.points !== item.pointsClass && (
                    <div>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </div>
                  )}
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/circuit/${item.alias}`}>{item.name}</NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.grandPrix.split(", ").map((e) => (
                      <div key={e} className="driver-circuits-grand-prix">
                        {e}
                      </div>
                    ))}
                  </Item.Description>
                  <Item.Description>
                    <Segment basic className="overflow">
                      <table className="basic-table">
                        <thead>
                          <tr>
                            <th>
                              <FormattedMessage id={"app.table.header.gp"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.race"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p1"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p2"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.p3"} />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.podiums"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.polepos"}
                              />
                            </th>
                            <th>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.compl"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.inc"} />
                            </th>
                            <th>
                              <FormattedMessage id={"app.table.header.disq"} />
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td
                              data-title="Grand Prix"
                              className={`${gpClassName}`}
                            >
                              {item.gp === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/gp/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.gp === null
                                    ? String.fromCharCode(160)
                                    : item.gp}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Kwalifikacje"
                              className={`${qualClassName}`}
                            >
                              {item.qual === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/qual/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.qual === null
                                    ? String.fromCharCode(160)
                                    : item.qual}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyścig"
                              className={`${startsClassName}`}
                            >
                              {item.starts === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/starts/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.starts === null
                                    ? String.fromCharCode(160)
                                    : item.starts}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P1" className={`${winsClassName}`}>
                              {item.wins === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/wins/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.wins === null
                                    ? String.fromCharCode(160)
                                    : item.wins}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="P2"
                              className={`${secondClassName}`}
                            >
                              {item.second === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/second/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.second === null
                                    ? String.fromCharCode(160)
                                    : item.second}
                                </NavLink>
                              )}
                            </td>
                            <td data-title="P3" className={`${thirdClassName}`}>
                              {item.third === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/third/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.third === null
                                    ? String.fromCharCode(160)
                                    : item.third}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Podium"
                              className={`${podiumClassName}`}
                            >
                              {item.podium === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/podium/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.podium === null
                                    ? String.fromCharCode(160)
                                    : item.podium}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Pole Position"
                              className={`${poleposClassName}`}
                            >
                              {item.polepos === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/polepos/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.polepos === null
                                    ? String.fromCharCode(160)
                                    : item.polepos}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Naj. okrążenia"
                              className={`${bestlapsClassName}`}
                            >
                              {item.bestlaps === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/bestlaps/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.bestlaps === null
                                    ? String.fromCharCode(160)
                                    : item.bestlaps}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. ukończone"
                              className={`${completedClassName}`}
                            >
                              {item.completed === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/completed/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.completed === null
                                    ? String.fromCharCode(160)
                                    : item.completed}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Wyś. nieukończone"
                              className={`${incompleteClassName}`}
                            >
                              {item.incomplete === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/incomplete/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.incomplete === null
                                    ? String.fromCharCode(160)
                                    : item.incomplete}
                                </NavLink>
                              )}
                            </td>
                            <td
                              data-title="Dyskwalifikacje"
                              className={`${disqClassName}`}
                            >
                              {item.disq === 0 ? (
                                0
                              ) : (
                                <NavLink
                                  to={`/team-events/disq/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                                >
                                  {item.disq === null
                                    ? String.fromCharCode(160)
                                    : item.disq}
                                </NavLink>
                              )}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Segment>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderCountriesFlags(countries) {
    const items = [];
    countries.forEach((country) => {
      const record = <Flag key={country} name={country} />;
      items.push(record);
    });
    return items;
  }

  renderCurrentDrivers() {
    const items = [];
    const { props } = this;
    const { currentDrivers } = props.team.data;

    currentDrivers.forEach((driver) => {
      let picDriver = `/build/images/drivers/driver_no_profile.jpg`;
      if (driver) {
        picDriver = `/build/images/drivers/driver_${driver.id}_profile.jpg`;
      }
      const record = (
        <div key={driver.id}>
          <Divider hidden fitted></Divider>
          <Grid centered>
            <Grid.Column mobile={8} tablet={16} computer={16}>
              <NavLink to={`/driver/${driver?.alias}`}>
                <Image
                  centered
                  src={picDriver}
                  className="team-details-photo"
                />
              </NavLink>
            </Grid.Column>
            <Grid.Column mobile={8} tablet={16} computer={16}>
              <Segment padded basic>
                <Header size="large" textAlign="center">
                  <NavLink to={`/driver/${driver?.alias}`}>
                    {driver?.driver}
                  </NavLink>
                  <Header.Subheader>
                    <Flag name={driver?.countryCode} />
                    {driver?.country}
                  </Header.Subheader>
                </Header>
              </Segment>
              <Segment basic textAlign="center">
                <Statistic>
                  <Statistic.Value>
                    {driver?.place}
                    {"./"}
                    {driver?.points}
                    <small className="very">{" pkt."}</small>
                  </Statistic.Value>
                  <Statistic.Label>
                    <FormattedMessage id={"app.stats.season"} />{" "}
                    {driver?.season}
                  </Statistic.Label>
                </Statistic>
              </Segment>
              <div className="buttons">
                <NavLink
                  className="secondary"
                  to={`/driver-events/starts/${driver?.alias}`}
                >
                  Statystyki
                </NavLink>
              </div>
            </Grid.Column>
          </Grid>
        </div>
      );
      items.push(record);
    });
    return items;
  }

  renderTeamDriversStats() {
    const { props } = this;
    const { alias, driversStats } = props.team.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {driversStats.bestDriverByWins[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/wins/${alias}/${driversStats.bestDriverByWins[0].alias}/-/-/-/-/-/-/1`}
                >
                  {driversStats.bestDriverByWins[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {driversStats.bestDriverByWins[0] == null ? (
                "-"
              ) : (
                <NavLink
                  to={`/gp-stats/${driversStats.bestDriverByWins[0].alias}`}
                >
                  {driversStats.bestDriverByWins[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {driversStats.bestDriverByPoints[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/points/${alias}/${driversStats.bestDriverByPoints[0].alias}/-/-/-/-/-/-/1`}
                >
                  {Math.round(driversStats.bestDriverByPoints[0].amount * 100) /
                    100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {driversStats.bestDriverByPoints[0] == null ? (
                "-"
              ) : (
                <NavLink
                  to={`/gp-stats/${driversStats.bestDriverByPoints[0].alias}`}
                >
                  {driversStats.bestDriverByPoints[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {driversStats.bestDriverByPodium[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/podium/${alias}/${driversStats.bestDriverByPodium[0].alias}/-/-/-/-/-/-/1`}
                >
                  {driversStats.bestDriverByPodium[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {driversStats.bestDriverByPodium[0] == null ? (
                "-"
              ) : (
                <NavLink
                  to={`/gp-stats/${driversStats.bestDriverByPodium[0].alias}`}
                >
                  {driversStats.bestDriverByPodium[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {driversStats.bestDriverByPolepos[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/polepos/${alias}/${driversStats.bestDriverByPolepos[0].alias}/-/-/-/-/-/-/1`}
                >
                  {driversStats.bestDriverByPolepos[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {driversStats.bestDriverByPolepos[0] == null ? (
                "-"
              ) : (
                <NavLink
                  to={`/gp-stats/${driversStats.bestDriverByPolepos[0].alias}`}
                >
                  {driversStats.bestDriverByPolepos[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {driversStats.bestDriverByBestlaps[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/bestlaps/${alias}/${driversStats.bestDriverByBestlaps[0].alias}/-/-/-/-/-/-/1`}
                >
                  {driversStats.bestDriverByBestlaps[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {driversStats.bestDriverByBestlaps[0] == null ? (
                "-"
              ) : (
                <NavLink
                  to={`/gp-stats/${driversStats.bestDriverByBestlaps[0].alias}`}
                >
                  {driversStats.bestDriverByBestlaps[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderGPStats() {
    const { props } = this;
    const { alias, gpStats } = props.team.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {gpStats.wins == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/wins/${alias}/-/${gpStats.wins.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.wins.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {gpStats.wins == 0 ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.wins.alias}`}>
                  {gpStats.wins.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {gpStats.points == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/points/${alias}/-/-/${gpStats.points.alias}/-/-/-/-/-/1`}
                >
                  {Math.round(gpStats.points.amount * 100) / 100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {gpStats.points == 0 ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.points.alias}`}>
                  {gpStats.points.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {gpStats.podium == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/podium/${alias}/-/-/${gpStats.podium.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.podium.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {gpStats.podium == 0 ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.podium.alias}`}>
                  {gpStats.podium.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {gpStats.polepos == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/polepos/${alias}/-/-/${gpStats.polepos.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.polepos.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {gpStats.polepos == 0 ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.polepos.alias}`}>
                  {gpStats.polepos.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {gpStats.bestlaps == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/bestlaps/${alias}/-/-/${gpStats.bestlaps.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.bestlaps.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {gpStats.bestlaps == 0 ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.bestlaps.alias}`}>
                  {gpStats.bestlaps.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderCircuitsStats() {
    const { props } = this;
    const { alias, circuitsStats } = props.team.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {circuitsStats.wins == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/wins/${alias}/-/-/${circuitsStats.wins.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.wins.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {circuitsStats.wins == 0 ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.wins.alias}`}>
                  {circuitsStats.wins.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {circuitsStats.points == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/points/${alias}/-/-/${circuitsStats.points.alias}/-/-/-/-/1`}
                >
                  {Math.round(circuitsStats.points.amount * 100) / 100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {circuitsStats.points == 0 ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.points.alias}`}>
                  {circuitsStats.points.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {circuitsStats.podium == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/podium/${alias}/-/-/${circuitsStats.podium.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.podium.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {circuitsStats.podium == 0 ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.podium.alias}`}>
                  {circuitsStats.podium.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {circuitsStats.polepos == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/polepos/${alias}/-/-/${circuitsStats.polepos.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.polepos.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {circuitsStats.polepos == 0 ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.polepos.alias}`}>
                  {circuitsStats.polepos.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {circuitsStats.bestlaps == 0 ? (
                0
              ) : (
                <NavLink
                  to={`/team-events/bestlaps/${alias}/-/-/${circuitsStats.bestlaps.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.bestlaps.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {circuitsStats.bestlaps == 0 ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.bestlaps.alias}`}>
                  {circuitsStats.bestlaps.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderStats() {
    const { props } = this;
    const { alias, ranking, stats } = props.team.data;
    const {
      seasons,
      gp,
      wins,
      points,
      podium,
      polepos,
      bestlaps,
      starts,
      qual,
      sprints,
      completed,
      incomplete,
      pointPlaces,
      noPointPlaces,
      notQualified,
      notStarted,
      penalty,
      finished,
      retirement,
      disq,
    } = stats;

    const element = (
      <>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {seasons === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/gp/${alias}`}>{seasons}</NavLink>
                  <NavLink to="/teams-records/seasons/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.seasons}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.seasons"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {gp === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/gp/${alias}`}>{gp}</NavLink>
                  <NavLink to="/teams-records/gp/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.gp}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.gp"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {qual === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/qual/${alias}`}>{qual}</NavLink>
                  <NavLink to="/teams-records/qual/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.qual}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.qual"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {starts === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/starts/${alias}`}>
                    {starts}
                  </NavLink>
                  <NavLink to="/teams-records/starts/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.starts}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {sprints === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/sprints/${alias}`}>
                    {sprints}
                  </NavLink>
                  <NavLink to="/teams-records/sprints/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.sprints}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.sprints"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {wins === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/wins/${alias}`}>{wins}</NavLink>
                  <NavLink to="/teams-records/wins/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.wins}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {points === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/points/${alias}`}>
                    {+(Math.round(points * 100) / 100)}
                  </NavLink>
                  <NavLink to="/teams-records/points/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.points}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {podium === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/podium/${alias}`}>
                    {podium}
                  </NavLink>
                  <NavLink to="/teams-records/podium/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.podium}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {polepos === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/polepos/${alias}`}>
                    {polepos}
                  </NavLink>
                  <NavLink to="/teams-records/polepos/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.polepos}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {bestlaps === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/bestlaps/${alias}`}>
                    {bestlaps}
                  </NavLink>
                  <NavLink to="/teams-records/bestlaps/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.bestlaps}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {pointPlaces === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/points-places/${alias}`}>
                    {pointPlaces}
                  </NavLink>
                  <NavLink to="/teams-records/points-places/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.pointPlaces}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {noPointPlaces === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/no-points-places/${alias}`}>
                    {noPointPlaces}
                  </NavLink>
                  <NavLink to="/teams-records/no-points-places/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.noPointPlaces}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.no.points.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {notQualified === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/not-qualified/${alias}`}>
                    {notQualified}
                  </NavLink>
                  <NavLink to="/teams-records/not-qualified/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.notQualified}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.not.qualified"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {notStarted === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/not-started/${alias}`}>
                    {notStarted}
                  </NavLink>
                  <NavLink to="/teams-records/not-started/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.notStarted}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.not.started"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {penalty === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/penalty/${alias}`}>
                    {penalty}
                  </NavLink>
                  <NavLink to="/teams-records/penalty/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.penalty}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.penalty"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {completed === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/completed/${alias}`}>
                    {completed}
                  </NavLink>
                  <NavLink to="/teams-records/completed/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.completed}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.completed"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {incomplete === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/incomplete/${alias}`}>
                    {incomplete}
                  </NavLink>
                  <NavLink to="/teams-records/incomplete/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.incomplete}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.incomplete"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {finished === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/finished/${alias}`}>
                    {finished}
                  </NavLink>
                  <NavLink to="/teams-records/finished/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.finished}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.finished"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {retirement === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/retirement/${alias}`}>
                    {retirement}
                  </NavLink>
                  <NavLink to="/teams-records/retirement/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.retirement}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.retirement"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {disq === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/team-events/disq/${alias}`}>{disq}</NavLink>
                  <NavLink to="/teams-records/disq/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.disq}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.disq"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderDriversTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="team-drivers" id="team-drivers">
          <div className="hideForMobile">
            <Grid>
              <Grid.Row textAlign="center" columns={5} only="tablet computer">
                {this.renderTeamDriversStats()}
              </Grid.Row>
            </Grid>
          </div>
          <div className="hideForDesktop">
            <Grid columns={3} divided="vertically">
              {this.renderTeamDriversMobile()}
            </Grid>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.driver"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.driver"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "gp"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "qual"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "starts"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "wins"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "second"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "third"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "podium"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "polepos"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "bestlaps"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "completed"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "incomplete"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "disq"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.driversSort.column === "points"
                          ? state.driversSort.direction
                          : null
                      }
                      onClick={() => this.onDriversSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderTeamDrivers()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  renderGPTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="team-grandprix">
          <div className="hideForMobile">
            <Grid>
              <Grid.Row textAlign="center" columns={5} only="tablet computer">
                {this.renderGPStats()}
              </Grid.Row>
            </Grid>
          </div>
          <div className="hideForDesktop">
            <Grid columns={3} divided="vertically">
              {this.renderTeamGrandPrixMobile()}
            </Grid>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp.long"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "gp"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "qual"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "starts"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "wins"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "second"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "third"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "podium"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "polepos"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "bestlaps"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "completed"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "incomplete"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "disq"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "points"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderTeamGrandPrix()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  renderCircuitsTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="team-circuits">
          <div className="hideForMobile">
            <Grid>
              <Grid.Row textAlign="center" columns={5} only="tablet computer">
                {this.renderCircuitsStats()}
              </Grid.Row>
            </Grid>
          </div>
          <div className="hideForDesktop">
            <Grid columns={3} divided="vertically">
              {this.renderTeamCircuitsMobile()}
            </Grid>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.circuit"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.circuit"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "gp"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "qual"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "starts"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "wins"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "second"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "third"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "podium"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "polepos"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "bestlaps"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "completed"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "incomplete"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "disq"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "points"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderTeamCircuits()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.team.data || props.team.loading) {
      return (
        <section id="team-details" name="team-details" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const {
      name,
      id,
      alias,
      prevId,
      nextId,
      country,
      countryCode,
      countryShort,
      engineNames,
      seasons,
      drivers,
      gp,
      circuits,
      stats,
      career,
      currentDrivers,
      gpStats,
      driversStats,
      internet,
    } = props.team.data;
    const { starts, qual } = stats;
    const {
      bestResult,
      bestResultSeason,
      champCount,
      firstGP,
      lastGP,
      firstWin,
      lastWin,
      firstPodium,
      lastPodium,
      firstQual,
      lastQual,
      firstPP,
      lastPP,
      firstBL,
      lastBL,
      gpPlacesTotal,
      ppPlacesTotal,
      gpPlacesBySeasons,
      gpPointsBySeasons,
      gpPointsClassBySeasons,
    } = career;
    const {
      bestDriverByWins,
      bestDriverByPodium,
      bestDriverByPolepos,
      bestDriverByBestlaps,
      bestDriverByPoints,
      bestDriverByStarts,
      bestDriverByQualStarts,
      bestDriverByCompleted,
    } = driversStats;

    const countries = countryCode.split(",");
    const picProfile = `/build/images/teams/team_${id}_profile.jpg`;
    const picLogo = `/build/images/teams/team_${id}_profile_logo.jpg`;

    let linkPrev = "";
    let linkNext = "";
    if (props.match.params.year) {
      linkPrev = `/team/${props.match.params.year}/${prevId}`;
      linkNext = `/team/${props.match.params.year}/${nextId}`;
    } else {
      linkPrev = `/team/${prevId}`;
      linkNext = `/team/${nextId}`;
    }

    const gpPlacesData = [];
    gpPlacesTotal?.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return gpPlacesData.push(value);
    });

    const ppPlacesData = [];
    ppPlacesTotal?.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return ppPlacesData.push(value);
    });

    const configPlacesAllSeasons = {
      chart: {
        height: 300,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>{series.name}</b></br>" +
          formatMessage({ id: "app.stats.place" }) +
          " {point.key}: <b>{point.y:f}</b>",
        pointFormat: "",
        footerFormat: "",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              if (!event.point.name.startsWith("<")) {
                if (event.point.series.name == "Pola startowe") {
                  props.history.push({
                    pathname: `/team-events/qual-places/${alias}/-/-/-/-/-/-/-/${event.point.name}/`,
                  });
                } else {
                  props.history.push({
                    pathname: `/team-events/race-places/${alias}/-/-/-/-/-/-/-/${event.point.name}/`,
                  });
                }
              }
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.race" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: gpPlacesData,
        },
      ],
    };

    const configPolePosAllSeasons = {
      chart: {
        height: 300,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 10,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>{series.name}</b></br>" +
          formatMessage({ id: "app.stats.place" }) +
          " {point.key}: <b>{point.y:f}</b>",
        pointFormat: "",
        footerFormat: "",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              if (!event.point.name.startsWith("<")) {
                props.history.push({
                  pathname: `/team-events/grid-places/${alias}/-/-/-/-/-/-/-/${event.point.name}/`,
                });
              }
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.grid" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: ppPlacesData,
        },
      ],
    };

    const gpPlacesBySeasonsData = [];
    gpPlacesBySeasons?.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return gpPlacesBySeasonsData.push(value);
    });

    const gpPointsClassBySeasonsData = [];
    gpPointsClassBySeasons?.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseFloat(item.y, 0);
      return gpPointsClassBySeasonsData.push(value);
    });

    const gpPointsBySeasonsData = [];
    gpPointsBySeasons?.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseFloat(item.y, 0);
      return gpPointsBySeasonsData.push(value);
    });

    const configPlacesBySeasons = {
      chart: {
        type: "line",
        height: 300,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: [
        {
          title: {
            text: "",
          },
          min: 0,
          allowDecimals: false,
          tickInterval: 1,
          reversed: true,
        },
      ],
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>" +
          formatMessage({ id: "app.stats.season" }) +
          ": {point.key}</b><br/>",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true,
          },
        },
        series: {
          label: {
            enabled: false,
          },
        },
      },
      series: [
        {
          type: "line",
          name: formatMessage({ id: "app.stats.place" }),
          color: "#000000",
          data: gpPlacesBySeasonsData,
        },
      ],
    };

    const configPointsBySeasons = {
      chart: {
        height: 300,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: [
        {
          title: {
            text: "",
          },
          opposite: false,
          min: 0,
          allowDecimals: false,
        },
      ],
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>" +
          formatMessage({ id: "app.stats.season" }) +
          ": {point.key}</b><br/>",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        column: {
          grouping: false,
        },
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              props.history.push({
                pathname: `/teams-records/points/-/-/${event.point.name}/${event.point.name}/0/1/`,
              });
            },
          },
        },
      },
      series: [
        {
          yAxis: 0,
          type: "column",
          name: formatMessage({ id: "app.stats.points.class" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: gpPointsClassBySeasonsData,
          stack: "Punkty",
          dataLabels: {
            enabled: true,
          },
        },
      ],
    };

    const panes = [
      {
        menuItem: formatMessage(
          { id: "app.page.team.stats.tab.drivers" },
          { count: drivers.length }
        ),
        render: () => this.renderDriversTab(),
      },
      {
        menuItem: formatMessage(
          { id: "app.page.team.stats.tab.gp" },
          { count: gp.length }
        ),
        render: () => this.renderGPTab(),
      },
      {
        menuItem: formatMessage(
          { id: "app.page.team.stats.tab.circuits" },
          { count: circuits.length }
        ),
        render: () => this.renderCircuitsTab(),
      },
    ];

    return (
      <section id="team-details" name="team-details" className="section-page">
        <SectionPageBanner
          title={name}
          subtitle={<FormattedMessage id={"app.page.team.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                {!props.match.params.year && (
                  <Segment basic padded>
                    <Select
                      ref={(ref) => {
                        this.comboTeam = ref;
                      }}
                      isLoading={isLoadingExternally}
                      name="drivers"
                      multi={multi}
                      ignoreCase={ignoreCase}
                      ignoreAccents={ignoreAccents}
                      value={state.selectValue}
                      onChange={this.onInputChange}
                      options={props.comboTeams.data}
                      clearable={clearable}
                      labelKey="label"
                      valueKey="value"
                      placeholder={
                        <FormattedMessage id={"app.placeholder.select.team"} />
                      }
                      noResultsText={
                        <FormattedMessage id={"app.placeholder.no.results"} />
                      }
                      className="react-select-container"
                      classNamePrefix="react-select"
                    />
                  </Segment>
                )}
                <div>
                  <Grid centered>
                    <Grid.Column mobile={8} tablet={16} computer={16}>
                      <Segment basic textAlign="center">
                        <Image
                          centered
                          src={picProfile}
                          alt={name}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_profile.jpg";
                          }}
                        />
                        {champCount > 0 && (
                          <Label attached="bottom" color="red" size="large">
                            {champCount}x{" "}
                            <FormattedMessage id={"app.stats.wc"} />
                          </Label>
                        )}
                      </Segment>
                    </Grid.Column>
                    <Grid.Column
                      mobile={8}
                      tablet={16}
                      computer={16}
                      verticalAlign="middle"
                    >
                      <Image
                        centered
                        src={picLogo}
                        alt={countryShort}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/countries/country_no_profile_logo.jpg";
                        }}
                      />
                    </Grid.Column>
                  </Grid>
                  <div className="stats-box">
                    {props.match.params.year && (
                      <>
                        <Segment basic textAlign="center">
                          <Divider hidden fitted></Divider>
                          {seasons.length > 0 ? (
                            <>
                              <Statistic size="large" inverted>
                                <Statistic.Value>
                                  {seasons[0].place}
                                  {"./"}
                                  {seasons[0].points}{" "}
                                  <small className="very">
                                    <FormattedMessage id={"app.stats.pts"} />
                                  </small>
                                </Statistic.Value>
                                <Statistic.Label>
                                  <FormattedMessage id={"app.stats.season"} />{" "}
                                  {seasons[0]?.season}
                                </Statistic.Label>
                              </Statistic>
                            </>
                          ) : (
                            <Header size="large" textAlign="center">
                              <FormattedMessage id={"app.stats.debut"} />
                            </Header>
                          )}
                        </Segment>
                        <Divider></Divider>
                      </>
                    )}
                    {seasons.length > 0 ? (
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>
                            {bestResult == "MŚ" ? (
                              <>
                                {champCount}
                                <small className="very">x</small> {bestResult}
                              </>
                            ) : (
                              <>
                                {bestResult ? (
                                  <>
                                    {bestResult}
                                    <small className="very">
                                      ({bestResultSeason})
                                    </small>
                                  </>
                                ) : (
                                  <FormattedMessage id={"app.stats.nc"} />
                                )}
                              </>
                            )}
                          </Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.best.result"} />
                          </Statistic.Label>
                        </Statistic>
                        <div className="buttons">
                          <NavLink
                            className="primary"
                            to={`/team-events/starts/${alias}`}
                          >
                            <FormattedMessage id={"app.button.history"} />
                          </NavLink>{" "}
                          <NavLink
                            className="secondary"
                            to={`/team-models/${alias}`}
                          >
                            <FormattedMessage id={"app.button.models"} />
                          </NavLink>
                        </div>
                      </Segment>
                    ) : (
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>-</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.best.result"} />
                          </Statistic.Label>
                        </Statistic>
                      </Segment>
                    )}
                    {currentDrivers.length > 0 && this.renderCurrentDrivers()}
                  </div>
                </div>
                <div className="info-box">
                  <Segment padded basic>
                    <Item.Group divided>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.country"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              {this.renderCountriesFlags(countries)}
                              {country}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.webpage"} />
                          </Item.Header>
                          <Item.Meta>
                            {internet ? (
                              <a
                                href={`http://${internet}`}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {internet}
                              </a>
                            ) : (
                              "-"
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.f1.sesons"} />
                          </Item.Header>
                          <Item.Meta>{career.seasons}</Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Item.Header>
                          <Item.Meta>{seasons.length}</Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.engines"} />
                          </Item.Header>
                          <Item.Meta>
                            {engineNames?.split(",").length} - {engineNames}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstGP.id == null && "-"}
                            {firstGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstGP.alias}/${firstGP.year}`}
                                >
                                  {firstGP.year}
                                  {" - "}
                                  {firstGP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${firstGP.driverAlias}`}>
                                  {firstGP.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastGP.id == null && "-"}
                            {lastGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastGP.alias}/${lastGP.year}`}
                                >
                                  {lastGP.year}
                                  {" - "}
                                  {lastGP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${lastGP.driverAlias}`}>
                                  {lastGP.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.win"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstWin.id == null && "-"}
                            {firstWin.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstWin.alias}/${firstWin.year}`}
                                >
                                  {firstWin.year}
                                  {" - "}
                                  {firstWin.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${firstWin.driverAlias}`}>
                                  {firstWin.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.win"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastWin.id == null && "-"}
                            {lastWin.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastWin.alias}/${lastWin.year}`}
                                >
                                  {lastWin.year}
                                  {" - "}
                                  {lastWin.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${lastWin.driverAlias}`}>
                                  {lastWin.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.podium"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstPodium.id == null && "-"}
                            {firstPodium.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstPodium.alias}/${firstPodium.year}`}
                                >
                                  {firstPodium.year}
                                  {" - "}
                                  {firstPodium.gp}
                                </NavLink>
                                {" ("}
                                <NavLink
                                  to={`/driver/${firstPodium.driverAlias}`}
                                >
                                  {firstPodium.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.podium"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastPodium.id == null && "-"}
                            {lastPodium.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastPodium.alias}/${lastPodium.year}`}
                                >
                                  {lastPodium.year}
                                  {" - "}
                                  {lastPodium.gp}
                                </NavLink>
                                {" ("}
                                <NavLink
                                  to={`/driver/${lastPodium.driverAlias}`}
                                >
                                  {lastPodium.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstQual.id == null && "-"}
                            {firstQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstQual.alias}/${firstQual.year}`}
                                >
                                  {firstQual.year}
                                  {" - "}
                                  {firstQual.gp}
                                </NavLink>
                                {" ("}
                                <NavLink
                                  to={`/driver/${firstQual.driverAlias}`}
                                >
                                  {firstQual.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastQual.id == null && "-"}
                            {lastQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastQual.alias}/${lastQual.year}`}
                                >
                                  {lastQual.year}
                                  {" - "}
                                  {lastQual.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${lastQual.driverAlias}`}>
                                  {lastQual.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstPP.id == null && "-"}
                            {firstPP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstPP.alias}/${firstPP.year}`}
                                >
                                  {firstPP.year}
                                  {" - "}
                                  {firstPP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${firstPP.driverAlias}`}>
                                  {firstPP.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastPP.id == null && "-"}
                            {lastPP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastPP.alias}/${lastPP.year}`}
                                >
                                  {lastPP.year}
                                  {" - "}
                                  {lastPP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${lastPP.driverAlias}`}>
                                  {lastPP.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstBL.id == null && "-"}
                            {firstBL.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstBL.alias}/${firstBL.year}`}
                                >
                                  {firstBL.year}
                                  {" - "}
                                  {firstBL.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${firstBL.driverAlias}`}>
                                  {firstBL.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastBL.id == null && "-"}
                            {lastBL.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastBL.alias}/${lastBL.year}`}
                                >
                                  {lastBL.year}
                                  {" - "}
                                  {lastBL.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/driver/${lastBL.driverAlias}`}>
                                  {lastBL.driver}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                    </Item.Group>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment basic>
                  {starts === "0" && qual === "0" && (
                    <Message>
                      <Message.Header>
                        <FormattedMessage id={"app.message.header"} />
                      </Message.Header>
                      <p>
                        <FormattedMessage id={"app.page.team.info"} />
                      </p>
                    </Message>
                  )}
                  {(stats.starts > 0 || stats.qual > 0) && (
                    <Grid stackable centered>
                      <Grid.Column mobile={16} tablet={11} computer={12}>
                        <Segment padded basic>
                          <div className="team-stats">
                            <Grid>
                              <Grid.Row
                                textAlign="center"
                                columns={4}
                                only="mobile"
                              >
                                {this.renderStats()}
                              </Grid.Row>
                              <Grid.Row
                                textAlign="center"
                                columns={5}
                                only="tablet computer"
                              >
                                {this.renderStats()}
                              </Grid.Row>
                            </Grid>
                          </div>
                          <div className="team-seasons-summary">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.team.seasons.summary.header"}
                                  values={{ value: seasons.length }}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic className="overflow-mobile">
                              <Table basic="very" celled unstackable>
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.season"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.season"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.engine"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.engine.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.model"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.model.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={
                                                "app.table.header.driver.short"
                                              }
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.driver"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.gp"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.gp.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.qual"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.qual.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.race"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.race.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p1"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p1.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p2"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p2.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p3"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p3.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.podiums"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.podiums.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.polepos"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.polepos.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.bestlaps"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.bestlaps.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.compl"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.compl.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.inc"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.inc.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.points"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.points.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.place"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.place.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                  {this.renderTeamSeasonsSummary()}
                                </Table.Body>
                              </Table>
                            </Segment>
                            <Segment basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage
                                    id={"app.message.header.warning"}
                                  />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.team.seasons.summary.info"}
                                  />
                                </p>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.team.seasons.summary.info2"}
                                  />
                                </p>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.team.seasons.summary.info3"}
                                  />
                                </p>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.legend"} />
                                </Message.Header>
                                <p>
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.engine"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.engines"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.model"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.models"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.driver.short"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.drivers"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.gp"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage id={"app.stats.gp"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.race"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.races"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.qual"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.qual"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p1"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.wins"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p2"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage id={"app.stats.p2"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p3"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage id={"app.stats.p3"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.podiums"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.podium"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.polepos"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.bestlaps"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.bestlaps"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.compl"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.completed"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.inc"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.incomplete"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.points"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.points"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.place"}
                                    />
                                  </b>
                                  {" - "}
                                  <FormattedMessage
                                    id={"app.stats.place"}
                                  />{" "}
                                </p>
                              </Message>
                              <Divider hidden fitted></Divider>
                            </Segment>
                          </div>
                          <div className="team-season-details">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.team.season.details.header"}
                                  values={{
                                    season: state.selectSeason.value,
                                  }}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic textAlign="left" className="bold">
                              <Select
                                ref={(ref) => {
                                  this.comboSeason = ref;
                                }}
                                isLoading={isLoadingExternally}
                                name="seasons"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={state.selectSeason}
                                onChange={this.onSeasonChange}
                                options={seasons
                                  .map((s) => {
                                    return {
                                      value: s.season,
                                      label: s.season,
                                    };
                                  })
                                  .sort((a, b) => b.value - a.value)}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={
                                  <FormattedMessage
                                    id={"app.placeholder.select.season"}
                                  />
                                }
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container-short"
                                classNamePrefix="react-select"
                              />
                            </Segment>
                            <div className="hideForDesktop">
                              {this.renderTeamSeasonDetailsMobile()}
                            </div>
                            <div className="hideForMobile">
                              {this.renderTeamSeasonDetails()}
                            </div>
                          </div>
                          <SectionPageHeader
                            title="Statystyki"
                            type="secondary"
                          />
                          <Tab
                            menu={{ secondary: true, pointing: true }}
                            panes={panes}
                          />
                        </Segment>
                      </Grid.Column>
                      <Grid.Column mobile={16} tablet={5} computer={4}>
                        <div className="team-charts-all-seasons-content">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.team.charts.class.title"}
                              />
                            }
                          />
                          {career.bestResult ? (
                            <ReactHighcharts config={configPlacesBySeasons} />
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  {
                                    <FormattedMessage
                                      id={"app.page.team.charts.class.info"}
                                    />
                                  }
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={
                                  "app.page.team.charts.point.by.seasons.title"
                                }
                              />
                            }
                          />
                          {career.bestResult ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts config={configPointsBySeasons} />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  {
                                    <FormattedMessage
                                      id={
                                        "app.page.team.charts.point.by.seasons.info"
                                      }
                                    />
                                  }
                                </p>
                              </Message>
                            </Segment>
                          )}
                        </div>
                        <div className="team-charts-all-seasons-content">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.team.charts.places.title"}
                              />
                            }
                          />
                          {gpStats.completed ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts
                                config={configPlacesAllSeasons}
                              />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.team.charts.places.info"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.team.charts.grid.title"}
                              />
                            }
                          />
                          {ppPlacesTotal.length > 0 ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts
                                config={configPolePosAllSeasons}
                              />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.team.charts.grid.info"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                        </div>
                        <div className="team-drivers-stats">
                          <Segment basic>
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.team.team.stats.title"}
                                />
                              }
                            />
                            {bestDriverByStarts.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.races"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByStarts[0].driver} -{" "}
                                  {bestDriverByStarts[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByStarts[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByStarts[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByStarts}
                                  seriesName={formatMessage({
                                    id: "app.stats.races",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByQualStarts.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.qual"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByQualStarts[0].driver} -{" "}
                                  {bestDriverByQualStarts[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByQualStarts[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByQualStarts[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByQualStarts}
                                  seriesName={formatMessage({
                                    id: "app.stats.qual",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByPoints.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.points"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByPoints[0].driver} -{" "}
                                  {bestDriverByPoints[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByPoints[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByPoints[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByPoints}
                                  seriesName={formatMessage({
                                    id: "app.stats.points",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByWins.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.wins"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByWins[0].driver} -{" "}
                                  {bestDriverByWins[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByWins[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByWins[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByWins}
                                  seriesName={formatMessage({
                                    id: "app.stats.wins",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByPodium.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.podium"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByPodium[0].driver} -{" "}
                                  {bestDriverByPodium[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByPodium[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByPodium[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByPodium}
                                  seriesName={formatMessage({
                                    id: "app.stats.podium",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByPolepos.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.polepos"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByPolepos[0].driver} -{" "}
                                  {bestDriverByPolepos[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByPolepos[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByPolepos[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByPolepos}
                                  seriesName={formatMessage({
                                    id: "app.stats.polepos",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByBestlaps.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.bestlaps"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByBestlaps[0].driver} -{" "}
                                  {bestDriverByBestlaps[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByBestlaps[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByBestlaps[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByBestlaps}
                                  seriesName={formatMessage({
                                    id: "app.stats.bestlaps",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                            {bestDriverByCompleted.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.completed"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {bestDriverByCompleted[0].driver} -{" "}
                                  {bestDriverByCompleted[0].amount}
                                </Header>
                                <NavLink
                                  to={`/driver/${bestDriverByCompleted[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${bestDriverByCompleted[0].idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestDriverByCompleted}
                                  seriesName={formatMessage({
                                    id: "app.stats.completed",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            )}
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                  )}
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

Team.propTypes = {
  fetchTeam: PropTypes.func.isRequired,
  team: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboTeams: PropTypes.func.isRequired,
  comboTeams: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  teamId: PropTypes.string,
};

function mapStateToProps({ team, comboTeams }) {
  return { team, comboTeams };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTeam, fetchComboTeams }, dispatch);
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Team));
