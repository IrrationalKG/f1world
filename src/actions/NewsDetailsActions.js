// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_NEWS_DETAILS = "FETCH_NEWS_DETAILS";
export const FETCH_NEWS_DETAILS_SUCCESS = "FETCH_NEWS_DETAILS_SUCCESS";
export const FETCH_NEWS_DETAILS_ERROR = "FETCH_NEWS_DETAILS_ERROR";

function requestData() {
  return {
    type: FETCH_NEWS_DETAILS,
  };
}

function receiveData(data) {
  return {
    type: FETCH_NEWS_DETAILS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_NEWS_DETAILS_ERROR,
    payload: error,
  };
}

export function fetchNewsDetails(id, lang) {
  const url = `${ROOT_URL}/get_news_details.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
