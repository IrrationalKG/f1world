<?
header('Access-Control-Allow-Origin: *');

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// aktualny rok
$query="(SELECT max(season) maxYear FROM teams_class)";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}
$prevYear=$currentYear-1;

// pobranie kolejnosci zespołów
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name asc";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams group by team order by name asc";
}
$result = mysqli_query($dbhandle,$query);
$teamsTab = array();
$cnt=0;
$currentTeamCnt=0;
$maxTeamCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $teamsTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentTeamCnt = $cnt;
  }
  $cnt+=1;
}
$maxTeamCnt = $cnt;

// min pozycja
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name asc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams order by name asc limit 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name desc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams order by name desc limit 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzedni zespół
if (($currentTeamCnt-1)<0){
  $prevId=$max;
}else{
  $prevId = $teamsTab[$currentTeamCnt-1];
}
// nastepny zespół
if (($currentTeamCnt+1)>=$maxTeamCnt){
  $nextId=$min;
}else{
  $nextId = $teamsTab[$currentTeamCnt+1];
}

// pobranie id
$query="SELECT lower(team) id, alias_name, banner, name, fullname FROM teams WHERE alias_name='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
$banner;
$name;
$fullname;
$aliasName;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
  $banner = $r["banner"];
  $name = $r["name"];
  $fullname = $r["fullname"];
  $aliasName = $r["alias_name"];
}

// modele 
$query="SELECT teams_models.id_team_model, teams_models.team, teams.name, 
GROUP_CONCAT(DISTINCT teams.engine ORDER BY teams.engine SEPARATOR ', ') engine,
GROUP_CONCAT(DISTINCT teams_models.season ORDER BY teams_models.season SEPARATOR ', ') season,
teams_models.model, GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamName
FROM teams_models
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team_model = teams_models.id_team_model AND drivers_gp_involvements.season = teams_models.season
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE teams_models.team = '$id'
GROUP BY teams_models.model
ORDER BY teams_models.season DESC, teams_models.model DESC";
$result = mysqli_query($dbhandle,$query);
$teamModelsItems=array();
while($r = mysqli_fetch_assoc($result)) {
   $teamModelsItems[] = $r;
}
$teamItems["prevId"]=$prevId;
$teamItems["nextId"]=$nextId;
$teamItems["id"]=$id;
$teamItems["name"]=$name;
$teamItems["fullname"]=$fullname;
$teamItems["aliasName"]=$aliasName;
$teamItems["banner"]=$banner;
$teamItems["models"]=$teamModelsItems;
$teamItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $teamItems;

print json_encode($response);
mysqli_free_result($result);
?>
