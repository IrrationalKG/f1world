<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// grand prix
$start_time1 = microtime(true);
$query="SELECT gp.country_code name, gp.name gp, gp.name_short nameShort from gp,gp_season where gp.id_gp=gp_season.id_gp and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}
$classDrivers["createTime GP"]=microtime(true)-$start_time1;

// regulamin
$start_time2 = microtime(true);
if ($lang=='pl'){
  $query="SELECT regulations from season_regulations where year='$year'";
}else{
  $query="SELECT regulations_en as regulations from season_regulations where year='$year'";
}
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $regulations = $r;
}
$classDrivers["createTime Regulations"]=microtime(true)-$start_time2;

//zwyciezcy w sezonie
$start_time3 = microtime(true);
$query="SELECT id_driver,count(id_drivers_gp) wins from drivers_gp_results where race_date>='$year-01-01' and race_date<='$year-12-31' and race_pos=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversWinsTab[$tmp_id_driver] = $r["wins"];
}
$classDrivers["createTime Winners"]=microtime(true)-$start_time3;

//podium w sezonie
$start_time4 = microtime(true);
$query="SELECT id_driver,count(id_drivers_gp) podiums from drivers_gp_results where race_date>='$year-01-01' and race_date<='$year-12-31' and race_pos<4 and race_completed=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversPodiumsTab[$tmp_id_driver] = $r["podiums"];
}
$classDrivers["createTime Podium"]=microtime(true)-$start_time4;

//pole position w sezonie
$start_time5 = microtime(true);
$query="SELECT id_driver,count(id_starting_grid) pp from drivers_gp_starting_grid where season='$year' and grid_pos=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversPolePosTab[$tmp_id_driver] = $r["pp"];
}
$classDrivers["createTime PP"]=microtime(true)-$start_time5;

//best laps w sezonie
$start_time6 = microtime(true);
$query="SELECT id_driver,count(id_drivers_gp) bestlaps from drivers_gp_results where race_date>='$year-01-01' and race_date<='$year-12-31' and race_best_lap=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversBLTab[$tmp_id_driver] = $r["bestlaps"];
}
$classDrivers["createTime BL"]=microtime(true)-$start_time6;

/**
* Klasyfikacje
**/
// miejsca
$start_time7 = microtime(true);
$query="SELECT drivers.alias alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,
place, points, points_class pointsClass, is_classified isClassified, picture, '' wins, '' podium, '' pp, '' bestLap,
COALESCE((SELECT GROUP_CONCAT(DISTINCT CONCAT(name,' ',COALESCE(engine,'')) SEPARATOR ', ') FROM teams, drivers_gp_results
WHERE teams.id_team=drivers_gp_results.id_team AND drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.season=drivers_class.season), (SELECT CONCAT(name,' ',COALESCE(engine,'')) FROM teams WHERE teams.id_team=drivers_class.id_team)) team
from drivers_class,drivers where season='$year' and drivers.id_driver=drivers_class.id_driver 
order by place";
$result = mysqli_query($dbhandle,$query);
$placesItems=array();
$classDrivers["createTime Places"]=microtime(true)-$start_time7;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name_alias alias, gp.name gp,
  grid_pos grid, race_pos place, race_completed completed,race_time time,race_best_lap bestLap,race_add_info info, excluded_from_class excluded, race_points points
  from gp_season 
  left join gp on gp.id_gp=gp_season.id_gp 
  left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp and drivers_gp_results.id_driver=$tmp_id_driver and drivers_gp_results.season='$year'
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = gp.id_gp AND drivers_gp_starting_grid.id_driver = $tmp_id_driver AND drivers_gp_starting_grid.season = '$year'
  where gp_season.season=$year group by gp.id_gp order by gp_season.sort, race_pos";
  $result2 = mysqli_query($dbhandle,$query2);
  
  $gpPlaces=array();
  $classDrivers["createTime GP PLACES"]=microtime(true)-$start_time7;
  while($r2 = mysqli_fetch_assoc($result2)) {
    if($r2["completed"]=='0') {
        if ($r2["time"]=='DNQ') $r2["place"]="NQ";
        else {
            if (strpos($r2["info"],"dyskwalifikacja") === false){
               $r2["place"]="-";
            }else{
               $r2["place"]="DS";
            }
        }
        if ($r2["time"]=='DNS') $r2["place"]="";
    }else if($r2["completed"]=="1") {
      $r2["place"]=$r2["place"];
    }else{
      $r2["place"]="";
    }
    $gpPlaces[] = $r2;
  }
  $r["gpPlaces"]=$gpPlaces;
  $r["wins"]=$driversWinsTab[$r["id"]]==null ? 0 : $driversWinsTab[$r["id"]];
  $r["podium"]=$driversPodiumsTab[$r["id"]]==null ? 0 : $driversPodiumsTab[$r["id"]];
  $r["pp"]=$driversPolePosTab[$r["id"]]==null ? 0 : $driversPolePosTab[$r["id"]];
  $r["bestLap"]=$driversBLTab[$r["id"]]==null ? 0 : $driversBLTab[$r["id"]];
  $placesItems[] = $r;
}

// punkty
$start_time8 = microtime(true);
$query="SELECT drivers.alias alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,
place, points, points_class pointsClass, is_classified isClassified, picture, '' wins, '' podium, '' pp, '' bestLap,
COALESCE((SELECT GROUP_CONCAT(DISTINCT CONCAT(name,' ',COALESCE(engine,'')) SEPARATOR ', ') FROM teams, drivers_gp_results
WHERE teams.id_team=drivers_gp_results.id_team AND drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.season=drivers_class.season), (SELECT CONCAT(name,' ',COALESCE(engine,'')) FROM teams WHERE teams.id_team=drivers_class.id_team)) team
from drivers_class,drivers where season='$year' and drivers.id_driver=drivers_class.id_driver 
order by place";
$result = mysqli_query($dbhandle,$query);
$pointsItems=array();
$classDrivers["createTime points"]=microtime(true)-$start_time8;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id"];
  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, gp.name_alias alias, 
  SUM(race_points) race_points,
  (SELECT sprint_points FROM drivers_sprint_results WHERE season=$year and id_driver=$tmp_id_driver and drivers_sprint_results.id_gp=gp.id_gp) as sprint_points,
  COALESCE(race_points,0) +  COALESCE((SELECT sprint_points FROM drivers_sprint_results WHERE season=$year and id_driver=$tmp_id_driver and drivers_sprint_results.id_gp=gp.id_gp), 0) as points,
  race_best_lap_points bestLapPoints,race_completed completed,race_time time,race_add_info info, excluded_from_class excluded
  from gp_season left join gp on gp.id_gp=gp_season.id_gp left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp
  and drivers_gp_results.id_driver=$tmp_id_driver and drivers_gp_results.season='$year'
  where gp_season.season=$year group by gp_season.id_gp order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPoints=array();
  $classDrivers["createTime GP POINTS"]=microtime(true)-$start_time8;
  while($r2 = mysqli_fetch_assoc($result2)) {
    if($r2["race_points"]==NULL && $r2["sprint_points"]==NULL) {
      $r2["points"]="";
    }
    $gpPoints[] = $r2;
  }
  $r["gpPoints"]=$gpPoints;
  $r["wins"]=$driversWinsTab[$r["id"]]==null ? 0 : $driversWinsTab[$r["id"]];
  $r["podium"]=$driversPodiumsTab[$r["id"]]==null ? 0 : $driversPodiumsTab[$r["id"]];
  $r["pp"]=$driversPolePosTab[$r["id"]]==null ? 0 : $driversPolePosTab[$r["id"]];
  $r["bestLap"]=$driversBLTab[$r["id"]]==null ? 0 : $driversBLTab[$r["id"]];
  $pointsItems[] = $r;
}

$classDrivers["gp"]=$gpItems;
$classDrivers["regulations"]=$regulations["regulations"];
$classDrivers["classPlaces"]=$placesItems;
$classDrivers["classPoints"]=$pointsItems;
//$classDrivers["qual"]=$qualItems;

$classDrivers["createTime"]=microtime(true)-$start_time;

// Response
$response = $classDrivers;

print json_encode($response);
mysqli_free_result($result);
?>
