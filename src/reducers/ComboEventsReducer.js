import { FETCH_COMBO_EVENTS_SUCCESS } from "../actions/ComboEventsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_COMBO_EVENTS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    default:
      return state;
  }
};
