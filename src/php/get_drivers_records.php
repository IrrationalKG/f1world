<?
header('Access-Control-Allow-Origin: *');

$page=isset($_GET['page']) ? $_GET['page'] : null;
if ($page==null) $page=isset($_POST['page']) ? $_POST['page'] : null;
if ($page==null) $page=-1;
if ($page=='-') $page=-1;

$stats=isset($_GET['stats']) ? $_GET['stats'] : null;
if ($stats==null) $stats=isset($_POST['stats']) ? $_POST['stats'] : null;
if ($stats==null) $stats=-1;
if ($stats=='-') $stats=-1;

$position=isset($_GET['pos']) ? $_GET['pos'] : null;
if ($position==null) $position=isset($_POST['pos']) ? $_POST['pos'] : null;
if ($position==null) $position=-1;
if ($position=='-') $position=-1;

$team=isset($_GET['team']) ? $_GET['team'] : null;
if ($team==null) $team=isset($_POST['team']) ? $_POST['team'] : null;
if ($team==null) $team=-1;
if ($team=='-') $team=-1;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;
if ($gp==null) $gp=-1;
if ($gp=='-') $gp=-1;

$circuit=isset($_GET['circuit']) ? $_GET['circuit'] : null;
if ($circuit==null) $circuit=isset($_POST['circuit']) ? $_POST['circuit'] : null;
if ($circuit==null) $circuit=-1;
if ($circuit=='-') $circuit=-1;

$beginYear=isset($_GET['beginYear']) ? $_GET['beginYear'] : null;
if ($beginYear==null) $beginYear=isset($_POST['beginYear']) ? $_POST['beginYear'] : null;
if ($beginYear==null) $beginYear=-1;
if ($beginYear=='-') $beginYear=-1;

$endYear=isset($_GET['endYear']) ? $_GET['endYear'] : null;
if ($endYear==null) $endYear=isset($_POST['endYear']) ? $_POST['endYear'] : null;
if ($endYear==null) $endYear=-1;
if ($endYear=='-') $endYear=-1;

$status=isset($_GET['status']) ? $_GET['status'] : null;
if ($status==null) $status=isset($_POST['status']) ? $_POST['status'] : null;
if ($status==null) $status=-1;
if ($status=='-') $status=-1;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT name, display_name displayName, show_link showLink from stats where alias='$stats'";
$result = mysqli_query($dbhandle,$query);
$statsName="";
$statsLabel="";
$showLink="";
while($r = mysqli_fetch_assoc($result)) {
  $statsName = $r["name"];
  $statsLabel = $r["displayName"];
  $showLink = $r["showLink"];
}

$pages=1;
$limit=30;
$offset=(($page-1)*$limit);

$query_select_amount="SELECT COUNT(DISTINCT drivers_gp_results.race_date) amount,";
$query_select_user="
CONCAT(
    drivers.name,
    ' ',
    drivers.surname
) as name,";
$query_select="drivers.id_driver id, drivers.alias, drivers.country_code countryCode,";
if ($lang=='pl') {
  $query_select.="drivers.country,";
}else{
  $query_select.="drivers.country_en country,";
}
$query_select.="drivers.picture, '' place, drivers.show_driver active
FROM drivers_gp_involvements
LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
$query_conditions="";
$query_order="";

/**
 * Wyscig
 */
//wyscigi
if ($stats=='starts') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. miejsc punktowanych
if ($stats=='points-places') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. miejsc niepunktowanych
if ($stats=='no-points-places') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL))";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. sklasyfikowanych
if ($stats=='completed') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. niesklasyfikowanych
if ($stats=='incomplete') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. ukończonych
if ($stats=='finished') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=1";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. nieukończonych
if ($stats=='retirement') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. dyskwalifikacji
if ($stats=='disq') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND disq=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. miejsc
if ($stats=='race-places') {
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=$position AND race_completed=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. pp, naj. okrąż. i zwycięstw
if ($stats=='polepos-bestlaps-wins') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.race_best_lap=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. pp i zwycięstw
if ($stats=='polepos-wins') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. naj. okrąż. i zwycięstw
if ($stats=='bestlaps-wins') {
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_results.race_best_lap=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// wg gp
if ($stats=='races-by-gp') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
// wg roznych gp
if ($stats=='races-different-gp') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.name_short) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// wg torow
if ($stats=='races-by-circuit') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}
// wg roznych torow
if ($stats=='races-different-circuit') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.circuit_alias) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// wg zespolow
if ($stats=='races-by-teams') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    teams.name,
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers_gp_involvements.team ORDER BY amount DESC, surname";
}
//srednie miejsce w gp
if ($stats=='average-place') {
  $query_select_amount="SELECT ROUND(AVG(race_pos) ,2) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount ASC, surname";
}
//srednie punkty w gp
if ($stats=='average-points') {
  $query_select_amount="SELECT ROUND(AVG(COALESCE(drivers_gp_results.race_points,0)+COALESCE(drivers_sprint_results.sprint_points,0)) ,2) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}

/**
 * Pola startowe
 */
//naj. startów z miejsca
if ($stats=='grid-places') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. zwyciestw z miejsca
if ($stats=='wins-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. podium z miejsca
if ($stats=='podium-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. punktów z miejsca
if ($stats=='points-from-place') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
	$query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_points>0 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount > 0 ORDER BY amount DESC, surname";
}
//naj. sklasyfikowanych wyścigów z miejsca
if ($stats=='completed-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. niesklasyfikowanych wyścigów z miejsca
if ($stats=='incomplete-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. uk. wyścigów z miejsca
if ($stats=='finished-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. nie uk. wyścigów z miejsca
if ($stats=='retirement-from-place') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=$position";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// Największy awans w GP względem pozycji startowej
if ($stats=='race-grid-diff-up') {
  $query_select_amount="SELECT (COALESCE(drivers_gp_starting_grid.grid_pos,0) - COALESCE(drivers_gp_results.race_pos,0)) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_starting_grid.grid_pos,
    ' -> ',
    drivers_gp_results.race_pos,
    ' - ',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ' ',
    drivers_gp_results.season,
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  
	$query_order=" HAVING amount > 0 ORDER BY amount DESC, surname";
}
// Największa strata pozycji w GP względem pozycji startowej
if ($stats=='race-grid-diff-down') {
  $query_select_amount="SELECT (COALESCE(drivers_gp_starting_grid.grid_pos,0) - COALESCE(drivers_gp_results.race_pos,0)) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_starting_grid.grid_pos,
    ' -> ',
    drivers_gp_results.race_pos,
    ' - ',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ' ',
    drivers_gp_results.season,
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  
	$query_order=" HAVING amount < 0 ORDER BY amount, surname";
}
// Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa
if ($stats=='race-pos-higher-than-grid') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_conditions.=" AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// Najwięcej % wyścigów ukończonych na pozycji wyższej niż startowa
if ($stats=='race-pos-higher-than-grid-percent') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) races, ROUND(COUNT(drivers_gp_results.race_date) / (SELECT COUNT(d.race_date) FROM drivers_gp_results d WHERE d.id_driver=drivers.id_driver)*100,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(drivers_gp_results.race_date),
    ' / ',
    (SELECT COUNT(d.race_date) FROM drivers_gp_results d WHERE d.id_driver=drivers.id_driver),
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_conditions.=" AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, races DESC, surname";
}
// Najwięcej wyścigów ukończonych na pozycji nizszej niż startowa
if ($stats=='race-pos-lower-than-grid') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// Najwięcej % wyścigów ukończonych na pozycji nizszej niż startowa
if ($stats=='race-pos-lower-than-grid-percent') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) races, ROUND(COUNT(drivers_gp_results.race_date) / (SELECT COUNT(d.race_date) FROM drivers_gp_results d WHERE d.id_driver=drivers.id_driver)*100,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(drivers_gp_results.race_date),
    ' / ',
    (SELECT COUNT(d.race_date) FROM drivers_gp_results d WHERE d.id_driver=drivers.id_driver),
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, races DESC, surname";
}
// Wygrana z najniższej pozycji startowej
if ($stats=='race-win-lowest-grid') {
  $query_select_amount="SELECT MAX(drivers_gp_starting_grid.grid_pos) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// Podium z najniższej pozycji startowej
if ($stats=='race-podium-lowest-grid') {
  $query_select_amount="SELECT MAX(drivers_gp_starting_grid.grid_pos) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//srednia pozycja na starcie
if ($stats=='avg-grid-pos') {
  $query_select_amount="SELECT ROUND(AVG(grid_pos) ,2) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.co_driver=0";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}

/**
 * Pole Position
 */
//pole position
if ($stats=='polepos') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//pole position - procent
if ($stats=='polepos-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_pp_results.id_drivers_pp) /
  (SELECT COUNT(d.id_drivers_pp) FROM drivers_pp_results d WHERE d.id_driver=drivers_gp_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_pp_results.id_drivers_pp) pp,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_pp_results.id_drivers_pp),
    '/',
    (SELECT COUNT(d.id_drivers_pp) FROM drivers_pp_results d WHERE d.id_driver=drivers_gp_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, pp DESC, surname";
}
if ($stats=='polepos-no-win-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_pp_results.id_drivers_pp) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE (drivers_gp_starting_grid.is_pp=0 OR drivers_gp_starting_grid.id_starting_grid is null)";
  	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
//pole position no win - procent
if ($stats=='polepos-no-win-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_pp_results.id_drivers_pp) /
  (SELECT COUNT(d.id_drivers_pp) FROM drivers_pp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(drivers_pp_results.id_drivers_pp) pp,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_pp_results.id_drivers_pp),
    '/',
    (SELECT COUNT(d.id_drivers_pp) FROM drivers_pp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE (drivers_gp_starting_grid.is_pp=0 OR drivers_gp_starting_grid.id_starting_grid is null)";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount> 0 ORDER BY amount DESC, pp DESC, surname";
}
//naj. zwycięstw z pp
if ($stats=='wins-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. podium z pp
if ($stats=='podium-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. punktów z pp
if ($stats=='points-from-polepos') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
	$query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_points>0 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount > 0 ORDER BY amount DESC, surname";
}
//naj. sklasyfikowanych wyścigów z pp
if ($stats=='completed-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. niesklasyfikowanych wyścigów z pp
if ($stats=='incomplete-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. uk. wyścigów z pp
if ($stats=='finished-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. nie uk. wyścigów z pp
if ($stats=='retirement-from-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. zwycięstw spoza pp
if ($stats=='wins-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. podium spoza pp
if ($stats=='podium-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. punktów spoza pp
if ($stats=='points-outside-polepos') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
	$query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_points>0 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount > 0 ORDER BY amount DESC, surname";
}
//naj. sklasyfikowanych wyścigów spoza pp
if ($stats=='completed-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. niesklasyfikowanych wyścigów spoza pp
if ($stats=='incomplete-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. uk wyścigów spoza pp
if ($stats=='finished-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. nie uk wyścigów spoza pp
if ($stats=='retirement-outside-polepos') {
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-first-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_pp_results.qual_date),(SELECT MIN(drivers_pp_results.qual_date) FROM drivers_pp_results WHERE drivers_pp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='polepos-first-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_pp_results.qual_date),(SELECT MIN(drivers_pp_results.qual_date) FROM drivers_pp_results WHERE drivers_pp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-first-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.qual_date<=MIN(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='polepos-first-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.qual_date<=MIN(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-last-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_pp_results.qual_date) FROM drivers_pp_results WHERE drivers_pp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='polepos-last-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_pp_results.qual_date) FROM drivers_pp_results WHERE drivers_pp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-last-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.qual_date>MAX(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='polepos-last-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.qual_date>MAX(drivers_pp_results.qual_date)) amount,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions=" WHERE drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-number-by-season') {
  $query_select_amount="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_starting_grid.season,
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='polepos-percent-by-season') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) / (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_starting_grid.season) *100 ,1) amount,COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) pp,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_starting_grid.season,
    ' - ',
    ' ',
    COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid),
    '/',
    (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_starting_grid.season),
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, pp DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='polepos-by-gp') {
  $query_select_amount="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
if ($stats=='polepos-different-gp') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.name_short) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname
  ) as name,";
	$query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-by-circuit') {
  $query_select_amount="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.=" drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}
if ($stats=='polepos-different-circuit') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.circuit_alias) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname
  ) as name,";
	$query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
	$query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='polepos-by-teams') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    teams.name,
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver, drivers_gp_involvements.team ORDER BY amount DESC, surname";
}
if ($stats=='polepos-different-teams') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(DISTINCT teams.name SEPARATOR ', '),
    ')'
  ) as name,";
  $query_select="drivers.id_driver id,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM
  drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions.=" WHERE drivers_gp_starting_grid.id_starting_grid IS NOT NULL";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
	$query_order=" GROUP BY drivers.id_driver ORDER BY amount DESC, surname";
}

/**
 * Wygrane
 */
//zwyciestwa
if ($stats=='wins') {
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//zwyciestwa - procent
if ($stats=='wins-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / 
  (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-no-win-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_results.race_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>1";
  $query_conditions.=" AND drivers.id_driver NOT IN (SELECT DISTINCT dr.id_driver FROM drivers_gp_results dr WHERE dr.race_pos=1)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
if ($stats=='wins-no-win-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / 
  (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,  COUNT(DISTINCT drivers_gp_results.race_date) races,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>1";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, races DESC, surname";
}
if ($stats=='wins-first-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='wins-first-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-first-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='wins-first-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-last-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='wins-last-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-last-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='wins-last-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-number-by-season') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='wins-percent-by-season') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season) *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ' - ',
    ' ',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='wins-by-gp') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
if ($stats=='wins-different-gp') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.name_short) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-by-circuit') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}
if ($stats=='wins-different-circuit') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.circuit_alias) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='wins-by-teams') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    teams.name,
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers_gp_involvements.team ORDER BY amount DESC, surname";
}
if ($stats=='wins-different-teams') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(DISTINCT teams.name SEPARATOR ', '),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY drivers.id_driver ORDER BY amount DESC, surname";
}

/**
 * Podium
 */
//podium
if ($stats=='podium') {
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//podium - procent
if ($stats=='podium-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
  $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-no-win-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_results.race_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>3";
  $query_conditions.=" AND drivers.id_driver NOT IN (SELECT DISTINCT dr.id_driver FROM drivers_gp_results dr WHERE dr.race_pos<4)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
//podium - procent
if ($stats=='podium-no-win-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
  $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>3";
  
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-first-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='podium-first-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-first-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='podium-first-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-last-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='podium-last-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-last-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='podium-last-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-number-by-season') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='podium-percent-by-season') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season) *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ' - ',
    ' ',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='podium-by-gp') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
if ($stats=='podium-different-gp') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.name_short) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='podium-by-circuit') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}
if ($stats=='podium-different-circuit') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.circuit_alias) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}

/**
 * Punkty
 */
//punkty
if ($stats=='points') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
	$query_conditions=" WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short";
  $query_order.=" HAVING amount > 0 ORDER BY amount DESC, surname";
}
if ($stats=='points-scored-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_results.race_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
  $query_conditions.=" AND drivers.id_driver NOT IN (SELECT DISTINCT dr.id_driver FROM drivers_gp_results dr WHERE drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)";
  $query_conditions.=")";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
//wyscigi z punktami - procent
if ($stats=='points-percent') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_results.race_date) racesWithPoints, ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / 
  (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver AND d.co_driver=0";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
 	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short";
  $query_order.=" ORDER BY amount DESC, 1 DESC, surname";
}
if ($stats=='points-no-scored-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_results.race_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL))";
  //$query_conditions.=" AND drivers.id_driver NOT IN (SELECT DISTINCT dr.id_driver FROM drivers_gp_results dr WHERE dr.race_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
// wyscigi bez punktow - procent
if ($stats=='points-no-scored-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver)*100 ,1) amount,
  COUNT(DISTINCT drivers_gp_results.race_date) races,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver),
    ')'
  ) as name,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL))";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, races DESC, surname";
}
if ($stats=='points-first-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='points-first-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='points-first-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='points-first-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='points-last-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='points-last-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='points-last-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='points-last-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='points-number-by-season') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='points-by-gp') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
if ($stats=='points-by-circuit') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_gp_results.race_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}

/**
 * Najl. okrążenia
 */
//naj. okrąż.
if ($stats=='bestlaps') {
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. okrąż. - procent
if ($stats=='bestlaps-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) races,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
  $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, races DESC, surname";
}
//bez naj. okrąż.
if ($stats=='bestlaps-no-win-gp-number') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_results.race_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0";
  $query_conditions.=" AND drivers.id_driver NOT IN (SELECT DISTINCT dr.id_driver FROM drivers_gp_results dr WHERE dr.race_best_lap=1";
  $query_conditions.=")";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, surname";
}
//bez naj. okrąż. - procent
if ($stats=='bestlaps-no-win-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_gp_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) races,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_gp) FROM drivers_gp_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_gp_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
  $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, races DESC, surname";
}
if ($stats=='bestlaps-first-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='bestlaps-first-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount desc, surname";
}
if ($stats=='bestlaps-first-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='bestlaps-first-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date<=MIN(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount desc, surname";
}
if ($stats=='bestlaps-last-win-diff-asc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='bestlaps-last-win-diff-desc') {
  $query_select_amount="SELECT DATEDIFF((SELECT MAX(drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver),MAX(drivers_gp_results.race_date)) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount desc, surname";
}
if ($stats=='bestlaps-last-win-gp-number-asc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, surname";
}
if ($stats=='bestlaps-last-win-gp-number-desc') {
  $query_select_amount="SELECT (SELECT count(d2.race_date) FROM drivers_gp_results d2 WHERE d2.id_driver=drivers.id_driver AND d2.race_date>=MAX(drivers_gp_results.race_date))-1 amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount desc, surname";
}
if ($stats=='bestlaps-number-by-season') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='bestlaps-percent-by-season') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season) *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) races,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    drivers_gp_results.season,
    ' - ',
    ' ',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(DISTINCT drivers_gp_involvements.id_gp) FROM drivers_gp_involvements WHERE drivers_gp_involvements.season=drivers_gp_results.season),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, drivers_gp_results.season ORDER BY amount DESC, races DESC, surname, drivers_gp_results.season DESC";
}
if ($stats=='bestlaps-by-gp') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.name AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.name_short ORDER BY amount DESC, surname, gp.name_short DESC";
}
if ($stats=='bestlaps-different-gp') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.name_short) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
if ($stats=='bestlaps-by-circuit') {
  $query_select_amount="SELECT COUNT(drivers_gp_results.race_date) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    CAST(gp.circuit AS CHAR CHARACTER SET utf8),
    ')'
  ) as name,";
	$query_conditions.=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short, gp.circuit ORDER BY amount DESC, surname, gp.circuit DESC";
}
if ($stats=='bestlaps-different-circuit') {
  $query_select_amount="SELECT COUNT(DISTINCT gp.circuit_alias) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}

/**
 * Sprinty
 */
//sprinty
if ($stats=='sprints') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_sprint_results.sprint_date) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty - wygrane
if ($stats=='sprint-wins') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_sprint_results.sprint_date) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty - procent
if ($stats=='sprints-wins-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_sprint_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) sprints,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_sprint_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, sprints DESC, surname";
}
//sprinty - podium
if ($stats=='sprint-podiums') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_sprint_results.sprint_date) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos<4";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty podium - procent
if ($stats=='sprints-podiums-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_sprint_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) sprints,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_sprint_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos<4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, sprints DESC, surname";
}
//sprinty - punkty
if ($stats=='sprint-points') {
  $query_select_amount="SELECT COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_points>0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty punkty - procent
if ($stats=='sprints-points-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_sprint_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) sprints,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_sprint_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_points>0";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, sprints DESC, surname";
}
//sprinty - sklasyfikowany
if ($stats=='sprint-completed') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_sprint_results.sprint_date) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty sklasyfikowany - procent
if ($stats=='sprints-completed-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_sprint_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) sprints,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_sprint_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, sprints DESC, surname";
}
//sprinty - niesklasyfikowany
if ($stats=='sprint-incompleted') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_sprint_results.sprint_date) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//sprinty niesklasyfikowany - procent
if ($stats=='sprints-incompleted-percent') {
  $query_select_amount="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
  LEFT JOIN teams t ON d.id_team=t.id_team
  LEFT JOIN gp g ON d.id_gp=g.id_gp
  WHERE d.id_driver=drivers_sprint_results.id_driver";
  if ($beginYear!=-1) $query_select_amount.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select_amount.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select_amount.=" AND g.circuit_alias='$circuit'";
  if ($team!=-1) $query_select_amount.=" AND t.alias_name='$team'";
  $query_select_amount.=") *100 ,1) amount,COUNT(DISTINCT drivers_gp_results.race_date) sprints,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    COUNT(DISTINCT drivers_gp_results.race_date),
    '/',
    (SELECT COUNT(d.id_drivers_sprint) FROM drivers_sprint_results d 
    LEFT JOIN teams t ON d.id_team=t.id_team
    LEFT JOIN gp g ON d.id_gp=g.id_gp
    WHERE d.id_driver=drivers_sprint_results.id_driver";
    if ($beginYear!=-1) $query_select_user.=" AND d.season='$beginYear'";
    if ($gp!=-1) $query_select_user.=" AND g.name_alias='$gp'";
    if ($circuit!=-1) $query_select_user.=" AND g.circuit_alias='$circuit'";
    if ($team!=-1) $query_select_user.=" AND t.alias_name='$team'";
    $query_select_user.="),
    ')'
  ) as name,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=0";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, sprints DESC, surname";
}

/**
 * Tytuly mistrzowskie
 */
$lastSeason = 2025;
if ($stats=='champs-number') {
  $query_select_amount="SELECT COUNT(drivers_class.id_drivers_class) amount,MAX(drivers_class.season) lastSeason,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(drivers_class.season ORDER BY drivers_class.season SEPARATOR ', '),
    ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1 AND drivers_class.season<$lastSeason";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, 2, surname";
}
if ($stats=='champs-by-seasons') {
  $query_select_amount="SELECT drivers_class.season amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1 AND drivers_class.season<$lastSeason";
	$query_order=" ORDER BY season DESC";
}
if ($stats=='champs-seasons-needed') {
  $query_select_amount="SELECT
    (
    SELECT
        COUNT(dc.season)
    FROM
        drivers_class dc
    WHERE
        dc.season <=(
        SELECT
            MIN(dc2.season)
        FROM
            drivers_class dc2
        WHERE
            id_driver = drivers_class.id_driver
            AND dc2.place=1
    ) AND dc.id_driver = drivers_class.id_driver
  ) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount";
}
if ($stats=='champs-wins-needed-asc') {
  $query_select_amount="SELECT
  (
  SELECT
  COUNT(drivers_gp_results.id_gp)
  FROM
      drivers_gp_results
  LEFT JOIN drivers_class dc ON dc.id_driver=drivers_gp_results.id_driver AND dc.season=drivers_gp_results.season    
  WHERE
      drivers_gp_results.race_pos = 1 
      AND drivers_gp_results.id_driver = drivers_class.id_driver
      AND dc.place=1
  AND drivers_gp_results.season=drivers_class.season
  ) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname,
      ' (',
      drivers_class.season,
      ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1";
	$query_order=" ORDER BY amount, drivers_class.season";
}
if ($stats=='champs-wins-needed-desc') {
  $query_select_amount="SELECT
  (
  SELECT
  COUNT(drivers_gp_results.id_gp)
  FROM
      drivers_gp_results
  LEFT JOIN drivers_class dc ON dc.id_driver=drivers_gp_results.id_driver AND dc.season=drivers_gp_results.season    
  WHERE
      drivers_gp_results.race_pos = 1 
      AND drivers_gp_results.id_driver = drivers_class.id_driver
      AND dc.place=1
  AND drivers_gp_results.season=drivers_class.season
  ) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname,
      ' (',
      drivers_class.season,
      ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1";
	$query_order=" ORDER BY amount DESC, drivers_class.season";
}
if ($stats=='champs-wins-percent') {
  $query_select_amount="SELECT
  ROUND(
      (
      SELECT
          COUNT(drivers_gp_results.id_gp)
      FROM
          drivers_gp_results
      LEFT JOIN drivers_class dc ON
          dc.id_driver = drivers_gp_results.id_driver AND dc.season = drivers_gp_results.season
      WHERE
          drivers_gp_results.race_pos = 1 AND drivers_gp_results.id_driver = drivers_class.id_driver AND dc.place = 1 AND drivers_gp_results.season = drivers_class.season
  ) / (
      SELECT
          COUNT(gp_season.id_gp_season)
      FROM
          gp_season
      WHERE
          gp_season.season = drivers_class.season
  ) * 100,0) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname,
      ' (',
      (
      SELECT
          COUNT(drivers_gp_results.id_gp)
      FROM
          drivers_gp_results
      LEFT JOIN drivers_class dc ON
          dc.id_driver = drivers_gp_results.id_driver AND dc.season = drivers_gp_results.season
      WHERE
          drivers_gp_results.race_pos = 1 AND drivers_gp_results.id_driver = drivers_class.id_driver AND dc.place = 1 AND drivers_gp_results.season = drivers_class.season
      ),
      '/',
      (
      SELECT
          COUNT(gp_season.id_gp_season)
      FROM
          gp_season
      WHERE
          gp_season.season = drivers_class.season
      ),
      ' - ',
      drivers_class.season,
      ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1";
	$query_order=" ORDER BY amount DESC, drivers_class.season";
}
if ($stats=='champs-points') {
  $query_select_amount="SELECT drivers_class.points_class amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname,
      ' (',
      drivers_class.season,
      ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1 AND drivers_class.season<$lastSeason";
	$query_order=" ORDER BY amount DESC, drivers_class.season";
}
if ($stats=='champs-points-diff') {
  $query_select_amount="SELECT drivers_class.points_class - (SELECT dc.points_class FROM drivers_class dc WHERE dc.season=drivers_class.season AND dc.place=2) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
      drivers.name,
      ' ',
      drivers.surname,
      ' (',
      drivers_class.season,
      ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.=" drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class
  LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=1 AND drivers_class.season<$lastSeason";
	$query_order=" ORDER BY amount, drivers_class.season";
}
if ($stats=='champs-vice-number') {
  $query_select_amount="SELECT COUNT(drivers_class.id_drivers_class) amount,MAX(drivers_class.season) lastSeason,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(drivers_class.season ORDER BY drivers_class.season SEPARATOR ', '),
    ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place=2 AND drivers_class.season<$lastSeason";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, 2, surname";
}
if ($stats=='champs-podium') {
  $query_select_amount="SELECT COUNT(drivers_class.id_drivers_class) amount,MAX(drivers_class.season) lastSeason,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(drivers_class.season ORDER BY drivers_class.season SEPARATOR ', '),
    ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE drivers_class.place<=3 AND drivers_class.season<$lastSeason";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, 2, surname";
}
if ($stats=='champs-best-result') {
  $query_select_amount="SELECT (SELECT COUNT(place) FROM drivers_class dc WHERE dc.place=MIN(drivers_class.place) AND dc.id_driver=drivers_class.id_driver GROUP BY dc.id_driver), MIN(place) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    (SELECT COUNT(place) FROM drivers_class dc WHERE dc.place=MIN(drivers_class.place) AND dc.id_driver=drivers_class.id_driver GROUP BY dc.id_driver),
    'x)'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver";
  $query_conditions=" WHERE is_classified=1 AND drivers_class.season<$lastSeason";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount, 1 DESC, surname";
}
if ($stats=='champs-different-teams') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.team) amount,";
  $query_select="drivers.id_driver id,
  CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(DISTINCT teams.name SEPARATOR ', '),
    ')'
  ) as name,
  drivers.alias,
  drivers.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="drivers.country,";
  }else{
    $query_select.="drivers.country_en country,";
  }
  $query_select.="drivers.picture,
  '' place,
  drivers.show_driver active
  FROM drivers_class LEFT JOIN drivers ON drivers.id_driver = drivers_class.id_driver
  LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_driver = drivers_class.id_driver
  AND drivers_gp_involvements.season=drivers_class.season
  LEFT JOIN teams ON drivers_gp_involvements.team=teams.team";
  $query_conditions=" WHERE drivers_class.place=1 AND drivers_class.is_classified=1 AND drivers_class.season<$lastSeason";
	$query_order=" GROUP BY drivers_gp_involvements.id_driver ORDER BY amount DESC, surname";
}

/**
 * Misc
 */
//sezony
if ($stats=='seasons') {
  $query_select_amount="SELECT count(DISTINCT drivers_gp_involvements.season) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// grand prix
if ($stats=='gp') {
  $query_select_amount="SELECT COUNT(DISTINCT CONCAT(drivers_gp_involvements.season,drivers_gp_involvements.id_gp)) amount,";
  $query_conditions=" WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// gp w roznych zespolach
if ($stats=='different-teams') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.team) amount,";
  $query_select_user="CONCAT(
    drivers.name,
    ' ',
    drivers.surname,
    ' (',
    GROUP_CONCAT(DISTINCT teams.name SEPARATOR ', '),
    ')'
  ) as name,";
	$query_conditions=" WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)";
  $query_order=" GROUP BY drivers.id_driver ORDER BY amount DESC, surname";
}
//kwalifikacje
if ($stats=='qual') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_pp_results.qual_date) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_pp IS NOT NULL";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//poza podium
if ($stats=='race-fourth-place') {
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos=4";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//odpadniecie na 1 okraz
if ($stats=='race-first-lap-incomplete') {
  $query_conditions=" WHERE drivers_gp_involvements.id_involvement IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
  $query_conditions.=" AND drivers_gp_results.race_laps=0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// Najwiecej niezakwalifikowan
if ($stats=='not-qualified') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.id_involvement) amount,";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NULL AND drivers_pp_results.not_qualified=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
//naj. nie wystartowal
if ($stats=='not-started') {
  $query_select_amount="SELECT COUNT(DISTINCT drivers_gp_involvements.id_involvement) amount,";
  $query_conditions.=" WHERE drivers_pp_results.not_started=1";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// nigdy nie zakwalifikowani
if ($stats=='race-never-qualified') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NULL AND drivers_pp_results.not_qualified=1";
  $query_conditions.=" AND (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver)=0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// nigdy nie zostali sklasyfikowani
if ($stats=='race-never-completed') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
  $query_conditions.=" AND (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver AND drivers_gp_results.race_completed=1)=0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// nigdy nie ukonczyli
if ($stats=='race-never-finished') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount,";
	$query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0";
  $query_conditions.=" AND (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver AND drivers_gp_results.race_finished=1)=0";
  $query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short ORDER BY amount DESC, surname";
}
// nigdy nie zdobyli punktow
if ($stats=='race-never-point-scored') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount, 
  (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver) races,";
	$query_conditions=" WHERE drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount=races ORDER BY amount DESC, surname";
}
// nigdy nie zdobyli pp
if ($stats=='race-never-polepos') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount, 
  (SELECT COUNT(DISTINCT dr.id_involvement) FROM drivers_gp_involvements dr WHERE dr.id_driver=drivers_gp_involvements.id_driver) races,";
  $query_select.=" LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions.=" WHERE drivers_gp_starting_grid.is_pp=0";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount=races ORDER BY amount DESC, surname";
}
// nigdy nie uzyskali najl. okrazenia
if ($stats=='race-never-bestlaps') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount, 
  (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver) races,";
  $query_conditions.=" WHERE drivers_gp_results.race_best_lap=0";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount=races ORDER BY amount DESC, surname";
}
// nigdy nie stali na podium
if ($stats=='race-never-podium') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount, 
  (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver) races,";
  $query_conditions.=" WHERE drivers_gp_results.race_pos>3";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount=races ORDER BY amount DESC, surname";
}
// nigdy nie wygrali
if ($stats=='race-never-win') {
  $query_select_amount="SELECT COUNT(drivers_gp_involvements.id_involvement) amount, 
  (SELECT COUNT(DISTINCT dr.race_date) FROM drivers_gp_results dr WHERE dr.id_driver=drivers_gp_involvements.id_driver) races,";
  $query_conditions.=" WHERE drivers_gp_results.race_pos>1";
	$query_order=" GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short HAVING amount=races ORDER BY amount DESC, surname";
}

// sql
if ($beginYear!=-1) $query_conditions.=" AND drivers_gp_involvements.season='$beginYear'";
if ($gp!=-1) $query_conditions.=" AND gp.name_alias='$gp'";
if ($circuit!=-1) $query_conditions.=" AND gp.circuit_alias='$circuit'";
if ($team!=-1) $query_conditions.=" AND teams.alias_name='$team'";
if ($status == '1') $query_conditions.=" AND drivers.show_driver=1";

$query_select_with_amount=$query_select_amount.$query_select_user.$query_select;

$query="$query_select_with_amount$query_conditions$query_order LIMIT $limit OFFSET $offset";
$result = mysqli_query($dbhandle,$query);
$recordItems=array();
$place = $offset;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $r["place"] = $place;
  $recordItems[] = $r;

}
$query="$query_select_with_amount$query_conditions$query_order";
$result = mysqli_query($dbhandle,$query);
$bestDriver="";
$bestDriverId="";
$amount="";
$total=0;
while($r = mysqli_fetch_assoc($result)) {
  if ($pages == 1) {
    $bestDriverId = $r["id"];
    $bestDriver = $r["name"];
    $amount = $r["amount"];
  }
  $pages+=1;
  $total+=1;
}
$pages = ceil($pages / $limit);


$recordsItems["stats"]=$stats;
$recordsItems["statsName"]=$statsName;
$recordsItems["statsLabel"]=$statsLabel;
$recordsItems["showLink"]=$showLink;
$recordsItems["team"]=$team;
$recordsItems["gp"]=$gp;
$recordsItems["circuit"]=$circuit;
$recordsItems["beginYear"]=$beginYear;
$recordsItems["endYear"]=$endYear;
$recordsItems["status"]=$status;
$recordsItems["pages"]=$pages;
$recordsItems["items"]=$recordItems;
$recordsItems["bestDriverId"]=$bestDriverId;
$recordsItems["bestDriver"]=$bestDriver;
$recordsItems["amount"]=$amount;
$recordsItems["total"]=$total;
$recordsItems["query"]=$query;
$recordsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $recordsItems;

print json_encode($response);
mysqli_free_result($result);
?>
