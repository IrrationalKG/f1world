/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  GridColumn,
  Segment,
  Loader,
  Statistic,
  Flag,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import NextGPCountdown from "../NextGPCountdown/NextGPCountdown";
import { fetchNextGP } from "../../actions/NextGPActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./NextGP.less";

class NextGP extends Component {
  componentDidMount() {
    const { props } = this;
    props.fetchNextGP(props.year, props.id, Cookies.get("lang"));
  }

  renderCircuitRecords() {
    const { props } = this;
    const { alias, gpAlias, stats } = props.nextGP.data;
    return (
      <>
        <Grid.Column>
          <Statistic size="large" id="driver-starts" inverted>
            <NavLink to={`/driver/${stats.starts.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.starts.idDriver}_profile.jpg`}
                alt={stats.starts.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.starts.alias}`}>
                <div>{stats.starts.name}</div>
                <Flag name={stats.starts.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/starts/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div> {+(Math.round(stats.starts.amount * 100) / 100)}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              {" "}
              <FormattedMessage id={"app.stats.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic size="large" id="driver-wins" inverted>
            <NavLink to={`/driver/${stats.wins.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.wins.idDriver}_profile.jpg`}
                alt={stats.wins.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.wins.alias}`}>
                <div>{stats.wins.name}</div>
                <Flag name={stats.wins.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/wins/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div>{stats.wins.amount}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic size="large" id="driver-podium" inverted>
            <NavLink to={`/driver/${stats.podium.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.podium.idDriver}_profile.jpg`}
                alt={stats.podium.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.podium.alias}`}>
                <div>{stats.podium.name}</div>
                <Flag name={stats.podium.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/podium/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div>{stats.podium.amount}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic size="large" id="driver-points" inverted>
            <NavLink to={`/driver/${stats.points.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.points.idDriver}_profile.jpg`}
                alt={stats.points.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.points.alias}`}>
                <div>{stats.points.name}</div>
                <Flag name={stats.points.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/points/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div>{+(Math.round(stats.points.amount * 100) / 100)}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic size="large" id="driver-polepos" inverted>
            <NavLink to={`/driver/${stats.polepos.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.polepos.idDriver}_profile.jpg`}
                alt={stats.polepos.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.polepos.alias}`}>
                <div>{stats.polepos.name}</div>
                <Flag name={stats.polepos.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/polepos/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div>{stats.polepos.amount}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic size="large" id="driver-bestlaps" inverted>
            <NavLink to={`/driver/${stats.bestlaps.alias}`}>
              <Image
                src={`/build/images/drivers/driver_${stats.bestlaps.idDriver}_profile.jpg`}
                alt={stats.bestlaps.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <h4>
              <NavLink to={`/driver/${stats.bestlaps.alias}`}>
                <div>{stats.bestlaps.name}</div>
                <Flag name={stats.bestlaps.countryCode} />
              </NavLink>
            </h4>
            <Statistic.Value>
              <NavLink
                to={`/drivers-records/bestlaps/-/${gpAlias}/${alias}/-/-/-/1`}
              >
                <div>{stats.bestlaps.amount}</div>
              </NavLink>
            </Statistic.Value>
            <Statistic.Label>
              {" "}
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
      </>
    );
  }

  render() {
    const { props } = this;
    if (!props.nextGP.data) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    const { alias, country, name, circuit, date, stats, round } =
      props.nextGP.data;
    const pic = `build/images/circuits/circuit_${alias}_banner.jpg`;
    const picName = pic.substring(0, pic.length - 4);
    const picp = `${picName}.webp`;
    const pic1024w = `${picName}-1024w.jpg`;
    const pic1024wp = `${picName}-1024w.webp`;
    const trackPic = `build/images/circuits/circuit_${alias}_map.jpg`;
    const link = `/circuit/${alias}`;
    const linkContest = "/contest/rate";
    return (
      <section id="next-gp" name="next-gp">
        <div className="next-gp-header">
          <h2>
            <FormattedMessage id={"app.page.home.nextgp.title"} />
          </h2>
          <h3>{name}</h3>
        </div>
        <div className="next-gp-subheader">
          <Grid verticalAlign="middle">
            <GridColumn mobile={2} tablet={3} computer={3}>
              <NavLink to={link}>
                <Image
                  src={`/build/images/countries/${country}.jpg`}
                  alt={alias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/countries/countries_no_profile.jpg";
                  }}
                />{" "}
              </NavLink>
            </GridColumn>
            <GridColumn
              mobile={14}
              tablet={13}
              computer={13}
              className="next-gp-subheader-circuit-name"
            >
              <NavLink to={link}>{circuit} </NavLink>
            </GridColumn>
          </Grid>
        </div>
        <NextGPCountdown />
        <div className="next-gp-date">{date}</div>
        <div className="next-gp-banner">
          <NavLink to={link}>
            <picture>
              <source
                media="(max-width: 1024px)"
                srcSet={pic1024wp}
                type="image/webp"
              />
              <source media="(max-width: 1024px)" srcSet={pic1024w} />
              <source srcSet={picp} type="image/webp" />
              <img src={pic} alt="Następna Grand Prix" />
            </picture>
          </NavLink>
          <div className="next-gp-season">
            <div className="next-gp-season-title">
              <FormattedMessage id={"app.page.home.nextgp.records.round"} />
            </div>
            <div className="next-gp-season-year">{round}</div>
          </div>
        </div>
        <div className="next-gp-content">
          <div className="next-gp-map">
            <NavLink to={link}>
              <Image src={trackPic} alt={alias} />
            </NavLink>
          </div>
          {stats.wins.amount != null && (
            <Segment basic className="next-gp-stats">
              <h3>
                <FormattedMessage id={"app.page.home.nextgp.records.title"} />
              </h3>
              <Grid>
                <Grid.Row textAlign="center" columns={3}>
                  {this.renderCircuitRecords()}
                </Grid.Row>
              </Grid>
              <div className="buttons">
                <NavLink className="primary" to={link}>
                  <FormattedMessage id={"app.button.info"} />
                </NavLink>
                <NavLink className="quaternary" to={linkContest}>
                  <FormattedMessage id={"app.button.rate"} />
                </NavLink>
              </div>
            </Segment>
          )}
        </div>
      </section>
    );
  }
}

NextGP.propTypes = {
  fetchNextGP: PropTypes.func.isRequired,
  nextGP: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  id: PropTypes.string,
};

function mapStateToProps({ nextGP }) {
  return { nextGP };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchNextGP }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NextGP);
