import {
  FETCH_DRIVER_TEAMMATES_REQUEST,
  FETCH_DRIVER_TEAMMATES_SUCCESS,
  FETCH_DRIVER_TEAMMATES_ERROR,
} from "../actions/DriverTeammatesActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_DRIVER_TEAMMATES_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_DRIVER_TEAMMATES_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_DRIVER_TEAMMATES_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
