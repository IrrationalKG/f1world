// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GPSTATS_REQUEST = "FETCH_GPSTATS_REQUEST";
export const FETCH_GPSTATS_SUCCESS = "FETCH_GPSTATS_SUCCESS";
export const FETCH_GPSTATS_ERROR = "FETCH_GPSTATS_ERROR";

function requestData() {
  return {
    type: FETCH_GPSTATS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GPSTATS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GPSTATS_ERROR,
    payload: error,
  };
}

export function fetchGPStats(id, lang) {
  const url = `${ROOT_URL}/get_gp_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
