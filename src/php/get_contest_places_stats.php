<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// wygrane zwykla
$query="SELECT count(*) as amount, alias from typy,users where typy.uczestnik=users.id_user and ((pkt_gp=10 and sezon<2010) OR (pkt_gp=25 and sezon>=2010)) group by uczestnik order by amount desc";
$result = mysqli_query($dbhandle,$query);
$winsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $winsItems[$r["alias"]] = $r["amount"];
}
// drugie miejsca zwykla
$query="SELECT count(*) as amount, alias from typy,users where typy.uczestnik=users.id_user and ((pkt_gp=8 and sezon<2010) OR (pkt_gp=18 and sezon>=2010)) group by uczestnik order by amount desc";
$result = mysqli_query($dbhandle,$query);
$secondItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $secondItems[$r["alias"]] = $r["amount"];
}
// trzecie miejsca zwykla
$query="SELECT count(*) as amount, alias from typy,users where typy.uczestnik=users.id_user and ((pkt_gp=6 and sezon<2010) OR (pkt_gp=15 and sezon>=2010)) group by uczestnik order by amount desc";
$result = mysqli_query($dbhandle,$query);
$thirdItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $thirdItems[$r["alias"]] = $r["amount"];
}

$contest=array();
$contest["wins"]=$winsItems;
$contest["second"]=$secondItems;
$contest["third"]=$thirdItems;

$contest["createTime"]=microtime(true)-$start_time;

// Response
$response = $contest;

print json_encode($response);
mysqli_free_result($result);
?>
