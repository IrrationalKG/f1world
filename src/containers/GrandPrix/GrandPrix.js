/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubgp","renderDriverSubgp"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Grid,
  Loader,
  Header,
  Divider,
  Statistic,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchGrandPrix } from "../../actions/GrandPrixActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GrandPrix.less";

class GrandPrix extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderGrandPrix = this.renderGrandPrix.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGrandPrix(Cookies.get("lang"));
  }

  renderGrandPrix() {
    const items = [];
    const { props } = this;
    const { data } = props.gp;
    if (data) {
      data.forEach((item) => {
        const { id, alias, name, races, seasons } = item;
        const filename = `/build/images/countries/${id}.jpg`;
        const detailsLink = `/gp-stats/${alias}`;
        const record = (
          <Grid.Column
            textAlign="center"
            key={id}
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <NavLink to={detailsLink}>
                    <Image
                      centered
                      src={filename}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/countries/country_no_profile.jpg";
                      }}
                      className="fluid"
                    />
                  </NavLink>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content>{name}</Header.Content>
                    </Header>
                    <Statistic size="large">
                      <Statistic.Value>{races}</Statistic.Value>
                      <Statistic.Label>
                        <FormattedMessage id={"app.stats.gp"} />
                      </Statistic.Label>
                    </Statistic>
                    <Divider></Divider>
                    <Header as="h2">
                      <Header.Content>
                        <Header.Subheader>{seasons}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <div className="buttons">
                    <NavLink className="primary" to={detailsLink}>
                      <FormattedMessage id={"app.button.details"} />
                    </NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { props } = this;
    if (!props.gp.data || props.gp.loading) {
      return (
        <section id="grand-prix" name="grand-prix" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "../build/images/history/history_banner.jpg";
    return (
      <section id="grand-prix" name="grand-prix" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.gp.stats.list.title"} />}
          subtitle={<FormattedMessage id={"app.page.gp.stats.list.subtitle"} />}
        />
        <div className="section-page-content">
          <Grid>{this.renderGrandPrix()}</Grid>
        </div>
      </section>
    );
  }
}
GrandPrix.propTypes = {
  fetchGrandPrix: PropTypes.func.isRequired,
  gp: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      year: PropTypes.string,
    }).isRequired,
  }).isRequired,
  year: PropTypes.string,
};

function mapStateToProps({ gp }) {
  return { gp };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGrandPrix }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GrandPrix);
