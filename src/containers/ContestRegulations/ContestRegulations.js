/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { Grid } from "semantic-ui-react";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { FormattedMessage } from "react-intl";
import styles from "./ContestRegulations.less";

class ContestRegulations extends Component {
  render() {
    const pic = "../build/images/contest/contest_banner.jpg";
    return (
      <section
        id="contest-regulations"
        name="contest-regulations"
        className="section-page"
      >
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.contest.regulations.title"} />}
          subtitle={
            <FormattedMessage id={"app.page.contest.regulations.subtitle"} />
          }
        />
        <div className="section-page-content contest-regulations-content">
          <Grid stackable>
            <Grid.Row>
              <Grid.Column>
                <ol>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info1"}
                    />
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info2"}
                    />
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info3"}
                    />

                    <ol>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info4"}
                        />
                      </li>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info5"}
                        />
                      </li>
                    </ol>
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info6"}
                    />

                    <ol>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info7"}
                        />
                      </li>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info8"}
                        />
                      </li>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info9"}
                        />
                      </li>
                      <li>
                        <FormattedMessage
                          id={"app.page.contest.regulations.info10"}
                        />
                      </li>
                    </ol>
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info11"}
                    />
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info12"}
                    />
                  </li>
                  <li>
                    <FormattedMessage
                      id={"app.page.contest.regulations.info13"}
                    />
                  </li>
                </ol>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </section>
    );
  }
}

export default ContestRegulations;
