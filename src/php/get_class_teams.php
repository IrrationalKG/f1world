<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// grand prix w sezonie
$query="SELECT gp.country_code name, gp.name gp, gp.name_short nameShort from gp,gp_season where gp.id_gp=gp_season.id_gp
and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}
// regulamin sezonu
if ($lang=='pl'){
  $query="SELECT regulations from season_regulations where year='$year'";
}else{
  $query="SELECT regulations_en as regulations from season_regulations where year='$year'";
}
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $regulations = $r;
}
//zwyciestwa w sezonie
$query="SELECT teams.alias team,count(id_drivers_gp) as wins from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and race_pos=1
group by team";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["team"];
  $teamsWinsTab[$tmp_id_team] = $r["wins"];
}
//podium w sezonie
$query="SELECT teams.alias team,count(id_drivers_gp) as podiums from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and race_pos<4 and race_completed=1
group by team";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["team"];
  $teamsPodiumsTab[$tmp_id_team] = $r["podiums"];
}
//pole position w sezonie
$query="SELECT teams.alias team,count(id_drivers_pp) as pp from drivers_pp_results,teams
where drivers_pp_results.id_team=teams.id_team and qual_date>='$year-01-01' and qual_date<='$year-12-31' and qual_pos=1
group by team";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["team"];
  $teamsPolePosTab[$tmp_id_team] = $r["pp"];
}
//best laps w sezonie
$query="SELECT teams.alias team,count(id_drivers_gp) as bestlaps from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and race_date>='$year-01-01' and race_date<='$year-12-31' and race_best_lap=1
group by team";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["team"];
  $teamsBLTab[$tmp_id_team] = $r["bestlaps"];
}

/**
* Klasyfikacje
**/
// miejsca
$query="SELECT distinct teams.alias_name alias, teams.alias aliasTeam,teams.id_team id,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,
lower(teams.team) team,teams.country_code country, place, points, points_class pointsClass, picture, '' wins, '' podium, '' pp, '' bestLap, '' drivers,";
if ($lang=='pl'){
  $query.="GROUP_CONCAT(distinct COALESCE(teams_models.team_name, 'Prywatnie') ORDER BY 1 SEPARATOR ', ') modelName";
}else{
  $query.="GROUP_CONCAT(distinct COALESCE(teams_models.team_name, 'Private') ORDER BY 1 SEPARATOR ', ') modelName";
}
$query.=" from teams_class
LEFT JOIN teams ON teams.id_team=teams_class.id_team 
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team = teams_class.id_team
AND drivers_gp_involvements.season = teams_class.season
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
where teams_class.season='$year' 
group by teams_class.id_team
order by place";
$result = mysqli_query($dbhandle,$query);
$teamsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["aliasTeam"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name_alias alias, gp.name gp, gp.id_gp id
  from gp_season left join gp on gp.id_gp=gp_season.id_gp where gp_season.season=$year order by gp_season.sort";

  $result2 = mysqli_query($dbhandle,$query2);
  $gpPlaces=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $tmp_id_gp = $r2["id"];

    $query5="SELECT sum(race_team_points) points, race_points_ignored pointsIgnored,teams.alias aliasTeam,
    coalesce(grid_pos,0) grid, sum(race_best_lap) bestLap
    from drivers_gp_results
    LEFT JOIN teams ON teams.id_team = drivers_gp_results.id_team
    LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_results.id_gp 
    AND drivers_gp_starting_grid.id_team = drivers_gp_results.id_team AND drivers_gp_starting_grid.season = drivers_gp_results.season 
    and drivers_gp_starting_grid.is_pp=1
    where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year'
    and drivers_gp_results.id_gp=$tmp_id_gp group by teams.id_team,drivers_gp_results.id_gp order by 1 desc";
    $result5 = mysqli_query($dbhandle,$query5);
    $r2["teamPlace"] = "";
    $r2["pointsIgnored"] = "0";
    $cnt=0;
    while($r5 = mysqli_fetch_assoc($result5)) {
      $cnt+=1;
      if ($r5["aliasTeam"] == $tmp_id_team) {
        if ($r5["points"] > 0){
          $r2["teamPlace"] = $cnt;
        }else{
          $r2["teamPlace"] = "-";
        }
        $r2["pointsIgnored"] = $r5["pointsIgnored"];
        $r2["grid"] = $r5["grid"];
        $r2["bestLap"] = $r5["bestLap"];
        break;
      }
    }

    $gpPlaces[]=$r2;
  }
  //$r["drivers"]=$drivers;
  $r["gpPlaces"]=$gpPlaces;
  $r["wins"]=$teamsWinsTab[$r["aliasTeam"]]==null ? 0 : $teamsWinsTab[$r["aliasTeam"]];
  $r["podium"]=$teamsPodiumsTab[$r["aliasTeam"]]==null ? 0 : $teamsPodiumsTab[$r["aliasTeam"]];
  $r["pp"]=$teamsPolePosTab[$r["aliasTeam"]]==null ? 0 : $teamsPolePosTab[$r["aliasTeam"]];
  $r["bestLap"]=$teamsBLTab[$r["aliasTeam"]]==null ? 0 : $teamsBLTab[$r["aliasTeam"]];
  $teamsItems[] = $r;
}

// punkty
$query="SELECT distinct teams.alias_name alias, teams.alias aliasTeam, teams.id_team id,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,
lower(teams.team) team,teams.country_code country, place, points, points_class pointsClass, picture, '' wins, '' podium, '' pp, '' bestLap, '' drivers,";
if ($lang=='pl'){
  $query.="GROUP_CONCAT(distinct COALESCE(teams_models.team_name, 'Prywatnie') ORDER BY 1 SEPARATOR ', ') modelName";
}else{
  $query.="GROUP_CONCAT(distinct COALESCE(teams_models.team_name, 'Private') ORDER BY 1 SEPARATOR ', ') modelName";
}
$query.=" from teams_class
LEFT JOIN teams ON teams.id_team=teams_class.id_team 
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team = teams_class.id_team
AND drivers_gp_involvements.season = teams_class.season
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
where teams_class.season='$year' 
group by teams_class.id_team
order by place";
$result = mysqli_query($dbhandle,$query);
$pointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["aliasTeam"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name_alias alias, gp.name gp, gp.id_gp id
  from gp_season left join gp on gp.id_gp=gp_season.id_gp where gp_season.season=$year order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPoints=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $tmp_id_gp = $r2["id"];

    $query5="SELECT SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_team=teams.id_team AND drivers_sprint_results.season=$year and id_gp=$tmp_id_gp),0) points, teams.alias aliasTeam,excluded_from_team_class
    from drivers_gp_results,teams
    where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year'
    and id_gp=$tmp_id_gp
    group by teams.id_team,id_gp order by 1 desc";
    $result5 = mysqli_query($dbhandle,$query5);
    $r2["teamPoints"] = "-";
    $cnt=0;
    while($r5 = mysqli_fetch_assoc($result5)) {
      if ($r5["aliasTeam"] == $tmp_id_team) {
        if ($r5["points"] > 0 && $r5["excluded_from_team_class"] == 1){
          $r2["teamPoints"] = "(".$r5["points"].")";
        }else{
          $r2["teamPoints"] = $r5["points"];
        }
        break;
      }
    }

    $gpPoints[]=$r2;
  }
  $r["gpPoints"]=$gpPoints;
  $r["wins"]=$teamsWinsTab[$r["aliasTeam"]]==null ? 0 : $teamsWinsTab[$r["aliasTeam"]];
  $r["podium"]=$teamsPodiumsTab[$r["aliasTeam"]]==null ? 0 : $teamsPodiumsTab[$r["aliasTeam"]];
  $r["pp"]=$teamsPolePosTab[$r["aliasTeam"]]==null ? 0 : $teamsPolePosTab[$r["aliasTeam"]];
  $r["bestLap"]=$teamsBLTab[$r["aliasTeam"]]==null ? 0 : $teamsBLTab[$r["aliasTeam"]];
  $pointsItems[] = $r;
}

$classTeams["gp"]=$gpItems;
$classTeams["regulations"]=$regulations["regulations"];
$classTeams["classPlaces"]=$teamsItems;
$classTeams["classPoints"]=$pointsItems;

$classTeams["createTime"]=microtime(true)-$start_time;

// Response
$response = $classTeams;

print json_encode($response);
mysqli_free_result($result);
?>
