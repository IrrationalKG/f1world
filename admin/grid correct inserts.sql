UPDATE `drivers_gp_involvements` SET `id_team_model` = '588', `id_tyre` = '1' WHERE season=2022 AND id_team=26;
UPDATE `drivers_gp_involvements` SET `number` = 45 WHERE season=2022 AND id_driver=995;


INSERT INTO drivers_gp_starting_grid (id_driver, id_team, team, id_gp, season, grid_pos, grid_add_info)
SELECT id_driver, id_team, team, id_gp, season, qual_pos, qual_add_info
FROM drivers_pp_results
WHERE drivers_pp_results.season=2022 and drivers_pp_results.id_gp=87;


INSERT INTO drivers_gp_starting_grid (id_driver, id_team, team, id_gp, season, grid_pos, grid_add_info)
SELECT id_driver, id_team, team, id_gp, season, sprint_pos, ''
FROM drivers_sprint_results
WHERE drivers_sprint_results.season=2022 and drivers_sprint_results.id_gp=87;

INSERT INTO drivers_sprint_results (id_driver, id_team, team, id_gp, season, sprint_pos, sprint_add_info, sprint_time, sprint_completed, sprint_date)
SELECT id_driver, id_team, team, id_gp, season, qual_pos, qual_add_info, qual_time, qual_completed, qual_date
FROM drivers_pp_results
WHERE drivers_pp_results.season=2021 and drivers_pp_results.id_gp=61;