/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Grid, Icon } from "semantic-ui-react";
import PropTypes from "prop-types";
import styles from "./SectionPageBanner.less";

export default class SectionPageBanner extends Component {
  render() {
    const { label, title, subtitle, linkPrev, linkNext } = this.props;
    return (
      <div className="section-page-banner">
        <div className="section-page-banner-text">
          <Grid stackable centered>
            <Grid.Column mobile={16} tablet={4} computer={3} className="box1">
              <Grid verticalAlign="middle">
                <Grid.Row stretched>
                  {linkPrev && (
                    <Grid.Column width={2} textAlign="left">
                      <NavLink to={linkPrev}>
                        <Icon link name="chevron left" size="large" />
                      </NavLink>
                    </Grid.Column>
                  )}

                  <Grid.Column
                    width={linkPrev && linkNext ? 12 : 16}
                    textAlign="center"
                  >
                    <h2>{title}</h2>
                  </Grid.Column>

                  {linkNext && (
                    <Grid.Column width={2} textAlign="right">
                      <NavLink to={linkNext}>
                        <Icon link name="chevron right" size="large" />
                      </NavLink>
                    </Grid.Column>
                  )}
                </Grid.Row>
              </Grid>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={9} computer={11} className="box2">
              <h2>{subtitle}</h2>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={3} computer={2} className="box2">
              {label && <div className="section-page-info">{label}</div>}
            </Grid.Column>
          </Grid>
        </div>
      </div>
    );
  }
}

SectionPageBanner.propTypes = {
  label: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  subtitle: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  linkPrev: PropTypes.string,
  linkNext: PropTypes.string,
};
