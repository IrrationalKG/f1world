/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Segment, Flag, Loader, Image, Table } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchDriversCompareTeammates } from "../../actions/DriversCompareTeammatesActions";
import { Adsense } from "@ctrl/react-adsense";
import { FormattedMessage } from "react-intl";
import styles from "./DriversCompareTeammates.less";

class DriversCompareTeammates extends Component {
  componentDidMount() {
    const { props } = this;
    props.fetchDriversCompareTeammates(props.year);
  }

  renderDriversComparison() {
    const items = [];
    const { props } = this;
    const { drivers } = props.driversCompareTeammates.data;
    drivers &&
      drivers.forEach((item) => {
        const picDriver1 = `/build/images/drivers/driver_${item.driver1.id}_profile.jpg`;
        const picDriver2 = `/build/images/drivers/driver_${item.driver2.id}_profile.jpg`;
        const linkDriver1 = `/driver/${item.driver1.alias}`;
        const linkDriver2 = `/driver/${item.driver2.alias}`;
        const element = (
          <Fragment key={item.team}>
            <Table.Row>
              <Table.Cell colSpan="3">
                <div className="team-name">
                  {item.name} {item.engine}
                </div>
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>
                <div className="center-aligned">
                  <NavLink to={linkDriver1}>
                    <Image
                      size="small"
                      centered
                      src={picDriver1}
                      alt={`${item.driver1.name} ${item.driver1.surname}`}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/drivers/driver_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  <h3>
                    <NavLink to={linkDriver1}>
                      <div>{item.driver1.name}</div>
                      <div>{item.driver1.surname}</div>
                      <Flag name={item.driver1.countryCode} />
                    </NavLink>
                  </h3>
                </div>
              </Table.Cell>
              <Table.Cell className="versus-container">
                <div className="center-aligned">
                  <div className="versus">
                    {item.driver1.race}
                    {":"}
                    {item.driver2.race}
                  </div>
                  <div>
                    <FormattedMessage id={"app.stats.race"} />
                  </div>
                  <div className="versus">
                    {item.driver1.qual}
                    {":"}
                    {item.driver2.qual}
                  </div>
                  <div>
                    <FormattedMessage id={"app.stats.qual"} />
                  </div>
                </div>
              </Table.Cell>
              <Table.Cell>
                <div className="center-aligned">
                  <NavLink to={linkDriver2}>
                    <Image
                      size="small"
                      centered
                      src={picDriver2}
                      alt={`${item.driver2.name} ${item.driver2.surname}`}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/drivers/driver_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  <h3>
                    <NavLink to={linkDriver2}>
                      <div>{item.driver2.name}</div>
                      <div>{item.driver2.surname}</div>
                      <Flag name={item.driver2.countryCode} />
                    </NavLink>
                  </h3>
                </div>
              </Table.Cell>
            </Table.Row>
          </Fragment>
        );
        items.push(element);
      });
    return items;
  }

  render() {
    const { props } = this;
    if (!props.driversCompareTeammates.data) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }

    return (
      <section id="drivers-compare-teammates" name="drivers-compare-teammates">
        <div className="drivers-compare-teammates-header">
          <h2>
            <FormattedMessage
              id={"app.page.home.drivers.compare.teammates.title"}
            />
          </h2>
          <h3>
            <FormattedMessage id={"app.stats.season"} /> {props.year}
          </h3>
        </div>
        <div className="drivers-compare-teammates-content">
          <Segment basic textAlign="center">
            <Table celled unstackable fixed>
              <Table.Body>{this.renderDriversComparison()}</Table.Body>
            </Table>
            <div className="hideForMobile">
              <div className="adv">
                <FormattedMessage id={"app.advert"} />
              </div>
              <Adsense client="ca-pub-6611514323676676" slot="5517130189" />
            </div>
          </Segment>
        </div>
      </section>
    );
  }
}

DriversCompareTeammates.propTypes = {
  fetchDriversCompareTeammates: PropTypes.func.isRequired,
  driversCompareTeammates: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ driversCompareTeammates }) {
  return { driversCompareTeammates };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchDriversCompareTeammates }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DriversCompareTeammates);
