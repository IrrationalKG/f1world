// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GRAND_PRIX_REQUEST = "FETCH_GRAND_PRIX_REQUEST";
export const FETCH_GRAND_PRIX_SUCCESS = "FETCH_GRAND_PRIX_SUCCESS";
export const FETCH_GRAND_PRIX_ERROR = "FETCH_GRAND_PRIX_ERROR";

function requestData() {
  return {
    type: FETCH_GRAND_PRIX_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GRAND_PRIX_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GRAND_PRIX_ERROR,
    payload: error,
  };
}

export function fetchGrandPrix(lang) {
  const url = `${ROOT_URL}/get_gp_list.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
