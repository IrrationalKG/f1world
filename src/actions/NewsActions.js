// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_NEWS = "FETCH_NEWS";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_ERROR = "FETCH_NEWS_ERROR";

function requestData() {
  return {
    type: FETCH_NEWS,
  };
}

function receiveData(data) {
  return {
    type: FETCH_NEWS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_NEWS_ERROR,
    payload: error,
  };
}

export function fetchNews(lang) {
  const url = `${ROOT_URL}/get_news.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
