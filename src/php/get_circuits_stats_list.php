<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// liczba gp
$query="SELECT count(name) races,lower(circuit_alias) id FROM gp_season,gp
WHERE gp_season.id_gp=gp.id_gp and gp_season.date<CURDATE() GROUP BY gp.circuit_alias ORDER BY circuit";
$result = mysqli_query($dbhandle,$query);
$circuitsRacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_gp = $r["id"];
  $circuitsRacesTab[$tmp_id_gp] = $r["races"];
}

// lata rozgrywania
$query="SELECT CONCAT(MIN(season),' - ',MAX(season)) seasons,lower(circuit_alias) id FROM gp_season,gp
WHERE gp_season.id_gp=gp.id_gp GROUP BY gp.circuit_alias ORDER BY circuit";
$result = mysqli_query($dbhandle,$query);
$circuitsSeasonsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_gp = $r["id"];
  $circuitsSeasonsTab[$tmp_id_gp] = $r["seasons"];
}

$query="SELECT DISTINCT circuit name,track, circuit_alias alias,lower(circuit_alias) id,'' races, '' seasons
FROM gp GROUP BY circuit ORDER BY circuit ASC";
$result = mysqli_query($dbhandle,$query);
$circuitItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["races"]=$circuitsRacesTab[$r["id"]]==null ? 0 : $circuitsRacesTab[$r["id"]];
  $r["seasons"]=$circuitsSeasonsTab[$r["id"]]==null ? 0 : $circuitsSeasonsTab[$r["id"]];
  $circuitItems[] = $r;
}

// Response
$response = $circuitItems;

print json_encode($response);
mysqli_free_result($result);
?>
