// // JSON
// const request = require('../json/awards.json');
//
// export const FETCH_CONTEST_AWARDS = 'FETCH_CONTEST_AWARDS';
//
// export function fetchContestAwards() {
//   return {
//     type: FETCH_CONTEST_AWARDS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_CONTEST_AWARDS_REQUEST = 'FETCH_CONTEST_AWARDS';
export const FETCH_CONTEST_AWARDS_SUCCESS = 'FETCH_CONTEST_AWARDS_SUCCESS';
export const FETCH_CONTEST_AWARDS_ERROR = 'FETCH_CONTEST_AWARDS_ERROR';

function requestData() {
  return {
    type: FETCH_CONTEST_AWARDS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_AWARDS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_AWARDS_ERROR,
    payload: error
  };
}

export function fetchContestAwards() {
  const url = `${ROOT_URL}/get_contest_awards.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
