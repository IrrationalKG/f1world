// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_DRIVERS_STATS_REQUEST = "FETCH_DRIVERS_STATS_REQUEST";
export const FETCH_DRIVERS_STATS_SUCCESS = "FETCH_DRIVERS_STATS_SUCCESS";
export const FETCH_DRIVERS_STATS_ERROR = "FETCH_DRIVERS_STATS_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVERS_STATS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVERS_STATS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVERS_STATS_ERROR,
    payload: error,
  };
}

export function fetchDriversStats(letter, lang) {
  const url = `${ROOT_URL}/get_drivers_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          letter: letter,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}

export function fetchDriversStatsByCountry(country, lang) {
  const url = `${ROOT_URL}/get_drivers_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          country: country,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
