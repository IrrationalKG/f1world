import { FETCH_GRAND_PRIX_REQUEST, FETCH_GRAND_PRIX_SUCCESS, FETCH_GRAND_PRIX_ERROR } from '../actions/GrandPrixActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_GRAND_PRIX_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_GRAND_PRIX_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_GRAND_PRIX_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
