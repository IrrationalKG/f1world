<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// grand prix
$query="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, name_alias alias from gp,gp_season where gp.id_gp=gp_season.id_gp and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}

// punkty - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country, place, points, points_class pointsClass,
GROUP_CONCAT(
  DISTINCT teams.alias_name SEPARATOR ', '
) teamsAliases,
GROUP_CONCAT(
  DISTINCT TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) SEPARATOR ', '
) teamsNames
from drivers_class, drivers, teams, drivers_gp_involvements
where drivers_class.season='$year' 
and drivers.id_driver=drivers_class.id_driver 
AND drivers_gp_involvements.id_driver = drivers_class.id_driver 
AND drivers_gp_involvements.season = drivers_class.season 
AND teams.id_team = drivers_gp_involvements.id_team
GROUP BY drivers_class.id_driver
order by place";
$result = mysqli_query($dbhandle,$query);
$driversPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id"];
  $query2="SELECT gp.country_code as name, lower(gp.name_short) nameShort, gp.name as gp, 
  drivers_gp_results.race_points,
  drivers_sprint_results.sprint_points,
  COALESCE(drivers_gp_results.race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0) as points,
  COALESCE(drivers_gp_results.excluded_from_class,0) as excluded_from_class,
  drivers_gp_results.race_completed completed,
  drivers_gp_results.race_time time,
  drivers_gp_results.race_add_info info
  from gp_season 
  left join gp on gp.id_gp=gp_season.id_gp 
  left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp
  and drivers_gp_results.id_driver=$tmp_id_driver 
  and drivers_gp_results.season='$year'
  left join drivers_sprint_results on drivers_sprint_results.id_gp=gp.id_gp
  and drivers_sprint_results.id_driver=$tmp_id_driver 
  and drivers_sprint_results.season=$year
  where gp_season.season=$year
  group by gp_season.id_gp
  order by gp_season.sort, drivers_gp_results.race_pos";
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPoints=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
     if($r2["race_points"]===null && $r2["sprint_points"]===null) {
      $r2["points"] = null;
     }else{
      if ($r2["excluded_from_class"]== 1){
        $r2["points"] = "(".$r2["points"].")";
      }else{
        $r2["points"] = $r2["points"];
      }
    }
    $gpPoints[] = $r2;
  }
  $r["gpPoints"]=$gpPoints;
  $driversPointsItems[] = $r;
}

// punkty - zespoly
$query="SELECT distinct teams.alias_name alias,teams.id_team id,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,
lower(teams.team) team,teams.country_code country, place, points, points_class pointsClass,
GROUP_CONCAT(DISTINCT teams_models.team_name ORDER BY teams_models.team_name SEPARATOR ', ') modelName
from teams_class 
LEFT JOIN teams ON teams.id_team = teams_class.id_team
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team=teams_class.id_team AND drivers_gp_involvements.season=teams_class.season
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
where teams_class.season='$year' 
GROUP BY teams.id_team
order by place";
$result = mysqli_query($dbhandle,$query);
$teamsPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, gp.id_gp id
  from gp_season left join gp on gp.id_gp=gp_season.id_gp where gp_season.season=$year order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPoints=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $tmp_id_gp = $r2["id"];

    $query5="SELECT SUM(race_team_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_team=teams.id_team AND drivers_sprint_results.season=$year and id_gp=$tmp_id_gp),0) as points,
    lower(teams.team) team, teams.id_team id, excluded_from_team_class from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team
    and season='$year' and id_gp=$tmp_id_gp group by teams.id_team,id_gp order by 1 desc";
    $result5 = mysqli_query($dbhandle,$query5);
    $r2["points"] = null;
    $cnt=0;
    while($r5 = mysqli_fetch_assoc($result5)) {
      if ($r5["id"] == $tmp_id_team) {
        if ($r5["excluded_from_team_class"]== 1){
          $r2["points"] = "(".$r5["points"].")";
        }else{
          $r2["points"] = $r5["points"];
        }
        break;
      }
    }

    $gpPoints[]=$r2;
  }
  $r["gpPoints"]=$gpPoints;
  $teamsPointsItems[] = $r;
}

// regulamin
$query="SELECT";
if ($lang=='pl') {
  $query.=" regulations";
}else{
  $query.=" regulations_en as regulations";
}  
$query.=" from season_regulations where year='$year'";
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $regulations = $r["regulations"];
}

$seasonPoints["gp"]=$gpItems;
$seasonPoints["classDriversPoints"]=$driversPointsItems;
$seasonPoints["classTeamsPoints"]=$teamsPointsItems;
$seasonPoints["regulations"]=$regulations;
$seasonPoints["createTime"]=microtime(true)-$start_time;

// Response
$response = $seasonPoints;

print json_encode($response);
mysqli_free_result($result);
?>
