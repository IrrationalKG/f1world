// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_MENU = "FETCH_MENU";
export const FETCH_MENU_SUCCESS = "FETCH_MENU_SUCCESS";
export const FETCH_MENU_ERROR = "FETCH_MENU_ERROR";

function requestData() {
  return {
    type: FETCH_MENU,
  };
}

function receiveData(data) {
  return {
    type: FETCH_MENU_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_MENU_ERROR,
    payload: error,
  };
}

export function fetchMenu(year, gp, lang) {
  const url = `${ROOT_URL}/get_menu.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          gp: gp,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
