import { FETCH_NEXTGP, FETCH_NEXTGP_SUCCESS, FETCH_NEXTGP_ERROR } from '../actions/NextGPActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_NEXTGP:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_NEXTGP_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_NEXTGP_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
