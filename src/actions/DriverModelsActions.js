// PHP
import axios from "axios";
import { ROOT_URL } from '../config/config';

export const FETCH_DRIVER_MODELS_REQUEST = "FETCH_DRIVER_MODELS_REQUEST";
export const FETCH_DRIVER_MODELS_SUCCESS = "FETCH_DRIVER_MODELS_SUCCESS";
export const FETCH_DRIVER_MODELS_ERROR = "FETCH_DRIVER_MODELS_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVER_MODELS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVER_MODELS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVER_MODELS_ERROR,
    payload: error,
  };
}

export function fetchDriverModels(id) {
  const url = `${ROOT_URL}/get_driver_models.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
