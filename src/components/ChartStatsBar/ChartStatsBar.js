/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
import PropTypes from "prop-types";
import styles from "./ChartStatsBar.less";

export default class ChartStatsBar extends Component {
  render() {
    const { values, seriesName, height } = this.props;

    const chartHeight = height == undefined ? "200px" : height;

    const columnColors = (function () {
      const colors = [];
      let i;
      for (i = 0; i < 5; i += 1) {
        colors.push(ReactHighcharts.Highcharts.Color("#ebdda7").get());
      }
      return colors;
    })();

    const chartData = [];
    values.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name.toUpperCase();
      value.y = parseInt(item.y, 0);
      return chartData.push(value);
    });

    const configChart = {
      chart: {
        height: chartHeight,
        type: "bar",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: 0,
          staggerLines: 1
        },
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        title: {
          text: " ",
        },
        labels: {
          enabled: false,
        },
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: false,
        itemMarginBottom: 50,
      },
      tooltip: {
        headerFormat: `<b>{point.key}</b><br/>`,
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        bar: {
          colorByPoint: true,
          colors: columnColors,
        },
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: seriesName,
          pointWidth: 15,
          color: "#cbb973",
          data: chartData,
        },
      ],
    };

    return (
      <div className="section-page-content charts-content">
        <ReactHighcharts config={configChart} />
      </div>
    );
  }
}

ChartStatsBar.propTypes = {
  values: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  seriesName: PropTypes.string,
  tooltipLabel: PropTypes.string,
};
