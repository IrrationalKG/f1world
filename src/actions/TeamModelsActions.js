// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TEAM_MODELS_REQUEST = "FETCH_TEAM_MODELS_REQUEST";
export const FETCH_TEAM_MODELS_SUCCESS = "FETCH_TEAM_MODELS_SUCCESS";
export const FETCH_TEAM_MODELS_ERROR = "FETCH_TEAM_MODELS_ERROR";

function requestData() {
  return {
    type: FETCH_TEAM_MODELS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TEAM_MODELS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TEAM_MODELS_ERROR,
    payload: error,
  };
}

export function fetchTeamModels(id, lang) {
  const url = `${ROOT_URL}/get_team_models.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
