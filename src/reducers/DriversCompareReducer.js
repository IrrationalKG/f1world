import { FETCH_DRIVERS_COMPARE_REQUEST, FETCH_DRIVERS_COMPARE_SUCCESS, FETCH_DRIVERS_COMPARE_ERROR } from '../actions/DriversCompareActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_DRIVERS_COMPARE_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_DRIVERS_COMPARE_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_DRIVERS_COMPARE_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
