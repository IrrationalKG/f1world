/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Segment,
  Item,
  Flag,
  Statistic,
  Message,
  Image,
  Label,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchDriversRecords } from "../../actions/DriversRecordsActions";
import { fetchTeamsRecords } from "../../actions/TeamsRecordsActions";
const cmbStatsDrivers = require("../../json/combo_grouped_stats.json");
const cmbStatsDriversEn = require("../../json/combo_grouped_stats_en.json");
const cmbStatsTeams = require("../../json/combo_grouped_stats_teams.json");
const cmbStatsTeamsEn = require("../../json/combo_grouped_stats_teams_en.json");
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./RecordsWidget.less";

class RecordsWidget extends Component {
  constructor(props) {
    super(props);

    const comboStatsDrivers =
      Cookies.get("lang") == "pl" ? cmbStatsDrivers : cmbStatsDriversEn;
    const comboStatsTeams =
      Cookies.get("lang") == "pl" ? cmbStatsTeams : cmbStatsTeamsEn;

    let driversParamStatValue;
    while (!driversParamStatValue) {
      driversParamStatValue = this.getRandomDriversStat(comboStatsDrivers);
      if (driversParamStatValue) break;
    }

    let teamsParamStatValue;
    while (!teamsParamStatValue) {
      teamsParamStatValue = this.getRandomTeamsStat(comboStatsTeams);
      if (teamsParamStatValue) break;
    }

    let driversParamTeamValue = "-";
    let driversParamGPValue = "-";
    let driversParamCircuitValue = "-";
    let driversParamBeginYearValue = "-";
    let driversParamStatusValue = "0";

    const driverRandomPlace = Math.random();
    let driversParamPlaceValue = Math.floor(
      driverRandomPlace > 0 ? driverRandomPlace * 20 : 1
    );

    let teamsParamTeamValue = "-";
    let teamsParamGPValue = "-";
    let teamsParamCircuitValue = "-";
    let teamsParamBeginYearValue = "-";
    let teamsParamStatusValue = "0";

    const teamRandomPlace = Math.random();
    let teamsParamPlaceValue = Math.floor(
      teamRandomPlace > 0 ? teamRandomPlace * 20 : 1
    );

    this.state = {
      comboStatsDrivers: comboStatsDrivers,
      comboStatsTeams: comboStatsTeams,

      driversParamStatValue: driversParamStatValue.value,
      driversParamPlaceValue: driversParamPlaceValue,
      driversParamTeamValue: driversParamTeamValue,
      driversParamGPValue: driversParamGPValue,
      driversParamCircuitValue: driversParamCircuitValue,
      driversParamBeginYearValue: driversParamBeginYearValue,
      driversParamStatusValue: driversParamStatusValue,

      teamsParamStatValue: teamsParamStatValue.value,
      teamsParamPlaceValue: teamsParamPlaceValue,
      teamsParamTeamValue: teamsParamTeamValue,
      teamsParamGPValue: teamsParamGPValue,
      teamsParamCircuitValue: teamsParamCircuitValue,
      teamsParamBeginYearValue: teamsParamBeginYearValue,
      teamsParamStatusValue: teamsParamStatusValue,
    };
  }

  componentDidMount() {
    const { props, state } = this;

    props.fetchDriversRecords(
      "1",
      state.driversParamStatValue,
      state.driversParamPlaceValue,
      "-",
      "-",
      "-",
      "-",
      "-",
      "0",
      Cookies.get("lang")
    );
    props.fetchTeamsRecords(
      "1",
      state.teamsParamStatValue,
      state.teamsParamPlaceValue,
      "-",
      "-",
      "-",
      "-",
      "0",
      Cookies.get("lang")
    );
  }

  getRandomDriversStat(comboStatsDrivers) {
    const statsCategorySize = comboStatsDrivers?.length;
    const randomStatsCategoryIdx =
      Math.floor(Math.random() * statsCategorySize) - 1;

    const statsFromCategory =
      comboStatsDrivers[randomStatsCategoryIdx]?.options;

    if (!statsFromCategory) return;

    const statsFromCategorySize = statsFromCategory?.length;
    const randomStatsFromCategoryIdx =
      Math.floor(Math.random() * statsFromCategorySize) - 1;
    return statsFromCategory[randomStatsFromCategoryIdx];
  }

  getRandomTeamsStat(comboStatsTeams) {
    const statsCategorySize = comboStatsTeams?.length;
    const randomStatsCategoryIdx =
      Math.floor(Math.random() * statsCategorySize) - 1;

    const statsFromCategory = comboStatsTeams[randomStatsCategoryIdx]?.options;

    if (!statsFromCategory) return;

    const statsFromCategorySize = statsFromCategory?.length;
    const randomStatsFromCategoryIdx =
      Math.floor(Math.random() * statsFromCategorySize) - 1;
    return statsFromCategory[randomStatsFromCategoryIdx];
  }

  findObjectById(arr, value) {
    for (const obj of arr) {
      if (obj.value === value) {
        return obj;
      }
      if (obj.options && obj.options.length > 0) {
        const result = this.findObjectById(obj.options, value);
        if (result) {
          return result;
        }
      }
    }
    return null;
  }

  renderDriversRecords() {
    const elements = [];
    const { props, state } = this;
    const { items } = props.driversRecords.data;

    const driverStatsName = this.findObjectById(
      state.comboStatsDrivers,
      state.driversParamStatValue
    );

    const filteredItems = items.slice(0, 10);
    filteredItems.forEach((item, idx) => {
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const link = `/driver/${item.alias}`;
      let active = "";
      if (item.active === "1") {
        active = "active-driver";
      }
      const element = (
        <Item id={idx} key={idx}>
          <Segment basic className="image-link" size="small">
            <NavLink to={link}>
              <Image
                centered
                size="small"
                src={filename}
                alt={item.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
              {item.active == 1 && (
                <Label
                  color="yellow"
                  attached="bottom"
                  size="tiny"
                  className="center-aligned"
                >
                  <FormattedMessage id={"app.stats.active"} />
                </Label>
              )}
            </NavLink>
          </Segment>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.place}
                {". "}
                <Flag name={item.countryCode} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.country}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>{item.amount}</Statistic.Value>
              <Statistic.Label className="no-wrap">
                {driverStatsName.shortLabel}
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    if (items.length === 0) {
      const element = (
        <Message key="message-info">
          <Message.Header>
            <FormattedMessage id={"app.message.header"} />
          </Message.Header>
          <p>
            <FormattedMessage id={"app.placeholder.no.results"} />
          </p>
        </Message>
      );
      elements.push(element);
    }
    return elements;
  }

  renderTeamsRecords() {
    const elements = [];
    const { props, state } = this;
    const { items } = props.teamsRecords.data;

    const teamStatsName = this.findObjectById(
      state.comboStatsTeams,
      state.teamsParamStatValue
    );

    const filteredItems = items.slice(0, 10);
    filteredItems.forEach((item, idx) => {
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.id}_profile_logo.jpg`;
      }
      const link = `/team/${item.alias}`;
      let active = "";
      if (item.active === "1") {
        active = "active-team";
      }
      const countries = item.countryCode.split(",");
      const element = (
        <Item id={idx} key={idx}>
          <Segment basic className="image-link" size="small">
            <NavLink to={link}>
              <Image
                centered
                size="small"
                src={filename}
                alt={item.name}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
              {item.active == 1 && (
                <Label
                  color="yellow"
                  attached="bottom"
                  size="tiny"
                  className="center-aligned"
                >
                  <FormattedMessage id={"app.stats.active"} />
                </Label>
              )}
            </NavLink>
          </Segment>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.place}
                {". "}
                {this.renderCountriesFlags(countries)}
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.country}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                {Math.round(item.amount * 100, 2) / 100}
              </Statistic.Value>
              <Statistic.Label>{teamStatsName.shortLabel}</Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    if (items.length === 0) {
      const element = (
        <Message key="message-info">
          <Message.Header>
            <FormattedMessage id={"app.message.header"} />
          </Message.Header>
          <p>
            <FormattedMessage id={"app.placeholder.no.results"} />
          </p>
        </Message>
      );
      elements.push(element);
    }
    return elements;
  }

  renderCountriesFlags(countries) {
    const items = [];
    countries.forEach((country) => {
      const record = <Flag key={country} name={country} />;
      items.push(record);
    });
    return items;
  }

  render() {
    const { driversRecords, teamsRecords } = this.props;
    const { state } = this;
    if (
      !driversRecords.data ||
      driversRecords.loading ||
      !teamsRecords.data ||
      teamsRecords.loading
    ) {
      return (
        <section
          id="records-widget"
          name="records-widget"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }

    let driverStatsName = this.findObjectById(
      state.comboStatsDrivers,
      state.driversParamStatValue
    );
    if (driverStatsName.label.includes("...")) {
      driverStatsName.label =
        driverStatsName.label.slice(0, -3) + " " + state.driversParamPlaceValue;
    }

    let teamStatsName = this.findObjectById(
      state.comboStatsTeams,
      state.teamsParamStatValue
    );
    if (teamStatsName.label.includes("...")) {
      teamStatsName.label =
        teamStatsName.label.slice(0, -3) + " " + state.teamsParamPlaceValue;
    }

    return (
      <section
        id="records-widget"
        name="records-widget"
        className="section-page"
      >
        <div className="records-widget-header">
          <h2>
            <FormattedMessage id={"app.page.home.records.widget.title"} />
          </h2>
          <h3>
            <FormattedMessage id={"app.page.home.records.widget.subtitle"} />
          </h3>
        </div>
        <div className="records-widget-content">
          <Grid stackable centered>
            <Grid.Column mobile={16} tablet={16} computer={8}>
              <Segment basic className="records-widget-drivers">
                <SectionPageHeader
                  title={
                    <FormattedMessage
                      id={"app.page.home.records.widget.drivers"}
                    />
                  }
                />
                <SectionPageHeader
                  title={driverStatsName.label}
                  type="secondary"
                />
                <Item.Group divided unstackable>
                  {this.renderDriversRecords()}
                </Item.Group>
                <div className="buttons">
                  <NavLink
                    className="primary"
                    to={`/drivers-records/${state.driversParamStatValue}/${state.driversParamTeamValue}/${state.driversParamGPValue}/${state.driversParamCircuitValue}/${state.driversParamBeginYearValue}/-/${state.driversParamStatusValue}/${state.driversParamPlaceValue}/`}
                  >
                    <FormattedMessage id={"app.button.more"} />
                  </NavLink>
                </div>
              </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={16} computer={8}>
              <Segment basic className="records-widget-teams">
                <SectionPageHeader
                  title={
                    <FormattedMessage
                      id={"app.page.home.records.widget.teams"}
                    />
                  }
                />
                <SectionPageHeader
                  title={teamStatsName.label}
                  type="secondary"
                />
                <Item.Group divided unstackable>
                  {this.renderTeamsRecords()}
                </Item.Group>
                <div className="buttons">
                  <NavLink
                    className="primary"
                    to={`/teams-records/${state.teamsParamStatValue}/${state.teamsParamGPValue}/${state.teamsParamCircuitValue}/${state.teamsParamBeginYearValue}/-/${state.teamsParamStatusValue}/${state.teamsParamPlaceValue}/`}
                  >
                    <FormattedMessage id={"app.button.more"} />
                  </NavLink>
                </div>
              </Segment>
            </Grid.Column>
          </Grid>
        </div>
      </section>
    );
  }
}

RecordsWidget.propTypes = {
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      teamId: PropTypes.string,
      stat: PropTypes.string,
      gpId: PropTypes.string,
      circuitId: PropTypes.string,
      beginYear: PropTypes.string,
      endYear: PropTypes.string,
      place: PropTypes.string,
    }),
  }),
  fetchDriversRecords: PropTypes.func.isRequired,
  fetchTeamsRecords: PropTypes.func.isRequired,
  driversRecords: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  teamsRecords: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ driversRecords, teamsRecords }) {
  return {
    driversRecords,
    teamsRecords,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchDriversRecords,
      fetchTeamsRecords,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(RecordsWidget);
