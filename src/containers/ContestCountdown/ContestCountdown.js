/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["initContestCountdown"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Cookies from "js-cookie";
import styles from "./ContestCountdown.less";

class ContestCountdown extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      countdownStr: "",
    };

    this.initContestCountdown = this.initContestCountdown.bind(this);
  }

  componentDidMount() {
    setInterval(() => this.initContestCountdown(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  initContestCountdown() {
    const { props } = this;

    let time = new Date(props.date);

    let raceStarted = false;
    let nextRace = false;
    let countdownStr = "";

    // data - 1 h
    const dt = new Date();
    let timezoneoffset = dt.getTimezoneOffset();
    timezoneoffset /= 60;
    timezoneoffset = -timezoneoffset;
    dt.setTime(dt.getTime());
    if (time !== 0) {
      if (time - dt < 0) {
        time = time - dt;
        if (time < 0) {
          nextRace = true;
        } else {
          raceStarted = true;
        }
      }
      let v1 = Math.round((time - dt) / 1000);
      if (v1 < 0) {
        v1 = Math.round((dt - time) / 1000);
      }
      const sec = v1 % 60;
      v1 = Math.floor(v1 / 60);
      const min = v1 % 60;
      v1 = Math.floor(v1 / 60);
      const hour = v1 % 24;
      const day = Math.floor(v1 / 24);

      let dayStr = "";
      if (day === 1) {
        if (Cookies.get("lang") == "pl") {
          dayStr = `${day} dzień `;
        } else {
          dayStr = `${day} day `;
        }
      } else {
        if (Cookies.get("lang") == "pl") {
          dayStr = `${day} dni `;
        } else {
          dayStr = `${day} days `;
        }
      }
      let hourStr = "";
      if (hour === 0) {
        if (Cookies.get("lang") == "pl") {
          hourStr = `${hour} godz. `;
        } else {
          hourStr = `${hour} hours `;
        }
      }
      if (hour === 1) {
        if (Cookies.get("lang") == "pl") {
          hourStr = `${hour} godz. `;
        } else {
          hourStr = `${hour} hour `;
        }
      }
      if ((hour > 1 && hour < 5) || hour > 21) {
        if (Cookies.get("lang") == "pl") {
          hourStr = `${hour} godz. `;
        } else {
          hourStr = `${hour} hours `;
        }
      }
      if (hour >= 5 && hour < 22) {
        if (Cookies.get("lang") == "pl") {
          hourStr = `${hour} godz. `;
        } else {
          hourStr = `${hour} hours `;
        }
      }
      let minStr = "";
      if (min === 1) {
        minStr = `${min} min. `;
      } else {
        if (
          min === 2 ||
          min === 3 ||
          min === 4 ||
          min === 22 ||
          min === 23 ||
          min === 24 ||
          min === 32 ||
          min === 33 ||
          min === 34 ||
          min === 42 ||
          min === 43 ||
          min === 44 ||
          min === 52 ||
          min === 53 ||
          min === 54
        ) {
          minStr = `${min} min. `;
        }
        minStr = `${min} min. `;
      }
      let secStr = "";
      if (sec === 1) {
        if (Cookies.get("lang") == "pl") {
          secStr = `${sec} sek. `;
        } else {
          secStr = `${sec} sec. `;
        }
      } else {
        if (
          sec === 2 ||
          sec === 3 ||
          sec === 4 ||
          sec === 22 ||
          sec === 23 ||
          sec === 24 ||
          sec === 32 ||
          sec === 33 ||
          sec === 34 ||
          sec === 42 ||
          sec === 43 ||
          sec === 44 ||
          sec === 52 ||
          sec === 53 ||
          sec === 54
        ) {
          if (Cookies.get("lang") == "pl") {
            secStr = `${sec} sek. `;
          } else {
            secStr = `${sec} sec. `;
          }
        }
        if (Cookies.get("lang") == "pl") {
          secStr = `${sec} sek. `;
        } else {
          secStr = `${sec} sec. `;
        }
      }
      if (raceStarted === true) {
        if (Cookies.get("lang") == "pl") {
          countdownStr = "Wyścig trwa...";
        } else {
          countdownStr = "Race in progress...";
        }
      } else if (nextRace === true) {
        if (Cookies.get("lang") == "pl") {
          countdownStr = "Wyścig zakończony";
        } else {
          countdownStr = "Race finished...";
        }
      } else {
        countdownStr = `${dayStr}  ${hourStr} ${minStr}`;
      }
      this.setState({ countdownStr });
    }
  }

  render() {
    const { state } = this;
    return <div id="contest-countdown">{state.countdownStr}</div>;
  }
}

ContestCountdown.propTypes = {
  nextGP: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ nextGP }) {
  return { nextGP };
}

export default connect(mapStateToProps)(ContestCountdown);
