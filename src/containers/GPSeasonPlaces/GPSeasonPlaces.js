/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Table,
  Message,
  Image,
  Item,
  Label,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchGPSeasonPlaces } from "../../actions/GPSeasonPlacesActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPSeasonPlaces.less";

class GPSeasonPlaces extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: props.match.params.year,
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPSeasonPlaces(props.match.params.year, Cookies.get("lang"));
    props.fetchComboSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboSeasons.data?.find((e) => e.value == nextYear)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchGPSeasonPlaces(nextYear);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/gp-season/places/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderClassDriversPlaces() {
    const elements = [];
    const { props } = this;
    const { classDriversPlaces } = props.gpseasonPlaces.data;
    classDriversPlaces.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.teamsNames?.split(", ").map((teamName, index) => {
                      return (
                        <span key={teamName}>
                          {index != 0 && ", "}
                          {teamName}
                        </span>
                      );
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPlaces.map((e) => (
            <Table.Cell
              data-title={e.gp}
              key={e.gp}
              className={e.place <= 3 || e.points > 0 ? `cell-${e.place}` : ""}
            >
              {e.grid == "1" ? (
                <div className="gp-pp">
                  <FormattedMessage id={"app.table.header.polepos"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
              {e.place === null ? (
                String.fromCharCode(160)
              ) : e.place != "-" ? (
                e.place
              ) : (
                <>&mdash;</>
              )}
              {e.bestLap == "1" ? (
                <div className="gp-bl">
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
            </Table.Cell>
          ))}
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassDriversPlacesMobile() {
    const elements = [];
    const { props } = this;
    const { classDriversPlaces, gp } = props.gpseasonPlaces.data;
    classDriversPlaces.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/driver/${item.alias}`}>
            <Image
              size="small"
              src={driverPic}
              alt={item.name}
              className="cell-photo"
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.teamsNames?.split(", ").map((teamName, index) => {
                return (
                  <div key={teamName}>
                    <NavLink
                      to={`/team/${item.teamsAliases?.split(", ")[index]}`}
                    >
                      {teamName}
                    </NavLink>
                  </div>
                );
              })}
            </Item.Description>
            <Item.Description>
              {item.gpPlaces.map((e) => (
                <span key={e.gp} className="key-value-box">
                  <div className="key-value-box-header">
                    {e.nameShort.toUpperCase()}
                  </div>
                  <div
                    className={
                      e.place <= 3 || e.points > 0
                        ? `key-value-box-value cell-${e.place}`
                        : "key-value-box-value"
                    }
                  >
                    {e.place === null ? (
                      String.fromCharCode(160)
                    ) : e.place != "-" ? (
                      e.place
                    ) : (
                      <>&mdash;</>
                    )}
                    {e.grid == "1" && (
                      <Label attached="top left" color="red" size="mini">
                        &nbsp;
                        <FormattedMessage id={"app.table.header.polepos"} />
                        &nbsp;
                      </Label>
                    )}
                    {e.bestLap == "1" && (
                      <Label attached="top right" color="green" size="mini">
                        &nbsp;
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                        &nbsp;
                      </Label>
                    )}
                  </div>
                </span>
              ))}
            </Item.Description>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsPlaces() {
    const elements = [];
    const { props } = this;
    const { classTeamsPlaces } = props.gpseasonPlaces.data;
    classTeamsPlaces.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName?.split(", ").map((modelName) => {
                      return <div key={modelName}>{modelName}</div>;
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPlaces.map((e) => (
            <Table.Cell
              data-title={e.gp}
              key={e.gp}
              className={e.place <= 3 ? `cell-${e.place}` : ""}
            >
              {e.grid == "1" ? (
                <div className="gp-pp">
                  <FormattedMessage id={"app.table.header.polepos"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
              {e.place === null ? (
                String.fromCharCode(160)
              ) : e.place != "-" ? (
                e.place
              ) : (
                <>&mdash;</>
              )}
              {e.bestLap == "1" ? (
                <div className="gp-bl">
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                </div>
              ) : (
                <div>&nbsp;</div>
              )}
            </Table.Cell>
          ))}
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsPlacesMobile() {
    const elements = [];
    const { props } = this;
    const { classTeamsPlaces, gp } = props.gpseasonPlaces.data;
    classTeamsPlaces.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/team/${item.alias}`}>
            <Image
              size="small"
              src={teamPic}
              alt={item.name}
              onError={(e) => {
                e.target.src = "/build/images/teams/team_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.modelName?.split(", ").map((modelName) => {
                return <div key={modelName}>{modelName}</div>;
              })}
            </Item.Description>
            <Item.Description>
              {item.gpPlaces.map((e) => (
                <span key={e.gp} className="key-value-box">
                  <div className="key-value-box-header">
                    {e.nameShort.toUpperCase()}
                  </div>
                  <div
                    className={
                      e.place <= 3
                        ? `key-value-box-value cell-${e.place}`
                        : "key-value-box-value"
                    }
                  >
                    {e.place === null ? (
                      String.fromCharCode(160)
                    ) : e.place != "-" ? (
                      e.place
                    ) : (
                      <>&mdash;</>
                    )}
                    {e.grid == "1" && (
                      <Label attached="top left" color="red" size="mini">
                        &nbsp;
                        <FormattedMessage id={"app.table.header.polepos"} />
                        &nbsp;
                      </Label>
                    )}
                    {e.bestLap == "1" && (
                      <Label attached="top right" color="green" size="mini">
                        &nbsp;
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                        &nbsp;
                      </Label>
                    )}
                  </div>
                </span>
              ))}
            </Item.Description>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpseasonPlaces.data || props.gpseasonPlaces.loading) {
      return (
        <section
          id="gpseason-places-details"
          name="gpseason-places-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { gp, regulations } = props.gpseasonPlaces.data;

    const linkPrev = `/gp-season/places/${prevYear}`;
    const linkNext = `/gp-season/places/${nextYear}`;
    if (gp.length === 0) {
      return (
        <section
          id="gpseason-places-details"
          name="gpseason-places-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.gpseason.places.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.gpseason.places.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage id={"app.page.gpseason.points.info"} />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="gpseason-places-details"
        name="gpseason-places-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.gpseason.places.title"}
              values={{ season: year }}
            />
          }
          subtitle={
            <FormattedMessage id={"app.page.gpseason.places.subtitle"} />
          }
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic padded>
            <div className="buttons">
              <NavLink to={`/gp-season/${year}`} className="secondary">
                <FormattedMessage id={"app.button.summary"} />
              </NavLink>
              <NavLink to={`/gp-season/points/${year}`} className="secondary">
                <FormattedMessage id={"app.button.points"} />
              </NavLink>
              <NavLink to={`/gp-season/places/${year}`} className="primary">
                <FormattedMessage id={"app.button.places"} />
              </NavLink>
              <NavLink to={`/gp-season/qual/${year}`} className="secondary">
                <FormattedMessage id={"app.button.grid"} />
              </NavLink>
              <NavLink to={`/gp-season/summary/${year}`} className="secondary">
                <FormattedMessage id={"app.button.statistics"} />
              </NavLink>
              <NavLink to={`/gp-season/drivers/${year}`} className="secondary">
                <FormattedMessage id={"app.button.drivers"} />
              </NavLink>
              <NavLink to={`/gp-season/teams/${year}`} className="secondary">
                <FormattedMessage id={"app.button.teams"} />
              </NavLink>
            </div>
            <div className="gp-season-places-filters">
              <Select
                ref={(ref) => {
                  this.comboSeason = ref;
                }}
                isLoading={isLoadingExternally}
                name="seasons"
                multi={multi}
                ignoreCase={ignoreCase}
                ignoreAccents={ignoreAccents}
                value={state.selectValue}
                onChange={this.onInputChange}
                options={props.comboSeasons.data}
                clearable={clearable}
                labelKey="label"
                valueKey="value"
                placeholder={formatMessage({
                  id: "app.placeholder.select.season",
                })}
                noResultsText={
                  <FormattedMessage id={"app.placeholder.no.results"} />
                }
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>
            <div className="class-drivers-places">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.drivers"} />}
                type="secondary"
              />
              <div className="hideForDesktop">
                <Segment basic>
                  <Item.Group divided unstackable>
                    {this.renderClassDriversPlacesMobile()}
                  </Item.Group>
                </Segment>
              </div>
              <div className="hideForMobile">
                <Segment basic className="overflow">
                  <Table
                    basic="very"
                    celled
                    className="responsive-table center-aligned"
                  >
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell className="cell-header-number">
                          #
                        </Table.HeaderCell>
                        <Table.HeaderCell className="left">
                          <FormattedMessage id={"app.table.header.driver"} />
                        </Table.HeaderCell>
                        {gp.map((item) => (
                          <Table.HeaderCell
                            data-title={item.gp}
                            key={item.alias}
                            className="cell-default-size"
                          >
                            <NavLink to={`/gp-result/${item.alias}/${year}`}>
                              {item.nameShort}
                            </NavLink>
                            <br />
                            <Flag name={item.name} />
                          </Table.HeaderCell>
                        ))}
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>{this.renderClassDriversPlaces()}</Table.Body>
                  </Table>
                </Segment>
              </div>
            </div>
            <div className="class-teams-places">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.teams"} />}
                type="secondary"
              />
              {year > 1957 ? (
                <>
                  <div className="hideForDesktop">
                    <Segment basic>
                      <Item.Group divided unstackable>
                        {this.renderClassTeamsPlacesMobile()}
                      </Item.Group>
                    </Segment>
                  </div>
                  <div className="hideForMobile">
                    <Segment basic className="overflow">
                      <Table
                        basic="very"
                        celled
                        className="responsive-table center-aligned"
                      >
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell className="cell-header-number">
                              #
                            </Table.HeaderCell>
                            <Table.HeaderCell className="left">
                              <FormattedMessage id={"app.table.header.team"} />
                            </Table.HeaderCell>
                            {gp.map((item) => (
                              <Table.HeaderCell
                                data-title={item.gp}
                                key={item.alias}
                                className="cell-default-size"
                              >
                                <NavLink
                                  to={`/gp-result/${item.alias}/${year}`}
                                >
                                  {item.nameShort}
                                </NavLink>
                                <br />
                                <Flag name={item.name} />
                              </Table.HeaderCell>
                            ))}
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>{this.renderClassTeamsPlaces()}</Table.Body>
                      </Table>
                    </Segment>
                  </div>
                </>
              ) : (
                <Message icon>
                  <Icon name="warning circle" />
                  <Message.Content>
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <p>
                      <FormattedMessage
                        id={"app.page.gpseason.places.info2"}
                        values={{ season: year }}
                      />
                    </p>
                  </Message.Content>
                </Message>
              )}
            </div>
            <Segment padded basic>
              <Regulations regulations={regulations}></Regulations>
            </Segment>
          </Segment>
        </div>
      </section>
    );
  }
}

GPSeasonPlaces.propTypes = {
  fetchGPSeasonPlaces: PropTypes.func.isRequired,
  gpseasonPlaces: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboSeasons: PropTypes.func.isRequired,
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpseasonPlaces, comboSeasons }) {
  return { gpseasonPlaces, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchGPSeasonPlaces, fetchComboSeasons },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPSeasonPlaces)
);
