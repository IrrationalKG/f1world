// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const SAVE_CONTEST_REGISTER = 'SAVE_CONTEST_REGISTER';
export const SAVE_CONTEST_REGISTER_SUCCESS = 'SAVE_CONTEST_REGISTER_SUCCESS';
export const SAVE_CONTEST_REGISTER_ERROR = 'SAVE_CONTEST_REGISTER_ERROR';

function requestData() {
  return {
    type: SAVE_CONTEST_REGISTER
  };
}

function receiveData(data) {
  return {
    type: SAVE_CONTEST_REGISTER_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: SAVE_CONTEST_REGISTER_ERROR,
    payload: error
  };
}

export function saveContestRegister(data) {
  const url = `${ROOT_URL}/post_contest_register.php`;
  const user = JSON.stringify(data);
  return (dispatch) => {
    dispatch(requestData());
    axios.post(url, user, {
      headers: {
        'Content-Type': 'application/json',
        // 'Access-Control-Request-Methods': 'GET,HEAD,OPTIONS,POST,PUT'
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
