<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

/**
*-------------
* Kierowcy
*-------------
**/
// starty
$query="select id_driver,count(distinct race_date) as starts from drivers_gp_results where season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversStartsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_driver = $r["id_driver"];
	$driversStartsTabM[$tmp_id_driver] = $r["starts"];
}
// kwalifikacje
$query="select id_driver,count(distinct qual_date) as qual from drivers_pp_results where season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversQualTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_driver = $r["id_driver"];
	$driversQualTabM[$tmp_id_driver] = $r["qual"];
}
// punkty
$query="select drivers.id_driver,TRUNCATE(ROUND(SUM(points),2),2) race_pts from drivers_class,drivers
where drivers.id_driver=drivers_class.id_driver and season=$year group by drivers.id_driver order by points desc";
$resultM = mysqli_query($dbhandle,$query);
$driversPointsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPointsTabM[$tmp_id_driver] = $r["race_pts"];
}
// zwyciestwa
$query="select id_driver,count(distinct race_date) as wins from drivers_gp_results where race_pos=1 and season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversWinsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversWinsTabM[$tmp_id_driver] = $r["wins"];
}
// podium
$query="select id_driver,count(distinct race_date) as podiums from drivers_gp_results where race_pos<4 and race_completed=1 and season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversPodiumsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPodiumsTabM[$tmp_id_driver] = $r["podiums"];
}
// pole position
$query="select id_driver,count(distinct id_starting_grid) as pp from drivers_gp_starting_grid where is_pp=1 and season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversPPTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPPTabM[$tmp_id_driver] = $r["pp"];
}
// best laps
$query="select id_driver,count(distinct race_date) as bestlaps from drivers_gp_results where race_best_lap=1 and season=$year group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversBLTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversBLTabM[$tmp_id_driver] = $r["bestlaps"];
}
// champ
$query="select drivers.id_driver,drivers.name,drivers.surname,drivers.country_short,count(id_drivers_class) as champs from drivers_class,drivers where drivers.id_driver=drivers_class.id_driver and place=1 and season=$year group by id_driver order by champs desc,surname";
$resultM = mysqli_query($dbhandle,$query);
$driversWCTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversWCTabM[$tmp_id_driver] = $r["champs"];
}

// zespoły
$query="SELECT id_driver, 
GROUP_CONCAT(DISTINCT teams.name ORDER BY name ASC SEPARATOR ', ') name, 
GROUP_CONCAT(DISTINCT LOWER(teams.team) ORDER BY teams.team ASC SEPARATOR ', ') team 
FROM drivers_gp_involvements, teams WHERE drivers_gp_involvements.id_team = teams.id_team AND drivers_gp_involvements.season = '$year' 
GROUP BY id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversTeamsNamesTabM = array();
$driversTeamsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversTeamsNamesTabM[$tmp_id_driver] = $r["name"];
  $driversTeamsTabM[$tmp_id_driver] = $r["team"];
}

$driversItems = array();
$query="SELECT
drivers.alias,
drivers.id_driver id,
'drivers' type,
drivers.name,
drivers.surname,
drivers.country_short,
drivers.country_code country,
drivers.photo,
drivers.picture,
drivers.team_color,
GROUP_CONCAT(DISTINCT drivers_gp_involvements.number SEPARATOR ', ') number,
coalesce(drivers_class.place,1000) seasonPlace,
drivers_class.is_classified isClassified,
'' seasonPoints,
'' wins,
'' points,
'' podium,
'' starts,
'' qual,
'' polePosition,
'' bestLaps,
'' bestResult,
'' team
FROM drivers_gp_involvements
LEFT JOIN drivers_class ON drivers_gp_involvements.season=drivers_class.season
AND drivers_gp_involvements.id_driver=drivers_class.id_driver AND drivers_class.season = '$year'
LEFT JOIN drivers ON drivers_gp_involvements.id_driver = drivers.id_driver 
WHERE drivers_gp_involvements.season = '$year'
group by drivers_gp_involvements.id_driver    
ORDER BY
seasonPlace,
surname";
// SELECT drivers.alias,drivers.id_driver id,drivers_class.number,'drivers' type,
// drivers.name,drivers.surname,drivers.country_short,drivers.country_code country,drivers.photo,drivers.picture,drivers.team_color,
// (SELECT GROUP_CONCAT(DISTINCT number SEPARATOR ', ') FROM drivers_gp_involvements where drivers_gp_involvements.season=drivers_class.season and drivers_gp_involvements.id_driver=drivers.id_driver ORDER BY number) number,
// drivers_class.place seasonPlace,'' seasonPoints,'' wins,'' points,'' podium,'' starts, '' qual, '' polePosition,'' bestLaps,'' bestResult,'' team 
// from drivers_class,drivers 
// where drivers_class.id_driver=drivers.id_driver 
// and season='$year' 
// order by seasonPlace, surname";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   if (empty($driversStartsTabM[$r["id"]])){
     $r["starts"]=0;
   }else{
     $r["starts"]=(int)$driversStartsTabM[$r["id"]];
   }
   if (empty($driversQualTabM[$r["id"]])){
    $r["qual"]=0;
  }else{
    $r["qual"]=(int)$driversQualTabM[$r["id"]];
  }
   if (empty($driversPointsTabM[$r["id"]])){
     $r["points"]=0;
   }else{
     $r["points"]=(double)$driversPointsTabM[$r["id"]];
   }
   if (empty($driversWinsTabM[$r["id"]])){
     $r["wins"]=0;
   }else{
     $r["wins"]=(int)$driversWinsTabM[$r["id"]];
   }
   if (empty($driversPodiumsTabM[$r["id"]])){
     $r["podium"]=0;
   }else{
     $r["podium"]=(int)$driversPodiumsTabM[$r["id"]];
   }
   if (empty($driversPPTabM[$r["id"]])){
     $r["polePosition"]=0;
   }else{
     $r["polePosition"]=(int)$driversPPTabM[$r["id"]];
   }
   if (empty($driversBLTabM[$r["id"]])){
     $r["bestLaps"]=0;
   }else{
     $r["bestLaps"]=(int)$driversBLTabM[$r["id"]];
   }

   if (!empty($driversWCTabM[$r["id"]])&&$year!='2025'){
     $r["champ"]="MŚ";
   }

   $r["teamName"]=$driversTeamsNamesTabM[$r["id"]];
   $r["team"]=$driversTeamsTabM[$r["id"]];
   $driversItems[] = $r;
}

$seasonDrivers["drivers"]=$driversItems;

// Response
$response = $seasonDrivers;

print json_encode($response);
mysqli_free_result($result);
?>
