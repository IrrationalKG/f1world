/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Table,
  Message,
  Image,
  Item,
  Statistic,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchGPSeasonSummary } from "../../actions/GPSeasonSummaryActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPSeasonSummary.less";

class GPSeasonSummary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPSeasonSummary(props.match.params.year, Cookies.get("lang"));
    props.fetchComboSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboSeasons.data?.find((e) => e.value == nextYear)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchGPSeasonSummary(nextYear);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/gp-season/summary/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderClassDriversSummary() {
    const elements = [];
    const { props } = this;
    const { classDriversSummary } = props.gpseasonSummary.data;
    const { year } = props.match.params;
    classDriversSummary.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.teamsNames?.split(", ").map((teamName, index) => {
                      return (
                        <span key={teamName}>
                          {index != 0 && ", "}
                          {teamName}
                        </span>
                      );
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Starty">
            <NavLink
              to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.starts}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            <NavLink
              to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.wins}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca">
            <NavLink
              to={`/driver-events/second/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.second}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca">
            <NavLink
              to={`/driver-events/third/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.third}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            <NavLink
              to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.podium}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            <NavLink
              to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.polepos}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            <NavLink
              to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.bestlaps}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Miejsca punktowane">
            <NavLink
              to={`/driver-events/pointsplaces/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : item.pointsPlaces}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Ukończone wyścigi">
            <NavLink
              to={`/driver-events/completed/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.completed}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Nieukończone wyścigi">
            <NavLink
              to={`/driver-events/incomplete/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : item.incomplete}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje">
            <NavLink
              to={`/driver-events/disq/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.disq}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="cell-body-points">
            <NavLink
              to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.pointsClass === null
                ? String.fromCharCode(160)
                : item.pointsClass}
              <small>
                {item.pointsClass !== item.points && ` (${item.points})`}
              </small>
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassDriversSummaryMobile() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classDriversSummary, gp } = props.gpseasonSummary.data;
    classDriversSummary.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/driver/${item.alias}`}>
            <Image
              size="small"
              src={driverPic}
              alt={item.name}
              className="cell-photo"
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.teamsNames?.split(", ").map((teamName, index) => {
                return (
                  <div key={teamName}>
                    <NavLink
                      to={`/team/${item.teamsAliases?.split(", ")[index]}`}
                    >
                      {teamName}
                    </NavLink>
                  </div>
                );
              })}
            </Item.Description>
            <Item.Description>
              <span className="key-value-box">
                <div className="key-value-box-header">ST</div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.starts}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p1"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.wins}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p2"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/second/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.second}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p3"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/third/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.third}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.podiums"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.podium}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.polepos"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.polepos}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.bestlaps}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.points.races"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/pointsplaces/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.pointsPlaces}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.compl"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/completed/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.completed}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.inc"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/incomplete/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.incomplete}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.disq"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/driver-events/disq/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.disq}
                  </NavLink>
                </div>
              </span>
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                <NavLink
                  to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : item.pointsClass}
                  <div>
                    <small>
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </div>
                </NavLink>
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsSummary() {
    const elements = [];
    const { props } = this;
    const { classTeamsSummary } = props.gpseasonSummary.data;
    const { year } = props.match.params;
    classTeamsSummary.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName?.split(", ").map((modelName) => {
                      return <div key={modelName}>{modelName}</div>;
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Starty">
            <NavLink
              to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.starts}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            <NavLink
              to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.wins}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca">
            <NavLink
              to={`/team-events/second/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.second}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca">
            <NavLink
              to={`/team-events/third/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.third}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            <NavLink
              to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.podium}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            <NavLink
              to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.polepos}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            <NavLink
              to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.bestlaps}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Miejsca punktowane">
            <NavLink
              to={`/team-events/pointsplaces/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : item.pointsPlaces}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Ukończone wyścigi">
            <NavLink
              to={`/team-events/completed/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.completed}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Nieukończone wyścigi">
            <NavLink
              to={`/team-events/incomplete/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null
                ? String.fromCharCode(160)
                : item.incomplete}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje">
            <NavLink
              to={`/team-events/disq/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.points === null ? String.fromCharCode(160) : item.disq}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="cell-body-points">
            <NavLink
              to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.pointsClass === null
                ? String.fromCharCode(160)
                : item.pointsClass}
              <small>
                {item.pointsClass !== item.points && ` (${item.points})`}
              </small>
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsSummaryMobile() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classTeamsSummary, gp } = props.gpseasonSummary.data;
    classTeamsSummary.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/team/${item.alias}`}>
            <Image
              size="small"
              src={teamPic}
              alt={item.name}
              onError={(e) => {
                e.target.src = "/build/images/teams/team_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.modelName?.split(", ").map((modelName) => {
                return <div key={modelName}>{modelName}</div>;
              })}
            </Item.Description>
            <Item.Description>
              <span className="key-value-box">
                <div className="key-value-box-header">ST</div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.starts}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p1"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.wins}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p2"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/second/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.second}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.p3"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/third/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.third}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.podiums"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.podium}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.polepos"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.polepos}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.bestlaps}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.points.races"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/pointsplaces/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.pointsPlaces}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.compl"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/completed/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.completed}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.inc"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/incomplete/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.incomplete}
                  </NavLink>
                </div>
              </span>
              <span className="key-value-box">
                <div className="key-value-box-header">
                  <FormattedMessage id={"app.table.header.disq"} />
                </div>
                <div className="key-value-box-value">
                  <NavLink
                    to={`/team-events/disq/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                  >
                    {item.points === null
                      ? String.fromCharCode(160)
                      : item.disq}
                  </NavLink>
                </div>
              </span>
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                <NavLink
                  to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : item.pointsClass}
                  <div>
                    <small>
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </div>
                </NavLink>
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpseasonSummary.data || props.gpseasonSummary.loading) {
      return (
        <section
          id="gpseason-summary-details"
          name="gpseason-summary-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { gp, regulations } = props.gpseasonSummary.data;

    const linkPrev = `/gp-season/summary/${prevYear}`;
    const linkNext = `/gp-season/summary/${nextYear}`;
    if (gp.length === 0) {
      return (
        <section
          id="gpseason-summary-details"
          name="gpseason-summary-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.gpseason.summary.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.gpseason.summary.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage id={"app.page.gpseason.summary.info"} />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="gpseason-summary-details"
        name="gpseason-summary-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.gpseason.summary.title"}
              values={{ season: year }}
            />
          }
          subtitle={
            <FormattedMessage id={"app.page.gpseason.summary.subtitle"} />
          }
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic padded>
            <div className="buttons">
              <NavLink to={`/gp-season/${year}`} className="secondary">
                <FormattedMessage id={"app.button.summary"} />
              </NavLink>
              <NavLink to={`/gp-season/points/${year}`} className="secondary">
                <FormattedMessage id={"app.button.points"} />
              </NavLink>
              <NavLink to={`/gp-season/places/${year}`} className="secondary">
                <FormattedMessage id={"app.button.places"} />
              </NavLink>
              <NavLink to={`/gp-season/qual/${year}`} className="secondary">
                <FormattedMessage id={"app.button.grid"} />
              </NavLink>
              <NavLink to={`/gp-season/summary/${year}`} className="primary">
                <FormattedMessage id={"app.button.statistics"} />
              </NavLink>
              <NavLink to={`/gp-season/drivers/${year}`} className="secondary">
                <FormattedMessage id={"app.button.drivers"} />
              </NavLink>
              <NavLink to={`/gp-season/teams/${year}`} className="secondary">
                <FormattedMessage id={"app.button.teams"} />
              </NavLink>
            </div>
            <div className="gp-season-summary-filters">
              <Select
                ref={(ref) => {
                  this.comboSeason = ref;
                }}
                isLoading={isLoadingExternally}
                name="seasons"
                multi={multi}
                ignoreCase={ignoreCase}
                ignoreAccents={ignoreAccents}
                value={state.selectValue}
                onChange={this.onInputChange}
                options={props.comboSeasons.data}
                clearable={clearable}
                labelKey="label"
                valueKey="value"
                placeholder={formatMessage({
                  id: "app.placeholder.select.season",
                })}
                noResultsText={
                  <FormattedMessage id={"app.placeholder.no.results"} />
                }
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>
            <div className="class-drivers-summary">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.drivers"} />}
                type="secondary"
              />
              <div className="hideForDesktop">
                <Segment basic>
                  <Item.Group divided unstackable>
                    {this.renderClassDriversSummaryMobile()}
                  </Item.Group>
                </Segment>
              </div>
              <div className="hideForMobile">
                <Segment basic className="overflow">
                  <Table
                    basic="very"
                    celled
                    className="responsive-table center-aligned"
                  >
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell className="cell-header-number">
                          #
                        </Table.HeaderCell>
                        <Table.HeaderCell className="left">
                          <FormattedMessage id={"app.table.header.driver"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>ST</Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.p1"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.p2"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.p3"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.podiums"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.polepos"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.bestlaps"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage
                            id={"app.table.header.points.races"}
                          />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.compl"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.inc"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          <FormattedMessage id={"app.table.header.disq"} />
                        </Table.HeaderCell>
                        <Table.HeaderCell className="cell-header-points">
                          <FormattedMessage id={"app.table.header.points"} />
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>{this.renderClassDriversSummary()}</Table.Body>
                  </Table>
                </Segment>
              </div>
            </div>
            <div className="class-teams-summary">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.teams"} />}
                type="secondary"
              />
              {year > 1949 ? (
                <>
                  <div className="hideForDesktop">
                    <Segment basic>
                      <Item.Group divided unstackable>
                        {this.renderClassTeamsSummaryMobile()}
                      </Item.Group>
                    </Segment>
                  </div>
                  <div className="hideForMobile">
                    <Segment basic className="overflow">
                      <Table
                        basic="very"
                        celled
                        className="responsive-table center-aligned"
                      >
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell className="cell-header-number">
                              #
                            </Table.HeaderCell>
                            <Table.HeaderCell className="left">
                              <FormattedMessage id={"app.table.header.team"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>ST</Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.p1"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.p2"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.p3"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.podiums"}
                              />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.polepos"}
                              />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.points.races"}
                              />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.compl"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.inc"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell>
                              <FormattedMessage id={"app.table.header.disq"} />
                            </Table.HeaderCell>
                            <Table.HeaderCell className="cell-header-points">
                              <FormattedMessage
                                id={"app.table.header.points"}
                              />
                            </Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>
                          {this.renderClassTeamsSummary()}
                        </Table.Body>
                      </Table>
                    </Segment>
                  </div>
                </>
              ) : (
                <Message icon>
                  <Icon name="warning circle" />
                  <Message.Content>
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <p>
                      {" "}
                      <FormattedMessage
                        id={"app.page.gpseason.summary.info2"}
                      />
                    </p>
                  </Message.Content>
                </Message>
              )}
            </div>
            <Segment padded basic>
              <Regulations regulations={regulations}></Regulations>
            </Segment>
          </Segment>
        </div>
      </section>
    );
  }
}

GPSeasonSummary.propTypes = {
  fetchGPSeasonSummary: PropTypes.func.isRequired,
  gpseasonSummary: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboSeasons: PropTypes.func.isRequired,
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpseasonSummary, comboSeasons }) {
  return { gpseasonSummary, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchGPSeasonSummary, fetchComboSeasons },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPSeasonSummary)
);
