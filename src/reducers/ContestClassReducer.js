import { FETCH_CONTEST_CLASS_REQUEST, FETCH_CONTEST_CLASS_SUCCESS, FETCH_CONTEST_CLASS_ERROR } from '../actions/ContestClassActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CONTEST_CLASS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CONTEST_CLASS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CONTEST_CLASS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
