/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchTrivia } from "../../actions/TriviaActions";
import Cookies from "js-cookie";
import styles from "./Trivia.less";

class Trivia extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderTrivia = this.renderTrivia.bind(this);
  }

  componentWillMount() {
    const { props } = this;
    props.fetchTrivia(Cookies.get("lang"));
  }

  componentDidMount() {
    this.renderTrivia();
  }

  renderTrivia() {
    const triviaItems = [];
    const { props } = this;
    const trivia = props.trivia.data;
    if (trivia !== null) {
      trivia.data.forEach((item) => {
        const value = item.text.substring(0, item.text.indexOf(" "));
        const text = item.text.substring(item.text.indexOf(" ") + 1);
        const triviaItem = (
          <div key={item.id}>
            <div className="value">{value}</div>
            <NavLink to={item.link}>
              <div className="text">{text}</div>
            </NavLink>
          </div>
        );
        triviaItems.push(triviaItem);
      });
    }
    return triviaItems;
  }

  render() {
    return (
      <section id="trivia">
        <span>{this.renderTrivia()}</span>
      </section>
    );
  }
}

Trivia.propTypes = {
  fetchTrivia: PropTypes.func.isRequired,
};

function mapStateToProps({ trivia }) {
  return { trivia };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTrivia }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Trivia);
