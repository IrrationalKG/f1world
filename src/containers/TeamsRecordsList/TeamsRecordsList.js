/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubgp","renderTeamsubgp"] }] */
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import {
  CardGroup,
  CardHeader,
  CardDescription,
  CardContent,
  Card,
} from "semantic-ui-react";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
const comboGroupedStats = require("../../json/combo_grouped_stats_teams.json");
const comboGroupedStatsEn = require("../../json/combo_grouped_stats_teams_en.json");
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./TeamsRecordsList.less";

class TeamsRecordsList extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderTeamsRecordsList = this.renderTeamsRecordsList.bind(this);
  }

  renderTeamsRecordsList() {
    const items = [];
    const comboStats =
      Cookies.get("lang") == "pl" ? comboGroupedStats : comboGroupedStatsEn;
    if (comboStats) {
      comboStats.forEach((item) => {
        const { label, options } = item;
        const record = (
          <Card key={label}>
            <CardContent>
              <CardHeader>{label}</CardHeader>
              <CardDescription className="teams-records-box">
                <ul>
                  {options.map((e) => {
                    return (
                      <NavLink
                        key={e.value}
                        to={`teams-records/${e.value}/-/-/-/-/0/1/`}
                      >
                        <li>{e.label}</li>
                      </NavLink>
                    );
                  })}
                </ul>
              </CardDescription>
            </CardContent>
          </Card>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    return (
      <section
        id="teams-records-list"
        name="teams-records-list"
        className="section-page"
      >
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.teams.records.list.title"} />}
          subtitle={
            <FormattedMessage id={"app.page.teams.records.list.subtitle"} />
          }
        />
        <div className="section-page-content">
          <CardGroup itemsPerRow={5} className="hideMobile hideTablet">
            {this.renderTeamsRecordsList()}
          </CardGroup>
          <CardGroup itemsPerRow={3} className="hideMobile hideDesktop">
            {this.renderTeamsRecordsList()}
          </CardGroup>
          <CardGroup itemsPerRow={1} className="hideDesktop hideTablet">
            {this.renderTeamsRecordsList()}
          </CardGroup>
        </div>
      </section>
    );
  }
}

export default TeamsRecordsList;
