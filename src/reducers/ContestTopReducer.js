import { FETCH_CONTEST_TOP_REQUEST, FETCH_CONTEST_TOP_SUCCESS, FETCH_CONTEST_TOP_ERROR } from '../actions/ContestTopActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CONTEST_TOP_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CONTEST_TOP_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CONTEST_TOP_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
