/*global __dirname*/
const path = require("path");
const webpack = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");

module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  entry: ["./src/index.js"],
  devServer: {
    historyApiFallback: true,
    inline: true,
    port: 3000,
    hot: true,
  },
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/build/",
    filename: "[name]-chunk.js",
    chunkFilename: "[name]-chunk.js",
  },
  resolve: {
    alias: {
      "../../theme.config$": path.join(
        __dirname,
        "src/styles/semantic-ui/theme.config"
      ),
    },
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-react", "@babel/preset-env"],
          },
        },
      },
      {
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "less-loader",
          },
        ],
        test: /\.less$/,
      },
      {
        use: [
          {
            loader: "file-loader",
            options: {
              name: "fonts/[name].[ext]",
            },
          },
        ],
        test: /\.(woff|woff2|eot|ttf|otf)$/,
      },
      // kopiuje wszystkie includowane pliki graficzne z katalogu src do build
      {
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]",
              context: "./src",
            },
          },
        ],
        test: /\.(png|svg|jpe?g|gif|webp)$/i,
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "index.ejs",
    }),
    new FaviconsWebpackPlugin({
      inject: true,
      logo: "./src/icons/favicon.jpg",
      prefix: "icons/",
    }),
    new CopyWebpackPlugin(
      [
        { context: "src/images", from: "**/*", to: "images" },
        { from: "src/json/*.json", to: "json" },
      ],
      {
        copyUnmodified: true,
      }
    ),
  //   new WriteFilePlugin(
  //     {
  //     // Write only files that have ".jpg" extension.
  //     test: /\.(jpg|json|webp)$/,
  //     useHashIndex: true,
  //   }
  // ),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "window.$": "jquery",
    }),
  ],
  externals: {
    "react/addons": true, // important!!
    "react/lib/ExecutionEnvironment": true,
    "react/lib/ReactContext": true,
  },
};
