<?
header('Access-Control-Allow-Origin: *');

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie id gp
$query="SELECT DISTINCT gp.id_gp,gp_season.sprint FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp and gp.name_alias='$id'
AND gp_season.season='$year'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id_gp"];
}

// min pozycja
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp AND season=$year order by sort asc limit 1";
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp AND season=$year order by sort desc limit 1";
$result = mysqli_query($dbhandle,$query);
$max;
$maxPos;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
  $maxPos = $r["sort"];
}

// poprzednia gp
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
AND sort=(SELECT sort FROM gp_season WHERE id_gp='$id' AND season=$year)-1 AND season=$year";
$result = mysqli_query($dbhandle,$query);
$prevId=$max;
while($r = mysqli_fetch_assoc($result)) {
    $prevId = $r["alias"];
}

// nastepna gp
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
AND sort=(SELECT sort FROM gp_season WHERE id_gp='$id' AND season=$year)+1 AND season=$year";
$result = mysqli_query($dbhandle,$query);
$nextId = $min;
while($r = mysqli_fetch_assoc($result)) {
  $nextId = $r["alias"];
}

// informacje o gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,gp.id_gp id, gp.circuit, gp.circuit_alias circuitAlias, lower(gp.name_short) countryShort,
gp.country_code countryCode, COALESCE(drivers_gp_results.race_date,SUBSTR(gp_season.date,1,10)) raceDate, '' prevId, '' nextId, gp_season.sprint
FROM gp
LEFT JOIN gp_season ON gp.id_gp=gp_season.id_gp AND gp_season.season='$year'
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_gp=gp_season.id_gp and race_date>='$year-01-01' and race_date<'$year-12-31'
WHERE gp.id_gp=$id";
$result = mysqli_query($dbhandle,$query);
$gpResultItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["prevId"]=$prevId;
   $r["nextId"]=$nextId;
   $gpResultItems = $r;
}

// sezony
$query="SELECT DISTINCT SUBSTR(race_date, 1, 4) season 
from drivers_gp_results,gp where gp.id_gp=drivers_gp_results.id_gp 
and gp.name_alias IN (SELECT t.name_alias FROM gp t WHERE t.id_gp=$id)
order by race_date asc";
$result = mysqli_query($dbhandle,$query);
$seasons=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasons[] = $r["season"];
}
$gpResultItems["seasons"]=$seasons;

/**
* Wyniki
**/
$resultsItems;
$pointsItems;

// punkty - kierowcy
$query="SELECT drivers.alias driverAlias,drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode, drivers.picture picture,
(SELECT GROUP_CONCAT(DISTINCT CONCAT(name,' ',COALESCE(engine,'')) SEPARATOR ', ') FROM teams, drivers_gp_results
WHERE teams.id_team=drivers_gp_results.id_team AND drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.season='$year' and drivers_gp_results.id_gp=$id) as team,
drivers_gp_results.race_points as race_points, 
drivers_sprint_results.sprint_points as sprint_points,
COALESCE(drivers_gp_results.race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0) as points,
drivers_gp_results.race_best_lap_points,
COALESCE(drivers_gp_results.excluded_from_class,0) as excluded,
drivers_gp_results.race_add_info as info, 
drivers_gp_results.race_pos as place
from drivers_class
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers_class.id_driver and drivers_gp_results.season='$year' and drivers_gp_results.id_gp=$id
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_driver=drivers_class.id_driver and drivers_sprint_results.season='$year' and drivers_sprint_results.id_gp=$id
LEFT JOIN drivers ON drivers.id_driver=drivers_class.id_driver
LEFT JOIN teams ON teams.id_team=drivers_class.id_team
where drivers_class.season=$year
and COALESCE(drivers_gp_results.race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0) > 0  
ORDER BY points DESC";
$result = mysqli_query($dbhandle,$query);
$driverPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverPointsItems[] = $r;
}
$pointsItems["drivers"]=$driverPointsItems;

// punkty - zespoly
$query="SELECT teams.alias_name teamAlias,lower(teams.team) idTeam, teams.name, teams.engine, teams.country_code teamCountryCode, teams.picture picture, 
SUM(drivers_gp_results.race_team_points) + COALESCE((SELECT SUM(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.season=$year AND drivers_sprint_results.id_gp=$id),0) as points,
COALESCE(drivers_gp_results.excluded_from_team_class,0) as excluded,
teams_models.team_name modelName
from teams_class
LEFT JOIN teams ON teams.id_team=teams_class.id_team
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_team=teams_class.id_team and drivers_gp_results.season='$year' and drivers_gp_results.id_gp=$id
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_drivers_gp = drivers_gp_results.id_drivers_gp AND drivers_gp_involvements.season = drivers_gp_results.season AND drivers_gp_involvements.id_gp = $id
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
where teams_class.season=$year
group by teams_class.id_team  
having points > 0
ORDER BY points DESC";
$result = mysqli_query($dbhandle,$query);
$teamPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamPointsItems[] = $r;
}
$pointsItems["teams"]=$teamPointsItems;

$resultsItems["points"]=$pointsItems;

// najlepsze okrazenie
$query="SELECT drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver,
drivers.country_code driverCountryCode,lower(teams.team) idTeam,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) team,
teams.country_code teamCountryCode,race_best_lap_time time
from drivers_gp_results,drivers,teams where drivers.id_driver=drivers_gp_results.id_driver
and teams.id_team=drivers_gp_results.id_team and race_date>='$year-01-01' and race_date<'$year-12-31'
and id_gp=$id and race_best_lap=1";
$result = mysqli_query($dbhandle,$query);
$bestLapItem=array();
while($r = mysqli_fetch_assoc($result)) {
  $bestLapItem[] = $r;
}
$resultsItems["bestLap"]=$bestLapItem;

// wyniki gp
$query="SELECT 
drivers.id_driver idDriver,
drivers.alias driverAlias,
CONCAT(drivers.name, ' ', drivers.surname) driver,
drivers.country_code driverCountryCode,
teams.alias_name teamAlias,
LOWER(teams.team) idTeam,
teams.name team,
teams.country_code teamCountryCode,
drivers_sprint_results.sprint_pos sprintPos,
drivers_sprint_results.sprint_points sprintPoints,
drivers_pp_results.qual_pos qualPlace,
drivers_gp_starting_grid.grid_pos gridPlace,
drivers_gp_results.race_pos place,
drivers_gp_results.race_points points,
drivers_gp_results.excluded_from_class excluded,
drivers_gp_results.disq,
drivers_gp_results.race_laps laps,";
if ($lang=='pl') {
  $query.="drivers_gp_results.race_time time, drivers_gp_results.race_add_info info,";
}else{
  $query.="(CASE 
  WHEN drivers_gp_results.race_time LIKE '+1 %' THEN REPLACE(drivers_gp_results.race_time,'okrąż.', 'lap')
 	WHEN drivers_gp_results.race_time LIKE '*%' THEN REPLACE(drivers_gp_results.race_time,'okrąż.', 'lap')
  ELSE REPLACE(drivers_gp_results.race_time,'okrąż.', 'laps')
  END) time, drivers_gp_results.race_add_info_en info,";
}
$query.="drivers_gp_results.race_completed completed,
drivers_gp_results.race_finished finished,
drivers_gp_results.race_best_lap bestLap,
drivers_gp_results.race_best_lap_time bestLapTime,
drivers_gp_results.co_driver coDriver,
COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,
drivers_gp_involvements.number,
teams_models.model,
teams_models.team_name modelName,
teams.engine,
tyres.name tyre
FROM drivers_gp_involvements
LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp = drivers_gp_involvements.id_gp AND gp_season.season = drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp 
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team=drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp=drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season=drivers_gp_involvements.season 
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre
WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL 
AND drivers_gp_results.season='$year' 
AND drivers_gp_results.id_gp=$id 
order by race_pos, co_driver";
$result = mysqli_query($dbhandle,$query);
$raceItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $raceItems[] = $r;
}

// pola startowe
$query="SELECT drivers.alias driverAlias, drivers.id_driver idDriver, CONCAT(drivers.name, ' ', drivers.surname) driver, drivers.country_code driverCountryCode, 
teams.alias_name teamAlias, LOWER(teams.team) idTeam, teams.name team, teams.country_code teamCountryCode, 
grid_pos place, not_started notStarted, from_pits fromPits, from_back_grid fromBackGrid,"; 
if ($lang=='pl') {
  $query.="grid_add_info info, ";
}else{
  $query.="grid_add_info_en info,";
}
$query.="GROUP_CONCAT(drivers_gp_involvements.number SEPARATOR ', ') number,
teams_models.model,
teams_models.team_name modelName,
teams.engine,
tyres.name tyre
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver 
and drivers_gp_starting_grid.id_team=drivers_gp_involvements.id_team
and drivers_gp_starting_grid.id_gp=drivers_gp_involvements.id_gp
and drivers_gp_starting_grid.season=drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre
WHERE drivers_gp_starting_grid.season='$year' 
and drivers_gp_starting_grid.id_gp=$id 
GROUP BY drivers_gp_involvements.id_driver
order by grid_pos";
$result = mysqli_query($dbhandle,$query);
$gridItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gridItems[] = $r;
}

// wyniki kwalifikacji
$query="SELECT qual_pos place,drivers.alias driverAlias,drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver,
drivers.country_code driverCountryCode,teams.alias_name teamAlias,
lower(teams.team) idTeam,teams.name team,teams.country_code teamCountryCode,
qual_time time,"; 
if ($lang=='pl') {
  $query.="qual_add_info info, ";
}else{
  $query.="qual_add_info_en info, ";
}
$query.="not_qualified notQualified, not_started notStarted, qual_completed completed, qual_segment segment,
GROUP_CONCAT(drivers_gp_involvements.number SEPARATOR ', ') number,
teams_models.model,
teams_models.team_name modelName,
teams.engine,
tyres.name tyre
FROM drivers_gp_involvements
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp and drivers_gp_involvements.id_team=drivers_pp_results.id_team
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre
WHERE drivers_pp_results.season='$year' 
and drivers_pp_results.id_gp=$id 
and drivers_pp_results.qual_completed=1 
GROUP BY drivers_gp_involvements.id_driver,drivers_gp_involvements.id_team
order by qual_pos";
$result = mysqli_query($dbhandle,$query);
$qualItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $qualItems[] = $r;
}

// wyniki sprintu
$query="SELECT sprint_pos place,drivers.alias driverAlias,drivers.id_driver idDriver,
CONCAT(drivers.name,' ',drivers.surname) driver,drivers.country_code driverCountryCode,teams.alias_name teamAlias,
lower(teams.team) idTeam, teams.name team,teams.country_code teamCountryCode,
sprint_points points, sprint_time time,"; 
if ($lang=='pl') {
  $query.="sprint_add_info info, ";
}else{
  $query.="sprint_add_info_en info, ";
}
$query.="sprint_completed completed,
drivers_gp_involvements.number,
teams_models.model,
teams_models.team_name modelName,
teams.engine,
tyres.name tyre
FROM drivers_gp_involvements
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint and drivers_gp_involvements.id_team=drivers_sprint_results.id_team
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre
WHERE drivers_sprint_results.season='$year'  
and drivers_sprint_results.id_gp=$id 
order by sprint_pos";
$result = mysqli_query($dbhandle,$query);
$sprintItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $sprintItems[] = $r;
}

// lista startowa
$query="SELECT drivers.alias driverAlias, drivers.id_driver idDriver, CONCAT(drivers.name,' ',drivers.surname) driver, drivers.country_code driverCountryCode,
GROUP_CONCAT(DISTINCT teams.alias_name SEPARATOR ', ') teamAlias, 
GROUP_CONCAT(DISTINCT LOWER(teams.team) SEPARATOR ', ') idTeam, 
GROUP_CONCAT(DISTINCT teams.name SEPARATOR ', ') team,
GROUP_CONCAT(drivers_gp_involvements.number SEPARATOR ', ') number,
GROUP_CONCAT(drivers_gp_involvements.id_drivers_gp SEPARATOR ', ') id_drivers_gp, 
GROUP_CONCAT(drivers_gp_involvements.id_drivers_pp SEPARATOR ', ') id_drivers_pp, 
GROUP_CONCAT(drivers_gp_involvements.id_drivers_sprint SEPARATOR ', ') id_drivers_sprint,
teams_models.model,
teams_models.team_name modelName,
teams.engine
FROM drivers_gp_involvements
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
WHERE drivers_gp_involvements.season = '$year' AND drivers_gp_involvements.id_gp = $id
GROUP BY drivers_gp_involvements.id_driver
ORDER BY drivers.surname, drivers_gp_involvements.number";
$result = mysqli_query($dbhandle,$query);
$entrantsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $entrantsItems[] = $r;
}

$resultsItems["race"]=$raceItems;
$resultsItems["grid"]=$gridItems;
$resultsItems["qual"]=$qualItems;
$resultsItems["sprint"]=$sprintItems;
$resultsItems["entrants"]=$entrantsItems;
$gpResultItems["results"]=$resultsItems;

// regulamin
if ($lang=='pl') {
  $query="SELECT regulations from season_regulations where year='$year'";
}else{
  $query="SELECT regulations_en regulations from season_regulations where year='$year'";
}
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $gpResultItems["regulations"] = $r["regulations"];
}

$gpResultItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $gpResultItems;

print json_encode($response);
mysqli_free_result($result);
?>
