<?
header('Access-Control-Allow-Origin: *');

$year=$_GET['year'];
if ($year==null) $year=$_POST['year'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// max season
$query="SELECT max(sezon) maxYear FROM typy";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
$maxYear="";
while($r = mysqli_fetch_assoc($result)) {
  $contest["maxYear"]=$r["maxYear"];
  $maxYear=$r["maxYear"];
}

// klasyfikacja zwykla
$query="SELECT id_user id,CONCAT(name,' ',surname) name, alias, place, diffrence diff, points
FROM competition_places,users
WHERE users.id_user=competition_places.user AND competition_places.season='$year'
AND round = (SELECT MAX(round) FROM competition_places WHERE season='$year')
ORDER BY place, competition_places.points DESC,users.surname";
$result = mysqli_query($dbhandle,$query);
$classificationItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
  $query2="select distinct gp.name gp, gp.country_code name, gp.name_alias alias, coalesce(typy.place,'-') place, coalesce(typy.suma,'-') points from gp_season left join gp On gp_season.id_gp=gp.id_gp left join typy on typy.gp=gp.name_short and typy.gp=gp.name_short AND typy.sezon='$year' and typy.is_deleted=0 and typy.uczestnik='$id' where gp_season.season='$year' order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gp=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $gp[] = $r2;
  }
  $r["gp"]=$gp;
  $classificationItems[] = $r;
}

// Statystyki
$statsItems=array();
// sezon
$statsContestItems=array();
// liczba sezonow
$query="SELECT count(distinct sezon) amount FROM typy";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["seasons"] = $r["amount"];
}
// liczba graczy
$query="SELECT count(distinct uczestnik) amount FROM typy";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["players"] = $r["amount"];
}
// liczba zwycięzców
$query="SELECT count(distinct uczestnik) amount FROM typy,users WHERE typy.uczestnik=users.id_user AND place=1 and is_deleted=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["winners"] = $r["amount"];
}
// liczba 2
$query="SELECT count(distinct uczestnik) amount FROM typy,users WHERE typy.uczestnik=users.id_user AND place=2 and is_deleted=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["second"] = $r["amount"];
}
// liczba 3
$query="SELECT count(distinct uczestnik) amount FROM typy,users WHERE typy.uczestnik=users.id_user AND place=3 and is_deleted=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["third"] = $r["amount"];
}
// liczba graczy na podium
$query="SELECT count(distinct uczestnik) amount FROM typy,users WHERE typy.uczestnik=users.id_user AND place<4 and is_deleted=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["podium"] = $r["amount"];
}
$statsItems["contest"]=$statsContestItems;

// gracze
$statsPlayersItems=array();
// najwiecej sezonow
$query="SELECT count(distinct sezon) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 20";
$result = mysqli_query($dbhandle,$query);
$seasonsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasons"][] = $r;
}
// najwiecej typowan
$query="SELECT count(*) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 20";
$result = mysqli_query($dbhandle,$query);
$seasonsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["rounds"][] = $r;
}
// najwiecej punktow
$query="SELECT sum(suma) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 20";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["points"][] = $r;
}
// najwiecej punktow gp
$query="SELECT sum(pkt_gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 20";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["pointsGP"][] = $r;
}
// najwiecej zwyciestw w sezonach
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place=1 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonWins"][] = $r;
}
// najwiecej 2 miejsc w sezonach
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place=2 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonSecond"][] = $r;
}
// najwiecej 3 miejsc w sezonach
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place=3 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonThird"][] = $r;
}
// najwiecej podium w sezonach
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place<=3 GROUP BY users.id_user ORDER BY 1 DESC";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonPodiums"][] = $r;
}
// najwiecej zwyciestw w sezonach GP
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place_gp=1 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonWinsGP"][] = $r;
}
// najwiecej 2 miejsc w sezonach GP
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place_gp=2 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonSecondGP"][] = $r;
}
// najwiecej 3 miejsc w sezonach GP
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place_gp=3 GROUP BY users.id_user ORDER BY 1 DESC LIMIT 50";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonThirdGP"][] = $r;
}
// najwiecej podium w sezonach GP
$query="SELECT count(id_stats) amount, CONCAT(name,' ',surname) name, alias FROM competition_stats,users WHERE competition_stats.user=users.id_user and place_gp>0 and place_gp<=3 GROUP BY users.id_user ORDER BY 1 DESC";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["seasonPodiumsGP"][] = $r;
}
// max punktow w kolejce
$query="SELECT sum(suma) amount,gp, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik,gp ORDER BY 1 DESC, send_date ASC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["maxRoundPoints"] = $r;
}
// zwyciestwa
if ($year) {
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=1 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}else{
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND place=1 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["roundWins"][] = $r;
}
// drugie miejsca
if ($year) {
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=2 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}else{
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND place=2 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["roundSecond"][] = $r;
}
// trzecie miejsca
if ($year) {
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=3 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}else{
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND place=3 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["roundThird"][] = $r;
}
// podium
if ($year) {
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place<4 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}else{
  $query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND place<4 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
}
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["roundPodiums"][] = $r;
}
// top 10
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and place<11 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["roundTop10"][] = $r;
}
$statsItems["players"]=$statsPlayersItems;

$contest["season"]=$year;
$contest["stats"]=$statsItems;
$contest["createTime"]=microtime(true)-$start_time;

// Response
$response = $contest;

print json_encode($response);
mysqli_free_result($result);
?>
