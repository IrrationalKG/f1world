/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { Icon, Divider, Grid } from "semantic-ui-react";
import faviconImg from "../../images/favicon.jpg";
import { FormattedMessage, injectIntl } from "react-intl";
import styles from "./Footer.less";

class Footer extends Component {
  render() {
    const { params } = this.props;
    const { formatMessage } = this.props.intl;

    if (!params.data || params.loading) {
      return (
        <section id="footer">
          <div className="full-height" />
        </section>
      );
    }
    const { nextGPAlias, currentGPYear } = params.data;
    const picName = faviconImg.substring(0, faviconImg.length - 4);
    const picp = `${picName}.webp`;
    return (
      <footer id="footer">
        <div className="ui stackable grid">
          <div className="row">
            <div className="four wide tablet four wide computer column">
              <div className="footer-logo">
                <Grid stackable>
                  <Grid.Row columns={2}>
                    <Grid.Column width={3}>
                      <picture>
                        <source srcSet={picp} type="image/webp" />
                        <img
                          src={faviconImg}
                          alt="F1World logo"
                          width="56px"
                          height="56px"
                        />
                      </picture>
                    </Grid.Column>
                    <Grid.Column width={13}>
                      <div className="title">
                        Formula One World {currentGPYear}
                      </div>
                      <p>
                        <a href="https://www.formula1.com">
                          <FormattedMessage id={"app.page.footer.title1"} />
                        </a>
                        <FormattedMessage id={"app.page.footer.title2"} />
                      </p>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <a
                  rel="noopener noreferrer"
                  href="mailto:kryglow@gmail.com"
                  target="_blank"
                  title="Email"
                >
                  <Icon name="mail" size="small" />
                  kryglow@gmail.com
                </a>
              </div>
              <ul className="social-icons">
                <li>
                  <a
                    rel="noopener noreferrer"
                    href="https://www.facebook.com/f1world2025"
                    target="_blank"
                    title="Share on Facebook"
                  >
                    <Icon name="facebook square" size="large" />
                  </a>
                </li>
                {/* <li>
                  <a
                    rel="noopener noreferrer"
                    href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fwww.f1world.pl&amp;text=Formula%20One%20World%20{lastGPYear}%20%3A%3A%20Formula%201%20-%20informacje%2C%20wyniki%20Grand%20Prix%20oraz%20statystyki%20F1:%20http%3A%2F%2Fwww.f1world.pl"
                    target="_blank"
                    title="Tweet"
                  >
                    <Icon name="twitter" size="large" />
                  </a>
                </li> */}
              </ul>
            </div>
            <div className="three wide tablet three wide computer column">
              <Divider horizontal inverted>
                <FormattedMessage id={"app.stats.season"} /> {currentGPYear}
              </Divider>
              <ul className="footer-links">
                <li>
                  <NavLink
                    exact
                    to={`/drivers-list/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.drivers",
                    })}
                  >
                    <FormattedMessage id={"app.stats.drivers"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/teams-list/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.teams",
                    })}
                  >
                    <FormattedMessage id={"app.stats.teams"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/circuits-list/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.circuits",
                    })}
                  >
                    <FormattedMessage id={"app.stats.circuits"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/results-list/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.results",
                    })}
                  >
                    <FormattedMessage id={"app.stats.results"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/classification/drivers-places/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.class.drivers",
                    })}
                  >
                    <FormattedMessage id={"app.stats.class.drivers"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/classification/teams-places/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.class.teams",
                    })}
                  >
                    <FormattedMessage id={"app.stats.class.teams"} />
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="three wide tablet three wide computer column">
              <Divider horizontal inverted>
                <FormattedMessage id={"app.stats.history"} />{" F1"}
              </Divider>
              <ul className="footer-links">
                <li>
                  <NavLink
                    exact
                    to="/champs"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.world.champs",
                    })}
                  >
                    <FormattedMessage id={"app.stats.world.champs"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/seasons"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.seasons",
                    })}
                  >
                    <FormattedMessage id={"app.stats.seasons"} />{" F1"}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/drivers-stats-list/a"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.drivers",
                    })}
                  >
                    <FormattedMessage id={"app.stats.drivers"} />{" F1"}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/teams-stats-list/a"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.teams",
                    })}
                  >
                    <FormattedMessage id={"app.stats.teams"} />{" F1"}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/circuits-stats-list"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.circuits",
                    })}
                  >
                    <FormattedMessage id={"app.stats.circuits"} />{" F1"}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/drivers-records"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.drivers.records",
                    })}
                  >
                    <FormattedMessage id={"app.stats.drivers.records"} />{" F1"}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/teams-records"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.stats.teams.records",
                    })}
                  >
                    <FormattedMessage id={"app.stats.teams.records"} />{" F1"}
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="three wide tablet three wide computer column">
              <Divider horizontal inverted>
                <FormattedMessage id={"app.page.footer.contest"} />
              </Divider>
              <ul className="footer-links">
                <li>
                  <NavLink
                    exact
                    to="/contest/rate"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.rate",
                    })}
                  >
                    <FormattedMessage id={"app.page.footer.contest.rate"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/contest/gprate/${currentGPYear}/${nextGPAlias}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.gp.rates",
                    })}
                  >
                    <FormattedMessage id={"app.page.footer.contest.gp.rates"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/contest/class/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.class.standard",
                    })}
                  >
                    <FormattedMessage
                      id={"app.page.footer.contest.class.standard"}
                    />{" "}
                    {currentGPYear}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/contest/class-gp/${currentGPYear}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.class.gp",
                    })}
                  >
                    <FormattedMessage id={"app.page.footer.contest.class.gp"} />{" "}
                    {currentGPYear}
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to={`/contest/class/${currentGPYear - 1}`}
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.historical.results",
                    })}
                  >
                    <FormattedMessage
                      id={"app.page.footer.contest.historical.results"}
                    />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/contest/top"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.top100",
                    })}
                  >
                    <FormattedMessage id={"app.page.footer.contest.top100"} />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/contest/regulations"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.regulation",
                    })}
                  >
                    <FormattedMessage
                      id={"app.page.footer.contest.regulation"}
                    />
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/contest/register"
                    activeClassName="footer-active-link"
                    title={formatMessage({
                      id: "app.page.footer.contest.register",
                    })}
                  >
                    <FormattedMessage id={"app.page.footer.contest.register"} />
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="three wide tablet three wide computer column">
              <Divider horizontal inverted>
                <FormattedMessage id={"app.stats.drivers"} />
              </Divider>
              <ul className="footer-links">
                <li>
                  <NavLink
                    exact
                    to="/driver/lewis-hamilton"
                    activeClassName="footer-active-link"
                    title="Lewis Hamilton"
                  >
                    Lewis Hamilton
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/max-verstappen"
                    activeClassName="footer-active-link"
                    title="Max Verstappen"
                  >
                    Max Verstappen
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/oscar-piastri"
                    activeClassName="footer-active-link"
                    title="Oscar Piastri"
                  >
                    Oscar Piastri
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/lando-norris"
                    activeClassName="footer-active-link"
                    title="Lando Norris"
                  >
                    Lando Norris
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/george-russell"
                    activeClassName="footer-active-link"
                    title="George Russell"
                  >
                    George Russell
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/charles-leclerc"
                    activeClassName="footer-active-link"
                    title="Charles Leclerc"
                  >
                    Charles Leclerc
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/fernando-alonso"
                    activeClassName="footer-active-link"
                    title="Fernando Alonso"
                  >
                    Fernando Alonso
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    exact
                    to="/driver/oliver-bearman"
                    activeClassName="footer-active-link"
                    title="Ollie Bearman"
                  >
                    Ollie Bearman
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="row">
            <div className="footer-copyrights">
              <hr />
              &copy; 1997-
              {currentGPYear} © Copyright CGL Software,{" "}
              <FormattedMessage id={"app.page.footer.copyrights"} /> Formula One
              World {currentGPYear}
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

Footer.propTypes = {
  params: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  currentGPYear: PropTypes.string,
  nextGPAlias: PropTypes.string,
};

function mapStateToProps({ params }) {
  return { params };
}

export default injectIntl(connect(mapStateToProps)(Footer));
