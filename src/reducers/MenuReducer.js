import { FETCH_MENU_SUCCESS } from '../actions/MenuActions';

const initial = {
  data: [
    {
      id: 'drivers',
      title: 'Kierowcy',
      items: []
    },
    {
      id: 'teams',
      title: 'Zespoły',
      items: []
    },
    {
      id: 'circuits',
      title: 'Tory',
      items: []
    },
    {
      id: 'results',
      title: 'Wyniki',
      items: []
    },
    {
      id: 'classifications',
      title: 'Klasyfikacje',
      items: []
    },
    {
      id: 'history',
      title: 'Historia',
      items: []
    },
    {
      id: 'contest',
      title: 'Konkurs',
      items: []
    }
  ],
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_MENU_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    default:
      return state;
  }
};
