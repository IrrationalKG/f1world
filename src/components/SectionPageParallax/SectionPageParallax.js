/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from 'react';
import { Parallax, Background } from 'react-parallax';
import PropTypes from 'prop-types';
import styles from './SectionPageParallax.less';

export default class SectionPageParallax extends Component {
  render() {
    const { pic } = this.props;
    const picName = pic.substring(0, pic.length - 4);
    const picp = `${picName}.webp`;
    const pic1024w = `${picName}-1024w.jpg`;
    const pic1024wp = `${picName}-1024w.webp`;
    return (
      <Parallax strength={-200}>
        <div className="parallax-bg" />
        <Background className="custom-bg">
          <picture>
            <source media="(max-width: 1024px)" srcSet={pic1024wp} type="image/webp" />
            <source media="(max-width: 1024px)" srcSet={pic1024w} />
            <source srcSet={picp} type="image/webp" />
            <img
              src={pic}
              alt="parallax"
            />
          </picture>
        </Background>
      </Parallax>
    );
  }
}

SectionPageParallax.propTypes = {
  pic: PropTypes.string
};
