<?
header('Access-Control-Allow-Origin: *');

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie kolejnosci gp
$query="SELECT DISTINCT name_alias alias FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp order by name asc";
$result = mysqli_query($dbhandle,$query);
$gpTab = array();
$cnt=0;
$currentGPCnt=0;
$maxGPCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $cnt+=1;
  $gpTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentGPCnt = $cnt;
  }
}
$maxGPCnt = $cnt;

// min pozycja
$query="SELECT DISTINCT name_alias alias FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp order by name asc limit 1";
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
$query="SELECT DISTINCT name_alias alias FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp order by name desc limit 1";
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzednia gp
if (($currentGPCnt-1)<=0){
  $prevId=$max;
}else{
  $prevId = $gpTab[$currentGPCnt-1];
}
// nastepna gp
if (($currentGPCnt+1)>$maxGPCnt){
  $nextId=$min;
}else{
  $nextId = $gpTab[$currentGPCnt+1];
}

// informacje o gp
$query="SELECT DISTINCT ";
if ($lang=='pl') {
  $query.="gp.name,";
}else{
  $query.="gp.name_en as name,";
}  
$query.="gp.name_alias alias, lower(gp.name_short) countryShort, gp.country_code countryCode, CONCAT(MIN(drivers_gp_results.season),' - ',MAX(drivers_gp_results.season)) seasons, count(distinct drivers_gp_results.season) grandPrix, '' prevId, '' nextId
FROM gp, drivers_gp_results WHERE gp.id_gp=drivers_gp_results.id_gp AND gp.name_alias='$id'";
$result = mysqli_query($dbhandle,$query);
$gpStatsItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["prevId"]=$prevId;
   $r["nextId"]=$nextId;
   $gpStatsItems = $r;
}

// tory
$query="SELECT circuit,circuit_alias alias,country_code countryCode, track, CONCAT(MIN(gp_season.season),' - ',MAX(gp_season.season)) seasons, count(gp_season.season) amount FROM gp, gp_season WHERE gp_season.id_gp=gp.id_gp AND name_alias='$id' GROUP BY gp.id_gp order by MIN(gp_season.season)";
$result = mysqli_query($dbhandle,$query);
$gpCircuitsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpCircuitsItems[] = $r;
}

// starty - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,COUNT(drivers_gp_results.id_drivers_gp) amount, COUNT(drivers_gp_results.id_drivers_gp) y
from drivers_gp_results,drivers,teams,gp
where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp and gp.name_alias='$id'
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverStartsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverStartsItems[] = $r;
}
// starty - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,count(distinct drivers_gp_results.race_date) amount, count(distinct drivers_gp_results.race_date) y
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp
and gp.name_alias='$id'
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamStartsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamStartsItems[] = $r;
}
$startsItems["drivers"]=$driverStartsItems;
$startsItems["teams"]=$teamStartsItems;
$statsItems["starts"]=$startsItems;

// zwyciestwa - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,COUNT(drivers_gp_results.id_drivers_gp) amount, COUNT(drivers_gp_results.id_drivers_gp) y
from drivers_gp_results,drivers,teams,gp
where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp and gp.name_alias='$id'
and drivers_gp_results.race_pos=1
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverWinsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverWinsItems[] = $r;
}
// zwyciestwa - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,count(distinct drivers_gp_results.race_date) amount, count(distinct drivers_gp_results.race_date) y
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp
and gp.name_alias='$id' and drivers_gp_results.race_pos=1
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamWinsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamWinsItems[] = $r;
}
$winsItems["drivers"]=$driverWinsItems;
$winsItems["teams"]=$teamWinsItems;
$statsItems["wins"]=$winsItems;

// podium - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y
from drivers_gp_results,drivers,teams,gp
where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp and gp.name_alias='$id'
and drivers_gp_results.race_pos<4
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverPodiumItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverPodiumItems[] = $r;
}
// podium - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,count(drivers_gp_results.race_date) amount, count(drivers_gp_results.race_date) y
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp
and gp.name_alias='$id' and drivers_gp_results.race_pos<4
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamPodiumItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamPodiumItems[] = $r;
}
$podiumItems["drivers"]=$driverPodiumItems;
$podiumItems["teams"]=$teamPodiumItems;
$statsItems["podium"]=$podiumItems;

// punkty - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,
SUM(drivers_gp_results.race_points) + COALESCE((SELECT SUM(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers.id_driver AND drivers_sprint_results.id_gp=gp.id_gp),0) as amount,
SUM(drivers_gp_results.race_points) + COALESCE((SELECT SUM(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers.id_driver AND drivers_sprint_results.id_gp=gp.id_gp),0) as y
from drivers_gp_results,drivers,teams,gp
where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp and gp.name_alias='$id'
and drivers_gp_results.race_points>0
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverPointsItems[] = $r;
}
// punkty - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,
SUM(drivers_gp_results.race_points) + COALESCE((SELECT SUM(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp),0) as amount,
SUM(drivers_gp_results.race_points) + COALESCE((SELECT SUM(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp),0) as y
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp
and gp.name_alias='$id' and drivers_gp_results.race_points>0
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamPointsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamPointsItems[] = $r;
}
$pointsItems["drivers"]=$driverPointsItems;
$pointsItems["teams"]=$teamPointsItems;
$statsItems["points"]=$pointsItems;

// pole position - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,COUNT(drivers_gp_starting_grid.id_starting_grid) amount,COUNT(drivers_gp_starting_grid.id_starting_grid) y
from drivers_gp_starting_grid,drivers,teams,gp
where drivers.id_driver=drivers_gp_starting_grid.id_driver and teams.id_team=drivers_gp_starting_grid.id_team and gp.id_gp=drivers_gp_starting_grid.id_gp and gp.name_alias='$id'
and drivers_gp_starting_grid.is_pp=1
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverPoleposItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverPoleposItems[] = $r;
}
// pole position - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,count(drivers_gp_starting_grid.id_starting_grid) amount,count(drivers_gp_starting_grid.id_starting_grid) y
from drivers_gp_starting_grid,teams,gp where teams.id_team=drivers_gp_starting_grid.id_team and gp.id_gp=drivers_gp_starting_grid.id_gp
and gp.name_alias='$id' and drivers_gp_starting_grid.is_pp=1
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamPoleposItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamPoleposItems[] = $r;
}
$poleposItems["drivers"]=$driverPoleposItems;
$poleposItems["teams"]=$teamPoleposItems;
$statsItems["polepos"]=$poleposItems;

// best laps - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,drivers.surname name,drivers.country_code countryCode,drivers.country,
drivers.picture,COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y
from drivers_gp_results,drivers,teams,gp
where drivers.id_driver=drivers_gp_results.id_driver and teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp and gp.name_alias='$id'
and drivers_gp_results.race_best_lap=1
GROUP BY drivers.id_driver order by amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$driverBestLapsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driverBestLapsItems[] = $r;
}
// best laps - zespoly
$query="SELECT teams.alias_name alias,lower(teams.team) id, teams.name, teams.country_code countryCode, teams.country,
teams.picture picture,count(drivers_gp_results.race_date) amount,count(drivers_gp_results.race_date) y
from drivers_gp_results,teams,gp where teams.id_team=drivers_gp_results.id_team and gp.id_gp=drivers_gp_results.id_gp
and gp.name_alias='$id' and drivers_gp_results.race_best_lap=1
GROUP BY id order by amount DESC, teams.name LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$teamBestLapsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamBestLapsItems[] = $r;
}
$bestlapsItems["drivers"]=$driverBestLapsItems;
$bestlapsItems["teams"]=$teamBestLapsItems;
$statsItems["bestlaps"]=$bestlapsItems;


$query="SELECT SUBSTR(race_date, 1, 4) season,race_date date,gp.circuit circuit,gp.circuit_alias circuitAlias, gp.track,
drivers.alias driverAlias,drivers.id_driver idDriver,CONCAT(drivers.name, ' ' ,drivers.surname) driver,drivers.country_code driverCountryCode,drivers.picture,teams.alias_name teamAlias,
lower(teams.team) idTeam,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) team,teams.country_code teamCountryCode, 
race_points + COALESCE((SELECT sprint_points FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver AND drivers_sprint_results.season=SUBSTR(race_date, 1, 4) AND drivers_sprint_results.id_gp=gp.id_gp),0) as points,
race_add_info info,
CONCAT(
  ppDrivers.name,
  ' ',
  ppDrivers.surname
) ppDriver,
ppDrivers.alias ppAlias,
ppDrivers.id_driver ppId,
ppDrivers.country_code ppCountryCode,
teams_models.team_name modelName
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN teams ON teams.id_team=drivers_gp_results.id_team
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_results.id_gp AND drivers_gp_starting_grid.season = drivers_gp_results.season AND drivers_gp_starting_grid.is_pp=1
LEFT JOIN drivers ppDrivers ON ppDrivers.id_driver=drivers_gp_starting_grid.id_driver
LEFT JOIN drivers_gp_involvements ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
WHERE gp.name_alias='$id' and drivers_gp_results.race_pos=1
order by race_date DESC";
$result = mysqli_query($dbhandle,$query);
$raceItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $raceItems[] = $r;
}
$statsItems["races"]=$raceItems;

$gpStatsItems["circuits"]=$gpCircuitsItems;
$gpStatsItems["stats"]=$statsItems;
$gpStatsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $gpStatsItems;

print json_encode($response);
mysqli_free_result($result);
?>
