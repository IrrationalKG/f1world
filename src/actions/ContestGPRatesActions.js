// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_CONTEST_GPRATES_REQUEST = "FETCH_CONTEST_GPRATES_REQUEST";
export const FETCH_CONTEST_GPRATES_SUCCESS = "FETCH_CONTEST_GPRATES_SUCCESS";
export const FETCH_CONTEST_GPRATES_ERROR = "FETCH_CONTEST_GPRATES_ERROR";

function requestData() {
  return {
    type: FETCH_CONTEST_GPRATES_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_GPRATES_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_GPRATES_ERROR,
    payload: error,
  };
}

export function fetchContestGPRates(year, gp, lang) {
  const url = `${ROOT_URL}/get_contest_gprates.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          gp: gp,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
