/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderAwards","renderPlacesStats"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Icon,
  Table,
  Grid,
  Statistic,
  Label,
  Item,
  Message,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchContestGPRates } from "../../actions/ContestGPRatesActions";
import { fetchComboContestSeasons } from "../../actions/ComboContestSeasonsActions";
import { fetchComboContestGP } from "../../actions/ComboContestGPActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ContestGPRates.less";

class ContestGPRates extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValueSeason: { value: props.match.params.year },
      selectValueGP: { value: props.match.params.gpId },
    };

    this.onInputSeasonChange = this.onInputSeasonChange.bind(this);
    this.onInputGPChange = this.onInputGPChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchContestGPRates(
      props.match.params.year,
      props.match.params.gpId,
      Cookies.get("lang")
    );
    props.fetchComboContestSeasons();
    props.fetchComboContestGP(props.match.params.year, Cookies.get("lang"));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year, gpId } = props.match.params;
    const nextYear = nextProps.match.params.year;
    const nextGPId = nextProps.match.params.gpId;

    this.setState({
      isLoadingExternally: false,
      selectValueSeason: {
        value: nextYear,
        label: nextProps.comboContestSeasons.data?.find(
          (e) => e.value == nextYear
        )?.label,
      },
      selectValueGP: {
        value: nextGPId,
        label: nextProps.comboContestGP.data?.find((e) => e.value == nextGPId)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchContestGPRates(nextYear, nextGPId, Cookies.get("lang"));
      props.fetchComboContestGP(nextYear, Cookies.get("lang"));
    }
    if (gpId !== nextGPId) {
      props.fetchContestGPRates(nextYear, nextGPId, Cookies.get("lang"));
    }
  }

  onInputSeasonChange(event) {
    const { props, state } = this;
    props.history.push({
      pathname: `/contest/gprate/${event.value}/${state.selectValueGP.value}`,
    });
    this.setState({
      selectValueSeason: event,
    });
  }

  onInputGPChange(event) {
    const { props, state } = this;
    props.history.push({
      pathname: `/contest/gprate/${state.selectValueSeason.value}/${event.value}`,
    });
    this.setState({
      selectValueGP: event,
    });
  }

  renderGPRatesMobile() {
    const { props } = this;
    const { season, rates, gpResults } = props.contestGPRates.data;
    if (rates.length > 0) {
      const classItems = [];
      let place = 0;
      rates.forEach((item) => {
        place += 1;
        let ppEmpty = true;
        let m1Empty = true;
        let m2Empty = true;
        let m3Empty = true;
        let m4Empty = true;
        let m5Empty = true;
        let m6Empty = true;
        let m7Empty = true;
        let m8Empty = true;
        let m9Empty = true;
        let m10Empty = true;

        let ppColor = "empty";
        let m1Color = "empty";
        let m2Color = "empty";
        let m3Color = "empty";
        let m4Color = "empty";
        let m5Color = "empty";
        let m6Color = "empty";
        let m7Color = "empty";
        let m8Color = "empty";
        let m9Color = "empty";
        let m10Color = "empty";

        if (gpResults !== null) {
          if (item.pp === gpResults.pp) {
            ppColor = "exact";
            ppEmpty = false;
          } else {
            ppColor = "empty";
            ppEmpty = false;
          }

          if (item.m1 === gpResults.m1) {
            m1Color = "exact";
            m1Empty = false;
          } else if (
            item.m1 === gpResults.m2 ||
            item.m1 === gpResults.m3 ||
            item.m1 === gpResults.m4 ||
            item.m1 === gpResults.m5 ||
            item.m1 === gpResults.m6 ||
            item.m1 === gpResults.m7 ||
            item.m1 === gpResults.m8 ||
            item.m1 === gpResults.m9 ||
            item.m1 === gpResults.m10
          ) {
            m1Color = "top10";
            m1Empty = false;
          }

          if (item.m2 === gpResults.m2) {
            m2Color = "exact";
            m2Empty = false;
          } else if (
            item.m2 === gpResults.m1 ||
            item.m2 === gpResults.m3 ||
            item.m2 === gpResults.m4 ||
            item.m2 === gpResults.m5 ||
            item.m2 === gpResults.m6 ||
            item.m2 === gpResults.m7 ||
            item.m2 === gpResults.m8 ||
            item.m2 === gpResults.m9 ||
            item.m2 === gpResults.m10
          ) {
            m2Color = "top10";
            m2Empty = false;
          }

          if (item.m3 === gpResults.m3) {
            m3Color = "exact";
            m3Empty = false;
          } else if (
            item.m3 === gpResults.m1 ||
            item.m3 === gpResults.m2 ||
            item.m3 === gpResults.m4 ||
            item.m3 === gpResults.m5 ||
            item.m3 === gpResults.m6 ||
            item.m3 === gpResults.m7 ||
            item.m3 === gpResults.m8 ||
            item.m3 === gpResults.m9 ||
            item.m3 === gpResults.m10
          ) {
            m3Color = "top10";
            m3Empty = false;
          }

          if (item.m4 === gpResults.m4) {
            m4Color = "exact";
            m4Empty = false;
          } else if (
            item.m4 === gpResults.m1 ||
            item.m4 === gpResults.m2 ||
            item.m4 === gpResults.m3 ||
            item.m4 === gpResults.m5 ||
            item.m4 === gpResults.m6 ||
            item.m4 === gpResults.m7 ||
            item.m4 === gpResults.m8 ||
            item.m4 === gpResults.m9 ||
            item.m4 === gpResults.m10
          ) {
            m4Color = "top10";
            m4Empty = false;
          }

          if (item.m5 === gpResults.m5) {
            m5Color = "exact";
            m5Empty = false;
          } else if (
            item.m5 === gpResults.m1 ||
            item.m5 === gpResults.m2 ||
            item.m5 === gpResults.m3 ||
            item.m5 === gpResults.m4 ||
            item.m5 === gpResults.m6 ||
            item.m5 === gpResults.m7 ||
            item.m5 === gpResults.m8 ||
            item.m5 === gpResults.m9 ||
            item.m5 === gpResults.m10
          ) {
            m5Color = "top10";
            m5Empty = false;
          }

          if (item.m6 === gpResults.m6) {
            m6Color = "exact";
            m6Empty = false;
          } else if (
            item.m6 === gpResults.m1 ||
            item.m6 === gpResults.m2 ||
            item.m6 === gpResults.m3 ||
            item.m6 === gpResults.m4 ||
            item.m6 === gpResults.m5 ||
            item.m6 === gpResults.m7 ||
            item.m6 === gpResults.m8 ||
            item.m6 === gpResults.m9 ||
            item.m6 === gpResults.m10
          ) {
            m6Color = "top10";
            m6Empty = false;
          }

          if (item.m7 === gpResults.m7) {
            m7Color = "exact";
            m7Empty = false;
          } else if (
            item.m7 === gpResults.m1 ||
            item.m7 === gpResults.m2 ||
            item.m7 === gpResults.m3 ||
            item.m7 === gpResults.m4 ||
            item.m7 === gpResults.m5 ||
            item.m7 === gpResults.m6 ||
            item.m7 === gpResults.m8 ||
            item.m7 === gpResults.m9 ||
            item.m7 === gpResults.m10
          ) {
            m7Color = "top10";
            m7Empty = false;
          }

          if (item.m8 === gpResults.m8) {
            m8Color = "exact";
            m8Empty = false;
          } else if (
            item.m8 === gpResults.m1 ||
            item.m8 === gpResults.m2 ||
            item.m8 === gpResults.m3 ||
            item.m8 === gpResults.m4 ||
            item.m8 === gpResults.m5 ||
            item.m8 === gpResults.m6 ||
            item.m8 === gpResults.m7 ||
            item.m8 === gpResults.m9 ||
            item.m8 === gpResults.m10
          ) {
            m8Color = "top10";
            m8Empty = false;
          }

          if (item.m9 === gpResults.m9) {
            m9Color = "exact";
            m9Empty = false;
          } else if (
            item.m9 === gpResults.m1 ||
            item.m9 === gpResults.m2 ||
            item.m9 === gpResults.m3 ||
            item.m9 === gpResults.m4 ||
            item.m9 === gpResults.m5 ||
            item.m9 === gpResults.m6 ||
            item.m9 === gpResults.m7 ||
            item.m9 === gpResults.m8 ||
            item.m9 === gpResults.m10
          ) {
            m9Color = "top10";
            m9Empty = false;
          }

          if (item.m10 === gpResults.m10) {
            m10Color = "exact";
            m10Empty = false;
          } else if (
            item.m10 === gpResults.m1 ||
            item.m10 === gpResults.m2 ||
            item.m10 === gpResults.m3 ||
            item.m10 === gpResults.m4 ||
            item.m10 === gpResults.m5 ||
            item.m10 === gpResults.m6 ||
            item.m10 === gpResults.m7 ||
            item.m10 === gpResults.m8 ||
            item.m10 === gpResults.m9
          ) {
            m10Color = "top10";
            m10Empty = false;
          }
        }
        const link = `/contest/player/${item.alias}`;
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
              <Item.Meta>
                {"Punkty: "}
                {item.points}
                {" Premia: "}
                {item.bonus}
              </Item.Meta>
              <Item.Meta>
                <div>{item.sendDate}</div>
              </Item.Meta>
              <Item.Description>
                {season >= 2005 && (
                  <span className="key-value-box">
                    <div className={`key-value-box-header ${ppColor}`}>
                      <FormattedMessage id={"app.table.header.polepos"} />
                    </div>
                    <div className="key-value-box-value">
                      {item.pp === "" && "-"}
                      {ppEmpty && item.pp}
                      {!ppEmpty && item.pp}
                    </div>
                  </span>
                )}
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m1Color}`}>
                    <FormattedMessage id={"app.table.header.p1"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m1 === "" && "-"}
                    {m1Empty && item.m1}
                    {!m1Empty && item.m1}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m2Color}`}>
                    <FormattedMessage id={"app.table.header.p2"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m2 === "" && "-"}
                    {m2Empty && item.m2}
                    {!m2Empty && item.m2}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m3Color}`}>
                    <FormattedMessage id={"app.table.header.p3"} />
                  </div>
                  <div className="key-value-box-value">
                    {item.m3 === "" && "-"}
                    {m3Empty && item.m3}
                    {!m3Empty && item.m3}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m4Color}`}>P4</div>
                  <div className="key-value-box-value">
                    {item.m4 === "" && "-"}
                    {m4Empty && item.m4}
                    {!m4Empty && item.m4}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m5Color}`}>P5</div>
                  <div className="key-value-box-value">
                    {item.m5 === "" && "-"}
                    {m5Empty && item.m5}
                    {!m5Empty && item.m5}
                  </div>
                </span>
                <span className="key-value-box">
                  <div className={`key-value-box-header ${m6Color}`}>P6</div>
                  <div className="key-value-box-value">
                    {item.m6 === "" && "-"}
                    {m6Empty && item.m6}
                    {!m6Empty && item.m6}
                  </div>
                </span>
                {season >= 2005 && (
                  <>
                    <span className="key-value-box">
                      <div className={`key-value-box-header ${m7Color}`}>
                        P7
                      </div>
                      <div className="key-value-box-value">
                        {item.m7 === "" && "-"}
                        {m7Empty && item.m7}
                        {!m7Empty && item.m7}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className={`key-value-box-header ${m8Color}`}>
                        P8
                      </div>
                      <div className="key-value-box-value">
                        {item.m8 === "" && "-"}
                        {m8Empty && item.m8}
                        {!m8Empty && item.m8}
                      </div>
                    </span>
                  </>
                )}
                {season >= 2010 && (
                  <>
                    <span className="key-value-box">
                      <div className={`key-value-box-header ${m9Color}`}>
                        P9
                      </div>
                      <div className="key-value-box-value">
                        {item.m9 === "" && "-"}
                        {m9Empty && item.m9}
                        {!m9Empty && item.m9}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className={`key-value-box-header ${m10Color}`}>
                        P10
                      </div>
                      <div className="key-value-box-value">
                        {item.m10 === "" && "-"}
                        {m10Empty && item.m10}
                        {!m10Empty && item.m10}
                      </div>
                    </span>
                  </>
                )}
              </Item.Description>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right" size="large">
                <Statistic.Value>{item.sum}</Statistic.Value>
                <Statistic.Label>Suma</Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderGPRates() {
    const elements = [];
    const { props } = this;
    const { season, rates, gpResults } = props.contestGPRates.data;
    let cnt = 0;
    rates.forEach((item) => {
      cnt += 1;

      let ppEmpty = true;
      let m1Empty = true;
      let m2Empty = true;
      let m3Empty = true;
      let m4Empty = true;
      let m5Empty = true;
      let m6Empty = true;
      let m7Empty = true;
      let m8Empty = true;
      let m9Empty = true;
      let m10Empty = true;

      let ppColor = "";
      let m1Color = "";
      let m2Color = "";
      let m3Color = "";
      let m4Color = "";
      let m5Color = "";
      let m6Color = "";
      let m7Color = "";
      let m8Color = "";
      let m9Color = "";
      let m10Color = "";

      if (gpResults !== null) {
        if (item.pp === gpResults.pp) {
          ppColor = "red";
          ppEmpty = false;
        } else {
          ppColor = "";
          ppEmpty = true;
        }

        if (item.m1 === gpResults.m1) {
          m1Color = "red";
          m1Empty = false;
        } else if (
          item.m1 === gpResults.m2 ||
          item.m1 === gpResults.m3 ||
          item.m1 === gpResults.m4 ||
          item.m1 === gpResults.m5 ||
          item.m1 === gpResults.m6 ||
          item.m1 === gpResults.m7 ||
          item.m1 === gpResults.m8 ||
          item.m1 === gpResults.m9 ||
          item.m1 === gpResults.m10
        ) {
          m1Color = "grey";
          m1Empty = false;
        }

        if (item.m2 === gpResults.m2) {
          m2Color = "red";
          m2Empty = false;
        } else if (
          item.m2 === gpResults.m1 ||
          item.m2 === gpResults.m3 ||
          item.m2 === gpResults.m4 ||
          item.m2 === gpResults.m5 ||
          item.m2 === gpResults.m6 ||
          item.m2 === gpResults.m7 ||
          item.m2 === gpResults.m8 ||
          item.m2 === gpResults.m9 ||
          item.m2 === gpResults.m10
        ) {
          m2Color = "grey";
          m2Empty = false;
        }

        if (item.m3 === gpResults.m3) {
          m3Color = "red";
          m3Empty = false;
        } else if (
          item.m3 === gpResults.m1 ||
          item.m3 === gpResults.m2 ||
          item.m3 === gpResults.m4 ||
          item.m3 === gpResults.m5 ||
          item.m3 === gpResults.m6 ||
          item.m3 === gpResults.m7 ||
          item.m3 === gpResults.m8 ||
          item.m3 === gpResults.m9 ||
          item.m3 === gpResults.m10
        ) {
          m3Color = "grey";
          m3Empty = false;
        }

        if (item.m4 === gpResults.m4) {
          m4Color = "red";
          m4Empty = false;
        } else if (
          item.m4 === gpResults.m1 ||
          item.m4 === gpResults.m2 ||
          item.m4 === gpResults.m3 ||
          item.m4 === gpResults.m5 ||
          item.m4 === gpResults.m6 ||
          item.m4 === gpResults.m7 ||
          item.m4 === gpResults.m8 ||
          item.m4 === gpResults.m9 ||
          item.m4 === gpResults.m10
        ) {
          m4Color = "grey";
          m4Empty = false;
        }

        if (item.m5 === gpResults.m5) {
          m5Color = "red";
          m5Empty = false;
        } else if (
          item.m5 === gpResults.m1 ||
          item.m5 === gpResults.m2 ||
          item.m5 === gpResults.m3 ||
          item.m5 === gpResults.m4 ||
          item.m5 === gpResults.m6 ||
          item.m5 === gpResults.m7 ||
          item.m5 === gpResults.m8 ||
          item.m5 === gpResults.m9 ||
          item.m5 === gpResults.m10
        ) {
          m5Color = "grey";
          m5Empty = false;
        }

        if (item.m6 === gpResults.m6) {
          m6Color = "red";
          m6Empty = false;
        } else if (
          item.m6 === gpResults.m1 ||
          item.m6 === gpResults.m2 ||
          item.m6 === gpResults.m3 ||
          item.m6 === gpResults.m4 ||
          item.m6 === gpResults.m5 ||
          item.m6 === gpResults.m7 ||
          item.m6 === gpResults.m8 ||
          item.m6 === gpResults.m9 ||
          item.m6 === gpResults.m10
        ) {
          m6Color = "grey";
          m6Empty = false;
        }

        if (item.m7 === gpResults.m7) {
          m7Color = "red";
          m7Empty = false;
        } else if (
          item.m7 === gpResults.m1 ||
          item.m7 === gpResults.m2 ||
          item.m7 === gpResults.m3 ||
          item.m7 === gpResults.m4 ||
          item.m7 === gpResults.m5 ||
          item.m7 === gpResults.m6 ||
          item.m7 === gpResults.m8 ||
          item.m7 === gpResults.m9 ||
          item.m7 === gpResults.m10
        ) {
          m7Color = "grey";
          m7Empty = false;
        }

        if (item.m8 === gpResults.m8) {
          m8Color = "red";
          m8Empty = false;
        } else if (
          item.m8 === gpResults.m1 ||
          item.m8 === gpResults.m2 ||
          item.m8 === gpResults.m3 ||
          item.m8 === gpResults.m4 ||
          item.m8 === gpResults.m5 ||
          item.m8 === gpResults.m6 ||
          item.m8 === gpResults.m7 ||
          item.m8 === gpResults.m9 ||
          item.m8 === gpResults.m10
        ) {
          m8Color = "grey";
          m8Empty = false;
        }

        if (item.m9 === gpResults.m9) {
          m9Color = "red";
          m9Empty = false;
        } else if (
          item.m9 === gpResults.m1 ||
          item.m9 === gpResults.m2 ||
          item.m9 === gpResults.m3 ||
          item.m9 === gpResults.m4 ||
          item.m9 === gpResults.m5 ||
          item.m9 === gpResults.m6 ||
          item.m9 === gpResults.m7 ||
          item.m9 === gpResults.m8 ||
          item.m9 === gpResults.m10
        ) {
          m9Color = "grey";
          m9Empty = false;
        }

        if (item.m10 === gpResults.m10) {
          m10Color = "red";
          m10Empty = false;
        } else if (
          item.m10 === gpResults.m1 ||
          item.m10 === gpResults.m2 ||
          item.m10 === gpResults.m3 ||
          item.m10 === gpResults.m4 ||
          item.m10 === gpResults.m5 ||
          item.m10 === gpResults.m6 ||
          item.m10 === gpResults.m7 ||
          item.m10 === gpResults.m8 ||
          item.m10 === gpResults.m9
        ) {
          m10Color = "grey";
          m10Empty = false;
        }
      }

      const element = (
        <Table.Row key={`gprates-${item.id}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
            <div className="send-date">{item.sendDate}</div>
          </Table.Cell>
          {season >= 2005 && (
            <Table.Cell data-title="Pole Position">
              {item.pp === "" && "-"}
              {ppEmpty && item.pp}
              {!ppEmpty && (
                <Label
                  htmlFor="pp"
                  color={ppColor}
                  key={`gprates-${item.id}-pp`}
                  size="large"
                >
                  {item.pp}
                </Label>
              )}
            </Table.Cell>
          )}
          <Table.Cell data-title="1.">
            {item.m1 === "" && "-"}
            {m1Empty && item.m1}
            {!m1Empty && (
              <Label
                htmlFor="m1"
                color={m1Color}
                key={`gprates-${item.id}-m1`}
                size="large"
              >
                {item.m1}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="2.">
            {item.m2 === "" && "-"}
            {m2Empty && item.m2}
            {!m2Empty && (
              <Label
                htmlFor="m2"
                color={m2Color}
                key={`gprates-${item.id}-m2`}
                size="large"
              >
                {item.m2}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="3.">
            {item.m3 === "" && "-"}
            {m3Empty && item.m3}
            {!m3Empty && (
              <Label
                htmlFor="m3"
                color={m3Color}
                key={`gprates-${item.id}-m3`}
                size="large"
              >
                {item.m3}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="4.">
            {item.m4 === "" && "-"}
            {m4Empty && item.m4}
            {!m4Empty && (
              <Label
                htmlFor="m4"
                color={m4Color}
                key={`gprates-${item.id}-m4`}
                size="large"
              >
                {item.m4}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="5.">
            {item.m5 === "" && "-"}
            {m5Empty && item.m5}
            {!m5Empty && (
              <Label
                htmlFor="m5"
                color={m5Color}
                key={`gprates-${item.id}-m5`}
                size="large"
              >
                {item.m5}
              </Label>
            )}
          </Table.Cell>
          <Table.Cell data-title="6.">
            {item.m6 === "" && "-"}
            {m6Empty && item.m6}
            {!m6Empty && (
              <Label
                htmlFor="m6"
                color={m6Color}
                key={`gprates-${item.id}-m6`}
                size="large"
              >
                {item.m6}
              </Label>
            )}
          </Table.Cell>
          {season >= 2005 && (
            <Table.Cell data-title="7.">
              {item.m7 === "" && "-"}
              {m7Empty && item.m7}
              {!m7Empty && (
                <Label
                  htmlFor="m7"
                  color={m7Color}
                  key={`gprates-${item.id}-m7`}
                  size="large"
                >
                  {item.m7}
                </Label>
              )}
            </Table.Cell>
          )}
          {season >= 2005 && (
            <Table.Cell data-title="8.">
              {item.m8 === "" && "-"}
              {m8Empty && item.m8}
              {!m8Empty && (
                <Label
                  htmlFor="m8"
                  color={m8Color}
                  key={`gprates-${item.id}-m8`}
                  size="large"
                >
                  {item.m8}
                </Label>
              )}
            </Table.Cell>
          )}
          {season >= 2010 && (
            <Table.Cell data-title="9.">
              {item.m9 === "" && "-"}
              {m9Empty && item.m9}
              {!m9Empty && (
                <Label
                  htmlFor="m9"
                  color={m9Color}
                  key={`gprates-${item.id}-m9`}
                  size="large"
                >
                  {item.m9}
                </Label>
              )}
            </Table.Cell>
          )}
          {season >= 2010 && (
            <Table.Cell data-title="10.">
              {item.m10 === "" && "-"}
              {m10Empty && item.m10}
              {!m10Empty && (
                <Label
                  htmlFor="m10"
                  color={m10Color}
                  key={`gprates-${item.id}-m10`}
                  size="large"
                >
                  {item.m10}
                </Label>
              )}
            </Table.Cell>
          )}
          <Table.Cell data-title="Punkty">
            {item.points === null ? "-" : item.points}
          </Table.Cell>
          <Table.Cell data-title="Premia">
            {item.bonus === null ? "-" : item.bonus}
          </Table.Cell>
          <Table.Cell data-title="Suma">
            {item.sum === null ? "-" : item.sum}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.contestGPRates.data || props.contestGPRates.loading) {
      return (
        <section
          id="contest-gprates-details"
          name="contest-gprates-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { name, season, prevId, prevYear, nextId, nextYear, rates } =
      props.contestGPRates.data;

    const linkPrev = `/contest/gprate/${prevYear}/${prevId}`;
    const linkNext = `/contest/gprate/${nextYear}/${nextId}`;

    if (rates.length === 0) {
      return (
        <section
          id="contest-gprates-details"
          name="contest-gprates-details"
          className="section-page"
        >
          <SectionPageBanner
            title={name !== null ? `${name} ${season}` : `${season}`}
            subtitle={
              <FormattedMessage id={"app.page.contest.gprate.subtitle"} />
            }
            linkPrev={parseInt(prevYear, 10) >= 2004 ? linkPrev : null}
            linkNext={nextId !== null ? linkNext : null}
          />
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column mobile={16} tablet={4} computer={3}>
                <div className="section-page-filters">
                  <Segment padded basic>
                    <Grid stackable padded>
                      <Grid.Row columns={1}>
                        <Grid.Column>
                          <Segment basic>
                            <Select
                              ref={(ref) => {
                                this.comboContestGP = ref;
                              }}
                              isLoading={isLoadingExternally}
                              name="gp"
                              multi={multi}
                              ignoreCase={ignoreCase}
                              ignoreAccents={ignoreAccents}
                              value={state.selectValueGP}
                              onChange={this.onInputGPChange}
                              options={props.comboContestGP.data}
                              clearable={clearable}
                              labelKey="label"
                              valueKey="value"
                              placeholder="Wybierz grand prix"
                              noResultsText={
                                <FormattedMessage
                                  id={"app.placeholder.no.results"}
                                />
                              }
                              className="react-select-container"
                              classNamePrefix="react-select"
                            />
                          </Segment>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment basic>
                            <Select
                              ref={(ref) => {
                                this.comboContestSeason = ref;
                              }}
                              isLoading={isLoadingExternally}
                              name="seasons"
                              multi={multi}
                              ignoreCase={ignoreCase}
                              ignoreAccents={ignoreAccents}
                              value={state.selectValueSeason}
                              onChange={this.onInputSeasonChange}
                              options={props.comboContestSeasons.data}
                              clearable={clearable}
                              labelKey="label"
                              valueKey="value"
                              placeholder={formatMessage({
                                id: "app.placeholder.select.season",
                              })}
                              noResultsText={
                                <FormattedMessage
                                  id={"app.placeholder.no.results"}
                                />
                              }
                              className="react-select-container"
                              classNamePrefix="react-select"
                            />
                          </Segment>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment padded basic>
                  <Header as="h2" icon textAlign="center">
                    <Icon name="info" circular />
                    <Header.Content>
                      <FormattedMessage id={"app.page.contest.gprate.info"} />
                    </Header.Content>
                  </Header>
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="contest-gprates-details"
        name="contest-gprates-details"
        className="section-page"
      >
        <SectionPageBanner
          title={name !== null ? `${name} ${season}` : `${season}`}
          subtitle="Typy"
          linkPrev={parseInt(prevYear, 10) >= 2004 ? linkPrev : null}
          linkNext={nextId !== null ? linkNext : null}
        />
        <Segment basic className="left-sidebar">
          <Grid stackable centered columns="equal">
            <Grid.Column
              mobile={16}
              tablet={4}
              computer={3}
              className="info-box"
            >
              <Segment basic padded>
                <Grid stackable padded>
                  <Grid.Row columns={1}>
                    <Grid.Column>
                      <Select
                        ref={(ref) => {
                          this.comboContestGP = ref;
                        }}
                        isLoading={isLoadingExternally}
                        name="gp"
                        multi={multi}
                        ignoreCase={ignoreCase}
                        ignoreAccents={ignoreAccents}
                        value={state.selectValueGP}
                        onChange={this.onInputGPChange}
                        options={props.comboContestGP.data}
                        clearable={clearable}
                        labelKey="label"
                        valueKey="value"
                        placeholder="Wybierz grand prix"
                        noResultsText={
                          <FormattedMessage id={"app.placeholder.no.results"} />
                        }
                        className="react-select-container"
                        classNamePrefix="react-select"
                      />
                    </Grid.Column>
                    <Grid.Column>
                      <Select
                        ref={(ref) => {
                          this.comboContestSeason = ref;
                        }}
                        isLoading={isLoadingExternally}
                        name="seasons"
                        multi={multi}
                        ignoreCase={ignoreCase}
                        ignoreAccents={ignoreAccents}
                        value={state.selectValueSeason}
                        onChange={this.onInputSeasonChange}
                        options={props.comboContestSeasons.data}
                        clearable={clearable}
                        labelKey="label"
                        valueKey="value"
                        placeholder={formatMessage({
                          id: "app.placeholder.select.season",
                        })}
                        noResultsText={
                          <FormattedMessage id={"app.placeholder.no.results"} />
                        }
                        className="react-select-container"
                        classNamePrefix="react-select"
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={12} computer={13}>
              <Segment basic>
                <div className="section-page-content gprates">
                  <Segment basic className="gprates-mobile">
                    <Item.Group divided unstackable>
                      {this.renderGPRatesMobile()}
                    </Item.Group>
                  </Segment>
                  <Segment basic className="gprates-desktop overflow">
                    <SectionPageHeader title={name} className="left-aligned" />
                    <Table
                      basic="very"
                      celled
                      className="responsive-table center-aligned"
                    >
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>#</Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage id={"app.table.header.player"} />
                          </Table.HeaderCell>
                          {season >= 2005 && (
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.polepos"}
                              />
                            </Table.HeaderCell>
                          )}
                          <Table.HeaderCell>1</Table.HeaderCell>
                          <Table.HeaderCell>2</Table.HeaderCell>
                          <Table.HeaderCell>3</Table.HeaderCell>
                          <Table.HeaderCell>4</Table.HeaderCell>
                          <Table.HeaderCell>5</Table.HeaderCell>
                          <Table.HeaderCell>6</Table.HeaderCell>
                          {season >= 2005 && (
                            <Table.HeaderCell>7</Table.HeaderCell>
                          )}
                          {season >= 2005 && (
                            <Table.HeaderCell>8</Table.HeaderCell>
                          )}
                          {season >= 2010 && (
                            <Table.HeaderCell>9</Table.HeaderCell>
                          )}
                          {season > 2010 && (
                            <Table.HeaderCell>10</Table.HeaderCell>
                          )}
                          <Table.HeaderCell>
                            <FormattedMessage id={"app.table.header.points"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <FormattedMessage id={"app.table.header.bonus"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell>
                            <FormattedMessage id={"app.table.header.sum"} />
                          </Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>{this.renderGPRates()}</Table.Body>
                    </Table>
                  </Segment>
                  <Message>
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <Message.List>
                      <Message.Item>
                        <span className="ui large red label">
                          {" "}
                          <FormattedMessage id={"app.table.header.driver"} />
                        </span>{" "}
                        <FormattedMessage
                          id={"app.page.contest.gprate.info2"}
                        />
                      </Message.Item>
                      <Message.Item>
                        <span className="ui large grey label">
                          <FormattedMessage id={"app.table.header.driver"} />
                        </span>{" "}
                        <FormattedMessage
                          id={"app.page.contest.gprate.info3"}
                        />
                        {season >= 2010 && (
                          <FormattedMessage
                            id={"app.page.contest.gprate.info3.1"}
                          />
                        )}
                        {season >= 2005 && season <= 2009 && (
                          <FormattedMessage
                            id={"app.page.contest.gprate.info3.2"}
                          />
                        )}
                        {season === 2004 && (
                          <FormattedMessage
                            id={"app.page.contest.gprate.info3.3"}
                          />
                        )}
                      </Message.Item>
                    </Message.List>
                  </Message>
                </div>
              </Segment>
            </Grid.Column>
          </Grid>
        </Segment>
      </section>
    );
  }
}

ContestGPRates.propTypes = {
  fetchContestGPRates: PropTypes.func.isRequired,
  contestGPRates: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboContestSeasons: PropTypes.func.isRequired,
  comboContestSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  fetchComboContestGP: PropTypes.func.isRequired,
  comboContestGP: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  gp: PropTypes.string,
};

function mapStateToProps({
  contestGPRates,
  comboContestSeasons,
  comboContestGP,
}) {
  return {
    contestGPRates,
    comboContestSeasons,
    comboContestGP,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchContestGPRates,
      fetchComboContestSeasons,
      fetchComboContestGP,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(ContestGPRates)
);
