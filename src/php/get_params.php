<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT
last_gp lastGP,
last_gp_alias lastGPAlias,";
if ($lang=='pl') {
  $query.="last_gp_name lastGPName,";
}else{
  $query.="last_gp_name_en lastGPName,";
}  
$query.="last_gp_id lastGPId,
last_gp_year lastGPYear,
next_gp nextGP,
next_gp_alias nextGPAlias,";
if ($lang=='pl') {
  $query.="next_gp_name nextGPName,";
}else{
  $query.="next_gp_name_en nextGPName,";
}  
$query.="next_gp_id nextGPId,
next_gp_year nextGPYear,
next_gp_month nextGPMonth,
next_gp_day nextGPDay,
next_gp_hour nextGPHour,
next_gp_min nextGPMin,
next_gp_qual_year nextGPQualYear,
next_gp_qual_month nextGPQualMonth,
next_gp_qual_day nextGPQualDay,
next_gp_qual_hour nextGPQualHour,
next_gp_qual_min nextGPQualMin,
contest_round contestRound
FROM params ORDER BY nextGPYear desc, contestRound DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$paramsItems;
while($r = mysqli_fetch_assoc($result)) {
  $paramsItems = $r;
  $nextGPId=$r["nextGPId"];
  $nextGPYear=$r["nextGPYear"];
}

$paramItems=$paramsItems;
$paramItems["query"]=$query;

//pobranie czy gp ma sprint
$query="SELECT sprint from gp_season
where id_gp='$nextGPId' 
and season='$nextGPYear'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $paramItems["nextGPSprint"]=$r["sprint"];
}

$paramItems["currentGPYear"]="2025";
$paramItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $paramItems;

print json_encode($response);
mysqli_free_result($result);
?>
