/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu","renderCountriesFlags"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Flag,
  Grid,
  Loader,
  Item,
  Header,
  Divider,
  Label,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import {
  fetchTeamsStats,
  fetchTeamsStatsByCountry,
} from "../../actions/TeamsStatsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./TeamsStats.less";

class TeamsStats extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderTeamsStats = this.renderTeamsStats.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    if (props.match.params.letter) {
      props.fetchTeamsStats(props.match.params.letter, Cookies.get("lang"));
    }
    if (props.match.params.country) {
      props.fetchTeamsStatsByCountry(
        props.match.params.country,
        Cookies.get("lang")
      );
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { letter } = props.match.params;
    const nextLetter = nextProps.match.params.letter;
    if (nextLetter && letter !== nextLetter) {
      props.fetchTeamsStats(nextLetter, Cookies.get("lang"));
    }

    const { country } = props.match.params;
    const nextCountry = nextProps.match.params.country;
    if (nextCountry && country !== nextCountry) {
      props.fetchTeamsStatsByCountry(nextCountry, Cookies.get("lang"));
    }
  }

  renderCountriesFlags(countries) {
    const items = [];
    countries.forEach((country) => {
      const record = <Flag key={country} name={country} />;
      items.push(record);
    });
    return items;
  }

  renderTeamsStats() {
    const items = [];
    const { props } = this;
    const { teams } = props.teamsStats.data;
    const { formatMessage } = this.props.intl;

    teams.forEach((item) => {
      let picProfile = "/build/images/teams/team_no_profile.jpg";
      const picProfileLogo = `/build/images/teams/team_${item.id}_logo.jpg`;
      if (item.picture === "1") {
        picProfile = `/build/images/teams/team_${item.id}_profile.jpg`;
      }
      const detailsLink = `/team/${item.alias}`;
      const countries = item.countryCode.split(",");
      const record = (
        <Grid.Column
          key={item.alias}
          textAlign="center"
          mobile={8}
          tablet={4}
          computer={2}
          className="menu-box-container"
        >
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Segment basic>
                  <NavLink to={detailsLink}>
                    <Image
                      centered
                      src={picProfile}
                      alt={item.name}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/teams/team_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  {item.champ && (
                    <Label attached="bottom" color="red">
                      {item.champ.replace(
                        "Mistrz Świata",
                        formatMessage({ id: "app.stats.wc" })
                      )}
                    </Label>
                  )}
                  <Image
                    className="team-logo"
                    centered
                    src={picProfileLogo}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_logo.jpg";
                    }}
                  />
                </Segment>
                <Segment basic className="menu-box-flex">
                  <Divider hidden fitted></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                      <Header.Subheader>
                        {this.renderCountriesFlags(countries)}
                        {item.country}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <Header.Subheader>{item.years}</Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider hidden fitted></Divider>
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="left" className="info-box-light">
                <Item.Group divided>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.seasons"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink className="link" to={`/team/${item.alias}`}>
                            {item.seasons}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.drivers"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink className="link" to={`/team/${item.alias}`}>
                            {item.drivers}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.races"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/starts/${item.alias}`}
                          >
                            {item.starts}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.qual"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/qual/${item.alias}`}
                          >
                            {item.qual}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.points"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/points/${item.alias}`}
                          >
                            {item.points}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.wins"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/wins/${item.alias}`}
                          >
                            {item.wins}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.podium"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/podium/${item.alias}`}
                          >
                            {item.podium}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.polepos"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/polepos/${item.alias}`}
                          >
                            {item.polepos}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.bestlaps"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/team-events/bestlaps/${item.alias}`}
                          >
                            {item.bestlaps}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                </Item.Group>
                <div className="buttons">
                  <NavLink className="primary" to={detailsLink}>
                    <FormattedMessage id={"app.button.details"} />
                  </NavLink>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      );
      items.push(record);
    });
    return items;
  }

  renderLettersLabels() {
    const items = [];
    const { props } = this;
    const { letters } = props.teamsStats.data;
    letters.forEach((item) => {
      const record = (
        <Label image key={item.letter}>
          <NavLink to={`/teams-stats-list/${item.letter}`}>
            {item.letter}
          </NavLink>
          <Label.Detail>{item.amount}</Label.Detail>
        </Label>
      );
      items.push(record);
    });
    return items;
  }

  renderCountriesLabels() {
    const items = [];
    const { props } = this;
    const { countries } = props.teamsStats.data;
    countries.forEach((item) => {
      const record = (
        <Label image key={item.countryCode}>
          <Flag name={item.countryCode} />
          <NavLink to={`/teams-stats-list/country/${item.countryCode}`}>
            {item.countryShort}
          </NavLink>
          <Label.Detail>{item.amount}</Label.Detail>
        </Label>
      );
      items.push(record);
    });
    return items;
  }

  render() {
    const { props } = this;
    if (!props.teamsStats.data || props.teamsStats.loading) {
      return (
        <section id="teams-stats" name="teams-stats" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }

    return (
      <section id="teams-stats" name="teams-stats" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.teams.stats.title"} />}
          subtitle={<FormattedMessage id={"app.page.teams.stats.subtitle"} />}
        />
        <Segment padded basic className="teams-stats-letters">
          {this.renderLettersLabels()}
          <Label>
            <NavLink to="/teams-stats-list/2025">2025</NavLink>
          </Label>
        </Segment>
        <Segment padded basic className="teams-stats-countries hideMobile">
          {this.renderCountriesLabels()}
        </Segment>
        <div className="section-page-content">
          <Grid>
            {this.renderTeamsStats()}
          </Grid>
        </div>
      </section>
    );
  }
}
TeamsStats.propTypes = {
  fetchTeamsStats: PropTypes.func.isRequired,
  fetchTeamsStatsByCountry: PropTypes.func.isRequired,
  teamsStats: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      letter: PropTypes.string,
    }).isRequired,
  }).isRequired,
  letter: PropTypes.string,
};

function mapStateToProps({ teamsStats }) {
  return { teamsStats };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchTeamsStats, fetchTeamsStatsByCountry },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(TeamsStats)
);
