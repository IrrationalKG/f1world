import { SAVE_CONTEST_REGISTER, SAVE_CONTEST_REGISTER_SUCCESS, SAVE_CONTEST_REGISTER_ERROR } from '../actions/ContestRegisterActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case SAVE_CONTEST_REGISTER:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case SAVE_CONTEST_REGISTER_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case SAVE_CONTEST_REGISTER_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
