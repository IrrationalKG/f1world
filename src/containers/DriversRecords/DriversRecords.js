/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Segment,
  Item,
  Flag,
  Form,
  Statistic,
  Pagination,
  Message,
  Image,
  Header,
  Label,
  Icon,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchDriversRecords } from "../../actions/DriversRecordsActions";
import { fetchComboTeams } from "../../actions/ComboTeamsActions";
import { fetchComboGP } from "../../actions/ComboGPActions";
import { fetchComboCircuits } from "../../actions/ComboCircuitsActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
const cmbStats = require("../../json/combo_grouped_stats.json");
const cmbStatsEn = require("../../json/combo_grouped_stats_en.json");
const cmbStatus = require("../../json/combo_status.json");
const cmbStatusEn = require("../../json/combo_status_en.json");
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriversRecords.less";

class DriversRecords extends Component {
  constructor(props) {
    super(props);

    const comboStats = Cookies.get("lang") == "pl" ? cmbStats : cmbStatsEn;
    const comboStatus = Cookies.get("lang") == "pl" ? cmbStatus : cmbStatusEn;

    let paramStatValue = "starts";
    let showFilters = true;
    let showInputPlace = false;
    if (
      props.match?.params.stat !== undefined &&
      props.match?.params.stat !== ""
    ) {
      paramStatValue = props.match?.params.stat;

      showFilters = this.findObjectById(
        comboStats,
        paramStatValue
      )?.showFilters;

      switch (paramStatValue) {
        case "race-places":
        case "grid-places":
        case "wins-from-place":
        case "podium-from-place":
        case "points-from-place":
        case "completed-from-place":
        case "incomplete-from-place":
        case "finished-from-place":
        case "retirement-from-place":
          showInputPlace = true;
          break;
        default:
          break;
      }
    }

    let paramTeamValue = "-";
    if (
      props.match?.params.teamId !== undefined &&
      props.match?.params.teamId !== ""
    ) {
      paramTeamValue = props.match?.params.teamId;
    }

    let paramGPValue = "-";
    if (
      props.match?.params.gpId !== undefined &&
      props.match?.params.gpId !== ""
    ) {
      paramGPValue = props.match?.params.gpId;
    }

    let paramCircuitValue = "-";
    if (
      props.match?.params.circuitId !== undefined &&
      props.match?.params.circuitId !== ""
    ) {
      paramCircuitValue = props.match?.params.circuitId;
    }

    let paramBeginYearValue = "-";
    if (
      props.match?.params.beginYear !== undefined &&
      props.match?.params.beginYear !== ""
    ) {
      paramBeginYearValue = props.match?.params.beginYear;
    }

    let paramStatusValue = "0";
    if (
      props.match?.params.status !== undefined &&
      props.match?.params.status !== ""
    ) {
      paramStatusValue = props.match?.params.status;
    }

    let paramPlaceValue = "1";
    if (
      props.match?.params.place !== undefined &&
      props.match?.params.place !== ""
    ) {
      paramPlaceValue = props.match?.params.place;
    }

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      shouldShowFilters: showFilters,
      shouldShowInputPlace: showInputPlace,

      paramStatValue: paramStatValue,
      paramTeamValue: paramTeamValue,
      paramGPValue: paramGPValue,
      paramCircuitValue: paramCircuitValue,
      paramBeginYearValue: paramBeginYearValue,
      paramStatusValue: paramStatusValue,
      paramPlaceValue: paramPlaceValue,

      comboStats: comboStats,
      comboStatus: comboStatus,

      selectStatsValue: {
        value: paramStatValue,
        label: this.findObjectById(comboStats, props.match?.params.stat)?.label,
      },
      selectTeamsValue: {
        value: paramTeamValue,
      },
      selectGPValue: {
        value: paramGPValue,
      },
      selectCircuitsValue: {
        value: paramCircuitValue,
      },
      selectPlaceValue: paramPlaceValue,
      selectBeginYearValue: {
        value: paramBeginYearValue,
      },
      selectStatusValue: {
        value: paramStatusValue,
        label: comboStatus.find((e) => e.value == paramStatusValue)?.label,
      },

      activePage: 1,
      boundaryRange: 1,
      siblingRange: 2,
      isSubmited: false,
    };
    this.handleStatsChange = this.handleStatsChange.bind(this);
    this.handlePlaceChange = this.handlePlaceChange.bind(this);
    this.handleTeamsChange = this.handleTeamsChange.bind(this);
    this.handleGPChange = this.handleGPChange.bind(this);
    this.handleCircuitsChange = this.handleCircuitsChange.bind(this);
    this.handleBeginYearChange = this.handleBeginYearChange.bind(this);
    this.handleStatusChange = this.handleStatusChange.bind(this);

    this.resetTeamFilter = this.resetTeamFilter.bind(this);
    this.resetGPFilter = this.resetGPFilter.bind(this);
    this.resetCircuitFilter = this.resetCircuitFilter.bind(this);
    this.resetBeginYearFilter = this.resetBeginYearFilter.bind(this);
    this.resetStatusFilter = this.resetStatusFilter.bind(this);

    this.handlePaginationChange = this.handlePaginationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { props, state } = this;
    props.fetchDriversRecords(
      "1",
      state.selectStatsValue?.value,
      state.selectPlaceValue,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectBeginYearValue?.value,
      "-",
      state.selectStatusValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboTeams(
      1,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    props.fetchComboGP("-", "-", "-", "-", "-", "-", "-", Cookies.get("lang"));
    props.fetchComboCircuits(
      1,
      "-",
      "-",
      state.selectGPValue?.value,
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (state.isSubmited !== prevState.isSubmited && state.isSubmited) {
      this.setState({
        isSubmited: false,
      });
      props.fetchDriversRecords(
        "1",
        state.selectStatsValue?.value,
        state.selectPlaceValue,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectBeginYearValue?.value,
        "-",
        state.selectStatusValue?.value,
        Cookies.get("lang")
      );
    }
    if (prevProps.comboTeams.data !== this.props.comboTeams.data) {
      let param = {
        value: "",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.teamId !== undefined &&
        props.match?.params.teamId !== ""
      ) {
        const paramValue =
          props.match?.params.teamId != "-" ? props.match?.params.teamId : "";
        param = {
          value: paramValue,
          label: this.props.comboTeams.data.find((e) => e.value == paramValue)
            ?.label,
        };
      }
      this.setState({ selectTeamsValue: param });
    }
    if (prevProps.comboGPS.data !== this.props.comboGPS.data) {
      let param = {
        value: "",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.gpId !== undefined &&
        props.match?.params.gpId !== ""
      ) {
        const paramValue =
          props.match?.params.gpId != "-" ? props.match?.params.gpId : "";
        param = {
          value: paramValue,
          label: this.props.comboGPS.data.find((e) => e.value == paramValue)
            ?.label,
        };
      }
      this.setState({
        selectGPValue: param,
      });
      props.fetchComboCircuits(
        1,
        "-",
        "-",
        props.match?.params.gpId,
        "-",
        "-",
        "-",
        "-",
        Cookies.get("lang")
      );
    }
    if (prevProps.comboCircuits.data !== this.props.comboCircuits.data) {
      let param = {
        value: "",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.circuitId !== undefined &&
        props.match?.params.circuitId !== ""
      ) {
        const paramValue =
          props.match?.params.circuitId != "-"
            ? props.match?.params.circuitId
            : "";
        param = {
          value: paramValue,
          label: this.props.comboCircuits.data.find(
            (e) => e.value == paramValue
          )?.label,
        };
      }
      this.setState({
        selectCircuitsValue: param,
      });
    }
    if (prevProps.comboSeasons.data !== this.props.comboSeasons.data) {
      let param = {
        value: "",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.beginYear !== undefined &&
        props.match?.params.beginYear !== ""
      ) {
        const paramValue =
          props.match?.params.beginYear != "-"
            ? props.match?.params.beginYear
            : "";
        param = {
          value: paramValue,
          label: this.props.comboSeasons.data.find((e) => e.value == paramValue)
            ?.label,
        };
      }
      this.setState({
        selectBeginYearValue: param,
      });
    }
  }

  resetPlaceFilter() {
    this.setState({
      paramPlaceValue: 1,
      selectPlaceValue: 1,
    });
    this.handleSubmit();
  }

  resetTeamFilter() {
    const { formatMessage } = this.props.intl;
    this.handleTeamsChange({
      value: "",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetGPFilter() {
    const { formatMessage } = this.props.intl;
    this.handleGPChange({
      value: "",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetCircuitFilter() {
    const { formatMessage } = this.props.intl;
    this.handleCircuitsChange({
      value: "",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetBeginYearFilter() {
    const { formatMessage } = this.props.intl;
    this.handleBeginYearChange({
      value: "",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetStatusFilter() {
    const { formatMessage } = this.props.intl;
    this.handleStatusChange({
      value: "0",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  handlePaginationChange = (e, { activePage }) => {
    const { props, state } = this;
    this.setState({ activePage });
    props.fetchDriversRecords(
      activePage,
      state.selectStatsValue?.value,
      state.selectPlaceValue,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectBeginYearValue?.value,
      "-",
      state.selectStatusValue?.value,
      Cookies.get("lang")
    );
  };

  handleStatsChange(event) {
    let showFilters = true;
    let showInputPlace = false;

    showFilters = this.findObjectById(
      this.state.comboStats,
      event.value
    )?.showFilters;

    switch (event.value) {
      case "race-places":
      case "grid-places":
      case "wins-from-place":
      case "podium-from-place":
      case "points-from-place":
      case "completed-from-place":
      case "incomplete-from-place":
      case "finished-from-place":
      case "retirement-from-place":
        showInputPlace = true;
        break;
      default:
        break;
    }

    if (showFilters == 0) {
      this.resetTeamFilter();
      this.resetGPFilter();
      this.resetCircuitFilter();
      this.resetBeginYearFilter();
      this.resetPlaceFilter();
    }

    this.setState({
      paramStatValue: event.value,
      selectStatsValue: event,
      shouldShowInputPlace: showInputPlace,
      shouldShowFilters: showFilters,
    });
    this.handleSubmit();
  }

  handlePlaceChange(event) {
    let paramValue = "1";
    if (event.target.value !== undefined && event.target.value !== "") {
      paramValue = event.target.value;
    }
    this.setState({
      paramPlaceValue: paramValue,
      selectPlaceValue: paramValue,
    });
    this.handleSubmit();
  }

  handleTeamsChange(event) {
    this.setState({
      paramTeamValue: event.value,
      selectTeamsValue: event,
    });
    this.handleSubmit();
  }

  handleGPChange(event) {
    const { props, state } = this;
    props.fetchComboCircuits(
      1,
      "-",
      "-",
      event.value,
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    this.setState({
      paramGPValue: event.value,
      selectGPValue: event,
    });

    this.handleCircuitsChange({
      value: state.selectCircuitsValue.value,
      label: state.selectCircuitsValue.label,
    });
  }

  handleCircuitsChange(event) {
    this.setState({
      paramCircuitValue: event.value,
      selectCircuitsValue: event,
    });
    this.handleSubmit();
  }

  handleBeginYearChange(event) {
    this.setState({
      paramBeginYearValue: event.value,
      selectBeginYearValue: event,
    });
    this.handleSubmit();
  }

  handleStatusChange(event) {
    this.setState({
      paramStatusValue: event.value,
      selectStatusValue: event,
    });
    this.handleSubmit();
  }

  handleSubmit() {
    this.setState({
      isSubmited: true,
    });
  }

  findObjectById(arr, value) {
    for (const obj of arr) {
      if (obj.value === value) {
        return obj;
      }
      if (obj.options && obj.options.length > 0) {
        const result = this.findObjectById(obj.options, value);
        if (result) {
          return result;
        }
      }
    }
    return null;
  }

  renderRecords() {
    const elements = [];
    const { props, state } = this;
    const { items } = props.driversRecords.data;

    const stat = this.findObjectById(state.comboStats, state.paramStatValue);

    items.forEach((item, idx) => {
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const link = `/driver/${item.alias}`;
      let active = "";
      if (item.active === "1") {
        active = "active-driver";
      }

      if (
        state.selectStatsValue.value == "race-win-lowest-grid" ||
        state.selectStatsValue.value == "race-podium-lowest-grid"
      ) {
        eventsLink = `/driver-events/${
          state.selectStatsValue.value || "starts"
        }/${item.alias}/${state.selectTeamsValue.value || "-"}/${
          state.selectGPValue.value || "-"
        }/${state.selectCircuitsValue.value || "-"}/-/-/-/${
          state.selectBeginYearValue.value || "-"
        }/${item.amount}/`;
      }
      
      let statName = state.selectStatsValue.value;
      switch (state.selectStatsValue.value) {
        case "race-never-qualified":
          statName = "not-qualified";
          break;
        case "race-never-completed":
          statName = "incomplete";
          break;
        case "race-never-finished":
          statName = "retirement";
          break;
        case "race-never-point-scored":
          statName = "points-no-scored-gp-number";
          break;
        case "race-never-polepos":
          statName = "polepos-no-win-gp-number";
          break;
        case "race-never-bestlaps":
          statName = "bestlaps-no-win-gp-number";
          break;
        case "race-never-podium":
          statName = "podium-no-win-gp-number";
          break;
        case "race-never-win":
          statName = "wins-no-win-gp-number";
          break;
        case "points-scored-gp-number":
          statName = "points-places";
          break;
        default:
          break;
      }

      let eventsLink = `/driver-events/${
        statName || "starts"
      }/${item.alias}/${state.selectTeamsValue.value || "-"}/${
        state.selectGPValue.value || "-"
      }/${state.selectCircuitsValue.value || "-"}/-/-/-/${
        state.selectBeginYearValue.value || "-"
      }/${state.selectPlaceValue || "0"}/`;

      const element = (
        <Item id={idx} key={idx}>
          <Segment basic className="image-link" size="small">
            <NavLink to={link}>
              <Image
                centered
                size="small"
                src={filename}
                alt={item.name}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
              {item.active == 1 && (
                <Label
                  color="yellow"
                  attached="bottom"
                  size="tiny"
                  className="center-aligned"
                >
                  <FormattedMessage id={"app.stats.active"} />
                </Label>
              )}
            </NavLink>
          </Segment>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.place}
                {". "}
                <Flag name={item.countryCode} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.country}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                {stat.showLink == 1 ? (
                  <NavLink to={eventsLink}>{item.amount}</NavLink>
                ) : (
                  item.amount
                )}
              </Statistic.Value>
              <Statistic.Label className="no-wrap">
                {stat.shortLabel}
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    if (items.length === 0) {
      const element = (
        <Message key="message-info">
          <Message.Header>
            <FormattedMessage id={"app.message.header"} />
          </Message.Header>
          <p>
            <FormattedMessage id={"app.placeholder.no.results"} />
          </p>
        </Message>
      );
      elements.push(element);
    }
    return elements;
  }

  render() {
    const {
      driversRecords,
      comboTeams,
      comboGPS,
      comboCircuits,
      comboSeasons,
    } = this.props;
    const { state } = this;
    const { formatMessage } = this.props.intl;

    if (!driversRecords.data || driversRecords.loading) {
      return (
        <section id="records" name="records" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const {
      isLoadingExternally,
      multi,
      ignoreCase,
      ignoreAccents,
      clearable,
      comboStatus,
      selectStatsValue,
      selectPlaceValue,
      selectTeamsValue,
      selectGPValue,
      selectCircuitsValue,
      selectBeginYearValue,
      selectStatusValue,
      shouldShowInputPlace,
      shouldShowFilters,
      inputPlaceWidth,
      activePage,
      boundaryRange,
      siblingRange,
    } = this.state;

    let picProfile = "";
    if (driversRecords.data.bestDriverId) {
      const driverAlias = driversRecords.data.bestDriverId;
      picProfile = `/build/images/drivers/driver_${driverAlias}_profile.jpg`;
    }

    const groupStyles = {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      color: "gray",
      fontSize: "0.9rem",
    };
    const formatGroupLabel = (data) => (
      <div style={groupStyles}>
        <span>{data.label}</span>
      </div>
    );

    const stat = this.findObjectById(state.comboStats, state.paramStatValue);

    return (
      <section id="records" name="records" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.drivers.records.title"} />}
          subtitle={
            <FormattedMessage id={"app.page.drivers.records.subtitle"} />
          }
        />
        <Grid stackable centered>
          <Grid.Column
            mobile={16}
            tablet={4}
            computer={3}
            className="left-sidebar"
          >
            {driversRecords.data.bestDriver && (
              <Grid centered className="stats-box">
                <Grid.Column mobile={8} tablet={16} computer={16}>
                  <Image
                    centered
                    src={picProfile}
                    alt={driversRecords.data.bestDriver}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </Grid.Column>
                <Grid.Column
                  mobile={8}
                  tablet={16}
                  computer={16}
                  textAlign="center"
                  verticalAlign="middle"
                >
                  <div>
                    <Statistic size="large" inverted>
                      <Header size="large" inverted>
                        {driversRecords.data.bestDriver}
                      </Header>
                      <Statistic.Value>
                        {driversRecords.data.amount}
                      </Statistic.Value>
                      <Statistic.Label>{stat.shortLabel}</Statistic.Label>
                    </Statistic>
                  </div>
                </Grid.Column>
              </Grid>
            )}
            <div className="section-page-filters records-filters">
              <form onSubmit={this.handleSubmit}>
                <Grid stackable padded>
                  <Grid.Row columns={1}>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="stats">
                          <FormattedMessage id={"app.stats.statistics"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="stats"
                            name="stats"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectStatsValue}
                            onChange={this.handleStatsChange}
                            options={state.comboStats}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.stats"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            formatGroupLabel={formatGroupLabel}
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    {shouldShowFilters && (
                      <>
                        <Grid.Column
                          width={inputPlaceWidth}
                          className={shouldShowInputPlace ? "" : "hide"}
                        >
                          <Segment basic>
                            <Form.Input
                              fluid
                              label={formatMessage({ id: "app.stats.place" })}
                              onChange={this.handlePlaceChange}
                              defaultValue={selectPlaceValue}
                              placeholder={formatMessage({
                                id: "app.stats.place",
                              })}
                            />
                          </Segment>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment basic>
                            <label htmlFor="teams">
                              <FormattedMessage id={"app.stats.team"} />
                              <Select
                                isLoading={isLoadingExternally}
                                id="teams"
                                name="teams"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={selectTeamsValue}
                                onChange={this.handleTeamsChange}
                                options={comboTeams.data}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={
                                  <FormattedMessage
                                    id={"app.placeholder.select.all"}
                                  />
                                }
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container"
                                classNamePrefix="react-select"
                              />
                            </label>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment basic>
                            <label htmlFor="gp">
                              <FormattedMessage id={"app.stats.gp"} />
                              <Select
                                isLoading={isLoadingExternally}
                                id="gp"
                                name="gp"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={selectGPValue}
                                onChange={this.handleGPChange}
                                options={comboGPS.data}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={
                                  <FormattedMessage
                                    id={"app.placeholder.select.all"}
                                  />
                                }
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container"
                                classNamePrefix="react-select"
                              />
                            </label>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment basic>
                            <label htmlFor="circuits">
                              <FormattedMessage id={"app.stats.circuit"} />
                              <Select
                                isLoading={isLoadingExternally}
                                id="circuits"
                                name="circuits"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={selectCircuitsValue}
                                onChange={this.handleCircuitsChange}
                                options={comboCircuits.data}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={
                                  <FormattedMessage
                                    id={"app.placeholder.select.all"}
                                  />
                                }
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container"
                                classNamePrefix="react-select"
                              />
                            </label>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column>
                          <Segment basic>
                            <label htmlFor="beginYear">
                              <FormattedMessage id={"app.stats.season"} />
                              <Select
                                isLoading={isLoadingExternally}
                                id="beginYear"
                                name="beginYear"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={selectBeginYearValue}
                                onChange={this.handleBeginYearChange}
                                options={comboSeasons.data}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={
                                  <FormattedMessage
                                    id={"app.placeholder.select.all"}
                                  />
                                }
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container"
                                classNamePrefix="react-select"
                              />
                            </label>
                          </Segment>
                        </Grid.Column>
                      </>
                    )}
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="status">
                          <FormattedMessage id={"app.stats.status"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="status"
                            name="status"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectStatusValue}
                            onChange={this.handleStatusChange}
                            options={comboStatus}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </form>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={12} computer={13}>
            <div className="section-page-content records">
              <Segment basic>
                <SectionPageHeader title={stat.label} type="secondary" />
                <SectionPageHeader
                  title={`${formatMessage({
                    id: "app.placeholder.records.found",
                  })}: ${driversRecords.data.total}`}
                  className="left-aligned"
                />
                <Segment basic padded>
                  {state.selectTeamsValue.value && (
                    <Label color="yellow">
                      {state.selectTeamsValue.label}
                      <Icon name="delete" onClick={this.resetTeamFilter} />
                    </Label>
                  )}
                  {state.selectGPValue.value && (
                    <Label color="yellow">
                      {state.selectGPValue.label}
                      <Icon name="delete" onClick={this.resetGPFilter} />
                    </Label>
                  )}
                  {state.selectCircuitsValue.value && (
                    <Label color="yellow">
                      {state.selectCircuitsValue.label}
                      <Icon name="delete" onClick={this.resetCircuitFilter} />
                    </Label>
                  )}
                  {state.selectBeginYearValue.value && (
                    <Label color="yellow">
                      {state.selectBeginYearValue.label}
                      <Icon name="delete" onClick={this.resetBeginYearFilter} />
                    </Label>
                  )}
                  {state.selectStatusValue.value == "1" && (
                    <Label color="yellow">
                      {state.selectStatusValue.label}
                      <Icon name="delete" onClick={this.resetStatusFilter} />
                    </Label>
                  )}
                </Segment>
                <Segment padded="very" basic textAlign="center">
                  {driversRecords.data.pages > 1 && (
                    <>
                      <div className="hideForDesktop">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driversRecords.data.pages}
                          ellipsisItem={null}
                          boundaryRange="0"
                          siblingRange="1"
                          size="small"
                        />
                      </div>
                      <div className="hideForMobile">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driversRecords.data.pages}
                          ellipsisItem={null}
                          boundaryRange={boundaryRange}
                          siblingRange={siblingRange}
                          size="small"
                        />
                      </div>
                    </>
                  )}
                </Segment>
                <div className="drivers-records-buttons">
                  <div>
                    <NavLink className="primary" to={`/drivers-records-list`}>
                      <FormattedMessage id={"app.button.back.to.records"} />
                    </NavLink>
                  </div>
                </div>
                <Item.Group divided unstackable>
                  {this.renderRecords()}
                </Item.Group>
                <Segment padded="very" basic textAlign="center">
                  {driversRecords.data.pages > 1 && (
                    <>
                      <div className="hideForDesktop">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driversRecords.data.pages}
                          ellipsisItem={null}
                          boundaryRange="0"
                          siblingRange="1"
                          size="small"
                        />
                      </div>
                      <div className="hideForMobile">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driversRecords.data.pages}
                          ellipsisItem={null}
                          boundaryRange={boundaryRange}
                          siblingRange={siblingRange}
                          size="small"
                        />
                      </div>
                    </>
                  )}
                </Segment>
              </Segment>
            </div>
          </Grid.Column>
        </Grid>
      </section>
    );
  }
}

DriversRecords.propTypes = {
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      teamId: PropTypes.string,
      stat: PropTypes.string,
      gpId: PropTypes.string,
      circuitId: PropTypes.string,
      beginYear: PropTypes.string,
      endYear: PropTypes.string,
      place: PropTypes.string,
    }),
  }),
  fetchDriversRecords: PropTypes.func.isRequired,
  fetchComboTeams: PropTypes.func.isRequired,
  fetchComboGP: PropTypes.func.isRequired,
  fetchComboCircuits: PropTypes.func.isRequired,
  fetchComboSeasons: PropTypes.func.isRequired,
  driversRecords: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  comboTeams: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboGPS: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboCircuits: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
};

function mapStateToProps({
  driversRecords,
  comboTeams,
  comboGPS,
  comboCircuits,
  comboSeasons,
}) {
  return {
    driversRecords,
    comboTeams,
    comboGPS,
    comboCircuits,
    comboSeasons,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchDriversRecords,
      fetchComboTeams,
      fetchComboGP,
      fetchComboCircuits,
      fetchComboSeasons,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(DriversRecords)
);
