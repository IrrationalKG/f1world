import { SAVE_CONTEST_RATE, SAVE_CONTEST_RATE_SUCCESS, SAVE_CONTEST_RATE_ERROR } from '../actions/ContestRateActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case SAVE_CONTEST_RATE:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case SAVE_CONTEST_RATE_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case SAVE_CONTEST_RATE_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
