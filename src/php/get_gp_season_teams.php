<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} 
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

/**
*-------------
* Zespoły
*-------------
**/
// starty
$query="SELECT team, count(items) as starts FROM (
SELECT season, lower(teams.id_team) team, group_concat(distinct race_date) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.season=$year
GROUP BY drivers_gp_results.race_date,teams.id_team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsStartsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsStartsTabM[$tmp_id_team] = $r["starts"];
}
// kwalifikacje
$query="SELECT team, count(items) as qual FROM (
SELECT season, lower(teams.id_team) team, group_concat(distinct qual_date) as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND drivers_pp_results.season=$year
GROUP BY drivers_pp_results.qual_date,teams.id_team
ORDER BY drivers_pp_results.qual_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsQualTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsQualTabM[$tmp_id_team] = $r["qual"];
}
// punkty
$query="SELECT TRUNCATE(SUM(points),2) as race_pts,teams.name,lower(teams.id_team) team
FROM teams_class,teams WHERE teams_class.id_team=teams.id_team AND teams_class.season=$year
GROUP BY teams.id_team";
$resultM = mysqli_query($dbhandle,$query);
$teamsPointsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsPointsTabM[$tmp_id_team] = $r["race_pts"];
}
// zwyciestwa
$query="SELECT team, count(items) as wins FROM (
SELECT season, lower(teams.id_team) team, group_concat(distinct drivers_gp_results.race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.season=$year
AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.race_date,teams.id_team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsWinsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsWinsTabM[$tmp_id_team] = $r["wins"];
}
// podium
$query="SELECT team, count(items) as podium FROM (
SELECT season, lower(teams.id_team) team, group_concat(distinct drivers_gp_results.race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.season=$year
AND drivers_gp_results.race_pos<4
GROUP BY drivers_gp_results.race_date,teams.id_team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsPodiumsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $id_team = $r["id_team"];
  $teamsPodiumsTabM[$tmp_id_team] = $r["podium"];
}
// pole position
$query="SELECT team, count(items) as pp FROM (
SELECT id_gp, lower(teams.id_team) team, group_concat(distinct drivers_gp_starting_grid.id_starting_grid) as items
FROM drivers_gp_starting_grid,teams 
WHERE drivers_gp_starting_grid.id_team=teams.id_team AND drivers_gp_starting_grid.season=$year
AND drivers_gp_starting_grid.is_pp=1
GROUP BY drivers_gp_starting_grid.id_gp,teams.id_team,drivers_gp_starting_grid.grid_pos
ORDER BY drivers_gp_starting_grid.id_gp ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsPPTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsPPTabM[$tmp_id_team] = $r["pp"];
}
// best laps
$query="SELECT team, count(items) as bestlaps FROM (
SELECT season, lower(teams.id_team) team, group_concat(distinct drivers_gp_results.race_best_lap) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.season=$year
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.race_date,teams.id_team,drivers_gp_results.race_best_lap
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_best_lap ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsBLTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsBLTabM[$tmp_id_team] = $r["bestlaps"];
}
// champs
$query="SELECT count(teams_class.team)as champs,lower(teams.id_team) team,teams.name,teams.country_short 
from teams_class,teams 
where teams.id_team=teams_class.id_team 
and place=1 
and season>1957 
and season=$year 
group by teams_class.id_team,teams.name,teams.country_short order by champs desc,name";
$resultM = mysqli_query($dbhandle,$query);
$teamsWCTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsWCTabM[$tmp_id_team] = $r["champs"];
}

$teamsItems = array();
$query="SELECT
teams.alias_name aliasName,
teams.alias,
LOWER(teams.team) id,
'teams' type,
teams.name,
teams.engine,
teams.country_short,
teams.country_code country,
teams.photo,
teams.picture,
teams.color,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') modelName,
drivers_gp_involvements.id_team idTeam,
coalesce(teams_class.place,1000) seasonPlace,
teams_class.is_classified isClassified,
'' wins,
'' points,
'' podium,
'' starts,
'' qual,
'' polePosition,
'' bestLaps,
'' bestResult
FROM drivers_gp_involvements
LEFT JOIN teams_class ON  drivers_gp_involvements.id_team=teams_class.id_team AND drivers_gp_involvements.season=teams_class.season
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model AND drivers_gp_involvements.season=teams_class.season
WHERE drivers_gp_involvements.season = '$year'
GROUP BY drivers_gp_involvements.id_team
ORDER BY
seasonPlace, teams.name";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $r["starts"]=(int)$teamsStartsTabM[$r["idTeam"]];
   $r["qual"]=(int)$teamsQualTabM[$r["idTeam"]];
   $r["points"]=(double)$teamsPointsTabM[$r["idTeam"]];
   $r["wins"]=(int)$teamsWinsTabM[$r["idTeam"]];
   $r["podium"]=(int)$teamsPodiumsTabM[$r["idTeam"]];
   $r["polePosition"]=(int)$teamsPPTabM[$r["idTeam"]];
   $r["bestLaps"]=(int)$teamsBLTabM[$r["idTeam"]];

   if (!empty($teamsWCTabM[$r["idTeam"]])&&$year!='2025'){
    $r["champ"]="MŚ";
   }

   $teamsItems[] = $r;
}

$seasonTeams["teams"]=$teamsItems;

// Response
$response = $seasonTeams;

print json_encode($response);
mysqli_free_result($result);
?>
