/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import ReactHighcharts from "react-highcharts";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Header,
  Segment,
  Item,
  Flag,
  Icon,
  Table,
  Statistic,
  Message,
  Divider,
  Label,
  Popup,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import ChartStatsBar from "../../components/ChartStatsBar/ChartStatsBar";
import ChartStatsBarRace from "../../components/ChartStatsBarRace/ChartStatsBarRace";
import { fetchGPSeason } from "../../actions/GPSeasonActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPSeason.less";

class GPSeason extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPSeason(props.match.params.year, Cookies.get("lang"));
    props.fetchComboSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboSeasons.data?.find((e) => e.value == nextYear)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchGPSeason(nextYear, Cookies.get("lang"));
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/gp-season/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderDriversClass() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { drivers } = props.gpseason.data.classification;
    drivers.forEach((item) => {
      const filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      const link = `/driver/${item.driverAlias}`;
      const element = (
        <Item key={item.idDriver}>
          <NavLink to={link}>
            <Image
              size="small"
              src={filename}
              className="cell-photo"
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.place}
                {". "}
                <Flag name={item.driverCountryCode} />
                {item.driver}
              </NavLink>
            </Item.Header>
            <Item.Description>
              {item.team?.split(", ").map((teamName) => {
                return <div key={teamName}>{teamName}</div>;
              })}
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value className="no-wrap">
                {item.points === "0" && item.points}
                {item.points > 0 && (
                  <NavLink
                    to={`/driver-events/points/${item.driverAlias}/-/-/-/-/-/-/${year}/-`}
                  >
                    {item.pointsClass}
                    <small className="very">
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </NavLink>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderDriversClassMobile() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { drivers } = props.gpseason.data.classification;
    drivers.forEach((item) => {
      const filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      const link = `/driver/${item.driverAlias}`;
      const element = (
        <Grid.Row key={item.idDriver}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link}>
                <Image
                  size="tiny"
                  src={filename}
                  onError={(e) => {
                    e.target.src = "/build/images/driversdriver_no_profile.jpg";
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11} verticalAlign="middle">
            <Item.Group>
              <Item>
                <Item.Content>
                  <Item.Header>
                    <NavLink to={link}>
                      {item.place}
                      {". "}
                      <Flag name={item.driverCountryCode} />
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.team?.split(", ").map((teamName) => {
                      return <div key={teamName}>{teamName}</div>;
                    })}
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2} verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value className="no-wrap">
                {item.points === "0" && item.points}
                {item.points > 0 && (
                  <NavLink
                    to={`/driver-events/points/${item.driverAlias}/-/-/-/-/-/-/${year}/-`}
                  >
                    {item.pointsClass}
                    <small className="very">
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </NavLink>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsClass() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { teams } = props.gpseason.data.classification;
    teams.forEach((item) => {
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;
      }
      const link = `/team/${item.teamAlias}`;
      const element = (
        <Item key={item.idTeam}>
          <NavLink to={link}>
            <Image
              size="small"
              src={filename}
              className="cell-photo"
              onError={(e) => {
                e.target.src = "/build/images/teams/team_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.place}
                {". "}
                <Flag name={item.teamCountryCode} />
                {item.team} {item.engine}
              </NavLink>
            </Item.Header>
            <Item.Description>
              {item.modelName?.split(", ").map((teamName) => {
                return <div key={teamName}>{teamName}</div>;
              })}
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value className="no-wrap">
                {item.points === "0" && item.points}
                {item.points > 0 && (
                  <NavLink
                    to={`/team-events/points/${item.teamAlias}/-/-/-/-/-/-/${year}/-`}
                  >
                    {item.pointsClass}
                    <small className="very">
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </NavLink>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsClassMobile() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { teams } = props.gpseason.data.classification;
    teams.forEach((item) => {
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.idTeam}_profile_logo.jpg`;
      }
      const link = `/team/${item.teamAlias}`;
      const element = (
        <Grid.Row key={item.idTeam}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link}>
                <Image
                  size="tiny"
                  src={filename}
                  onError={(e) => {
                    e.target.src = "/build/images/teams/team_no_profile.jpg";
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11} verticalAlign="middle">
            <Item.Group>
              <Item>
                <Item.Content>
                  <Item.Header>
                    <NavLink to={link}>
                      {item.place}
                      {". "}
                      <Flag name={item.teamCountryCode} />
                      {item.team} {item.engine}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.modelName?.split(", ").map((teamName) => {
                      return <div key={teamName}>{teamName}</div>;
                    })}
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2} verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value className="no-wrap">
                {item.points === "0" && item.points}
                {item.points > 0 && (
                  <NavLink
                    to={`/team-events/points/${item.teamAlias}/-/-/-/-/-/-/${year}/-`}
                  >
                    {item.pointsClass}
                    <small className="very">
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </NavLink>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderGrandPrixResult() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { gpResults } = props.gpseason.data;
    let currentDate = "";
    let idx = 0;
    let pos = "-";
    gpResults.forEach((item) => {
      if (currentDate !== item.date) {
        currentDate = item.date;
        idx += 1;
        pos = `${idx}.`;
      } else {
        pos = "-";
      }
      const circuitPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const driverPic = `/build/images/drivers/driver_${
        item.winnerId.split(", ")[0]
      }_profile.jpg`;
      const teamPic = `/build/images/teams/team_${item.winnerTeam.toLowerCase()}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.sort}>
          <Table.Cell data-title="#">{pos}</Table.Cell>
          <Table.Cell data-title="Data" className="no-wrap left">
            <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
              {item.date}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
                  <Image
                    size="tiny"
                    src={circuitPic}
                    alt={item.circuitAlias}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
                    {item.gp}{" "}
                  </NavLink>
                </div>
                <div>
                  <small>
                    <NavLink to={`/circuit/${item.circuitAlias}`}>
                      {item.circuit}
                    </NavLink>
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zwycięzca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.winnerAlias.split(", ")[0]}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.winner.split(", ")[0]}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  {item.winner?.split(", ").map((winner, index) => {
                    return (
                      <span key={index}>
                        {index != 0 && ", "}
                        <Flag
                          name={item.winnerCountryCode?.split(", ")[index]}
                        />
                        <NavLink
                          to={`/driver/${item.winnerAlias?.split(", ")[index]}`}
                        >
                          {winner}
                        </NavLink>
                      </span>
                    );
                  })}
                </div>
                <div>
                  <small>
                    <FormattedMessage id={"app.table.header.polepos"} />:{" "}
                    <NavLink to={`/driver/${item.ppAlias}`}>{item.pp}</NavLink>
                  </small>
                </div>
                {item.info.includes("jazda wspólna") && (
                  <small className="gpseason-add-info">*{item.info}</small>
                )}
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.winnerTeamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.winnerTeamName}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/team/${item.winnerTeamAlias}`}>
                    <Flag name={item.winnerTeamCountryCode} />{" "}
                    {item.winnerTeamName}
                  </NavLink>
                </div>
                <div>
                  <small>{item.winnerTeamModelName}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Czas" className="left">
            {item.time}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderGrandPrixResultMobile() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { gpResults } = props.gpseason.data;
    let currentDate = "";
    let idx = 0;
    let pos = "-";
    gpResults.forEach((item) => {
      if (currentDate !== item.date) {
        currentDate = item.date;
        idx += 1;
        pos = `${idx}.`;
      } else {
        pos = "-";
      }
      const circuitPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Grid.Row key={item.sort}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
                <Image
                  size="tiny"
                  src={circuitPic}
                  alt={item.circuitAlias}
                  className="item-photo"
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {pos}{" "}
                    <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
                      {item.gp}
                    </NavLink>
                    {" ("}
                    <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
                      {item.date}
                    </NavLink>
                    {") "}
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/circuit/${item.circuitAlias}`}>
                      {item.circuit}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    <NavLink to={`/driver/${item.winnerAlias}`}>
                      {item.winner}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    <NavLink to={`/team/${item.winnerTeamAlias}`}>
                      {item.winnerTeamName}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    {item.time}
                    {item.info.includes("jazda wspólna") && (
                      <div>
                        <small>*{item.info}</small>
                      </div>
                    )}
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderTeamModels() {
    const elements = [];
    const { props } = this;
    const { models } = props.gpseason.data;

    models.forEach((item) => {
      const picCar = `/build/images/teams/models/team_${item.team.toLowerCase()}_${item.model
        .toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const element = (
        <Grid.Column
          key={item.id_team_model}
          textAlign="center"
          mobile={16}
          tablet={8}
          computer={8}
        >
          <Segment padded basic>
            <Segment basic>
              <Popup
                content={
                  item.teamName ? (
                    item.teamName.split(", ").map((t) => {
                      return <div key={t}>{t}</div>;
                    })
                  ) : (
                    <FormattedMessage id={"app.stats.private"} />
                  )
                }
                key={item.name}
                position="bottom center"
                wide="very"
                trigger={
                  <NavLink
                    to={`/team-model-events/starts/${item.id_team_model}/${item.season}/-`}
                  >
                    <Image
                      centered
                      src={picCar}
                      alt={`${item.name} ${
                        item.engine
                      } ${item.model?.replaceAll("???", "")} ${item.season}`}
                      onError={(e) => {
                        e.target.src = "/build/images/teams/team_no_car.jpg";
                      }}
                    />
                    <div className="gpseason-models-box-model">
                      {item.name} {item.engine ? item.engine : ""}{" "}
                      {item.model
                        ? item.model.replace("_", "/").replaceAll("?", "")
                        : ""}
                    </div>
                  </NavLink>
                }
              />
              <Label attached="top left" color="red">
                &nbsp;{item.name}&nbsp;
              </Label>
            </Segment>
          </Segment>
        </Grid.Column>
      );
      elements.push(element);
    });

    return elements;
  }

  renderGrandPrix() {
    const items = [];
    const { props } = this;
    const { year } = props.match.params;
    const { gpResults } = props.gpseason.data;
    let currentDate = "";
    let idx = 0;
    let pos = "-";
    gpResults.forEach((item) => {
      if (currentDate !== item.date) {
        currentDate = item.date;
        idx += 1;
        pos = `${idx}.`;
      } else {
        pos = "-";
      }
      const element = (
        <div key={pos} className="gpseason-grand-prix-flag">
          {pos}
          <NavLink to={`/gp-result/${item.gpAlias}/${year}`}>
            <Image
              size="mini"
              src={`/build/images/countries/${item.nameShort.toLowerCase()}.jpg`}
              onError={(e) => {
                e.target.src = "/build/images/countries/country_no_profile.jpg";
              }}
            />
          </NavLink>
          {item.nameShort}
          <br />
          <small>
            {item.date.substring(8, 10)}
            {"."}
            {item.date.substring(5, 7)}
          </small>
        </div>
      );
      items.push(element);
    });
    return items;
  }

  renderGPInfo() {
    const { props } = this;

    const gpItems = [];
    const { year } = props.match.params;
    const { gpResults } = props.gpseason.data;
    gpResults.forEach((item) => {
      const {
        sort,
        gp,
        gpCountryCode,
        circuit,
        circuitAlias,
        gpAlias,
        date,
        winner,
        photo,
      } = item;
      let filename = "/build/images/circuits/circuit_no_profile.jpg";
      if (photo == "") {
        filename = "/build/images/circuits/circuit_no_profile.jpg";
      } else {
        filename = `/build/images/circuits/circuit_${circuitAlias}_profile.jpg`;
      }
      const detailsLink = `/gp-result/${gpAlias}/${year}`;
      const gpItem = (
        <div key={sort} className="box">
          <div className="box-extra">
            <NavLink to={`/gp-result/${gpAlias}/${year}`}>
              {sort}
              {". "}
              {gp}
            </NavLink>
          </div>
          <div className={`box-image`}>
            <NavLink to={detailsLink}>
              <Image src={filename} />
            </NavLink>
          </div>
          <div className="box-content">
            <div className="box-header">
              <Flag name={gpCountryCode} />
              <NavLink to={`/gp-result/${gpAlias}/${year}`}>{circuit}</NavLink>
            </div>
            <div className="box-description">{date}</div>
            <div className="box-info">
              <Icon name="trophy" /> {winner}
            </div>
          </div>
        </div>
      );
      gpItems.push(gpItem);
    });
    return gpItems;
  }

  getRandomColor = () => {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpseason.data || props.gpseason.loading) {
      return (
        <section
          id="gpseason-details"
          name="gpseason-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { year } = props.match.params;
    const {
      season,
      prevSeason,
      nextSeason,
      classification,
      driversStats,
      teamsStats,
      gpResults,
      regulations,
      driversSeasonWins,
      driversSeasonPoints,
      driversSeasonPodium,
      driversSeasonPolepos,
      driversSeasonBestlaps,
      teamsSeasonWins,
      teamsSeasonPoints,
      teamsSeasonPodium,
      teamsSeasonPolepos,
      teamsSeasonBestlaps,
      progres,
    } = props.gpseason.data;

    const linkPrev = `/gp-season/${prevSeason}`;
    const linkNext = `/gp-season/${nextSeason}`;
    if (gpResults.length === 0) {
      return (
        <section
          id="gpseason-details"
          name="gpseason-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.gpseason.title"}
                values={{ season: season }}
              />
            }
            subtitle={<FormattedMessage id={"app.page.gpseason.subtitle"} />}
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage
                  id={"app.page.gpseason.info"}
                  values={{ season: season }}
                />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    const winnerPic = `/build/images/drivers/driver_${classification.drivers[0].idDriver}_profile.jpg`;
    const secondPic = `/build/images/drivers/driver_${classification.drivers[1].idDriver}_profile.jpg`;
    const thirdPic = `/build/images/drivers/driver_${classification.drivers[2].idDriver}_profile.jpg`;

    const colors = ReactHighcharts.Highcharts.getOptions().colors.map((c, i) =>
      // Start out with a darkened base color (negative brighten), and end
      // up with a much brighter color
      ReactHighcharts.Highcharts.Color("#ffdd60")
        .brighten((i - 3) / 20)
        .get()
    );

    const driversSeasonWinsData = [];
    driversSeasonWins.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return driversSeasonWinsData.push(value);
    });

    const configStatsSeasonWinsPercent = {
      chart: {
        type: "pie",
        height: "250px",
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
      },
      title: {
        text: "",
      },
      tooltip: {
        valueSuffix: "",
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          size: "100%",
          allowPointSelect: true,
          cursor: "pointer",
          colors,
          borderRadius: 5,
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b><br>{point.percentage:.1f} %",
            distance: -50,
            filter: {
              property: "percentage",
              operator: ">",
              value: 4,
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.wins" }),
          colorByPoint: true,
          data: driversSeasonWinsData,
        },
      ],
    };

    const driversSeasonPodiumData = [];
    driversSeasonPodium.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return driversSeasonPodiumData.push(value);
    });

    const configStatsSeasonPodiumPercent = {
      chart: {
        type: "pie",
        height: "250px",
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
      },
      title: {
        text: "",
      },
      tooltip: {
        valueSuffix: "",
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          size: "100%",
          allowPointSelect: true,
          cursor: "pointer",
          colors,
          borderRadius: 5,
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b><br>{point.percentage:.1f} %",
            distance: -50,
            filter: {
              property: "percentage",
              operator: ">",
              value: 4,
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.podium" }),
          colorByPoint: true,
          data: driversSeasonPodiumData,
        },
      ],
    };

    const driversSeasonPoleposData = [];
    driversSeasonPolepos.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return driversSeasonPoleposData.push(value);
    });

    const configStatsSeasonPoleposPercent = {
      chart: {
        type: "pie",
        height: "250px",
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
      },
      title: {
        text: "",
      },
      tooltip: {
        valueSuffix: "",
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          size: "100%",
          allowPointSelect: true,
          cursor: "pointer",
          colors,
          borderRadius: 5,
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b><br>{point.percentage:.1f} %",
            distance: -50,
            filter: {
              property: "percentage",
              operator: ">",
              value: 4,
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.polepos" }),
          colorByPoint: true,
          data: driversSeasonPoleposData,
        },
      ],
    };

    const driversSeasonBestlapsData = [];
    driversSeasonBestlaps.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return driversSeasonBestlapsData.push(value);
    });

    const configStatsSeasonBestlapsPercent = {
      chart: {
        type: "pie",
        height: "250px",
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
      },
      title: {
        text: "",
      },
      tooltip: {
        valueSuffix: "",
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          size: "100%",
          allowPointSelect: true,
          cursor: "pointer",
          colors,
          borderRadius: 5,
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b><br>{point.percentage:.1f} %",
            distance: -50,
            filter: {
              property: "percentage",
              operator: ">",
              value: 4,
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.bestlaps" }),
          colorByPoint: true,
          data: driversSeasonBestlapsData,
        },
      ],
    };

    let progresData = [];
    const drivers = progres.map((e) => e.name);
    const uniqueDrivers = [...new Set(drivers)];

    const uniqueColors = [];
    uniqueDrivers.map((driverName) => {
      const driverGP = progres.filter((e) => e.name == driverName);

      const data = [];
      driverGP.map((e) => {
        const dataItem = { gp: "", points: "" };
        dataItem.gp = e.gp;
        dataItem.points = parseInt(e.points, 10);
        return data.push(dataItem);
      });

      const value = { name: "", data: "" };
      value.name = driverName;
      value.data = data;

      const color = this.getRandomColor();
      while (!uniqueColors.includes(color)) {
        value.color = color;
        uniqueColors.push(color);
      }

      return progresData.push(value);
    });

    // const configAllDriversProgress = {
    //   chart: {
    //     type: "line",
    //     height: 850,
    //   },
    //   title: {
    //     text: "",
    //   },
    //   subtitle: {
    //     text: " ",
    //   },
    //   xAxis: {
    //     categories: categories,
    //   },
    //   yAxis: {
    //     min: 1,
    //     max: 20,
    //     allowDecimals: false,
    //     tickInterval: 1,
    //     reversed: true,
    //     title: {
    //       text: "",
    //     },
    //   },
    //   legend: {
    //     enabled: true,
    //   },
    //   credits: {
    //     enabled: false,
    //   },
    //   tooltip: {
    //     headerFormat:
    //       '<span style="font-size:10px">Miejsce w GP {point.key}</span><table>',
    //     pointFormat:
    //       '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //       '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
    //     footerFormat: "</table>",
    //     shared: false,
    //     useHTML: true,
    //   },
    //   plotOptions: {
    //     area: {
    //       fillOpacity: 0.5,
    //     },
    //     series: {
    //       borderWidth: 0,
    //       label: {
    //         connectorAllowed: false,
    //       },
    //     },
    //   },
    //   series: progresData,
    // };

    return (
      <section
        id="gpseason-details"
        name="gpseason-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.gpseason.title"}
              values={{ season: season }}
            />
          }
          subtitle={<FormattedMessage id={"app.page.gpseason.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <Segment basic padded>
                  <Select
                    ref={(ref) => {
                      this.comboSeason = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="seasons"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectValue}
                    onChange={this.onInputChange}
                    options={props.comboSeasons.data}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={formatMessage({
                      id: "app.placeholder.select.season",
                    })}
                    noResultsText={
                      <FormattedMessage id={"app.placeholder.no.results"} />
                    }
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </Segment>
                <div className="gpseason-podium">
                  <div>
                    <SectionPageHeader
                      title={<FormattedMessage id={"app.stats.podium"} />}
                      type="tertiary"
                    />
                    <Grid centered className="info-box">
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <NavLink
                          to={`/driver/${classification.drivers[0].driverAlias}`}
                        >
                          <Image
                            centered
                            src={winnerPic}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </Grid.Column>
                      <Grid.Column
                        mobile={8}
                        tablet={16}
                        computer={16}
                        className="info-box"
                      >
                        <Segment padded basic textAlign="center">
                          <Header size="huge" inverted>
                            1
                          </Header>
                          <Header size="large" inverted>
                            <NavLink
                              to={`/driver/${classification.drivers[0].driverAlias}`}
                            >
                              <Flag
                                name={
                                  classification.drivers[0].driverCountryCode
                                }
                              />
                              {classification.drivers[0].driver}
                            </NavLink>
                            <Header.Subheader>
                              {classification.drivers[0].team}
                            </Header.Subheader>
                            <p>{classification.drivers[0].time}</p>
                          </Header>
                          <Statistic size="large" inverted>
                            <Statistic.Value>
                              <NavLink
                                to={`/driver-events/points/${classification.drivers[0].driverAlias}/-/-/-/-/-/-/${year}/-`}
                              >
                                {classification.drivers[0].pointsClass}
                                {classification.drivers[0].pointsClass !==
                                  classification.drivers[0].points && (
                                  <small className="very">
                                    ({classification.drivers[0].points})
                                  </small>
                                )}
                              </NavLink>
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.pts"} />
                            </Statistic.Label>
                          </Statistic>
                        </Segment>
                      </Grid.Column>
                    </Grid>
                    <Divider hidden fitted></Divider>
                    <Grid centered>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <NavLink
                          to={`/driver/${classification.drivers[1].driverAlias}`}
                        >
                          <Image
                            centered
                            src={secondPic}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </Grid.Column>
                      <Grid.Column
                        mobile={8}
                        tablet={16}
                        computer={16}
                        className="info-box"
                      >
                        <Segment padded basic textAlign="center">
                          <Header size="huge" inverted>
                            2
                          </Header>
                          <Header size="large" inverted>
                            <NavLink
                              to={`/driver/${classification.drivers[1].driverAlias}`}
                            >
                              <Flag
                                name={
                                  classification.drivers[1].driverCountryCode
                                }
                              />
                              {classification.drivers[1].driver}
                            </NavLink>
                            <Header.Subheader>
                              {classification.drivers[1].team}
                            </Header.Subheader>
                            <p>{classification.drivers[1].time}</p>
                          </Header>
                          <Statistic size="large" inverted>
                            <Statistic.Value>
                              <NavLink
                                to={`/driver-events/points/${classification.drivers[1].driverAlias}/-/-/-/-/-/-/${year}/-`}
                              >
                                {classification.drivers[1].pointsClass}
                                {classification.drivers[1].pointsClass !==
                                  classification.drivers[1].points && (
                                  <small className="very">
                                    ({classification.drivers[1].points})
                                  </small>
                                )}
                              </NavLink>
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.pts"} />
                            </Statistic.Label>
                          </Statistic>
                        </Segment>
                      </Grid.Column>
                    </Grid>
                    <Divider hidden fitted></Divider>
                    <Grid centered>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <NavLink
                          to={`/driver/${classification.drivers[2].driverAlias}`}
                        >
                          <Image
                            centered
                            src={thirdPic}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </Grid.Column>
                      <Grid.Column
                        mobile={8}
                        tablet={16}
                        computer={16}
                        className="info-box"
                      >
                        <Segment basic textAlign="center">
                          <Header size="huge" inverted>
                            3
                          </Header>
                          <Header size="large" inverted>
                            <NavLink
                              to={`/driver/${classification.drivers[2].driverAlias}`}
                            >
                              <Flag
                                name={
                                  classification.drivers[2].driverCountryCode
                                }
                              />
                              {classification.drivers[2].driver}
                            </NavLink>
                            <Header.Subheader>
                              {classification.drivers[2].team}
                            </Header.Subheader>
                            <p>{classification.drivers[2].time}</p>
                          </Header>
                          <Statistic size="large" inverted>
                            <Statistic.Value>
                              <NavLink
                                to={`/driver-events/points/${classification.drivers[2].driverAlias}/-/-/-/-/-/-/${year}/-`}
                              >
                                {classification.drivers[2].pointsClass}
                                {classification.drivers[2].pointsClass !==
                                  classification.drivers[2].points && (
                                  <small className="very">
                                    ({classification.drivers[2].points})
                                  </small>
                                )}
                              </NavLink>
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.pts"} />
                            </Statistic.Label>
                          </Statistic>
                        </Segment>
                      </Grid.Column>
                    </Grid>
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered>
                  <Grid.Column mobile={16} tablet={11} computer={12}>
                    <Segment basic padded>
                      <div className="buttons">
                        <NavLink to={`/gp-season/${year}`} className="primary">
                          <FormattedMessage id={"app.button.summary"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/points/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.points"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/places/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.places"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/qual/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.grid"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/summary/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.statistics"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/drivers/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.drivers"} />
                        </NavLink>
                        <NavLink
                          to={`/gp-season/teams/${year}`}
                          className="secondary"
                        >
                          <FormattedMessage id={"app.button.teams"} />
                        </NavLink>
                      </div>
                      <section id="gpseason-calendar">
                        <div className="gpseason-calendar-header">
                          <h2>
                            <FormattedMessage id={"app.stats.gp"} />
                          </h2>
                          <h3>
                            <FormattedMessage id={"app.stats.season"} /> {year}
                          </h3>
                        </div>
                        <div className="gpseason-calendar-content">
                          <div className="box-container">
                            {this.renderGPInfo()}
                          </div>
                        </div>
                      </section>
                      <div className="hideForMobile">
                        <div className="gpseason-stats">
                          <Divider hidden fitted></Divider>
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.gpseason.stats.title"}
                              />
                            }
                            type="secondary"
                          />
                          <Segment basic textAlign="center">
                            <Grid stackable>
                              <Grid.Row className="center-aligned">
                                <Grid.Column
                                  mobile={16}
                                  tablet={8}
                                  computer={4}
                                >
                                  <SectionPageHeader
                                    title={
                                      <FormattedMessage id={"app.stats.wins"} />
                                    }
                                    type="quaternary"
                                  />
                                  <ReactHighcharts
                                    config={configStatsSeasonWinsPercent}
                                  />
                                  <div className="buttons">
                                    <NavLink
                                      to={`/drivers-records/wins/-/-/-/${year}/-/0/1/`}
                                      className="primary"
                                    >
                                      <FormattedMessage
                                        id={"app.button.more"}
                                      />
                                    </NavLink>
                                  </div>
                                </Grid.Column>
                                <Grid.Column
                                  mobile={16}
                                  tablet={8}
                                  computer={4}
                                >
                                  <SectionPageHeader
                                    title={
                                      <FormattedMessage
                                        id={"app.stats.podium"}
                                      />
                                    }
                                    type="quaternary"
                                  />
                                  <ReactHighcharts
                                    config={configStatsSeasonPodiumPercent}
                                  />
                                  <div className="buttons">
                                    <NavLink
                                      to={`/drivers-records/podium/-/-/-/${year}/-/0/1/`}
                                      className="primary"
                                    >
                                      <FormattedMessage
                                        id={"app.button.more"}
                                      />
                                    </NavLink>
                                  </div>
                                </Grid.Column>
                                <Grid.Column
                                  mobile={16}
                                  tablet={8}
                                  computer={4}
                                >
                                  <SectionPageHeader
                                    title={
                                      <FormattedMessage
                                        id={"app.stats.polepos"}
                                      />
                                    }
                                    type="quaternary"
                                  />
                                  <ReactHighcharts
                                    config={configStatsSeasonPoleposPercent}
                                  />
                                  <div className="buttons">
                                    <NavLink
                                      to={`/drivers-records/polepos/-/-/-/${year}/-/0/1/`}
                                      className="primary"
                                    >
                                      <FormattedMessage
                                        id={"app.button.more"}
                                      />
                                    </NavLink>
                                  </div>
                                </Grid.Column>
                                <Grid.Column
                                  mobile={16}
                                  tablet={8}
                                  computer={4}
                                >
                                  <SectionPageHeader
                                    title={
                                      <FormattedMessage
                                        id={"app.stats.bestlaps"}
                                      />
                                    }
                                    type="quaternary"
                                  />
                                  <ReactHighcharts
                                    config={configStatsSeasonBestlapsPercent}
                                  />
                                  <div className="buttons">
                                    <NavLink
                                      to={`/drivers-records/bestlaps/-/-/-/${year}/-/0/1/`}
                                      className="primary"
                                    >
                                      <FormattedMessage
                                        id={"app.button.more"}
                                      />
                                    </NavLink>
                                  </div>
                                </Grid.Column>
                              </Grid.Row>
                            </Grid>
                          </Segment>
                        </div>
                      </div>
                      <div className="season-class-chart-header">
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.gpseason.stats.progress"}
                            />
                          }
                          type="secondary"
                        />
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.gpseason.stats.progress.info"}
                            />
                          }
                          type="quaternary"
                        />
                        <ChartStatsBarRace
                          values={progresData}
                          seriesName={formatMessage({
                            id: "app.stats.wins",
                          })}
                        />
                      </div>
                      <div className="gpseason-race">
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.gpseason.stats.race"}
                            />
                          }
                          type="secondary"
                        />
                        <div className="hideForDesktop">
                          <Segment basic>
                            <Grid columns={3} divided="vertically">
                              {this.renderGrandPrixResultMobile()}
                            </Grid>
                          </Segment>
                        </div>
                        <div className="hideForMobile">
                          <Segment basic className="overflow">
                            <Table basic="very" celled>
                              <Table.Header>
                                <Table.Row>
                                  <Table.HeaderCell>#</Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.date"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.gp.long"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.winner"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.team"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.time"}
                                    />
                                  </Table.HeaderCell>
                                </Table.Row>
                              </Table.Header>
                              <Table.Body>
                                {this.renderGrandPrixResult()}
                              </Table.Body>
                            </Table>
                          </Segment>
                        </div>
                        <Regulations regulations={regulations}></Regulations>
                      </div>
                      <div className="hideForMobile">
                        <div className="gpseason-models">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.gpseason.stats.models"}
                              />
                            }
                            type="secondary"
                          />
                          <Grid stackable>{this.renderTeamModels()}</Grid>
                        </div>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={5} computer={4}>
                    <div className="gpseason-points">
                      <Grid stackable centered columns="equal" padded>
                        <Grid.Row columns={1}>
                          <Grid.Column className="drivers-container">
                            <Segment basic className="drivers-list">
                              <SectionPageHeader
                                title={
                                  <FormattedMessage id={"app.stats.drivers"} />
                                }
                              />
                              <div className="hideForDesktop info-box">
                                <Segment basic>
                                  <Grid columns={3} divided="vertically">
                                    {this.renderDriversClassMobile()}
                                  </Grid>
                                </Segment>
                              </div>
                              <div className="hideForMobile info-box">
                                <Item.Group divided unstackable>
                                  {this.renderDriversClass()}
                                </Item.Group>
                              </div>
                            </Segment>
                          </Grid.Column>
                          <Grid.Column className="teams-container">
                            <Segment basic className="teams-list">
                              <SectionPageHeader
                                title={
                                  <FormattedMessage id={"app.stats.teams"} />
                                }
                              />
                              {year >= 1958 && (
                                <>
                                  <div className="hideForDesktop info-box">
                                    <Segment basic>
                                      <Grid columns={3} divided="vertically">
                                        {this.renderTeamsClassMobile()}
                                      </Grid>
                                    </Segment>
                                  </div>
                                  <div className="hideForMobile info-box">
                                    <Item.Group divided unstackable>
                                      {this.renderTeamsClass()}
                                    </Item.Group>
                                  </div>
                                </>
                              )}
                              {year < 1958 && (
                                <Message icon>
                                  <Icon name="warning circle" />
                                  <Message.Content>
                                    <Message.Header>
                                      <FormattedMessage
                                        id={"app.message.header"}
                                      />
                                    </Message.Header>
                                    <p>
                                      <FormattedMessage
                                        id={"app.page.gpseason.info2"}
                                        values={{ season: year }}
                                      />
                                    </p>
                                  </Message.Content>
                                </Message>
                              )}
                            </Segment>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </div>
                    <div className="gpseason-stats-content">
                      <SectionPageHeader
                        title={
                          <FormattedMessage
                            id={"app.page.gpseason.stats.drivers.title"}
                          />
                        }
                      />
                      <Segment basic>
                        <Grid stackable columns="1">
                          <Grid.Row textAlign="center">
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.wins"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {driversStats.wins.amount}
                                  {" - "}
                                  {driversStats.wins.driver}
                                </Header>
                                <NavLink
                                  to={`/driver/${driversStats.wins.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${driversStats.wins.idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={driversSeasonWins}
                                  seriesName={formatMessage({
                                    id: "app.stats.wins",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.points"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {
                                    +(
                                      Math.round(
                                        Number(driversStats.points.amount) * 100
                                      ) / 100
                                    )
                                  }
                                  {" - "}
                                  {driversStats.points.driver}
                                </Header>
                                <NavLink
                                  to={`/driver/${driversStats.points.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${driversStats.points.idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={driversSeasonPoints}
                                  seriesName={formatMessage({
                                    id: "app.stats.points",
                                  })}
                                  tooltipLabel="Kierowca"
                                  height="400px"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.podium"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {driversStats.podium.amount}
                                  {" - "}
                                  {driversStats.podium.driver}
                                </Header>
                                <NavLink
                                  to={`/driver/${driversStats.podium.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${driversStats.podium.idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={driversSeasonPodium}
                                  seriesName={formatMessage({
                                    id: "app.stats.podium",
                                  })}
                                  tooltipLabel="Kierowca"
                                  height="300px"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.polepos"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {driversStats.polepos.amount}
                                  {" - "}
                                  {driversStats.polepos.driver}
                                </Header>
                                <NavLink
                                  to={`/driver/${driversStats.polepos.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${driversStats.polepos.idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={driversSeasonPolepos}
                                  seriesName={formatMessage({
                                    id: "app.stats.polepos",
                                  })}
                                  tooltipLabel="Kierowca"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.bestlaps"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {driversStats.bestlaps.amount}
                                  {" - "}
                                  {driversStats.bestlaps.driver}
                                </Header>
                                <NavLink
                                  to={`/driver/${driversStats.bestlaps.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/drivers/driver_${driversStats.bestlaps.idDriver}_profile.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/drivers/driver_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={driversSeasonBestlaps}
                                  seriesName={formatMessage({
                                    id: "app.stats.bestlaps",
                                  })}
                                  tooltipLabel="Kierowca"
                                  height="300px"
                                />
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Segment>
                      <SectionPageHeader
                        title={
                          <FormattedMessage
                            id={"app.page.gpseason.stats.teams.title"}
                          />
                        }
                      />
                      <Segment basic>
                        <Grid stackable columns="1">
                          <Grid.Row textAlign="center">
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.wins"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {teamsStats.wins.amount}
                                  {" - "}
                                  {teamsStats.wins.team}
                                </Header>
                                <NavLink to={`/team/${teamsStats.wins.alias}`}>
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${teamsStats.wins.idTeam}_profile_logo.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={teamsSeasonWins}
                                  seriesName={formatMessage({
                                    id: "app.stats.wins",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.points"} />
                                  }
                                  type="quaternary"
                                />
                                {year < 1958 ? (
                                  <Message icon>
                                    <Icon name="warning circle" />
                                    <Message.Content>
                                      <Message.Header>
                                        <FormattedMessage
                                          id={"app.message.header"}
                                        />
                                      </Message.Header>
                                      <p>
                                        W sezonie {year} zespoły nie zdobywały
                                        punktów
                                      </p>
                                    </Message.Content>
                                  </Message>
                                ) : (
                                  <>
                                    <Header size="small" textAlign="center">
                                      {
                                        +(
                                          Math.round(
                                            Number(
                                              teamsStats.pointsClass.amount
                                            ) * 100
                                          ) / 100
                                        )
                                      }{" "}
                                      - {teamsStats.points.team}
                                    </Header>
                                    <NavLink
                                      to={`/team/${teamsStats.points.alias}`}
                                    >
                                      <Image
                                        size="tiny"
                                        centered
                                        src={`/build/images/teams/team_${teamsStats.points.idTeam}_profile_logo.jpg`}
                                        onError={(e) => {
                                          e.target.src =
                                            "/build/images/teams/team_no_profile.jpg";
                                        }}
                                      />
                                    </NavLink>
                                    <ChartStatsBar
                                      values={teamsSeasonPoints}
                                      seriesName={formatMessage({
                                        id: "app.stats.points",
                                      })}
                                      tooltipLabel="Zespół"
                                      height="400px"
                                    />
                                  </>
                                )}
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.podium"} />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {teamsStats.podium.amount}
                                  {" - "}
                                  {teamsStats.podium.team}
                                </Header>
                                <NavLink
                                  to={`/team/${teamsStats.podium.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${teamsStats.podium.idTeam}_profile_logo.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={teamsSeasonPodium}
                                  seriesName={formatMessage({
                                    id: "app.stats.podium",
                                  })}
                                  tooltipLabel="Zespół"
                                  height="300px"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.polepos"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {teamsStats.polepos.amount}
                                  {" - "}
                                  {teamsStats.polepos.team}
                                </Header>
                                <NavLink
                                  to={`/team/${teamsStats.polepos.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${teamsStats.polepos.idTeam}_profile_logo.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={teamsSeasonPolepos}
                                  seriesName=" Pole Position"
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            </Grid.Column>
                            <Grid.Column>
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.bestlaps"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header size="small" textAlign="center">
                                  {teamsStats.bestlaps.amount}
                                  {" - "}
                                  {teamsStats.bestlaps.team}
                                </Header>
                                <NavLink
                                  to={`/team/${teamsStats.bestlaps.alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${teamsStats.bestlaps.idTeam}_profile_logo.jpg`}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={teamsSeasonBestlaps}
                                  seriesName={formatMessage({
                                    id: "app.stats.bestlaps",
                                  })}
                                  tooltipLabel="Zespół"
                                  height="300px"
                                />
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Segment>
                    </div>
                    <div className="hideForDesktop">
                      <div className="gpseason-stats">
                        <Segment basic textAlign="center">
                          <SectionPageHeader
                            title={<FormattedMessage id={"app.stats.wins"} />}
                            type="quaternary"
                          />
                          <ReactHighcharts
                            config={configStatsSeasonWinsPercent}
                          />
                          <SectionPageHeader
                            title={<FormattedMessage id={"app.stats.podium"} />}
                            type="quaternary"
                          />
                          <ReactHighcharts
                            config={configStatsSeasonPodiumPercent}
                          />
                          <SectionPageHeader
                            title={
                              <FormattedMessage id={"app.stats.polepos"} />
                            }
                            type="quaternary"
                          />
                          <ReactHighcharts
                            config={configStatsSeasonPoleposPercent}
                          />
                          <SectionPageHeader
                            title={
                              <FormattedMessage id={"app.stats.bestlaps"} />
                            }
                            type="quaternary"
                          />
                          <ReactHighcharts
                            config={configStatsSeasonBestlapsPercent}
                          />
                        </Segment>
                      </div>
                      <div className="gpseason-models">
                        <SectionPageHeader
                          title={
                            <FormattedMessage
                              id={"app.page.gpseason.stats.models"}
                            />
                          }
                          type="secondary"
                        />
                        <Grid stackable>{this.renderTeamModels()}</Grid>
                      </div>
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

GPSeason.propTypes = {
  fetchGPSeason: PropTypes.func.isRequired,
  gpseason: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboSeasons: PropTypes.func.isRequired,
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpseason, comboSeasons }) {
  return { gpseason, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGPSeason, fetchComboSeasons }, dispatch);
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPSeason)
);
