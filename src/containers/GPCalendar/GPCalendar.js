/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import { Image, Icon, Flag, Loader } from "semantic-ui-react";
import PropTypes from "prop-types";
import { fetchGPInfo } from "../../actions/GPInfoActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPCalendar.less";

class GPCalendar extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderGPInfo = this.renderGPInfo.bind(this);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchGPInfo(props.year, Cookies.get("lang"));
  }

  renderGPInfo() {
    const { props } = this;
    if (props.gpinfo.data.length > 0) {
      const gpItems = [];
      const { data } = props.gpinfo;
      data.forEach((item) => {
        const {
          number,
          name,
          country,
          circuit,
          circuitAlias,
          alias,
          date,
          winner,
          photo,
        } = item;
        let filename = "/build/images/circuits/circuit_no_profile.jpg";
        if (photo == "") {
          filename = "/build/images/circuits/circuit_no_profile.jpg";
        } else {
          filename = `/build/images/circuits/circuit_${circuitAlias}_profile.jpg`;
        }
        const detailsLink = `/gp-result/${alias}/${props.year}`;
        let completedGP = "";
        if (winner !== "nieznany" && winner !== "unknown") {
          completedGP = "completed-gp";
        }
        const gpItem = (
          <div key={number} className="box">
            <div className={`box-extra ${completedGP}`}>
              <Icon name="trophy" /> {winner}
            </div>
            <div className={`box-image ${completedGP}`}>
              <NavLink to={detailsLink}>
                <Image src={filename} alt={`${name} - ${circuit}`} />
              </NavLink>
            </div>
            <div className="box-content">
              <div className="box-header">
                <Flag name={country} />
                <NavLink to={`/gp-result/${alias}/${props.year}`}>
                  {number}
                  {". "}
                  {name}
                </NavLink>
              </div>
              <div className="box-description">{date}</div>
              <div className="box-info">{circuit}</div>
            </div>
          </div>
        );
        gpItems.push(gpItem);
      });
      return gpItems;
    }
    return "";
  }

  render() {
    const { gpinfo, year } = this.props;
    if (!gpinfo.data || gpinfo.data.length === 0) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    return (
      <section id="gp-calendar">
        <div className="gp-calendar-header">
          <h2>
            <FormattedMessage id={"app.page.home.calendar.title"} />
          </h2>
          <h3>
            <FormattedMessage id={"app.stats.season"} /> {year}
          </h3>
        </div>
        <div className="gp-calendar-content">
          <div className="box-container">{this.renderGPInfo()}</div>
        </div>
      </section>
    );
  }
}

GPCalendar.propTypes = {
  fetchGPInfo: PropTypes.func.isRequired,
  gpinfo: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpinfo }) {
  return { gpinfo };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGPInfo }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GPCalendar);
