// const request = require('../json/circuits_stats_list.json');
//
// export const FETCH_CIRCUITS_STATS_SUCCESS = 'FETCH_CIRCUITS_STATS_SUCCESS';
//
// export function fetchCircuitsStats() {
//   return {
//     type: FETCH_CIRCUITS_STATS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_CIRCUITS_STATS_REQUEST = 'FETCH_CIRCUITS_STATS_REQUEST';
export const FETCH_CIRCUITS_STATS_SUCCESS = 'FETCH_CIRCUITS_STATS_SUCCESS';
export const FETCH_CIRCUITS_STATS_ERROR = 'FETCH_CIRCUITS_STATS_ERROR';

function requestData() {
  return {
    type: FETCH_CIRCUITS_STATS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_CIRCUITS_STATS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_CIRCUITS_STATS_ERROR,
    payload: error
  };
}

export function fetchCircuitsStats() {
  const url = `${ROOT_URL}/get_circuits_stats_list.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
