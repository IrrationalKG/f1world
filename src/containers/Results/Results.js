/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Grid,
  Loader,
  Header,
  Divider,
  Item,
  Label,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import { fetchMenu } from "../../actions/MenuActions";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Results.less";

class Results extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderResults = this.renderResults.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchMenu(props.match.params.year, "", Cookies.get("lang"));
  }

  renderResults() {
    const items = [];
    const { props } = this;
    const { data } = props.menu;
    if (data) {
      let num = 0;
      data[3].items.forEach((item) => {
        const {
          alias,
          name,
          circuit,
          date,
          winner,
          second,
          third,
          pp,
          bl,
          sprint,
          sprint_winner,
          photo,
          picture,
          isNextGP,
        } = item;
        let filename = "/build/images/countries/country_no_profile.jpg";
        if (picture === "1") {
          filename = `/build/images/countries/${photo}`;
        }
        const detailsLink = `/gp-result/${alias}/${props.match.params.year}`;
        let labelColor = winner != "nieznany" ? "grey" : "yellow";
        if (isNextGP === "1") {
          labelColor = "red";
        }
        num += 1;
        const record = (
          <Grid.Column
            textAlign="center"
            key={alias}
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <Segment basic>
                    <Image
                      fluid
                      label={{
                        color: `${labelColor}`,
                        corner: "left",
                        content: `${num}`,
                        size: "large",
                        horizontal: true,
                      }}
                    />
                    <NavLink to={detailsLink}>
                      <Image
                        centered
                        src={filename}
                        className="image-max-width menu-link"
                      />
                      {sprint == 1 && (
                        <Label attached="top right">
                          <FormattedMessage id={"app.stats.sprint"} />
                        </Label>
                      )}
                    </NavLink>
                  </Segment>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content>
                        {name}
                        <Header.Subheader>{circuit}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                    <Header as="h3">
                      <Header.Content>
                        <FormattedMessage id={"app.stats.date"} />
                        <Header.Subheader>{date}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider hidden fitted></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column textAlign="left" className="info-box-light">
                  <Item.Group divided>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.p1"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {winner != "nieznany" ? (
                              winner
                            ) : (
                              <FormattedMessage id={"app.stats.unknown"} />
                            )}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.p2"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {second ? (
                              second
                            ) : (
                              <FormattedMessage id={"app.stats.unknown"} />
                            )}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.p3"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {third ? (
                              third
                            ) : (
                              <FormattedMessage id={"app.stats.unknown"} />
                            )}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.pp"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {pp ? (
                              pp
                            ) : (
                              <FormattedMessage id={"app.stats.unknown"} />
                            )}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.bestlap"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {bl ? (
                              bl
                            ) : (
                              <FormattedMessage id={"app.stats.unknown"} />
                            )}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.sprint"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {sprint == "1" &&
                              (sprint_winner ? (
                                sprint_winner
                              ) : (
                                <FormattedMessage id={"app.stats.unknown"} />
                              ))}
                            {sprint == "0" && "-"}
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                  </Item.Group>
                  <div className="buttons">
                    <NavLink className="details menu-link" to={detailsLink}>
                      <FormattedMessage id={"app.button.details"} />
                    </NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { props } = this;
    if (!props.menu.data || props.menu.loading) {
      return (
        <section id="results" name="results" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "../build/images/results/results_banner.jpg";
    return (
      <section id="results" name="results" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.results.title"} />}
          subtitle={
            <FormattedMessage
              id={"app.page.results.subtitle"}
              values={{ season: props.match.params.year }}
            />
          }
        />
        <div>
          <Grid>{this.renderResults()}</Grid>
        </div>
      </section>
    );
  }
}
Results.propTypes = {
  fetchMenu: PropTypes.func.isRequired,
  menu: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      year: PropTypes.string,
    }).isRequired,
  }).isRequired,
  year: PropTypes.string,
};

function mapStateToProps({ menu }) {
  return { menu };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Results);
