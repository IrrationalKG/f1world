import React from "react";
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
import { Loader } from "semantic-ui-react";
import ScrollToTop from "./components/ScrollToTop/ScrollToTop";
import App from "./components/App/App";
import Home from "./components/Home/Home";

const delay = 300;
const LoadingInfo = (
  <section>
    <div className="full-height">
      <Loader active inline="centered">
        Ładowanie danych...
      </Loader>
    </div>
  </section>
);
const NewsDetails = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "news-details" */ "./containers/NewsDetails/NewsDetails"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Driver = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "drivers" */ "./containers/Driver/Driver"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriverModels = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "teams" */ "./containers/DriverModels/DriverModels"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriverTeammates = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "teams" */ "./containers/DriverTeammates/DriverTeammates"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Drivers = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "drivers" */ "./containers/Drivers/Drivers"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Team = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "teams" */ "./containers/Team/Team"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamModels = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "teams" */ "./containers/TeamModels/TeamModels"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Teams = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "teams" */ "./containers/Teams/Teams"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Circuit = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "circuits" */ "./containers/Circuit/Circuit"
    ),
  loading() {
    return LoadingInfo;
  },
});
const Circuits = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "circuits" */ "./containers/Circuits/Circuits"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Results = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "results" */ "./containers/Results/Results"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPResult = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "results" */ "./containers/GPResult/GPResult"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ClassDriversPlaces = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "classification" */ "./containers/ClassDriversPlaces/ClassDriversPlaces"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ClassDriversPoints = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "classification" */ "./containers/ClassDriversPoints/ClassDriversPoints"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ClassTeamsPlaces = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "classification" */ "./containers/ClassTeamsPlaces/ClassTeamsPlaces"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ClassTeamsPoints = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "classification" */ "./containers/ClassTeamsPoints/ClassTeamsPoints"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Champs = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/Champs/Champs"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const Seasons = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/Seasons/Seasons"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeason = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeason/GPSeason"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonPoints = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonPoints/GPSeasonPoints"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonPlaces = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonPlaces/GPSeasonPlaces"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonQual = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonQual/GPSeasonQual"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonSummary = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonSummary/GPSeasonSummary"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonDrivers = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonDrivers/GPSeasonDrivers"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPSeasonTeams = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPSeasonTeams/GPSeasonTeams"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GrandPrix = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GrandPrix/GrandPrix"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const GPStats = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/GPStats/GPStats"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const CircuitsStats = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/CircuitsStats/CircuitsStats"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriversStats = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/DriversStats/DriversStats"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamsStats = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/TeamsStats/TeamsStats"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriversRecords = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/DriversRecords/DriversRecords"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriversRecordsList = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/DriversRecordsList/DriversRecordsList"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const RecordsWidget = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/RecordsWidget/RecordsWidget"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const StatsWidget = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/StatsWidget/StatsWidget"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamsRecords = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/TeamsRecords/TeamsRecords"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamsRecordsList = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/TeamsRecordsList/TeamsRecordsList"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const DriverEvents = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/DriverEvents/DriverEvents"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamEvents = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/TeamEvents/TeamEvents"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const TeamModelEvents = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "history" */ "./containers/TeamModelEvents/TeamModelEvents"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestTop = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestTop/ContestTop"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestRegulations = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestRegulations/ContestRegulations"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestClass = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestClass/ContestClass"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestClassGP = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestClassGP/ContestClassGP"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestStats = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestStats/ContestStats"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestGPRates = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestGPRates/ContestGPRates"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestRegister = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestRegister/ContestRegister"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestRate = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestRate/ContestRate"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const ContestPlayer = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "contest" */ "./containers/ContestPlayer/ContestPlayer"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});
const NotFound = Loadable({
  loader: () =>
    import(
      /* webpackPrefetch: true */ /* webpackPreload: false */ /* webpackChunkName: "not-found" */ "./components/NotFound/NotFound"
    ),
  loading() {
    return LoadingInfo;
  },
  delay: delay,
});

export default (
  <ScrollToTop>
    <App>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/index.html" component={Home} />
        <Route exact path="/news/:newsId" component={NewsDetails} />
        <Route exact path="/drivers-list/:year" component={Drivers} />
        <Route exact path="/driver/:driverId" component={Driver} />
        <Route exact path="/driver/:year/:driverId" component={Driver} />
        <Route exact path="/driver-models/:driverId" component={DriverModels} />
        <Route
          exact
          path="/driver-teammates/:driverId"
          component={DriverTeammates}
        />
        <Route exact path="/teams-list/:year" component={Teams} />
        <Route exact path="/team/:teamId" component={Team} />
        <Route exact path="/team/:year/:teamId" component={Team} />
        <Route exact path="/team-models/:teamId" component={TeamModels} />
        <Route exact path="/circuits-list/:year" component={Circuits} />
        <Route exact path="/circuit/:circuitId" component={Circuit} />
        <Route exact path="/circuit/:year/:circuitId" component={Circuit} />
        <Route exact path="/results-list/:year" component={Results} />
        <Route exact path="/gp-result/:gpId/:year" component={GPResult} />
        <Route
          exact
          path="/classifications-list/:year"
          component={ClassDriversPoints}
        />
        <Route
          exact
          path="/classification/drivers-places/:year"
          component={ClassDriversPlaces}
        />
        <Route
          exact
          path="/classification/drivers-points/:year"
          component={ClassDriversPoints}
        />
        <Route
          exact
          path="/classification/teams-places/:year"
          component={ClassTeamsPlaces}
        />
        <Route
          exact
          path="/classification/teams-points/:year"
          component={ClassTeamsPoints}
        />
        <Route exact path="/history-list/:year" component={Champs} />
        <Route exact path="/champs" component={Champs} />
        <Route exact path="/seasons" component={Seasons} />
        <Route exact path="/gp-season/:year" component={GPSeason} />
        <Route
          exact
          path="/gp-season/points/:year"
          component={GPSeasonPoints}
        />
        <Route
          exact
          path="/gp-season/places/:year"
          component={GPSeasonPlaces}
        />
        <Route exact path="/gp-season/qual/:year" component={GPSeasonQual} />
        <Route
          exact
          path="/gp-season/summary/:year"
          component={GPSeasonSummary}
        />
        <Route
          exact
          path="/gp-season/drivers/:year"
          component={GPSeasonDrivers}
        />
        <Route exact path="/gp-season/teams/:year" component={GPSeasonTeams} />
        <Route exact path="/gp-stats-list" component={GrandPrix} />
        <Route exact path="/gp-stats/:id" component={GPStats} />
        <Route exact path="/circuits-stats-list" component={CircuitsStats} />
        <Route exact path="/drivers-stats-list" component={DriversStats} />
        <Route
          exact
          path="/drivers-stats-list/:letter"
          component={DriversStats}
        />
        <Route
          exact
          path="/drivers-stats-list/country/:country"
          component={DriversStats}
        />
        <Route exact path="/teams-stats-list" component={TeamsStats} />
        <Route exact path="/teams-stats-list/:letter" component={TeamsStats} />
        <Route
          exact
          path="/teams-stats-list/country/:country"
          component={TeamsStats}
        />
        <Route exact path="/drivers-records" component={DriversRecordsList} />
        <Route
          exact
          path="/drivers-records/:stat/:teamId/:gpId/:circuitId/:beginYear/:endYear/:status/:place"
          component={DriversRecords}
        />
        <Route
          exact
          path="/drivers-records-list"
          component={DriversRecordsList}
        />
        <Route exact path="/teams-records" component={TeamsRecordsList} />
        <Route
          exact
          path="/teams-records/:stat/:gpId/:circuitId/:beginYear/:endYear/:status/:place"
          component={TeamsRecords}
        />
        <Route exact path="/teams-records-list" component={TeamsRecordsList} />
        <Route
          exact
          path="/records-widget/:stat/:teamId/:gpId/:circuitId/:beginYear/:endYear/:status/:place"
          component={RecordsWidget}
        />
        <Route exact path="/stats-widget/:statId" component={StatsWidget} />
        <Route
          exact
          path="/driver-events/:event/:driverId"
          component={DriverEvents}
        />
        <Route
          exact
          path="/driver-events/:event/:driverId/:teamId/:gpId/:circuitId/:engine/:model/:tyre/:season/:place"
          component={DriverEvents}
        />
        <Route
          exact
          path="/team-events/:event/:teamId"
          component={TeamEvents}
        />
        <Route
          exact
          path="/team-events/:event/:teamId/:driverId/:gpId/:circuitId/:engine/:model/:tyre/:season/:place"
          component={TeamEvents}
        />
        <Route
          exact
          path="/team-model-events/:event/:teamModelId"
          component={TeamModelEvents}
        />
        <Route
          exact
          path="/team-model-events/:event/:teamModelId/:season/:driverId"
          component={TeamModelEvents}
        />
        <Route
          exact
          path="/team-model-events/:event/:teamModelId/:season/:driverId/:teamId"
          component={TeamModelEvents}
        />
        <Route exact path="/contest/top" component={ContestTop} />
        <Route
          exact
          path="/contest/regulations"
          component={ContestRegulations}
        />
        <Route exact path="/contest/class/:year" component={ContestClass} />
        <Route
          exact
          path="/contest/class-gp/:year"
          component={ContestClassGP}
        />
        <Route exact path="/contest/stats" component={ContestStats} />
        <Route exact path="/contest/stats/:year" component={ContestStats} />
        <Route
          exact
          path="/contest/gprate/:year/:gpId"
          component={ContestGPRates}
        />
        <Route exact path="/contest/register" component={ContestRegister} />
        <Route exact path="/contest/rate" component={ContestRate} />
        <Route exact path="/contest-list/:year" component={ContestClass} />
        <Route
          exact
          path="/contest/player/:playerId"
          component={ContestPlayer}
        />
        <Route
          exact
          path="/contest/player/:playerId/:year"
          component={ContestPlayer}
        />
        <Route path="*" component={NotFound} />
      </Switch>
    </App>
  </ScrollToTop>
);
