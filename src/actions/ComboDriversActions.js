// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_DRIVERS_REQUEST = "FETCH_COMBO_DRIVERS_REQUEST";
export const FETCH_COMBO_DRIVERS_SUCCESS = "FETCH_COMBO_DRIVERS_SUCCESS";
export const FETCH_COMBO_DRIVERS_ERROR = "FETCH_COMBO_DRIVERS_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_DRIVERS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_DRIVERS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_DRIVERS_ERROR,
    payload: error,
  };
}

export function fetchComboDrivers(
  empty,
  team,
  gp,
  circuit,
  engine,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_drivers.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          team: team,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
