<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// grand prix
$query="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, name_alias alias from gp,gp_season where gp.id_gp=gp_season.id_gp and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}

// miejsca - kierowcy
$query="SELECT drivers.alias alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country, place, points, points_class pointsClass,
GROUP_CONCAT(
  DISTINCT teams.alias_name SEPARATOR ', '
) teamsAliases,
GROUP_CONCAT(
  DISTINCT TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) SEPARATOR ', '
) teamsNames
from drivers_class, drivers, teams, drivers_gp_involvements
where drivers_class.season='$year' 
and drivers.id_driver=drivers_class.id_driver 
AND drivers_gp_involvements.id_driver = drivers_class.id_driver 
AND drivers_gp_involvements.season = drivers_class.season 
AND teams.id_team = drivers_gp_involvements.id_team
GROUP BY drivers_class.id_driver
order by place";
$result = mysqli_query($dbhandle,$query);
$driversPlacesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, race_pos place,race_completed completed,race_time time,
  race_add_info info,grid_pos grid,race_best_lap bestLap, race_points points
  from gp_season left join gp on gp.id_gp=gp_season.id_gp 
  left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp and drivers_gp_results.id_driver=$tmp_id_driver and drivers_gp_results.season='$year'
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = gp.id_gp AND drivers_gp_starting_grid.id_driver = $tmp_id_driver AND drivers_gp_starting_grid.season = '$year'
  where gp_season.season=$year 
  group by gp_season.id_gp
  order by gp_season.sort, drivers_gp_results.race_pos";
  
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPlaces=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    if($r2["completed"]=='0') {
        if ($r2["time"]=='DNQ') $r2["place"]="NQ";
        else {
            if (strpos($r2["info"],"dyskwalifikacja") === false){
               $r2["place"]="-";
            }else{
               $r2["place"]="DS";
            }
        }
    }else if($r2["completed"]=="1") {
      $r2["place"]=$r2["place"];
    }else{
      $r2["place"]=null;
    }
    $gpPlaces[] = $r2;
  }
  $r["gpPlaces"]=$gpPlaces;
  $driversPlacesItems[] = $r;
}

// miejsca - zespoly
$query="SELECT distinct teams.alias_name alias,teams.id_team id,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,
lower(teams.team) team,teams.country_code country, place, points, points_class pointsClass,
GROUP_CONCAT(DISTINCT teams_models.team_name ORDER BY teams_models.team_name SEPARATOR ', ') modelName
from teams_class 
LEFT JOIN teams ON teams.id_team = teams_class.id_team
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team=teams_class.id_team AND drivers_gp_involvements.season=teams_class.season
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
where teams_class.season='$year' 
GROUP BY teams.id_team   
order by place";
$result = mysqli_query($dbhandle,$query);
$teamsPlacesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];

  $query2="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name gp, gp.id_gp id
  from gp_season left join gp on gp.id_gp=gp_season.id_gp where gp_season.season=$year order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gpPlaces=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $tmp_id_gp = $r2["id"];
    $query5="SELECT sum(race_points) points,lower(teams.team) team,teams.id_team id,race_points_ignored, 
    coalesce(grid_pos,0) grid, sum(race_best_lap) bestLap
    from drivers_gp_results
    LEFT JOIN teams ON teams.id_team = drivers_gp_results.id_team
    LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_results.id_gp 
    AND drivers_gp_starting_grid.id_team = drivers_gp_results.id_team AND drivers_gp_starting_grid.season = drivers_gp_results.season 
    and drivers_gp_starting_grid.is_pp=1
    where drivers_gp_results.id_team=teams.id_team
    and drivers_gp_results.season='$year' and drivers_gp_results.id_gp=$tmp_id_gp group by teams.id_team,drivers_gp_results.id_gp order by 1 desc";
    $result5 = mysqli_query($dbhandle,$query5);
    $r2["place"] = null;
    $cnt=0;
    while($r5 = mysqli_fetch_assoc($result5)) {
      $cnt+=1;
      if ($r5["id"] == $tmp_id_team) {
        if ($r5["points"] > 0){
            $r2["place"] = $cnt;
        }else{
            $r2["place"] = "-";
        }
        $r2["grid"] = $r5["grid"];
        $r2["bestLap"] = $r5["bestLap"];
        $r2["points"] = $r5["points"];
        break;
      }
    }

    $gpPlaces[]=$r2;
  }
  $r["gpPlaces"]=$gpPlaces;
  $teamsPlacesItems[] = $r;
}

// regulamin
$query="SELECT";
if ($lang=='pl') {
  $query.=" regulations";
}else{
  $query.=" regulations_en as regulations";
}  
$query.=" from season_regulations where year='$year'";
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $regulations = $r["regulations"];
}

$seasonPlaces["gp"]=$gpItems;
$seasonPlaces["classDriversPlaces"]=$driversPlacesItems;
$seasonPlaces["classTeamsPlaces"]=$teamsPlacesItems;
$seasonPlaces["regulations"]=$regulations;
$seasonPlaces["createTime"]=microtime(true)-$start_time;

// Response
$response = $seasonPlaces;

print json_encode($response);
mysqli_free_result($result);
?>
