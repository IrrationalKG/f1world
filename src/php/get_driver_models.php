<?
header('Access-Control-Allow-Origin: *');

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// aktualny rok
$query="SELECT max(season) maxYear FROM drivers_class";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}
$prevYear=$currentYear-1;

// pobranie kolejnosci kierowcow
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc";
}else{
  $query="SELECT alias FROM drivers order by surname asc";
}
$result = mysqli_query($dbhandle,$query);
$driversTab = array();
$cnt=0;
$currentDriverCnt=0;
$maxDriverCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $driversTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentDriverCnt = $cnt;
  }
  $cnt+=1;
}
$maxDriverCnt = $cnt;

// min pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc LIMIT 1";
}else{
  $query="SELECT alias FROM drivers order by surname asc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname desc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias FROM drivers order by surname desc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzedni kierowca
if (($currentDriverCnt-1)<0){
  $prevId=$max;
}else{
  $prevId = $driversTab[$currentDriverCnt-1];
}
// nastepny kierowca
if (($currentDriverCnt+1)>=$maxDriverCnt){
  $nextId=$min;
}else{
  $nextId = $driversTab[$currentDriverCnt+1];
}

// pobranie id
$query="SELECT id_driver id FROM drivers WHERE alias='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
}

// informacje o kierowcy
$query="SELECT alias,id_driver id,CONCAT(name,' ',surname) name, country, country_code countryCode, lower(country_short) countryShort, '' prevId, '' nextId 
FROM drivers WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
$name;
$alias;
$countryShort;
while($r = mysqli_fetch_assoc($result)) {
   $name=$r["name"];
   $alias=$r["alias"];
   $countryShort=$r["countryShort"];
}

// modele 
$query="SELECT teams_models.id_team_model, 
teams_models.team, 
teams.name, 
GROUP_CONCAT(DISTINCT teams.engine ORDER BY teams.engine SEPARATOR ', ') engine,
teams_models.season,
teams_models.model, 
teams_models.team_name teamName,
drivers_gp_involvements.id_driver
FROM teams_models
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team_model = teams_models.id_team_model
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_involvements.id_driver = $id
GROUP BY teams_models.model
ORDER BY teams_models.season DESC, teams_models.model DESC, drivers_gp_involvements.id_driver";
$result = mysqli_query($dbhandle,$query);
$driverModelsItems=array();
while($r = mysqli_fetch_assoc($result)) {
   $driverModelsItems[] = $r;
}
$teamItems["prevId"]=$prevId;
$teamItems["nextId"]=$nextId;
$teamItems["id"]=$id;
$teamItems["name"]=$name;
$teamItems["alias"]=$alias;
$teamItems["countryShort"]=$countryShort;
$teamItems["models"]=$driverModelsItems;
$teamItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $teamItems;

print json_encode($response);
mysqli_free_result($result);
?>
