// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_DRIVER_EVENTS_REQUEST = "FETCH_DRIVER_EVENTS_REQUEST";
export const FETCH_DRIVER_EVENTS_SUCCESS = "FETCH_DRIVER_EVENTS_SUCCESS";
export const FETCH_DRIVER_EVENTS_ERROR = "FETCH_DRIVER_EVENTS_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVER_EVENTS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVER_EVENTS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVER_EVENTS_ERROR,
    payload: error,
  };
}

export function fetchDriverEvents(
  page,
  driver,
  event,
  pos,
  team,
  gp,
  circuit,
  engine,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_driver_events.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          page: page,
          driver: driver,
          event: event,
          pos: pos,
          team: team,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
