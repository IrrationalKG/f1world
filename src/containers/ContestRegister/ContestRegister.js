/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["render"] }] */

import React, { Component } from "react";
import { withFormik, Field } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Segment, Message, Label, Grid, Form } from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { countryOptions } from "../../countries";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { saveContestRegister } from "../../actions/ContestRegisterActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ContestRegister.less";

class ContestRegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      countrySelected: { value: "pl", label: "Poland" },
      newEntry: true,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (
      nextProps.contestRegister.data !== null &&
      nextProps.contestRegister.data !== props.contestRegister.data
    ) {
      this.setState({
        isLoadingExternally: false,
        newEntry: false,
      });
    } else {
      this.setState({
        isLoadingExternally: false,
        newEntry: true,
      });
    }
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    const {
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
      handleSubmit,
      isSubmitting,
      contestRegister,
    } = props;
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const SelectCountry = ({ field, form }) => (
      <Select
        isLoading={isLoadingExternally}
        id="country"
        name="country"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={state.countrySelected}
        onChange={(c) => {
          this.setState({
            countrySelected: {
              value: c.value,
              label: countryOptions?.find((e) => e.value == c.value)?.text,
            },
          });
          form.setFieldValue(field.name, c.value);
        }}
        options={countryOptions.map((c) => ({ value: c.value, label: c.text }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder="Wybierz kraj"
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className="react-select-container"
        classNamePrefix="react-select"
      />
    );
    if (
      !state.newEntry &&
      contestRegister.data &&
      contestRegister.data.status === "SUCCESS"
    ) {
      return (
        <section
          id="contest-register-details"
          name="contest-register-details"
          className="section-page"
        >
          <SectionPageBanner
            title={<FormattedMessage id={"app.page.contest.register.title"} />}
            subtitle={
              <FormattedMessage id={"app.page.contest.register.confirm"} />
            }
          />
          <Segment padded basic>
            <Message positive>
              <Message.Header>
                <FormattedMessage id={"app.message.header"} />
              </Message.Header>
              <Message.List>
                <Message.Item>
                  <FormattedMessage
                    id={"app.page.contest.register.confirm.info"}
                  />
                </Message.Item>
              </Message.List>
            </Message>
            <div className="buttons">
              <NavLink className="secondary" to="/contest/rate">
                <FormattedMessage id={"app.button.betting"} />
              </NavLink>
            </div>
          </Segment>
        </section>
      );
    }
    return (
      <section
        id="contest-register-details"
        name="contest-register-details"
        className="section-page"
      >
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.contest.register.title"} />}
          subtitle={
            <FormattedMessage id={"app.page.contest.register.subtitle"} />
          }
        />
        <div className="section-page-content register">
          {!state.newEntry &&
            contestRegister.data &&
            contestRegister.data.status !== "SUCCESS" && (
              <Segment padded basic>
                <Message negative>
                  <Message.Header>
                    <FormattedMessage id={"app.message.header"} />
                  </Message.Header>
                  <Message.List>
                    <Message.Item>
                      {contestRegister.data.status ===
                        "ERROR-PLAYER-ALREADY-EXISTS" && (
                        <FormattedMessage
                          id={"app.page.contest.register.error1"}
                        />
                      )}
                      {contestRegister.data.status ===
                        "ERROR-PLAYER-NOT-SAVED" && (
                        <FormattedMessage
                          id={"app.page.contest.register.error2"}
                        />
                      )}
                    </Message.Item>
                  </Message.List>
                </Message>
              </Segment>
            )}
          <form onSubmit={handleSubmit}>
            <Segment padded basic>
              <Grid stackable padded>
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Segment basic>
                      <div className="field">
                        <label htmlFor="name">
                          <FormattedMessage
                            id={"app.page.contest.register.name"}
                          />
                          <span className="marked">*</span>
                        </label>
                        <Form.Input
                          id="name"
                          name="name"
                          placeholder={formatMessage({
                            id: "app.page.contest.register.name",
                          })}
                          fluid
                          // error={touched.name && errors.name}
                          value={values.name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        {errors.name && touched.name && errors.name && (
                          <Label
                            htmlFor="name"
                            color="red"
                            pointing
                            size="large"
                          >
                            {errors.name}
                          </Label>
                        )}
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <Segment basic>
                      <div className="field">
                        <label htmlFor="surname">
                          <FormattedMessage
                            id={"app.page.contest.register.surname"}
                          />
                        </label>
                        <Form.Input
                          id="surname"
                          name="surname"
                          placeholder={formatMessage({
                            id: "app.page.contest.register.surname",
                          })}
                          fluid
                          error={touched.surname && errors.surname}
                          value={values.surname}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Segment basic>
                      <div className="field">
                        <label htmlFor="password">
                          <FormattedMessage
                            id={"app.page.contest.register.password"}
                          />
                          <span className="marked">*</span>
                        </label>
                        <Form.Input
                          id="password"
                          name="password"
                          placeholder={formatMessage({
                            id: "app.page.contest.register.password",
                          })}
                          type="password"
                          fluid
                          // error={touched.password && errors.password}
                          value={values.password}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        {errors.password &&
                          touched.password &&
                          errors.password && (
                            <Label
                              htmlFor="password"
                              color="red"
                              pointing
                              size="large"
                            >
                              {errors.password}
                            </Label>
                          )}
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <Segment basic>
                      <div className="field">
                        <label htmlFor="email">
                          <FormattedMessage
                            id={"app.page.contest.register.email"}
                          />
                        </label>
                        <Form.Input
                          id="email"
                          name="email"
                          placeholder={formatMessage({
                            id: "app.page.contest.register.email",
                          })}
                          fluid
                          //error={touched.email && errors.email}
                          value={values.email}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                        {errors.email && touched.email && errors.email && (
                          <Label
                            htmlFor="email"
                            color="red"
                            pointing
                            size="large"
                          >
                            {errors.email}
                          </Label>
                        )}
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={16}>
                    <Segment basic>
                      <label htmlFor="country">
                        <FormattedMessage
                          id={"app.page.contest.register.country"}
                        />
                      </label>
                      <Field name="country" component={SelectCountry} />
                    </Segment>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <div className="buttons">
                <Button type="submit" disabled={isSubmitting}>
                  <FormattedMessage id={"app.button.register"} />
                </Button>
              </div>
              <Segment basic>
                <Message>
                  <Message.Header>
                    <FormattedMessage id={"app.message.header"} />
                  </Message.Header>
                  <Message.List>
                    <Message.Item>
                      <FormattedMessage
                        id={"app.page.contest.register.info1"}
                      />
                    </Message.Item>
                    <Message.Item>
                      <FormattedMessage
                        id={"app.page.contest.register.info2"}
                      />
                    </Message.Item>
                    <Message.Item>
                      <FormattedMessage
                        id={"app.page.contest.register.info3"}
                      />
                    </Message.Item>
                  </Message.List>
                </Message>
              </Segment>
            </Segment>
          </form>
        </div>
      </section>
    );
  }
}

ContestRegisterForm.propTypes = {
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool,
  contestRegister: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  params: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  nextGPYear: PropTypes.string,
  nextGPAlias: PropTypes.string,
  errors: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  touched: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  values: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    name: Yup.string().required(
      Cookies.get("lang") == "pl"
        ? "Imię/Pseudonim jest wymagane!"
        : "Name/Nickname is required!"
    ),
    password: Yup.string().required(
      Cookies.get("lang") == "pl"
        ? "Hasło jest wymagane!"
        : "Password is required!"
    ),
    email: Yup.string().email(
      Cookies.get("lang") == "pl"
        ? "Błędny format adresu email!"
        : "Incorrect email address format!"
    ),
  }),
  mapPropsToValues: () => ({
    name: "",
    surname: "",
    password: "",
    email: "",
    country: "pl",
  }),
  handleSubmit: (payload, { props, setSubmitting }) => {
    props.saveContestRegister(payload);
    setSubmitting(false);
  },
  displayName: "ContestRegisterForm",
})(ContestRegisterForm);

function mapStateToProps({ contestRegister, params }) {
  return {
    contestRegister,
    params,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveContestRegister,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(formikEnhancer)
);
