<?
header('Access-Control-Allow-Origin: *');

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie id
$query="SELECT id_user id FROM users WHERE alias='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
}

// pobranie ostatniego sezonu
if ($year==null){
  $query="SELECT MAX(season) season FROM competition_stats WHERE user=$id";
  $result = mysqli_query($dbhandle,$query);
  while($r = mysqli_fetch_assoc($result)) {
    $year = $r["season"];
  }
}

// statystyki
$stats=array();

$query="SELECT CONCAT(min(season), ' - ',max(season)) seasons, count(id_stats) seasonsNo, sum(p1) wins, sum(p2) second, sum(p3) third, sum(pod) podium, sum(top10) top10
FROM competition_stats WHERE user=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $stats["seasons"]= $r["seasons"];
  $stats["seasonsNo"]= $r["seasonsNo"];
  $stats["wins"]=$r["wins"];
  $stats["second"]=$r["second"];
  $stats["third"]=$r["third"];
  $stats["podium"]=$r["podium"];
  $stats["top10"]=$r["top10"];
}
$query="SELECT total, place
FROM competition_history WHERE id_user=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $stats["allTimePlace"]=$r["place"];
  $stats["allTimePoints"]=$r["total"];
}
$query="SELECT place, points, place_gp, points_gp
FROM competition_stats WHERE user=$id AND season=$year";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $stats["currentSeasonPlace"]=$r["place"];
  $stats["currentSeasonPoints"]=$r["points"];
  $stats["currentSeasonPlaceGP"]=$r["place_gp"];
  $stats["currentSeasonPointsGP"]=$r["points_gp"];
}
$query="SELECT count(place) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsWins"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsWins"]=0;
  }
}
$query="SELECT count(place) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place=2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsSecond"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsSecond"]=0;
  }
}
$query="SELECT count(place) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place=3";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsThird"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsThird"]=0;
  }
}
$query="SELECT count(place) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place<4";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsPodium"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsPodium"]=0;
  }
}
$query="SELECT count(place) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place<11";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsTop10"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsTop10"]=0;
  }
}
$query="SELECT count(place_gp) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place_gp=1 and season>'2003'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsWinsGP"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsWinsGP"]=0;
  }
}
$query="SELECT count(place_gp) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place_gp=2 and season>'2003'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsSecondGP"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsSecondGP"]=0;
  }
}
$query="SELECT count(place_gp) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place_gp=3 and season>'2003'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsThirdGP"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsThirdGP"]=0;
  }
}
$query="SELECT count(place_gp) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place_gp<4 and season>'2003'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsPodiumGP"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsPodiumGP"]=0;
  }
}
$query="SELECT count(place_gp) amount, GROUP_CONCAT(DISTINCT season ORDER BY season ASC SEPARATOR ', ') years FROM competition_stats WHERE user=$id AND place_gp<11 and season>'2003'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  if ($r["amount"] >0 ){
    $stats["seasonsTop10GP"]=$r["amount"]." - ".$r["years"];
  }else{
    $stats["seasonsTop10GP"]=0;
  }
}

// kariera
$career;

$query="SELECT user, season, rounds, place, points, place_gp placeGP, points_gp pointsGP, p1 wins, p2 second, p3 third, pod podium, top10, max_points maxPoints, avg_points avgPoints
FROM competition_stats WHERE user=$id ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$seasons=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasons[] = $r;
}
$career["seasons"]=$seasons;

// miejsca w klasyfikacji zwyklej
$query="SELECT gp.name_short name, competition_places.place y
FROM gp
LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season=$year
LEFT JOIN competition_places ON competition_places.gp=gp.name_short
AND competition_places.season=$year AND competition_places.user=$id
WHERE gp_season.season=$year
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$classPlaces=array();
while($r = mysqli_fetch_assoc($result)) {
  $classPlaces[] = $r;
}
$career["classPlaces"]=$classPlaces;

// miejsca w klasyfikacji gp
$query="SELECT gp.name_short name, competition_places_gp.place y
FROM gp
LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season=$year
LEFT JOIN competition_places_gp ON competition_places_gp.gp=gp.name_short
AND competition_places_gp.season=$year AND competition_places_gp.user=$id
AND competition_places_gp.points>0
WHERE gp_season.season=$year
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$classPlacesGP=array();
while($r = mysqli_fetch_assoc($result)) {
  $classPlacesGP[] = $r;
}
$career["classPlacesGP"]=$classPlacesGP;

// punkty w kolejkach
$query="SELECT gp.name_short name, typy.suma y
FROM gp
LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season=$year
LEFT JOIN typy ON typy.gp=gp.name_short
AND typy.sezon=$year AND typy.uczestnik=$id and typy.is_deleted=0 and typy.place>0
WHERE gp_season.season=$year
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$roundPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $roundPoints[] = $r;
}
$career["roundPoints"]=$roundPoints;

// punkty GP w kolejkach
$query="SELECT gp.name_short name, typy.pkt_gp y
FROM gp
LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season=$year
LEFT JOIN typy ON typy.gp=gp.name_short
AND typy.sezon=$year AND typy.uczestnik=$id and typy.is_deleted=0 and typy.place>0
WHERE gp_season.season=$year
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$roundPointsGP = array();
while($r = mysqli_fetch_assoc($result)) {
  $roundPointsGP[] = $r;
}
$career["roundPointsGP"]=$roundPointsGP;

// miejsca w kolejkach
$query="SELECT gp.name_short name, typy.place y
FROM gp
LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season=$year
LEFT JOIN typy ON typy.gp=gp.name_short
AND typy.sezon=$year AND typy.uczestnik=$id and typy.is_deleted=0 and typy.place>0
WHERE gp_season.season=$year
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$roundPlaces=array();
while($r = mysqli_fetch_assoc($result)) {
  $roundPlaces[] = $r;
}
$career["roundPlaces"]=$roundPlaces;

// punkty w sezonach
$query="SELECT season name, points y FROM competition_stats WHERE user='$id' GROUP BY season";
$result = mysqli_query($dbhandle,$query);
$seasonsPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonsPoints[] = $r;
}
$career["seasonsPoints"]=$seasonsPoints;

// punkty gp w sezonach
$query="SELECT season name, points_gp y FROM competition_stats WHERE user='$id' and season>'2003' AND points_gp>0 GROUP BY season";
$result = mysqli_query($dbhandle,$query);
$seasonsPointsGP=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonsPointsGP[] = $r;
}
$career["seasonsPointsGP"]=$seasonsPointsGP;

// miejsca w sezonach
$query="SELECT season name, place y FROM competition_stats WHERE user='$id' GROUP BY season";
$result = mysqli_query($dbhandle,$query);
$seasonsPlaces=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonsPlaces[] = $r;
}
$career["seasonsPlaces"]=$seasonsPlaces;

// miejsca gp w sezonach
$query="SELECT season name, place_gp y FROM competition_stats WHERE user='$id' and season>'2003' AND points_gp>0 GROUP BY season";
$result = mysqli_query($dbhandle,$query);
$seasonsPlacesGP=array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonsPlacesGP[] = $r;
}
$career["seasonsPlacesGP"]=$seasonsPlacesGP;

// typowanie
$rates=array();
// klasyfikacja
$query="SELECT typy.send_date date, typy.sezon season, gp.name_alias alias, gp.id_gp id,";
if ($lang=='pl') {
  $query.="gp.name gp,";
}else{
  $query.="gp.name_en as gp,";
}  
$query.="gp.name_short country, gp.country_code countryCode,
pp, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, pkt points, premia bonus, suma sum, pkt_gp pointsGP, place
FROM typy,users,gp,gp_season
WHERE users.id_user=typy.uczestnik
AND gp.name_short=typy.gp
AND gp_season.id_gp=gp.id_gp
AND gp_season.season=$year
AND typy.sezon=$year
AND typy.uczestnik=$id
AND typy.is_deleted=0
ORDER BY gp_season.sort";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $gp = $r["country"];
  // wyniki gp
  $query1="SELECT pp, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10 FROM gp_winners WHERE gp='$gp' AND sezon='$year'";
  $result1 = mysqli_query($dbhandle,$query1);
  $gpResults;
  while($r1 = mysqli_fetch_assoc($result1)) {
    $gpResults = $r1;
  }

  $r["result"]=$gpResults;
  $rates[] = $r;
}

// gracz
$query="SELECT id_user id, alias, CONCAT(name,' ',surname) name,'' season, '' stats, '' career, '' rates
FROM users WHERE users.id_user='$id'";
$result = mysqli_query($dbhandle,$query);
$player;
while($r = mysqli_fetch_assoc($result)) {
  $player = $r;
}

$player["season"]=$year;
$player["stats"]=$stats;
$player["career"]=$career;
$player["rates"]=$rates;
$player["createTime"]=microtime(true)-$start_time;

// Response
$response = $player;

print json_encode($response);
mysqli_free_result($result);
?>
