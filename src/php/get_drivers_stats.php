<?
header('Access-Control-Allow-Origin: *');

$letter=$_GET['letter'];
if ($letter==null) $letter=$_POST['letter'];
if ($letter==null) $letter="A";

$country=$_GET['country'];
if ($country==null) $country=$_POST['country'];
if ($country==null) $country="";
if (strpos($country, 'gb+sct') !== false) {
  $country="gb sct";
}

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="(SELECT max(season) maxYear FROM gp_season)";
$result = mysqli_query($dbhandle,$query);
$year;
while($r = mysqli_fetch_assoc($result)) {
  $year = $r["maxYear"];
}

// mistrzowie swiata
$query="SELECT drivers.id_driver id,count(drivers.alias) champs from drivers_class,drivers
where drivers_class.id_driver=drivers.id_driver and drivers_class.place=1 and drivers_class.season<'$year'
group by drivers.id_driver ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$champsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $champsTab[$r["id"]] = $r["champs"];
}

// lata startow - gp
$query="SELECT CONCAT(MIN(SUBSTRING(race_date,1,4)),' - ',MAX(SUBSTRING(race_date,1,4))) years,drivers.id_driver id,drivers.surname from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver GROUP BY drivers.id_driver";
$result = mysqli_query($dbhandle,$query);
$yearsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $yearsTab[$r["id"]] = $r["years"];
}

// lata startow - pp
$query="SELECT CONCAT(MIN(SUBSTRING(qual_date,1,4)),' - ',MAX(SUBSTRING(qual_date,1,4))) years,drivers.id_driver id,drivers.surname from drivers_pp_results,drivers
where drivers_pp_results.id_driver=drivers.id_driver GROUP BY drivers.id_driver";
$result = mysqli_query($dbhandle,$query);
$yearsPPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $yearsPPTab[$r["id"]] = $r["years"];
}

// starty
$query="SELECT drivers_gp_results.id_driver id,count(DISTINCT race_date) as starts from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver
group by drivers_gp_results.id_driver order by starts desc";
$result = mysqli_query($dbhandle,$query);
$startsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $startsTab[$r["id"]] = $r["starts"];
}

// kwalifikacje
$query="SELECT drivers_pp_results.id_driver id,count(DISTINCT qual_date) as qual from drivers_pp_results,drivers
where drivers_pp_results.id_driver=drivers.id_driver
group by drivers_pp_results.id_driver order by qual desc";
$result = mysqli_query($dbhandle,$query);
$qualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $qualTab[$r["id"]] = $r["qual"];
}

// zwyciestwa
$query="SELECT drivers_gp_results.id_driver id,count(DISTINCT race_date) as wins from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver and race_pos=1
group by drivers_gp_results.id_driver order by wins desc";
$result = mysqli_query($dbhandle,$query);
$winsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $winsTab[$r["id"]] = $r["wins"];
}

// punkty
$query="SELECT drivers_gp_results.id_driver id,
sum(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_driver=drivers.id_driver),0) as points 
from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver and race_points>0
group by drivers_gp_results.id_driver order by points desc";
$result = mysqli_query($dbhandle,$query);
$pointsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $pointsTab[$r["id"]] = $r["points"];
}

// podium
$query="SELECT drivers_gp_results.id_driver id,count(DISTINCT race_date) as podium from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver and race_pos<4
group by drivers_gp_results.id_driver order by podium desc";
$result = mysqli_query($dbhandle,$query);
$podiumTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $podiumTab[$r["id"]] = $r["podium"];
}

// pole position
$query="SELECT drivers_gp_starting_grid.id_driver id,count(id_starting_grid) as polepos from drivers_gp_starting_grid,drivers
where drivers_gp_starting_grid.id_driver=drivers.id_driver and is_pp=1
group by drivers_gp_starting_grid.id_driver order by polepos desc";
$result = mysqli_query($dbhandle,$query);
$poleposTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $poleposTab[$r["id"]] = $r["polepos"];
}

// best laps
$query="SELECT drivers_gp_results.id_driver id,count(DISTINCT race_date) as bestlaps from drivers_gp_results,drivers
where drivers_gp_results.id_driver=drivers.id_driver and race_best_lap=1
group by drivers_gp_results.id_driver order by bestlaps desc";
$result = mysqli_query($dbhandle,$query);
$bestlapsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $bestlapsTab[$r["id"]] = $r["bestlaps"];
}

/**
* Ranking
*/

//zwyciestwa
$query="SELECT COUNT(*) wins, drivers.id_driver id,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$winsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $winsRank[$r["id"]] = $place;
}
//punkty
$query="SELECT SUM(points) points, drivers.id_driver id,surname FROM drivers_class,drivers WHERE drivers_class.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pointsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $pointsRank[$r["id"]] = $place;
}
//podium
$query="SELECT COUNT(*) podium, drivers.id_driver id,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos>0 AND race_pos<4 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$podiumRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $podiumRank[$r["id"]] = $place;
}
//pole pos
$query="SELECT COUNT(*) polepos, drivers.id_driver id,surname FROM drivers_gp_starting_grid,drivers WHERE drivers_gp_starting_grid.id_driver=drivers.id_driver AND is_pp=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$ppRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $ppRank[$r["id"]] = $place;
}
//naj. okrazenia
$query="SELECT COUNT(*) bestlaps, drivers.id_driver id,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_best_lap=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$blRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $blRank[$r["id"]] = $place;
}
//starty
$query="SELECT COUNT(*) starts, drivers.id_driver id,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$startsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $startsRank[$r["id"]] = $place;
}
//kwalifikacje
$query="SELECT COUNT(*) qual, drivers.id_driver id,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$qualRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $qualRank[$r["id"]] = $place;
}

if ($country!=null && strlen($country) > 1){
  $query="SELECT id_driver id,CONCAT(surname,' ',name) name,alias,";
  if ($lang=='pl') {
    $query.="country,";
  }else{
    $query.="country_en as country,";
  }  
  $query.="country_code countryCode,picture,
  '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ
  from drivers where drivers.country_code LIKE '$country%' order by surname,name";
}else if (strlen($letter) == 1){
  $query="SELECT id_driver id,CONCAT(surname,' ',name) name,alias,";
  if ($lang=='pl') {
    $query.="country,";
  }else{
    $query.="country_en as country,";
  }  
  $query.="country_code countryCode,picture,
  '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ
  from drivers where drivers.surname LIKE '$letter%' order by surname,name";
}else{
  $query="SELECT drivers.id_driver id,CONCAT(drivers.surname,' ',drivers.name) name,drivers.alias,";
  if ($lang=='pl') {
    $query.="drivers.country,";
  }else{
    $query.="drivers.country_en as country,";
  }  
  $query.="drivers.country_code countryCode,drivers.picture, '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ from drivers, drivers_class where drivers.id_driver=drivers_class.id_driver and drivers_class.season LIKE '$letter' order by surname,name";
}
$result = mysqli_query($dbhandle,$query);
$driverItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["champ"]=$champsTab[$r["id"]]==null ? '' : $champsTab[$r["id"]]."x Mistrz Świata";
  $r["years"]=$yearsTab[$r["id"]]==null ? $yearsPPTab[$r["id"]] : $yearsTab[$r["id"]];
  $r["starts"]=$startsTab[$r["id"]]==null ? '0' : $startsTab[$r["id"]]." (".$startsRank[$r["id"]].".)";
  $r["qual"]=$qualTab[$r["id"]]==null ? '0' : $qualTab[$r["id"]]." (".$qualRank[$r["id"]].".)";
  $r["wins"]=$winsTab[$r["id"]]==null ? '0' : $winsTab[$r["id"]]." (".$winsRank[$r["id"]].".)";
  $r["points"]=$pointsTab[$r["id"]]==null ? '0' : $pointsTab[$r["id"]]." (".$pointsRank[$r["id"]].".)";
  $r["podium"]=$podiumTab[$r["id"]]==null ? '0' : $podiumTab[$r["id"]]." (".$podiumRank[$r["id"]].".)";
  $r["polepos"]=$poleposTab[$r["id"]]==null ? '0' : $poleposTab[$r["id"]]." (".$ppRank[$r["id"]].".)";
  $r["bestlaps"]=$bestlapsTab[$r["id"]]==null ? '0' : $bestlapsTab[$r["id"]]." (".$blRank[$r["id"]].".)";
  $driverItems[] = $r;
}
$driversStatsItems["drivers"]=$driverItems;

$query="SELECT count(distinct drivers.id_driver) amount,drivers.country_code countryCode, drivers.country country, drivers.country_short countryShort FROM drivers group by drivers.country_code order by 1 DESC";
$result = mysqli_query($dbhandle,$query);
$countryItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $countryItems[] = $r;
}
$driversStatsItems["countries"]=$countryItems;

$query="SELECT count(drivers.surname) amount,left(UPPER(drivers.surname), 1) letter FROM `drivers` group by left(drivers.surname, 1) order by 2 ASC";
$result = mysqli_query($dbhandle,$query);
$lettersItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $lettersItems[] = $r;
}
$driversStatsItems["letters"]=$lettersItems;
$driversStatsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $driversStatsItems;

print json_encode($response);
mysqli_free_result($result);
?>
