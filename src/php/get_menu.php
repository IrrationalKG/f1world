<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

$yearPrev = $year-1;
include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

/**
*-------------
* Kierowcy
*-------------
**/
// sezony
$query="SELECT id_driver,count(distinct gp.season) as seasons, MIN(season) min, MAX(season) max
FROM drivers_gp_involvements gp
group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversSeasonsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_driver = $r["id_driver"];
  if ($r["min"] == $r["max"]) {
    $driversSeasonsTabM[$tmp_id_driver] = $r["min"]." (".$r["seasons"].")";
  }else{
	  $driversSeasonsTabM[$tmp_id_driver] = $r["min"]." - ".$r["max"]." (".$r["seasons"].")";
  }
}
// starty
$query="select id_driver,count(id_drivers_gp) as starts from drivers_gp_results group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversStartsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_driver = $r["id_driver"];
	$driversStartsTabM[$tmp_id_driver] = $r["starts"];
}
// punkty
$query="select drivers.id_driver,TRUNCATE(ROUND(SUM(points),2),2) race_pts from drivers_class,drivers
where drivers.id_driver=drivers_class.id_driver group by drivers.id_driver order by points desc";
$resultM = mysqli_query($dbhandle,$query);
$driversPointsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPointsTabM[$tmp_id_driver] = $r["race_pts"];
}
// zwyciestwa
$query="select id_driver,count(id_drivers_gp) as wins from drivers_gp_results where race_pos=1 group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversWinsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversWinsTabM[$tmp_id_driver] = $r["wins"];
}
// podium
$query="select id_driver,count(id_drivers_gp) as podiums from drivers_gp_results where race_pos<4 and race_completed=1 group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversPodiumsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPodiumsTabM[$tmp_id_driver] = $r["podiums"];
}
// pole position
$query="select id_driver,count(id_starting_grid) as pp from drivers_gp_starting_grid where is_pp=1 group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversPPTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversPPTabM[$tmp_id_driver] = $r["pp"];
}
// best laps
$query="select id_driver,count(id_drivers_gp) as bestlaps from drivers_gp_results where race_best_lap=1 group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversBLTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversBLTabM[$tmp_id_driver] = $r["bestlaps"];
}
// champs
$query="select drivers.id_driver,drivers.name,drivers.surname,drivers.country_short,count(id_drivers_class) as champs from drivers_class,drivers where drivers.id_driver=drivers_class.id_driver and place=1 and season<=$year group by id_driver order by champs desc,surname";
$resultM = mysqli_query($dbhandle,$query);
$driversWCTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversWCTabM[$tmp_id_driver] = $r["champs"];
}
//najlepszy wynik
$query="select id_driver,MIN(place) as bestResult, season from drivers_class where season<'$year' group by id_driver";
$resultM = mysqli_query($dbhandle,$query);
$driversBestTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversBestTabM[$tmp_id_driver] = $r["bestResult"].'. ('.$r["season"].')';
}
//klasyfikacja kierowców
$query="select drivers.id_driver,place,name,surname,country_short,points from drivers_class,drivers where drivers.id_driver=drivers_class.id_driver and season='$year' order by place";
$resultM = mysqli_query($dbhandle,$query);
$i=0;
$driversClsPosTabM = array();
$driversClsPtsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
  $driversClsPosTabM[$tmp_id_driver] = $i+1;
	$driversClsPtsTabM[$tmp_id_driver] = $r["points"];
	$i++;
}
// zespoły
$query="select id_driver,teams.name from drivers_class,teams where drivers_class.id_team=teams.id_team and drivers_class.season='$year' group by id_driver,teams.name";
$resultM = mysqli_query($dbhandle,$query);
$driversTeamsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversTeamsTabM[$tmp_id_driver] = $r["name"];
}

$driversItems = array();
$query="select drivers.alias,drivers.id_driver id,drivers_class.number,'drivers' type,drivers.name,drivers.surname,drivers.country_short,drivers.country_code country,drivers.photo,drivers.picture,drivers.team_color,'' seasonPlace,'' seasonPoints,'' wins,'' points,'' podium,'' starts,'' polePosition,'' bestLaps,'' bestResult,'' team from drivers_class,drivers where drivers_class.id_driver=drivers.id_driver and season='$year' order by sort";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
    if (empty($driversSeasonsTabM[$r["id"]])){
      $r["seasons"]=0;
    }else{
      $r["seasons"]=$driversSeasonsTabM[$r["id"]];
    }
   if (empty($driversStartsTabM[$r["id"]])){
     $r["starts"]=0;
   }else{
     $r["starts"]=(int)$driversStartsTabM[$r["id"]];
   }
   if (empty($driversPointsTabM[$r["id"]])){
     $r["points"]=0;
   }else{
     $r["points"]=(double)$driversPointsTabM[$r["id"]];
   }
   if (empty($driversWinsTabM[$r["id"]])){
     $r["wins"]=0;
   }else{
     $r["wins"]=(int)$driversWinsTabM[$r["id"]];
   }
   if (empty($driversPodiumsTabM[$r["id"]])){
     $r["podium"]=0;
   }else{
     $r["podium"]=(int)$driversPodiumsTabM[$r["id"]];
   }
   if (empty($driversPPTabM[$r["id"]])){
     $r["polePosition"]=0;
   }else{
     $r["polePosition"]=(int)$driversPPTabM[$r["id"]];
   }
   if (empty($driversBLTabM[$r["id"]])){
     $r["bestLaps"]=0;
   }else{
     $r["bestLaps"]=(int)$driversBLTabM[$r["id"]];
   }

   if (!empty($driversWCTabM[$r["id"]])){
     $r["bestResult"]=(int)$driversWCTabM[$r["id"]]."x MŚ";
   }else {
     if (empty($driversBestTabM[$r["id"]])){
       $r["bestResult"]="Debiutant";
     }else{
       $r["bestResult"]=$driversBestTabM[$r["id"]];
     }
   }

   $r["seasonPlace"]=(int)$driversClsPosTabM[$r["id"]];
   $r["seasonPoints"]=(int)$driversClsPtsTabM[$r["id"]];
   $r["team"]=$driversTeamsTabM[$r["id"]];
   $driversItems[] = $r;
}

$drivers = array();
$drivers['id'] = "drivers";
$drivers['title'] = "Kierowcy";
$drivers['items'] = $driversItems;

/**
*-------------
* Zespoły
*-------------
**/
// min, max sezon
$query="SELECT lower(teams.team) team, MIN(distinct season) as min, MAX(distinct season) as max
FROM drivers_gp_involvements,teams 
WHERE drivers_gp_involvements.id_team=teams.id_team
GROUP BY teams.team";
$resultM = mysqli_query($dbhandle,$query);
$teamsMinSeasonsTabM = array();
$teamsMaxSeasonsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  $teamsMinSeasonsTabM[$tmp_id_team] = $r["min"];
  $teamsMaxSeasonsTabM[$tmp_id_team] = $r["max"];
}

// sezony
$query="SELECT lower(teams.team) team, count(distinct season) as seasons
FROM drivers_gp_involvements,teams 
WHERE drivers_gp_involvements.id_team=teams.id_team
GROUP BY teams.team";
$resultM = mysqli_query($dbhandle,$query);
$teamsSeasonsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
  if ($teamsMinSeasonsTabM[$tmp_id_team] == $teamsMaxSeasonsTabM[$tmp_id_team]) {
    $teamsSeasonsTabM[$tmp_id_team] = $teamsMinSeasonsTabM[$tmp_id_team]." (".$r["seasons"].")";
  }else{
	  $teamsSeasonsTabM[$tmp_id_team] = $teamsMinSeasonsTabM[$tmp_id_team]." - ".$teamsMaxSeasonsTabM[$tmp_id_team]." (".$r["seasons"].")";
  }
}
// starty
$query="SELECT team, count(items) as starts FROM (
SELECT season, lower(teams.team) team, group_concat(distinct race_date) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
GROUP BY drivers_gp_results.race_date,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsStartsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsStartsTabM[$tmp_id_team] = $r["starts"];
}
// punkty
$query="SELECT TRUNCATE(SUM(points),2) as race_pts,teams.name,lower(teams.team) team
FROM teams_class,teams WHERE teams_class.id_team=teams.id_team
GROUP BY teams.team";
$resultM = mysqli_query($dbhandle,$query);
$teamsPointsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsPointsTabM[$tmp_id_team] = $r["race_pts"];
}
// zwyciestwa
$query="SELECT team, count(items) as wins FROM (
SELECT season, lower(teams.team) team, group_concat(distinct drivers_gp_results.race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsWinsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsWinsTabM[$tmp_id_team] = $r["wins"];
}
// podium
$query="SELECT team, count(items) as podium FROM (
SELECT season, lower(teams.team) team, group_concat(distinct drivers_gp_results.race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos<4
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsPodiumsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
  $tmp_id_team = $r["team"];
	$teamsPodiumsTabM[$tmp_id_team] = $r["podium"];
}
// pole position
$query="SELECT team, count(items) as pp FROM (
SELECT season, lower(teams.team) team, group_concat(distinct drivers_gp_starting_grid.is_pp) as items
FROM drivers_gp_starting_grid,teams 
WHERE drivers_gp_starting_grid.id_team=teams.id_team
AND drivers_gp_starting_grid.is_pp=1
GROUP BY drivers_gp_starting_grid.id_starting_grid,teams.team,drivers_gp_starting_grid.is_pp
ORDER BY drivers_gp_starting_grid.season ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsPPTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsPPTabM[$tmp_id_team] = $r["pp"];
}
// best laps
$query="SELECT team, count(items) as bestlaps FROM (
SELECT season, lower(teams.team) team, group_concat(distinct drivers_gp_results.race_best_lap) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_best_lap
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_best_lap ASC
) as t GROUP BY team ORDER BY 2 DESC";
$resultM = mysqli_query($dbhandle,$query);
$teamsBLTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsBLTabM[$tmp_id_team] = $r["bestlaps"];
}
// champs
$query="select count(teams_class.team)as champs,lower(teams.team) team,teams.name,teams.country_short from teams_class,teams where teams.id_team=teams_class.id_team and place=1 and season>1957 and season<=$year group by teams_class.team,teams.name,teams.country_short order by champs desc,name";
$resultM = mysqli_query($dbhandle,$query);
$teamsWCTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsWCTabM[$tmp_id_team] = $r["champs"];
}
//najlepszy wynik
$query="select lower(teams_class.team) team,MIN(place) as bestResult, season from teams_class where season<'$year' group by team";
$resultM = mysqli_query($dbhandle,$query);
$teamsBestTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_team = $r["team"];
	$teamsBestTabM[$tmp_id_team] = $r["bestResult"].'. ('.$r["season"].')';
}
//klasyfikacja zespolow
$query="SELECT distinct teams.id_team,teams.name as teamname,teams.engine,lower(teams.team) team,place,points_class points,country_short
from teams_class,teams where teams.id_team=teams_class.id_team and season='$year' order by place";
$resultM = mysqli_query($dbhandle,$query);
$i=0;
$teamsClsPosTabM = array();
$teamsClsPtsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_team = $r["team"];
  $teamsClsPosTabM[$tmp_team] = $i+1;
	$teamsClsPtsTabM[$tmp_team] = $r["points"];
	$i++;
}

$teamsItems = array();
$query="SELECT teams.alias_name aliasName,teams.alias,lower(teams.team) id,sort number,'teams' type,teams.name,COALESCE(teams.engine,teams.name) engine,teams.country_short,teams.country_code country,teams.photo,teams.picture,teams.color,
'' seasonPlace,'' seasonPoints,'' wins,'' points,'' podium,'' starts,'' polePosition,'' bestLaps,'' bestResult
from teams_class,teams where teams.id_team=teams_class.id_team and season='$year' order by sort";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $r["seasons"]=$teamsSeasonsTabM[$r["id"]];
   $r["starts"]=(int) isset($teamsStartsTabM[$r["id"]]) ? $teamsStartsTabM[$r["id"]] : 0;
   $r["points"]=(double) isset($teamsPointsTabM[$r["id"]]) ? $teamsPointsTabM[$r["id"]] : 0;
   $r["wins"]=(int) isset($teamsWinsTabM[$r["id"]]) ? $teamsWinsTabM[$r["id"]] : 0;
   $r["podium"]=(int) isset($teamsPodiumsTabM[$r["id"]]) ? $teamsPodiumsTabM[$r["id"]] : 0;
   $r["polePosition"]=(int) isset($teamsPPTabM[$r["id"]]) ? $teamsPPTabM[$r["id"]] : 0;
   $r["bestLaps"]=(int) isset($teamsBLTabM[$r["id"]]) ? $teamsBLTabM[$r["id"]] : 0;

   if ((int) isset($teamsWCTabM[$r["id"]]) ? $teamsWCTabM[$r["id"]] : 0 >0){
     $r["bestResult"]=(int)$teamsWCTabM[$r["id"]]."x MŚ";
   }else{
    if (empty($teamsBestTabM[$r["id"]])){
      $r["bestResult"]="-";
    }else{
      $r["bestResult"]=$teamsBestTabM[$r["id"]];
    }
   }

   $r["seasonPlace"]=(int)$teamsClsPosTabM[$r["id"]];
   $r["seasonPoints"]=(int)$teamsClsPtsTabM[$r["id"]];
   $teamsItems[] = $r;
}

$teams = array();
$teams['id'] = "teams";
$teams['title'] = "Zespoły";
$teams['items'] = $teamsItems;

/**
*-------------
* Tory
*-------------
**/
//nastepna gp
$query="SELECT next_gp_id FROM params ORDER BY next_gp_year desc,contest_round desc limit 1";
$result = mysqli_query($dbhandle,$query);
$next_gp_id = "";
while($r = mysqli_fetch_assoc($result)) {
	$next_gp_id = $r["next_gp_id"];
}
// zwyciestwa
$query="SELECT id_gp, (SELECT CONCAT(count(drivers_gp_results.id_driver),' - ',drivers.surname) name 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver 
where drivers_gp_results.race_pos=1 and drivers_gp_results.id_gp=gp_season.id_gp 
group by drivers_gp_results.id_driver 
order by count(drivers_gp_results.id_driver) desc limit 1) mostWins 
FROM gp_season WHERE gp_season.season='$year' ORDER BY gp_season.sort";
$resultM = mysqli_query($dbhandle,$query);
$circuitMostWinsTab = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_gp = $r["id_gp"];
	$circuitMostWinsTab[$tmp_id_gp] = $r["mostWins"];
	$i++;
}
// podium
$query="SELECT id_gp, (SELECT CONCAT(count(drivers_gp_results.id_driver),' - ',drivers.surname) name 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver 
where drivers_gp_results.race_pos<4 and drivers_gp_results.id_gp=gp_season.id_gp 
group by drivers_gp_results.id_driver 
order by count(drivers_gp_results.id_driver) desc limit 1) mostPodiums 
FROM gp_season WHERE gp_season.season='$year' ORDER BY gp_season.sort";
$resultM = mysqli_query($dbhandle,$query);
$circuitMostPodiumsTab = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_gp = $r["id_gp"];
	$circuitMostPodiumsTab[$tmp_id_gp] = $r["mostPodiums"];
	$i++;
}
// pole position
$query="SELECT id_gp, (SELECT CONCAT(count(drivers_gp_starting_grid.id_driver),' - ',drivers.surname) name 
from drivers_gp_starting_grid 
left join drivers on drivers.id_driver=drivers_gp_starting_grid.id_driver 
where drivers_gp_starting_grid.is_pp=1 and drivers_gp_starting_grid.id_gp=gp_season.id_gp 
group by drivers_gp_starting_grid.id_driver 
order by count(drivers_gp_starting_grid.id_driver) desc limit 1) mostPP 
FROM gp_season WHERE gp_season.season='$year' ORDER BY gp_season.sort";
$resultM = mysqli_query($dbhandle,$query);
$circuitMostPPTab = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_gp = $r["id_gp"];
	$circuitMostPPTab[$tmp_id_gp] = $r["mostPP"];
	$i++;
}
// punkty
$query="SELECT id_gp, (SELECT CONCAT(sum(drivers_gp_results.race_points),' - ',drivers.surname) name 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver 
where drivers_gp_results.race_points>0 and drivers_gp_results.id_gp=gp_season.id_gp 
group by drivers_gp_results.id_driver 
order by sum(drivers_gp_results.race_points) desc limit 1) mostPoints 
FROM gp_season WHERE gp_season.season='$year' ORDER BY gp_season.sort";
$resultM = mysqli_query($dbhandle,$query);
$circuitMostPointsTab = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_gp = $r["id_gp"];
	$circuitMostPointsTab[$tmp_id_gp] = $r["mostPoints"];
	$i++;
}
// best laps
$query="SELECT id_gp, (SELECT CONCAT(count(drivers_gp_results.id_driver),' - ',drivers.surname) name 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver 
where drivers_gp_results.race_best_lap=1 and drivers_gp_results.id_gp=gp_season.id_gp 
group by drivers_gp_results.id_driver 
order by count(drivers_gp_results.id_driver) desc limit 1) mostBL 
FROM gp_season WHERE gp_season.season='$year' ORDER BY gp_season.sort";
$resultM = mysqli_query($dbhandle,$query);
$circuitMostBLTab = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_gp = $r["id_gp"];
	$circuitMostBLTab[$tmp_id_gp] = $r["mostBL"];
	$i++;
}

$query="SELECT 'circuits' type,gp.circuit_alias alias,gp.name_short gp,gp.id_gp id,gp_season.sort number,gp_season.sprint,gp.photo,gp.picture,gp.country_code country,circuit,SUBSTRING(gp_season.date,1,16) as date,build,gp.laps,gp.length,gp.distance,track,drivers_gp_results.id_driver idDriver,coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') winner, '' as isNextGP,
(SELECT COUNT(gps.id_gp) FROM gp_season gps WHERE gps.id_gp=gp.id_gp) seasons,";
if ($lang=='pl') {
  $query.="gp.name";
}else{
  $query.="gp.name_en as name";
}
$query.=" from gp 
left join gp_season on gp.id_gp=gp_season.id_gp 
left join drivers_gp_results on gp_season.id_gp=drivers_gp_results.id_gp and drivers_gp_results.race_date>'$year-01-01' and race_date<'$year-12-31' and race_pos=1 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver 
where gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$circuitsItems = array();
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id"]==$next_gp_id){
    $r["isNextGP"]="1";
  };
  $r["mostWins"]=$circuitMostWinsTab[$r["id"]];
  $r["mostPodiums"]=$circuitMostPodiumsTab[$r["id"]];
  $r["mostPP"]=$circuitMostPPTab[$r["id"]];
  $r["mostPoints"]=$circuitMostPointsTab[$r["id"]];
  $r["mostBL"]=$circuitMostBLTab[$r["id"]];
  $circuitsItems[] = $r;
}

$circuits = array();
$circuits['id'] = "circuits";
$circuits['title'] = "Tory";
$circuits['items'] = $circuitsItems;

/**
*-------------
* Wyniki
*-------------
**/
$query="SELECT 'results' type,gp.name_alias alias,LOWER(gp.name_short) gp,gp.id_gp id,gp_season.sort number,gp_season.sprint,";
if ($lang=='pl') {
  $query.="gp.name";
}else{
  $query.="gp.name_en as name";
}
$query.=",concat(LOWER(gp.name_short),'.jpg') photo,gp.picture,gp.country_code country,circuit,SUBSTRING(gp_season.date,1,16) as date,
gp.laps,gp.length,gp.distance,drivers_gp_results.id_driver idDriver,
coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') winner,
(SELECT coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver
where season='$year'
and id_gp=gp.id_gp
and race_pos=2) as second,
(SELECT coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver
where season='$year'
and id_gp=gp.id_gp
and race_pos=3) as third, 
(SELECT coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') 
from drivers_gp_starting_grid  
left join drivers on drivers.id_driver=drivers_gp_starting_grid .id_driver
where season='$year'
and id_gp=gp.id_gp
and is_pp=1) as pp, 
(SELECT coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') 
from drivers_gp_results 
left join drivers on drivers.id_driver=drivers_gp_results.id_driver
where season='$year'
and id_gp=gp.id_gp
and race_best_lap=1) as bl, 
(SELECT coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') 
from drivers_sprint_results 
left join drivers on drivers.id_driver=drivers_sprint_results.id_driver
where season='$year'
and id_gp=gp.id_gp
and sprint_pos=1) as sprint_winner, 
'' as isNextGP
from gp
left join gp_season on gp.id_gp=gp_season.id_gp
left join drivers_gp_results on gp_season.id_gp=drivers_gp_results.id_gp and drivers_gp_results.race_date>'$year-01-01' and race_date<'$year-12-31' and race_pos=1
left join drivers on drivers.id_driver=drivers_gp_results.id_driver
where gp_season.season='$year'
order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$resultsItems = array();
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id"]==$next_gp_id){
    $r["isNextGP"]="1";
  };
   $resultsItems[] = $r;
}

$results = array();
$results['id'] = "results";
$results['title'] = "Wyniki";
$results['items'] =  $resultsItems;

/**
*-------------
* Klasyfikacja
*-------------
**/
$classifications = array();
$classifications['id'] = "classifications";
$classifications['title'] = "Klasyfikacje";
$classificationsItems = array();

$classificationsItem = array();
$classificationsItem['id'] = "drivers-places";
$classificationsItem['type'] = "classifications";
$classificationsItem['name'] = "Miejsca";
$classificationsItem['subtitle'] = "Kierowcy";
$classificationsItem['photo'] = "drivers-places";
$classificationsItems[0] = $classificationsItem;

$classificationsItem = array();
$classificationsItem['id'] = "teams-places";
$classificationsItem['type'] = "classifications";
$classificationsItem['name'] = "Miejsca";
$classificationsItem['subtitle'] = "Zespoły";
$classificationsItem['photo'] = "teams-places";
$classificationsItems[1] = $classificationsItem;

$classificationsItem = array();
$classificationsItem['id'] = "drivers-points";
$classificationsItem['type'] = "classifications";
$classificationsItem['name'] = "Punkty";
$classificationsItem['subtitle'] = "Kierowcy";
$classificationsItem['photo'] = "drivers-points";
$classificationsItems[2] = $classificationsItem;

$classificationsItem = array();
$classificationsItem['id'] = "teams-points";
$classificationsItem['type'] = "classifications";
$classificationsItem['name'] = "Punkty";
$classificationsItem['subtitle'] = "Zespoły";
$classificationsItem['photo'] = "teams-points";
$classificationsItems[3] = $classificationsItem;

$classifications['items'] = $classificationsItems;

/**
*-------------
* Historia
*-------------
**/
$history = array();
$history['id'] = "history";
$history['title'] = "Historia";
$historyItems = array();

$historyItem = array();
$historyItem['id'] = "champs";
$historyItem['type'] = "history";
$historyItem['name'] = "Mistrzowie";
$historyItem['subtitle'] = "Podsumowanie";
$historyItem['photo'] = "history-champs";
$historyItems[0] = $historyItem;

$historyItem = array();
$historyItem['id'] = "seasons";
$historyItem['type'] = "history";
$historyItem['name'] = "Sezony";
$historyItem['subtitle'] = "Wyniki";
$historyItem['photo'] = "history-gpseason";
$historyItems[1] = $historyItem;

$historyItem = array();
$historyItem['id'] = "drivers-stats-list/a";
$historyItem['type'] = "history";
$historyItem['name'] = "Kierowcy";
$historyItem['subtitle'] = "Statystyki";
$historyItem['photo'] = "history-drivers";
$historyItems[2] = $historyItem;

$historyItem = array();
$historyItem['id'] = "teams-stats-list/a";
$historyItem['type'] = "history";
$historyItem['name'] = "Zespoły";
$historyItem['subtitle'] = "Statystyki";
$historyItem['photo'] = "history-teams";
$historyItems[3] = $historyItem;

$historyItem = array();
$historyItem['id'] = "gp-stats-list";
$historyItem['type'] = "history";
$historyItem['name'] = "Grand Prix";
$historyItem['subtitle'] = "Statystyki";
$historyItem['photo'] = "history-gp";
$historyItems[4] = $historyItem;

$historyItem = array();
$historyItem['id'] = "circuits-stats-list";
$historyItem['type'] = "history";
$historyItem['name'] = "Tory";
$historyItem['subtitle'] = "Statystyki";
$historyItem['photo'] = "history-circuits";
$historyItems[5] = $historyItem;

$historyItem = array();
$historyItem['id'] = "drivers-records";
$historyItem['type'] = "history";
$historyItem['name'] = "Kierowcy";
$historyItem['subtitle'] = "Rekordy";
$historyItem['photo'] = "history-drivers-records";
$historyItems[6] = $historyItem;

$historyItem = array();
$historyItem['id'] = "teams-records";
$historyItem['type'] = "history";
$historyItem['name'] = "Zespoły";
$historyItem['subtitle'] = "Rekordy";
$historyItem['photo'] = "history-teams-records";
$historyItems[7] = $historyItem;
$history['items'] = $historyItems;

/**
*-------------
* Konkurs
*-------------
**/
$contest = array();
$contest['id'] = "contest";
$contest['title'] = "Konkurs";
$contestItems = array();

$contestItem = array();
$contestItem['id'] = "rate";
$contestItem['type'] = "contest";
$contestItem['name'] = "Typowanie";
$contestItem['subtitle'] = "Formularz";
$contestItem['photo'] = "contest-rate";
$contestItems[0] = $contestItem;

$contestItem = array();
$contestItem['id'] = "gprate/$year/$gp";
$contestItem['type'] = "contest";
$contestItem['name'] = "Typy na GP";
$contestItem['subtitle'] = "Zestawienie";
$contestItem['photo'] = "contest-gprate";
$contestItems[1] = $contestItem;

$contestItem = array();
$contestItem['id'] = "class/$year";
$contestItem['type'] = "contest";
$contestItem['name'] = "Klasyfikacja";
$contestItem['subtitle'] = "Zwykła";
$contestItem['photo'] = "contest-class";
$contestItems[2] = $contestItem;

$contestItem = array();
$contestItem['id'] = "class/$yearPrev";
$contestItem['type'] = "contest";
$contestItem['name'] = "Wyniki";
$contestItem['subtitle'] = "Historia";
$contestItem['photo'] = "contest-class-history";
$contestItems[3] = $contestItem;

$contestItem = array();
$contestItem['id'] = "stats";
$contestItem['type'] = "contest";
$contestItem['name'] = "Statystyki";
$contestItem['subtitle'] = "Historia";
$contestItem['photo'] = "history-champs";
$contestItems[4] = $contestItem;

$contestItem = array();
$contestItem['id'] = "top";
$contestItem['type'] = "contest";
$contestItem['name'] = "Lista TOP 100";
$contestItem['subtitle'] = "Historia";
$contestItem['photo'] = "contest-history";
$contestItems[5] = $contestItem;

$contestItem = array();
$contestItem['id'] = "regulations";
$contestItem['type'] = "contest";
$contestItem['name'] = "Regulamin";
$contestItem['subtitle'] = "Zasady";
$contestItem['photo'] = "contest-regulations";
$contestItems[6] = $contestItem;

$contestItem = array();
$contestItem['id'] = "register";
$contestItem['type'] = "contest";
$contestItem['name'] = "Rejestracja";
$contestItem['subtitle'] = "Formularz";
$contestItem['photo'] = "contest-register";
$contestItems[7] = $contestItem;

$contest['items'] = $contestItems;

// Response
$response[0] = $drivers;
$response[1] = $teams;
$response[2] = $circuits;
$response[3] = $results;
$response[4] = $classifications;
$response[5] = $history;
$response[6] = $contest;

print json_encode($response);
mysqli_free_result($result);
?>
