/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { Button, Icon, Divider } from "semantic-ui-react";
import PropTypes from "prop-types";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ChartStatsBarRace.less";

class ChartStatsBarRace extends Component {
  constructor(props) {
    super(props);

    this.chart = null;
    this.timerId = null;

    this.state = {
      //round: this.props.values[0].data.length,
      isPlaying: false,
    };
    //this.lastRound = this.props.values[0].data.length;
    Cookies.set("chartBarRaceRound", this.props.values[0].data.length, {
      expires: 365,
    });

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.handleChangeRound = this.handleChangeRound.bind(this);
  }

  handleChangeRound = (value) => () => {
    const round = parseInt(value, 10);
    Cookies.set("chartBarRaceRound", round);
    this.chart.update(
      {
        subtitle: {
          text: this.getSubtitle(round),
        },
      },
      false,
      false,
      false
    );
    this.chart.series[0].setData(this.getData(round)[0], true, {
      duration: 500,
    });
  };

  play = (e) => {
    e.preventDefault();
    let round =
      parseInt(Cookies.get("chartBarRaceRound"), 10) ==
      this.props.values[0].data.length
        ? 1
        : parseInt(Cookies.get("chartBarRaceRound"), 10);
    this.timerId = setInterval(() => {
      this.update(round);
      round += 1;
    }, 500);
    this.chart.sequenceTimer = this.timerId;
    this.setState({ isPlaying: true });
  };

  update = (round) => {
    if (round >= this.props.values[0].data.length + 1) {
      this.pause();
    } else {
      this.chart.series[0].setData(this.getData(round)[0], true, {
        duration: 400,
      });
      this.chart.update(
        {
          subtitle: {
            text: this.getSubtitle(round),
          },
        },
        false,
        false,
        false
      );
      Cookies.set("chartBarRaceRound", round);
    }
  };

  pause = () => {
    if (this.timerId) {
      clearInterval(this.timerId);
      this.timerId = null;
      this.setState({ isPlaying: false });
    }
    this.chart.sequenceTimer = undefined;
  };

  getSubtitle = (round) => {
    const { formatMessage } = this.props.intl;
    return `<div class="highcharts-subtitle-value">${round}</div><div class="highcharts-subtitle-label">${formatMessage(
      {
        id: "app.stats.round",
      }
    )}</div>`;
  };

  getData = (round) => {
    const dataset = this.props.values;
    const output = dataset
      .map((item) => {
        return {
          name: item.name,
          y: Number(item?.data[round - 1]?.points),
          color: item.color,
        };
      })
      .sort((a, b) => b.y - a.y);
    return [output];
  };

  render() {
    const { state, props } = this;
    const round = Cookies.get("chartBarRaceRound");
    const data = this.getData(round)[0];

    const maxValue =
      Math.ceil(
        Math.max(
          ...Object.values(
            props.values
              .map((e) => e.data[e.data.length - 1])
              .map((e) => e.points)
          )
        ) / 10
      ) * 10;

    const configChart = {
      chart: {
        height: data.length * 20,
        events: {
          load: function () {
            this.chart = this;
          },
        },
        animation: {
          duration: 500,
        },
        marginRight: 50,
      },
      title: {
        text: "",
      },
      subtitle: {
        useHTML: true,
        text: this.getSubtitle(round),
        floating: true,
        align: "right",
        verticalAlign: "bottom",
        y: 50,
        x: -30,
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        shared: true,
        useHTML: true,
      },
      xAxis: {
        type: "category",
        min: 0,
        labels: {
          animate: true,
        },
      },
      yAxis: {
        opposite: true,
        tickPixelInterval: 150,
        title: {
          text: null,
        },
        max: maxValue,
      },
      plotOptions: {
        series: {
          animation: false,
          groupPadding: 0,
          pointPadding: 0.1,
          borderWidth: 0,
          colorByPoint: true,
          dataSorting: {
            enabled: true,
            matchByName: true,
          },
          type: "bar",
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: [
        {
          type: "bar",
          name: round,
          data: data,
        },
      ],
      // responsive: {
      //   rules: [
      //     {
      //       condition: {
      //         maxWidth: 550,
      //       },
      //       chartOptions: {
      //         xAxis: {
      //           visible: false,
      //         },
      //         subtitle: {
      //           x: 0,
      //         },
      //         plotOptions: {
      //           series: {
      //             dataLabels: [
      //               {
      //                 enabled: true,
      //                 y: 8,
      //               },
      //               {
      //                 enabled: true,
      //                 format: "{point.name}",
      //                 y: -8,
      //                 style: {
      //                   fontWeight: "normal",
      //                   opacity: 0.7,
      //                 },
      //               },
      //             ],
      //           },
      //         },
      //       },
      //     },
      //   ],
      // },
    };

    return (
      <div className="section-page-content charts-content">
        <Divider hidden></Divider>
        <figure className="highcharts-figure">
          <div className="charts-rounds-container">
            {this.props.values[0].data.map((item, idx) => (
              <Button
                key={idx}
                onClick={this.handleChangeRound(idx + 1)}
                size="mini"
              >
                {idx + 1}
                {"."}
                <br />
                {item.gp}
              </Button>
            ))}
          </div>
          {!state.isPlaying ? (
            <Button size="mini" onClick={this.play} active={state.isPlaying}>
              <Icon name="play" disabled={state.isPlaying} />
              <FormattedMessage id={"app.page.chart.stats.bar.race.play"} />
            </Button>
          ) : (
            <Button size="mini" onClick={this.pause} active={!state.isPlaying}>
              <Icon name="stop" disabled={!state.isPlaying} />
              <FormattedMessage id={"app.page.chart.stats.bar.race.stop"} />
            </Button>
          )}
        </figure>
        <HighchartsReact
          ref={(chart) => {
            this.chart = chart && chart.chart;
          }}
          highcharts={Highcharts}
          options={configChart}
        />
      </div>
    );
  }
}

ChartStatsBarRace.propTypes = {
  values: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  seriesName: PropTypes.string,
  tooltipLabel: PropTypes.string,
};

export default injectIntl(ChartStatsBarRace);
