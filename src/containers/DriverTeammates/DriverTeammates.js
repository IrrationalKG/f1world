/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderCountriesFlags"] }] */

import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Segment,
  Statistic,
  Item,
  Divider,
  Flag,
  Table,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";

import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchDriverTeammates } from "../../actions/DriverTeammatesActions";
import { fetchComboDrivers } from "../../actions/ComboDriversActions";
import styles from "./DriverTeammates.less";

class DriverTeammates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.driverId },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchDriverTeammates(props.match.params.driverId);
    props.fetchComboDrivers( 0,
          "-",
          "-",
          "-",
          "-",
          "-",
          "-",
          "-",
          Cookies.get("lang"));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { driverId } = props.match.params;
    const nextDriverId = nextProps.match.params.driverId;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextDriverId,
        label: nextProps.comboDrivers.data?.find((e) => e.value == nextDriverId)
          ?.label,
      },
    });

    if (driverId !== nextDriverId) {
      props.fetchDriverTeammates(nextDriverId);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/driver-teammates/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderDriverTeammates() {
    const elements = [];
    const { props } = this;
    const { teammates, surname } = props.driverTeammates.data;

    const teammatesSort = teammates.sort((a, b) => b.total - a.total);
    teammatesSort.forEach((item, idx) => {
      const qualClassNameDriver =
        parseInt(item.teammatePP) < parseInt(item.driverPP)
          ? "cell-better"
          : parseInt(item.teammatePP) > parseInt(item.driverPP)
          ? "cell-worse"
          : "";

      const qualClassNameTeammate =
        parseInt(item.teammatePP) > parseInt(item.driverPP)
          ? "cell-better"
          : parseInt(item.teammatePP) < parseInt(item.driverPP)
          ? "cell-worse"
          : "";

      const raceClassNameDriver =
        parseInt(item.teammateWins) < parseInt(item.driverWins)
          ? "cell-better"
          : parseInt(item.teammateWins) > parseInt(item.driverWins)
          ? "cell-worse"
          : "";

      const raceClassNameTeammate =
        parseInt(item.teammateWins) > parseInt(item.driverWins)
          ? "cell-better"
          : parseInt(item.teammateWins) < parseInt(item.driverWins)
          ? "cell-worse"
          : "";

      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;

      const gp = item.gp;
      const element = (
        <Segment basic key={idx}>
          <div className="driver-teammates-driver">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={`${item.name} ${item.surname}`}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/driver/${item.alias}`}>
                    <Flag name={item.countryCode} /> {item.name} {item.surname}
                  </NavLink>
                </div>
                <div>
                  <small>{item.seasons}</small>
                </div>
              </div>
            </div>
            <Statistic floated="right">
              <Statistic.Value>{item.total}</Statistic.Value>
              <Statistic.Label><FormattedMessage id={"app.stats.races"} /></Statistic.Label>
            </Statistic>
          </div>
          <Table
            basic="very"
            celled
            className="responsive-table center-aligned"
          >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell className="cell-default-size" rowSpan="2">
                  #
                </Table.HeaderCell>
                <Table.HeaderCell className="left" rowSpan="2">
                  Sezon
                </Table.HeaderCell>
                <Table.HeaderCell className="left" rowSpan="2">
                  <FormattedMessage id={"app.table.header.gp.long"} />
                </Table.HeaderCell>
                <Table.HeaderCell className="left" rowSpan="2">
                  <FormattedMessage id={"app.table.header.team"} />
                </Table.HeaderCell>
                <Table.HeaderCell colSpan="2"><FormattedMessage id={"app.stats.qual"} /></Table.HeaderCell>
                <Table.HeaderCell colSpan="2">Wyścig</Table.HeaderCell>
              </Table.Row>
              <Table.Row>
                <Table.HeaderCell className="driver-teammates-cell-size">
                  {item.surname}
                </Table.HeaderCell>
                <Table.HeaderCell className="driver-teammates-cell-size">
                  {surname}
                </Table.HeaderCell>
                <Table.HeaderCell className="driver-teammates-cell-size">
                  {item.surname}
                </Table.HeaderCell>
                <Table.HeaderCell className="driver-teammates-cell-size">
                  {surname}
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell
                  data-title="Lp"
                  className="no-wrap"
                  colSpan="4"
                ></Table.Cell>
                <Table.Cell
                  data-title="Kwalifikacje kierowca 1"
                  className={qualClassNameTeammate}
                >
                  {item.teammatePP}
                </Table.Cell>
                <Table.Cell
                  data-title="Kwalifikacje kierowca 2"
                  className={qualClassNameDriver}
                >
                  {item.driverPP}
                </Table.Cell>
                <Table.Cell
                  data-title="Wyścig kierowca 1"
                  className={raceClassNameTeammate}
                >
                  {item.teammateWins}
                </Table.Cell>
                <Table.Cell
                  data-title="Wyścig kierowca 2"
                  className={raceClassNameDriver}
                >
                  {item.driverWins}
                </Table.Cell>
              </Table.Row>
              {this.renderDriverTeammatesGP(gp)}
            </Table.Body>
          </Table>
        </Segment>
      );
      elements.push(element);
    });

    return elements;
  }

  renderDriverTeammatesGP(gp) {
    const elements = [];

    gp.forEach((item, idx) => {
      const qualClassNameDriver =
        parseInt(item.driverQualPos) < parseInt(item.teammateQualPos)
          ? "cell-better"
          : "cell-worse";

      const qualClassNameTeammate =
        parseInt(item.driverQualPos) > parseInt(item.teammateQualPos)
          ? "cell-better"
          : "cell-worse";

      const raceClassNameDriver =
        parseInt(item.driverPos) < parseInt(item.teammatePos)
          ? "cell-better"
          : "cell-worse";

      const raceClassNameTeammate =
        parseInt(item.driverPos) > parseInt(item.teammatePos)
          ? "cell-better"
          : "cell-worse";

      const countryPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Table.Row key={idx}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Sezon" className="no-wrap left bold">
            <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
              {item.raceDate}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
                  <Image
                    size="tiny"
                    src={countryPic}
                    alt={item.gpAlias}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
                    {item.gpName}{" "}
                  </NavLink>
                </div>
                <div>
                  <small>{item.circuit}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            {item.teamName} {item.teamEngine}
          </Table.Cell>
          <Table.Cell
            data-title="Kwalifikacje partner"
            className={qualClassNameTeammate}
          >
            {item.teammateQualPos}
          </Table.Cell>
          <Table.Cell
            data-title="Kwalifikacje kierowca"
            className={qualClassNameDriver}
          >
            {item.driverQualPos}
          </Table.Cell>
          <Table.Cell
            data-title="Wyścig partner"
            className={raceClassNameTeammate}
          >
            {item.teammateRaceCompleted == 1 ? item.teammatePos : "-"}
          </Table.Cell>
          <Table.Cell
            data-title="Wyścig kierowca"
            className={raceClassNameDriver}
          >
            {item.driverRaceCompleted == 1 ? item.driverPos : "-"}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });

    return elements;
  }

  renderDriverTeammatesMobile() {
    const elements = [];
    const { props } = this;
    const { teammates, surname } = props.driverTeammates.data;

    const teammatesSort = teammates.sort((a, b) => b.total - a.total);
    teammatesSort.forEach((item, idx) => {
      const qualClassName =
        parseInt(item.teammatePP) < parseInt(item.driverPP)
          ? "cell-better"
          : "cell-worse";

      const raceClassName =
        parseInt(item.teammateWins) < parseInt(item.driverWins)
          ? "cell-better"
          : "cell-worse";

      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;

      const gp = item.gp;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/driver/${item.alias}`}>
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={`${item.name} ${item.surname}`}
                  className="cell-photo"
                  onError={(e) => {
                    e.target.src =
                      "/build/images/drivers/driver_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="tiny">
                <Statistic.Value>{item.total}</Statistic.Value>
                <Statistic.Label><FormattedMessage id={"app.table.header.gp"} /></Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/driver/${item.alias}`}>
                      <Flag name={item.countryCode} /> {item.name}{" "}
                      {item.surname}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>{item.seasons}</Item.Description>
                  <Item.Description>
                    <table className="driver-teammates-gp">
                      <tr>
                        <td className="no-border"></td>
                        <td
                          colSpan="2"
                          className={`center-aligned ${qualClassName}`}
                        >
                          {item.teammatePP} : {item.driverPP}
                        </td>
                        <td
                          colSpan="2"
                          className={`center-aligned ${raceClassName}`}
                        >
                          {item.teammateWins} : {item.driverWins}
                        </td>
                      </tr>
                      <tr>
                        <th><FormattedMessage id={"app.table.header.gp.long"} /></th>
                        <th colSpan="2" className="center-aligned">
                          <FormattedMessage id={"app.table.header.qual"} />
                        </th>
                        <th colSpan="2" className="center-aligned">
                          <FormattedMessage id={"app.table.header.gp"} />
                        </th>
                      </tr>

                      {this.renderDriverTeammatesGPMobile(gp)}
                    </table>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverTeammatesGPMobile(gp) {
    const elements = [];

    gp.forEach((item, idx) => {
      const qualClassNameDriver =
        parseInt(item.driverQualPos) < parseInt(item.teammateQualPos)
          ? "cell-better"
          : "cell-worse";

      const qualClassNameTeammate =
        parseInt(item.driverQualPos) > parseInt(item.teammateQualPos)
          ? "cell-better"
          : "cell-worse";

      const raceClassNameDriver =
        parseInt(item.driverPos) < parseInt(item.teammatePos)
          ? "cell-better"
          : "cell-worse";

      const raceClassNameTeammate =
        parseInt(item.driverPos) > parseInt(item.teammatePos)
          ? "cell-better"
          : "cell-worse";

      const element = (
        <tr>
          <td>
            <div>
              <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
                {idx + 1}. {item.gpName}{" "}
              </NavLink>
            </div>
            <div>
              <small>{item.circuit}</small>
            </div>
            <div>
              <small>{item.raceDate}</small>
            </div>
          </td>
          <td className={`driver-teammates-gp-place ${qualClassNameTeammate}`}>
            {item.teammateQualPos}
          </td>
          <td className={`driver-teammates-gp-place ${qualClassNameDriver}`}>
            {item.driverQualPos}
          </td>
          <td className={`driver-teammates-gp-place ${raceClassNameTeammate}`}>
            {item.teammateRaceCompleted == 1 ? item.teammatePos : "-"}
          </td>
          <td className={`driver-teammates-gp-place ${raceClassNameDriver}`}>
            {item.driverRaceCompleted == 1 ? item.driverPos : "-"}
          </td>
        </tr>
      );
      elements.push(element);
    });

    return elements;
  }

  render() {
    const { props, state } = this;
    if (!props.driverTeammates.data || props.driverTeammates.loading) {
      return (
        <section
          id="driver-teammates"
          name="driver-teammates"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const {
      name,
      surname,
      id,
      alias,
      countryShort,
      prevId,
      nextId,
      teammates,
    } = props.driverTeammates.data;
    const picProfile = `/build/images/drivers/driver_${id}_profile.jpg`;

    const linkPrev = `/driver-teammates/${prevId}`;
    const linkNext = `/driver-teammates/${nextId}`;

    return (
      <section
        id="driver-teammates"
        name="driver-teammates"
        className="section-page"
      >
        <SectionPageBanner
          title={`${name} ${surname}`}
          subtitle="Partnerzy"
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column mobile={16} tablet={4} computer={3}>
                <div className="section-page-filters">
                  <Select
                    ref={(ref) => {
                      this.comboDriver = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="drivers"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectValue}
                    onChange={this.onInputChange}
                    options={props.comboDrivers.data}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
                    noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </div>
                <div className="stats-box">
                  <div>
                    <Grid centered>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <Segment basic textAlign="center">
                          <Image
                            className="driver-photo"
                            centered
                            src={picProfile}
                            alt={name}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </Segment>
                      </Grid.Column>
                      <Grid.Column
                        mobile={8}
                        tablet={16}
                        computer={16}
                        verticalAlign="middle"
                      >
                        <Image
                          className="driver-photo"
                          centered
                          src={`/build/images/countries/${countryShort}.jpg`}
                          alt={countryShort}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/countries/country_no_profile.jpg";
                          }}
                        />
                      </Grid.Column>
                    </Grid>
                    <Segment basic textAlign="center">
                      <Statistic size="large">
                        <Statistic.Value>{teammates.length}</Statistic.Value>
                        <Statistic.Label>Partnerzy</Statistic.Label>
                      </Statistic>
                    </Segment>
                  </div>
                  <div className="buttons">
                    <NavLink className="primary" to={`/driver/${alias}`}>
                      <FormattedMessage id={"app.button.profile"} />
                    </NavLink>
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment padded basic>
                  <div className="hideForDesktop">
                    <Grid columns={3} divided="vertically">
                      {this.renderDriverTeammatesMobile()}
                    </Grid>
                  </div>
                  <div className="hideForMobile">
                    {this.renderDriverTeammates()}
                  </div>
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

DriverTeammates.propTypes = {
  fetchDriverTeammates: PropTypes.func.isRequired,
  driverTeammates: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboDrivers: PropTypes.func.isRequired,
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  driverId: PropTypes.string,
};

function mapStateToProps({ driverTeammates, comboDrivers }) {
  return { driverTeammates, comboDrivers };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchDriverTeammates, fetchComboDrivers },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverTeammates);
