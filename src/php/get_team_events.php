<?
header('Access-Control-Allow-Origin: *');

$page=isset($_GET['page']) ? $_GET['page'] : null;
if ($page==null) $page=isset($_POST['page']) ? $_POST['page'] : null;
if ($page==null) $page=-1;
if ($page=='-') $page=-1;

$event=isset($_GET['event']) ? $_GET['event'] : null;
if ($event==null) $event=isset($_POST['event']) ? $_POST['event'] : null;
if ($event==null) $event=-1;
if ($event=='-') $event=-1;

$position=isset($_GET['pos']) ? $_GET['pos'] : null;
if ($position==null) $position=isset($_POST['pos']) ? $_POST['pos'] : null;
if ($position==null) $position=-1;
if ($position=='-') $position=-1;

$team=isset($_GET['team']) ? $_GET['team'] : null;
if ($team==null) $team=isset($_POST['team']) ? $_POST['team'] : null;
if ($team==null) $team=-1;
if ($team=='-') $team=-1;

$driver=isset($_GET['driver']) ? $_GET['driver'] : null;
if ($driver==null) $driver=isset($_POST['driver']) ? $_POST['driver'] : null;
if ($driver==null) $driver=-1;
if ($driver=='-') $driver=-1;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;
if ($gp==null) $gp=-1;
if ($gp=='-') $gp=-1;

$circuit=isset($_GET['circuit']) ? $_GET['circuit'] : null;
if ($circuit==null) $circuit=isset($_POST['circuit']) ? $_POST['circuit'] : null;
if ($circuit==null) $circuit=-1;
if ($circuit=='-') $circuit=-1;

$engine=isset($_GET['engine']) ? $_GET['engine'] : null;
if ($engine==null) $engine=isset($_POST['engine']) ? $_POST['engine'] : null;
if ($engine==null) $engine=-1;
if ($engine=='-') $engine=-1;

$model=isset($_GET['model']) ? $_GET['model'] : null;
if ($model==null) $model=isset($_POST['model']) ? $_POST['model'] : null;
if ($model==null) $model=-1;
if ($model=='-') $model=-1;

$tyre=isset($_GET['tyre']) ? $_GET['tyre'] : null;
if ($tyre==null) $tyre=isset($_POST['tyre']) ? $_POST['tyre'] : null;
if ($tyre==null) $tyre=-1;
if ($tyre=='-') $tyre=-1;

$season=isset($_GET['season']) ? $_GET['season'] : null;
if ($season==null) $season=isset($_POST['season']) ? $_POST['season'] : null;
if ($season==null) $season=-1;
if ($season=='-') $season=-1;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie id
$query="SELECT id_team id,name, team FROM teams WHERE alias_name='$team'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $teamName = $r["name"];
  $teamShortName = $r["team"];
}

$pages=1;
$records=1;
$limit=50;
$offset=(($page-1)*$limit);

$query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(gp_season.date,1,10) raceDate, 
gp.id_gp id,";
if ($lang=='pl') {
  $query_select.="gp.name AS gpname,";
}else{
  $query_select.="gp.name_en AS gpname,";
}
$query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
drivers_pp_results.qual_pos qual, 
drivers_pp_results.qual_completed qualCompleted,
drivers_pp_results.not_qualified notQualified,
drivers_pp_results.not_started notStarted,
drivers_sprint_results.sprint_pos sprintPos, 
drivers_sprint_results.sprint_completed sprintCompleted,
drivers_gp_starting_grid.grid_pos gridPos,
drivers_gp_results.race_pos race, 
drivers_gp_results.race_completed raceCompleted,
drivers_gp_results.disq disq,
drivers_gp_results.race_laps laps,
drivers_gp_results.race_best_lap bestLap,
COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
 if ($lang=='pl') {
  $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
  COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
  COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
  COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
}else{
  $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
  COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
  COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
  COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
}
$query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
COALESCE(drivers_gp_results.race_team_points,0) racePoints,
COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
(COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
teams_models.id_team_model idTeam,
teams_models.model,
teams_models.team_name modelName,
teams.alias_name teamAlias,
teams.name teamName,
teams.team,
teams.engine,
teams.country_code teamCountryCode,
tyres.name tyre,
drivers_gp_involvements.number
FROM drivers_gp_involvements 
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
$query_conditions="";
$query_order="";

//gp
if ($event=='gp') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//starty
if ($event=='starts') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//kwalifikacje
if ($event=='qual') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,";
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprint, drivers_sprint_results.sprint_completed sprintCompleted,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_pp_results.not_qualified notQualified,
  drivers_pp_results.not_started notStarted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  drivers_gp_results.race_best_lap bestLap,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" WHERE drivers_pp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_pp is not null AND (drivers_gp_results.co_driver=0 OR drivers_gp_results.co_driver is null)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//zwyciestwa
if ($event=='wins') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//2 miejsca
if ($event=='second') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=2";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//3 miejsca
if ($event=='third') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=3";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//4 miejsca
if ($event=='fourth') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=4";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";

}
//5 miejsca
if ($event=='five') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=5";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";

}
//6 miejsca
if ($event=='six') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=6";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";

}
//7 miejsca
if ($event=='seven') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=7";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";

}
//8 miejsca
if ($event=='eight') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=8";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";

}
//wyścigi miejsca
if ($event=='race-places') {
  $query_conditions.=" WHERE drivers_gp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos=$position";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//kwalifikacje miejsca
if ($event=='qual-places') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,";
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprint, drivers_sprint_results.sprint_completed sprintCompleted,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_pp_results.not_qualified notQualified,
  drivers_pp_results.not_started notStarted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  drivers_gp_results.race_best_lap bestLap,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number 
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" WHERE drivers_pp_results.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_pp_results.qual_completed=1 AND drivers_pp_results.qual_pos=$position  AND (drivers_gp_results.co_driver=0 OR drivers_gp_results.co_driver is null)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//pola startowe miejsca
if ($event=='grid-places') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,";
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprint, drivers_sprint_results.sprint_completed sprintCompleted,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_pp_results.not_qualified notQualified,
  drivers_pp_results.not_started notStarted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  drivers_gp_results.race_best_lap bestLap,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number 
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" WHERE drivers_gp_starting_grid.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  if (strpos($position,'>')!== false){
    $gridPos = substr($position,1);
    $query_conditions.=" AND drivers_gp_results.co_driver=0";
    $query_conditions.=" AND drivers_gp_starting_grid.grid_pos>$gridPos";
  }else{
    $query_conditions.=" AND drivers_gp_starting_grid.grid_pos=$position  AND drivers_gp_results.co_driver=0";
  }
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//punkty
if ($event=='points') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0)"; 
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//pp
if ($event=='polepos') {
  $query_conditions.=" WHERE drivers_gp_starting_grid.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//bez pp
if ($event=='polepos-no-win-gp-number') {
  $query_conditions.=" WHERE drivers_gp_starting_grid.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND (drivers_gp_starting_grid.is_pp=0 OR drivers_gp_starting_grid.id_starting_grid is null) AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_pp_results.qual_date NOT IN (SELECT DISTINCT dpp.qual_date FROM drivers_gp_starting_grid dsg, drivers_pp_results dpp WHERE dsg.is_pp=1 AND dsg.id_team=dpp.id_team
   AND dsg.id_driver=dpp.id_driver AND dsg.id_gp=dpp.id_gp AND dsg.season=dpp.season AND dsg.team=teams.team AND dpp.team=teams.team)"; 
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//no
if ($event=='bestlaps') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1"; 
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//podium
if ($event=='podium') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers_gp_results.race_pos, drivers.surname";
}
//miejsca punktowane
if ($event=='points-places') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//miejsca niepunktowane
if ($event=='no-points-places') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_involvements.season>1957 AND d.team=drivers_gp_results.team)";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//nie zakwalifikowany
if ($event=='not-qualified') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_pp_results.not_qualified=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//nie wystartowal
if ($event=='not-started') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_pp_results.not_started=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//sklasyfikowany
if ($event=='completed') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//niesklasyfikwoany
if ($event=='incomplete') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
// ukonczone
if ($event=='finished') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
// nieukonczone
if ($event=='retirement') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//z kara
if ($event=='penalty') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.penalty=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//dyskwalifikacje
if ($event=='disq') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.disq=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//pp + wygrane
if ($event=='polepos-wins') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND (drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//naj. okraz. + wygrane
if ($event=='bestlaps-wins') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_results.race_best_lap=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//pp + naj. okraz. + wygrane
if ($event=='polepos-bestlaps-wins') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.race_best_lap=1";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//bez wygranej
if ($event=='wins-no-win-gp-number') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos>1 AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_pos=1 AND d.team=drivers_gp_results.team)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//bez podium
if ($event=='podium-no-win-gp-number') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos>3 AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_pos<4 AND d.team=drivers_gp_results.team)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}
//bez najl. okrąż.
if ($event=='bestlaps-no-win-gp-number') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0 AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_best_lap=1 AND d.team=drivers_gp_results.team)";
	if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_order="  ORDER BY gp_season.date DESC, drivers.surname";
}

//wygrane z pierwszego pola startowego
if ($event=='wins-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//podium z pierwszego pola startowego
if ($event=='podium-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//punkty z pierwszego pola startowego
if ($event=='points-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sklasyfikowany z pierwszego pola startowego
if ($event=='completed-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//niesklasyfikowany z pierwszego pola startowego
if ($event=='incomplete-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//uk. wyścigi z pierwszego pola startowego
if ($event=='finished-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//nie uk. wyścigi z pierwszego pola startowego
if ($event=='retirement-from-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//wygrane spoza pierwszego pola startowego
if ($event=='wins-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//podium spoza pierwszego pola startowego
if ($event=='podium-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//punkty spoza pierwszego pola startowego
if ($event=='points-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver = 0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//uk. wyścigi spoza pierwszego pola startowego
if ($event=='completed-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//nie uk. wyścigi spoza pierwszego pola startowego
if ($event=='incomplete-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//uk. wyścigi spoza pierwszego pola startowego
if ($event=='finished-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//nie uk. wyścigi spoza pierwszego pola startowego
if ($event=='retirement-outside-polepos') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}

//wygrane z miejsca ...
if ($event=='wins-from-place' || $event=='race-win-lowest-grid') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//podium z miejsca ...
if ($event=='podium-from-place' || $event=='race-podium-lowest-grid') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//punkty z miejsca ...
if ($event=='points-from-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sklasyfikowany z miejsca ...
if ($event=='completed-from-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//niesklasyfikowany z miejsca ...
if ($event=='incomplete-from-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//ukonczone z miejsca ...
if ($event=='finished-from-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//nieukonczone z miejsca ...
if ($event=='retirement-from-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//uk. wyścigi tuz poza podium
if ($event=='race-fourth-place') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos=4 AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa
if ($event=='race-pos-higher-than-grid' || $event=='race-pos-higher-than-grid-percent') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//Najwięcej wyścigów ukończonych na pozycji nizszej niż startowa
if ($event=='race-pos-lower-than-grid' || $event=='race-pos-lower-than-grid-percent') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos AND drivers_gp_results.co_driver=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}


//sprinty
if ($event=='sprints') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sprinty wygrane
if ($event=='sprint-wins') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos=1";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sprinty podium
if ($event=='sprint-podiums') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos<4";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sprinty punkty
if ($event=='sprint-points') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_points>0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sprinty sklasyfikowany
if ($event=='sprint-completed') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=1";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
//sprinty nieslasyfikowany
if ($event=='sprint-incompleted') {
  $query_conditions.=" WHERE drivers_gp_involvements.id_team IN (SELECT id_team FROM teams WHERE alias_name='$team')";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=0";
  if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
  $query_group_by.=" GROUP BY gp_season.date";
  $query_order=" ORDER BY gp_season.date DESC, drivers.surname";
}
// sql
if ($gp!=-1) $query_conditions.=" AND gp.name_alias='$gp'";
if ($circuit!=-1) $query_conditions.=" AND gp.circuit_alias='$circuit'";
if ($driver!=-1) $query_conditions.=" AND drivers.alias='$driver'";
if ($engine!=-1) $query_conditions.=" AND (teams.engine='$engine' OR (teams.engine IS NULL AND teams.name='$engine')) AND alias_name='$team'";
if ($model!=-1) $query_conditions.=" AND teams_models.model='$model'";
if ($tyre!=-1) $query_conditions.=" AND drivers_gp_involvements.id_tyre=$tyre";


$query="$query_select$query_conditions$query_order";
$result = mysqli_query($dbhandle,$query);
$total=0;
$lastDate = "";
$lastRacePos = "";
while($r = mysqli_fetch_assoc($result)) {
  $records += 1;
  if ($event=='points' || $event=='xpoints-places' || $event=='xno-points-places' || $event=='not-qualified' || $event=='not-started'
  || $event=='completed' || $event=='incomplete' || $event=='finished' || $event=='retirement' || $event=='podium-outside-polepos' || $event=='penalty') {
    $total += 1;
  }else{
    if ($event=='podium' || $event=='disq') {
      if ($lastDate !== $r["raceDate"] || $lastRacePos !== $r["race"]){
        $total += 1;
        $lastDate = $r["raceDate"];
        $lastRacePos = $r["race"];
      }
    }else{
      if ($lastDate !== $r["raceDate"]){
        $total += 1;
        $lastDate = $r["raceDate"];
      }
    }
  }
}
$pages = ceil($records / $limit);

//$query="$query_select$query_conditions$query_order LIMIT $limit OFFSET $offset";
$query="$query_select$query_conditions$query_order";
$result = mysqli_query($dbhandle,$query);
$recordItems=array();
//$place = $offset;
$place = 0;
$lastDate = "";
$lastRacePos = "";
$cnt = 1;
while($r = mysqli_fetch_assoc($result)) {
  // if ($event=='starts' ||  $event=='qual' || $event=='wins' || $event=='second' || $event=='third' ||  $event=='qual' || $event=='completed' || $event=='incomplete') {
  //   if ($lastDate !== $r["raceDate"]){
  //     $place += 1;
  //     $lastDate = $r["raceDate"];
  //   }
  // }else{
  //   $place += 1;
  // }
  if ($event=='points' || $event=='xpoints-places' || $event=='xno-points-places' || $event=='not-qualified' || $event=='not-started' 
  || $event=='completed' || $event=='incomplete' || $event=='finished' || $event=='retirement' || $event=='podium-outside-polepos' || $event=='penalty') {
    $place += 1;
  }else{
    if ($event=='podium' || $event=='disq') {
      if ($lastDate !== $r["raceDate"] || $lastRacePos !== $r["race"]){
        $place += 1;
        $lastDate = $r["raceDate"];
        $lastRacePos = $r["race"];
      }
    }else{
      if ($lastDate !== $r["raceDate"]){
        $place += 1;
        $lastDate = $r["raceDate"];
      }
    }
  }
  if ($cnt <= $offset) {
    $cnt += 1;
    continue;
  }else{
    if ($cnt > ($limit * $page)) break;
    $r["place"] = $total + 1 - $place;
    $recordItems[] = $r;
    $cnt += 1;
  }
}


$recordsItems["event"]=$event;
$recordsItems["teamName"]=$teamName;
$recordsItems["teamShortName"]=$teamShortName;
$recordsItems["team"]=$team;
$recordsItems["driver"]=$driver;
$recordsItems["gp"]=$gp;
$recordsItems["circuit"]=$circuit;
$recordsItems["season"]=$season;
$recordsItems["pages"]=$pages;
$recordsItems["results"]=$recordItems;
$recordsItems["query"]=$query;
$recordsItems["total"]=$total;
$recordsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $recordsItems;

print json_encode($response);
mysqli_free_result($result);
?>
