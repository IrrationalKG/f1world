<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT ";
if ($lang=='pl') {
  $query.="text";
}else{
  $query.="text_en as text";
}  
$query.=",link FROM stats_trivia ORDER BY RAND( ) LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$items = array();
while($r = mysqli_fetch_assoc($result)) {
  $items[] = $r;
}
// $items["createTime"]=microtime(true)-$start_time;

// Response
$response = $items;

print json_encode($response);
mysqli_free_result($result);
?>
