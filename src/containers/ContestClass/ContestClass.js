/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderAwards","renderPlacesStats"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Table,
  Popup,
  Grid,
  Statistic,
  Label,
  Item,
  Divider,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchContestClass } from "../../actions/ContestClassActions";
import { fetchComboContestSeasons } from "../../actions/ComboContestSeasonsActions";
import { fetchContestAwards } from "../../actions/ContestAwardsActions";
import { fetchContestPlacesStats } from "../../actions/ContestPlacesStatsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import styles from "./ContestClass.less";

class ContestClass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchContestAwards();
    props.fetchContestClass(props.match.params.year);
    props.fetchContestPlacesStats();
    props.fetchComboContestSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboContestSeasons.data?.find(
          (e) => e.value == nextYear
        )?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchContestClass(nextYear);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/contest/class/${event.value}` });
    this.setState({
      selectValue: event,
    });
  }

  renderAwards(awardsContent, type) {
    const { formatMessage } = this.props.intl;

    const awardsItems = [];
    if (awardsContent) {
      awardsContent.forEach((award) => {
        let content = `${award.place}. ${formatMessage({
          id: "app.stats.wins.season",
        })} ${award.season}`;
        let awardColor = "";
        switch (award.place) {
          case "1":
            awardColor = "yellow";
            break;
          case "2":
            awardColor = "grey";
            break;
          case "3":
            awardColor = "brown";
            break;
          default:
            return "";
        }
        let awardItem;
        const key = `${type}_${award.season}_${award.place}`;
        if (award.season === "-1") {
          content = `${award.place}. ${formatMessage({
            id: "app.stats.wins.alltime",
          })}`;
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="trophy" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        } else {
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="star" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        }
        awardsItems.push(awardItem);
        return awardsItems;
      });
    }
    return awardsItems;
  }

  renderAwardsNormal(item) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[0];
      const awardsContent = data[item.id];
      return this.renderAwards(awardsContent, "normal");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderPlacesStats(item) {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    const { wins, second, third } = props.contestPlacesStats.data;
    const winsContent = wins[item.alias];
    const secondContent = second[item.alias];
    const thirdContent = third[item.alias];
    const placesStatsItems = [];
    if (winsContent) {
      const content = `${formatMessage({
        id: "app.stats.wins.round",
      })} ${winsContent}`;
      const firstPlaces = (
        <Label htmlFor="places-stats-wins" circular size="small" color="yellow">
          {winsContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`wins_${item.id}`}
          trigger={firstPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    if (secondContent) {
      const content = `${formatMessage({
        id: "app.stats.2nd.round",
      })} ${secondContent}`;
      const secondPlaces = (
        <Label htmlFor="places-stats-second" circular size="small" color="grey">
          {secondContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`second_${item.id}`}
          trigger={secondPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    if (thirdContent) {
      const content = `${formatMessage({
        id: "app.stats.3rd.round",
      })} ${thirdContent}`;
      const thirdPlaces = (
        <Label htmlFor="places-stats-third" circular size="small" color="brown">
          {thirdContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`third_${item.id}`}
          trigger={thirdPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    return placesStatsItems;
  }

  renderPoints() {
    const { props } = this;
    const { classification, maxYear } = props.contestClass.data;
    if (classification.length > 0) {
      const classItems = [];
      let place = 0;
      classification.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {place}
                  {". "}
                  {item.name}
                </NavLink>
                {maxYear === props.match.params.year &&
                  (item.diff > 0 ? (
                    <Icon color="green" name="caret up" />
                  ) : (
                    <Icon color="red" name="caret down" />
                  ))}
                {maxYear === props.match.params.year && item.diff}
              </Item.Header>
              <div className="meta">{this.renderAwardsNormal(item)}</div>
              <Item.Extra>
                <FormattedMessage id={"app.stats.gp"} />:{" "}
                <Label htmlFor={item.name}>
                  <Flag name="au" />
                  {item.points}
                  <Label.Detail>{item.name}</Label.Detail>
                </Label>
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right" size="small">
                <Statistic.Value>{item.points}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.points"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderClassMobile() {
    const { props } = this;
    const { classification, maxYear } = props.contestClass.data;
    if (classification.length > 0) {
      const classItems = [];
      let place = 0;
      classification.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>{" "}
                {maxYear === props.match.params.year &&
                  (item.diff > 0 ? (
                    <Icon color="green" name="caret up" />
                  ) : (
                    <Icon color="red" name="caret down" />
                  ))}
                {maxYear === props.match.params.year && item.diff}
              </Item.Header>
              <div className="meta">
                {this.renderAwardsNormal(item)}
                {this.renderPlacesStats(item)}
              </div>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right">
                <Statistic.Value>{item.points}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.points"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderClass() {
    const elements = [];
    const { props } = this;
    const { classification, maxYear, season } = props.contestClass.data;
    let cnt = 0;
    classification.forEach((item) => {
      cnt += 1;
      const cellDiff =
        item.diff != 0
          ? item.diff > 0
            ? "class-position-up"
            : "class-position-down"
          : "";
      const element = (
        <Table.Row key={`class-${item.id}`}>
          <Table.Cell data-title="Miejsce" className="bold">
            {cnt}.
          </Table.Cell>
          {maxYear === props.match.params.year && (
            <Table.Cell
              data-title="+/-"
              className={`center no-wrap ${cellDiff}`}
            >
              {item.diff > 0 && <Icon color="green" name="caret up" />}
              {item.diff < 0 && <Icon color="red" name="caret down" />}
              {item.diff == 0 && <>-</>}
              {item.diff != 0 ? item.diff : ""}
            </Table.Cell>
          )}
          <Table.Cell data-title="Gracz" className="no-wrap left">
            <NavLink
              to={`/contest/player/${item.alias}`}
              className="bold-uppercase"
            >
              {item.name}
            </NavLink>
            <div className="awards">
              {this.renderAwardsNormal(item)}
              {this.renderPlacesStats(item)}
            </div>
          </Table.Cell>
          {item.gp.map((e) => (
            <Table.Cell
              className={e.place < 4 ? `cell-${e.place}` : ""}
              data-title={e.gp}
              key={e.gp}
            >
              {e.points === "0" || e.points === "-" ? (
                "-"
              ) : (
                <NavLink to={`/contest/gprate/${season}/${e.alias}`}>
                  {e.points}
                </NavLink>
              )}
              <br />
              {e.place === "-" ? (
                String.fromCharCode(160)
              ) : (
                <small>
                  {" ("}
                  {e.place}
                  {".)"}
                </small>
              )}
            </Table.Cell>
          ))}
          <Table.Cell data-title="Punkty" className="bold">
            {item.points === null ? String.fromCharCode(160) : item.points}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.contestClass.data || props.contestClass.loading) {
      return (
        <section
          id="contest-class-details"
          name="contest-class-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { gp, maxYear, stats } = props.contestClass.data;

    const linkPrev = `/contest/class/${prevYear}`;
    const linkNext = `/contest/class/${nextYear}`;
    if (stats.players.length === 0) {
      return (
        <section
          id="contest-class-details"
          name="contest-class-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.contest.class.title"}
                values={{ year: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.contest.class.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage
                  id={"app.page.contest.class.info1"}
                  values={{ year: year }}
                />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="contest-class-details"
        name="contest-class-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.contest.class.title"}
              values={{ year: year }}
            />
          }
          subtitle={<FormattedMessage id={"app.page.contest.class.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <Segment basic padded>
                  <Select
                    ref={(ref) => {
                      this.comboContestSeason = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="seasons"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectValue}
                    onChange={this.onInputChange}
                    options={props.comboContestSeasons.data}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={formatMessage({
                      id: "app.placeholder.select.season",
                    })}
                    noResultsText={
                      <FormattedMessage id={"app.placeholder.no.results"} />
                    }
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </Segment>
                <SectionPageHeader
                  title={<FormattedMessage id={"app.stats.statistics"} />}
                  type="tertiary"
                />
                <div className="contest-class-stats">
                  <Segment basic>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points" inverted>
                        <Statistic.Value>
                          {stats.contest.players}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.players"} />
                        </Statistic.Label>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-gp" inverted>
                        <Statistic.Value>
                          {stats.contest.rounds}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.rounds"} />
                        </Statistic.Label>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-avg" inverted>
                        <Statistic.Value>
                          {Math.round(stats.contest.averagePlayers, 1)}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.avg.players"} />
                        </Statistic.Label>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-polepos" inverted>
                        <Statistic.Value>
                          {stats.contest.winners}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.winners"} />
                        </Statistic.Label>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-bestlaps" inverted>
                        <Statistic.Value>
                          {stats.contest.podium}
                        </Statistic.Value>
                        <Statistic.Label>
                          {<FormattedMessage id={"app.stats.on.podium"} />}
                        </Statistic.Label>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points" inverted>
                        <Statistic.Value>
                          {stats.players.maxPoints.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.most.points"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.maxPoints.alias}`}
                        >
                          {stats.players.maxPoints.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-gp" inverted>
                        <Statistic.Value>
                          {stats.players.maxPointsGP.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.most.points.gp"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.maxPointsGP.alias}`}
                        >
                          {stats.players.maxPointsGP.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-avg" inverted>
                        <Statistic.Value>
                          {stats.players.averagePoints &&
                            Math.round(stats.players.averagePoints.amount, 1)}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.avg.points"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.averagePoints.alias}`}
                        >
                          {stats.players.averagePoints &&
                            stats.players.averagePoints.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-polepos" inverted>
                        <Statistic.Value>
                          {Math.round(stats.players.averagePointsGP.amount, 1)}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.avg.points.gp"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.averagePointsGP.alias}`}
                        >
                          {stats.players.averagePointsGP.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-bestlaps" inverted>
                        <Statistic.Value>
                          {stats.players.maxRoundPoints.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage
                            id={"app.stats.most.points.in.round"}
                          />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.maxRoundPoints.alias}`}
                        >
                          {stats.players.maxRoundPoints.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points" inverted>
                        <Statistic.Value>
                          {stats.players.wins.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.wins"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.wins.alias}`}
                        >
                          {stats.players.wins.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-gp" inverted>
                        <Statistic.Value>
                          {stats.players.second.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.2nd"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.second.alias}`}
                        >
                          {stats.players.second.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="player-points-avg" inverted>
                        <Statistic.Value>
                          {stats.players.third.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.3rd"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.third.alias}`}
                        >
                          {stats.players.third.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-polepos" inverted>
                        <Statistic.Value>
                          {stats.players.podium.amount}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.podium"} />
                        </Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.podium.alias}`}
                        >
                          {stats.players.podium.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                    <Segment basic textAlign="center">
                      <Statistic size="large" id="driver-bestlaps" inverted>
                        <Statistic.Value>
                          {stats.players.top10.amount}
                        </Statistic.Value>
                        <Statistic.Label>TOP 10</Statistic.Label>
                        <NavLink
                          to={`/contest/player/${stats.players.top10.alias}`}
                        >
                          {stats.players.top10.name}
                        </NavLink>
                      </Statistic>
                      <Divider></Divider>
                    </Segment>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment basic>
                  <div className="buttons">
                    <NavLink to={`/contest/class/${year}`} className="primary">
                      <FormattedMessage id={"app.stats.class.standard"} />
                    </NavLink>
                    <NavLink
                      to={`/contest/class-gp/${year}`}
                      className="secondary"
                    >
                      <FormattedMessage id={"app.stats.class.gp"} />
                    </NavLink>
                  </div>
                  <Segment padded basic>
                    <div className="class-normal">
                      <SectionPageHeader
                        title={`${formatMessage({
                          id: "app.stats.season",
                        })} ${year}`}
                        type="secondary"
                      />
                      <Segment basic className="class-mobile">
                        <Item.Group divided unstackable>
                          {this.renderClassMobile()}
                        </Item.Group>
                      </Segment>
                      <Segment basic className="class-desktop overflow">
                        <Table
                          basic="very"
                          celled
                          className="responsive-table center-aligned"
                        >
                          <Table.Header>
                            <Table.Row>
                              <Table.HeaderCell>#</Table.HeaderCell>
                              {maxYear === year && (
                                <Table.HeaderCell>+/-</Table.HeaderCell>
                              )}
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.player"}
                                />
                              </Table.HeaderCell>
                              {gp.map((item) => (
                                <Table.HeaderCell
                                  data-title={item.gp}
                                  key={item.gp}
                                >
                                  <span>
                                    {item.name}
                                    <div>
                                      <Flag name={item.name} />
                                    </div>
                                  </span>
                                </Table.HeaderCell>
                              ))}
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.points"}
                                />
                              </Table.HeaderCell>
                            </Table.Row>
                          </Table.Header>
                          <Table.Body>{this.renderClass()}</Table.Body>
                        </Table>
                      </Segment>
                    </div>
                  </Segment>
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

ContestClass.propTypes = {
  fetchContestAwards: PropTypes.func.isRequired,
  fetchContestPlacesStats: PropTypes.func.isRequired,
  fetchContestClass: PropTypes.func.isRequired,
  contestClass: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboContestSeasons: PropTypes.func.isRequired,
  comboContestSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  awards: PropTypes.object,
  contestPlacesStats: PropTypes.object,
};

function mapStateToProps({
  contestClass,
  comboContestSeasons,
  awards,
  contestPlacesStats,
}) {
  return {
    contestClass,
    comboContestSeasons,
    awards,
    contestPlacesStats,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchContestClass,
      fetchComboContestSeasons,
      fetchContestAwards,
      fetchContestPlacesStats,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(ContestClass)
);
