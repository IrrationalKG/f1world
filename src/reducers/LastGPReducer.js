import { FETCH_LASTGP, FETCH_LASTGP_SUCCESS, FETCH_LASTGP_ERROR } from '../actions/LastGPActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_LASTGP:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_LASTGP_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_LASTGP_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
