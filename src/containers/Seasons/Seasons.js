/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Flag,
  Grid,
  Loader,
  Item,
  Header,
  Divider,
  Statistic,
  Label,
  Message,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchSeasons } from "../../actions/SeasonsActions";
import { FormattedMessage } from "react-intl";
import styles from "./Seasons.less";

class Seasons extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderSeasons = this.renderSeasons.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchSeasons();
  }

  renderSeasons() {
    const items = [];
    const { props } = this;
    const { seasons } = props.seasons.data;
    seasons.forEach((item) => {
      const {
        season,
        winner,
        winnerTeam,
        drivers,
        teams,
        gps,
        winnersCount,
        podiumsCount,
        ppCount,
        blCount,
        pointsCount,
      } = item;
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (winner.picture === "1") {
        filename = `/build/images/drivers/driver_${winner.id}_profile.jpg`;
      }
      let filename2 = "";
      if (winnerTeam?.points_class) {
        filename2 = `/build/images/teams/team_${winnerTeam?.team}_logo.jpg`;
      }
      const detailsLink = `/gp-season/${season}`;
      const record = (
        <Grid.Column
          key={season}
          textAlign="center"
          mobile={8}
          tablet={4}
          computer={2}
          className="menu-box-container"
        >
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Segment basic>
                  <NavLink className="link" to={detailsLink}>
                    <Image
                      centered
                      src={filename}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/drivers/driver_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  <Label attached="top left" color="red" size="large">
                    &nbsp;{season}&nbsp;
                  </Label>
                </Segment>
                <Segment basic className="menu-box-flex">
                  <Divider hidden fitted></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <Flag name={winner.countryCode} />
                      <NavLink to={`/driver/${winner.alias}`}>
                        {winner.name}
                      </NavLink>
                      <Header.Subheader>
                        <FormattedMessage id={"app.stats.wc"} /> {season}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider></Divider>
                  <NavLink
                    to={`driver-events/wins/${winner.alias}/-/-/-/-/-/-/${season}/1/`}
                  >
                    <Statistic size="large">
                      <Statistic.Value>
                        {Math.round((winner.wins * 100) / winner.races, 0)}{" "}
                        <small>%</small>
                      </Statistic.Value>
                      <Statistic.Label>
                        <FormattedMessage id={"app.stats.gp.won"} />
                      </Statistic.Label>
                    </Statistic>
                  </NavLink>
                  <Divider hidden></Divider>
                  {winnerTeam?.points_class > 0 ? (
                    <>
                      <NavLink className="link" to={detailsLink}>
                        <Image
                          centered
                          src={filename2}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_profile.jpg";
                          }}
                        />
                      </NavLink>
                      <Header as="h2">
                        <Header.Content>
                          <NavLink to={`/team/${winnerTeam?.alias}`}>
                            {winnerTeam?.name}
                          </NavLink>
                        </Header.Content>
                      </Header>
                      <Header as="h2">
                        <Header.Content>
                          <Header.Subheader>
                            <FormattedMessage id={"app.stats.wc"} /> {season}
                          </Header.Subheader>
                        </Header.Content>
                      </Header>
                    </>
                  ) : (
                    <Segment basic>
                      <Message className="transparent">
                        <p>
                          <FormattedMessage id={"app.page.seasons.info"} />
                        </p>
                      </Message>
                    </Segment>
                  )}
                  <Divider hidden fitted></Divider>
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="left" className="info-box-light">
                <Item.Group divided>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.gp"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink to={`/gp-season/${season}`}>{gps}</NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.drivers"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/starts/-/-/-/${season}/${season}/0/1/`}
                        >
                          {drivers}
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.teams"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/teams-records/starts/-/-/${season}/${season}/0/1/`}
                        >
                          {teams}
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.winners.number"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/wins/-/-/-/${season}/${season}/0/1/`}
                        >
                          {winnersCount} (
                          {Math.round((winnersCount * 100) / drivers, 0)}%)
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.on.podium.number"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/podium/-/-/-/${season}/${season}/0/1/`}
                        >
                          {podiumsCount} (
                          {Math.round((podiumsCount * 100) / drivers, 0)}%)
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.on.polepos.number"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/polepos/-/-/-/${season}/${season}/0/1/`}
                        >
                          {ppCount} ({Math.round((ppCount * 100) / drivers, 0)}
                          %)
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage
                          id={"app.stats.with.bestlap.number"}
                        />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/bestlaps/-/-/-/${season}/${season}/0/1/`}
                        >
                          {blCount} ({Math.round((blCount * 100) / drivers, 0)}
                          %)
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content>
                      <Item.Header>
                        <FormattedMessage id={"app.stats.with.points.number"} />
                      </Item.Header>
                      <Item.Meta>
                        <NavLink
                          to={`/drivers-records/points/-/-/-/${season}/${season}/0/1/`}
                        >
                          {pointsCount} (
                          {Math.round((pointsCount * 100) / drivers, 0)}%)
                        </NavLink>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                </Item.Group>
                <div className="buttons">
                  <NavLink className="primary" to={detailsLink}>
                    <FormattedMessage id={"app.button.details"} />
                  </NavLink>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      );
      items.push(record);
    });
    return items;
  }

  renderSeasonsLabels() {
    const items = [];
    const { props } = this;
    const { seasons } = props.seasons.data;
    seasons.forEach((item) => {
      const record = (
        <Label image key={item.season}>
          <NavLink to={`/gp-season/${item.season}`}>{item.season}</NavLink>
          <Label.Detail>{item.gps}</Label.Detail>
        </Label>
      );
      items.push(record);
    });
    return items;
  }

  render() {
    const { seasons } = this.props;
    if (!seasons.data || seasons.loading) {
      return (
        <section id="seasons" name="seasons" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    return (
      <section id="seasons" name="seasons" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.seasons.title"} />}
          subtitle={<FormattedMessage id={"app.page.seasons.subtitle"} />}
        />
        <div className="section-page-content">
          <Segment
            padded
            basic
            className="seasons-labels hideMobile hideTablet"
          >
            {this.renderSeasonsLabels()}
          </Segment>
          <Grid>
            {this.renderSeasons()}
          </Grid>
        </div>
      </section>
    );
  }
}
Seasons.propTypes = {
  fetchSeasons: PropTypes.func.isRequired,
  seasons: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ seasons }) {
  return { seasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchSeasons }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Seasons);
