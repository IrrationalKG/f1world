/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu","renderTeamSubmenu","renderCircuitSubmenu","renderResultsSubmenu","renderClassificationsSubmenu","renderContestSubmenu","renderHistorySubmenu","initSmartmenus"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Segment,
  Label,
  Flag,
  Image,
  Grid,
  Header,
  Divider,
  Item,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import { fetchMenu } from "../../actions/MenuActions";
import Logo from "../../components/Logo/Logo";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Menu.less";

class Menu extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderMenu = this.renderMenu.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    const { year, gp } = props;
    props.fetchMenu(year, gp, Cookies.get("lang"));
  }

  componentDidMount() {
    this.initSmartmenus();
  }

  initSmartmenus() {
    // SmartMenus init
    $("#main-menu").smartmenus({
      subMenusSubOffsetX: 1,
      subMenusSubOffsetY: -8,
      keepInViewport: false,
    });

    // SmartMenus mobile menu toggle button
    const $mainMenuState = $("#main-menu-state");
    if ($mainMenuState.length) {
      // animate mobile menu
      $mainMenuState.on("change", function () {
        const $menu = $("#main-menu");
        if (this.checked) {
          $menu.hide().slideDown(250, () => {
            $menu.css("display", "");
          });
        } else {
          $menu.show().slideUp(250, () => {
            $menu.css("display", "");
          });
        }
      });
      // hide mobile menu beforeunload
      $(window).bind("beforeunload unload", () => {
        if ($mainMenuState[0].checked) {
          $mainMenuState[0].click();
        }
      });
      $("#main-menu").on("click.smapi", (e) => {
        // check namespace if you need to differentiate from a regular DOM event fired inside the menu tree
        if ($(e.target).hasClass("menu-link")) {
          if ($mainMenuState[0].checked) {
            $mainMenuState[0].click();
          }
        }
      });
    }
  }

  renderDriverSubmenu(item) {
    const { props } = this;
    const { year } = props;
    const {
      id,
      picture,
      alias,
      name,
      surname,
      number,
      team_color,
      seasonPlace,
      seasonPoints,
      country,
      team,
      bestResult,
      wins,
      points,
      podium,
      starts,
      polePosition,
      bestLaps,
      seasons,
    } = item;
    const detailsLink = `/driver/${year}/${alias}`;
    let filename = "/build/images/drivers/driver_no_profile.jpg";
    if (picture === "1") {
      filename = `/build/images/drivers/driver_${id}_profile.jpg`;
    }
    return (
      <Grid.Column
        key={id}
        textAlign="center"
        mobile={8}
        tablet={4}
        computer={2}
        className="menu-box-container"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Segment basic>
                <Image
                  fluid
                  label={{
                    corner: "left",
                    content: `#${number}`,
                    size: "large",
                    horizontal: true,
                    className: `${team_color}`,
                  }}
                />
                <NavLink to={detailsLink}>
                  <Image
                    centered
                    src={filename}
                    alt={`${name} ${surname}`}
                    className="menu-link"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
                <Label attached="bottom" color="red">
                  {bestResult == "Debiutant" ? (
                    <FormattedMessage id={"app.stats.debut"} />
                  ) : bestResult.includes("MŚ") ? (
                    <>
                      {bestResult.substring(0, 2)}{" "}
                      <FormattedMessage id={"app.stats.wc"} />
                    </>
                  ) : (
                    bestResult
                  )}
                </Label>
              </Segment>
              <Segment basic className="menu-box-flex">
                <Divider hidden fitted></Divider>
                <Header as="h2">
                  <Header.Content>
                    <Flag name={country} /> {name} {surname}
                    <Header.Subheader>{team}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider></Divider>
                <Header as="h3">
                  <Header.Content>
                    <FormattedMessage id={"app.stats.season"} /> {props.year}
                    <Header.Subheader>
                      {seasonPlace}
                      {". ("}
                      {seasonPoints} <FormattedMessage id={"app.stats.pts"} />
                      {")"}
                    </Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider hidden fitted></Divider>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign="left" className="info-box-light">
              <Item.Group divided>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.seasons"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/starts/${alias}`}
                        >
                          {seasons}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.gp"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/starts/${alias}`}
                        >
                          {starts}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.wins"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/wins/${alias}`}
                        >
                          {wins}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.points"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/points/${alias}`}
                        >
                          {points}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.podium"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/podium/${alias}`}
                        >
                          {podium}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.polepos"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/polepos/${alias}`}
                        >
                          {polePosition}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.bestlaps"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/driver-events/bestlaps/${alias}`}
                        >
                          {bestLaps}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
              </Item.Group>
              <div className="buttons">
                <NavLink className="details menu-link" to={detailsLink}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    );
  }

  renderTeamSubmenu(item, num) {
    const { props } = this;
    const { year } = props;
    const {
      id,
      picture,
      aliasName,
      name,
      color,
      seasonPlace,
      seasonPoints,
      country,
      engine,
      bestResult,
      wins,
      points,
      podium,
      starts,
      polePosition,
      bestLaps,
      seasons,
    } = item;
    const detailsLink = `/team/${year}/${aliasName}`;
    let filename = "/build/images/teams/team_no_profile.jpg";
    if (picture === "1") {
      filename = `/build/images/teams/team_${id}_profile.jpg`;
    }
    let filename2 = `/build/images/teams/team_${id}_logo.jpg`;
    return (
      <Grid.Column
        key={id}
        textAlign="center"
        mobile={8}
        tablet={4}
        computer={2}
        className="menu-box-container"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Segment basic>
                <Image
                  fluid
                  label={{
                    corner: "left",
                    content: `${num}`,
                    size: "large",
                    horizontal: true,
                    className: `${color}`,
                  }}
                />
                <NavLink to={detailsLink}>
                  <Image
                    centered
                    src={filename}
                    alt={name}
                    className="menu-link"
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
                <Label attached="bottom" color="red">
                  {bestResult == "Debiutant" ? (
                    <FormattedMessage id={"app.stats.debut"} />
                  ) : bestResult.includes("MŚ") ? (
                    <>
                      {bestResult.substring(0, bestResult.indexOf("x") + 1)}{" "}
                      <FormattedMessage id={"app.stats.wc"} />
                    </>
                  ) : (
                    bestResult
                  )}
                </Label>
                <Image
                  centered
                  src={filename2}
                  onError={(e) => {
                    e.target.src = "/build/images/teams/team_no_profile.jpg";
                  }}
                />
              </Segment>
              <Segment basic className="menu-box-flex">
                <Divider hidden fitted></Divider>
                <Header as="h2">
                  <Header.Content>
                    <Flag name={country} /> {name}
                    <Header.Subheader>{engine}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider></Divider>
                <Header as="h3">
                  <Header.Content>
                    <FormattedMessage id={"app.stats.season"} /> {props.year}
                    <Header.Subheader>
                      {seasonPlace}
                      {". ("}
                      {seasonPoints} <FormattedMessage id={"app.stats.pts"} />
                      {")"}
                    </Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider hidden fitted></Divider>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign="left" className="info-box-light">
              <Item.Group divided>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.seasons"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/starts/${aliasName}`}
                        >
                          {seasons}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.gp"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/starts/${aliasName}`}
                        >
                          {starts}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.wins"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/wins/${aliasName}`}
                        >
                          {wins}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.points"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/points/${aliasName}`}
                        >
                          {points}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.podium"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/podium/${aliasName}`}
                        >
                          {podium}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.polepos"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/polepos/${aliasName}`}
                        >
                          {polePosition}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.bestlaps"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        <NavLink
                          className="link"
                          to={`/team-events/bestlaps/${aliasName}`}
                        >
                          {bestLaps}
                        </NavLink>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
              </Item.Group>
              <div className="buttons">
                <NavLink className="details menu-link" to={detailsLink}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    );
  }

  renderCircuitSubmenu(item, num) {
    const { props } = this;
    const { year } = props;
    const {
      picture,
      alias,
      name,
      country,
      circuit,
      track,
      date,
      build,
      laps,
      length,
      distance,
      winner,
      mostWins,
      mostPP,
      mostPoints,
      mostBL,
      mostPodiums,
      sprint,
      isNextGP,
      seasons,
    } = item;
    const detailsLink = `/circuit/${year}/${alias}`;
    let filename = "/build/images/circuits/circuit_no_profile.jpg";
    if (picture === "1") {
      filename = `/build/images/circuits/circuit_${alias}_profile.jpg`;
    }
    let labelColor = winner != "nieznany" ? "grey" : "yellow";
    if (isNextGP === "1") {
      labelColor = "red";
    }
    return (
      <Grid.Column
        textAlign="center"
        key={item.alias}
        mobile={8}
        tablet={4}
        computer={2}
        className="menu-box-container"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Segment basic>
                <Image
                  fluid
                  label={{
                    color: `${labelColor}`,
                    corner: "left",
                    content: `${num}`,
                    size: "large",
                    horizontal: true,
                  }}
                />
                <NavLink to={detailsLink}>
                  <Image
                    centered
                    src={filename}
                    alt={`${circuit} - ${track}`}
                    className="menu-link"
                  />
                </NavLink>
                {sprint == 1 && (
                  <Label attached="top right">
                    <FormattedMessage id={"app.stats.sprint"} />
                  </Label>
                )}
                <Label attached="bottom" color="red">
                  {winner != "nieznany" ? (
                    winner
                  ) : (
                    <FormattedMessage id={"app.stats.unknown"} />
                  )}
                </Label>
              </Segment>
              <Segment basic className="menu-box-flex">
                <Divider hidden fitted></Divider>
                <Header as="h2">
                  <Header.Content className="content-text">
                    <Flag name={country} />
                    {name}
                    <Header.Subheader>{circuit}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider></Divider>
                <Header as="h3">
                  <Header.Content>
                    <FormattedMessage id={"app.stats.debut"} />
                    <Header.Subheader>{build}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider></Divider>
                <Header as="h3">
                  <Header.Content>
                    {track}
                    <Header.Subheader>{date}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider hidden fitted></Divider>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign="left" className="info-box-light">
              <Item.Group divided>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.gp"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{seasons}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.laps"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{laps}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.length"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {length} <small>km</small>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.distance"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {distance} <small>km</small>
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.wins"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{mostWins}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.points"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{mostPoints}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.podium"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{mostPodiums}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.polepos"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{mostPP}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.bestlaps"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>{mostBL}</span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
              </Item.Group>
              <div className="buttons">
                <NavLink className="details menu-link" to={detailsLink}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    );
  }

  renderResultsSubmenu(item, num) {
    const { props } = this;
    const {
      gp,
      alias,
      name,
      circuit,
      date,
      winner,
      second,
      third,
      pp,
      bl,
      sprint,
      sprint_winner,
      isNextGP,
    } = item;
    const detailsLink = `/gp-result/${alias}/${props.year}`;
    const filename = `/build/images/countries/${gp}.jpg`;
    let labelColor = winner != "nieznany" ? "grey" : "yellow";
    if (isNextGP === "1") {
      labelColor = "red";
    }
    return (
      <Grid.Column
        textAlign="center"
        key={alias}
        mobile={8}
        tablet={4}
        computer={2}
        className="menu-box-container"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Segment basic>
                <Image
                  fluid
                  label={{
                    color: `${labelColor}`,
                    corner: "left",
                    content: `${num}`,
                    size: "large",
                    horizontal: true,
                  }}
                />
                <NavLink to={detailsLink}>
                  <Image
                    centered
                    src={filename}
                    className="image-max-width menu-link"
                  />
                  {sprint == 1 && (
                    <Label attached="top right">
                      <FormattedMessage id={"app.stats.sprint"} />
                    </Label>
                  )}
                </NavLink>
              </Segment>
              <Segment basic className="menu-box-flex">
                <Divider hidden fitted></Divider>
                <Header as="h2">
                  <Header.Content>
                    {name}
                    <Header.Subheader>{circuit}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider></Divider>
                <Header as="h3">
                  <Header.Content>
                    <FormattedMessage id={"app.stats.date"} />
                    <Header.Subheader>{date}</Header.Subheader>
                  </Header.Content>
                </Header>
                <Divider hidden fitted></Divider>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign="left" className="info-box-light">
              <Item.Group divided>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.p1"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {winner != "nieznany" ? (
                          winner
                        ) : (
                          <FormattedMessage id={"app.stats.unknown"} />
                        )}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.p2"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {second ? (
                          second
                        ) : (
                          <FormattedMessage id={"app.stats.unknown"} />
                        )}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.p3"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {third ? (
                          third
                        ) : (
                          <FormattedMessage id={"app.stats.unknown"} />
                        )}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.pp"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {pp ? (
                          pp
                        ) : (
                          <FormattedMessage id={"app.stats.unknown"} />
                        )}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.bestlap"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {bl ? (
                          bl
                        ) : (
                          <FormattedMessage id={"app.stats.unknown"} />
                        )}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
                <Item>
                  <Item.Content verticalAlign="middle">
                    <Item.Header>
                      <FormattedMessage id={"app.stats.sprint"} />
                    </Item.Header>
                    <Item.Meta>
                      <span>
                        {sprint == "1" &&
                          (sprint_winner ? (
                            sprint_winner
                          ) : (
                            <FormattedMessage id={"app.stats.unknown"} />
                          ))}
                        {sprint == "0" && "-"}
                      </span>
                    </Item.Meta>
                  </Item.Content>
                </Item>
              </Item.Group>
              <div className="buttons">
                <NavLink className="details menu-link" to={detailsLink}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Grid.Column>
    );
  }

  renderClassificationsSubmenu(item) {
    const { props } = this;
    const { id, name, subtitle, photo } = item;
    const detailsLink = `/classification/${id}/${props.year}`;
    return (
      <Grid.Column
        textAlign="center"
        verticalAlign="top"
        key={id}
        mobile={8}
        tablet={4}
        computer={2}
      >
        <Segment basic>
          <Grid centered columns="equal">
            <Grid.Column textAlign="center" verticalAlign="top">
              <NavLink to={detailsLink}>
                <div
                  id={`class-submenu-img-${id}`}
                  className={`avatar bg-${photo} menu-link`}
                />
              </NavLink>
              <Header as="h2" inverted>
                <Header.Content>
                  <FormattedMessage id={`app.menu.${name}`} />
                </Header.Content>
                <Header.Subheader>
                  <FormattedMessage id={`app.menu.${subtitle}`} />
                </Header.Subheader>
              </Header>
              <br />
              <div className="ui center aligned">
                <NavLink to={detailsLink} className="details menu-link">
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
              <br />
            </Grid.Column>
          </Grid>
        </Segment>
      </Grid.Column>
    );
  }

  renderHistorySubmenu(item) {
    const { id, name, subtitle, photo } = item;
    const detailsLink = `/${id}`;
    return (
      <Grid.Column
        textAlign="center"
        key={id}
        mobile={8}
        tablet={4}
        computer={2}
      >
        <Segment basic>
          <Grid stackable centered columns="equal">
            <Grid.Column textAlign="center" verticalAlign="top">
              <NavLink to={detailsLink}>
                <div
                  id={`history-submenu-img-${id}`}
                  className={`avatar bg-${photo} menu-link`}
                />
              </NavLink>
              <Header as="h2" inverted>
                <Header.Content>
                  <FormattedMessage id={`app.menu.${name}`} />
                </Header.Content>
                <Header.Subheader>
                  <FormattedMessage id={`app.menu.${subtitle}`} />
                </Header.Subheader>
              </Header>
              <br />
              <div className="ui center aligned">
                <NavLink to={detailsLink} className="details menu-link">
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
              <br />
            </Grid.Column>
          </Grid>
        </Segment>
      </Grid.Column>
    );
  }

  renderContestSubmenu(item) {
    const { id, name, subtitle, photo } = item;
    const detailsLink = `/contest/${id}`;
    return (
      <Grid.Column
        textAlign="center"
        key={id}
        mobile={8}
        tablet={4}
        computer={2}
      >
        <Segment basic>
          <Grid stackable centered columns="equal">
            <Grid.Column textAlign="center" verticalAlign="top">
              <NavLink to={detailsLink}>
                <div
                  id={`contest-submenu-img-${id}`}
                  className={`avatar bg-${photo} menu-link`}
                />
              </NavLink>
              <Header as="h2" inverted>
                <Header.Content>
                  <FormattedMessage id={`app.menu.${name}`} />
                </Header.Content>
                <Header.Subheader>
                  <FormattedMessage id={`app.menu.${subtitle}`} />
                </Header.Subheader>
              </Header>
              <br />
              <div className="ui center aligned">
                <NavLink to={detailsLink} className="details menu-link">
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
              <br />
            </Grid.Column>
          </Grid>
        </Segment>
      </Grid.Column>
    );
  }

  renderSubmenu(item, num) {
    const { type } = item;
    let retVal = "";
    switch (type) {
      case "drivers":
        retVal = this.renderDriverSubmenu(item);
        break;
      case "teams":
        retVal = this.renderTeamSubmenu(item, num);
        break;
      case "circuits":
        retVal = this.renderCircuitSubmenu(item, num);
        break;
      case "results":
        retVal = this.renderResultsSubmenu(item, num);
        break;
      case "classifications":
        retVal = this.renderClassificationsSubmenu(item);
        break;
      case "history":
        retVal = this.renderHistorySubmenu(item);
        break;
      case "contest":
        retVal = this.renderContestSubmenu(item);
        break;
      default:
        return "";
    }
    return retVal;
  }

  renderMenu() {
    const { props } = this;
    const menuItems = [];
    const { data } = props.menu;
    if (data) {
      data.forEach((item) => {
        const menuItem = (
          <li key={item.id} className="has-mega-menu">
            <NavLink
              to={`/${item.id}-list/${props.year}`}
              className="hvr-overline-reveal"
            >
              <FormattedMessage id={`app.menu.${item.title}`} />
            </NavLink>
            <ul className="mega-menu fadeIn">
              <li>
                <Grid key={item.id} centered>
                  {item.items.length > 0 &&
                    item.items.map((e, idx) => this.renderSubmenu(e, idx + 1))}
                </Grid>
              </li>
            </ul>
          </li>
        );
        menuItems.push(menuItem);
      });
    }
    return menuItems;
  }

  render() {
    return (
      <nav className="main-nav">
        <input id="main-menu-state" type="checkbox" />
        <label className="main-menu-btn" htmlFor="main-menu-state">
          <span className="main-menu-btn-icon" />
        </label>
        <div className="nav-brand">
          <Logo />
        </div>
        <ul id="main-menu" className="sm sm-mint">
          {this.renderMenu()}
        </ul>
      </nav>
    );
  }
}

Menu.propTypes = {
  fetchMenu: PropTypes.func.isRequired,
  params: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  menu: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  gp: PropTypes.string,
};

function mapStateToProps({ menu }) {
  return { menu };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
