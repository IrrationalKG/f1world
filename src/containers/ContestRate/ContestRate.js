/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["render","initPPCountdown","initGPCountdown"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Button,
  Segment,
  Message,
  Label,
  Grid,
  Divider,
  Form,
  Loader,
  Statistic,
  Image,
  Header,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import ContestCountdown from "../ContestCountdown/ContestCountdown";
import { saveContestRate } from "../../actions/ContestRateActions";
import { fetchComboPlayers } from "../../actions/ComboPlayersActions";
import { fetchComboContestDrivers } from "../../actions/ComboContestDriversActions";
import { Adsense } from "@ctrl/react-adsense";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./ContestRate.less";

class ContestRateForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      newEntry: true,
      intervalQual: 0,
      intervalRace: 0,
      qualActive: true,
      raceActive: true,
      qualRatesChecked: true,
      raceRatesChecked: true,
      overwrite: false,
      player: "",
      password: "",
      pp: "",
      p1: "",
      p2: "",
      p3: "",
      p4: "",
      p5: "",
      p6: "",
      p7: "",
      p8: "",
      p9: "",
      p10: "",
      sendDate: "",
      playerError: "",
      passwordError: "",
      ppError: "",
      p1Error: "",
      p2Error: "",
      p3Error: "",
      p4Error: "",
      p5Error: "",
      p6Error: "",
      p7Error: "",
      p8Error: "",
      p9Error: "",
      p10Error: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleOverwriteSubmit = this.handleOverwriteSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleQualChange = this.handleQualChange.bind(this);
    this.handleRaceChange = this.handleRaceChange.bind(this);
    this.validatePlayer = this.validatePlayer.bind(this);
    this.validatePPDriver = this.validatePPDriver.bind(this);
    this.validateP1Driver = this.validateP1Driver.bind(this);
    this.validateP2Driver = this.validateP2Driver.bind(this);
    this.validateP3Driver = this.validateP3Driver.bind(this);
    this.validateP4Driver = this.validateP4Driver.bind(this);
    this.validateP5Driver = this.validateP5Driver.bind(this);
    this.validateP6Driver = this.validateP6Driver.bind(this);
    this.validateP7Driver = this.validateP7Driver.bind(this);
    this.validateP8Driver = this.validateP8Driver.bind(this);
    this.validateP9Driver = this.validateP9Driver.bind(this);
    this.validateP10Driver = this.validateP10Driver.bind(this);
    this.validateDrivers = this.validateDrivers.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchComboPlayers();
    props.fetchComboContestDrivers();
    this.checkQualDates();
    this.checkRaceDates();
  }

  componentDidMount() {
    const intervalQual = setInterval(this.checkQualDates, 60000);
    const intervalRace = setInterval(this.checkRaceDates, 60000);
    this.setState({ intervalQual: intervalQual });
    this.setState({ intervalRace: intervalRace });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    if (
      nextProps.params.data !== null &&
      nextProps.params.data !== props.params.data
    ) {
      this.checkQualDates();
      this.checkRaceDates();
    }
    if (
      nextProps.contestRate.data !== null &&
      nextProps.contestRate.data !== props.contestRate.data
    ) {
      this.setState({
        isLoadingExternally: false,
        newEntry: false,
      });
    } else {
      this.setState({
        isLoadingExternally: false,
        newEntry: true,
      });
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { contestRate } = this.props;
    const { player, pp, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 } = this.state;

    if (player !== "" && player !== prevState.player) {
      this.validatePlayer(player);
    }
    if (contestRate.data !== prevProps.contestRate.data && contestRate.data) {
      this.validatePassword();
    }
    if (pp !== "" && pp !== prevState.pp) {
      this.validatePPDriver(pp);
    }
    if (p1 !== "" && p1 !== prevState.p1) {
      this.validateP1Driver(p1);
    }
    if (p2 !== "" && p2 !== prevState.p2) {
      this.validateP2Driver(p2);
    }
    if (p3 !== "" && p3 !== prevState.p3) {
      this.validateP3Driver(p3);
    }
    if (p4 !== "" && p4 !== prevState.p4) {
      this.validateP4Driver(p4);
    }
    if (p5 !== "" && p5 !== prevState.p5) {
      this.validateP5Driver(p5);
    }
    if (p6 !== "" && p6 !== prevState.p6) {
      this.validateP6Driver(p6);
    }
    if (p7 !== "" && p7 !== prevState.p7) {
      this.validateP7Driver(p7);
    }
    if (p8 !== "" && p8 !== prevState.p8) {
      this.validateP8Driver(p8);
    }
    if (p9 !== "" && p9 !== prevState.p9) {
      this.validateP9Driver(p9);
    }
    if (p10 !== "" && p10 !== prevState.p10) {
      this.validateP10Driver(p10);
    }
  };

  componentWillUnmount() {
    const { state } = this;
    clearInterval(state.intervalQual);
    clearInterval(state.intervalRace);
  }

  handleQualChange = (e, data) => {
    this.setState({ qualRatesChecked: data.checked });
    if (!data.checked) {
      this.setState({ pp: "" });
      this.setState({ ppError: "" });
    }
  };

  handleRaceChange = (e, data) => {
    this.setState({ raceRatesChecked: data.checked });
    if (!data.checked) {
      this.setState({ p1: "" });
      this.setState({ p2: "" });
      this.setState({ p3: "" });
      this.setState({ p4: "" });
      this.setState({ p5: "" });
      this.setState({ p6: "" });
      this.setState({ p7: "" });
      this.setState({ p8: "" });
      this.setState({ p9: "" });
      this.setState({ p10: "" });
      this.setState({ p1Error: "" });
      this.setState({ p2Error: "" });
      this.setState({ p3Error: "" });
      this.setState({ p4Error: "" });
      this.setState({ p5Error: "" });
      this.setState({ p6Error: "" });
      this.setState({ p7Error: "" });
      this.setState({ p8Error: "" });
      this.setState({ p9Error: "" });
      this.setState({ p10Error: "" });
    }
  };

  handleChange = (name) => (value) => {
    this.setState({
      [name]: value.value,
    });
  };

  handleBlur = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleCancel = () => {
    const { props } = this;
    this.setState({ player: "" });
    this.setState({ password: "" });
    this.setState({ pp: "" });
    this.setState({ p1: "" });
    this.setState({ p2: "" });
    this.setState({ p3: "" });
    this.setState({ p4: "" });
    this.setState({ p5: "" });
    this.setState({ p6: "" });
    this.setState({ p7: "" });
    this.setState({ p8: "" });
    this.setState({ p9: "" });
    this.setState({ p10: "" });
    this.setState({ qualRatesChecked: true });
    this.setState({ raceRatesChecked: true });
    props.history.push("/contest/rate");
  };

  handleSubmit = (e) => {
    const { props, state } = this;
    const { params } = props;
    const {
      qualActive,
      raceActive,
      qualRatesChecked,
      raceRatesChecked,
      overwrite,
      player,
      password,
      pp,
      p1,
      p2,
      p3,
      p4,
      p5,
      p6,
      p7,
      p8,
      p9,
      p10,
      sendDate,
    } = state;

    e.preventDefault();
    this.validateForm();

    if (qualActive && raceActive) {
      if (qualRatesChecked && raceRatesChecked) {
        if (
          player === "" ||
          pp === "" ||
          p1 === "" ||
          p2 === "" ||
          p3 === "" ||
          p4 === "" ||
          p5 === "" ||
          p6 === "" ||
          p7 === "" ||
          p8 === "" ||
          p9 === "" ||
          p10 === ""
        ) {
          this.sectionRate.scrollIntoView({
            block: "start",
            behavior: "smooth",
          });
          return;
        }
      } else if (qualRatesChecked) {
        if (player === "" || pp === "") {
          this.sectionRate.scrollIntoView({
            block: "start",
            behavior: "smooth",
          });
          return;
        }
      } else if (raceRatesChecked) {
        if (
          player === "" ||
          p1 === "" ||
          p2 === "" ||
          p3 === "" ||
          p4 === "" ||
          p5 === "" ||
          p6 === "" ||
          p7 === "" ||
          p8 === "" ||
          p9 === "" ||
          p10 === ""
        ) {
          this.sectionRate.scrollIntoView({
            block: "start",
            behavior: "smooth",
          });
          return;
        }
      }
    } else if (qualActive) {
      if (player === "" || pp === "") {
        this.sectionRate.scrollIntoView({ block: "start", behavior: "smooth" });
        return;
      }
    } else if (raceActive) {
      if (
        player === "" ||
        p1 === "" ||
        p2 === "" ||
        p3 === "" ||
        p4 === "" ||
        p5 === "" ||
        p6 === "" ||
        p7 === "" ||
        p8 === "" ||
        p9 === "" ||
        p10 === ""
      ) {
        this.sectionRate.scrollIntoView({ block: "start", behavior: "smooth" });
        return;
      }
    }

    this.sectionRate.scrollIntoView({ block: "start", behavior: "smooth" });

    props.saveContestRate({
      overwrite: overwrite,
      qualRatesChecked: qualRatesChecked,
      raceRatesChecked: raceRatesChecked,
      gp: params.data.nextGP,
      season: params.data.nextGPYear,
      player: player,
      password: password,
      pp: pp,
      p1: p1,
      p2: p2,
      p3: p3,
      p4: p4,
      p5: p5,
      p6: p6,
      p7: p7,
      p8: p8,
      p9: p9,
      p10: p10,
      sendDate: sendDate,
    });
  };

  handleOverwriteSubmit = (e) => {
    const { props, state } = this;
    const { params } = props;
    const {
      qualRatesChecked,
      raceRatesChecked,
      player,
      password,
      pp,
      p1,
      p2,
      p3,
      p4,
      p5,
      p6,
      p7,
      p8,
      p9,
      p10,
    } = state;

    e.preventDefault();
    props.saveContestRate({
      overwrite: true,
      qualRatesChecked: qualRatesChecked,
      raceRatesChecked: raceRatesChecked,
      gp: params.data.nextGP,
      season: params.data.nextGPYear,
      player: player,
      password: password,
      pp: pp,
      p1: p1,
      p2: p2,
      p3: p3,
      p4: p4,
      p5: p5,
      p6: p6,
      p7: p7,
      p8: p8,
      p9: p9,
      p10: p10,
    });
  };

  checkQualDates = () => {
    const { params } = this.props;
    if (params.data == null) {
      return false;
    }
    const {
      nextGPQualYear,
      nextGPQualMonth,
      nextGPQualDay,
      nextGPQualHour,
      nextGPQualMin,
    } = params.data;
    const currentDate = new Date();
    const currentDateTime = currentDate.getTime();
    // data kwalifikacji -1 miesiac
    const date = new Date(
      parseInt(nextGPQualYear, 10),
      parseInt(nextGPQualMonth, 10) - 1,
      parseInt(nextGPQualDay, 10),
      parseInt(nextGPQualHour, 10),
      parseInt(nextGPQualMin, 10),
      0
    );
    const dateTime = date.getTime();
    if (currentDateTime - dateTime > 0) {
      this.setState({ qualActive: false });
      this.setState({ qualRatesChecked: false });
      return false;
    }
    return true;
  };

  checkRaceDates = () => {
    const { params } = this.props;
    if (params.data == null) {
      return false;
    }
    const { nextGPYear, nextGPMonth, nextGPDay, nextGPHour, nextGPMin } =
      params.data;
    const currentDate = new Date();
    const currentDateTime = currentDate.getTime();
    // data wyścigu -1 miesiac
    //const date = new Date(nextGPYear, nextGPMonth, nextGPDay, nextGPHour, nextGPMin, 0);
    const date = new Date(
      parseInt(nextGPYear, 10),
      parseInt(nextGPMonth, 10) - 1,
      parseInt(nextGPDay, 10),
      parseInt(nextGPHour, 10),
      parseInt(nextGPMin, 10),
      0
    );
    const dateTime = date.getTime();
    if (currentDateTime - dateTime > 0) {
      this.setState({ raceActive: false });
      return false;
    }
    return true;
  };

  validateForm() {
    const {
      qualActive,
      raceActive,
      qualRatesChecked,
      raceRatesChecked,
      player,
      pp,
      p1,
      p2,
      p3,
      p4,
      p5,
      p6,
      p7,
      p8,
      p9,
      p10,
    } = this.state;

    this.validatePlayer(player);
    if (qualActive && qualRatesChecked) {
      this.validatePPDriver(pp);
    }
    if (raceActive && raceRatesChecked) {
      this.validateP1Driver(p1);
      this.validateP2Driver(p2);
      this.validateP3Driver(p3);
      this.validateP4Driver(p4);
      this.validateP5Driver(p5);
      this.validateP6Driver(p6);
      this.validateP7Driver(p7);
      this.validateP8Driver(p8);
      this.validateP9Driver(p9);
      this.validateP10Driver(p10);
    }
  }

  validatePlayer(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error1" });
    }
    this.setState({ playerError: error });
  }

  validatePassword() {
    const { contestRate } = this.props;
    const { formatMessage } = this.props.intl;
    let error = "";
    if (contestRate.data.status === "ERROR-RATES-WRONG-PASSWORD") {
      error = formatMessage({ id: "app.page.contest.rate.error2" });
    }
    this.setState({ passwordError: error });
  }

  validatePPDriver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
    }
    this.setState({ ppError: error });
  }

  validateP1Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p1Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP2Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p2Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP3Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p3Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP4Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p4Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP5Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p5Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP6Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p6Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP7Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p7Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP8Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p8Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP9Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p9Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateP10Driver(value) {
    const { formatMessage } = this.props.intl;
    let error = "";
    if (!value) {
      error = formatMessage({ id: "app.page.contest.rate.error3" });
      this.setState({ p10Error: error });
      return;
    }
    this.validateDrivers();
  }

  validateDrivers() {
    const { formatMessage } = this.props.intl;
    const { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 } = this.state;
    const p1ValuesArr = [p2, p3, p4, p5, p6, p7, p8, p9, p10];
    if (p1 !== "") {
      if ($.inArray(p1, p1ValuesArr) === -1) {
        this.setState({ p1Error: "" });
      } else {
        this.setState({
          p1Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p2ValuesArr = [p1, p3, p4, p5, p6, p7, p8, p9, p10];
    if (p2 !== "") {
      if ($.inArray(p2, p2ValuesArr) === -1) {
        this.setState({ p2Error: "" });
      } else {
        this.setState({
          p2Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p3ValuesArr = [p1, p2, p4, p5, p6, p7, p8, p9, p10];
    if (p3 !== "") {
      if ($.inArray(p3, p3ValuesArr) === -1) {
        this.setState({ p3Error: "" });
      } else {
        this.setState({
          p3Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p4ValuesArr = [p1, p2, p3, p5, p6, p7, p8, p9, p10];
    if (p4 !== "") {
      if ($.inArray(p4, p4ValuesArr) === -1) {
        this.setState({ p4Error: "" });
      } else {
        this.setState({
          p4Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p5ValuesArr = [p1, p2, p3, p4, p6, p7, p8, p9, p10];
    if (p5 !== "") {
      if ($.inArray(p5, p5ValuesArr) === -1) {
        this.setState({ p5Error: "" });
      } else {
        this.setState({
          p5Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p6ValuesArr = [p1, p2, p3, p4, p5, p7, p8, p9, p10];
    if (p6 !== "") {
      if ($.inArray(p6, p6ValuesArr) === -1) {
        this.setState({ p6Error: "" });
      } else {
        this.setState({
          p6Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p7ValuesArr = [p1, p2, p3, p4, p5, p6, p8, p9, p10];
    if (p7 !== "") {
      if ($.inArray(p7, p7ValuesArr) === -1) {
        this.setState({ p7Error: "" });
      } else {
        this.setState({
          p7Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p8ValuesArr = [p1, p2, p3, p4, p5, p6, p7, p9, p10];
    if (p8 !== "") {
      if ($.inArray(p8, p8ValuesArr) === -1) {
        this.setState({ p8Error: "" });
      } else {
        this.setState({
          p8Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p9ValuesArr = [p1, p2, p3, p4, p5, p6, p7, p8, p10];
    if (p9 !== "") {
      if ($.inArray(p9, p9ValuesArr) === -1) {
        this.setState({ p9Error: "" });
      } else {
        this.setState({
          p9Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
    const p10ValuesArr = [p1, p2, p3, p4, p5, p6, p7, p8, p9];
    if (p10 !== "") {
      if ($.inArray(p10, p10ValuesArr) === -1) {
        this.setState({ p10Error: "" });
      } else {
        this.setState({
          p10Error: formatMessage({ id: "app.page.contest.rate.error4" }),
        });
      }
    }
  }

  render() {
    const { props, state } = this;
    const { params, contestRate } = props;
    const { formatMessage } = this.props.intl;

    const {
      qualActive,
      raceActive,
      qualRatesChecked,
      raceRatesChecked,
      player,
      pp,
      p1,
      p2,
      p3,
      p4,
      p5,
      p6,
      p7,
      p8,
      p9,
      p10,
      playerError,
      passwordError,
      ppError,
      p1Error,
      p2Error,
      p3Error,
      p4Error,
      p5Error,
      p6Error,
      p7Error,
      p8Error,
      p9Error,
      p10Error,
    } = state;
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;

    const SelectPlayer = () => (
      <Select
        isLoading={isLoadingExternally}
        id="player"
        name="player"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: player,
          label:
            props.comboPlayers.data?.find((e) => e.value == player)?.text ||
            formatMessage({ id: "app.placeholder.select.player" }),
        }}
        onChange={this.handleChange("player")}
        options={props.comboPlayers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={formatMessage({ id: "app.placeholder.select.player" })}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          playerError !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
        classNamePrefix="react-select"
      />
    );
    const SelectPPDriver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="pp"
        name="pp"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: pp,
          label: props.comboContestDrivers.data?.find((e) => e.value == pp)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("pp")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          ppError !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
        classNamePrefix="react-select"
      />
    );
    const SelectP1Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p1"
        name="p1"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p1,
          label: props.comboContestDrivers.data?.find((e) => e.value == p1)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p1")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p1Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP2Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p2"
        name="p2"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p2,
          label: props.comboContestDrivers.data?.find((e) => e.value == p2)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p2")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p2Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP3Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p3"
        name="p3"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p3,
          label: props.comboContestDrivers.data?.find((e) => e.value == p3)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p3")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p3Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP4Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p4"
        name="p4"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p4,
          label: props.comboContestDrivers.data?.find((e) => e.value == p4)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p4")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p4Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP5Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p5"
        name="p5"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p5,
          label: props.comboContestDrivers.data?.find((e) => e.value == p5)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p5")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p5Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP6Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p6"
        name="p6"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p6,
          label: props.comboContestDrivers.data?.find((e) => e.value == p6)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p6")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p6Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP7Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p7"
        name="p7"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p7,
          label: props.comboContestDrivers.data?.find((e) => e.value == p7)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p7")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p7Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP8Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p8"
        name="p8"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p8,
          label: props.comboContestDrivers.data?.find((e) => e.value == p8)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p8")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p8Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP9Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p9"
        name="p9"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p9,
          label: props.comboContestDrivers.data?.find((e) => e.value == p9)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p9")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p9Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    const SelectP10Driver = () => (
      <Select
        isLoading={isLoadingExternally}
        id="p10"
        name="p10"
        multi={multi}
        ignoreCase={ignoreCase}
        ignoreAccents={ignoreAccents}
        value={{
          value: p10,
          label: props.comboContestDrivers.data?.find((e) => e.value == p10)
            ?.text || <FormattedMessage id={"app.placeholder.select.driver"} />,
        }}
        onChange={this.handleChange("p10")}
        options={props.comboContestDrivers.data?.map((e) => ({
          value: e.value,
          label: e.text,
        }))}
        clearable={clearable}
        labelKey="text"
        valueKey="value"
        placeholder={<FormattedMessage id={"app.placeholder.select.driver"} />}
        noResultsText={<FormattedMessage id={"app.placeholder.no.results"} />}
        className={
          p10Error !== ""
            ? "react-select-container error-select"
            : "react-select-container"
        }
      />
    );
    if (!props.params.data || props.params.loading) {
      return (
        <section
          id="contest-rate-details"
          name="contest-rate-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    if (contestRate.data && contestRate.loading) {
      return (
        <section
          id="contest-rate-details"
          name="contest-rate-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.page.contest.rate.sending.types"} />
            </Loader>
          </div>
        </section>
      );
    }
    if (
      !state.newEntry &&
      contestRate.data &&
      contestRate.data.status === "SUCCESS"
    ) {
      return (
        <section
          id="contest-rate-details"
          name="contest-rate-details"
          className="section-page"
        >
          <SectionPageBanner
            title={`${params.data.nextGPName} ${params.data.nextGPYear}`}
            subtitle={
              <FormattedMessage id={"app.page.contest.rate.confirm.title"} />
            }
          />
          <Segment padded basic>
            <Message positive>
              <Message.Header>
                <FormattedMessage id={"app.message.header"} />
              </Message.Header>
              <Message.List>
                <Message.Item>
                  <FormattedMessage
                    id={"app.page.contest.rate.confirm.info"}
                    values={{ gp: params.data.nextGPName }}
                  />
                </Message.Item>
              </Message.List>
            </Message>
            <div className="buttons">
              <NavLink
                className="secondary"
                to={`/contest/gprate/${params.data.nextGPYear}/${params.data.nextGPAlias}`}
              >
                <FormattedMessage
                  id={"app.button.predictions.for.gp"}
                  values={{ gp: params.data.nextGPName }}
                />
              </NavLink>
            </div>
          </Segment>
        </section>
      );
    }
    if (
      !state.newEntry &&
      contestRate.data &&
      contestRate.data.status === "ERROR-RATES-ALREADY-SENT"
    ) {
      return (
        <section
          id="contest-rate-details"
          name="contest-rate-details"
          className="section-page"
        >
          <SectionPageBanner
            title={`${params.data.nextGPName} ${params.data.nextGPYear}`}
            subtitle={
              <FormattedMessage id={"app.page.contest.rate.confirm.title"} />
            }
          />
          <Segment padded basic>
            <Message negative>
              <Message.Header>
                <FormattedMessage id={"app.message.header"} />
              </Message.Header>
              <Message.List>
                <Message.Item>
                  <FormattedMessage
                    id={"app.page.contest.rate.confirm.info2"}
                    values={{ gp: params.data.nextGPName }}
                  />
                </Message.Item>
              </Message.List>
            </Message>
            <div className="buttons">
              <Button onClick={this.handleOverwriteSubmit}>
                <FormattedMessage id={"app.button.change"} />
              </Button>
              <Button onClick={this.handleCancel}>
                <FormattedMessage id={"app.button.cancel"} />
              </Button>
            </div>
          </Segment>
        </section>
      );
    }

    const raceDate = new Date(
      params.data.nextGPYear,
      params.data.nextGPMonth - 1,
      params.data.nextGPDay,
      params.data.nextGPHour,
      params.data.nextGPMin,
      0
    );
    const qualDate = new Date(
      params.data.nextGPQualYear,
      params.data.nextGPQualMonth - 1,
      params.data.nextGPQualDay,
      params.data.nextGPQualHour,
      params.data.nextGPQualMin,
      0
    );
    const filename = `/build/images/countries/${params.data.nextGP.toLowerCase()}.jpg`;
    return (
      <section
        id="contest-rate-details"
        name="contest-rate-details"
        className="section-page"
      >
        <SectionPageBanner
          title={`${params.data.nextGPName} ${params.data.nextGPYear}`}
          subtitle={<FormattedMessage id={"app.page.contest.rate.subtitle"} />}
        />
        <div
          ref={(c) => {
            this.sectionRate = c;
          }}
        />
        {/* <Segment basic>
          <Message negative>
            <Message.Header>
              <FormattedMessage id={"app.message.header"} />
            </Message.Header>
            <Message.List>
              <Message.Item>
                Aktualnie typowanie nieaktywne. 
              </Message.Item>
            </Message.List>
          </Message>
        </Segment> */}
        <div className="section-page-content contest-rate">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="info-box"
              >
                <Image
                  className="contest-rate-gp-photo"
                  centered
                  src={filename}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/countries/country_no_profile.jpg";
                  }}
                />
                <Segment padded basic textAlign="center">
                  <Divider hidden></Divider>
                  <Header size="large" inverted>
                    {params.data.nextGPSprint == "0" ? (
                      <FormattedMessage id={"app.stats.qual"} />
                    ) : (
                      <FormattedMessage id={"app.stats.sprint"} />
                    )}
                  </Header>
                  <Statistic size="mini" inverted>
                    <Statistic.Value>
                      {params.data.nextGPQualYear}
                      {"-"}
                      {parseInt(params.data.nextGPQualMonth, 10) < 10
                        ? `0${params.data.nextGPQualMonth}`
                        : params.data.nextGPQualMonth}
                      {"-"}
                      {parseInt(params.data.nextGPQualDay, 10) < 10
                        ? `0${params.data.nextGPQualDay}`
                        : params.data.nextGPQualDay}{" "}
                      {parseInt(params.data.nextGPQualHour, 10) < 10
                        ? `0${params.data.nextGPQualHour}`
                        : params.data.nextGPQualHour}
                      {":"}
                      {parseInt(params.data.nextGPQualMin, 10) < 10
                        ? `0${params.data.nextGPQualMin}`
                        : params.data.nextGPQualMin}
                    </Statistic.Value>
                  </Statistic>
                  <Divider></Divider>
                  <Header size="large" inverted>
                    <FormattedMessage id={"app.stats.race"} />
                  </Header>
                  <Statistic size="mini" inverted>
                    <Statistic.Value>
                      {params.data.nextGPYear}
                      {"-"}
                      {parseInt(params.data.nextGPMonth, 10) < 10
                        ? `0${params.data.nextGPMonth}`
                        : params.data.nextGPMonth}
                      {"-"}
                      {parseInt(params.data.nextGPDay, 10) < 10
                        ? `0${params.data.nextGPDay}`
                        : params.data.nextGPDay}{" "}
                      {parseInt(params.data.nextGPHour, 10) < 10
                        ? `0${params.data.nextGPHour}`
                        : params.data.nextGPHour}
                      {":"}
                      {parseInt(params.data.nextGPMin, 10) < 10
                        ? `0${params.data.nextGPMin}`
                        : params.data.nextGPMin}
                    </Statistic.Value>
                  </Statistic>
                  <Divider hidden></Divider>
                  <div className="hideForMobile">
                    <div className="adv">
                      <FormattedMessage id={"app.advert"} />
                    </div>
                    <Adsense
                      client="ca-pub-6611514323676676"
                      slot="5583221558"
                    />
                  </div>
                </Segment>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                {!state.newEntry &&
                  contestRate.data &&
                  contestRate.data.status !== "SUCCESS" && (
                    <Segment padded basic>
                      <Message negative>
                        <Message.Header>
                          <FormattedMessage id={"app.message.header"} />
                        </Message.Header>
                        <Message.List>
                          <Message.Item>
                            {contestRate.data.status ===
                              "ERROR-RATES-WRONG-PASSWORD" && (
                              <FormattedMessage
                                id={"app.page.contest.rate.wrong.password"}
                              />
                            )}
                          </Message.Item>
                        </Message.List>
                      </Message>
                    </Segment>
                  )}
                {((!qualRatesChecked && !raceRatesChecked) ||
                  playerError !== "" ||
                  ppError !== "" ||
                  p1Error !== "" ||
                  p2Error !== "" ||
                  p3Error !== "" ||
                  p4Error !== "" ||
                  p5Error !== "" ||
                  p6Error !== "" ||
                  p7Error !== "" ||
                  p8Error !== "" ||
                  p9Error !== "" ||
                  p10Error !== "") && (
                  <Segment padded basic>
                    <Message negative>
                      <Message.Header>
                        <FormattedMessage id={"app.message.header.warning"} />
                      </Message.Header>
                      <Message.List>
                        <Message.Item>
                          <FormattedMessage
                            id={"app.page.contest.rate.form.error"}
                          />
                        </Message.Item>
                      </Message.List>
                    </Message>
                  </Segment>
                )}
                {!qualActive && !raceActive && (
                  <Segment padded basic>
                    <Message negative>
                      <Message.Header>
                        <FormattedMessage id={"app.message.header.warning"} />
                      </Message.Header>
                      <Message.List>
                        <Message.Item>
                          <FormattedMessage
                            id={"app.page.contest.rate.blocked"}
                            values={{ gp: params.data.nextGPName }}
                          />
                        </Message.Item>
                      </Message.List>
                    </Message>
                  </Segment>
                )}
                {(qualActive || raceActive) && (
                  <form onSubmit={this.handleSubmit}>
                    <Segment padded basic>
                      <Grid stackable padded>
                        <Grid.Row>
                          <Grid.Column width={16}>
                            <Segment padded basic>
                              <Divider horizontal>
                                <FormattedMessage
                                  id={"app.page.contest.rate.player.data"}
                                />
                              </Divider>
                              {qualActive && raceActive && (
                                <Grid stackable padded>
                                  <Grid.Row>
                                    <Grid.Column width={16}>
                                      <br />
                                      <Form.Group inline>
                                        <Form.Checkbox
                                          name="qualRatesChecked"
                                          label={
                                            params.data.nextGPSprint == "0"
                                              ? formatMessage({
                                                  id: "app.page.contest.rate.predictions.for.qual",
                                                })
                                              : formatMessage({
                                                  id: "app.page.contest.rate.predictions.for.sprint",
                                                })
                                          }
                                          defaultChecked
                                          inline
                                          onChange={this.handleQualChange}
                                          error={
                                            !qualRatesChecked &&
                                            !raceRatesChecked
                                          }
                                        />
                                        <Form.Checkbox
                                          name="raceRatesChecked"
                                          label={formatMessage({
                                            id: "app.page.contest.rate.predictions.for.race",
                                          })}
                                          defaultChecked
                                          inline
                                          onChange={this.handleRaceChange}
                                          error={
                                            !qualRatesChecked &&
                                            !raceRatesChecked
                                          }
                                        />
                                      </Form.Group>
                                      {!qualRatesChecked &&
                                        !raceRatesChecked && (
                                          <Label
                                            htmlFor="p10"
                                            color="red"
                                            pointing
                                            size="large"
                                          >
                                            <FormattedMessage
                                              id={
                                                "app.page.contest.rate.select.predictions.type"
                                              }
                                            />
                                          </Label>
                                        )}
                                    </Grid.Column>
                                  </Grid.Row>
                                </Grid>
                              )}
                            </Segment>
                          </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                          <Grid.Column width={8}>
                            <Segment padded basic>
                              <label htmlFor="player">
                                <FormattedMessage
                                  id={"app.page.contest.rate.name"}
                                />
                                <span className="marked">*</span>
                              </label>
                              <Form.Field
                                name="player"
                                control={SelectPlayer}
                              />
                              {playerError && (
                                <Label
                                  htmlFor="player"
                                  color="red"
                                  pointing
                                  size="large"
                                >
                                  {playerError}
                                </Label>
                              )}
                            </Segment>
                          </Grid.Column>
                          <Grid.Column width={8}>
                            <Segment padded basic>
                              <label htmlFor="player">
                                <FormattedMessage
                                  id={"app.page.contest.rate.password"}
                                />
                              </label>
                              <Form.Input
                                id="password"
                                name="password"
                                placeholder={formatMessage({
                                  id: "app.page.contest.rate.password",
                                })}
                                type="password"
                                fluid
                                onBlur={this.handleBlur}
                                error={passwordError !== ""}
                              />
                              {passwordError && (
                                <Label
                                  htmlFor="password"
                                  color="red"
                                  pointing
                                  size="large"
                                >
                                  {passwordError}
                                </Label>
                              )}
                            </Segment>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                      {qualActive && qualRatesChecked && (
                        <Grid stackable padded>
                          <Grid.Row>
                            <Grid.Column width={16}>
                              <Segment basic textAlign="center">
                                <Divider horizontal>
                                  {params.data.nextGPSprint == "0"
                                    ? formatMessage({ id: "app.stats.qual" })
                                    : formatMessage({ id: "app.stats.sprint" })}
                                </Divider>
                                <div className="pp-countdown">
                                  <ContestCountdown date={qualDate} />
                                </div>
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                          <Grid.Row>
                            <Grid.Column width={16}>
                              <Segment padded basic>
                                <label htmlFor="pp">
                                  <FormattedMessage
                                    id={"app.page.contest.rate.winner"}
                                  />
                                </label>
                                <Form.Field
                                  name="pp"
                                  control={SelectPPDriver}
                                />
                                {ppError && (
                                  <Label
                                    htmlFor="pp"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {ppError}
                                  </Label>
                                )}
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      )}
                      {!qualActive && (
                        <Grid stackable padded>
                          <Grid.Row>
                            <Grid.Column width={16}>
                              <Segment basic textAlign="center">
                                <Divider horizontal>
                                  {params.data.nextGPSprint == "0"
                                    ? formatMessage({ id: "app.stats.qual" })
                                    : formatMessage({ id: "app.stats.sprint" })}
                                </Divider>
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                          <Grid.Row>
                            <Grid.Column width={16}>
                              <Segment padded basic>
                                <Message negative>
                                  <Message.List>
                                    <Message.Item>
                                      {params.data.nextGPSprint == "0"
                                        ? formatMessage({
                                            id: "app.page.contest.rate.qual.blocked",
                                          })
                                        : formatMessage({
                                            id: "app.page.contest.rate.sprint.blocked",
                                          })}
                                    </Message.Item>
                                  </Message.List>
                                </Message>
                              </Segment>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      )}
                      {raceActive && raceRatesChecked && (
                        <Segment padded basic>
                          <Grid stackable padded>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <Segment basic textAlign="center">
                                  <Divider horizontal>
                                    <FormattedMessage id={"app.stats.race"} />
                                  </Divider>
                                  <div className="gp-countdown">
                                    <ContestCountdown date={raceDate} />
                                  </div>
                                </Segment>
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p1">
                                  1.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p1"
                                  control={SelectP1Driver}
                                />
                                {p1Error && (
                                  <Label
                                    htmlFor="p1"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p1Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p2">
                                  2.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p2"
                                  control={SelectP2Driver}
                                />
                                {p2Error && (
                                  <Label
                                    htmlFor="p2"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p2Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p3">
                                  3.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p3"
                                  control={SelectP3Driver}
                                />
                                {p3Error && (
                                  <Label
                                    htmlFor="p3"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p3Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p4">
                                  4.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p4"
                                  control={SelectP4Driver}
                                />
                                {p4Error && (
                                  <Label
                                    htmlFor="p4"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p4Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p5">
                                  5.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p5"
                                  control={SelectP5Driver}
                                />
                                {p5Error && (
                                  <Label
                                    htmlFor="p5"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p5Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p6">
                                  6.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p6"
                                  control={SelectP6Driver}
                                />
                                {p6Error && (
                                  <Label
                                    htmlFor="p6"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p6Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p7">
                                  7.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p7"
                                  control={SelectP7Driver}
                                />
                                {p7Error && (
                                  <Label
                                    htmlFor="p7"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p7Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p8">
                                  8.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p8"
                                  control={SelectP8Driver}
                                />
                                {p8Error && (
                                  <Label
                                    htmlFor="p8"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p8Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p9">
                                  9.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p9"
                                  control={SelectP9Driver}
                                />
                                {p9Error && (
                                  <Label
                                    htmlFor="p9"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p9Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                              <Grid.Column width={16}>
                                <label htmlFor="p10">
                                  10.{" "}
                                  <FormattedMessage
                                    id={"app.page.contest.rate.place"}
                                  />
                                </label>
                                <Form.Field
                                  name="p10"
                                  control={SelectP10Driver}
                                />
                                {p10Error && (
                                  <Label
                                    htmlFor="p10"
                                    color="red"
                                    pointing
                                    size="large"
                                  >
                                    {p10Error}
                                  </Label>
                                )}
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </Segment>
                      )}
                      <div className="buttons">
                        <Button type="submit">
                          {" "}
                          <FormattedMessage id={"app.button.send"} />
                        </Button>
                      </div>
                      <Segment basic>
                        <Message>
                          <Message.Header>
                            <FormattedMessage id={"app.message.header"} />
                          </Message.Header>
                          <Message.List>
                            <Message.Item>
                              <FormattedMessage
                                id={"app.page.contest.rate.info1"}
                              />{" "}
                              <NavLink to="/contest/register">
                                {" "}
                                <FormattedMessage
                                  id={"app.page.contest.rate.info2"}
                                />
                              </NavLink>
                            </Message.Item>
                          </Message.List>
                        </Message>
                      </Segment>
                    </Segment>
                  </form>
                )}
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}
ContestRateForm.propTypes = {
  fetchComboPlayers: PropTypes.func.isRequired,
  fetchComboContestDrivers: PropTypes.func.isRequired,
  contestRate: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  params: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  nextGPQualYear: PropTypes.string,
  nextGPQualMonth: PropTypes.string,
  nextGPQualDay: PropTypes.string,
  nextGPQualHour: PropTypes.string,
  nextGPQualMin: PropTypes.string,
  nextGPYear: PropTypes.string,
  nextGPMonth: PropTypes.string,
  nextGPDay: PropTypes.string,
  nextGPHour: PropTypes.string,
  nextGPMin: PropTypes.string,
  nextGPAlias: PropTypes.string,
  history: PropTypes.object,
  saveContestRate: PropTypes.func.isRequired,
  comboPlayers: PropTypes.object,
  comboContestDrivers: PropTypes.object,
};

function mapStateToProps({
  contestRate,
  params,
  comboPlayers,
  comboContestDrivers,
}) {
  return {
    contestRate,
    params,
    comboPlayers,
    comboContestDrivers,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveContestRate,
      fetchComboPlayers,
      fetchComboContestDrivers,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(ContestRateForm)
);
