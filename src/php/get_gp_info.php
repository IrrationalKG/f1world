<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

// Kalendarz GP w sezonie
$query="select gp.name_alias alias,gp.name_short gp,gp.id_gp id,gp_season.sort number,";
if ($lang=='pl') {
  $query.="gp.name,coalesce(concat(drivers.name,' ',drivers.surname),'nieznany') winner,";
}else{
  $query.="gp.name_en as name,coalesce(concat(drivers.name,' ',drivers.surname),'unknown') winner,";
}  
$query.="gp.photo,gp.country_code country,circuit,circuit_alias circuitAlias,SUBSTRING(gp_season.date,1,16) date,gp.laps,gp.length,gp.distance,drivers_gp_results.id_driver idDriver
from gp
left join gp_season on gp.id_gp=gp_season.id_gp
left join drivers_gp_results on gp_season.id_gp=drivers_gp_results.id_gp and drivers_gp_results.race_date>'$year-01-01' and race_date<'$year-12-31' and race_pos=1
left join drivers on drivers.id_driver=drivers_gp_results.id_driver
where gp_season.season='$year'
order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems = array();
while($r = mysqli_fetch_assoc($result)) {
   $gpItems[] = $r;
}

// Response
$response = $gpItems;


print json_encode($response);
mysqli_free_result($result);
?>
