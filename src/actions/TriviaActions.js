// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TRIVIA_REQUEST = "FETCH_TRIVIA_REQUEST";
export const FETCH_TRIVIA_SUCCESS = "FETCH_TRIVIA_SUCCESS";
export const FETCH_TRIVIA_ERROR = "FETCH_TRIVIA_ERROR";

function requestData() {
  return {
    type: FETCH_TRIVIA_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TRIVIA_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TRIVIA_ERROR,
    payload: error,
  };
}

export function fetchTrivia(lang) {
  const url = `${ROOT_URL}/get_trivia.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
