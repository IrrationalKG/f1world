<?
include("dbinfo.inc.php");

if(isset($_GET['season'])){
    $season = $_GET['season'];
}else{
    $season = "2025";
}
if(isset($_GET['id_gp'])){
    $id_gp = $_GET['id_gp'];
}else{
    $id_gp = "-1";
}

$dbhandle = mysqli_connect('localhost',$username,$password);
@mysqli_select_db($dbhandle, $database) or die( "Unable to select database");

$query="SELECT 
drivers_gp_involvements.id_involvement,
drivers_gp_involvements.id_driver, drivers_gp_involvements.number, CONCAT(drivers.name,' ',drivers.surname) driverName, 
drivers_gp_involvements.id_team, teams.name teamName, teams.engine, 
drivers_gp_involvements.id_team_model, teams_models.model modelName,
drivers_gp_involvements.id_gp, 
drivers_gp_involvements.id_tyre, tyres.name tyreName
FROM drivers_gp_involvements 
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre=drivers_gp_involvements.id_tyre
where drivers_gp_involvements.season='$season' AND drivers_gp_involvements.id_gp=$id_gp ORDER BY drivers.name, drivers.surname";
echo $query;
$result_i=mysqli_query($dbhandle, $query);
$num_i=mysqli_num_rows($result_i);

$query="SELECT DISTINCT season FROM gp_season order by season";
$result_s=mysqli_query($dbhandle, $query);
$num_s=mysqli_num_rows($result_s);

$query="SELECT * FROM gp order by name";
$result_c=mysqli_query($dbhandle, $query);
$num_c=mysqli_num_rows($result_c);

$query="SELECT DISTINCT id_team_model, team_name, name, model FROM teams_models 
left join teams ON teams.team=teams_models.team where season=$season order by team_name, name, model";
$result_tm=mysqli_query($dbhandle, $query);
$num_tm=mysqli_num_rows($result_tm);
$teamsModelsTab = array();
while ($r = mysqli_fetch_assoc($result_tm)) {
    $tmp_id_team_model = $r["id_team_model"];
	$teamsModelsTab[$tmp_id_team_model] = $r["team_name"]." ".$r["name"]." ".$r["model"]." [".$r["id_team_model"]."]";
}

$query="SELECT id_tyre, name FROM tyres order by name";
$result_tr=mysqli_query($dbhandle, $query);
$num_tr=mysqli_num_rows($result_tr);
$tyresTab = array();
while ($r = mysqli_fetch_assoc($result_tr)) {
    $tmp_id_tyre = $r["id_tyre"];
	$tyresTab[$tmp_id_tyre] = $r["name"]." [".$r["id_tyre"]."]";
}


mysqli_close($dbhandle);

?>
<html>

<head>
    <title>Untitled Document</title>
    <script language="javascript" type="text/javascript">
    function doReload(season, id_gp) {
        document.location = 'add_car_model_and_tyres_info.php?season=' + season + '&id_gp=' + id_gp;

        /* But if you want to submit the form just comment above line and uncomment following lines*/
        //document.frm1.action = 'samepage.php';
        //document.frm1.method = 'post';
        //document.frm1.submit();
    }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
    <form method="post" action="insert_car_model_and_tyres_info.php">
        <p>Add GP Result<br>
            Sezon:
            <select name="season" onChange="doReload(this.value,<? echo "$id_gp"; ?>);">
                <?
            while ($r = mysqli_fetch_assoc($result_s)) {
                $year=$r["season"];
            ?>
                <option value="<? echo "$year"; ?>"
                    <?  if ($year == $season) { echo "selected";}else{ echo "";} ?>>
                    <? echo "$year"; ?>
                </option>
                <?
            }
            mysqli_free_result($result_s);
            ?>
            </select>
            GP:
            <select name="id_gp" onChange="doReload(<? echo "$season"; ?>,this.value);">
                <?
            $i=0;
            while ($r = mysqli_fetch_assoc($result_c)) {
                $gp=$r["id_gp"];
                $name=$r["name"];
                $circuit=$r["circuit"];
            ?>
                <option value="<? echo " $gp"; ?>"
                    <?  if ($gp == $id_gp) { echo "selected";}else{ echo "";} ?>>
                    <? echo "$name"; ?>&nbsp;(
                    <? echo "$circuit"; ?>)
                </option>
                <?
            }
            mysqli_free_result($result_c);
            ?>
            </select>
            All GP:
            <input type="checkbox" id="allGP" name="allGP" value="1">
            <br>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Number</th>
                <th>Driver</th>
                <th>Team</th>
                <th>Engine</th>
                <th>Model</th>
                <th>Model Name</th>
                <th>Tyre</th>
            </tr>
            <?
                while ($r = mysqli_fetch_assoc($result_i)) {
                    $id_involvement=$r["id_involvement"];
                    $id_driver=$r["id_driver"];
                    $driver_name=$r["driverName"];
                    $number=$r["number"];
                    $id_team=$r["id_team"];
                    $team_name=$r["teamName"];
                    $engine=$r["engine"];
                    $id_team_model=$r["id_team_model"];
                    $modelName=$r["modelName"];
                    $id_gp=$r["id_gp"];
                    $id_tyre=$r["id_tyre"];
                    $tyre_name=$r["tyreName"];
                ?>
            <tr>
                <td>
                    <? echo "$id_involvement"; ?>
                </td>
                <td>
                    <input id="number_<? echo "$id_involvement"; ?>" name="number_<? echo "$id_involvement"; ?>" value=
                    <? echo "$number"; ?>>
                </td>
                <td>
                    <? echo "$driver_name"; ?> [
                    <? echo "$id_driver"; ?>]
                </td>
                <td>
                    <? echo "$team_name"; ?> [
                    <? echo "$id_team"; ?>]
                </td>
                <td>
                    <? echo "$engine"; ?>
                </td>
                <td>
                    <select name="model_<? echo "$id_involvement"; ?>">
                        <option value=""></option>
                        <?
                         foreach ($teamsModelsTab as $x => $y) {
                        ?>
                            <option value="<? echo " $x"; ?>" <?  if ($x == $id_team_model) { echo "selected";}else{ echo "";} ?>>
                                <? echo "$y"; ?>
                            </option>
                        <?        
                        }
                        ?>
                    </select>
                    <? echo "[$id_team_model]"; ?>
                </td>
                <td>
                    <? echo "$modelName"; ?>
                </td>
                <td>
                    <select name="tyre_<? echo "$id_involvement"; ?>">
                        <option value=""></option>
                        <?
                         foreach ($tyresTab as $x => $y) {
                        ?>
                            <option value="<? echo " $x"; ?>" <?  if ($x == $id_tyre) { echo "selected";}else{ echo "";} ?>>
                                <? echo "$y"; ?>
                            </option>
                        <?        
                        }
                        ?>
                    </select>
                    <? echo "[$id_tyre]"; ?>
                </td>
            </tr>
            <?
                }
                mysqli_free_result($result_i);
                ?>
        </table>
        <br>
        <input type="submit" name="Submit" value="Submit">
        </p>
    </form>
</body>

</html>