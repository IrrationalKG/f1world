<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT id,link,";
if ($lang=='pl') {
  $query.="text";
}else{
  $query.="text_en as text";
}  
$query.=" FROM trivia ORDER BY date desc LIMIT 4";
$result = mysqli_query($dbhandle,$query);
$teamItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamItems[] = $r;
}
$triviaItems["data"]=$teamItems;
$triviaItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $triviaItems;

print json_encode($response);
mysqli_free_result($result);
?>
