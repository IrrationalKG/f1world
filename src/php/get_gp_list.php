<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// liczba gp
$query="SELECT count(name) races,lower(name_short) id FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp and gp_season.date < DATE(NOW()) GROUP BY gp.name_short ORDER BY name";
$result = mysqli_query($dbhandle,$query);
$gpRacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_gp = $r["id"];
  $gpRacesTab[$tmp_id_gp] = $r["races"];
}

// lata rozgrywania
$query="SELECT CONCAT(MIN(season),' - ',MAX(season)) seasons,lower(name_short) id FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp and gp_season.date < DATE(NOW()) GROUP BY gp.name_short ORDER BY name";
$result = mysqli_query($dbhandle,$query);
$gpSeasonsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_gp = $r["id"];
  $gpSeasonsTab[$tmp_id_gp] = $r["seasons"];
}

$query="SELECT DISTINCT ";
if ($lang=='pl') {
  $query.="name,";
}else{
  $query.="name_en as name,";
}  
$query.="name_alias alias,lower(name_short) id, country_code country,'' races, '' seasons FROM gp ORDER BY name ASC";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["races"]=$gpRacesTab[$r["id"]]==null ? 0 : $gpRacesTab[$r["id"]];
  $r["seasons"]=$gpSeasonsTab[$r["id"]]==null ? 0 : $gpSeasonsTab[$r["id"]];
  $gpItems[] = $r;
}

// Response
$response = $gpItems;

print json_encode($response);
mysqli_free_result($result);
?>
