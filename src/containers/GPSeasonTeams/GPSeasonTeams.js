/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Grid,
  Divider,
  Label,
  Item,
  Image,
  Statistic,
  Message,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchGPSeasonTeams } from "../../actions/GPSeasonTeamsActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPSeasonTeams.less";

class GPSeasonTeams extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPSeasonTeams(props.match.params.year);
    props.fetchComboSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboSeasons.data?.find((e) => e.value == nextYear)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchGPSeasonTeams(nextYear);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/gp-season/teams/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderTeams() {
    const items = [];
    const { props } = this;
    const { teams } = props.gpseasonTeams.data;

    if (teams) {
      teams.forEach((item) => {
        const {
          id,
          aliasName,
          name,
          engine,
          country,
          champ,
          wins,
          points,
          podium,
          starts,
          qual,
          polePosition,
          bestLaps,
          seasonPlace,
          isClassified,
        } = item;
        const teamPic = `/build/images/teams/team_${id}_profile_logo.jpg`;
        const detailsLink = `/team/${aliasName}`;
        const record = (
          <Grid.Column
            key={id}
            textAlign="center"
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column verticalAlign="top">
                  <Segment basic>
                    <NavLink to={detailsLink}>
                      <Image
                        centered
                        src={teamPic}
                        alt={`${name} ${engine}`}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                    {champ && (
                      <Label attached="bottom" color="red">
                        {champ}
                      </Label>
                    )}
                  </Segment>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content>
                        <Flag name={country} /> {name}
                        <Header.Subheader>
                          {engine ? engine : name}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                    {props.match.params.year > 1957 ? (
                      <Statistic size="large">
                        <Statistic.Value>
                          {isClassified == 1 ? (
                            seasonPlace
                          ) : (
                            <FormattedMessage id={"app.stats.nc"} />
                          )}
                        </Statistic.Value>
                        <Statistic.Label>
                          <FormattedMessage id={"app.stats.place"} />
                        </Statistic.Label>
                      </Statistic>
                    ) : (
                      <Segment basic>
                        <Message className="transparent">
                          <p>
                            <FormattedMessage
                              id={"app.page.gpseason.teams.info2"}
                            />
                          </p>
                        </Message>
                      </Segment>
                    )}
                    <Divider hidden></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column textAlign="left" className="info-box-light">
                  <Item.Group divided>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.races"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/starts/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {starts}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.qual"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/qual/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {qual}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.wins"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/wins/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {wins}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.points"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/points/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {points}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.podium"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/podium/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {podium}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.polepos"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/polepos/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {polePosition}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.bestlaps"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/team-events/bestlaps/${aliasName}/-/-/-/-/-/-/${props.match.params.year}/1/`}
                            >
                              {bestLaps}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                  </Item.Group>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpseasonTeams.data || props.gpseasonTeams.loading) {
      return (
        <section
          id="gpseason-teams-details"
          name="gpseason-teams-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { teams } = props.gpseasonTeams.data;

    const linkPrev = `/gp-season/teams/${prevYear}`;
    const linkNext = `/gp-season/teams/${nextYear}`;
    if (teams.length === 0) {
      return (
        <section
          id="gpseason-teams-details"
          name="gpseason-teams-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.gpseason.teams.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.gpseason.teams.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage id={"app.page.gpseason.teams.info"} />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="gpseason-teams-details"
        name="gpseason-teams-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.gpseason.teams.title"}
              values={{ season: year }}
            />
          }
          subtitle={
            <FormattedMessage id={"app.page.gpseason.teams.subtitle"} />
          }
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <div className="buttons">
              <NavLink to={`/gp-season/${year}`} className="secondary">
                <FormattedMessage id={"app.button.summary"} />
              </NavLink>
              <NavLink to={`/gp-season/points/${year}`} className="secondary">
                <FormattedMessage id={"app.button.points"} />
              </NavLink>
              <NavLink to={`/gp-season/places/${year}`} className="secondary">
                <FormattedMessage id={"app.button.places"} />
              </NavLink>
              <NavLink to={`/gp-season/qual/${year}`} className="secondary">
                <FormattedMessage id={"app.button.grid"} />
              </NavLink>
              <NavLink to={`/gp-season/summary/${year}`} className="secondary">
                <FormattedMessage id={"app.button.statistics"} />
              </NavLink>
              <NavLink to={`/gp-season/drivers/${year}`} className="secondary">
                <FormattedMessage id={"app.button.drivers"} />
              </NavLink>
              <NavLink to={`/gp-season/teams/${year}`} className="primary">
                <FormattedMessage id={"app.button.teams"} />
              </NavLink>
            </div>
            <div className="gp-season-teams-filters">
              <Select
                ref={(ref) => {
                  this.comboSeason = ref;
                }}
                isLoading={isLoadingExternally}
                name="seasons"
                multi={multi}
                ignoreCase={ignoreCase}
                ignoreAccents={ignoreAccents}
                value={state.selectValue}
                onChange={this.onInputChange}
                options={props.comboSeasons.data}
                clearable={clearable}
                labelKey="label"
                valueKey="value"
                placeholder={formatMessage({
                  id: "app.placeholder.select.season",
                })}
                noResultsText={
                  <FormattedMessage id={"app.placeholder.no.results"} />
                }
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>
            <div className="gp-season-teams-list">
              <SectionPageHeader
                title={
                  <FormattedMessage
                    id={"app.page.gpseason.teams.list"}
                    values={{ count: teams.length }}
                  />
                }
                type="secondary"
              />
              <Grid>{this.renderTeams()}</Grid>
            </div>
          </Segment>
        </div>
      </section>
    );
  }
}

GPSeasonTeams.propTypes = {
  fetchGPSeasonTeams: PropTypes.func.isRequired,
  gpseasonTeams: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboSeasons: PropTypes.func.isRequired,
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpseasonTeams, comboSeasons }) {
  return { gpseasonTeams, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchGPSeasonTeams, fetchComboSeasons },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPSeasonTeams)
);
