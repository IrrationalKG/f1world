<?
header('Access-Control-Allow-Origin: *');

$id1=isset($_GET['driver1']) ? $_GET['driver1'] : null;
if ($id1==null) $id1=isset($_POST['driver1']) ? $_POST['driver1'] : null;
if ($id1=='-') $id1=null;

$id2=isset($_GET['driver2']) ? $_GET['driver2'] : null;
if ($id2==null) $id2=isset($_POST['driver2']) ? $_POST['driver2'] : null;
if ($id2=='-') $id2=null;

$season=isset($_GET['season']) ? $_GET['season'] : null;
if ($season==null) $season=isset($_POST['season']) ? $_POST['season'] : null;
if ($season=='-') $season=null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

/**
* KIEROWCA 1
*/
// pobranie liczby zawodników
if ($id1==null && $id2==null) {
  $query="SELECT COUNT(distinct drivers.id_driver) amount FROM drivers
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver
  WHERE drivers_gp_results.id_drivers_gp is not null";
  if ($season!=null){
    $query.=" AND drivers_gp_results.race_date>='$season-01-01' AND drivers_gp_results.race_date<='$season-12-31'";
  }else{
    $query.=" GROUP BY drivers.id_driver HAVING count(drivers_gp_results.id_drivers_gp) > 5";
  }
}else{
  $query="SELECT COUNT(drivers.id_driver) amount FROM drivers
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver
  WHERE drivers_gp_results.id_drivers_gp is not null";
  if ($season!=null){
    $query.=" AND drivers_gp_results.race_date>='$season-01-01' AND drivers_gp_results.race_date<='$season-12-31'";
  }
  $query.=" GROUP BY drivers.id_driver";
}

$result = mysqli_query($dbhandle,$query);
$amount=0;
while($r = mysqli_fetch_assoc($result)) {
  $amount = $amount + $r["amount"];
}

// pobranie losowo id zawodników
$idxDriver1 = rand(1, $amount);
$idxDriver2 = rand(1, $amount);
while($idxDriver1 === $idxDriver2){
  $idxDriver2 = rand(1, $amount);
}

// pobranie id kierowcy 1
if ($id1==null) {
  $query="SELECT drivers.id_driver id FROM drivers
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver
  WHERE drivers_gp_results.id_drivers_gp is not null";
  if ($season!=null){
    $query.=" AND drivers_gp_results.race_date>='$season-01-01' AND drivers_gp_results.race_date<='$season-12-31'";
    $query.=" GROUP BY drivers.id_driver ORDER BY surname asc";
  }else{
    $query.=" GROUP BY drivers.id_driver HAVING count(drivers_gp_results.id_drivers_gp) > 5 ORDER BY surname asc";
  }
  $result = mysqli_query($dbhandle,$query);
  $cnt=1;

  while($r = mysqli_fetch_assoc($result)) {
    if ($cnt === $idxDriver1) {
      $id1 = $r["id"];
      break;
    }
    $cnt+=1;
  }
}

// pobranie id kierowcy 2
if ($id2==null) {
  $query="SELECT drivers.id_driver id FROM drivers
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver
  WHERE drivers_gp_results.id_drivers_gp is not null";
  if ($season!=null){
    $query.=" AND drivers_gp_results.race_date>='$season-01-01' AND drivers_gp_results.race_date<='$season-12-31'";
    $query.=" GROUP BY drivers.id_driver ORDER BY surname asc";
  }else{
    $query.=" GROUP BY drivers.id_driver HAVING count(drivers_gp_results.id_drivers_gp) > 5 ORDER BY surname asc";
  }
  $result = mysqli_query($dbhandle,$query);
  $cnt=1;
  $id2=0;
  while($r = mysqli_fetch_assoc($result)) {
    if ($cnt === $idxDriver2) {
      $id2 = $r["id"];
      break;
    }
    $cnt+=1;
  }
}

$driversCompareItems["id1"]=$id1;
$driversCompareItems["id2"]=$id2;

// pobranie kierowcow do wyboru
$query="SELECT drivers.id_driver value,CONCAT(surname,' ',name) label FROM drivers
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver
WHERE drivers_gp_results.id_drivers_gp is not null";
if ($season!=null){
  $query.=" AND drivers_gp_results.race_date>='$season-01-01' AND drivers_gp_results.race_date<='$season-12-31'";
}
$query.=" GROUP BY drivers.id_driver ORDER BY surname asc";
$result = mysqli_query($dbhandle,$query);
$drivers;
while($r = mysqli_fetch_assoc($result)) {
  $drivers[] = $r;
}


$driver1="";
// informacje o kierowcy
$query="SELECT alias,id_driver id, name, surname,";
if ($lang=='pl') {
  $query.="country,";
}else{
  $query.="country_en as country,";
}  
$query.="country_code countryCode, picture FROM drivers WHERE id_driver=$id1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $driver1 = $r;
}

/**
* Statystyki
**/
$statsItems="";

//sezony
$query="SELECT MAX(SUBSTRING(race_date,1,4)) maxYear,MIN(SUBSTRING(race_date,1,4)) minYear, count(DISTINCT SUBSTRING(race_date,1,4)) seasonsNo FROM drivers_gp_results WHERE id_driver=$id1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["seasons"] = $r["minYear"]." - ".$r["maxYear"];
  $statsItems["seasonsNo"] = $r["seasonsNo"];
}

//najlepszy wynik
$query="SELECT count(place) amount, MIN(place) as bestResult, GROUP_CONCAT(DISTINCT season ORDER BY season DESC SEPARATOR ', ') seasons
FROM drivers_class where id_driver=$id1 and season<(SELECT max(season) FROM gp_season) group by place LIMIT 1";
$resultM = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($resultM)) {
  if ($r["bestResult"] === '1'){
    $statsItems["bestResult"] = "MŚ (".$r["amount"]."x)";
  }else {
    $statsItems["bestResult"] = $r["bestResult"].". miejsce";
  }
}

//zwyciestwa
$query="SELECT COUNT(*) wins FROM drivers_gp_results WHERE id_driver=$id1 AND race_pos=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["wins"] = $r["wins"];
}

//punkty
$query="SELECT TRUNCATE(ROUND(SUM(points),2),2) points FROM drivers_class WHERE id_driver=$id1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["points"] = $r["points"];
}

//podium
$query="SELECT COUNT(*) podium FROM drivers_gp_results WHERE id_driver=$id1 AND race_pos<4";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["podium"] = $r["podium"];
}

//pole positions
$query="SELECT COUNT(*) polepos FROM drivers_pp_results WHERE id_driver=$id1 AND qual_pos=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["polepos"] = $r["polepos"];
}

//najlepsze okrazenia
$query="SELECT COUNT(*) bestlaps FROM drivers_gp_results WHERE id_driver=$id1 AND race_best_lap=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["bestlaps"] = $r["bestlaps"];
}

//liczba grand prix
$query="SELECT COUNT(*) starts FROM drivers_gp_results WHERE id_driver=$id1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["starts"] = $r["starts"];
}

//liczba kwalifikacji
$query="SELECT COUNT(*) qual FROM drivers_pp_results WHERE id_driver=$id1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["qual"] = $r["qual"];
}

//wyscigi ukonczone
$query="SELECT COUNT(*) completed FROM drivers_gp_results WHERE id_driver=$id1 AND race_completed=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["completed"] = $r["completed"];
}

//wys nieukonczone
$query="SELECT COUNT(*) incomplete FROM drivers_gp_results WHERE id_driver=$id1 AND race_completed=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["incomplete"] = $r["incomplete"];
}

//dyskwalifikacje
$query="SELECT COUNT(*) disqualifications FROM drivers_gp_results WHERE id_driver=$id1 AND race_add_info LIKE 'dyskwalifikacja'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["disqualifications"] = $r["disqualifications"];
}

$driver1["stats"]=$statsItems;

/**
* Ranking - miejsca
**/
$rankingItems="";

//zwyciestwa
$query="SELECT COUNT(*) wins, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["wins"] = $pos;
    break;
  }
  $pos+=1;
}

//punkty
$query="SELECT TRUNCATE(ROUND(SUM(points),2),2) points, drivers.id_driver,surname FROM drivers_class,drivers WHERE drivers_class.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["points"] = $pos;
    break;
  }
  $pos+=1;
}

//podium
$query="SELECT COUNT(*) podium, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos>0 AND race_pos<4 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["podium"] = $pos;
    break;
  }
  $pos+=1;
}

//pole positions
$query="SELECT COUNT(*) polepos, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver AND qual_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["polepos"] = $pos;
    break;
  }
  $pos+=1;
}

//najlepsze okrazenia
$query="SELECT COUNT(*) bestlaps, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_best_lap=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["bestlaps"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba grand prix
$query="SELECT COUNT(*) starts, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["starts"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba kwalifikacji
$query="SELECT COUNT(*) qual, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["qual"] = $pos;
    break;
  }
  $pos+=1;
}

//wyscigi ukonczone
$query="SELECT COUNT(*) completed, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["completed"] = $pos;
    break;
  }
  $pos+=1;
}

//wys nieukonczone
$query="SELECT COUNT(*) incomplete, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=0 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["incomplete"] = $pos;
    break;
  }
  $pos+=1;
}

//dyskwalifikacje
$query="SELECT COUNT(*) disqualifications, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_add_info LIKE 'dyskwalifikacja' group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id1){
    $rankingItems["disqualifications"] = $pos;
    break;
  }
  $pos+=1;
}

$driver1["ranking"]=$rankingItems;

// wyniki  gp w sezonie
if ($season) {
  $query="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name_alias alias,";
  if ($lang=='pl') {
    $query.="gp.name gp,race_add_info info,";
  }else{
    $query.="gp.name_en gp,race_add_info_en info,";
  }  
  $query.="grid_pos grid, race_pos place, race_completed completed,race_time time,race_best_lap bestLap, excluded_from_class excluded, race_points points
  from gp_season 
  left join gp on gp.id_gp=gp_season.id_gp 
  left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp and drivers_gp_results.id_driver=$id1 and drivers_gp_results.season='$season'
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = gp.id_gp AND drivers_gp_starting_grid.id_driver = $id1 AND drivers_gp_starting_grid.season = '$season'
  where gp_season.season=$season group by gp.id_gp order by gp_season.sort, race_pos";
  $result = mysqli_query($dbhandle,$query);
  $gpResultsItems=array();
  while($r = mysqli_fetch_assoc($result)) {
    $gpResultsItems[] = $r;
 }
 $driver1["gpResults"]=$gpResultsItems;
}
/**
* KIEROWCA 2
*/

$driver2="";
// informacje o kierowcy
$query="SELECT alias,id_driver id, name, surname, country, country_code countryCode, picture FROM drivers WHERE id_driver=$id2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $driver2 = $r;
}

/**
* Statystyki
**/
$statsItems="";

//sezony
$query="SELECT MAX(SUBSTRING(race_date,1,4)) maxYear,MIN(SUBSTRING(race_date,1,4)) minYear, count(DISTINCT SUBSTRING(race_date,1,4)) seasonsNo FROM drivers_gp_results WHERE id_driver=$id2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["seasons"] = $r["minYear"]." - ".$r["maxYear"];
  $statsItems["seasonsNo"] = $r["seasonsNo"];
}

//najlepszy wynik
$query="SELECT count(place) amount, MIN(place) as bestResult, GROUP_CONCAT(DISTINCT season ORDER BY season DESC SEPARATOR ', ') seasons
FROM drivers_class where id_driver=$id2 and season<(SELECT max(season) FROM gp_season) group by place LIMIT 1";
$resultM = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($resultM)) {
  if ($r["bestResult"] === '1'){
    $statsItems["bestResult"] = "MŚ (".$r["amount"]."x)";
  }else {
    $statsItems["bestResult"] = $r["bestResult"].". miejsce";
  }
}

//zwyciestwa
$query="SELECT COUNT(*) wins FROM drivers_gp_results WHERE id_driver=$id2 AND race_pos=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["wins"] = $r["wins"];
}

//punkty
$query="SELECT TRUNCATE(ROUND(SUM(points),2),2) points FROM drivers_class WHERE id_driver=$id2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["points"] = $r["points"];
}

//podium
$query="SELECT COUNT(*) podium FROM drivers_gp_results WHERE id_driver=$id2 AND race_pos<4";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["podium"] = $r["podium"];
}

//pole positions
$query="SELECT COUNT(*) polepos FROM drivers_pp_results WHERE id_driver=$id2 AND qual_pos=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["polepos"] = $r["polepos"];
}

//najlepsze okrazenia
$query="SELECT COUNT(*) bestlaps FROM drivers_gp_results WHERE id_driver=$id2 AND race_best_lap=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["bestlaps"] = $r["bestlaps"];
}

//liczba grand prix
$query="SELECT COUNT(*) starts FROM drivers_gp_results WHERE id_driver=$id2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["starts"] = $r["starts"];
}

//liczba kwalifikacji
$query="SELECT COUNT(*) qual FROM drivers_pp_results WHERE id_driver=$id2";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["qual"] = $r["qual"];
}

//wyscigi ukonczone
$query="SELECT COUNT(*) completed FROM drivers_gp_results WHERE id_driver=$id2 AND race_completed=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["completed"] = $r["completed"];
}

//wys nieukonczone
$query="SELECT COUNT(*) incomplete FROM drivers_gp_results WHERE id_driver=$id2 AND race_completed=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["incomplete"] = $r["incomplete"];
}

//dyskwalifikacje
$query="SELECT COUNT(*) disqualifications FROM drivers_gp_results WHERE id_driver=$id2 AND race_add_info LIKE 'dyskwalifikacja'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["disqualifications"] = $r["disqualifications"];
}

$driver2["stats"]=$statsItems;

/**
* Ranking - miejsca
**/
$rankingItems="";

//zwyciestwa
$query="SELECT COUNT(*) wins, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["wins"] = $pos;
    break;
  }
  $pos+=1;
}

//punkty
$query="SELECT TRUNCATE(ROUND(SUM(points),2),2) points, drivers.id_driver,surname FROM drivers_class,drivers WHERE drivers_class.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["points"] = $pos;
    break;
  }
  $pos+=1;
}

//podium
$query="SELECT COUNT(*) podium, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos>0 AND race_pos<4 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["podium"] = $pos;
    break;
  }
  $pos+=1;
}

//pole positions
$query="SELECT COUNT(*) polepos, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver AND qual_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["polepos"] = $pos;
    break;
  }
  $pos+=1;
}

//najlepsze okrazenia
$query="SELECT COUNT(*) bestlaps, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_best_lap=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["bestlaps"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba grand prix
$query="SELECT COUNT(*) starts, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["starts"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba kwalifikacji
$query="SELECT COUNT(*) qual, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["qual"] = $pos;
    break;
  }
  $pos+=1;
}

//wyscigi ukonczone
$query="SELECT COUNT(*) completed, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["completed"] = $pos;
    break;
  }
  $pos+=1;
}

//wys nieukonczone
$query="SELECT COUNT(*) incomplete, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=0 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["incomplete"] = $pos;
    break;
  }
  $pos+=1;
}

//dyskwalifikacje
$query="SELECT COUNT(*) disqualifications, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_add_info LIKE 'dyskwalifikacja' group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id2){
    $rankingItems["disqualifications"] = $pos;
    break;
  }
  $pos+=1;
}

// wyniki  gp w sezonie
if ($season) {
  $query="SELECT gp.country_code name, lower(gp.name_short) nameShort, gp.name_alias alias,";
  if ($lang=='pl') {
    $query.="gp.name gp,race_add_info info,";
  }else{
    $query.="gp.name_en gp,race_add_info_en info,";
  }  
  $query.="grid_pos grid, race_pos place, race_completed completed,race_time time,race_best_lap bestLap, excluded_from_class excluded, race_points points
  from gp_season 
  left join gp on gp.id_gp=gp_season.id_gp 
  left join drivers_gp_results on drivers_gp_results.id_gp=gp.id_gp and drivers_gp_results.id_driver=$id2 and drivers_gp_results.season='$season'
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = gp.id_gp AND drivers_gp_starting_grid.id_driver = $id2 AND drivers_gp_starting_grid.season = '$season'
  where gp_season.season=$season group by gp.id_gp order by gp_season.sort, race_pos";
  $result = mysqli_query($dbhandle,$query);
  $gpResultsItems=array();
  while($r = mysqli_fetch_assoc($result)) {
    $gpResultsItems[] = $r;
 }
 $driver2["gpResults"]=$gpResultsItems;
}

$driver2["ranking"]=$rankingItems;

$driversCompareItems["season"]=$season;
$driversCompareItems["amount"]=$amount;
$driversCompareItems["driver1"]=$driver1;
$driversCompareItems["driver2"]=$driver2;
$driversCompareItems["drivers"]=$drivers;
$driversCompareItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $driversCompareItems;

print json_encode($response);
mysqli_free_result($result);
?>
