<?
header('Access-Control-Allow-Origin: *');

$page=isset($_GET['page']) ? $_GET['page'] : null;
if ($page==null) $page=isset($_POST['page']) ? $_POST['page'] : null;
if ($page==null) $page=-1;
if ($page=='-') $page=-1;

$stats=isset($_GET['stats']) ? $_GET['stats'] : null;
if ($stats==null) $stats=isset($_POST['stats']) ? $_POST['stats'] : null;
if ($stats==null) $stats=-1;
if ($stats=='-') $stats=-1;

$position=isset($_GET['pos']) ? $_GET['pos'] : null;
if ($position==null) $position=isset($_POST['pos']) ? $_POST['pos'] : null;
if ($position==null) $position=-1;
if ($position=='-') $position=-1;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;
if ($gp==null) $gp=-1;
if ($gp=='-') $gp=-1;

$circuit=isset($_GET['circuit']) ? $_GET['circuit'] : null;
if ($circuit==null) $circuit=isset($_POST['circuit']) ? $_POST['circuit'] : null;
if ($circuit==null) $circuit=-1;
if ($circuit=='-') $circuit=-1;

$beginYear=isset($_GET['beginYear']) ? $_GET['beginYear'] : null;
if ($beginYear==null) $beginYear=isset($_POST['beginYear']) ? $_POST['beginYear'] : null;
if ($beginYear==null) $beginYear=-1;
if ($beginYear=='-') $beginYear=-1;

$endYear=isset($_GET['endYear']) ? $_GET['endYear'] : null;
if ($endYear==null) $endYear=isset($_POST['endYear']) ? $_POST['endYear'] : null;
if ($endYear==null) $endYear=-1;
if ($endYear=='-') $endYear=-1;

$status=isset($_GET['status']) ? $_GET['status'] : null;
if ($status==null) $status=isset($_POST['status']) ? $_POST['status'] : null;
if ($status==null) $status=-1;
if ($status=='-') $status=-1;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT name, display_name displayName, show_link showLink from stats where alias='$stats'";
$result = mysqli_query($dbhandle,$query);
$statsName="";
$statsLabel="";
$showLink="";
while($r = mysqli_fetch_assoc($result)) {
  $statsName = $r["name"];
  $statsLabel = $r["displayName"];
  $showLink = $r["showLink"];
}
$pages=1;
$limit=30;
$offset=(($page-1)*$limit);

$query_select="";
$query_conditions="";
$query_order="";

/**
 * Wyscig [fixed]
 */
//wyscigi
if ($stats=='starts') {
  $query_select="SELECT COUNT(distinct CONCAT(drivers_gp_involvements.id_gp, drivers_gp_involvements.season)) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_involvements,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_involvements.id_team=teams.id_team AND drivers_gp_involvements.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp IS NOT NULL";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. miejsc punktowanych
if ($stats=='points-places') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. miejsc niepunktowanych
if ($stats=='no-points-places') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND d.team=drivers_gp_results.team)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. zakwalifikowany
if ($stats=='not-qualified') {
  $query_select="SELECT COUNT(distinct drivers_pp_results.id_drivers_pp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp";
	$query_conditions=" WHERE drivers_pp_results.id_team=teams.id_team AND drivers_pp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_pp_results.not_qualified=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. nie wystartowal
if ($stats=='not-started') {
  $query_select="SELECT COUNT(distinct drivers_pp_results.id_drivers_pp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp";
	$query_conditions=" WHERE drivers_pp_results.id_team=teams.id_team AND drivers_pp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_pp_results.not_started=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. sklasyfikowanych
if ($stats=='completed') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. niesklasyfikowanych
if ($stats=='incomplete') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. ukończonych wyścigów
if ($stats=='finished') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_finished=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. nieukończonych wyścigów
if ($stats=='retirement') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. dyskwalifikacji
if ($stats=='disq') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND disq=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. kar
if ($stats=='penalty') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND penalty=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. miejsc
if ($stats=='race-places') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_pos=$position AND race_completed=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. pp, naj. okrąż. i zwycięstw
if ($stats=='polepos-bestlaps-wins') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. pp i zwycięstw
if ($stats=='polepos-wins') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp"; 
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. naj. okrąż. i zwycięstw
if ($stats=='bestlaps-wins') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//srednie miejsce w gp
if ($stats=='average-place') {
  $query_select="SELECT ROUND(AVG(race_pos) ,2) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_pp_results";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_drivers_pp=drivers_pp_results.id_drivers_pp";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_order=" GROUP BY teams.team ORDER BY amount asc, teams.name";
}
//srednie punkty w gp
if ($stats=='average-points') {
  $query_select="SELECT ROUND(((SUM(COALESCE(drivers_gp_results.race_team_points,0))+SUM(COALESCE(drivers_sprint_results.sprint_points,0)))/COUNT(distinct race_date)),2) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON drivers_gp_involvements.id_team = teams.id_team 
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp";
  $query_conditions=" WHERE drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}

/**
 * Grid [fixed]
 */
//naj. startów z miejsca
if ($stats=='grid-places') {
  $query_select="SELECT COUNT(id_starting_grid) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_starting_grid.id_team=teams.id_team AND drivers_gp_starting_grid.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. zwyciestw z miejsca
if ($stats=='wins-from-place') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. punktów z miejsca
if ($stats=='points-from-place') {
  $query_select="SELECT SUM(COALESCE(drivers_gp_results.race_team_points,0))+SUM(COALESCE(drivers_sprint_results.sprint_points,0)) AS amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON drivers_gp_involvements.id_team = teams.id_team 
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN drivers_gp_starting_grid ON 
  drivers_gp_starting_grid.id_team=drivers_gp_involvements.id_team 
  and drivers_gp_starting_grid.id_gp=drivers_gp_involvements.id_gp 
  and drivers_gp_starting_grid.id_driver=drivers_gp_involvements.id_driver 
  AND drivers_gp_starting_grid.season=drivers_gp_involvements.season";
  $query_conditions=" WHERE drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. podium z miejsca
if ($stats=='podium-from-place') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. sklasyfikowany z miejsca
if ($stats=='completed-from-place') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. niesklasyfikowany z miejsca
if ($stats=='incomplete-from-place') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. ukonczonych z miejsca
if ($stats=='finished-from-place') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. nie ukonczonych z miejsca
if ($stats=='retirement-from-place') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa
if ($stats=='race-pos-higher-than-grid') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_conditions.=" AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa - procent
if ($stats=='race-pos-higher-than-grid-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
  $query_conditions.=" AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//Najwięcej wyścigów ukończonych na pozycji nizszej niż startowa
if ($stats=='race-pos-lower-than-grid') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa - procent
if ($stats=='race-pos-lower-than-grid-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
// Wygrana z najniższej pozycji startowej
if ($stats=='race-win-lowest-grid') {
  $query_select="SELECT MAX(drivers_gp_starting_grid.grid_pos) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
// Podium z najniższej pozycji startowej
if ($stats=='race-podium-lowest-grid') {
  $query_select="SELECT MAX(drivers_gp_starting_grid.grid_pos) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}

/**
 * Pole Position [fixed]
 */
//pole position
if ($stats=='polepos') {
  $query_select="SELECT COUNT(id_starting_grid) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_starting_grid,gp,drivers_gp_results";
	$query_conditions=" WHERE drivers_gp_starting_grid.id_team=teams.id_team AND drivers_gp_starting_grid.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team 
  AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver 
  AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp 
  AND drivers_gp_starting_grid.season=drivers_gp_results.season
  AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver = 0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//pole position - procent
if ($stats=='polepos-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver = 0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='polepos-no-win-gp-number') {
  $query_select="SELECT COUNT(DISTINCT drivers_pp_results.qual_date) as amount, lower(teams.team) id, teams.name as name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_starting_grid,gp,drivers_gp_results,drivers_pp_results";
  $query_conditions=" WHERE drivers_gp_starting_grid.id_team=teams.id_team 
  AND drivers_gp_starting_grid.id_gp=gp.id_gp 
  AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team 
  AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver 
  AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp 
  AND drivers_gp_starting_grid.season=drivers_gp_results.season
  AND drivers_pp_results.id_team=drivers_gp_starting_grid.id_team 
  AND drivers_pp_results.id_gp=drivers_gp_starting_grid.id_gp 
  AND drivers_pp_results.id_driver=drivers_gp_starting_grid.id_driver 
  AND drivers_pp_results.season=drivers_gp_starting_grid.season 
  AND (drivers_gp_starting_grid.is_pp=0 OR drivers_gp_starting_grid.id_starting_grid is null) AND drivers_gp_results.co_driver=0";    
  $query_conditions.=" AND drivers_pp_results.qual_date NOT IN (SELECT DISTINCT dpp.qual_date FROM drivers_gp_starting_grid dsg, drivers_pp_results dpp WHERE dsg.is_pp=1 AND dsg.id_team=dpp.id_team AND dsg.id_driver=dpp.id_driver AND dsg.id_gp=dpp.id_gp AND dsg.season=dpp.season AND dsg.team=teams.team AND dpp.team=teams.team)";
  $query_order.=" GROUP BY teams.team HAVING count(DISTINCT drivers_pp_results.qual_date) > 0 ORDER BY amount DESC, teams.name";
}
//pole position - procent
if ($stats=='polepos-no-win-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_pp_results.qual_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_pp_results.qual_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid,drivers_pp_results";
	$query_conditions=" WHERE drivers_gp_starting_grid.id_team=teams.id_team 
  AND drivers_gp_starting_grid.id_gp=gp.id_gp 
  AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team 
  AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver 
  AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp 
  AND drivers_gp_starting_grid.season=drivers_gp_results.season
  AND drivers_pp_results.id_team=drivers_gp_starting_grid.id_team 
  AND drivers_pp_results.id_gp=drivers_gp_starting_grid.id_gp 
  AND drivers_pp_results.id_driver=drivers_gp_starting_grid.id_driver 
  AND drivers_pp_results.season=drivers_gp_starting_grid.season 
  AND (drivers_gp_starting_grid.is_pp=0 OR drivers_gp_starting_grid.id_starting_grid is null) AND drivers_gp_results.co_driver=0";
  $query_conditions.=" AND drivers_pp_results.qual_date NOT IN (SELECT DISTINCT dpp.qual_date FROM drivers_gp_starting_grid dsg, drivers_pp_results dpp WHERE dsg.is_pp=1 AND dsg.id_team=dpp.id_team AND dsg.id_driver=dpp.id_driver AND dsg.id_gp=dpp.id_gp AND dsg.season=dpp.season AND dsg.team=teams.team AND dpp.team=teams.team)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
//naj. zwycięstw z pp
if ($stats=='wins-from-polepos') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. punktów z pp
if ($stats=='points-from-polepos') {
  $query_select="SELECT SUM(COALESCE(drivers_gp_results.race_team_points,0))+SUM(COALESCE(drivers_sprint_results.sprint_points,0)) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE drivers_gp_results.race_team_points > 0  AND drivers_gp_results.race_points_ignored=0";
  $query_conditions.=" AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. podium z pp
if ($stats=='podium-from-polepos') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver = 0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. sklasyfikowany z pp
if ($stats=='completed-from-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. niesklasyfikowany z pp
if ($stats=='incomplete-from-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//ukonczone z pp
if ($stats=='finished-from-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//nieukonczone z pp
if ($stats=='retirement-from-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. zwycięstw spoza pp
if ($stats=='wins-outside-polepos') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. podium spoza pp
if ($stats=='podium-outside-polepos') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. punktów spoza pp
if ($stats=='points-outside-polepos') {
  $query_select="SELECT SUM(race_team_points) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj.sklasyfikowany spoza pp
if ($stats=='completed-outside-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. niesklasyfikwoany spoza pp
if ($stats=='incomplete-outside-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. uk. wyścigów spoza pp
if ($stats=='finished-outside-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. nie uk. wyścigów spoza pp
if ($stats=='retirement-outside-polepos') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_starting_grid.id_team=drivers_gp_results.id_team AND drivers_gp_starting_grid.id_driver=drivers_gp_results.id_driver AND drivers_gp_starting_grid.id_gp=drivers_gp_results.id_gp AND drivers_gp_starting_grid.season=drivers_gp_results.season";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='polepos-first-win-diff-asc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_pp_results.qual_date),(SELECT MIN(d.qual_date) FROM drivers_pp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='polepos-first-win-diff-desc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_pp_results.qual_date),(SELECT MIN(d.qual_date) FROM drivers_pp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";     
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='polepos-first-win-gp-number-asc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.team=teams.team AND d2.qual_date<=MIN(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='polepos-first-win-gp-number-desc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.team=teams.team AND d2.qual_date<=MIN(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='polepos-last-win-diff-asc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.qual_date) FROM drivers_pp_results d WHERE d.team=teams.team),MAX(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";     
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='polepos-last-win-diff-desc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.qual_date) FROM drivers_pp_results d WHERE d.team=teams.team),MAX(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='polepos-last-win-gp-number-asc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.team=teams.team AND d2.qual_date>MAX(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";      
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='polepos-last-win-gp-number-desc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.qual_date) FROM drivers_pp_results d2 WHERE d2.team=teams.team AND d2.qual_date>MAX(drivers_pp_results.qual_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid,drivers_gp_results";
  $query_conditions=" WHERE drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp AND drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.co_driver=0";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='polepos-percent-by-season') {
  $query_select="SELECT ROUND(COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) / (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_pp_results d WHERE d.season=drivers_gp_starting_grid.season AND d.team=teams.team) *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (',
  drivers_gp_starting_grid.season,
  ' - ',
  ' ',
  COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid),
  '/',
  (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_pp_results d WHERE d.season=drivers_gp_starting_grid.season AND d.team=teams.team),
  ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_starting_grid.season ORDER BY amount DESC, teams.name";
}
if ($stats=='polepos-number-by-season') {
  $query_select="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_starting_grid.season,
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp,drivers_gp_starting_grid";
  $query_conditions=" WHERE drivers_pp_results.id_team=teams.id_team AND gp.id_gp=drivers_pp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_starting_grid.id_driver = drivers_pp_results.id_driver AND drivers_gp_starting_grid.id_team = drivers_pp_results.id_team AND drivers_gp_starting_grid.id_gp = drivers_pp_results.id_gp AND drivers_gp_starting_grid.season = drivers_pp_results.season";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_starting_grid.season ORDER BY amount DESC, teams.name";
}

/**
 * Wygrane [fixed]
 */
//zwyciestwa
if ($stats=='wins') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//zwyciestwa - procent
if ($stats=='wins-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='wins-no-win-gp-number') {
  $query_select="SELECT count(DISTINCT drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,
  teams.country country,
  teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos>1";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_pos=1 AND d.team=drivers_gp_results.team)";
  $query_order.=" GROUP BY teams.team HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, teams.name";
}
if ($stats=='wins-no-win-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT distinct dgr.race_date from drivers_gp_results dgr where dgr.race_pos=1 AND dgr.team=drivers_gp_results.team)";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='wins-first-win-diff-asc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='wins-first-win-diff-desc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='wins-first-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='wins-first-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='wins-last-win-diff-asc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='wins-last-win-diff-desc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='wins-last-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='wins-last-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='wins-number-by-season') {
  $query_select="SELECT COUNT(drivers_gp_results.race_date) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}
if ($stats=='wins-percent-by-season') {
  $query_select="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team) *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ' - ',
  ' ',
  COUNT(DISTINCT drivers_gp_results.race_date),
  '/',
  (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team),
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}

/**
 * Podium [fixed]
 */
//podium
if ($stats=='podium') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//podium procent
if ($stats=='podium-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.id_drivers_gp) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.id_drivers_gp) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.id_drivers_gp),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND co_driver=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='podium-no-win-gp-number') {
  $query_select="SELECT count(DISTINCT drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos>3";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_pos<4 AND d.team=drivers_gp_results.team)";  
  $query_order.=" GROUP BY teams.team HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, teams.name";
}
if ($stats=='podium-no-win-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.id_drivers_gp) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.id_drivers_gp) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT distinct dgr.race_date from drivers_gp_results dgr where dgr.race_pos<4 AND dgr.team=drivers_gp_results.team)";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='podium-first-win-diff-asc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='podium-first-win-diff-desc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='podium-first-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos<4  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='podium-first-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos<4  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='podium-last-win-diff-asc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='podium-last-win-diff-desc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='podium-last-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos<4  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='podium-last-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_pos<4  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='podium-number-by-season') {
  $query_select="SELECT COUNT(drivers_gp_results.race_date) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}
if ($stats=='podium-percent-by-season') {
  $query_select="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team) *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ' - ',
  ' ',
  COUNT(DISTINCT drivers_gp_results.race_date),
  '/',
  (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team),
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}

/**
 * Punkty [fixed]
 */
//punkty
if ($stats=='points') {
  if ($gp!=-1 || $circuit!=-1){
    $query_select="SELECT SUM(coalesce(drivers_gp_results.race_team_points,0)) + SUM(coalesce(drivers_sprint_results.sprint_points,0)) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
    teams.country_code countryCode,";
    if ($lang=='pl') {
      $query_select.="teams.country,";
    }else{
      $query_select.="teams.country_en country,";
    }  
    $query_select.="teams.picture,'' place,
    (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
    FROM drivers_gp_involvements 
  	LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  	LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  	LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  	LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  	LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  	$query_conditions=" WHERE (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0) AND drivers_gp_results.race_points_ignored=0";
    $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
  }else{
    $query_select="SELECT SUM(points) as amount,SUM(points_class) as amountClass,lower(teams.team) id, teams.name, teams.alias_name alias, 
    teams.country_code countryCode,";
    if ($lang=='pl') {
      $query_select.="teams.country,";
    }else{
      $query_select.="teams.country_en country,";
    }  
    $query_select.="teams.picture,'' place,
    (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active";
    $query_select.=" from teams_class,teams";
    $query_conditions=" WHERE teams_class.id_team=teams.id_team AND teams_class.points>0";
    $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
  }
}
if ($stats=='points-scored-gp-number') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//punkty - procent
if ($stats=='points-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp    
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.id_driver=drivers_gp_results.id_driver AND drivers_sprint_results.season=drivers_gp_results.season";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
//naj. miejsc niepunktowanych
if ($stats=='points-no-scored-gp-number') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_involvements.season>1957 AND d.team=drivers_gp_results.team)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//punkty - procent
if ($stats=='points-no-scored-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  COUNT(distinct drivers_gp_results.race_date) races,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp    
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.id_driver=drivers_gp_results.id_driver AND drivers_sprint_results.season=drivers_gp_results.season";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_involvements.season>1957 AND d.team=drivers_gp_results.team)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, races DESC, teams.name";
}
if ($stats=='points-first-win-gp-number-asc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.team=teams.team AND d2.race_date<=MIN(drivers_gp_results.race_date) AND d2.season>1957) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0) AND drivers_gp_involvements.season>1957 AND drivers_gp_results.race_points_ignored=0";
  $query_order=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='points-first-win-gp-number-desc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.team=teams.team AND d2.race_date<=MIN(drivers_gp_results.race_date) AND d2.season>1957) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0) AND drivers_gp_involvements.season>1957 AND drivers_gp_results.race_points_ignored=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='points-last-win-gp-number-asc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.team=teams.team AND d2.race_date>MAX(drivers_gp_results.race_date) AND d2.season>1957) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0) AND drivers_gp_involvements.season>1957 AND drivers_gp_results.race_points_ignored=0";
  $query_order=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='points-last-win-gp-number-desc') {
  $query_select="SELECT (SELECT count(DISTINCT d2.race_date) FROM drivers_gp_results d2 WHERE d2.team=teams.team AND d2.race_date>MAX(drivers_gp_results.race_date) AND d2.season>1957) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0) AND drivers_gp_involvements.season>1957 AND drivers_gp_results.race_points_ignored=0";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='points-no-win-gp-number') {
  $query_select="SELECT count(DISTINCT drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team";
  $query_conditions=" WHERE (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_involvements.season>1957";
  $query_conditions.=" AND teams.team NOT IN (SELECT DISTINCT dr.team FROM drivers_gp_results dr WHERE dr.race_team_points>0)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}

/**
 * Najlepsze okrazenia [fixed]
 */
//naj. okrąż.
if ($stats=='bestlaps') {
  $query_select="SELECT COUNT(distinct drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//naj. okrąż. - procent
if ($stats=='bestlaps-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='bestlaps-no-win-gp-number') {
  $query_select="SELECT count(DISTINCT drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_best_lap=1 AND d.team=drivers_gp_results.team)";
  $query_order.=" GROUP BY teams.team HAVING count(DISTINCT drivers_gp_results.race_date) > 0 ORDER BY amount DESC, teams.name";
}
//naj. okrąż. - procent
if ($stats=='bestlaps-no-win-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_gp_results.race_date),'/',(SELECT COUNT(DISTINCT d.race_date) FROM drivers_gp_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_gp_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0";
  $query_conditions.=" AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_results d WHERE d.race_best_lap=1 AND d.team=drivers_gp_results.team)";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='bestlaps-first-win-diff-asc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='bestlaps-first-win-diff-desc') {
  $query_select="SELECT DATEDIFF(MIN(drivers_gp_results.race_date),(SELECT MIN(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='bestlaps-first-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_best_lap=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='bestlaps-first-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MIN(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_best_lap=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date<=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='bestlaps-last-win-diff-asc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='bestlaps-last-win-diff-desc') {
  $query_select="SELECT DATEDIFF((SELECT MAX(d.race_date) FROM drivers_gp_results d WHERE d.team=teams.team),MAX(drivers_gp_results.race_date)) as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='bestlaps-last-win-gp-number-asc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_best_lap=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='bestlaps-last-win-gp-number-desc') {
  $query_select="SELECT count(distinct drivers_gp_results.race_date)-1 as amount,
  lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
    from drivers_gp_results
  LEFT JOIN teams ON drivers_gp_results.id_team=teams.id_team
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  JOIN (SELECT MAX(d.race_date) as minDate, team FROM drivers_gp_results d WHERE d.race_best_lap=1  GROUP BY d.team) as t ON drivers_gp_results.team=t.team";
  $query_conditions=" WHERE drivers_gp_results.race_date>=t.minDate";
  $query_order.=" GROUP BY teams.team ORDER BY amount DESC, teams.name";
}
if ($stats=='bestlaps-number-by-season') {
  $query_select="SELECT COUNT(drivers_gp_results.race_date) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}
if ($stats=='bestlaps-percent-by-season') {
  $query_select="SELECT ROUND(COUNT(DISTINCT drivers_gp_results.race_date) / (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team) *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,
  ' (', 
  drivers_gp_results.season,
  ' - ',
  ' ',
  COUNT(DISTINCT drivers_gp_results.race_date),
  '/',
  (SELECT COUNT(DISTINCT d.id_gp) FROM drivers_gp_results d WHERE d.season=drivers_gp_results.season AND d.team=teams.team),
  ')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
  $query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND gp.id_gp=drivers_gp_results.id_gp";    
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1";
  $query_order.=" GROUP BY teams.team, drivers_gp_results.season ORDER BY amount DESC, teams.name";
}

/**
 * Sprint [fixed]
 */
//sprinty
if ($stats=='sprints') {
	$query_select="SELECT COUNT(distinct drivers_sprint_results.sprint_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty wygrane
if ($stats=='sprint-wins') {
	$query_select="SELECT COUNT(distinct drivers_sprint_results.sprint_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_pos=1";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty - wygrane procent
if ($stats=='sprints-wins-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_sprint_results.sprint_date) / (SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_sprint_results.sprint_date),'/',(SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_pos=1";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty podium
if ($stats=='sprint-podiums') {
	$query_select="SELECT COUNT(distinct drivers_sprint_results.sprint_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_pos<4";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty - podium procent
if ($stats=='sprints-podiums-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_sprint_results.sprint_date) / (SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_sprint_results.sprint_date),'/',(SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_pos<4";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty - punkty
if ($stats=='sprint-points') {
	$query_select="SELECT COALESCE(SUM(drivers_sprint_results.sprint_points),0) as amount,lower(teams.team) id, teams.name, teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_points>0";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//sprinty - z punktami procent
if ($stats=='sprints-points-percent') {
  $query_select="SELECT ROUND(COUNT(distinct drivers_sprint_results.sprint_date) / (SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.=") *100 ,1) as amount,
  lower(teams.team) id, 
  CONCAT(teams.name,' (',COUNT(distinct drivers_sprint_results.sprint_date),'/',(SELECT COUNT(DISTINCT d.sprint_date) FROM drivers_sprint_results d LEFT JOIN gp g ON d.id_gp=g.id_gp  WHERE d.team=drivers_sprint_results.team";
  if ($beginYear!=-1) $query_select.=" AND d.season='$beginYear'";
  if ($gp!=-1) $query_select.=" AND g.name_alias='$gp'";
  if ($circuit!=-1) $query_select.=" AND g.circuit_alias='$circuit'";
  $query_select.="),')') as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_sprint_results,gp";
	$query_conditions=" WHERE drivers_sprint_results.id_team=teams.id_team AND drivers_sprint_results.id_gp=gp.id_gp AND drivers_sprint_results.sprint_points>0";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}

/**
 * Tytuly mistrzowskie
 */
$lastSeason = 2025;
if ($stats=='champs-number') {
  $query_select="SELECT COUNT(teams_class.id_teams_class) amount, MAX(teams_class.season) lastSeason,
  lower(teams.team) id, 
  CONCAT(
    teams.name,
    ' (',
    GROUP_CONCAT(teams_class.season ORDER BY teams_class.season SEPARATOR ', '),
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, MIN(teams_class.season), teams.name";
}
if ($stats=='champs-by-seasons') {
  $query_select="SELECT teams_class.season amount,
  lower(teams.team) id, 
  teams.name as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY season DESC";
}
if ($stats=='champs-seasons-needed') {
  $query_select="SELECT (
    SELECT
        COUNT(dc.season)
    FROM
        teams_class dc
    WHERE
        dc.season <=(
        SELECT
            MIN(dc2.season)
        FROM
            teams_class dc2
        WHERE
            id_team = teams_class.id_team
            AND dc2.place=1
    ) AND dc.id_team = teams_class.id_team
  ) amount,
  lower(teams.team) id, 
  teams.name as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" GROUP BY teams.team ORDER BY amount, teams.name";
}
if ($stats=='champs-wins-needed-asc') {
  $query_select="SELECT 
   (
    SELECT
    COUNT(drivers_gp_results.id_gp)
    FROM
        drivers_gp_results
    LEFT JOIN teams_class dc ON dc.id_team=drivers_gp_results.id_team AND dc.season=drivers_gp_results.season    
    WHERE
        drivers_gp_results.race_pos = 1 
        AND drivers_gp_results.id_team = teams_class.id_team
        AND dc.place=1
    AND drivers_gp_results.season=teams_class.season
  ) amount,
  lower(teams.team) id, 
   CONCAT(
    teams.name,
    ' (',
    teams_class.season,
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY amount, teams.name";
}
if ($stats=='champs-wins-needed-desc') {
  $query_select="SELECT 
   (
    SELECT
    COUNT(drivers_gp_results.id_gp)
    FROM
        drivers_gp_results
    LEFT JOIN teams_class dc ON dc.id_team=drivers_gp_results.id_team AND dc.season=drivers_gp_results.season    
    WHERE
        drivers_gp_results.race_pos = 1 
        AND drivers_gp_results.id_team = teams_class.id_team
        AND dc.place=1
    AND drivers_gp_results.season=teams_class.season
  ) amount,
  lower(teams.team) id, 
   CONCAT(
    teams.name,
    ' (',
    teams_class.season,
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY amount DESC, teams.name";
}
if ($stats=='champs-wins-percent') {
  $query_select="SELECT 
  ROUND(
   (
    SELECT
    COUNT(drivers_gp_results.id_gp)
    FROM
        drivers_gp_results
    LEFT JOIN teams_class dc ON dc.id_team=drivers_gp_results.id_team AND dc.season=drivers_gp_results.season    
    WHERE
        drivers_gp_results.race_pos = 1 
        AND drivers_gp_results.id_team = teams_class.id_team
        AND dc.place=1
    AND drivers_gp_results.season=teams_class.season
   ) / (
      SELECT
          COUNT(gp_season.id_gp_season)
      FROM
          gp_season
      WHERE
          gp_season.season = teams_class.season
  ) * 100,0) amount,
  lower(teams.team) id, 
   CONCAT(
    teams.name,
    ' (',
    teams_class.season,
    ' - ',
    (
    SELECT
    COUNT(drivers_gp_results.id_gp)
    FROM
        drivers_gp_results
    LEFT JOIN teams_class dc ON dc.id_team=drivers_gp_results.id_team AND dc.season=drivers_gp_results.season    
    WHERE
        drivers_gp_results.race_pos = 1 
        AND drivers_gp_results.id_team = teams_class.id_team
        AND dc.place=1
    AND drivers_gp_results.season=teams_class.season
   ),
   '/',
    (
      SELECT
          COUNT(gp_season.id_gp_season)
      FROM
          gp_season
      WHERE
          gp_season.season = teams_class.season
    ),
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY amount DESC, teams.name";
}
if ($stats=='champs-points') {
  $query_select="SELECT teams_class.points_class amount,
  lower(teams.team) id, 
   CONCAT(
    teams.name,
    ' (',
    teams_class.season,
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY amount DESC, teams.name";
}
if ($stats=='champs-points-diff') {
  $query_select="SELECT teams_class.points_class - (SELECT dc.points_class FROM teams_class dc WHERE dc.season=teams_class.season AND dc.place=2) amount,
  lower(teams.team) id, 
   CONCAT(
    teams.name,
    ' (',
    teams_class.season,
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" ORDER BY amount DESC, teams.name";
}
if ($stats=='champs-vice-number') {
  $query_select="SELECT COUNT(teams_class.id_teams_class) amount, MAX(teams_class.season) lastSeason,
  lower(teams.team) id, 
  CONCAT(
    teams.name,
    ' (',
    GROUP_CONCAT(teams_class.season ORDER BY teams_class.season SEPARATOR ', '),
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place=2 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, MIN(teams_class.season), teams.name";
}
if ($stats=='champs-podium') {
  $query_select="SELECT COUNT(teams_class.id_teams_class) amount, MAX(teams_class.season) lastSeason,
  lower(teams.team) id, 
  CONCAT(
    teams.name,
    ' (',
    GROUP_CONCAT(teams_class.season ORDER BY teams_class.season SEPARATOR ', '),
    ')'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.place<=3 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, MIN(teams_class.season), teams.name";
}
if ($stats=='champs-best-result') {
  $query_select="SELECT (SELECT COUNT(place) FROM teams_class tc WHERE tc.place=MIN(teams_class.place) AND tc.team=teams_class.team AND tc.is_classified=1 AND tc.season>1957 AND tc.season<$lastSeason GROUP BY tc.team), MIN(place) amount,
  lower(teams.team) id, 
  CONCAT(
    teams.name,
    ' (',
    (SELECT COUNT(place) FROM teams_class tc WHERE tc.place=MIN(teams_class.place) AND tc.team=teams_class.team AND tc.is_classified=1 AND tc.season>1957 AND tc.season<$lastSeason GROUP BY tc.team),
    'x)'
  ) as name,
  teams.alias_name alias,
  teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  FROM teams_class LEFT JOIN teams ON teams.id_team = teams_class.id_team";
  $query_conditions=" WHERE teams_class.is_classified=1 AND teams_class.season>1957 AND teams_class.season<$lastSeason";
	$query_order=" GROUP BY teams.team ORDER BY amount, 1 desc, teams.name";
}

/**
 * Misc [fixed]
 */
//sezony
if ($stats=='seasons') {
	$query_select="SELECT COUNT(distinct drivers_gp_results.season) as amount, lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//gp
if ($stats=='gp') {
	$query_select="SELECT COUNT(distinct CONCAT(drivers_gp_involvements.id_gp, drivers_gp_involvements.season)) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_involvements,gp";
	$query_conditions=" WHERE drivers_gp_involvements.id_team=teams.id_team AND drivers_gp_involvements.id_gp=gp.id_gp";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//kwalifikacje
if ($stats=='qual') {
	$query_select="SELECT COUNT(distinct drivers_pp_results.qual_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_pp_results,gp";
	$query_conditions=" WHERE drivers_pp_results.id_team=teams.id_team AND drivers_pp_results.id_gp=gp.id_gp";
	$query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
//poza podium
if ($stats=='race-fourth-place') {
  $query_select="SELECT COUNT(id_drivers_gp) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active
  from teams,drivers_gp_results,gp";
	$query_conditions=" WHERE drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_gp=gp.id_gp";
  $query_conditions.=" AND drivers_gp_results.race_pos=4";
  $query_order=" GROUP BY teams.team ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-qualified') {
  $query_select="SELECT COUNT(drivers_pp_results.qual_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(DISTINCT drivers_gp_results.race_date) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NULL";
  $query_conditions.=" AND drivers_pp_results.not_qualified=1";
  $query_order=" GROUP BY drivers_pp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-completed') {
  $query_select="SELECT COUNT(drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_completed=1) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-finished') {
  $query_select="SELECT COUNT(drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_finished=1) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_finished=0 AND drivers_gp_results.disq=0";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-point-scored') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_team_points>0 AND drivers_gp_results.season>1957) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_team_points=0 AND drivers_gp_results.season>1957";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-polepos') {
  $query_select="SELECT COUNT(DISTINCT drivers_pp_results.qual_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(DISTINCT drivers_gp_starting_grid.id_starting_grid) FROM drivers_gp_starting_grid WHERE drivers_gp_starting_grid.team=teams.team AND drivers_gp_starting_grid.is_pp=1) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_team = drivers_gp_involvements.id_team AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_pp IS NOT NULL";
  $query_conditions.=" AND (drivers_gp_starting_grid.is_pp=0  OR drivers_gp_starting_grid.is_pp is null)";
  $query_order=" GROUP BY drivers_pp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-bestlaps') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_best_lap=1) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_best_lap=0";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-podium') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_pos<4) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>3";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}
if ($stats=='race-never-win') {
  $query_select="SELECT COUNT(DISTINCT drivers_gp_results.race_date) as amount,lower(teams.team) id, teams.name, teams.alias_name alias, teams.country_code countryCode,";
  if ($lang=='pl') {
    $query_select.="teams.country,";
  }else{
    $query_select.="teams.country_en country,";
  }  
  $query_select.="teams.picture,'' place,
  (SELECT COALESCE('1','0') from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class)) active,
  (SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.team=teams.team AND drivers_gp_results.race_pos=1) as races
  FROM drivers_gp_involvements
  LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp";
  $query_conditions=" WHERE  drivers_gp_involvements.id_drivers_gp IS NOT NULL";
  $query_conditions.=" AND drivers_gp_results.race_pos>1";
  $query_order=" GROUP BY drivers_gp_results.team HAVING races=0 ORDER BY amount desc, teams.name";
}


// sql
if ($beginYear!=-1) $query_conditions.=" AND drivers_gp_results.season='$beginYear'";
if ($gp!=-1) $query_conditions.=" AND gp.name_alias='$gp'";
if ($circuit!=-1) $query_conditions.=" AND gp.circuit_alias='$circuit'";
if ($status == '1') $query_conditions.=" AND teams.team IN (SELECT teams_class.team from teams_class where teams_class.team=teams.team AND teams_class.season=(SELECT MAX(season) FROM teams_class))";

$query="$query_select$query_conditions$query_order LIMIT $limit OFFSET $offset";
$result = mysqli_query($dbhandle,$query);
$recordItems=array();
$place = $offset;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $r["place"] = $place;
  $recordItems[] = $r;

}
$query="$query_select$query_conditions$query_order";
$result = mysqli_query($dbhandle,$query);
$bestTeam="";
$bestTeamId="";
$amount="";
$total=0;
while($r = mysqli_fetch_assoc($result)) {
  if ($pages == 1) {
    $bestTeamId = $r["id"];
    $bestTeam = $r["name"];
    $amount = $r["amount"];
  }
  $pages+=1;
  $total+=1;
}
$pages = ceil($pages / $limit);


$recordsItems["stats"]=$stats;
$recordsItems["statsName"]=$statsName;
$recordsItems["statsLabel"]=$statsLabel;
$recordsItems["showLink"]=$showLink;
$recordsItems["gp"]=$gp;
$recordsItems["circuit"]=$circuit;
$recordsItems["beginYear"]=$beginYear;
$recordsItems["endYear"]=$endYear;
$recordsItems["status"]=$status;
$recordsItems["pages"]=$pages;
$recordsItems["items"]=$recordItems;
$recordsItems["bestTeamId"]=$bestTeamId;
$recordsItems["bestTeam"]=$bestTeam;
$recordsItems["amount"]=$amount;
$recordsItems["total"]=$total;
$recordsItems["query"]=$query;
$recordsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $recordsItems;

print json_encode($response);
mysqli_free_result($result);
?>
