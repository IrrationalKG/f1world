<?
header('Access-Control-Allow-Origin: *');

$page=isset($_GET['page']) ? $_GET['page'] : null;
if ($page==null) $page=isset($_POST['page']) ? $_POST['page'] : null;
if ($page==null) $page=-1;
if ($page=='-') $page=-1;

$idTeamModel=isset($_GET['idTeamModel']) ? $_GET['idTeamModel'] : null;
if ($idTeamModel==null) $idTeamModel=isset($_POST['idTeamModel']) ? $_POST['idTeamModel'] : null;
if ($idTeamModel==null) $idTeamModel=-1;
if ($idTeamModel=='-') $idTeamModel=-1;

$event=isset($_GET['event']) ? $_GET['event'] : null;
if ($event==null) $event=isset($_POST['event']) ? $_POST['event'] : null;
if ($event==null) $event=-1;
if ($event=='-') $event=-1;

$position=isset($_GET['pos']) ? $_GET['pos'] : null;
if ($position==null) $position=isset($_POST['pos']) ? $_POST['pos'] : null;
if ($position==null) $position=-1;
if ($position=='-') $position=-1;

$team=isset($_GET['team']) ? $_GET['team'] : null;
if ($team==null) $team=isset($_POST['team']) ? $_POST['team'] : null;
if ($team==null) $team=-1;
if ($team=='-') $team=-1;

$driver=isset($_GET['driver']) ? $_GET['driver'] : null;
if ($driver==null) $driver=isset($_POST['driver']) ? $_POST['driver'] : null;
if ($driver==null) $driver=-1;
if ($driver=='-') $driver=-1;

$season=isset($_GET['season']) ? $_GET['season'] : null;
if ($season==null) $season=isset($_POST['season']) ? $_POST['season'] : null;
if ($season==null) $season=-1;
if ($season=='-') $season=-1;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie modelu
if ($idTeamModel!=-1) {
  $query="SELECT model,lower(team) team FROM teams_models WHERE id_team_model='$idTeamModel'";
  $result = mysqli_query($dbhandle,$query);
  $models=array();
  while($r = mysqli_fetch_assoc($result)) {
    $models[] = $r["model"];
    $teamShortName = $r["team"];
  }
  $model=$models[0];
}else{
  $query="SELECT DISTINCT lower(team) team FROM teams WHERE alias_name='$team'";
  $result = mysqli_query($dbhandle,$query);
  while($r = mysqli_fetch_assoc($result)) {
    $teamShortName = $r["team"];
  }
  $query="SELECT DISTINCT model,lower(team) team FROM teams_models WHERE team='$teamShortName' AND season='$season'";
  $result = mysqli_query($dbhandle,$query);
  $models=array();
  while($r = mysqli_fetch_assoc($result)) {
    $models[] = $r["model"];
  }
}

// pobranie id
$query="SELECT DISTINCT name, lower(team) team, alias_name FROM teams WHERE team='$teamShortName'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $teamName = $r["name"];
  $teamShortName = $r["team"];
  $teamAlias = $r["alias_name"];
}

$pages=1;
$records=1;
$limit=50;
$offset=(($page-1)*$limit);

$query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(gp_season.date,1,10) raceDate, 
gp.id_gp id,"; 
if ($lang=='pl') {
  $query_select.="gp.name AS gpname,";
}else{
  $query_select.="gp.name_en AS gpname,";
}
$query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
drivers_sprint_results.sprint_pos sprintPos, 
drivers_sprint_results.sprint_completed sprintCompleted,
COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
drivers_pp_results.qual_pos qual, 
drivers_pp_results.qual_completed qualCompleted,
drivers_pp_results.not_qualified notQualified,
drivers_pp_results.not_started notStarted,
drivers_gp_starting_grid.grid_pos gridPos,
MIN(drivers_gp_results.race_pos) race, 
drivers_gp_results.race_completed raceCompleted,
drivers_gp_results.race_best_lap bestLap,
drivers_gp_results.disq disq,
drivers_gp_results.race_laps laps,
COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
 if ($lang=='pl') {
  $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
  COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
  COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
  COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
}else{
  $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
  COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
  COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
  COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
}
$query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
COALESCE(drivers_gp_results.race_team_points,0) racePoints,
COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
(COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
teams_models.model,
teams_models.team_name modelName,
teams.alias_name teamAlias,
teams.name teamName,
teams.team,
teams.engine,
teams.country_code teamCountryCode,
tyres.name tyre,
drivers_gp_involvements.number
FROM drivers_gp_involvements 
LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
$query_conditions=" WHERE teams_models.team='$teamShortName'";
$query_order="";

//gp
if ($event=='gp') {
  $query_conditions.=" AND drivers_gp_involvements.id_involvement is not null";
}
//wyscigi
if ($event=='starts') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
}
//kwalifikacje
if ($event=='qual') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,"; 
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprintPos, drivers_sprint_results.sprint_completed sprintCompleted,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.race_best_lap bestLap,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_pp is not null";
}
//zwyciestwa
if ($event=='wins') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=1";
}
//2 miejsca
if ($event=='second') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=2";
}
//3 miejsca
if ($event=='third') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_pos=3";
}
//wyścigi miejsca
if ($event=='race-places') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_gp is not null";
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos=$position";
}
//kwalifikacje miejsca
if ($event=='qual-places') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,"; 
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprintPos, drivers_sprint_results.sprint_completed sprintCompleted,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.race_best_lap bestLap,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number 
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" AND drivers_pp_results.qual_completed=1 AND drivers_pp_results.qual_pos=$position";
}
//pola startowe miejsca
if ($event=='grid-places') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,"; 
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprintPos, drivers_sprint_results.sprint_completed sprintCompleted,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.race_best_lap bestLap,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number 
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" AND drivers_gp_starting_grid.grid_pos=$position";
}
//punkty
if ($event=='points') {
  $query_conditions.=" AND (drivers_gp_results.race_team_points > 0 OR drivers_sprint_results.sprint_points > 0)"; 
}
//pp
if ($event=='polepos') {
  $query_select="SELECT SUBSTRING(gp_season.date,1,4) season, SUBSTRING(drivers_pp_results.qual_date,1,10) raceDate, 
  gp.id_gp id,"; 
  if ($lang=='pl') {
    $query_select.="gp.name AS gpname,";
  }else{
    $query_select.="gp.name_en AS gpname,";
  }
  $query_select.="gp.name_alias alias, gp.name_short nameShort, gp.country_code countryCode, gp.circuit, gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) driver, drivers.alias driverAlias,drivers.country_code driverCountryCode, drivers.id_driver idDriver,
  drivers_sprint_results.sprint_pos sprintPos, drivers_sprint_results.sprint_completed sprintCompleted,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  drivers_pp_results.qual_pos qual, drivers_pp_results.qual_completed qualCompleted,
  drivers_gp_starting_grid.grid_pos gridPos,
  drivers_gp_results.race_pos race, drivers_gp_results.race_completed raceCompleted,
  drivers_gp_results.race_best_lap bestLap,
  drivers_gp_results.disq disq,
  drivers_gp_results.race_laps laps,
  COALESCE(drivers_gp_results.shared_drive,'') sharedDrive,";
  if ($lang=='pl') {
    $query_select.="COALESCE(drivers_gp_results.race_add_info,'') info,
    COALESCE(drivers_pp_results.qual_add_info,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info,'') sprintInfo,";
  }else{
    $query_select.="COALESCE(drivers_gp_results.race_add_info_en,'') info,
    COALESCE(drivers_pp_results.qual_add_info_en,'') qualInfo,
    COALESCE(drivers_gp_starting_grid.grid_add_info_en,'') gridInfo,
    COALESCE(drivers_sprint_results.sprint_add_info_en,'') sprintInfo,";
  }
  $query_select.="COALESCE(drivers_gp_results.excluded_from_team_class,0) excluded, 
  COALESCE(drivers_gp_results.race_team_points,0) racePoints,
  COALESCE(drivers_sprint_results.sprint_points,0) sprintPoints,
  (COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  teams_models.model,
  teams_models.team_name modelName,
  teams.alias_name teamAlias,
  teams.name teamName,
  teams.team,
  teams.engine,
  teams.country_code teamCountryCode,
  tyres.name tyre,
  drivers_gp_involvements.number
  FROM drivers_gp_involvements 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_involvements.id_gp
  LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
  LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
  LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
  LEFT JOIN teams_models ON teams_models.id_team_model = drivers_gp_involvements.id_team_model
  LEFT JOIN tyres ON tyres.id_tyre = drivers_gp_involvements.id_tyre";
  $query_conditions.=" AND drivers_gp_starting_grid.is_pp=1";
}
//no
if ($event=='bestlaps') {
  $query_conditions.=" AND drivers_gp_results.race_best_lap=1"; 
}
//podium
if ($event=='podium') {
  $query_conditions.=" AND drivers_gp_results.race_pos<4";
}
//miejsca punktowane
if ($event=='points-places') {
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_results.season>1957";
}
//ukończone
if ($event=='completed') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1";
}
//nieukończone
if ($event=='incomplete') {
  $query_conditions.=" AND drivers_gp_results.race_completed=0";
}
//dyskwalifikacje
if ($event=='disq') {
  $query_conditions.=" AND drivers_gp_results.race_add_info='dyskwalifikacja'";
}
//pp + wygrane
if ($event=='polepos-wins') {
  $query_conditions.=" AND (drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1)";
}
//naj. okraz. + wygrane
if ($event=='bestlaps-wins') {
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_results.race_best_lap=1";
}
//pp + naj. okraz. + wygrane
if ($event=='polepos-bestlaps-wins') {
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.is_pp=1 AND drivers_gp_results.race_best_lap=1";
}
//wygrane z pierwszego pola startowego
if ($event=='wins-from-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=1";
}
//podium z pierwszego pola startowego
if ($event=='podium-from-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=1";
}
//punkty z pierwszego pola startowego
if ($event=='points-from-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos=1";
}
//uk. wyścigi z pierwszego pola startowego
if ($event=='completed-from-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
}
//nie uk. wyścigi z pierwszego pola startowego
if ($event=='incomplete-from-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=1 AND drivers_gp_results.co_driver=0";
}
//wygrane spoza pierwszego pola startowego
if ($event=='wins-outside-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos>1";
}
//podium spoza pierwszego pola startowego
if ($event=='podium-outside-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos>1";
}
//punkty spoza pierwszego pola startowego
if ($event=='points-outside-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos>1";
}
//uk. wyścigi spoza pierwszego pola startowego
if ($event=='completed-outside-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
}
//nie uk. wyścigi spoza pierwszego pola startowego
if ($event=='incomplete-outside-polepos') {
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos>1 AND drivers_gp_results.co_driver=0";
}
//wygrane z miejsca ...
if ($event=='wins-from-place') {
  $query_conditions.=" AND drivers_gp_results.race_pos=1 AND drivers_gp_starting_grid.grid_pos=$position";
}
//podium z miejsca ...
if ($event=='podium-from-place') {
  $query_conditions.=" AND drivers_gp_results.race_pos<4 AND drivers_gp_starting_grid.grid_pos=$position";
}
//punkty z miejsca ...
if ($event=='points-from-place') {
  $query_conditions.=" AND drivers_gp_results.race_team_points>0 AND drivers_gp_starting_grid.grid_pos=$position";
}
//uk. wyścigi z miejsca ...
if ($event=='completed-from-place') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
}
//nie uk. wyścigi z miejsca ...
if ($event=='incomplete-from-place') {
  $query_conditions.=" AND drivers_gp_results.race_completed=0 AND drivers_gp_starting_grid.grid_pos=$position AND drivers_gp_results.co_driver=0";
}
//sprinty
if ($event=='sprints') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL";
}
//sprinty wygrane
if ($event=='sprint-wins') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos=1";
}
//sprinty podium
if ($event=='sprint-podiums') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_pos<4";
}
//sprinty punkty
if ($event=='sprint-points') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_points>0";
}
//sprinty sklasyfikowany
if ($event=='sprint-completed') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=1";
}
//sprinty niesklasyfikowany
if ($event=='sprint-incompleted') {
  $query_conditions.=" AND drivers_gp_involvements.id_drivers_sprint IS NOT NULL AND drivers_sprint_results.sprint_completed=0";
}
//uk. wyścigi tuz poza podium
if ($event=='race-fourth-place') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos=4 AND drivers_gp_results.co_driver=0";
}
//Najwięcej wyścigów ukończonych na pozycji wyższej niż startowa
if ($event=='race-pos-higher-than-grid' || $event=='race-pos-higher-than-grid-percent') {
  $query_conditions.=" AND drivers_gp_results.race_completed=1 AND drivers_gp_results.race_pos<drivers_gp_starting_grid.grid_pos AND drivers_gp_results.co_driver=0";
}
//Najwięcej wyścigów ukończonych na pozycji nizszej niż startowa
if ($event=='race-pos-lower-than-grid' || $event=='race-pos-lower-than-grid-percent') {
  $query_conditions.=" AND drivers_gp_results.race_pos>drivers_gp_starting_grid.grid_pos AND drivers_gp_results.co_driver=0";
}

// sql
if ($idTeamModel!=-1) $query_conditions.=" AND teams_models.model='$model'";
if ($season!=-1) $query_conditions.=" AND drivers_gp_involvements.season=$season";
if ($driver!=-1) $query_conditions.=" AND drivers.alias='$driver'";
$query_group_by.=" GROUP BY gp_season.date, drivers_gp_involvements.id_driver";
$query_order="  ORDER BY gp_season.date DESC, drivers.surname";

$query="$query_select$query_conditions$query_group_by$query_order";
$result = mysqli_query($dbhandle,$query);
$total=0;
$lastDate = "";
while($r = mysqli_fetch_assoc($result)) {
  $records += 1;
  if ($event=='podium' || $event=='points' || $event=='points-places' || $event=='podium-outside-polepos') {
    $total += 1;
  }else{
    if ($lastDate !== $r["raceDate"]){
      $total += 1;
      $lastDate = $r["raceDate"];
    }
  }
}
$pages = ceil($records / $limit);

$query="$query_select$query_conditions$query_group_by$query_order";
$result = mysqli_query($dbhandle,$query);
$recordItems=array();
$place = 0;
$lastDate = "";
$cnt = 1;
while($r = mysqli_fetch_assoc($result)) {
  if ($event=='podium' || $event=='points' || $event=='points-places' || $event=='podium-outside-polepos') {
    $place += 1;
  }else{
    if ($lastDate !== $r["raceDate"]){
      $place += 1;
      $lastDate = $r["raceDate"];
    }
  }
  if ($cnt <= $offset) {
    $cnt += 1;
    continue;
  }else{
    if ($cnt > ($limit * $page)) break;
    $r["place"] = $total + 1 - $place;
    $recordItems[] = $r;
    $cnt += 1;
  }
}

$recordsItems["idTeamModel"]=$idTeamModel;
$recordsItems["models"]=$models;
$recordsItems["event"]=$event;
$recordsItems["teamName"]=$teamName;
$recordsItems["teamShortName"]=$teamShortName;
$recordsItems["teamAlias"]=$teamAlias;
$recordsItems["driver"]=$driver;
$recordsItems["team"]=$team;
$recordsItems["season"]=$season;
$recordsItems["pages"]=$pages;
$recordsItems["results"]=$recordItems;
$recordsItems["total"]=$total;
$recordsItems["QUERY"]=$query;
$recordsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $recordsItems;

print json_encode($response);
mysqli_free_result($result);
?>
