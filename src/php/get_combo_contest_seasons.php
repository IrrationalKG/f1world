<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// sezony
$query="SELECT sezon value, sezon label  FROM `typy` group by sezon ORDER by sezon desc";
$result = mysqli_query($dbhandle,$query);
$seasons=array();
// $empty = (object) array(
//     'value' => '',
//     'label' => 'Wszystkie',
// );
// $seasons[] = $empty;
while($r = mysqli_fetch_assoc($result)) {
  $seasons[] = $r;
}
//$seasons["query"] = $query;
// Response
$response = $seasons;

print json_encode($response);
mysqli_free_result($result);
?>
