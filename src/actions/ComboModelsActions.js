// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_MODELS_REQUEST = "FETCH_COMBO_MODELS_REQUEST";
export const FETCH_COMBO_MODELS_SUCCESS = "FETCH_COMBO_MODELS_SUCCESS";
export const FETCH_COMBO_MODELS_ERROR = "FETCH_COMBO_MODELS_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_MODELS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_MODELS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_MODELS_ERROR,
    payload: error,
  };
}

export function fetchComboModels(
  empty,
  driver,
  team,
  gp,
  circuit,
  engine,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_models.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          driver: driver,
          team: team,
          gp: gp,
          circuit: circuit,
          engine: engine,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
