// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GPRESULT_REQUEST = "FETCH_GPRESULT_REQUEST";
export const FETCH_GPRESULT_SUCCESS = "FETCH_GPRESULT_SUCCESS";
export const FETCH_GPRESULT_ERROR = "FETCH_GPRESULT_ERROR";

function requestData() {
  return {
    type: FETCH_GPRESULT_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GPRESULT_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GPRESULT_ERROR,
    payload: error,
  };
}

export function fetchGPResult(year, id, lang) {
  const url = `${ROOT_URL}/get_gp_result.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          id: id,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
