<?
include("dbinfo.inc.php");
mysql_connect(localhost,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

$query="SELECT * FROM drivers where show_driver=1 order by surname";
$result_d=mysql_query($query);
$num_d=mysql_numrows($result_d);

$query="SELECT * FROM teams where show_team=1 order by name";
$result_t=mysql_query($query);
$num_t=mysql_numrows($result_t);

$query="SELECT * FROM gp order by name";
$result_c=mysql_query($query);
$num_c=mysql_numrows($result_c);

mysql_close();

?>
<html>
<head>
<title>Add Pole Position Results</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<form method="post" action="insert_pp_result.php">
  <p>Add Pole Position Result<br>
    GP:
    <select name="id_gp">
    <?
    $i=0;
    while ($i < $num_c) {
      $id_gp=mysql_result($result_c,$i,"id_gp");
      $name=mysql_result($result_c,$i,"name");
      $circuit=mysql_result($result_c,$i,"circuit");
      ?>
    <option value="<? echo "$id_gp"; ?>"><? echo "$name"; ?>&nbsp;(<? echo "$circuit"; ?>)</option>
      <?
      ++$i;
    }
    ?>
    </select>
    <br>
    Pole Position Date (yyyy-mm-dd):
    <input type="text" name="qual_date" maxlength="10" size="10">
    <br>
    Driver:
    <select name="id_driver">
<?
$i=0;
while ($i < $num_d) {
	$id_driver=mysql_result($result_d,$i,"id_driver");
	$name=mysql_result($result_d,$i,"name");
	$surname=mysql_result($result_d,$i,"surname");
	?>
<option value="<? echo "$id_driver"; ?>"><? echo "$surname"; ?>&nbsp;<? echo "$name"; ?></option>
	<?
	++$i;
}
?>
    </select>
    <br>
    Team:
    <select name="id_team">
<?
$i=0;
while ($i < $num_t) {
	$id_team=mysql_result($result_t,$i,"id_team");
	$name=mysql_result($result_t,$i,"name");
	$engine=mysql_result($result_t,$i,"engine");
	?>
<option value="<? echo "$id_team"; ?>"><? echo "$name $engine"; ?></option>
	<?
	++$i;
}
?>
    </select>
    <br>
    Pole Position:
    <input type="text" name="qual_pos" maxlength="2" size="2">
    <br>
    Pole Position Time:
    <input type="text" name="qual_time">
    <br>
    Pole Position Segment:
    <input type="text" name="qual_segment" maxlength="2" size="2">
    <br>
    Pole Position completed:
    <select name="qual_completed">
      <option value="0">false</option>
      <option value="1">true</option>
    </select>
    <br>
    Pole Position additional info:
    <textarea name="qual_add_info"></textarea>
    <br>
    <input type="submit" name="Submit" value="Submit">
  </p>
  </form>
</body>
</html>
