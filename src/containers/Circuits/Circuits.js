/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Flag,
  Grid,
  Loader,
  Item,
  Header,
  Divider,
  Label,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchMenu } from "../../actions/MenuActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Circuits.less";

class Circuits extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderCircuits = this.renderCircuits.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchMenu(props.match.params.year, "", Cookies.get("lang"));
  }

  renderCircuits() {
    const items = [];
    const { props } = this;
    const { data } = props.menu;
    if (data) {
      let num = 0;
      data[2].items.forEach((item) => {
        const {
          alias,
          name,
          country,
          circuit,
          track,
          date,
          build,
          laps,
          length,
          distance,
          winner,
          mostWins,
          mostPP,
          mostPoints,
          mostPodiums,
          mostBL,
          sprint,
          picture,
          isNextGP,
          seasons,
        } = item;
        let filename = "/build/images/circuits/circuit_no_profile.jpg";
        if (picture === "1") {
          filename = `/build/images/circuits/circuit_${alias}_profile.jpg`;
        }
        const detailsLink = `/circuit/${alias}`;
        let labelColor = winner != "nieznany" ? "grey" : "yellow";
        if (isNextGP === "1") {
          labelColor = "red";
        }
        num += 1;
        const record = (
          <Grid.Column
            textAlign="center"
            key={item.alias}
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <Segment basic>
                    <Image
                      fluid
                      label={{
                        color: `${labelColor}`,
                        corner: "left",
                        content: `${num}`,
                        size: "large",
                        horizontal: true,
                      }}
                    />
                    <NavLink to={detailsLink}>
                      <Image
                        centered
                        src={filename}
                        alt={`${circuit} - ${track}`}
                        className="menu-link"
                      />
                    </NavLink>
                    {sprint == 1 && (
                      <Label attached="top right">
                        <FormattedMessage id={"app.stats.sprint"} />
                      </Label>
                    )}
                    <Label attached="bottom" color="red">
                      {winner != "nieznany" ? (
                        winner
                      ) : (
                        <FormattedMessage id={"app.stats.unknown"} />
                      )}
                    </Label>
                  </Segment>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content className="content-text">
                        <Flag name={country} />
                        {name}
                        <Header.Subheader>{circuit}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                    <Header as="h3">
                      <Header.Content>
                        <FormattedMessage id={"app.stats.debut"} />
                        <Header.Subheader>{build}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                    <Header as="h3">
                      <Header.Content>
                        {track}
                        <Header.Subheader>{date}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider hidden fitted></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column textAlign="left" className="info-box-light">
                  <Item.Group divided>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.gp"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{seasons}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.laps"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{laps}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.length"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {length} <small>km</small>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.distance"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            {distance} <small>km</small>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.wins"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{mostWins}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.points"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{mostPoints}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.podium"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{mostPodiums}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.polepos"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{mostPP}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.bestlaps"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>{mostBL}</span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                  </Item.Group>
                  <div className="buttons">
                    <NavLink className="details menu-link" to={detailsLink}>
                      <FormattedMessage id={"app.button.details"} />
                    </NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { props } = this;
    if (!props.menu.data || props.menu.loading) {
      return (
        <section id="circuits" name="circuits" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "../build/images/circuits/circuits_banner.jpg";
    return (
      <section id="circuits" name="circuits" className="section-page">
        <SectionPageBanner
          pic={picBanner}
          title={<FormattedMessage id={"app.page.circuits.title"} />}
          subtitle={
            <FormattedMessage
              id={"app.page.circuits.subtitle"}
              values={{ season: props.match.params.year }}
            />
          }
        />
        <Grid>{this.renderCircuits()}</Grid>
      </section>
    );
  }
}
Circuits.propTypes = {
  fetchMenu: PropTypes.func.isRequired,
  menu: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      year: PropTypes.string,
    }).isRequired,
  }).isRequired,
  year: PropTypes.string,
};

function mapStateToProps({ menu }) {
  return { menu };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Circuits);
