<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

$query="select id_news,filename,";
if ($lang=='pl') {
  $query.="title, news,";
}else{
  $query.="title_en title, news_en news,";
} 
$query.="news_date,source,news_type,picture,SUBSTRING(news_date,1,4) as newsyear,SUBSTRING(news_date,6,2) as newsmonth from news"; 
$query.=" where news_type<>2 order by news_date desc LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$newsItems = array();
while($r = mysqli_fetch_assoc($result)) {

  // Damn pesky carriage returns...
  // $text = str_replace("\r\n", "\n", $r["news"]);
  $news = str_replace("\r", "", $r["news"]);
  $news = str_replace("<br/>", "", $news);

  $endPos = strrpos($news, "--Results--");
  if ($endPos >0) { // note: three equal signs
    $news = substr($news,0 ,$endPos);
  }
  $r["news"]=$news;
  $newsItems[] = $r;
}

// Response
$response = $newsItems;

print json_encode($response);
mysqli_free_result($result);
?>
