<?
header('Access-Control-Allow-Origin: *');

  include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT
last_gp lastGP,
last_gp_alias lastGPAlias,
last_gp_name lastGPName,
last_gp_id lastGPId,
last_gp_year lastGPYear,
next_gp nextGP,
next_gp_alias nextGPAlias,
next_gp_name nextGPName,
next_gp_id nextGPId,
next_gp_year nextGPYear,
next_gp_month nextGPMonth,
next_gp_day nextGPDay,
next_gp_hour nextGPHour,
next_gp_min nextGPMin,
next_gp_qual_year nextGPQualYear,
next_gp_qual_month nextGPQualMonth,
next_gp_qual_day nextGPQualDay,
next_gp_qual_hour nextGPQualHour,
next_gp_qual_min nextGPQualMin,
contest_round contestRound
FROM params";
$result = mysqli_query($dbhandle,$query);
$paramsItems;
while($r = mysqli_fetch_assoc($result)) {
  $paramsItems = $r;
}
$paramItems=$paramsItems;
$paramItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $paramItems;

print json_encode($response);
mysqli_free_result($result);
?>
