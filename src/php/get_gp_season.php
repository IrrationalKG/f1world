<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$gpSeasonItems["season"]=$year;
$gpSeasonItems["prevSeason"]=$year-1;
$gpSeasonItems["nextSeason"]=$year+1;

// regulamin
$query="SELECT";
if ($lang=='pl') {
  $query.=" regulations";
}else{
  $query.=" regulations_en as regulations";
}  
$query.=" from season_regulations where year='$year'";
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $gpSeasonItems["regulations"] = $r["regulations"];
}

/**
* Statystyki sezonu
*/

/**
* Kierowcy
*/
// zwyciestwa
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year AND race_pos=1
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["wins"]=$statsItems;

// podium
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year AND race_pos>0 AND race_pos<4
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["podium"]=$statsItems;

// pole positions
$query="SELECT
COUNT(
    drivers_gp_starting_grid.id_starting_grid
) amount,
drivers_gp_starting_grid.id_driver idDriver,
drivers.surname driver,
drivers.alias
FROM
drivers_gp_starting_grid,
drivers,
drivers_class
WHERE
drivers_gp_starting_grid.id_driver = drivers.id_driver AND drivers.id_driver = drivers_class.id_driver 
AND drivers_gp_starting_grid.season = '$year' AND drivers_class.season = $year AND drivers_gp_starting_grid.grid_pos = 1
GROUP BY drivers_gp_starting_grid.id_driver
ORDER BY 1 DESC, drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["polepos"]=$statsItems;

// naj. okrążenia
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year AND race_best_lap=1
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["bestlaps"]=$statsItems;

// ukonczone wyscigi
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year AND race_completed=1
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["raceCompleted"]=$statsItems;

// punkty
$query="SELECT SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_driver=drivers.id_driver AND drivers_sprint_results.season=$year),0) as amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["points"]=$statsItems;

// punkty do klasyfikacji
$query="SELECT SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_driver=drivers.id_driver AND drivers_sprint_results.season=$year),0) as amount,drivers_gp_results.id_driver idDriver,
drivers.surname driver,drivers.alias FROM drivers_gp_results,drivers,drivers_class
WHERE drivers_gp_results.id_driver=drivers.id_driver AND drivers.id_driver=drivers_class.id_driver
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND drivers_class.season=$year AND excluded_from_class=0
GROUP BY drivers_gp_results.id_driver ORDER BY 1 DESC,drivers_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["driversStats"]["pointsClass"]=$statsItems;

/**
* Wykresy
*/
// zwyciestwa w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
WHERE drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonWins=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonWins[] = $r;
}
$gpSeasonItems["driversSeasonWins"]=$driversSeasonWins;

// punkty w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_driver=drivers.id_driver AND drivers_sprint_results.season=$year),0) as  y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
WHERE drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
AND drivers_gp_results.race_points>0 
-- AND excluded_from_class=0
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonPoints[] = $r;
}
$gpSeasonItems["driversSeasonPoints"]=$driversSeasonPoints;

// podium w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
WHERE drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
AND drivers_gp_results.race_pos>0 AND drivers_gp_results.race_pos<4
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonPodium=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonPodium[] = $r;
}
$gpSeasonItems["driversSeasonPodium"]=$driversSeasonPodium;

// pole position w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_starting_grid.grid_pos) y
FROM drivers_gp_starting_grid
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_starting_grid.id_driver
WHERE drivers_gp_starting_grid.season='$year'
AND drivers_gp_starting_grid.is_pp=1
GROUP BY drivers_gp_starting_grid.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonPolepos=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonPolepos[] = $r;
}
$gpSeasonItems["driversSeasonPolepos"]=$driversSeasonPolepos;

// naj okrążenie w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_best_lap) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
WHERE drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonBestlaps=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonBestlaps[] = $r;
}
$gpSeasonItems["driversSeasonBestlaps"]=$driversSeasonBestlaps;

// wyś ukończone w sezonie - kierowcy
$query="SELECT UPPER(drivers.surname) name, COUNT(drivers_gp_results.race_best_lap) y
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
WHERE drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
AND drivers_gp_results.race_completed=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 2 DESC";
$result = mysqli_query($dbhandle,$query);
$driversSeasonRaceCompleted=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversSeasonRaceCompleted[] = $r;
}
$gpSeasonItems["driversSeasonRaceCompleted"]=$driversSeasonRaceCompleted;

//liczba kierowcow ktora wystartowala w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount  FROM drivers_gp_results where drivers_gp_results.season='$year'";
$result = mysqli_query($dbhandle,$query);
$startsCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$startsCount = $r["amount"];
}
$gpSeasonItems["driversStartsCount"]=$startsCount;

//liczba kierowcow z wygrana w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount  FROM drivers_gp_results where drivers_gp_results.race_pos=1 AND drivers_gp_results.season='$year'";
$result = mysqli_query($dbhandle,$query);
$winnersCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$winnersCount = $r["amount"];
}
$gpSeasonItems["driversWinnersCount"]=$winnersCount;

//liczba kierowcow na podium w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount FROM drivers_gp_results where drivers_gp_results.race_pos<4 AND drivers_gp_results.season='$year'";
$result = mysqli_query($dbhandle,$query);
$podiumsCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$podiumsCount = $r["amount"];
}
$gpSeasonItems["driversPodiumsCount"]=$podiumsCount;

//liczba kierowcow z pole position w sezonie
$query="SELECT count(distinct drivers_gp_starting_grid.id_driver) amount FROM drivers_gp_starting_grid where drivers_gp_starting_grid.is_pp=1 AND drivers_gp_starting_grid.season='$year'";
$result = mysqli_query($dbhandle,$query);
$ppCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$ppCount = $r["amount"];
}
$gpSeasonItems["driversPPCount"]=$ppCount;

//liczba kierowcow z najlepszym okrazeniem w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount FROM drivers_gp_results where drivers_gp_results.race_best_lap=1 AND drivers_gp_results.season='$year'";
$result = mysqli_query($dbhandle,$query);
$blCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$blCount = $r["amount"];
}
$gpSeasonItems["driversBLCount"]=$blCount;

//liczba kierowcow punktujacych w sezonie
$query="SELECT count(drivers_class.id_driver) amount FROM drivers_class where drivers_class.points_class>0 AND drivers_class.season='$year'";
$result = mysqli_query($dbhandle,$query);
$pointsCount = 0;
while($r = mysqli_fetch_assoc($result)) {
	$pointsCount = $r["amount"];
}
$gpSeasonItems["driversPointsCount"]=$pointsCount;

/**
* Zespoły
*/
// zwyciestwa
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_pos=1
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["wins"]=$statsItems;

// podium
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_pos>0 AND race_pos<4
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["podium"]=$statsItems;

// pole positions
$query="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_starting_grid,teams,teams_class
WHERE drivers_gp_starting_grid.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND drivers_gp_starting_grid.season='$year' AND teams_class.season=$year AND is_pp=1
GROUP BY drivers_gp_starting_grid.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["polepos"]=$statsItems;

// naj. okrążenia
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_best_lap=1
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["bestlaps"]=$statsItems;

// wyscigi ukonczone
$query="SELECT COUNT(DISTINCT drivers_gp_results.id_gp) amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_completed=1
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["raceCompleted"]=$statsItems;

// punkty
$query="SELECT SUM(race_team_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_team=teams.id_team AND drivers_sprint_results.season=$year),0) as amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["points"]=$statsItems;

// punkty do klasyfikacji
$query="SELECT SUM(race_team_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_team=teams.id_team AND drivers_sprint_results.season=$year),0) as amount,lower(teams.team) idTeam,
TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) team,teams.alias_name alias
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND excluded_from_team_class=0
GROUP BY drivers_gp_results.id_team ORDER BY 1 DESC,teams_class.place LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$statsItems="";
while($r = mysqli_fetch_assoc($result)) {
  $statsItems = $r;
}
$gpSeasonItems["teamsStats"]["pointsClass"]=$statsItems;
/**
* Wykresy
*/
// zwyciestwa w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_pos=1
GROUP BY drivers_gp_results.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonWins=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonWins[] = $r;
}
$gpSeasonItems["teamsSeasonWins"]=$teamsSeasonWins;

// punkty w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, SUM(race_team_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE id_team=teams.id_team AND drivers_sprint_results.season=$year),0) as y
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND excluded_from_team_class=0
GROUP BY drivers_gp_results.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonPoints[] = $r;
}
$gpSeasonItems["teamsSeasonPoints"]=$teamsSeasonPoints;

// podium w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_pos) y
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND race_pos>0 AND race_pos<4
GROUP BY drivers_gp_results.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonPodium=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonPodium[] = $r;
}
$gpSeasonItems["teamsSeasonPodium"]=$teamsSeasonPodium;

// pole position w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_starting_grid.grid_pos) y
FROM drivers_gp_starting_grid,teams,teams_class
WHERE drivers_gp_starting_grid.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND drivers_gp_starting_grid.season='$year' AND teams_class.season=$year AND is_pp=1
GROUP BY drivers_gp_starting_grid.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonPolepos=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonPolepos[] = $r;
}
$gpSeasonItems["teamsSeasonPolepos"]=$teamsSeasonPolepos;

// naj okrążenie w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(drivers_gp_results.race_best_lap) y
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonBestlaps=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonBestlaps[] = $r;
}
$gpSeasonItems["teamsSeasonBestlaps"]=$teamsSeasonBestlaps;

// wyscigi ukonczone w sezonie - zespoły
$query="SELECT UPPER(teams.name) name, COUNT(DISTINCT drivers_gp_results.id_gp) y
FROM drivers_gp_results,teams,teams_class
WHERE drivers_gp_results.id_team=teams.id_team AND teams.id_team=teams_class.id_team
AND race_date>='$year-01-01' AND race_date<'$year-12-31' AND teams_class.season=$year AND drivers_gp_results.race_completed=1
GROUP BY drivers_gp_results.id_team ORDER BY 2 DESC,teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsSeasonRaceCompleted=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsSeasonRaceCompleted[] = $r;
}
$gpSeasonItems["teamsSeasonRaceCompleted"]=$teamsSeasonRaceCompleted;

/**
* Klasyfikacje
*/

// klasyfikacja kierowców
$query="SELECT DISTINCT drivers_class.place,drivers.id_driver idDriver,CONCAT(drivers.name,' ',drivers.surname) driver,
drivers.alias driverAlias,drivers.country_code driverCountryCode,drivers_class.points,drivers_class.points_class pointsClass, drivers.picture,
( SELECT GROUP_CONCAT(DISTINCT CONCAT(name,' ',COALESCE(engine,'')) SEPARATOR ', ') FROM teams, drivers_gp_results
WHERE teams.id_team=drivers_gp_results.id_team AND drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.season='$year' ) team
FROM drivers_class,drivers,drivers_gp_results WHERE drivers_class.id_driver=drivers.id_driver
AND drivers_gp_results.id_driver=drivers.id_driver 
AND drivers_class.season=$year 
AND drivers_gp_results.season='$year' 
AND drivers_class.is_classified=1
ORDER BY drivers_class.place";
$result = mysqli_query($dbhandle,$query);
$driversClassItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversClassItems[] = $r;
}
$classificationItems["drivers"]=$driversClassItems;

// klasyfikacja zespołów
$query="SELECT teams_class.place,lower(teams.team) idTeam,teams.name team,teams.engine engine,teams.alias_name teamAlias,
teams.country_code teamCountryCode,teams_class.points points,teams_class.points_class pointsClass,teams.picture,
(SELECT GROUP_CONCAT(DISTINCT team_name SEPARATOR ', ')
FROM teams_models 
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team_model=teams_models.id_team_model 
 WHERE teams_models.season = teams_class.season and drivers_gp_involvements.id_team=teams_class.id_team)
 modelName 
FROM teams_class
LEFT JOIN teams ON teams.id_team=teams_class.id_team
WHERE teams_class.id_team=teams.id_team 
AND teams_class.season=$year
AND teams_class.points_class>0
GROUP BY teams_class.id_team
ORDER BY teams_class.place";
$result = mysqli_query($dbhandle,$query);
$teamsClassItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsClassItems[] = $r;
}
$classificationItems["teams"]=$teamsClassItems;

$gpSeasonItems["classification"]=$classificationItems;

// wyniki grand prix
$query="SELECT drivers_gp_results.race_date date,gp_season.sort,";
  if ($lang=='pl') {
    $query.="gp.name gp,";
  }else{
    $query.="gp.name_en as gp,";
  }  
  $query.="gp.name_short nameShort,
  gp.name_alias gpAlias,
  gp.country_code gpCountryCode,
  gp.circuit,
  gp.circuit_alias circuitAlias,
  CONCAT(drivers.name,' ',drivers.surname) winner,
  drivers.alias winnerAlias,
  drivers.id_driver winnerId,
  drivers.country_code winnerCountryCode,
  CONCAT(teams.name,' ',COALESCE(teams.engine, '')) winnerTeamName,
  teams.alias_name winnerTeamAlias,
  teams.id_team winnerTeamId,
  teams.team winnerTeam,
  teams.country_code winnerTeamCountryCode,
  teams_models.team_name winnerTeamModelName,
  drivers_gp_results.race_time time,
  drivers_gp_results.race_add_info info,
  CONCAT(
          ppDrivers.name,
          ' ',
          ppDrivers.surname
      ) pp,
  ppDrivers.alias ppAlias,
  ppDrivers.id_driver ppId,
  ppDrivers.country_code ppCountryCode
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN gp_season ON gp_season.id_gp=drivers_gp_involvements.id_gp AND gp_season.season=drivers_gp_involvements.season
LEFT JOIN gp ON gp.id_gp=gp_season.id_gp
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.season = drivers_gp_involvements.season AND drivers_gp_starting_grid.is_pp=1
LEFT JOIN drivers ppDrivers ON ppDrivers.id_driver=drivers_gp_starting_grid.id_driver
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
WHERE drivers_gp_involvements.season = '$year' 
AND drivers_gp_results.race_pos = 1
ORDER BY
  gp_season.sort, drivers_gp_results.co_driver";
$result = mysqli_query($dbhandle,$query);
$resultsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $resultsItems[] = $r;
}
$gpSeasonItems["gpResults"]=$resultsItems;

// modele 
$query="SELECT DISTINCT
teams_models.id_team_model,
teams_models.team,
teams.name,
teams.engine,
teams.alias_name aliasName,
teams.id_team,
teams_models.season season,
teams_models.model,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamName
FROM
drivers_gp_involvements
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
WHERE
drivers_gp_involvements.season = $year
AND teams_models.season = $year
GROUP BY teams_models.team,teams_models.model    
ORDER BY
teams.name,
teams_models.model";
$result = mysqli_query($dbhandle,$query);
$teamModelsItems=array();
while($r = mysqli_fetch_assoc($result)) {
   $teamModelsItems[] = $r;
}
$gpSeasonItems["models"]=$teamModelsItems;

$gpSeasonItems["createTime"]=microtime(true)-$start_time;

/**
*-------------
* Progres - wykres
*-------------
**/
$query="SELECT DISTINCT CONCAT(SUBSTRING(drivers.name,1,1),'.',drivers.surname) as name, drivers.color, gp_season.sort round,gp.name_short gp, gp_season.date 
,COALESCE((SELECT SUM(COALESCE(t2.points,0)) FROM drivers_gp_results t2 WHERE t2.id_driver = drivers_class.id_driver AND t2.race_date <= gp_season.date AND t2.season='$year' and t2.excluded_from_class=0),0) AS points
FROM drivers_class 
LEFT JOIN gp_season ON gp_season.season=drivers_class.season
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers_class.id_driver AND drivers_gp_results.season=drivers_class.season AND drivers_gp_results.id_gp=gp_season.id_gp
LEFT JOIN drivers ON drivers.id_driver=drivers_class.id_driver
LEFT JOIN gp ON gp.id_gp=gp_season.id_gp
WHERE drivers_class.season=$year
AND drivers_class.points>0
order by 1, gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$progresItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $progresItems[] = $r;
}

$gpSeasonItems["progres"]=$progresItems;

// Response
$response = $gpSeasonItems;

print json_encode($response);
mysqli_free_result($result);
?>
