/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Popup,
  Flag,
  Loader,
  Label,
  Segment,
  Header,
  Statistic,
  Divider,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import { fetchGPInfo } from "../../actions/GPInfoActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPCalendarWidget.less";

class GPCalendarWidget extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderGPInfo = this.renderGPInfo.bind(this);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchGPInfo(props.year, Cookies.get("lang"));
  }

  renderGPInfo() {
    const { props } = this;
    if (props.gpinfo.data.length > 0) {
      const gpItems = [];
      const { data } = props.gpinfo;

      const nextGP = data.find((e) => e.idDriver != null);

      data.forEach((item) => {
        const {
          number,
          name,
          country,
          gp,
          circuit,
          circuitAlias,
          alias,
          date,
          winner,
          idDriver,
          photo,
        } = item;
        let filename = "/build/images/circuits/circuit_no_profile.jpg";
        if (photo == "") {
          filename = "/build/images/circuits/circuit_no_profile.jpg";
        } else {
          filename = `/build/images/circuits/circuit_${circuitAlias}_profile.jpg`;
        }
        const detailsLink = `/gp-result/${alias}/${props.year}`;

        let completedGP = "";
        if (winner !== "nieznany" && winner !== "unknown") {
          completedGP = "completed-gp";
          filename = `/build/images/drivers/driver_${idDriver}_profile.jpg`;
        }

        let activeGP = "";
        if (gp == nextGP?.gp) {
          activeGP = "active-gp";
        } else if (nextGP == undefined && number == 1) {
          activeGP = "active-gp";
        }
        const gpItem = (
          <div key={number} className={`box ${completedGP} ${activeGP}`}>
            <Popup
              position="bottom center"
              trigger={
                <NavLink to={detailsLink}>
                  <div className={`box-header ${completedGP} ${activeGP}`}>
                    <Flag name={country} />
                    <div>
                      {number}
                      {". "} {gp}
                    </div>
                  </div>
                  <div className={`box-image ${completedGP} ${activeGP}`}>
                    <Segment basic textAlign="center">
                      <Image src={filename} alt={`${name} - ${circuit}`} />
                    </Segment>
                  </div>
                  <div className="box-content">
                    <div
                      className={`box-description ${completedGP}-description  ${activeGP}-description`}
                    >
                      <p>{circuit}</p>
                    </div>
                    <div className={`box-info ${completedGP} ${activeGP}`}>
                      {date}
                    </div>
                  </div>
                </NavLink>
              }
              flowing
              hoverable
            >
              <Segment basic textAlign="center">
                <Segment basic>
                  <Image
                    src={filename}
                    alt={`${name} - ${circuit}`}
                    className="gp-calendar-widget-pic"
                  />
                  <Label attached="top left" color="yellow" size="huge">
                    {number}
                  </Label>
                  <Label attached="bottom" color="yellow" size="medium">
                    {date}
                  </Label>
                </Segment>
                <Divider hidden fitted></Divider>
                <Header as="h3">
                  <Flag name={country} /> {name}
                </Header>
                <div>
                  <small>{circuit}</small>
                </div>
                <Divider hidden fitted></Divider>
                {idDriver ? (
                  <Image
                    src={`/build/images/drivers/driver_${idDriver}_profile.jpg`}
                    alt={winner}
                    size="small"
                    className="center-aligned"
                  />
                ) : (
                  <Image
                    src={`/build/images/drivers/driver_no_profile.jpg`}
                    alt={winner}
                    size="small"
                    className="center-aligned"
                  />
                )}
                <Statistic size="small">
                  <Statistic.Value>{winner}</Statistic.Value>
                  <Statistic.Label>
                    <FormattedMessage id={"app.stats.winner"} />
                  </Statistic.Label>
                </Statistic>
                <div className="buttons no-padding">
                  <NavLink
                    className="secondary"
                    to={`/gp-result/${alias}/${props.year}`}
                  >
                    <FormattedMessage id={"app.button.results"} />
                  </NavLink>
                </div>
              </Segment>
            </Popup>
          </div>
        );
        gpItems.push(gpItem);
      });
      return gpItems;
    }
    return "";
  }

  render() {
    const { gpinfo } = this.props;
    if (!gpinfo.data || gpinfo.data.length === 0) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    return (
      <section id="gp-calendar-widget">
        <div className="gp-calendar-widget-content">
          <div className="box-container">{this.renderGPInfo()}</div>
        </div>
      </section>
    );
  }
}

GPCalendarWidget.propTypes = {
  fetchGPInfo: PropTypes.func.isRequired,
  gpinfo: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpinfo }) {
  return { gpinfo };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGPInfo }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GPCalendarWidget);
