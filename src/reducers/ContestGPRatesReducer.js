import { FETCH_CONTEST_GPRATES_REQUEST, FETCH_CONTEST_GPRATES_SUCCESS, FETCH_CONTEST_GPRATES_ERROR } from '../actions/ContestGPRatesActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CONTEST_GPRATES_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CONTEST_GPRATES_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CONTEST_GPRATES_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
