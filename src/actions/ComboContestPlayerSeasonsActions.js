// const request = require('../json/combo_contest_player_seasons.json');
//
// export const FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS = 'FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS';
//
// export function fetchComboContestPlayerSeasons() {
//   return {
//     type: FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_COMBO_CONTEST_PLAYER_SEASONS_REQUEST = 'FETCH_COMBO_CONTEST_PLAYER_SEASONS_REQUEST';
export const FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS = 'FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS';
export const FETCH_COMBO_CONTEST_PLAYER_SEASONS_ERROR = 'FETCH_COMBO_CONTEST_PLAYER_SEASONS_ERROR';

function requestData() {
  return {
    type: FETCH_COMBO_CONTEST_PLAYER_SEASONS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_CONTEST_PLAYER_SEASONS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_CONTEST_PLAYER_SEASONS_ERROR,
    payload: error
  };
}

export function fetchComboContestPlayerSeasons(player) {
  const url = `${ROOT_URL}/get_combo_contest_player_seasons.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url, {
      params: {
        player: player
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
