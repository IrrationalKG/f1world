/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./SectionPageHeader.less";

export default class SectionPageHeader extends Component {
  render() {
    const { title, subtitle, type } = this.props;
    let className = "";
    if (type !== undefined && type !== "") {
      className = `-${type}`;
    }
    return (
      <div className={`section-page-header${className}`}>
        {title ? <h2>{title}</h2> : <span>&nbsp;</span>}
        {subtitle && <h3>{subtitle}</h3>}
      </div>
    );
  }
}

SectionPageHeader.propTypes = {
  title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  subtitle: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  type: PropTypes.string,
};
