// const request = require('../json/contest_top.json');
//
// export const FETCH_CONTEST_TOP_SUCCESS = 'FETCH_CONTEST_TOP_SUCCESS';
//
// export function fetchContestTop() {
//   return {
//     type: FETCH_CONTEST_TOP_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_CONTEST_TOP_REQUEST = 'FETCH_CONTEST_TOP_REQUEST';
export const FETCH_CONTEST_TOP_SUCCESS = 'FETCH_CONTEST_TOP_SUCCESS';
export const FETCH_CONTEST_TOP_ERROR = 'FETCH_CONTEST_TOP_ERROR';

function requestData() {
  return {
    type: FETCH_CONTEST_TOP_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_TOP_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_TOP_ERROR,
    payload: error
  };
}

export function fetchContestTop() {
  const url = `${ROOT_URL}/get_contest_top.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
