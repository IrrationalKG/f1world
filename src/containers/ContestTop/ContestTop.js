/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Loader, Segment, Table } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchContestTop } from "../../actions/ContestTopActions";
import { FormattedMessage } from "react-intl";
import styles from "./ContestTop.less";

class ContestTop extends Component {
  componentDidMount() {
    const { props } = this;
    props.fetchContestTop();
  }

  renderRecords() {
    const items = [];
    const { props } = this;
    const { users } = props.contestTop.data;
    let cnt = 0;
    users.forEach((item) => {
      cnt += 1;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="#">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left bold-uppercase">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="1999">{item.s1999}</Table.Cell>
          <Table.Cell data-title="2000">{item.s2000}</Table.Cell>
          <Table.Cell data-title="2001">{item.s2001}</Table.Cell>
          <Table.Cell data-title="2002">{item.s2002}</Table.Cell>
          <Table.Cell data-title="2003">{item.s2003}</Table.Cell>
          <Table.Cell data-title="2004">{item.s2004}</Table.Cell>
          <Table.Cell data-title="2005">{item.s2005}</Table.Cell>
          <Table.Cell data-title="2006">{item.s2006}</Table.Cell>
          <Table.Cell data-title="2007">{item.s2007}</Table.Cell>
          <Table.Cell data-title="2008">{item.s2008}</Table.Cell>
          <Table.Cell data-title="2009">{item.s2009}</Table.Cell>
          <Table.Cell data-title="2010">{item.s2010}</Table.Cell>
          <Table.Cell data-title="2011">{item.s2011}</Table.Cell>
          <Table.Cell data-title="2012">{item.s2012}</Table.Cell>
          <Table.Cell data-title="2013">{item.s2013}</Table.Cell>
          <Table.Cell data-title="2014">{item.s2014}</Table.Cell>
          <Table.Cell data-title="2015">{item.s2015}</Table.Cell>
          <Table.Cell data-title="2016">{item.s2016}</Table.Cell>
          <Table.Cell data-title="2017">{item.s2017}</Table.Cell>
          <Table.Cell data-title="2018">{item.s2018}</Table.Cell>
          <Table.Cell data-title="2019">{item.s2019}</Table.Cell>
          <Table.Cell data-title="2020">{item.s2020}</Table.Cell>
          <Table.Cell data-title="2021">{item.s2021}</Table.Cell>
          <Table.Cell data-title="2022">{item.s2022}</Table.Cell>
          <Table.Cell data-title="2023">{item.s2023}</Table.Cell>
          <Table.Cell data-title="2024">{item.s2024}</Table.Cell>
          <Table.Cell data-title="Suma">{item.points}</Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  render() {
    const { contestTop } = this.props;
    if (!contestTop.data || contestTop.loading) {
      return (
        <section id="contest-top" name="contest-top" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "/build/images/contest/contest_banner.jpg";
    return (
      <section id="contest-top" name="contest-top" className="section-page">
        <SectionPageBanner
          title="TOP 100"
          subtitle={
            <FormattedMessage id={"app.page.contest.top100.subtitle"} />
          }
        />
        <Segment basic>
          <div className="section-page-content contest-top">
            <Segment basic className="overflow">
              <Table
                basic="very"
                celled
                className="responsive-table center-aligned"
              >
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <FormattedMessage id={"app.table.header.player"} />
                    </Table.HeaderCell>
                    <Table.HeaderCell data-title="1999">1999</Table.HeaderCell>
                    <Table.HeaderCell data-title="2000">2000</Table.HeaderCell>
                    <Table.HeaderCell data-title="2001">2001</Table.HeaderCell>
                    <Table.HeaderCell data-title="2002">2002</Table.HeaderCell>
                    <Table.HeaderCell data-title="2003">2003</Table.HeaderCell>
                    <Table.HeaderCell data-title="2004">2004</Table.HeaderCell>
                    <Table.HeaderCell data-title="2005">2005</Table.HeaderCell>
                    <Table.HeaderCell data-title="2006">2006</Table.HeaderCell>
                    <Table.HeaderCell data-title="2007">2007</Table.HeaderCell>
                    <Table.HeaderCell data-title="2008">2008</Table.HeaderCell>
                    <Table.HeaderCell data-title="2009">2009</Table.HeaderCell>
                    <Table.HeaderCell data-title="2010">2010</Table.HeaderCell>
                    <Table.HeaderCell data-title="2011">2011</Table.HeaderCell>
                    <Table.HeaderCell data-title="2012">2012</Table.HeaderCell>
                    <Table.HeaderCell data-title="2013">2013</Table.HeaderCell>
                    <Table.HeaderCell data-title="2014">2014</Table.HeaderCell>
                    <Table.HeaderCell data-title="2015">2015</Table.HeaderCell>
                    <Table.HeaderCell data-title="2016">2016</Table.HeaderCell>
                    <Table.HeaderCell data-title="2017">2017</Table.HeaderCell>
                    <Table.HeaderCell data-title="2018">2018</Table.HeaderCell>
                    <Table.HeaderCell data-title="2019">2019</Table.HeaderCell>
                    <Table.HeaderCell data-title="2020">2020</Table.HeaderCell>
                    <Table.HeaderCell data-title="2021">2021</Table.HeaderCell>
                    <Table.HeaderCell data-title="2022">2022</Table.HeaderCell>
                    <Table.HeaderCell data-title="2023">2023</Table.HeaderCell>
                    <Table.HeaderCell data-title="2024">2024</Table.HeaderCell>
                    <Table.HeaderCell>
                      <FormattedMessage id={"app.table.header.points"} />
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderRecords()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </Segment>
      </section>
    );
  }
}

ContestTop.propTypes = {
  fetchContestTop: PropTypes.func.isRequired,
  contestTop: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ contestTop }) {
  return {
    contestTop,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchContestTop,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ContestTop);
