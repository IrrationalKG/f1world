// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GPSEASON_DRIVERS_REQUEST = "FETCH_GPSEASON_DRIVERS_REQUEST";
export const FETCH_GPSEASON_DRIVERS_SUCCESS = "FETCH_GPSEASON_DRIVERS_SUCCESS";
export const FETCH_GPSEASON_DRIVERS_ERROR = "FETCH_GPSEASON_DRIVERS_ERROR";

function requestData() {
  return {
    type: FETCH_GPSEASON_DRIVERS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GPSEASON_DRIVERS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GPSEASON_DRIVERS_ERROR,
    payload: error,
  };
}

export function fetchGPSeasonDrivers(year, lang) {
  const url = `${ROOT_URL}/get_gp_season_drivers.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
