<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// grand prix
$query="SELECT gp.country_code name, gp.name gp, name_alias alias from gp,gp_season where gp.id_gp=gp_season.id_gp and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}

/**
* KIEROWCY
**/
//starty w sezonie
$query="SELECT id_driver,count(id_drivers_gp) starts from drivers_gp_results where drivers_gp_results.season='$year' group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversStartsTab[$tmp_id_driver] = $r["starts"];
}
//zwyciestwa w sezonie
$query="SELECT id_driver,count(id_drivers_gp) wins from drivers_gp_results where drivers_gp_results.season='$year' and race_pos=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversWinsTab[$tmp_id_driver] = $r["wins"];
}
//drugie miejsca w sezonie
$query="SELECT id_driver,count(id_drivers_gp) second from drivers_gp_results where drivers_gp_results.season='$year' and race_pos=2 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversSecondTab[$tmp_id_driver] = $r["second"];
}
//trzecie miejsca w sezonie
$query="SELECT id_driver,count(id_drivers_gp) third from drivers_gp_results where drivers_gp_results.season='$year' and race_pos=3 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversThirdTab[$tmp_id_driver] = $r["third"];
}
//podium w sezonie
$query="SELECT id_driver,count(id_drivers_gp) podiums from drivers_gp_results where drivers_gp_results.season='$year' and race_pos<4 and race_completed=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversPodiumsTab[$tmp_id_driver] = $r["podiums"];
}
//pole position w sezonie
$query="SELECT id_driver,count(id_starting_grid) pp from drivers_gp_starting_grid where drivers_gp_starting_grid.season='$year' and is_pp=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversPolePosTab[$tmp_id_driver] = $r["pp"];
}
//best laps w sezonie
$query="SELECT id_driver,count(id_drivers_gp) bestlaps from drivers_gp_results where drivers_gp_results.season='$year' and race_best_lap=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversBLTab[$tmp_id_driver] = $r["bestlaps"];
}
//gp z punktami w sezonie
$query="SELECT id_driver,count(id_drivers_gp) pointsPlaces from drivers_gp_results where drivers_gp_results.season='$year' and race_points>0 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversPointsPlacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversPointsPlacesTab[$tmp_id_driver] = $r["pointsPlaces"];
}
//ukonczone gp w sezonie
$query="SELECT id_driver,count(id_drivers_gp) completed from drivers_gp_results where drivers_gp_results.season='$year' and race_completed=1 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversCompletedTab[$tmp_id_driver] = $r["completed"];
}
//nieukonczone gp w sezonie
$query="SELECT id_driver,count(id_drivers_gp) incomplete from drivers_gp_results where drivers_gp_results.season='$year' and race_completed=0 group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversIncompleteTab[$tmp_id_driver] = $r["incomplete"];
}
//dyskwalifikacje w sezonie
$query="SELECT id_driver,count(id_drivers_gp) disq from drivers_gp_results where drivers_gp_results.season='$year' and race_add_info='dyskwalifikacja' group by id_driver";
$result = mysqli_query($dbhandle,$query);
$driversDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id_driver"];
  $driversDisqTab[$tmp_id_driver] = $r["disq"];
}
// podsumowanie
$query="SELECT drivers.alias alias,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.country_code country,
place, points, points_class pointsClass, '' starts, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed,
'' incomplete, '' disq,
GROUP_CONCAT(
  DISTINCT teams.alias_name SEPARATOR ', '
) teamsAliases,
GROUP_CONCAT(
  DISTINCT TRIM(CONCAT(teams.name,' ',COALESCE(teams.engine,''))) SEPARATOR ', '
) teamsNames
from drivers_class, drivers, teams, drivers_gp_involvements
where drivers_class.season='$year' 
and drivers.id_driver=drivers_class.id_driver 
AND drivers_gp_involvements.id_driver = drivers_class.id_driver 
AND drivers_gp_involvements.season = drivers_class.season 
AND teams.id_team = drivers_gp_involvements.id_team
GROUP BY drivers_class.id_driver
order by place";
$result = mysqli_query($dbhandle,$query);
$driversSummaryItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_driver = $r["id"];

  $r["starts"]=$driversStartsTab[$r["id"]]==null ? 0 : $driversStartsTab[$r["id"]];
  $r["wins"]=$driversWinsTab[$r["id"]]==null ? 0 : $driversWinsTab[$r["id"]];
  $r["second"]=$driversSecondTab[$r["id"]]==null ? 0 : $driversSecondTab[$r["id"]];
  $r["third"]=$driversThirdTab[$r["id"]]==null ? 0 : $driversThirdTab[$r["id"]];
  $r["podium"]=$driversPodiumsTab[$r["id"]]==null ? 0 : $driversPodiumsTab[$r["id"]];
  $r["polepos"]=$driversPolePosTab[$r["id"]]==null ? 0 : $driversPolePosTab[$r["id"]];
  $r["bestlaps"]=$driversBLTab[$r["id"]]==null ? 0 : $driversBLTab[$r["id"]];
  $r["pointsPlaces"]=$driversPointsPlacesTab[$r["id"]]==null ? 0 : $driversPointsPlacesTab[$r["id"]];
  $r["completed"]=$driversCompletedTab[$r["id"]]==null ? 0 : $driversCompletedTab[$r["id"]];
  $r["incomplete"]=$driversIncompleteTab[$r["id"]]==null ? 0 : $driversIncompleteTab[$r["id"]];
  $r["disq"]=$driversDisqTab[$r["id"]]==null ? 0 : $driversDisqTab[$r["id"]];
  $driversSummaryItems[] = $r;
}


/**
* ZESPOŁY
**/
//starty w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(DISTINCT id_gp) as starts from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsStartsTab[$tmp_id_team] = $r["starts"];
}
//zwyciestwa w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as wins from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_pos=1 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsWinsTab[$tmp_id_team] = $r["wins"];
}
//drugie miejsca w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as second from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_pos=2 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsSecondTab[$tmp_id_team] = $r["second"];
}
//trzecie miejsca w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as third from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_pos=3 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsThirdTab[$tmp_id_team] = $r["third"];
}
//podium w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as podiums from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_pos<4 and race_completed=1 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsPodiumsTab[$tmp_id_team] = $r["podiums"];
}
//pole position w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_starting_grid) as pp from drivers_gp_starting_grid,teams
where drivers_gp_starting_grid.id_team=teams.id_team and drivers_gp_starting_grid.season='$year' and is_pp=1 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsPolePosTab[$tmp_id_team] = $r["pp"];
}
//best laps w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as bestlaps from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_best_lap=1 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsBLTab[$tmp_id_team] = $r["bestlaps"];
}
//miejsca punktowane w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as pointsPlaces from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_points>0 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsPointsPlacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsPointsPlacesTab[$tmp_id_team] = $r["pointsPlaces"];
}
//gp ukonczone w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(DISTINCT id_gp) as completed from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_completed=1 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsCompletedTab[$tmp_id_team] = $r["completed"];
}
//gp nieukonczone w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(DISTINCT id_gp) as incomplete from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_completed=0 group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsIncompleteTab[$tmp_id_team] = $r["incomplete"];
}
//dyskwalifikacje w sezonie
$query="SELECT lower(teams.team) team,teams.id_team id,count(id_drivers_gp) as disq from drivers_gp_results,teams
where drivers_gp_results.id_team=teams.id_team and drivers_gp_results.season='$year' and race_add_info='dyskwalifikacja' group by teams.id_team";
$result = mysqli_query($dbhandle,$query);
$teamsDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];
  $teamsDisqTab[$tmp_id_team] = $r["disq"];
}

// podsumowanie
$query="SELECT distinct teams.alias_name alias,teams.id_team id,CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,
lower(teams.team) team,teams.country_code country,
place, points, points_class pointsClass, '' starts, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed,
'' incomplete, '' disq,
GROUP_CONCAT(DISTINCT teams_models.team_name ORDER BY teams_models.team_name SEPARATOR ', ') modelName
from teams_class 
LEFT JOIN teams ON teams.id_team = teams_class.id_team
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team=teams_class.id_team AND drivers_gp_involvements.season=teams_class.season
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model
where teams_class.season='$year' 
GROUP BY teams.id_team     
order by place";
$result = mysqli_query($dbhandle,$query);
$teamsSummaryItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_id_team = $r["id"];

  $r["starts"]=$teamsStartsTab[$r["id"]]==null ? 0 : $teamsStartsTab[$r["id"]];
  $r["wins"]=$teamsWinsTab[$r["id"]]==null ? 0 : $teamsWinsTab[$r["id"]];
  $r["second"]=$teamsSecondTab[$r["id"]]==null ? 0 : $teamsSecondTab[$r["id"]];
  $r["third"]=$teamsThirdTab[$r["id"]]==null ? 0 : $teamsThirdTab[$r["id"]];
  $r["podium"]=$teamsPodiumsTab[$r["id"]]==null ? 0 : $teamsPodiumsTab[$r["id"]];
  $r["polepos"]=$teamsPolePosTab[$r["id"]]==null ? 0 : $teamsPolePosTab[$r["id"]];
  $r["bestlaps"]=$teamsBLTab[$r["id"]]==null ? 0 : $teamsBLTab[$r["id"]];
  $r["pointsPlaces"]=$teamsPointsPlacesTab[$r["id"]]==null ? 0 : $teamsPointsPlacesTab[$r["id"]];
  $r["completed"]=$teamsCompletedTab[$r["id"]]==null ? 0 : $teamsCompletedTab[$r["id"]];
  $r["incomplete"]=$teamsIncompleteTab[$r["id"]]==null ? 0 : $teamsIncompleteTab[$r["id"]];
  $r["disq"]=$teamsDisqTab[$r["id"]]==null ? 0 : $teamsDisqTab[$r["id"]];
  $teamsSummaryItems[] = $r;
}

// regulamin
$query="SELECT";
if ($lang=='pl') {
  $query.=" regulations";
}else{
  $query.=" regulations_en as regulations";
}  
$query.=" from season_regulations where year='$year'";
$result = mysqli_query($dbhandle,$query);
$regulations="";
while($r = mysqli_fetch_assoc($result)) {
  $regulations = $r["regulations"];
}

$seasonSummary["gp"]=$gpItems;
$seasonSummary["classDriversSummary"]=$driversSummaryItems;
$seasonSummary["classTeamsSummary"]=$teamsSummaryItems;
$seasonSummary["regulations"]=$regulations;
$seasonSummary["createTime"]=microtime(true)-$start_time;

// Response
$response = $seasonSummary;

print json_encode($response);
mysqli_free_result($result);
?>
