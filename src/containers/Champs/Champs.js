/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Header,
  Segment,
  Item,
  Flag,
  Table,
  Statistic,
  Message,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchChamps } from "../../actions/ChampsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Champs.less";

class Champs extends Component {
  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchChamps(Cookies.get("lang"));
  }

  renderDriversChamps() {
    const items = [];
    const { champs } = this.props;
    const { drivers } = champs.data.mostTitles;
    drivers.forEach((item, idx) => {
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const link = `/driver/${item.alias}`;
      const element = (
        <Item key={item.id}>
          <NavLink to={link} className="image-link">
            <Image
              size="small"
              src={filename}
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {idx + 1}
                {". "}
                <Flag name={item.countryCode} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.country}</Item.Description>
            <Item.Extra>{item.champSeasons}</Item.Extra>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic size="large" floated="right" inverted>
              <Statistic.Value>{item.champs}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.wc"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsChamps() {
    const items = [];
    const { champs } = this.props;
    const { teams } = champs.data.mostTitles;
    teams.forEach((item, idx) => {
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.id}_profile_logo.jpg`;
      }
      const link = `/team/${item.alias}`;
      const element = (
        <Item key={item.id}>
          <NavLink to={link} className="image-link">
            <Image
              size="small"
              src={filename}
              onError={(e) => {
                e.target.src = "/build/images/teams/team_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {idx + 1}
                {". "}
                <Flag name={item.countryCode} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.country}</Item.Description>
            <Item.Extra>{item.champSeasons}</Item.Extra>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic size="large" floated="right" inverted>
              <Statistic.Value>{item.champs}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.wc"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderCountriesChamps() {
    const items = [];
    const { champs } = this.props;
    const { countries } = champs.data.mostTitles;
    countries.forEach((item, idx) => {
      const filename = `/build/images/countries/${item.id}.jpg`;
      const element = (
        <Item key={item.id}>
          <div className="image-link">
            <Image
              size="small"
              src={filename}
              onError={(e) => {
                e.target.src = "/build/images/countries/country_no_profile.jpg";
              }}
            />
          </div>
          <Item.Content>
            <Item.Header>
              {idx + 1}
              {". "}
              <Flag name={item.countryCode} />
              {item.name}
            </Item.Header>
            <Item.Extra>{item.champSeasons}</Item.Extra>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic size="large" floated="right" inverted>
              <Statistic.Value>{item.champs}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.wc"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      items.push(element);
    });
    return items;
  }

  renderDriversChampsBySeason() {
    const items = [];
    const { champs } = this.props;
    const { drivers } = champs.data.seasons;
    drivers.forEach((item) => {
      const driverPicReplace = "/build/images/drivers/driver_no_profile.jpg";
      let driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.season}>
          <Table.Cell data-title="Sezon" className="bold">
            <NavLink to={`/gp-season/${item.season}`}>{item.season}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.name}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src = driverPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.countryCode} />
                  <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>{item.country}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Starty">
            <NavLink
              to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.starts}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            <NavLink
              to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.wins}{" "}
              <small>({Math.round((item.wins * 100) / item.starts, 0)}%)</small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            <NavLink
              to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.podium}{" "}
              <small>
                ({Math.round((item.podium * 100) / item.starts, 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            <NavLink
              to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.polepos}{" "}
              <small>
                ({Math.round((item.polepos * 100) / item.starts, 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            <NavLink
              to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.bestlaps}{" "}
              <small>
                ({Math.round((item.bestlaps * 100) / item.starts, 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="bold">
            <NavLink
              to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {Math.round(item.pointsClass * 100) / 100}
              {item.points !== item.pointsClass && (
                <small>
                  {" ("}
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : Math.round(item.points * 100) / 100}
                  {")"}
                </small>
              )}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderDriversChampsBySeasonMobile() {
    const items = [];
    const { champs } = this.props;
    const { drivers } = champs.data.seasons;
    drivers.forEach((item) => {
      const picReplace = "/build/images/drivers/driver_no_profile.jpg";
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const link = `/driver/${item.alias}`;
      const element = (
        <Grid.Row key={item.season}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="image-link">
                <Image
                  size="tiny"
                  src={filename}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
            <Segment basic padded textAlign="center" className="bold">
              <NavLink to={`/gp-season/${item.season}`}>{item.season}</NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={12}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      <Flag name={item.countryCode} />
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Extra>
                    <span className="key-value-box">
                      <div className="key-value-box-header">ST</div>
                      <div className="key-value-box-value">
                        {item.starts === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.starts}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.wins"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.wins === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.wins}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.podiums"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.podium === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.podium}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.polepos"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.polepos === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.polepos}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.bestlaps === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.bestlaps}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.points"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.pointsClass === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {Math.round(item.pointsClass * 100) / 100}
                          </NavLink>
                        )}
                        {item.points !== item.pointsClass && (
                          <small>
                            <NavLink
                              to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                            >
                              {" ("}
                              {item.points === null
                                ? String.fromCharCode(160)
                                : Math.round(item.points * 100) / 100}
                              {")"}
                            </NavLink>
                          </small>
                        )}
                      </div>
                    </span>
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsChampsBySeason() {
    const items = [];
    const { champs } = this.props;
    const { teams } = champs.data.seasons;
    teams.forEach((item) => {
      const teamPicReplace = "/build/images/teams/team_no_profile.jpg";
      let teamPic = `/build/images/teams/team_${item.id}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.season}>
          <Table.Cell data-title="Sezon" className="bold">
            <NavLink to={`/gp-season/${item.season}`}>{item.season}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={`${item.name} ${item.engine}`}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src = teamPicReplace;
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.countryCode} />{" "}
                  <NavLink to={`/team/${item.alias}`}>
                    {item.name} {item.engine}
                  </NavLink>
                </div>
                <div>
                  <small>{item.country}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Starty">
            <NavLink
              to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.starts}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            <NavLink
              to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.wins}{" "}
              <small>({Math.round((item.wins * 100) / item.starts, 0)}%)</small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            <NavLink
              to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.podium}{" "}
              <small>
                ({Math.round((item.podium * 100) / (item.starts * 2), 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            <NavLink
              to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.polepos}{" "}
              <small>
                ({Math.round((item.polepos * 100) / item.starts, 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            <NavLink
              to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.bestlaps}{" "}
              <small>
                ({Math.round((item.bestlaps * 100) / item.starts, 0)}%)
              </small>
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="bold">
            <NavLink
              to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
            >
              {item.pointsClass === "0"
                ? "-"
                : Math.round(item.pointsClass * 100) / 100}
              {item.points !== item.pointsClass && (
                <small>
                  {" ("}
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : Math.round(item.points * 100) / 100}
                  {")"}
                </small>
              )}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderTeamsChampsBySeasonMobile() {
    const items = [];
    const { champs } = this.props;
    const { teams } = champs.data.seasons;
    teams.forEach((item) => {
      const picReplace = "/build/images/teams/team_no_profile.jpg";
      let filename = "/build/images/teams/team_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/teams/team_${item.id}_profile_logo.jpg`;
      }
      const link = `/team/${item.alias}`;
      const element = (
        <Grid.Row key={item.season}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={link} className="image-link">
                <Image
                  size="tiny"
                  src={filename}
                  onError={(e) => {
                    e.target.src = picReplace;
                  }}
                />
              </NavLink>
            </Segment>
            <Segment basic padded textAlign="center" className="bold">
              <NavLink to={`/gp-season/${item.season}`}>{item.season}</NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <NavLink to={link}>
                      <Flag name={item.countryCode} />
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Extra>
                    <span className="key-value-box">
                      <div className="key-value-box-header">ST</div>
                      <div className="key-value-box-value">
                        {item.starts === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.starts}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.wins"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.wins === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.wins}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.podiums"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.podium === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.podium}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.polepos"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.polepos === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.polepos}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.bestlaps"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.bestlaps === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {item.bestlaps}
                          </NavLink>
                        )}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.points"} />
                      </div>
                      <div className="key-value-box-value">
                        {item.pointsClass === null ? (
                          String.fromCharCode(160)
                        ) : (
                          <NavLink
                            to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                          >
                            {Math.round(item.pointsClass * 100) / 100}
                          </NavLink>
                        )}
                        {item.points !== item.pointsClass && (
                          <small>
                            <NavLink
                              to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1/`}
                            >
                              {" ("}
                              {item.points === null
                                ? String.fromCharCode(160)
                                : Math.round(item.points * 100) / 100}
                              {")"}
                            </NavLink>
                          </small>
                        )}
                      </div>
                    </span>
                  </Item.Extra>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  render() {
    const { champs } = this.props;
    const { formatMessage } = this.props.intl;

    if (!champs.data || champs.loading) {
      return (
        <section
          id="champs-details"
          name="champs-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { mostTitles } = champs.data;

    return (
      <section
        id="champs-details"
        name="champs-details"
        className="section-page"
      >
        <div className="section-page-content">
          <SectionPageBanner
            title={<FormattedMessage id={"app.page.champs.title"} />}
            subtitle={<FormattedMessage id={"app.page.champs.subtitle"} />}
          />
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <div className="champs-most-titles">
                  <Segment textAlign="center" basic>
                    <SectionPageHeader
                      title={<FormattedMessage id={"app.stats.drivers"} />}
                      type="tertiary"
                    />
                    <Grid centered className="info-box">
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <NavLink to={`/driver/${mostTitles.drivers[0].alias}`}>
                          <Image
                            centered
                            src={`/build/images/drivers/driver_${mostTitles.drivers[0].id}_profile.jpg`}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </Grid.Column>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <div className="vertical-middle-aligned">
                          <Segment basic textAlign="center">
                            <Statistic size="large" inverted>
                              <Header size="large" inverted>
                                <NavLink
                                  to={`/driver/${mostTitles.drivers[0].alias}`}
                                >
                                  <Flag
                                    name={mostTitles.drivers[0].countryCode}
                                  />
                                  {mostTitles.drivers[0].name}
                                </NavLink>
                              </Header>
                              <Statistic.Value>
                                {mostTitles.drivers[0].champs}
                                <small className="very">x</small>
                              </Statistic.Value>
                              <Statistic.Label>
                                <FormattedMessage id={"app.stats.wc.long"} />
                              </Statistic.Label>
                            </Statistic>
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                    <SectionPageHeader
                      title={<FormattedMessage id={"app.stats.teams"} />}
                      type="tertiary"
                    />
                    <Grid centered className="info-box">
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <NavLink to={`/team/${mostTitles.teams[0].alias}`}>
                          <Image
                            centered
                            src={`/build/images/teams/team_${mostTitles.teams[0].id}_profile_logo.jpg`}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/teams/team_no_profile.jpg";
                            }}
                          />
                        </NavLink>
                      </Grid.Column>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <div className="vertical-middle-aligned">
                          <Segment basic textAlign="center">
                            <Statistic size="large" inverted>
                              <Header size="large" inverted>
                                <NavLink
                                  to={`/team/${mostTitles.teams[0].alias}`}
                                >
                                  <Flag
                                    name={mostTitles.teams[0].countryCode}
                                  />
                                  {mostTitles.teams[0].name}
                                </NavLink>
                              </Header>
                              <Statistic.Value>
                                {mostTitles.teams[0].champs}
                                <small className="very">x</small>
                              </Statistic.Value>
                              <Statistic.Label>
                                <FormattedMessage id={"app.stats.wc.long"} />
                              </Statistic.Label>
                            </Statistic>
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                    <SectionPageHeader
                      title={<FormattedMessage id={"app.stats.countries"} />}
                      type="tertiary"
                    />
                    <Grid centered className="info-box">
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <Image
                          centered
                          src={`/build/images/countries/${mostTitles.countries[0].id}.jpg`}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/countries/country_no_profile.jpg";
                          }}
                          className="fluid"
                        />
                      </Grid.Column>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <div className="vertical-middle-aligned">
                          <Segment basic textAlign="center">
                            <Statistic size="large" inverted>
                              <Header size="large" inverted>
                                <Flag
                                  name={mostTitles.countries[0].countryCode}
                                />
                                {mostTitles.countries[0].name}
                              </Header>
                              <Statistic.Value>
                                {mostTitles.countries[0].champs}
                                <small className="very">x</small>
                              </Statistic.Value>
                              <Statistic.Label>
                                <FormattedMessage id={"app.stats.wc.long"} />
                              </Statistic.Label>
                            </Statistic>
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered>
                  <Grid.Column mobile={16} tablet={11} computer={12}>
                    <Segment basic padded>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.drivers"} />}
                        type="secondary"
                      />
                      <Segment basic>
                        <div className="hideForDesktop">
                          <Grid columns={3} divided="vertically">
                            {this.renderDriversChampsBySeasonMobile()}
                          </Grid>
                        </div>
                        <div className="hideForMobile">
                          <Segment basic className="overflow">
                            <Table
                              basic="very"
                              celled
                              className="responsive-table center-aligned"
                            >
                              <Table.Header>
                                <Table.Row>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.season"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.driver"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.gp"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.wins"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.podiums"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.bestlaps"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.points"}
                                    />
                                  </Table.HeaderCell>
                                </Table.Row>
                              </Table.Header>
                              <Table.Body>
                                {this.renderDriversChampsBySeason()}
                              </Table.Body>
                            </Table>
                          </Segment>
                        </div>
                      </Segment>
                      <Segment basic>
                        <Message>
                          <Message.Header>
                            <FormattedMessage
                              id={"app.message.header.warning"}
                            />
                          </Message.Header>
                          <p>
                            <FormattedMessage id={"app.page.champs.info1"} />
                          </p>
                        </Message>
                      </Segment>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.teams"} />}
                        type="secondary"
                      />
                      <Segment basic>
                        <div className="hideForDesktop">
                          <Grid columns={3} divided="vertically">
                            {this.renderTeamsChampsBySeasonMobile()}
                          </Grid>
                        </div>
                        <div className="hideForMobile">
                          <Segment basic className="overflow">
                            <Table
                              basic="very"
                              celled
                              className="responsive-table center-aligned"
                            >
                              <Table.Header>
                                <Table.Row>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.season"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.team"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.gp"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.wins"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.podiums"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.bestlaps"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.points"}
                                    />
                                  </Table.HeaderCell>
                                </Table.Row>
                              </Table.Header>
                              <Table.Body>
                                {this.renderTeamsChampsBySeason()}
                              </Table.Body>
                            </Table>
                          </Segment>
                        </div>
                      </Segment>
                      <Segment basic>
                        <Message>
                          <Message.Header>
                            <FormattedMessage
                              id={"app.message.header.warning"}
                            />
                          </Message.Header>
                          <p>
                            <FormattedMessage id={"app.page.champs.info2"} />
                            <br />
                            <FormattedMessage id={"app.page.champs.info3"} />
                          </p>
                        </Message>
                      </Segment>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={5} computer={4}>
                    <Segment basic className="champs-drivers">
                      <SectionPageHeader
                        title={
                          <FormattedMessage id={"app.page.champs.drivers.wc"} />
                        }
                      />
                      <div className="info-box">
                        <Item.Group divided unstackable>
                          {this.renderDriversChamps()}
                        </Item.Group>
                      </div>
                    </Segment>
                    <Segment basic className="champs-teams">
                      <SectionPageHeader
                        title={
                          <FormattedMessage id={"app.page.champs.teams.wc"} />
                        }
                      />
                      <div className="info-box">
                        <Item.Group divided unstackable>
                          {this.renderTeamsChamps()}
                        </Item.Group>
                      </div>
                    </Segment>
                    <Segment basic className="champs-countries">
                      <SectionPageHeader
                        title={
                          <FormattedMessage
                            id={"app.page.champs.countries.wc"}
                          />
                        }
                      />
                      <div className="info-box">
                        <Item.Group divided unstackable>
                          {this.renderCountriesChamps()}
                        </Item.Group>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

Champs.propTypes = {
  fetchChamps: PropTypes.func.isRequired,
  champs: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ champs }) {
  return { champs };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchChamps }, dispatch);
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Champs));
