/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  GridColumn,
  Loader,
  Segment,
  Item,
  Flag,
  Statistic,
  Table,
  Label,
  Message,
  Header,
  Divider,
  Popup,
  Tab,
  TabPane,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import ChartStatsBar from "../../components/ChartStatsBar/ChartStatsBar";
import { fetchDriver } from "../../actions/DriverActions";
import { fetchComboDrivers } from "../../actions/ComboDriversActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Driver.less";

class Driver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.driverId },
      selectSeason: {
        value: "",
        label: "",
      },
      teamsSort: {
        column: "gp",
        direction: null,
      },
      gpSort: {
        column: "gp",
        direction: null,
      },
      circuitSort: {
        column: "gp",
        direction: null,
      },
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSeasonChange = this.onSeasonChange.bind(this);
    this.onTeamsSort = this.onTeamsSort.bind(this);
    this.onGPSort = this.onGPSort.bind(this);
    this.onCircuitSort = this.onCircuitSort.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchDriver(
      props.match.params.driverId,
      props.match.params.year,
      Cookies.get("lang")
    );
    if (!props.match.params.year) {
      props.fetchComboDrivers(
        0,
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        Cookies.get("lang")
      );
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { driverId } = props.match.params;
    const nextDriverId = nextProps.match.params.driverId;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextDriverId,
        label: nextProps.comboDrivers.data?.find((e) => e.value == nextDriverId)
          ?.label,
      },
      selectSeason: {
        value: nextProps.driver.data?.career.results[0]?.season,
        label: nextProps.driver.data?.career.results[0]?.season,
      },
    });

    if (driverId !== nextDriverId) {
      props.fetchDriver(nextDriverId, nextYear, Cookies.get("lang"));
    }
  }

  onSeasonChange(event) {
    const { props } = this;

    this.setState({
      selectSeason: {
        value: event.value,
        label: props.driver.data?.career.results
          ?.map((s) => {
            return { value: s.season, label: s.season };
          })
          .find((e) => e.value == event.value)?.label,
      },
    });
    this.renderDriverSeasonDetails();
  }

  onInputChange(event) {
    const { props } = this;
    if (props.match.params.year) {
      if (event.value) {
        props.history.push({
          pathname: `/driver/${props.match.params.year}/${event.value}`,
        });
      } else {
        props.history.push({
          pathname: `/drivers-stats-list/${props.match.params.year}`,
        });
      }
    } else {
      if (event.value) {
        props.history.push({ pathname: `/driver/${event.value}` });
      } else {
        props.history.push({ pathname: `/drivers-stats-list/a` });
      }
    }
    this.setState({
      selectValue: event,
    });
  }

  onTeamsSort(value) {
    let direction = this.state.teamsSort.direction;
    if (this.state.teamsSort.column == value) {
      direction =
        this.state.teamsSort.direction == null
          ? "descending"
          : this.state.teamsSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      teamsSort: {
        column: value,
        direction: direction,
      },
    });
  }

  onGPSort(value) {
    let direction = this.state.gpSort.direction;
    if (this.state.gpSort.column == value) {
      direction =
        this.state.gpSort.direction == null
          ? "descending"
          : this.state.gpSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      gpSort: {
        column: value,
        direction: direction,
      },
    });
  }

  onCircuitSort(value) {
    let direction = this.state.circuitSort.direction;
    if (this.state.circuitSort.column == value) {
      direction =
        this.state.circuitSort.direction == null
          ? "descending"
          : this.state.circuitSort.direction == "descending"
          ? "ascending"
          : null;
    } else {
      direction = "descending";
    }

    this.setState({
      circuitSort: {
        column: value,
        direction: direction,
      },
    });
  }

  renderDriverSeasonsSummary() {
    const elements = [];
    const { props } = this;
    const { results } = props.driver.data.career;
    const { alias, teams } = props.driver.data;
    let sumGP = 0;
    let sumStarts = 0;
    let sumQuals = 0;
    let sumWins = 0;
    let sumSecond = 0;
    let sumThird = 0;
    let sumPodium = 0;
    let sumPolepos = 0;
    let sumBestlaps = 0;
    // let sumPointsPlaces = 0;
    let sumCompleted = 0;
    let sumIncomplete = 0;
    let sumDisq = 0;
    let sumPoints = 0;
    let sumPointsClass = 0;
    results.forEach((item) => {
      sumGP += parseInt(item.gp, 0);
      sumStarts += parseInt(item.starts, 0);
      sumQuals += parseInt(item.qual, 0);
      sumWins += parseInt(item.wins, 0);
      sumSecond += parseInt(item.second, 0);
      sumThird += parseInt(item.third, 0);
      sumPodium += parseInt(item.podium, 0);
      sumPolepos += parseInt(item.polepos, 0);
      sumBestlaps += parseInt(item.bestlaps, 0);
      // sumPointsPlaces += parseInt(item.pointsPlaces, 0);
      sumCompleted += parseInt(item.completed, 0);
      sumIncomplete += parseInt(item.incomplete, 0);
      sumDisq += parseInt(item.disq, 0);
      sumPoints += parseFloat(item.points, 1);
      sumPointsClass += parseFloat(item.pointsClass, 1);
    });

    const element = (
      <Table.Row key="summary" className="summary">
        <Table.Cell data-title="Razem" className="bold">
          <FormattedMessage id={"app.table.header.total"} />
        </Table.Cell>
        <Table.Cell data-title="Teamy">
          <div className="hideMobile hideTablet">
            <Popup
              position="right center"
              trigger={<a href="#driver-teams">{teams.length}</a>}
              flowing
              hoverable
            >
              <Grid className="driver-season-summary-grid">
                {teams.map((item, idx) => (
                  <GridColumn
                    key={idx}
                    className="driver-season-summary-grid-column"
                  >
                    <Segment basic padded textAlign="center">
                      <NavLink to={`/team/${item.alias}`}>
                        <Image
                          centered
                          size="large"
                          src={`/build/images/teams/team_${item.team.toLowerCase()}_profile_logo.jpg`}
                          alt={`${item.name}`}
                          className="driver-season-car-model-photo-small"
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_car.jpg";
                          }}
                        />
                      </NavLink>
                      <Header as="h3">
                        <Flag name={item.country} /> {item.name}
                      </Header>
                      <div>
                        <small>
                          {item.seasonMin == item.seasonMax
                            ? item.seasonMax
                            : `${item.seasonMin} - ${item.seasonMax}`}
                        </small>
                      </div>
                      <div>
                        <Statistic>
                          <Statistic.Value>{item.seasons}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Statistic.Label>
                        </Statistic>
                      </div>
                      <div className="buttons no-padding">
                        <div>
                          <NavLink
                            className="primary"
                            to={`/team/${item.alias}`}
                          >
                            <FormattedMessage id={"app.button.profile"} />
                          </NavLink>
                        </div>
                        <div>
                          <NavLink
                            className="secondary"
                            to={`/driver-events/gp/${alias}/${item.alias}/-/-/-/-/-/-/1/`}
                          >
                            <FormattedMessage id={"app.button.history"} />
                          </NavLink>
                        </div>
                      </div>
                    </Segment>
                  </GridColumn>
                ))}
              </Grid>
            </Popup>
          </div>
          <div className="hideDesktop">
            <a href="#driver-teams">{teams.length}</a>
          </div>
        </Table.Cell>
        <Table.Cell data-title="Grand Prix">
          {sumGP === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/gp/${alias}`}>
              {sumGP === null ? String.fromCharCode(160) : sumGP}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Kwalifikacje">
          {sumQuals === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/qual/${alias}`}>
              {sumQuals === null ? String.fromCharCode(160) : sumQuals}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Wyścigi">
          {sumStarts === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/starts/${alias}`}>
              {sumStarts === null ? String.fromCharCode(160) : sumStarts}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Wygrane">
          {sumWins === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/wins/${alias}`}>
              {sumWins === null ? String.fromCharCode(160) : sumWins}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Drugie miejsca">
          {sumSecond === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/second/${alias}`}>
              {sumSecond === null ? String.fromCharCode(160) : sumSecond}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Trzecie miejsca">
          {sumThird === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/third/${alias}`}>
              {sumThird === null ? String.fromCharCode(160) : sumThird}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Podium">
          {sumPodium === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/podium/${alias}`}>
              {sumPodium === null ? String.fromCharCode(160) : sumPodium}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Pole Position">
          {sumPolepos === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/polepos/${alias}`}>
              {sumPolepos === null ? String.fromCharCode(160) : sumPolepos}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Naj. okrążenia">
          {sumBestlaps === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/bestlaps/${alias}`}>
              {sumBestlaps === null ? String.fromCharCode(160) : sumBestlaps}
            </NavLink>
          )}
        </Table.Cell>
        {/* <Table.Cell data-title="Miejsca punktowane">
          {sumPointsPlaces === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/points-places/${alias}`}>
              {sumPointsPlaces === null
                ? String.fromCharCode(160)
                : sumPointsPlaces}
            </NavLink>
          )}
        </Table.Cell> */}
        <Table.Cell data-title="Ukończone wyścigi">
          {sumCompleted === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/completed/${alias}`}>
              {sumCompleted === null ? String.fromCharCode(160) : sumCompleted}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Nieukończone wyścigi">
          {sumIncomplete === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/incomplete/${alias}`}>
              {sumIncomplete === null
                ? String.fromCharCode(160)
                : sumIncomplete}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Punkty">
          {sumPoints === 0 ? (
            0
          ) : (
            <NavLink to={`/driver-events/points/${alias}`}>
              {sumPoints === null
                ? String.fromCharCode(160)
                : Math.round(sumPoints * 100) / 100}
              {sumPoints !== sumPointsClass && (
                <>
                  <br />
                  <small>
                    {" ("}
                    {sumPointsClass === null
                      ? String.fromCharCode(160)
                      : Math.round(sumPointsClass * 100) / 100}
                    {")"}
                  </small>
                </>
              )}
            </NavLink>
          )}
        </Table.Cell>
        <Table.Cell data-title="Miejsce"></Table.Cell>
      </Table.Row>
    );
    elements.push(element);

    const elements2 = [];
    results.forEach((item, idx) => {
      const uniqueTeamNames = [
        ...new Map(item.teams.map((item) => [item["team"], item])).values(),
      ];

      const element2 = (
        <Table.Row key={idx}>
          <Table.Cell data-title="Sezon" className="bold">
            <div className="hideMobile hideTablet">
              <Popup
                position="right center"
                trigger={
                  <NavLink to={`/gp-season/${item.season}`}>
                    {item.season}{" "}
                  </NavLink>
                }
                flowing
                hoverable
              >
                <div className="driver-season-summary-grid-title">
                  <FormattedMessage id={"app.stats.season"} /> {item.season}
                </div>
                <Grid className="driver-season-summary-grid">
                  {item.gpResults
                    .filter((e) => e.team != null)
                    .map((gp, idx) => (
                      <GridColumn
                        key={idx}
                        className="driver-season-summary-grid-column"
                      >
                        <Segment basic padded textAlign="center">
                          <div>
                            <div>
                              <Segment basic>
                                <NavLink
                                  to={`/gp-result/${gp.alias}/${item.season}`}
                                >
                                  <Image
                                    size="large"
                                    src={`/build/images/circuits/circuit_${gp.circuitAlias}_profile.jpg`}
                                    alt={gp.gp}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/circuits/circuit_no_profile.jpg";
                                    }}
                                  />
                                  <Label
                                    attached="top left"
                                    color="yellow"
                                    size="huge"
                                  >
                                    {idx + 1}
                                  </Label>
                                  <Label
                                    attached="bottom"
                                    color="yellow"
                                    size="medium"
                                  >
                                    {gp.raceDate}
                                  </Label>
                                </NavLink>
                              </Segment>
                              <Header as="h3">
                                <Flag name={gp.name} /> {gp.gp}
                              </Header>
                              <div>
                                <small>{gp.circuit}</small>
                              </div>
                            </div>
                            <div>
                              <Divider hidden></Divider>
                              <NavLink to={`/team/${gp.teamAlias}`}>
                                <Image
                                  size="tiny"
                                  src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                                  alt={gp.team}
                                  centered
                                  onError={(e) => {
                                    e.target.src =
                                      "/build/images/teams/team_no_profile.jpg";
                                  }}
                                />
                              </NavLink>
                              <Divider hidden fitted></Divider>
                              <div className="bold-uppercase">
                                {gp.teamFullName}
                              </div>
                              <Divider hidden fitted></Divider>
                              <small>
                                {gp.sprint == 1 && (
                                  <span className="key-value-box">
                                    <div className="key-value-box-header">
                                      <FormattedMessage
                                        id={"app.table.header.sprint"}
                                      />
                                    </div>
                                    <div
                                      className={`key-value-box-value ${
                                        gp.sprintPos < 4 &&
                                        `cell-${gp.sprintPos}`
                                      }`}
                                    >
                                      {gp.sprintPos || "-"}
                                    </div>
                                  </span>
                                )}
                                <span className="key-value-box">
                                  <div className="key-value-box-header">
                                    <FormattedMessage
                                      id={"app.table.header.qual"}
                                    />
                                  </div>
                                  <div
                                    className={`key-value-box-value ${
                                      gp.qual < 4 && `cell-${gp.qual}`
                                    }`}
                                  >
                                    {gp.qual || "-"}
                                  </div>
                                </span>
                                <span className="key-value-box">
                                  <div className="key-value-box-header">
                                    <FormattedMessage
                                      id={"app.table.header.race"}
                                    />
                                  </div>
                                  <div
                                    className={`key-value-box-value ${
                                      gp.place < 4 && `cell-${gp.place}`
                                    }`}
                                  >
                                    {gp.completed == 1 ? (
                                      gp.place
                                    ) : gp.qual ? (
                                      gp.notQualified == 1 ? (
                                        <FormattedMessage id={"app.stats.nq"} />
                                      ) : gp.notStarted == 1 ? (
                                        <FormattedMessage id={"app.stats.ns"} />
                                      ) : (
                                        "-"
                                      )
                                    ) : (
                                      String.fromCharCode(160)
                                    )}
                                  </div>
                                </span>
                              </small>
                            </div>
                            <div>
                              <Statistic>
                                <Statistic.Value>
                                  {gp.racePoints || 0}
                                </Statistic.Value>
                                <Statistic.Label>
                                  <FormattedMessage id={"app.stats.points"} />
                                </Statistic.Label>
                              </Statistic>
                            </div>
                            <div className="buttons no-padding">
                              <NavLink
                                className="secondary"
                                to={`/gp-result/${gp.alias}/${item.season}`}
                              >
                                <FormattedMessage id={"app.button.results"} />
                              </NavLink>
                            </div>
                          </div>
                        </Segment>
                      </GridColumn>
                    ))}
                </Grid>
              </Popup>
            </div>
            <div className="hideDesktop">
              <NavLink to={`/gp-season/${item.season}`}>{item.season} </NavLink>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespoły">
            {uniqueTeamNames.map((e, idx) => (
              <Popup
                key={idx}
                position="right center"
                trigger={
                  <div>
                    <NavLink
                      to={`/driver-events/gp/${item.alias}/${e.alias}/-/-/-/-/-/${item.season}/1`}
                    >
                      <Image
                        size="mini"
                        src={`/build/images/teams/team_${e.team}_profile_logo.jpg`}
                        alt={e.team}
                        centered
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                  </div>
                }
                flowing
                hoverable
              >
                <Grid
                  columns={
                    item.teams.filter((x) => x.team == e.team).length <= 6
                      ? item.teams.filter((x) => x.team == e.team).length
                      : 6
                  }
                  className="driver-season-summary-grid"
                >
                  {item.teams
                    .filter((x) => x.team == e.team)
                    .map((team, idx) => (
                      <GridColumn
                        key={idx}
                        className="driver-season-summary-grid-column"
                      >
                        <Segment basic padded textAlign="center">
                          <NavLink to={`/team/${team.alias}`}>
                            <Image
                              size="large"
                              src={`/build/images/teams/models/team_${team.team.toLowerCase()}_${team.model
                                .toLowerCase()
                                .replace("???", "3")
                                .replace("??", "2")
                                .replace("?", "1")}.jpg`}
                              alt={`${team.name} ${team.model.replaceAll(
                                "???",
                                ""
                              )} ${team.season}`}
                              centered
                              onError={(e) => {
                                e.target.src =
                                  "/build/images/teams/team_no_car.jpg";
                              }}
                            />
                          </NavLink>
                          <Header as="h3">
                            <Flag name={team.countryCode} />
                            {team.name} {team.engine}
                          </Header>
                          <div>
                            <small>
                              {team.engine ? team.engine : team.name}{" "}
                              {team.model?.replace("_", "/").replace("?", "")}
                            </small>
                          </div>
                          <div>
                            <small className="very">
                              {team.teamModelName ? (
                                team.teamModelName
                              ) : (
                                <FormattedMessage id={"app.stats.private"} />
                              )}
                            </small>
                          </div>
                          <div>
                            <Statistic>
                              <Statistic.Value>{team.starts}</Statistic.Value>
                              <Statistic.Label>
                                <FormattedMessage id={"app.stats.gp"} />
                              </Statistic.Label>
                            </Statistic>
                          </div>
                          <div className="buttons no-padding">
                            <div>
                              <NavLink
                                className="primary"
                                to={`/team/${team.alias}`}
                              >
                                <FormattedMessage id={"app.button.profile"} />
                              </NavLink>
                            </div>
                            <div>
                              {/* <NavLink
                                className="secondary"
                                to={`/team-model-events/gp/${team.idTeamModel}/${team.season}/${alias}`}
                              > */}
                              <NavLink
                                className="secondary"
                                to={`/driver-events/gp/${item.alias}/-/-/-/-/${team.idTeamModel}/-/${item.season}/1`}
                              >
                                <FormattedMessage id={"app.button.history"} />
                              </NavLink>
                            </div>
                          </div>
                        </Segment>
                      </GridColumn>
                    ))}
                </Grid>
              </Popup>
            ))}
          </Table.Cell>
          <Table.Cell data-title="Grand Prix">
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje">
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścigi">
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Wygrane"
            className={item.wins > 0 ? "cell-1" : ""}
          >
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Drugie miejsca"
            className={item.second > 0 ? "cell-2" : ""}
          >
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Trzecie miejsca"
            className={item.third > 0 ? "cell-3" : ""}
          >
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium">
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position">
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia">
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          {/* <Table.Cell data-title="Miejsca punktowane">
            {item.pointsPlaces === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points-places/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.pointsPlaces === null
                  ? String.fromCharCode(160)
                  : item.pointsPlaces}
              </NavLink>
            )}
          </Table.Cell> */}
          <Table.Cell data-title="Ukończone wyścigi">
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Nieukończone wyścigi">
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty">
            {item.points === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
              >
                {item.points === null
                  ? String.fromCharCode(160)
                  : Math.round(item.points * 100) / 100}
                {item.points !== item.pointsClass && (
                  <small>
                    {" ("}
                    {item.pointsClass === null
                      ? String.fromCharCode(160)
                      : Math.round(item.pointsClass * 100) / 100}
                    {")"}
                  </small>
                )}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Miejsce"
            className={`bold cell-${item.place < 4 && item.place}`}
          >
            <NavLink to={`/classification/drivers-points/${item.season}`}>
              {item.place === "1" && item.season < 2025 && (
                <FormattedMessage id={"app.stats.wc"} />
              )}
              {item.season == 2025 && item.place === "1" && item.place}
              {item.place !== "1" &&
                (item.place == "NK" ? (
                  <FormattedMessage id={"app.stats.nc"} />
                ) : (
                  item.place
                ))}
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements2.push(element2);
    });

    elements.push(elements2);

    return elements;
  }

  renderDriverSeasonDetails() {
    const elements = [];
    const { props } = this;
    const { results, gpPlaces, ppPlaces } = props.driver.data.career;
    let sumGP = 0;
    let sumStarts = 0;
    let sumQuals = 0;
    let sumWins = 0;
    let sumSecond = 0;
    let sumThird = 0;
    let sumPodium = 0;
    let sumPolepos = 0;
    let sumBestlaps = 0;
    let sumPointsPlaces = 0;
    let sumCompleted = 0;
    let sumIncomplete = 0;
    let sumDisq = 0;
    let sumPoints = 0;
    let sumPointsClass = 0;

    const { formatMessage } = this.props.intl;
    const selectedSeason = this.state.selectSeason.value;
    const filteredSeason = results.filter((e) => e.season == selectedSeason);

    filteredSeason.forEach((item) => {
      const gpPlacesData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseInt(item.place, 0);
        return gpPlacesData.push(value);
      });

      const gpPointsData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseFloat(item.points, 0);
        return gpPointsData.push(value);
      });

      const gpPointsClassData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseFloat(item.classPoints, 0);
        return gpPointsClassData.push(value);
      });

      const configGPResults = {
        chart: {
          height: 250,
        },
        title: {
          text: "",
        },
        subtitle: {
          text: " ",
        },
        xAxis: {
          type: "category",
        },
        yAxis: [
          {
            title: {
              text: "",
            },
            min: 0,
            allowDecimals: false,
            reversed: true,
            tickInterval: 1,
          },
          {
            title: {
              text: "",
            },
            opposite: true,
            min: 0,
            allowDecimals: false,
          },
        ],
        legend: {
          enabled: true,
        },
        credits: {
          enabled: false,
        },
        tooltip: {
          headerFormat: "<b>GP: {point.key}</b><br/>",
          shared: false,
          useHTML: true,
        },
        plotOptions: {
          column: {
            grouping: false,
          },
          series: {
            borderWidth: 0,
            label: {
              enabled: false,
            },
            dataLabels: {
              enabled: true,
              format: "{point.y}",
            },
            cursor: "pointer",
          },
        },
        series: [
          {
            yAxis: 1,
            type: "column",
            name: formatMessage({ id: "app.stats.points" }),
            color: "#b1b1b1",
            maxPointWidth: 20,
            data: gpPointsData,
            stack: "Punkty",
          },
          {
            yAxis: 1,
            type: "column",
            name: formatMessage({ id: "app.stats.points.class" }),
            color: "#cbb973",
            maxPointWidth: 20,
            data: gpPointsClassData,
            stack: "Punkty",
            dataLabels: {
              inside: true,
            },
          },
          {
            type: "line",
            name: formatMessage({ id: "app.stats.places" }),
            color: "#000000",
            maxPointWidth: 20,
            data: gpPlacesData,
            label: {
              onArea: false,
            },
          },
        ],
      };

      const gpPlacesBySeasonData = [];
      gpPlaces
        ?.filter((e) => e.season == item.season)
        .sort((a, b) => a.name - b.name)
        .map((item) => {
          const value = { name: "", y: 0 };
          value.name = item.name;
          value.y = parseInt(item.y, 0);
          return gpPlacesBySeasonData.push(value);
        });

      const ppPlacesBySeasonData = [];
      ppPlaces
        ?.filter((e) => e.season == item.season)
        .sort((a, b) => a.name - b.name)
        .map((item) => {
          const value = { name: "", y: 0 };
          value.name = item.name;
          value.y = parseInt(item.y, 0);
          return ppPlacesBySeasonData.push(value);
        });

      const configPlacesBySeason = {
        chart: {
          height: 200,
          type: "column",
        },
        title: {
          text: "",
        },
        subtitle: {
          text: " ",
        },
        xAxis: {
          type: "category",
        },
        yAxis: {
          min: 0,
          allowDecimals: false,
          tickInterval: 1,
          title: {
            text: "",
          },
        },
        legend: {
          enabled: true,
        },
        credits: {
          enabled: false,
        },
        tooltip: {
          headerFormat:
            "<b>{series.name}</b></br>" +
            formatMessage({ id: "app.stats.place" }) +
            " {point.key}: <b>{point.y:f}</b>",
          pointFormat: "",
          footerFormat: "",
          shared: false,
          useHTML: true,
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: "{point.y}",
            },
            cursor: "pointer",
            events: {
              click: function (event) {
                if (event.point.series.name.startsWith("Pola startowe")) {
                  props.history.push({
                    pathname: `/driver-events/grid-places/${item.alias}/-/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                  });
                } else {
                  props.history.push({
                    pathname: `/driver-events/race-places/${item.alias}/-/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                  });
                }
              },
            },
          },
        },
        series: [
          {
            name: formatMessage({ id: "app.stats.race" }),
            color: "#cbb973",
            maxPointWidth: 20,
            data: gpPlacesBySeasonData,
          },
          {
            name: formatMessage({ id: "app.stats.grid" }),
            color: "#b1b1b1",
            maxPointWidth: 20,
            data: ppPlacesBySeasonData,
          },
        ],
      };

      const element = (
        <Segment basic key={item.season}>
          <Segment basic>
            <div className="box-image-name">
              <Statistic>
                <Statistic.Value>
                  <NavLink to={`/classification/drivers-points/${item.season}`}>
                    {item.place === "1" && item.season < 2025 && (
                      <FormattedMessage id={"app.stats.wc"} />
                    )}
                    {item.season == 2025 && item.place === "1" && item.place}
                    {item.place !== "1" && item.place != "NK" ? (
                      item.place
                    ) : (
                      <FormattedMessage id={"app.stats.nc"} />
                    )}
                  </NavLink>
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.place"} />
                </Statistic.Label>
              </Statistic>
            </div>
            <Divider></Divider>
            <div className="driver-season-car-container">
              {item.carModels?.map((e, idx) => (
                <div key={idx} className="driver-season-car-model">
                  <div className="box-image-name">
                    <NavLink to={`/team/${e.alias}`}>
                      <Image
                        size="tiny"
                        src={`/build/images/teams/team_${e.team}_profile_logo.jpg`}
                        alt={e.name}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                    {e.engine && (
                      <Image
                        size="tiny"
                        src={`/build/images/engines/engine_${e.engine
                          ?.toLowerCase()
                          .replaceAll(".", "_")
                          .replaceAll(" ", "_")}.jpg`}
                        alt={e.name}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    )}
                    <div>
                      <div>
                        <Flag name={e.countryCode} />{" "}
                        <NavLink to={`/team/${e.alias}`}>{e.name}</NavLink>
                      </div>
                      <div>
                        <small>
                          {e.teamName ? (
                            e.teamName
                          ) : (
                            <FormattedMessage id={"app.stats.private"} />
                          )}
                        </small>
                      </div>
                    </div>
                  </div>
                  <Divider hidden fitted></Divider>
                  <NavLink
                    to={`/team-model-events/gp/${e.idTeamModel}/${item.season}/${item.alias}`}
                  >
                    <Image
                      centered
                      src={`/build/images/teams/models/team_${e.team.toLowerCase()}_${e.model
                        .toLowerCase()
                        .replace("???", "3")
                        .replace("??", "2")
                        .replace("?", "1")}.jpg`}
                      alt={`${e.name} ${e.model.replaceAll("???", "")} ${
                        item.season
                      }`}
                      className="driver-season-car-model-photo"
                      onError={(e) => {
                        e.target.src = "/build/images/teams/team_no_car.jpg";
                      }}
                    />
                  </NavLink>
                  <div className="bold-uppercase">
                    <small>
                      {e.name} {e.model?.replace("_", "/").replace("?", "")}
                    </small>
                  </div>
                  <div>
                    <small>
                      {e.teamName ? (
                        e.teamName
                      ) : (
                        <FormattedMessage id={"app.stats.private"} />
                      )}
                    </small>
                  </div>
                </div>
              ))}
            </div>
            <Divider></Divider>
            <div className="driver-season-stats overflow">
              <Table basic="very" celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell className="no-wrap no-background">
                      {item.gp === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.gp === null
                            ? String.fromCharCode(160)
                            : item.gp}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.qual === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.qual === null
                            ? String.fromCharCode(160)
                            : item.qual}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.starts === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.starts === null
                            ? String.fromCharCode(160)
                            : item.starts}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.wins === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.wins === null
                            ? String.fromCharCode(160)
                            : item.wins}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.second === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.second === null
                            ? String.fromCharCode(160)
                            : item.second}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.third === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.third === null
                            ? String.fromCharCode(160)
                            : item.third}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.podium === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.podium === null
                            ? String.fromCharCode(160)
                            : item.podium}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.polepos === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.polepos === null
                            ? String.fromCharCode(160)
                            : item.polepos}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.bestlaps === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.bestlaps === null
                            ? String.fromCharCode(160)
                            : item.bestlaps}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.completed === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.completed === null
                            ? String.fromCharCode(160)
                            : item.completed}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.incomplete === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.incomplete === null
                            ? String.fromCharCode(160)
                            : item.incomplete}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.disq === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/disq/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.disq === null
                            ? String.fromCharCode(160)
                            : item.disq}
                        </NavLink>
                      )}
                    </Table.Cell>
                    <Table.Cell className="no-wrap no-background">
                      {item.points === 0 ? (
                        0
                      ) : (
                        <NavLink
                          to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                        >
                          {item.points === 0
                            ? String.fromCharCode(160)
                            : Math.round(item.points * 100) / 100}
                          {item.points !== item.pointsClass && (
                            <small>
                              {" ("}
                              {item.pointsClass === 0
                                ? String.fromCharCode(160)
                                : Math.round(item.pointsClass * 100) / 100}
                              {")"}
                            </small>
                          )}
                        </NavLink>
                      )}
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
              <Segment basic textAlign="center" className="overflow">
                <span className="gp-key-value-box">
                  <div>{String.fromCharCode(160)}</div>
                  <div>
                    {String.fromCharCode(160)}
                    <br />
                    {String.fromCharCode(160)}
                  </div>
                  <div>{String.fromCharCode(160)}</div>
                  <div className="gp-key-value-box-value-first">
                    <Popup
                      content={<FormattedMessage id={"app.stats.qual"} />}
                      position="bottom center"
                      wide="very"
                      trigger={
                        <small>
                          <FormattedMessage id={"app.table.header.qual"} />
                        </small>
                      }
                    />
                  </div>
                  <div className="gp-key-value-box-value">
                    <Popup
                      content={<FormattedMessage id={"app.stats.grid"} />}
                      position="bottom center"
                      wide="very"
                      trigger={
                        <small>
                          <FormattedMessage id={"app.table.header.grid"} />
                        </small>
                      }
                    />
                  </div>
                  <div className="gp-key-value-box-value">
                    <Popup
                      content={<FormattedMessage id={"app.stats.race"} />}
                      position="bottom center"
                      wide="very"
                      trigger={
                        <small>
                          <FormattedMessage id={"app.table.header.race"} />
                        </small>
                      }
                    />
                  </div>
                  <div className="gp-key-value-box-value-points">
                    <small>
                      <FormattedMessage id={"app.table.header.points"} />
                    </small>
                  </div>
                  <div className="gp-key-value-box-value-empty">
                    {String.fromCharCode(160)}
                  </div>
                </span>
                {item.gpResults.map((gp, idx) => (
                  <span key={idx} className="gp-key-value-box">
                    {gp.sprint == "1" ? (
                      <div className="gp-key-value-box-value-sprint">
                        <small>
                          <FormattedMessage id={"app.table.header.sprint"} />
                        </small>
                      </div>
                    ) : (
                      <div className="gp-key-value-box-value-empty">
                        {String.fromCharCode(160)}
                      </div>
                    )}
                    <div className="gp-key-value-box-header-top">{idx + 1}</div>
                    <div className="gp-key-value-box-header">
                      <Popup
                        content={`${gp.gp} - ${gp.raceDate}`}
                        position="bottom center"
                        wide="very"
                        trigger={
                          <NavLink to={`/gp-result/${gp.alias}/${item.season}`}>
                            <small>{gp.nameShort}</small>
                            <br />
                            <Flag name={gp.name} />
                          </NavLink>
                        }
                      />
                    </div>
                    <div className="gp-key-value-box-value">
                      {gp.modelName ? (
                        <Popup
                          content={`${gp.teamFullName} - ${
                            gp.teamName ? (
                              gp.teamName
                            ) : (
                              <FormattedMessage id={"app.stats.private"} />
                            )
                          } ${gp.modelName}`}
                          position="bottom center"
                          wide="very"
                          trigger={
                            <Image
                              size="mini"
                              src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                              alt={gp.team}
                              className="center-aligned"
                              onError={(e) => {
                                e.target.src =
                                  "/build/images/teams/team_no_profile.jpg";
                              }}
                            />
                          }
                        />
                      ) : (
                        <Image
                          size="mini"
                          src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                          alt={gp.team}
                          className="center-aligned"
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_profile.jpg";
                          }}
                        />
                      )}
                    </div>
                    <div className="gp-key-value-box-value">
                      {gp.qual ? gp.qual : String.fromCharCode(160)}
                      {gp.qualInfo && (
                        <>
                          {String.fromCharCode(160)}
                          <Popup
                            content={`${gp.qualInfo}`}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <Label
                                attached="top right"
                                color="blue"
                                size="small"
                              >
                                !
                              </Label>
                            }
                          />
                        </>
                      )}
                    </div>
                    <div
                      className={
                        gp.grid == "1"
                          ? `gp-key-value-box-value cell-${gp.grid}`
                          : "gp-key-value-box-value"
                      }
                    >
                      <small>
                        {gp.grid
                          ? gp.grid
                          : gp.qual
                          ? "-"
                          : String.fromCharCode(160)}
                      </small>
                      {gp.gridInfo && (
                        <>
                          {String.fromCharCode(160)}
                          <Popup
                            content={`${gp.gridInfo}`}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <Label
                                attached="top right"
                                color="blue"
                                size="small"
                              >
                                !
                              </Label>
                            }
                          />
                        </>
                      )}
                    </div>
                    <div
                      className={
                        gp.place == "1" || gp.place == "2" || gp.place == "3"
                          ? `gp-key-value-box-value cell-${gp.place}`
                          : "gp-key-value-box-value"
                      }
                    >
                      <small>
                        {gp.completed == 1 ? (
                          gp.place
                        ) : gp.qual ? (
                          gp.notQualified == 1 ? (
                            <FormattedMessage id={"app.stats.nq"} />
                          ) : gp.notStarted == 1 ? (
                            <FormattedMessage id={"app.stats.ns"} />
                          ) : (
                            "-"
                          )
                        ) : (
                          String.fromCharCode(160)
                        )}
                      </small>
                      {gp.info && (
                        <>
                          {String.fromCharCode(160)}
                          <Popup
                            content={`${gp.info} (${formatMessage({
                              id: "app.stats.lap",
                            })} ${gp.laps})`}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <Label
                                attached="top right"
                                color="blue"
                                size="small"
                              >
                                !
                              </Label>
                            }
                          />
                        </>
                      )}
                    </div>
                    <div
                      className={
                        gp.points > 0
                          ? `gp-key-value-box-value-points-scored`
                          : "gp-key-value-box-value-points"
                      }
                    >
                      {gp.excluded == null || gp.excluded == "0" ? (
                        <small>
                          {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                        </small>
                      ) : (
                        <>
                          <small className="red">{gp.points}</small>
                          <Popup
                            content={formatMessage({
                              id: "app.stats.points.excluded",
                            })}
                            position="bottom center"
                            wide="very"
                            trigger={
                              <Label
                                attached="top right"
                                color="blue"
                                size="small"
                              >
                                !
                              </Label>
                            }
                          />
                        </>
                      )}
                      {gp.sprintPoints > 1 && (
                        <>
                          {String.fromCharCode(160)}
                          <Popup
                            content={
                              formatMessage({
                                id: "app.stats.points.sprint.part1",
                              }) +
                              gp.sprintPoints +
                              formatMessage({
                                id: "app.stats.points.sprint.part2",
                              }) +
                              gp.sprintPos +
                              formatMessage({
                                id: "app.stats.points.sprint.part3",
                              })
                            }
                            position="bottom center"
                            wide="very"
                            trigger={
                              <Label
                                attached="top right"
                                color="blue"
                                size="small"
                              >
                                !
                              </Label>
                            }
                          />
                        </>
                      )}
                    </div>
                    {gp.bestLap == "1" ? (
                      <div className="gp-key-value-box-value-bl">
                        <small>
                          <FormattedMessage id={"app.table.header.bestlaps"} />
                        </small>
                      </div>
                    ) : (
                      <div className="gp-key-value-box-value-empty">
                        {String.fromCharCode(160)}
                      </div>
                    )}
                  </span>
                ))}
              </Segment>
            </div>
          </Segment>
          {item.starts > 0 ? (
            <Segment basic>
              <Divider hidden></Divider>
              <SectionPageHeader
                title={
                  <FormattedMessage
                    id={"app.page.driver.season.details.chart1.header"}
                  />
                }
                type="quaternary"
              />
              <Divider hidden></Divider>
              <ReactHighcharts config={configGPResults} />
              <SectionPageHeader
                title={
                  <FormattedMessage
                    id={"app.page.driver.season.details.chart2.header"}
                  />
                }
                type="quaternary"
              />
              <Divider hidden></Divider>
              <ReactHighcharts config={configPlacesBySeason} />
            </Segment>
          ) : (
            <Segment basic>
              <Message>
                <Message.Header>
                  <FormattedMessage id={"app.message.header"} />
                </Message.Header>
                <p>
                  <FormattedMessage id={"app.page.driver.charts.grid.info"} />
                </p>
              </Message>
            </Segment>
          )}
        </Segment>
      );
      elements.push(element);
      sumGP += parseInt(item.gp, 0);
      sumStarts += parseInt(item.starts, 0);
      sumQuals += parseInt(item.qual, 0);
      sumWins += parseInt(item.wins, 0);
      sumSecond += parseInt(item.second, 0);
      sumThird += parseInt(item.third, 0);
      sumPodium += parseInt(item.podium, 0);
      sumPolepos += parseInt(item.polepos, 0);
      sumBestlaps += parseInt(item.bestlaps, 0);
      sumPointsPlaces += parseInt(item.pointsPlaces, 0);
      sumCompleted += parseInt(item.completed, 0);
      sumIncomplete += parseInt(item.incomplete, 0);
      sumDisq += parseInt(item.disq, 0);
      sumPoints += parseFloat(item.points, 1);
      sumPointsClass += parseFloat(item.pointsClass, 1);
    });

    return elements;
  }

  renderDriverSeasonDetailsMobile() {
    const elements = [];
    const { props } = this;
    const { results, gpPlaces, ppPlaces } = props.driver.data.career;

    const { formatMessage } = this.props.intl;
    const selectedSeason = this.state.selectSeason.value;
    const filteredSeason = results.filter((e) => e.season == selectedSeason);

    filteredSeason.forEach((item) => {
      const gpPlacesData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseInt(item.place, 0);
        return gpPlacesData.push(value);
      });

      const gpPointsData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseFloat(item.points, 0);
        return gpPointsData.push(value);
      });

      const gpPointsClassData = [];
      item.gpResults.map((item) => {
        const value = { name: "", y: 0 };
        value.name = item.nameShort;
        value.y = parseFloat(item.classPoints, 0);
        return gpPointsClassData.push(value);
      });

      const configGPResults = {
        chart: {
          height: 250,
        },
        title: {
          text: "",
        },
        subtitle: {
          text: " ",
        },
        xAxis: {
          type: "category",
        },
        yAxis: [
          {
            title: {
              text: "",
            },
            min: 0,
            allowDecimals: false,
            reversed: true,
            tickInterval: 1,
          },
          {
            title: {
              text: "",
            },
            opposite: true,
            min: 0,
            allowDecimals: false,
          },
        ],
        legend: {
          enabled: true,
        },
        credits: {
          enabled: false,
        },
        tooltip: {
          headerFormat: "<b>GP: {point.key}</b><br/>",
          shared: false,
          useHTML: true,
        },
        plotOptions: {
          column: {
            grouping: false,
          },
          series: {
            borderWidth: 0,
            label: {
              enabled: false,
            },
            dataLabels: {
              enabled: true,
              format: "{point.y}",
            },
            cursor: "pointer",
          },
        },
        series: [
          {
            yAxis: 1,
            type: "column",
            name: formatMessage({ id: "app.stats.points" }),
            color: "#b1b1b1",
            maxPointWidth: 20,
            data: gpPointsData,
            stack: "Punkty",
          },
          {
            yAxis: 1,
            type: "column",
            name: formatMessage({ id: "app.stats.points.class" }),
            color: "#cbb973",
            maxPointWidth: 20,
            data: gpPointsClassData,
            stack: "Punkty",
            dataLabels: {
              inside: true,
            },
          },
          {
            type: "line",
            name: formatMessage({ id: "app.stats.places" }),
            color: "#000000",
            maxPointWidth: 20,
            data: gpPlacesData,
            label: {
              onArea: false,
            },
          },
        ],
      };

      const gpPlacesBySeasonData = [];
      gpPlaces
        ?.filter((e) => e.season == item.season)
        .sort((a, b) => a.name - b.name)
        .map((item) => {
          const value = { name: "", y: 0 };
          value.name = item.name;
          value.y = parseInt(item.y, 0);
          return gpPlacesBySeasonData.push(value);
        });

      const ppPlacesBySeasonData = [];
      ppPlaces
        ?.filter((e) => e.season == item.season)
        .sort((a, b) => a.name - b.name)
        .map((item) => {
          const value = { name: "", y: 0 };
          value.name = item.name;
          value.y = parseInt(item.y, 0);
          return ppPlacesBySeasonData.push(value);
        });

      const configPlacesBySeason = {
        chart: {
          height: 200,
          type: "column",
        },
        title: {
          text: "",
        },
        subtitle: {
          text: " ",
        },
        xAxis: {
          type: "category",
        },
        yAxis: {
          min: 0,
          allowDecimals: false,
          tickInterval: 1,
          title: {
            text: "",
          },
        },
        legend: {
          enabled: true,
        },
        credits: {
          enabled: false,
        },
        tooltip: {
          headerFormat:
            "<b>{series.name}</b></br>" +
            formatMessage({ id: "app.stats.place" }) +
            " {point.key}: <b>{point.y:f}x</b>",
          pointFormat: "",
          footerFormat: "",
          shared: false,
          useHTML: true,
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: "{point.y}",
            },
            cursor: "pointer",
            events: {
              click: function (event) {
                if (event.point.series.name.startsWith("Pola startowe")) {
                  props.history.push({
                    pathname: `/driver-events/grid-places/${item.alias}/-/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                  });
                } else {
                  props.history.push({
                    pathname: `/driver-events/race-places/${item.alias}/-/-/-/-/-/-/${selectedSeason}/${event.point.name}/`,
                  });
                }
              },
            },
          },
        },
        series: [
          {
            name: formatMessage({ id: "app.stats.race" }),
            color: "#cbb973",
            maxPointWidth: 20,
            data: gpPlacesBySeasonData,
          },
          {
            name: formatMessage({ id: "app.stats.grid" }),
            color: "#b1b1b1",
            maxPointWidth: 20,
            data: ppPlacesBySeasonData,
          },
        ],
      };

      const element = (
        <Segment basic key={item.season}>
          <Segment basic>
            <Statistic size="mini">
              <Statistic.Value>
                {item.place === "1" && item.season < 2025 && (
                  <FormattedMessage id={"app.stats.wc"} />
                )}
                {item.season == 2025 && item.place === "1" && item.place}
                {item.place !== "1" && item.place}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.place"} />
              </Statistic.Label>
            </Statistic>
          </Segment>
          <Segment basic>
            <Divider></Divider>
            {item.carModels?.map((e, idx) => (
              <Segment basic key={idx}>
                <div className="box-image-name" key={idx}>
                  <div>
                    <NavLink to={`/team/${e.alias}`}>
                      <Image
                        size="tiny"
                        src={`/build/images/teams/team_${e.team}_profile_logo.jpg`}
                        alt={e.name}
                        className="cell-photo"
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                  </div>
                  {e.engine && (
                    <div>
                      <Image
                        size="tiny"
                        src={`/build/images/engines/engine_${e.engine
                          ?.toLowerCase()
                          .replaceAll(".", "_")
                          .replaceAll(" ", "_")}.jpg`}
                        alt={e.name}
                        className="cell-photo"
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </div>
                  )}
                  <div>
                    <div>
                      <Flag name={e.countryCode} />{" "}
                      <NavLink to={`/team/${e.alias}`}>{e.name}</NavLink>
                    </div>
                    <div>
                      <small>
                        {e.teamName ? (
                          e.teamName
                        ) : (
                          <FormattedMessage id={"app.stats.private"} />
                        )}
                      </small>
                    </div>
                  </div>
                </div>
                <Divider hidden fitted></Divider>
                <NavLink
                  to={`/team-model-events/gp/${e.idTeamModel}/${item.season}/${item.alias}`}
                >
                  <Image
                    centered
                    src={`/build/images/teams/models/team_${e.team.toLowerCase()}_${e.model
                      .toLowerCase()
                      .replace("???", "3")
                      .replace("??", "2")
                      .replace("?", "1")}.jpg`}
                    alt={`${e.name} ${e.model.replaceAll("???", "")} ${
                      item.season
                    }`}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_car.jpg";
                    }}
                  />
                </NavLink>
                <Segment basic padded>
                  <div className="bold-uppercase">
                    <small>
                      {e.name} {e.model?.replace("_", "/").replace("?", "")}
                    </small>
                  </div>
                  <div>
                    <small>
                      {e.teamName ? (
                        e.teamName
                      ) : (
                        <FormattedMessage id={"app.stats.private"} />
                      )}
                    </small>
                  </div>
                </Segment>
                <Divider></Divider>
              </Segment>
            ))}
          </Segment>
          <Segment basic className="overflow">
            <table className="basic-table">
              <thead>
                <tr>
                  <th>
                    <FormattedMessage id={"app.table.header.gp"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.qual"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.race"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p1"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p2"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.p3"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.podiums"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.polepos"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.bestlaps"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.compl"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.inc"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.disq"} />
                  </th>
                  <th>
                    <FormattedMessage id={"app.table.header.points"} />
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="Grand Prix">
                    {item.gp === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/gp/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.gp === null ? String.fromCharCode(160) : item.gp}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Kwalifikacje">
                    {item.qual === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/qual/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.qual === null
                          ? String.fromCharCode(160)
                          : item.qual}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyścig">
                    {item.starts === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/starts/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.starts === null
                          ? String.fromCharCode(160)
                          : item.starts}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P1">
                    {item.wins === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/wins/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.wins === null
                          ? String.fromCharCode(160)
                          : item.wins}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P2">
                    {item.second === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/second/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.second === null
                          ? String.fromCharCode(160)
                          : item.second}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="P3">
                    {item.third === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/third/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.third === null
                          ? String.fromCharCode(160)
                          : item.third}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Podium">
                    {item.podium === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/podium/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.podium === null
                          ? String.fromCharCode(160)
                          : item.podium}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Pole Position">
                    {item.polepos === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/polepos/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.polepos === null
                          ? String.fromCharCode(160)
                          : item.polepos}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Naj. okrążenia">
                    {item.bestlaps === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/bestlaps/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.bestlaps === null
                          ? String.fromCharCode(160)
                          : item.bestlaps}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyś. ukończone">
                    {item.completed === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/completed/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.completed === null
                          ? String.fromCharCode(160)
                          : item.completed}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Wyś. nieukończone">
                    {item.incomplete === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/incomplete/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.incomplete === null
                          ? String.fromCharCode(160)
                          : item.incomplete}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Dyskwalifikacje">
                    {item.disq === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/disq/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.disq === null
                          ? String.fromCharCode(160)
                          : item.disq}
                      </NavLink>
                    )}
                  </td>
                  <td data-title="Punkty">
                    {item.points === 0 ? (
                      0
                    ) : (
                      <NavLink
                        to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${item.season}/1`}
                      >
                        {item.points === 0
                          ? String.fromCharCode(160)
                          : Math.round(item.points * 100) / 100}
                        {item.points !== item.pointsClass && (
                          <small>
                            {" ("}
                            {item.pointsClass === 0
                              ? String.fromCharCode(160)
                              : Math.round(item.pointsClass * 100) / 100}
                            {")"}
                          </small>
                        )}
                      </NavLink>
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
            <Divider hidden></Divider>
          </Segment>
          <Segment basic textAlign="center" className="overflow">
            <Divider hidden></Divider>
            <span className="gp-key-value-box">
              <div>{String.fromCharCode(160)}</div>
              <div>
                {String.fromCharCode(160)}
                <br />
                {String.fromCharCode(160)}
              </div>
              <div>{String.fromCharCode(160)}</div>
              <div className="gp-key-value-box-value-first">
                <Popup
                  content="Kwalifikacje"
                  position="bottom center"
                  wide="very"
                  trigger={
                    <small>
                      <FormattedMessage id={"app.table.header.qual"} />
                    </small>
                  }
                />
              </div>
              <div className="gp-key-value-box-value">
                <Popup
                  content="Pole startowe"
                  position="bottom center"
                  wide="very"
                  trigger={
                    <small>
                      <FormattedMessage id={"app.table.header.grid"} />
                    </small>
                  }
                />
              </div>
              <div className="gp-key-value-box-value">
                <Popup
                  content="Wyścig"
                  position="bottom center"
                  wide="very"
                  trigger={
                    <small>
                      <FormattedMessage id={"app.table.header.race"} />
                    </small>
                  }
                />
              </div>
              <div className="gp-key-value-box-value-points">
                <small>
                  <FormattedMessage id={"app.table.header.points"} />
                </small>
              </div>
              <div className="gp-key-value-box-value-empty">
                {String.fromCharCode(160)}
              </div>
            </span>
            {item.gpResults.map((gp, idx) => (
              <span key={idx} className="gp-key-value-box">
                {gp.sprint == "1" ? (
                  <div className="gp-key-value-box-value-sprint">
                    <small>Spr</small>
                  </div>
                ) : (
                  <div className="gp-key-value-box-value-empty">
                    {String.fromCharCode(160)}
                  </div>
                )}
                <div className="gp-key-value-box-header-top">{idx + 1}</div>
                <div className="gp-key-value-box-header">
                  <Popup
                    content={`${gp.gp}`}
                    position="bottom center"
                    wide="very"
                    trigger={
                      <NavLink to={`/gp-result/${gp.alias}/${item.season}`}>
                        <small>{gp.nameShort}</small>
                        <br />
                        <Flag name={gp.name} />
                      </NavLink>
                    }
                  />
                </div>
                <div className="gp-key-value-box-value">
                  {gp.modelName ? (
                    <Popup
                      content={`${gp.teamFullName} - ${
                        gp.teamName ? (
                          gp.teamName
                        ) : (
                          <FormattedMessage id={"app.stats.private"} />
                        )
                      } ${gp.modelName}`}
                      position="bottom center"
                      wide="very"
                      trigger={
                        <Image
                          size="mini"
                          src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                          alt={gp.team}
                          className="center-aligned"
                          onError={(e) => {
                            e.target.src =
                              "/build/images/teams/team_no_profile.jpg";
                          }}
                        />
                      }
                    />
                  ) : (
                    <Image
                      size="mini"
                      src={`/build/images/teams/team_${gp.team}_profile_logo.jpg`}
                      alt={gp.team}
                      className="center-aligned"
                      onError={(e) => {
                        e.target.src =
                          "/build/images/teams/team_no_profile.jpg";
                      }}
                    />
                  )}
                </div>
                <div className="gp-key-value-box-value">
                  <small>{gp.qual ? gp.qual : String.fromCharCode(160)}</small>
                  {gp.qualInfo && (
                    <>
                      {String.fromCharCode(160)}
                      <Popup
                        content={`${gp.qualInfo}`}
                        position="bottom center"
                        wide="very"
                        trigger={
                          <Label attached="top right" color="blue" size="small">
                            !
                          </Label>
                        }
                      />
                    </>
                  )}
                </div>
                <div
                  className={
                    gp.grid == "1"
                      ? `gp-key-value-box-value cell-${gp.grid}`
                      : "gp-key-value-box-value"
                  }
                >
                  <small>
                    {gp.grid
                      ? gp.grid
                      : gp.qual
                      ? "-"
                      : String.fromCharCode(160)}
                  </small>
                  {gp.gridInfo && (
                    <>
                      {String.fromCharCode(160)}
                      <Popup
                        content={`${gp.gridInfo}`}
                        position="bottom center"
                        wide="very"
                        trigger={
                          <Label attached="top right" color="blue" size="small">
                            !
                          </Label>
                        }
                      />
                    </>
                  )}
                </div>
                <div
                  className={
                    gp.place == "1" || gp.place == "2" || gp.place == "3"
                      ? `gp-key-value-box-value cell-${gp.place}`
                      : "gp-key-value-box-value"
                  }
                >
                  <small>
                    {gp.completed == 1 ? (
                      gp.place
                    ) : gp.qual ? (
                      gp.notQualified == 1 ? (
                        <FormattedMessage id={"app.stats.nq"} />
                      ) : gp.notStarted == 1 ? (
                        <FormattedMessage id={"app.stats.ns"} />
                      ) : (
                        "-"
                      )
                    ) : (
                      String.fromCharCode(160)
                    )}
                  </small>
                  {gp.info && (
                    <>
                      {String.fromCharCode(160)}
                      <Popup
                        content={`${gp.info} (${formatMessage({
                          id: "app.stats.lap",
                        })} ${gp.laps})`}
                        position="bottom center"
                        wide="very"
                        trigger={
                          <Label attached="top right" color="blue" size="small">
                            !
                          </Label>
                        }
                      />
                    </>
                  )}
                </div>
                <div
                  className={
                    gp.points > 0
                      ? `gp-key-value-box-value-points-scored`
                      : "gp-key-value-box-value-points"
                  }
                >
                  {gp.excluded == null || gp.excluded == "0" ? (
                    <small>
                      {gp.points ? gp.points : String.fromCharCode(160)}{" "}
                    </small>
                  ) : (
                    <>
                      <small className="red">{gp.points}</small>
                      <Popup
                        content={
                          <FormattedMessage id={"app.stats.points.excluded"} />
                        }
                        position="bottom center"
                        wide="very"
                        trigger={
                          <Label attached="top right" color="blue" size="small">
                            !
                          </Label>
                        }
                      />
                    </>
                  )}
                  {gp.sprintPoints > 1 && (
                    <>
                      {String.fromCharCode(160)}
                      <Popup
                        content={
                          formatMessage({
                            id: "app.stats.points.sprint.part1",
                          }) +
                          gp.sprintPoints +
                          formatMessage({
                            id: "app.stats.points.sprint.part2",
                          }) +
                          gp.sprintPos +
                          formatMessage({ id: "app.stats.points.sprint.part3" })
                        }
                        position="bottom center"
                        wide="very"
                        trigger={
                          <Label attached="top right" color="blue" size="small">
                            !
                          </Label>
                        }
                      />
                    </>
                  )}
                </div>
                {gp.bestLap == "1" ? (
                  <div className="gp-key-value-box-value-bl">
                    <small>
                      <FormattedMessage id={"app.table.header.bestlaps"} />
                    </small>
                  </div>
                ) : (
                  <div className="gp-key-value-box-value-empty">
                    {String.fromCharCode(160)}
                  </div>
                )}
              </span>
            ))}
          </Segment>
          {item.starts > 0 ? (
            <>
              <Segment basic>
                <Divider hidden></Divider>
                <SectionPageHeader
                  title={
                    <FormattedMessage
                      id={"app.page.driver.season.details.chart1.header"}
                    />
                  }
                  type="quaternary"
                />
                <Divider hidden></Divider>
                <ReactHighcharts config={configGPResults} />
              </Segment>
              <Segment basic>
                <SectionPageHeader
                  title={
                    <FormattedMessage
                      id={"app.page.driver.season.details.chart2.header"}
                    />
                  }
                  type="quaternary"
                />
                <Divider hidden></Divider>
                <ReactHighcharts config={configPlacesBySeason} />
              </Segment>
            </>
          ) : (
            <Segment basic>
              <Message>
                <Message.Header>
                  <FormattedMessage id={"app.message.header"} />
                </Message.Header>
                <p>
                  <FormattedMessage id={"app.page.driver.charts.grid.info"} />
                </p>
              </Message>
            </Segment>
          )}
        </Segment>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverTeammates() {
    const elements = [];
    const { props } = this;
    const { teammates } = props.driver.data;

    const teammatesSort = teammates.sort((a, b) => b.total - a.total);
    teammatesSort.forEach((item, idx) => {
      const qualClassName =
        parseInt(item.teammatePP) < parseInt(item.driverPP)
          ? "cell-better"
          : parseInt(item.teammatePP) > parseInt(item.driverPP)
          ? "cell-worse"
          : "";

      const raceClassName =
        parseInt(item.teammateWins) < parseInt(item.driverWins)
          ? "cell-better"
          : parseInt(item.teammateWins) > parseInt(item.driverWins)
          ? "cell-worse"
          : "";

      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={`${item.alias}_${idx}`}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={`${item.name} ${item.surname}`}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/driver/${item.alias}`}>
                    <Flag name={item.countryCode} /> {item.name} {item.surname}
                  </NavLink>
                </div>
                <div>
                  <small>{item.seasons}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.teammatePP} : {item.driverPP}
          </Table.Cell>
          <Table.Cell data-title="Wyścigi" className={raceClassName}>
            {item.teammateWins} : {item.driverWins}
          </Table.Cell>
          <Table.Cell data-title="Razem">{item.total}</Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverTeammatesMobile() {
    const elements = [];
    const { props } = this;
    const { teammates } = props.driver.data;

    const teammatesSort = teammates.sort((a, b) => b.total - a.total);
    teammatesSort.forEach((item, idx) => {
      const qualClassName =
        parseInt(item.teammatePP) < parseInt(item.driverPP)
          ? "cell-better"
          : parseInt(item.teammatePP) > parseInt(item.driverPP)
          ? "cell-worse"
          : "";

      const raceClassName =
        parseInt(item.teammateWins) < parseInt(item.driverWins)
          ? "cell-better"
          : parseInt(item.teammateWins) > parseInt(item.driverWins)
          ? "cell-worse"
          : "";

      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/driver/${item.alias}`}>
                <Image
                  size="tiny"
                  src={driverPic}
                  alt={`${item.name} ${item.surname}`}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/drivers/driver_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="mini">
                <Statistic.Value>{item.total}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.table.header.gp"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/driver/${item.alias}`}>
                      <Flag name={item.countryCode} /> {item.name}{" "}
                      {item.surname}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>{item.seasons}</Item.Description>
                  <Item.Description>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.qual"} />
                      </div>
                      <div className={`key-value-box-value ${qualClassName}`}>
                        {item.teammatePP} : {item.driverPP}
                      </div>
                    </span>
                    <span className="key-value-box">
                      <div className="key-value-box-header">
                        <FormattedMessage id={"app.table.header.gp"} />
                      </div>
                      <div className={`key-value-box-value ${raceClassName}`}>
                        {item.teammateWins} : {item.driverWins}
                      </div>
                    </span>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverTeams() {
    const elements = [];
    const { props } = this;
    const { teams, teamsStats } = props.driver.data;
    const { alias } = props.driver.data;

    const items = [...teams];
    switch (this.state.teamsSort.direction) {
      case "descending":
        items.sort(
          (a, b) =>
            b[this.state.teamsSort.column] - a[this.state.teamsSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) =>
            a[this.state.teamsSort.column] - b[this.state.teamsSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName =
        teamsStats.bestTeamByGP[0]?.amount === item.gp
          ? "cell-1"
          : "" + teamsStats.bestTeamByGP[1]?.amount === item.gp
          ? "cell-2"
          : "" + teamsStats.bestTeamByGP[2]?.amount === item.gp
          ? "cell-3"
          : "";

      const startsClassName =
        teamsStats.bestTeamByStarts[0]?.amount === item.starts
          ? "cell-1"
          : "" + teamsStats.bestTeamByStarts[1]?.amount === item.starts
          ? "cell-2"
          : "" + teamsStats.bestTeamByStarts[2]?.amount === item.starts
          ? "cell-3"
          : "";

      const qualClassName =
        teamsStats.bestTeamByQualStarts[0]?.amount === item.qual
          ? "cell-1"
          : "" + teamsStats.bestTeamByQualStarts[1]?.amount === item.qual
          ? "cell-2"
          : "" + teamsStats.bestTeamByQualStarts[2]?.amount === item.qual
          ? "cell-3"
          : "";

      const winsClassName =
        teamsStats.bestTeamByWins[0]?.amount === item.wins
          ? "cell-1"
          : "" + teamsStats.bestTeamByWins[1]?.amount === item.wins
          ? "cell-2"
          : "" + teamsStats.bestTeamByWins[2]?.amount === item.wins
          ? "cell-3"
          : "";

      const secondClassName =
        teamsStats.bestTeamBySecondPlaces[0]?.amount === item.second
          ? "cell-1"
          : "" + teamsStats.bestTeamBySecondPlaces[1]?.amount === item.second
          ? "cell-2"
          : "" + teamsStats.bestTeamBySecondPlaces[2]?.amount === item.second
          ? "cell-3"
          : "";

      const thirdClassName =
        teamsStats.bestTeamByThirdPlaces[0]?.amount === item.third
          ? "cell-1"
          : "" + teamsStats.bestTeamByThirdPlaces[1]?.amount === item.third
          ? "cell-2"
          : "" + teamsStats.bestTeamByThirdPlaces[2]?.amount === item.third
          ? "cell-3"
          : "";

      const podiumClassName =
        teamsStats.bestTeamByPodium[0]?.amount === item.podium
          ? "cell-1"
          : "" + teamsStats.bestTeamByPodium[1]?.amount === item.podium
          ? "cell-2"
          : "" + teamsStats.bestTeamByPodium[2]?.amount === item.podium
          ? "cell-3"
          : "";

      const poleposClassName =
        teamsStats.bestTeamByPolepos[0]?.amount === item.polepos
          ? "cell-1"
          : "" + teamsStats.bestTeamByPolepos[1]?.amount === item.polepos
          ? "cell-2"
          : "" + teamsStats.bestTeamByPolepos[2]?.amount === item.polepos
          ? "cell-3"
          : "";

      const bestlapsClassName =
        teamsStats.bestTeamByBestlaps[0]?.amount === item.bestlaps
          ? "cell-1"
          : "" + teamsStats.bestTeamByBestlaps[1]?.amount === item.bestlaps
          ? "cell-2"
          : "" + teamsStats.bestTeamByBestlaps[2]?.amount === item.bestlaps
          ? "cell-3"
          : "";

      // const pointsPlacesClassName =
      //   teamsStats.bestTeamByPointRaces[0]?.amount === item.pointsPlaces
      //     ? "cell-1"
      //     : "" + teamsStats.bestTeamByPointRaces[1]?.amount ===
      //       item.pointsPlaces
      //     ? "cell-2"
      //     : "" + teamsStats.bestTeamByPointRaces[2]?.amount ===
      //       item.pointsPlaces
      //     ? "cell-3"
      //     : "";

      const completedClassName =
        teamsStats.bestTeamByCompleted[0]?.amount === item.completed
          ? "cell-1"
          : "" + teamsStats.bestTeamByCompleted[1]?.amount === item.completed
          ? "cell-2"
          : "" + teamsStats.bestTeamByCompleted[2]?.amount === item.completed
          ? "cell-3"
          : "";

      const incompleteClassName =
        teamsStats.bestTeamByIncompleted[0]?.amount === item.incomplete
          ? "cell-1"
          : "" + teamsStats.bestTeamByIncompleted[1]?.amount === item.incomplete
          ? "cell-2"
          : "" + teamsStats.bestTeamByIncompleted[2]?.amount === item.incomplete
          ? "cell-3"
          : "";

      const disqClassName =
        teamsStats.bestTeamByDisq[0]?.amount === item.disq
          ? "cell-1"
          : "" + teamsStats.bestTeamByDisq[1]?.amount === item.disq
          ? "cell-2"
          : "" + teamsStats.bestTeamByDisq[2]?.amount === item.disq
          ? "cell-3"
          : "";

      const pointsClassName =
        teamsStats.bestTeamByPoints[0]?.amount === item.points
          ? "cell-1"
          : "" + teamsStats.bestTeamByPoints[1]?.amount === item.points
          ? "cell-2"
          : "" + teamsStats.bestTeamByPoints[2]?.amount === item.points
          ? "cell-3"
          : "";

      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Table.Row key={`${item.alias}_${idx}`}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.team}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/team/${item.alias}`}>
                    <Flag name={item.country} /> {item.name}
                  </NavLink>
                </div>
                <div>
                  <small>
                    {item.seasonMin == item.seasonMax
                      ? item.seasonMax
                      : `${item.seasonMin} - ${item.seasonMax}`}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/gp/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/qual/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/starts/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>

          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/wins/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/second/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/third/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/podium/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/polepos/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/bestlaps/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          {/* <Table.Cell
            data-title="Miejsca punktowane"
            className={pointsPlacesClassName}
          >
            {item.pointsPlaces === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points-places/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.pointsPlaces === null
                  ? String.fromCharCode(160)
                  : item.pointsPlaces}
              </NavLink>
            )}
          </Table.Cell> */}
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/completed/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/incomplete/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/disq/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            {item.points === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points/${alias}/${item.alias}/-/-/-/-/-/-/1`}
              >
                {item.points === null
                  ? String.fromCharCode(160)
                  : Math.round(item.points * 100) / 100}
                {item.points !== item.pointsClass && (
                  <div>
                    <small>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </small>
                  </div>
                )}
              </NavLink>
            )}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverTeamsMobile() {
    const elements = [];
    const { props } = this;
    const { teams, teamsStats } = props.driver.data;
    const { alias } = props.driver.data;

    teams.forEach((item, idx) => {
      const gpClassName =
        teamsStats.bestTeamByGP[0]?.amount === item.gp
          ? "cell-1"
          : "" + teamsStats.bestTeamByGP[1]?.amount === item.gp
          ? "cell-2"
          : "" + teamsStats.bestTeamByGP[2]?.amount === item.gp
          ? "cell-3"
          : "";

      const startsClassName =
        teamsStats.bestTeamByStarts[0]?.amount === item.starts
          ? "cell-1"
          : "" + teamsStats.bestTeamByStarts[1]?.amount === item.starts
          ? "cell-2"
          : "" + teamsStats.bestTeamByStarts[2]?.amount === item.starts
          ? "cell-3"
          : "";

      const qualClassName =
        teamsStats.bestTeamByQualStarts[0]?.amount === item.qual
          ? "cell-1"
          : "" + teamsStats.bestTeamByQualStarts[1]?.amount === item.qual
          ? "cell-2"
          : "" + teamsStats.bestTeamByQualStarts[2]?.amount === item.qual
          ? "cell-3"
          : "";

      const winsClassName =
        teamsStats.bestTeamByWins[0]?.amount === item.wins
          ? "cell-1"
          : "" + teamsStats.bestTeamByWins[1]?.amount === item.wins
          ? "cell-2"
          : "" + teamsStats.bestTeamByWins[2]?.amount === item.wins
          ? "cell-3"
          : "";

      const secondClassName =
        teamsStats.bestTeamBySecondPlaces[0]?.amount === item.second
          ? "cell-1"
          : "" + teamsStats.bestTeamBySecondPlaces[1]?.amount === item.second
          ? "cell-2"
          : "" + teamsStats.bestTeamBySecondPlaces[2]?.amount === item.second
          ? "cell-3"
          : "";

      const thirdClassName =
        teamsStats.bestTeamByThirdPlaces[0]?.amount === item.third
          ? "cell-1"
          : "" + teamsStats.bestTeamByThirdPlaces[1]?.amount === item.third
          ? "cell-2"
          : "" + teamsStats.bestTeamByThirdPlaces[2]?.amount === item.third
          ? "cell-3"
          : "";

      const podiumClassName =
        teamsStats.bestTeamByPodium[0]?.amount === item.podium
          ? "cell-1"
          : "" + teamsStats.bestTeamByPodium[1]?.amount === item.podium
          ? "cell-2"
          : "" + teamsStats.bestTeamByPodium[2]?.amount === item.podium
          ? "cell-3"
          : "";

      const poleposClassName =
        teamsStats.bestTeamByPolepos[0]?.amount === item.polepos
          ? "cell-1"
          : "" + teamsStats.bestTeamByPolepos[1]?.amount === item.polepos
          ? "cell-2"
          : "" + teamsStats.bestTeamByPolepos[2]?.amount === item.polepos
          ? "cell-3"
          : "";

      const bestlapsClassName =
        teamsStats.bestTeamByBestlaps[0]?.amount === item.bestlaps
          ? "cell-1"
          : "" + teamsStats.bestTeamByBestlaps[1]?.amount === item.bestlaps
          ? "cell-2"
          : "" + teamsStats.bestTeamByBestlaps[2]?.amount === item.bestlaps
          ? "cell-3"
          : "";

      // const pointsPlacesClassName =
      //   teamsStats.bestTeamByPointRaces[0]?.amount === item.pointsPlaces
      //     ? "cell-1"
      //     : "" + teamsStats.bestTeamByPointRaces[1]?.amount ===
      //       item.pointsPlaces
      //     ? "cell-2"
      //     : "" + teamsStats.bestTeamByPointRaces[2]?.amount ===
      //       item.pointsPlaces
      //     ? "cell-3"
      //     : "";

      const completedClassName =
        teamsStats.bestTeamByCompleted[0]?.amount === item.completed
          ? "cell-1"
          : "" + teamsStats.bestTeamByCompleted[1]?.amount === item.completed
          ? "cell-2"
          : "" + teamsStats.bestTeamByCompleted[2]?.amount === item.completed
          ? "cell-3"
          : "";

      const incompleteClassName =
        teamsStats.bestTeamByIncompleted[0]?.amount === item.incomplete
          ? "cell-1"
          : "" + teamsStats.bestTeamByIncompleted[1]?.amount === item.incomplete
          ? "cell-2"
          : "" + teamsStats.bestTeamByIncompleted[2]?.amount === item.incomplete
          ? "cell-3"
          : "";

      const disqClassName =
        teamsStats.bestTeamByDisq[0]?.amount === item.disq
          ? "cell-1"
          : "" + teamsStats.bestTeamByDisq[1]?.amount === item.disq
          ? "cell-2"
          : "" + teamsStats.bestTeamByDisq[2]?.amount === item.disq
          ? "cell-3"
          : "";

      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Grid.Row key={`${item.alias}_${idx}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/team/${item.alias}`}>
                <Image
                  size="tiny"
                  src={teamPic}
                  alt={item.team}
                  onError={(e) => {
                    e.target.src = "/build/images/teams/team_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="mini">
                <Statistic.Value>
                  {item.points === 0 ? (
                    0
                  ) : (
                    <NavLink
                      to={`/driver-events/points/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                    >
                      {item.points === null
                        ? String.fromCharCode(160)
                        : Math.round(item.points * 100) / 100}
                      {item.points !== item.pointsClass && (
                        <div>
                          <small className="very">
                            {" ("}
                            {item.pointsClass === null
                              ? String.fromCharCode(160)
                              : Math.round(item.pointsClass * 100) / 100}
                            {")"}
                          </small>
                        </div>
                      )}
                    </NavLink>
                  )}
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}. <Flag name={item.country} />
                    <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.seasonMin == item.seasonMax
                      ? item.seasonMax
                      : `${item.seasonMin} - ${item.seasonMax}`}
                  </Item.Description>
                  <Item.Description className="overflow">
                    <table className="basic-table">
                      <thead>
                        <tr>
                          <th>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.race"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </th>
                          <th>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </th>
                          {/* <th>
                            <FormattedMessage
                              id={"app.table.header.points.races"}
                            />
                          </th> */}
                          <th>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td
                            data-title="Grand Prix"
                            className={`${gpClassName}`}
                          >
                            {item.gp === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/gp/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.gp === null
                                  ? String.fromCharCode(160)
                                  : item.gp}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Kwalifikacje"
                            className={`${qualClassName}`}
                          >
                            {item.qual === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/qual/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.qual === null
                                  ? String.fromCharCode(160)
                                  : item.qual}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyścig"
                            className={`${startsClassName}`}
                          >
                            {item.starts === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/starts/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.starts === null
                                  ? String.fromCharCode(160)
                                  : item.starts}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P1" className={`${winsClassName}`}>
                            {item.wins === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/wins/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.wins === null
                                  ? String.fromCharCode(160)
                                  : item.wins}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P2" className={`${secondClassName}`}>
                            {item.second === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/second/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.second === null
                                  ? String.fromCharCode(160)
                                  : item.second}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P3" className={`${thirdClassName}`}>
                            {item.third === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/third/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.third === null
                                  ? String.fromCharCode(160)
                                  : item.third}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Podium"
                            className={`${podiumClassName}`}
                          >
                            {item.podium === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/podium/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.podium === null
                                  ? String.fromCharCode(160)
                                  : item.podium}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Pole Position"
                            className={`${poleposClassName}`}
                          >
                            {item.polepos === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/polepos/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.polepos === null
                                  ? String.fromCharCode(160)
                                  : item.polepos}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Naj. okrążenia"
                            className={`${bestlapsClassName}`}
                          >
                            {item.bestlaps === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/bestlaps/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.bestlaps === null
                                  ? String.fromCharCode(160)
                                  : item.bestlaps}
                              </NavLink>
                            )}
                          </td>
                          {/* <td
                            data-title="Miejsca punktowane"
                            className={`${pointsPlacesClassName}`}
                          >
                            {item.pointsPlaces === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/points/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.pointsPlaces === null
                                  ? String.fromCharCode(160)
                                  : item.pointsPlaces}
                              </NavLink>
                            )}
                          </td> */}
                          <td
                            data-title="Wyś. ukończone"
                            className={`${completedClassName}`}
                          >
                            {item.completed === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/completed/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.completed === null
                                  ? String.fromCharCode(160)
                                  : item.completed}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyś. nieukończone"
                            className={`${incompleteClassName}`}
                          >
                            {item.incomplete === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/incomplete/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.incomplete === null
                                  ? String.fromCharCode(160)
                                  : item.incomplete}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Dyskwalifikacje"
                            className={`${disqClassName}`}
                          >
                            {item.disq === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/disq/${alias}/${item.alias}/-/-/-/-/-/-/1`}
                              >
                                {item.disq === null
                                  ? String.fromCharCode(160)
                                  : item.disq}
                              </NavLink>
                            )}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverGrandPrix() {
    const elements = [];
    const { props } = this;
    const { gp, gpStats } = props.driver.data;
    const { alias } = props.driver.data;

    const items = [...gp];
    switch (this.state.gpSort.direction) {
      case "descending":
        items.sort(
          (a, b) => b[this.state.gpSort.column] - a[this.state.gpSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) => a[this.state.gpSort.column] - b[this.state.gpSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName = gpStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        gpStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName = gpStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName = gpStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        gpStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        gpStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        gpStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        gpStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        gpStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      // const pointsPlacesClassName =
      //   gpStats.pointsPlaces?.amount === item.pointsPlaces ? "cell-1" : "";

      const completedClassName =
        gpStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        gpStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName = gpStats.disq?.amount === item.disq ? "cell-1" : "";

      const pointsClassName =
        gpStats.points?.amount === item.points ? "cell-1" : "";

      const countryPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Table.Row key={item.alias}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/gp-stats/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={countryPic}
                    alt={item.circuitAlias}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div className="inline-block vertical-middle-aligned">
                <div>
                  <NavLink to={`/gp-stats/${item.alias}`}>{item.name} </NavLink>
                </div>
                <div>
                  <small>
                    {item.circuits.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/gp/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/qual/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/starts/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/wins/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/second/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/third/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/podium/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/polepos/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/bestlaps/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          {/* <Table.Cell
            data-title="Miejsca punktowane"
            className={pointsPlacesClassName}
          >
            {item.pointsPlaces === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points-places/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.pointsPlaces === null
                  ? String.fromCharCode(160)
                  : item.pointsPlaces}
              </NavLink>
            )}
          </Table.Cell> */}
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/completed/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/incomplete/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/disq/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            {item.points === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
              >
                {item.points === null
                  ? String.fromCharCode(160)
                  : Math.round(item.points * 100) / 100}
                {item.points !== item.pointsClass && (
                  <div>
                    <small>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </small>
                  </div>
                )}
              </NavLink>
            )}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverGrandPrixMobile() {
    const elements = [];
    const { props } = this;
    const { gp, gpStats } = props.driver.data;
    const { alias } = props.driver.data;

    const gpSort = gp.sort((a, b) => b.starts - a.starts);
    gpSort.forEach((item, idx) => {
      const gpClassName = gpStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        gpStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName = gpStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName = gpStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        gpStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        gpStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        gpStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        gpStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        gpStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      // const pointsPlacesClassName =
      //   gpStats.pointsPlaces?.amount === item.pointsPlaces ? "cell-1" : "";

      const completedClassName =
        gpStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        gpStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName = gpStats.disq?.amount === item.disq ? "cell-1" : "";

      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/gp-stats/${item.alias}`}>
                <Image
                  size="tiny"
                  src={gpPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="mini">
                <Statistic.Value>
                  {item.points === 0 ? (
                    0
                  ) : (
                    <NavLink
                      to={`/driver-events/points/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                    >
                      {item.points === null
                        ? String.fromCharCode(160)
                        : Math.round(item.points * 100) / 100}
                      {item.points !== item.pointsClass && (
                        <div>
                          <small className="very">
                            {" ("}
                            {item.pointsClass === null
                              ? String.fromCharCode(160)
                              : Math.round(item.pointsClass * 100) / 100}
                            {")"}
                          </small>
                        </div>
                      )}
                    </NavLink>
                  )}
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/gp-stats/${item.alias}`}>
                      {item.name}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.circuits.split(", ").map((e) => (
                      <div key={e}>{e}</div>
                    ))}
                  </Item.Description>
                  <Item.Description className="overflow">
                    <table className="basic-table">
                      <thead>
                        <tr>
                          <th>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.race"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </th>
                          <th>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </th>
                          {/* <th>
                            <FormattedMessage
                              id={"app.table.header.points.races"}
                            />
                          </th> */}
                          <th>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td
                            data-title="Grand Prix"
                            className={`${gpClassName}`}
                          >
                            {item.gp === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/gp/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.gp === null
                                  ? String.fromCharCode(160)
                                  : item.gp}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Kwalifikacje"
                            className={`${qualClassName}`}
                          >
                            {item.qual === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/qual/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.qual === null
                                  ? String.fromCharCode(160)
                                  : item.qual}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyścig"
                            className={`${startsClassName}`}
                          >
                            {item.starts === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/starts/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.starts === null
                                  ? String.fromCharCode(160)
                                  : item.starts}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P1" className={`${winsClassName}`}>
                            {item.wins === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/wins/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.wins === null
                                  ? String.fromCharCode(160)
                                  : item.wins}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P2" className={`${secondClassName}`}>
                            {item.second === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/second/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.second === null
                                  ? String.fromCharCode(160)
                                  : item.second}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P3" className={`${thirdClassName}`}>
                            {item.third === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/third/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.third === null
                                  ? String.fromCharCode(160)
                                  : item.third}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Podium"
                            className={`${podiumClassName}`}
                          >
                            {item.podium === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/podium/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.podium === null
                                  ? String.fromCharCode(160)
                                  : item.podium}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Pole Position"
                            className={`${poleposClassName}`}
                          >
                            {item.polepos === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/polepos/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.polepos === null
                                  ? String.fromCharCode(160)
                                  : item.polepos}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Naj. okrążenia"
                            className={`${bestlapsClassName}`}
                          >
                            {item.bestlaps === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/bestlaps/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.bestlaps === null
                                  ? String.fromCharCode(160)
                                  : item.bestlaps}
                              </NavLink>
                            )}
                          </td>
                          {/* <td
                            data-title="Miejsca punktowane"
                            className={`${pointsPlacesClassName}`}
                          >
                            {item.pointsPlaces === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/points/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.pointsPlaces === null
                                  ? String.fromCharCode(160)
                                  : item.pointsPlaces}
                              </NavLink>
                            )}
                          </td> */}
                          <td
                            data-title="Wyś. ukończone"
                            className={`${completedClassName}`}
                          >
                            {item.completed === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/completed/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.completed === null
                                  ? String.fromCharCode(160)
                                  : item.completed}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyś. nieukończone"
                            className={`${incompleteClassName}`}
                          >
                            {item.incomplete === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/incomplete/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.incomplete === null
                                  ? String.fromCharCode(160)
                                  : item.incomplete}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Dyskwalifikacje"
                            className={`${disqClassName}`}
                          >
                            {item.disq === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/disq/${alias}/-/${item.alias}/-/-/-/-/-/1/`}
                              >
                                {item.disq === null
                                  ? String.fromCharCode(160)
                                  : item.disq}
                              </NavLink>
                            )}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverCircuits() {
    const elements = [];
    const { props } = this;
    const { circuits, circuitsStats } = props.driver.data;
    const { alias } = props.driver.data;

    const items = [...circuits];
    switch (this.state.circuitSort.direction) {
      case "descending":
        items.sort(
          (a, b) =>
            b[this.state.circuitSort.column] - a[this.state.circuitSort.column]
        );
        break;
      case "ascending":
        items.sort(
          (a, b) =>
            a[this.state.circuitSort.column] - b[this.state.circuitSort.column]
        );
        break;
      default:
        break;
    }

    items.forEach((item, idx) => {
      const gpClassName = circuitsStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        circuitsStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName =
        circuitsStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName =
        circuitsStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        circuitsStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        circuitsStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        circuitsStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        circuitsStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        circuitsStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      // const pointsPlacesClassName =
      //   circuitsStats.pointsPlaces?.amount === item.pointsPlaces
      //     ? "cell-1"
      //     : "";

      const completedClassName =
        circuitsStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        circuitsStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName =
        circuitsStats.disq?.amount === item.disq ? "cell-1" : "";

      const pointsClassName =
        circuitsStats.points?.amount === item.points ? "cell-1" : "";

      const gpArray = item.grandPrix.split(", ");
      const circuitPic = `/build/images/circuits/circuit_${item.alias.toLowerCase()}_profile.jpg`;
      const element = (
        <Table.Row key={item.alias}>
          <Table.Cell data-title="Lp" className="no-wrap">
            {idx + 1}.
          </Table.Cell>
          <Table.Cell data-title="Tor" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/circuit/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={circuitPic}
                    alt={item.circuitAlias}
                    className="cell-photo"
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/circuit/${item.alias}`}>{item.name} </NavLink>
                </div>
                <div>
                  <small>
                    {item.grandPrix.split(", ").map((e, idx) => (
                      <div key={e}>{gpArray[idx]}</div>
                    ))}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className={gpClassName}>
            {item.gp === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/gp/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.gp === null ? String.fromCharCode(160) : item.gp}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Kwalifikacje" className={qualClassName}>
            {item.qual === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/qual/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.qual === null ? String.fromCharCode(160) : item.qual}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wyścig" className={startsClassName}>
            {item.starts === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/starts/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.starts === null ? String.fromCharCode(160) : item.starts}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Wygrane" className={winsClassName}>
            {item.wins === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/wins/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.wins === null ? String.fromCharCode(160) : item.wins}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca" className={secondClassName}>
            {item.second === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/second/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.second === null ? String.fromCharCode(160) : item.second}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca" className={thirdClassName}>
            {item.third === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/third/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.third === null ? String.fromCharCode(160) : item.third}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Podium" className={podiumClassName}>
            {item.podium === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/podium/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.podium === null ? String.fromCharCode(160) : item.podium}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Pole Position" className={poleposClassName}>
            {item.polepos === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/polepos/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.polepos === null
                  ? String.fromCharCode(160)
                  : item.polepos}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Naj. okrążenia" className={bestlapsClassName}>
            {item.bestlaps === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/bestlaps/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.bestlaps === null
                  ? String.fromCharCode(160)
                  : item.bestlaps}
              </NavLink>
            )}
          </Table.Cell>
          {/* <Table.Cell
            data-title="Miejsca punktowane"
            className={pointsPlacesClassName}
          >
            {item.pointsPlaces === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points-places/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.pointsPlaces === null
                  ? String.fromCharCode(160)
                  : item.pointsPlaces}
              </NavLink>
            )}
          </Table.Cell> */}
          <Table.Cell
            data-title="Ukończone wyścigi"
            className={completedClassName}
          >
            {item.completed === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/completed/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.completed === null
                  ? String.fromCharCode(160)
                  : item.completed}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell
            data-title="Nieukończone wyścigi"
            className={incompleteClassName}
          >
            {item.incomplete === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/incomplete/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.incomplete === null
                  ? String.fromCharCode(160)
                  : item.incomplete}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Dyskwalifikacje" className={disqClassName}>
            {item.disq === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/disq/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.disq === null ? String.fromCharCode(160) : item.disq}
              </NavLink>
            )}
          </Table.Cell>
          <Table.Cell data-title="Punkty" className={pointsClassName}>
            {item.points === 0 ? (
              0
            ) : (
              <NavLink
                to={`/driver-events/points/${alias}/-/-/${item.alias}/-/-/-/-/1`}
              >
                {item.points === null
                  ? String.fromCharCode(160)
                  : Math.round(item.points * 100) / 100}
                {item.points !== item.pointsClass && (
                  <div>
                    <small>
                      {" ("}
                      {item.pointsClass === null
                        ? String.fromCharCode(160)
                        : Math.round(item.pointsClass * 100) / 100}
                      {")"}
                    </small>
                  </div>
                )}
              </NavLink>
            )}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriverCircuitsMobile() {
    const elements = [];
    const { props } = this;
    const { circuits, circuitsStats } = props.driver.data;
    const { alias } = props.driver.data;

    const circuitsSort = circuits.sort((a, b) => b.starts - a.starts);
    circuitsSort.forEach((item, idx) => {
      const gpClassName = circuitsStats.gp?.amount === item.gp ? "cell-1" : "";

      const startsClassName =
        circuitsStats.starts?.amount === item.starts ? "cell-1" : "";

      const qualClassName =
        circuitsStats.qual?.amount === item.qual ? "cell-1" : "";

      const winsClassName =
        circuitsStats.wins?.amount === item.wins ? "cell-1" : "";

      const secondClassName =
        circuitsStats.second?.amount === item.second ? "cell-1" : "";

      const thirdClassName =
        circuitsStats.third?.amount === item.third ? "cell-1" : "";

      const podiumClassName =
        circuitsStats.podium?.amount === item.podium ? "cell-1" : "";

      const poleposClassName =
        circuitsStats.polepos?.amount === item.polepos ? "cell-1" : "";

      const bestlapsClassName =
        circuitsStats.bestlaps?.amount === item.bestlaps ? "cell-1" : "";

      // const pointsPlacesClassName =
      //   circuitsStats.pointsPlaces?.amount === item.pointsPlaces
      //     ? "cell-1"
      //     : "";

      const completedClassName =
        circuitsStats.completed?.amount === item.completed ? "cell-1" : "";

      const incompleteClassName =
        circuitsStats.incomplete?.amount === item.incomplete ? "cell-1" : "";

      const disqClassName =
        circuitsStats.disq?.amount === item.disq ? "cell-1" : "";

      const gpArray = item.grandPrix.split(", ");
      const circuitPic = `/build/images/circuits/circuit_${item.alias.toLowerCase()}_profile.jpg`;
      const element = (
        <Grid.Row key={item.alias}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/circuit/${item.alias}`}>
                <Image
                  size="tiny"
                  src={circuitPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Statistic size="mini">
                <Statistic.Value>
                  {item.points === 0 ? (
                    0
                  ) : (
                    <NavLink
                      to={`/driver-events/points/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                    >
                      {item.points === null
                        ? String.fromCharCode(160)
                        : Math.round(item.points * 100) / 100}
                      {item.points !== item.pointsClass && (
                        <div>
                          <small className="very">
                            {" ("}
                            {item.pointsClass === null
                              ? String.fromCharCode(160)
                              : Math.round(item.pointsClass * 100) / 100}
                            {")"}
                          </small>
                        </div>
                      )}
                    </NavLink>
                  )}
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Segment>
          </Grid.Column>
          <Grid.Column width={13}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {idx + 1}.{" "}
                    <NavLink to={`/circuit/${item.alias}`}>{item.name}</NavLink>
                  </Item.Header>
                  <Item.Description>
                    {item.countries.split(", ").map((e, idx) => (
                      <div key={e}>{gpArray[idx]}</div>
                    ))}
                  </Item.Description>
                  <Item.Description className="overflow">
                    <table className="basic-table">
                      <thead>
                        <tr>
                          <th>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.race"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </th>
                          <th>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </th>
                          {/* <th>
                            <FormattedMessage
                              id={"app.table.header.points.races"}
                            />
                          </th> */}
                          <th>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </th>
                          <th>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td
                            data-title="Grand Prix"
                            className={`${gpClassName}`}
                          >
                            {item.gp === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/gp/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.gp === null
                                  ? String.fromCharCode(160)
                                  : item.gp}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Kwalifikacje"
                            className={`${qualClassName}`}
                          >
                            {item.qual === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/qual/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.qual === null
                                  ? String.fromCharCode(160)
                                  : item.qual}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyścig"
                            className={`${startsClassName}`}
                          >
                            {item.starts === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/starts/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.starts === null
                                  ? String.fromCharCode(160)
                                  : item.starts}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P1" className={`${winsClassName}`}>
                            {item.wins === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/wins/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.wins === null
                                  ? String.fromCharCode(160)
                                  : item.wins}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P2" className={`${secondClassName}`}>
                            {item.second === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/second/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.second === null
                                  ? String.fromCharCode(160)
                                  : item.second}
                              </NavLink>
                            )}
                          </td>
                          <td data-title="P3" className={`${thirdClassName}`}>
                            {item.third === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/third/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.third === null
                                  ? String.fromCharCode(160)
                                  : item.third}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Podium"
                            className={`${podiumClassName}`}
                          >
                            {item.podium === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/podium/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.podium === null
                                  ? String.fromCharCode(160)
                                  : item.podium}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Pole Position"
                            className={`${poleposClassName}`}
                          >
                            {item.polepos === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/polepos/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.polepos === null
                                  ? String.fromCharCode(160)
                                  : item.polepos}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Naj. okrążenia"
                            className={`${bestlapsClassName}`}
                          >
                            {item.bestlaps === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/bestlaps/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.bestlaps === null
                                  ? String.fromCharCode(160)
                                  : item.bestlaps}
                              </NavLink>
                            )}
                          </td>
                          {/* <td
                            data-title="Miejsca punktowane"
                            className={`${pointsPlacesClassName}`}
                          >
                            {item.pointsPlaces === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/points/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.pointsPlaces === null
                                  ? String.fromCharCode(160)
                                  : item.pointsPlaces}
                              </NavLink>
                            )}
                          </td> */}
                          <td
                            data-title="Wyś. ukończone"
                            className={`${completedClassName}`}
                          >
                            {item.completed === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/completed/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.completed === null
                                  ? String.fromCharCode(160)
                                  : item.completed}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Wyś. nieukończone"
                            className={`${incompleteClassName}`}
                          >
                            {item.incomplete === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/incomplete/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.incomplete === null
                                  ? String.fromCharCode(160)
                                  : item.incomplete}
                              </NavLink>
                            )}
                          </td>
                          <td
                            data-title="Dyskwalifikacje"
                            className={`${disqClassName}`}
                          >
                            {item.disq === 0 ? (
                              0
                            ) : (
                              <NavLink
                                to={`/driver-events/disq/${alias}/-/-/${item.alias}/-/-/-/-/1`}
                              >
                                {item.disq === null
                                  ? String.fromCharCode(160)
                                  : item.disq}
                              </NavLink>
                            )}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderStats() {
    const { props } = this;
    const { alias, ranking, stats } = props.driver.data;
    const {
      seasons,
      gp,
      wins,
      points,
      podium,
      polepos,
      bestlaps,
      starts,
      qual,
      sprints,
      completed,
      incomplete,
      finished,
      retirement,
      pointPlaces,
      noPointPlaces,
      notQualified,
      notStarted,
      penalty,
      disq,
    } = stats;

    const element = (
      <>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {seasons === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/gp/${alias}`}>{seasons}</NavLink>
                  <NavLink to="/drivers-records/seasons/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.seasons}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.seasons"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {gp === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/gp/${alias}`}>{gp}</NavLink>
                  <NavLink to="/drivers-records/gp/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.gp}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.gp"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {qual === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/qual/${alias}`}>{qual}</NavLink>
                  <NavLink to="/drivers-records/qual/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.qual}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.qual"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {starts === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/starts/${alias}`}>
                    {starts}
                  </NavLink>
                  <NavLink to="/drivers-records/starts/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.starts}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {sprints === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/sprints/${alias}`}>
                    {sprints}
                  </NavLink>
                  <NavLink to="/drivers-records/sprints/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.sprints}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.sprints"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {wins === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/wins/${alias}`}>{wins}</NavLink>
                  <NavLink to="/drivers-records/wins/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.wins}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {points === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/points/${alias}`}>
                    {+(Math.round(points * 100) / 100)}
                  </NavLink>
                  <NavLink to="/drivers-records/points/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.points}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {podium === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/podium/${alias}`}>
                    {podium}
                  </NavLink>
                  <NavLink to="/drivers-records/podium/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.podium}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {polepos === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/polepos/${alias}`}>
                    {polepos}
                  </NavLink>
                  <NavLink to="/drivers-records/polepos/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.polepos}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {bestlaps === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/bestlaps/${alias}`}>
                    {bestlaps}
                  </NavLink>
                  <NavLink to="/drivers-records/bestlaps/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.bestlaps}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {pointPlaces === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/points-places/${alias}`}>
                    {pointPlaces}
                  </NavLink>
                  <NavLink to="/drivers-records/points-places/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.pointPlaces}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {noPointPlaces === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/no-points-places/${alias}`}>
                    {noPointPlaces}
                  </NavLink>
                  <NavLink to="/drivers-records/no-points-places/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.noPointPlaces}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.no.points.races"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {notQualified === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/not-qualified/${alias}`}>
                    {notQualified}
                  </NavLink>
                  <NavLink to="/drivers-records/not-qualified/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.notQualified}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.not.qualified"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {notStarted === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/not-started/${alias}`}>
                    {notStarted}
                  </NavLink>
                  <NavLink to="/drivers-records/not-started/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.notStarted}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.not.started"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {penalty === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/penalty/${alias}`}>
                    {penalty}
                  </NavLink>
                  <NavLink to="/drivers-records/penalty/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.penalty}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.penalty"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>

        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {completed === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/completed/${alias}`}>
                    {completed}
                  </NavLink>
                  <NavLink to="/drivers-records/completed/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.completed}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.completed"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {incomplete === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/incomplete/${alias}`}>
                    {incomplete}
                  </NavLink>
                  <NavLink to="/drivers-records/incomplete/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.incomplete}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.incomplete"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {finished === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/finished/${alias}`}>
                    {finished}
                  </NavLink>
                  <NavLink to="/drivers-records/finished/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.finished}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.finished"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {retirement === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/retirement/${alias}`}>
                    {retirement}
                  </NavLink>
                  <NavLink to="/drivers-records/retirement/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.retirement}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.retirement"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Value>
              {disq === "0" ? (
                <div>
                  0<small className="very"> </small>
                </div>
              ) : (
                <div>
                  <NavLink to={`/driver-events/disq/${alias}`}>{disq}</NavLink>
                  <NavLink to="/drivers-records/disq/-/-/-/-/-/0/1/">
                    <small className="very">
                      {" ("}
                      {ranking.disq}
                      {".)"}
                    </small>
                  </NavLink>
                </div>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.disq"} />
            </Statistic.Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderTeamStats() {
    const { props } = this;
    const { alias, teamsStats } = props.driver.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {teamsStats.bestTeamByWins[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/wins/${alias}/${teamsStats.bestTeamByWins[0].alias}/-/-/-/-/-/-/1`}
                >
                  {teamsStats.bestTeamByWins[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {teamsStats.bestTeamByWins[0] == null ? (
                "-"
              ) : (
                <NavLink to={`/team/${teamsStats.bestTeamByWins[0].alias}`}>
                  {teamsStats.bestTeamByWins[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {teamsStats.bestTeamByPoints[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/points/${alias}/${teamsStats.bestTeamByPoints[0].alias}/-/-/-/-/-/-/1`}
                >
                  {Math.round(teamsStats.bestTeamByPoints[0].amount * 100) /
                    100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {teamsStats.bestTeamByPoints[0] == null ? (
                "-"
              ) : (
                <NavLink to={`/team/${teamsStats.bestTeamByPoints[0].alias}`}>
                  {teamsStats.bestTeamByPoints[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {teamsStats.bestTeamByPodium[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/podium/${alias}/${teamsStats.bestTeamByPodium[0].alias}/-/-/-/-/-/-/1`}
                >
                  {teamsStats.bestTeamByPodium[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {teamsStats.bestTeamByPodium[0] == null ? (
                "-"
              ) : (
                <NavLink to={`/team/${teamsStats.bestTeamByPodium[0].alias}`}>
                  {teamsStats.bestTeamByPodium[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {teamsStats.bestTeamByPolepos[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/polepos/${alias}/${teamsStats.bestTeamByPolepos[0].alias}/-/-/-/-/-/-/1`}
                >
                  {teamsStats.bestTeamByPolepos[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {teamsStats.bestTeamByPolepos[0] == null ? (
                "-"
              ) : (
                <NavLink to={`/team/${teamsStats.bestTeamByPolepos[0].alias}`}>
                  {teamsStats.bestTeamByPolepos[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {teamsStats.bestTeamByBestlaps[0] == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/bestlaps/${alias}/${teamsStats.bestTeamByBestlaps[0].alias}/-/-/-/-/-/-/1`}
                >
                  {teamsStats.bestTeamByBestlaps[0].amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {teamsStats.bestTeamByBestlaps[0] == null ? (
                "-"
              ) : (
                <NavLink to={`/team/${teamsStats.bestTeamByBestlaps[0].alias}`}>
                  {teamsStats.bestTeamByBestlaps[0].name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderGPStats() {
    const { props } = this;
    const { alias, gpStats } = props.driver.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {gpStats.wins == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/wins/${alias}/-/${gpStats.wins.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.wins.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {gpStats.wins == null ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.wins.alias}`}>
                  {gpStats.wins.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {gpStats.points == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/points/${alias}/-/-/${gpStats.points.alias}/-/-/-/-/-/1`}
                >
                  {Math.round(gpStats.points.amount * 100) / 100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {gpStats.points == null ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.points.alias}`}>
                  {gpStats.points.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {gpStats.podium == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/podium/${alias}/-/-/${gpStats.podium.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.podium.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {gpStats.podium == null ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.podium.alias}`}>
                  {gpStats.podium.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {gpStats.polepos == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/polepos/${alias}/-/-/${gpStats.polepos.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.polepos.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {gpStats.polepos == null ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.polepos.alias}`}>
                  {gpStats.polepos.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {gpStats.bestlaps == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/bestlaps/${alias}/-/-/${gpStats.bestlaps.alias}/-/-/-/-/-/1`}
                >
                  {gpStats.bestlaps.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {gpStats.bestlaps == null ? (
                "-"
              ) : (
                <NavLink to={`/gp-stats/${gpStats.bestlaps.alias}`}>
                  {gpStats.bestlaps.name}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderCircuitsStats() {
    const { props } = this;
    const { alias, circuitsStats } = props.driver.data;

    const element = (
      <>
        <Grid.Column>
          <Statistic id="wins">
            <Statistic.Value>
              {circuitsStats.wins == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/wins/${alias}/-/-/${circuitsStats.wins.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.wins.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.wins"} />
            </Statistic.Label>
            <Label htmlFor="wins" size="small">
              {circuitsStats.wins == null ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.wins.alias}`}>
                  {circuitsStats.wins.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="points">
            <Statistic.Value>
              {circuitsStats.points == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/points/${alias}/-/-/${circuitsStats.points.alias}/-/-/-/-/1`}
                >
                  {Math.round(circuitsStats.points.amount * 100) / 100}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.points"} />
            </Statistic.Label>
            <Label htmlFor="points" size="small">
              {circuitsStats.points == null ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.points.alias}`}>
                  {circuitsStats.points.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="podium">
            <Statistic.Value>
              {circuitsStats.podium == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/podium/${alias}/-/-/${circuitsStats.podium.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.podium.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.podium"} />
            </Statistic.Label>
            <Label htmlFor="podium" size="small">
              {circuitsStats.podium == null ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.podium.alias}`}>
                  {circuitsStats.podium.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="polepos">
            <Statistic.Value>
              {circuitsStats.polepos == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/polepos/${alias}/-/-/${circuitsStats.polepos.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.polepos.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.polepos"} />
            </Statistic.Label>
            <Label htmlFor="polepos" size="small">
              {circuitsStats.polepos == null ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.polepos.alias}`}>
                  {circuitsStats.polepos.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic id="bestlaps">
            <Statistic.Value>
              {circuitsStats.bestlaps == null ? (
                0
              ) : (
                <NavLink
                  to={`/driver-events/bestlaps/${alias}/-/-/${circuitsStats.bestlaps.alias}/-/-/-/-/1`}
                >
                  {circuitsStats.bestlaps.amount}
                </NavLink>
              )}
            </Statistic.Value>
            <Statistic.Label>
              <FormattedMessage id={"app.stats.bestlaps"} />
            </Statistic.Label>
            <Label htmlFor="bestlaps" size="small">
              {circuitsStats.bestlaps == null ? (
                "-"
              ) : (
                <NavLink to={`/circuit/${circuitsStats.bestlaps.alias}`}>
                  {circuitsStats.bestlaps.circuit}
                </NavLink>
              )}
            </Label>
          </Statistic>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderTeammatesTab() {
    const { props } = this;
    const { alias, stats, teammates } = props.driver.data;
    const { starts } = stats;

    const element = (
      <TabPane>
        {starts > 0 && (
          <div className="driver-teammates">
            <div className="hideForDesktop">
              <Segment basic>
                <Grid columns={3} divided="vertically">
                  {this.renderDriverTeammatesMobile()}
                </Grid>
              </Segment>
            </div>
            <div className="hideForMobile">
              <Segment basic>
                <Table basic="very" celled>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell className="cell-default-size">
                        #
                      </Table.HeaderCell>
                      <Table.HeaderCell className="left">
                        <Popup
                          trigger={
                            <div>
                              <FormattedMessage
                                id={"app.table.header.teammate"}
                              />
                            </div>
                          }
                          flowing
                          hoverable
                        >
                          <FormattedMessage id={"app.table.header.teammate"} />
                        </Popup>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <Popup
                          trigger={
                            <div>
                              <FormattedMessage id={"app.table.header.qual"} />
                            </div>
                          }
                          flowing
                          hoverable
                        >
                          <FormattedMessage id={"app.table.header.qual.long"} />
                        </Popup>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <Popup
                          trigger={
                            <div>
                              <FormattedMessage id={"app.table.header.race"} />
                            </div>
                          }
                          flowing
                          hoverable
                        >
                          <FormattedMessage id={"app.table.header.race.long"} />
                        </Popup>
                      </Table.HeaderCell>
                      <Table.HeaderCell className="cell-default-size">
                        <Popup
                          trigger={
                            <div>
                              <FormattedMessage id={"app.table.header.total"} />
                            </div>
                          }
                          flowing
                          hoverable
                        >
                          <FormattedMessage id={"app.table.header.total"} />
                        </Popup>
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>{this.renderDriverTeammates()}</Table.Body>
                </Table>
                <Divider hidden></Divider>
              </Segment>
            </div>
            <div className="buttons">
              <NavLink className="primary" to={`/driver-teammates/${alias}`}>
                <FormattedMessage id={"app.button.details"} />
              </NavLink>
            </div>
          </div>
        )}
      </TabPane>
    );
    return element;
  }

  renderTeamsTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="driver-teams" id="driver-teams">
          <div className="hideForMobile">
            <Segment basic>
              <Grid>
                <Grid.Row textAlign="center" columns={5} only="tablet computer">
                  {this.renderTeamStats()}
                </Grid.Row>
              </Grid>
            </Segment>
          </div>
          <div className="hideForDesktop">
            <Segment basic>
              <Grid columns={3} divided="vertically">
                {this.renderDriverTeamsMobile()}
              </Grid>
            </Segment>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.team"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.team"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "gp"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "qual"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "starts"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "wins"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "second"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "third"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "podium"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "polepos"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "bestlaps"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "completed"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "incomplete"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "disq"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.teamsSort.column === "points"
                          ? state.teamsSort.direction
                          : null
                      }
                      onClick={() => this.onTeamsSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderDriverTeams()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  renderGPTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="driver-grandprix">
          <div className="hideForMobile">
            <Segment basic>
              <Grid>
                <Grid.Row textAlign="center" columns={5} only="tablet computer">
                  {this.renderGPStats()}
                </Grid.Row>
              </Grid>
            </Segment>
          </div>
          <div className="hideForDesktop">
            <Segment basic>
              <Grid columns={3} divided="vertically">
                {this.renderDriverGrandPrixMobile()}
              </Grid>
            </Segment>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp.long"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "gp"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "qual"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "starts"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "wins"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "second"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "third"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "podium"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "polepos"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "bestlaps"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "completed"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "incomplete"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "disq"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.gpSort.column === "points"
                          ? state.gpSort.direction
                          : null
                      }
                      onClick={() => this.onGPSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderDriverGrandPrix()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  renderCircuitsTab() {
    const { state } = this;

    const element = (
      <TabPane>
        <div className="driver-circuits">
          <div className="hideForMobile">
            <Segment basic>
              <Grid>
                <Grid.Row textAlign="center" columns={5} only="tablet computer">
                  {this.renderCircuitsStats()}
                </Grid.Row>
              </Grid>
            </Segment>
          </div>
          <div className="hideForDesktop">
            <Segment basic>
              <Grid columns={3} divided="vertically">
                {this.renderDriverCircuitsMobile()}
              </Grid>
            </Segment>
          </div>
          <div className="hideForMobile">
            <Segment basic className="overflow">
              <Table basic="very" celled sortable>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>#</Table.HeaderCell>
                    <Table.HeaderCell className="left">
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.circuit"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.circuit"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "gp"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("gp")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.gp.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "qual"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("qual")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.qual.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "starts"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("starts")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.race.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "wins"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("wins")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p1"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p1.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "second"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("second")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p2"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p2.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "third"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("third")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.p3"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.p3.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "podium"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("podium")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.podiums"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.podiums.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "polepos"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("polepos")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.polepos.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "bestlaps"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("bestlaps")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage
                              id={"app.table.header.bestlaps"}
                            />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage
                          id={"app.table.header.bestlaps.long"}
                        />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "completed"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("completed")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.compl"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.compl.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "incomplete"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("incomplete")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.inc"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.inc.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "disq"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("disq")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.disq"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.disq.long"} />
                      </Popup>
                    </Table.HeaderCell>
                    <Table.HeaderCell
                      sorted={
                        state.circuitSort.column === "points"
                          ? state.circuitSort.direction
                          : null
                      }
                      onClick={() => this.onCircuitSort("points")}
                    >
                      <Popup
                        trigger={
                          <div>
                            <FormattedMessage id={"app.table.header.points"} />
                          </div>
                        }
                        flowing
                        hoverable
                      >
                        <FormattedMessage id={"app.table.header.points.long"} />
                      </Popup>
                    </Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>{this.renderDriverCircuits()}</Table.Body>
              </Table>
            </Segment>
          </div>
        </div>
      </TabPane>
    );
    return element;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.driver.data || props.driver.loading) {
      return (
        <section
          id="driver-details"
          name="driver-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const {
      name,
      id,
      alias,
      prevId,
      nextId,
      country,
      countryCode,
      bornDate,
      bornPlace,
      info,
      number,
      teamColor,
      team,
      teams,
      gp,
      circuits,
      countryShort,
      teamShort,
      teamsStats,
      teammates,
      stats,
      internet,
    } = props.driver.data;
    const { starts, qual } = props.driver.data.stats;
    const {
      seasons,
      bestResult,
      bestResultSeason,
      champCount,
      allTeams,
      results,
      firstGP,
      lastGP,
      firstWin,
      lastWin,
      firstPodium,
      lastPodium,
      firstQual,
      lastQual,
      firstPP,
      lastPP,
      firstBL,
      lastBL,
      gpPlacesTotal,
      ppPlacesTotal,
      gpPlacesBySeasons,
      gpPointsBySeasons,
      gpPointsClassBySeasons,
      gpWinsFromPlaces,
    } = props.driver.data.career;
    const {
      bestTeamByStarts,
      bestTeamByQualStarts,
      bestTeamByWins,
      bestTeamByPodium,
      bestTeamByPolepos,
      bestTeamByBestlaps,
      bestTeamByPoints,
      bestTeamByPointRaces,
    } = teamsStats;

    const picProfile = `/build/images/drivers/driver_${id}_profile.jpg`;
    let picTeam = "";
    if (teamShort) {
      picTeam = `/build/images/teams/team_${teamShort.toLowerCase()}_profile_logo.jpg`;
    }
    let linkPrev = "";
    let linkNext = "";
    if (props.match.params.year) {
      linkPrev = `/driver/${props.match.params.year}/${prevId}`;
      linkNext = `/driver/${props.match.params.year}/${nextId}`;
    } else {
      linkPrev = `/driver/${prevId}`;
      linkNext = `/driver/${nextId}`;
    }

    const gpPlacesData = [];
    gpPlacesTotal.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return gpPlacesData.push(value);
    });

    const ppPlacesData = [];
    ppPlacesTotal.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return ppPlacesData.push(value);
    });

    const configPlacesAllSeasons = {
      chart: {
        height: 300,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>{series.name}</b></br>" +
          formatMessage({ id: "app.stats.place" }) +
          " {point.key}: <b>{point.y:f}</b>",
        pointFormat: "",
        footerFormat: "",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              if (!event.point.name.startsWith("<")) {
                props.history.push({
                  pathname: `/driver-events/race-places/${alias}/-/-/-/-/-/-/-/${event.point.name}/`,
                });
              }
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.race" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: gpPlacesData,
        },
      ],
    };

    const configPolePosAllSeasons = {
      chart: {
        height: 300,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>{series.name}</b></br>" +
          formatMessage({ id: "app.stats.places" }) +
          " {point.key}: <b>{point.y:f}</b>",
        pointFormat: "",
        footerFormat: "",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              if (!event.point.name.startsWith("<")) {
                props.history.push({
                  pathname: `/driver-events/grid-places/${alias}/-/-/-/-/-/-/-/${event.point.name}/`,
                });
              }
            },
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.grid" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: ppPlacesData,
        },
      ],
    };

    const gpPlacesBySeasonsData = [];
    gpPlacesBySeasons.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return gpPlacesBySeasonsData.push(value);
    });

    const gpPointsClassBySeasonsData = [];
    gpPointsClassBySeasons.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseFloat(item.y, 0);
      return gpPointsClassBySeasonsData.push(value);
    });

    const gpPointsBySeasonsData = [];
    gpPointsBySeasons.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseFloat(item.y, 0);
      return gpPointsBySeasonsData.push(value);
    });

    const configPlacesBySeasons = {
      chart: {
        type: "line",
        height: 300,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: [
        {
          title: {
            text: "",
          },
          min: 0,
          allowDecimals: false,
          reversed: true,
          tickInterval: 1,
        },
      ],
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>" +
          formatMessage({ id: "app.stats.season" }) +
          ": {point.key}</b><table>",
        pointFormat:
          "<tr><td>{series.name}: </td><td><b>{point.y:f}</b></td></tr>",
        footerFormat: "</table>",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true,
          },
        },
        series: {
          label: {
            enabled: false,
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.place" }),
          color: "#000000",
          maxPointWidth: 20,
          data: gpPlacesBySeasonsData,
        },
      ],
    };

    const configPointsBySeasons = {
      chart: {
        height: 300,
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: [
        {
          title: {
            text: "",
          },
          opposite: false,
          min: 0,
          allowDecimals: false,
        },
      ],
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>" +
          formatMessage({ id: "app.stats.season" }) +
          ": {point.key}</b><br/>",
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        column: {
          grouping: false,
        },
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
          cursor: "pointer",
          events: {
            click: function (event) {
              props.history.push({
                pathname: `/drivers-records/points/-/-/-/${event.point.name}/${event.point.name}/0/1/`,
              });
            },
          },
        },
      },
      series: [
        {
          yAxis: 0,
          type: "column",
          name: formatMessage({ id: "app.stats.points" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: gpPointsClassBySeasonsData,
          stack: "Punkty",
          dataLabels: {
            inside: false,
          },
        },
      ],
    };

    const gpWinsFromPlacesData = [];
    gpWinsFromPlaces.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return gpWinsFromPlacesData.push(value);
    });

    const configWinsFromPlaces = {
      chart: {
        height: 300,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: " ",
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        tickInterval: 1,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      tooltip: {
        headerFormat:
          "<b>" +
          formatMessage({ id: "app.stats.place" }) +
          ": {point.key}</b><br/>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: formatMessage({ id: "app.stats.wins" }),
          color: "#cbb973",
          maxPointWidth: 20,
          data: gpWinsFromPlacesData,
        },
      ],
    };

    const panes = [
      {
        menuItem: formatMessage(
          { id: "app.page.driver.stats.tab.teams" },
          { count: teams.length }
        ),
        render: () => this.renderTeamsTab(),
      },
      {
        menuItem: formatMessage(
          { id: "app.page.driver.stats.tab.gp" },
          { count: gp.length }
        ),
        render: () => this.renderGPTab(),
      },
      {
        menuItem: formatMessage(
          { id: "app.page.driver.stats.tab.circuits" },
          { count: circuits.length }
        ),
        render: () => this.renderCircuitsTab(),
      },
      {
        menuItem: formatMessage(
          { id: "app.page.driver.stats.tab.teammates" },
          { count: teammates.length }
        ),
        render: () => this.renderTeammatesTab(),
      },
    ];

    return (
      <section
        id="driver-details"
        name="driver-details"
        className="section-page"
      >
        <SectionPageBanner
          title={name}
          subtitle={<FormattedMessage id={"app.page.driver.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                {!props.match.params.year && (
                  <Segment basic padded>
                    <Select
                      ref={(ref) => {
                        this.comboDriver = ref;
                      }}
                      isLoading={isLoadingExternally}
                      name="drivers"
                      multi={multi}
                      ignoreCase={ignoreCase}
                      ignoreAccents={ignoreAccents}
                      value={state.selectValue}
                      onChange={this.onInputChange}
                      options={props.comboDrivers.data}
                      clearable={clearable}
                      labelKey="label"
                      valueKey="value"
                      placeholder={
                        <FormattedMessage
                          id={"app.placeholder.select.driver"}
                        />
                      }
                      noResultsText={
                        <FormattedMessage id={"app.placeholder.no.results"} />
                      }
                      className="react-select-container"
                      classNamePrefix="react-select"
                      aria-label="driver select"
                    />
                  </Segment>
                )}
                <div>
                  <Grid centered>
                    <Grid.Column mobile={8} tablet={16} computer={16}>
                      {props.match.params.year && (
                        <Image
                          fluid
                          label={{
                            corner: "left",
                            content: `#${number}`,
                            size: "large",
                            horizontal: true,
                            className: `${teamColor}`,
                          }}
                        />
                      )}
                      <Segment basic textAlign="center">
                        <Image
                          fluid
                          centered
                          src={picProfile}
                          alt={name}
                          onError={(e) => {
                            e.target.src =
                              "/build/images/drivers/driver_no_profile.jpg";
                          }}
                        />
                        {champCount > 0 && (
                          <Label attached="bottom" color="red" size="large">
                            {champCount}x{" "}
                            <FormattedMessage id={"app.stats.wc"} />
                          </Label>
                        )}
                      </Segment>
                    </Grid.Column>
                    <Grid.Column
                      mobile={8}
                      tablet={16}
                      computer={16}
                      verticalAlign="middle"
                    >
                      <Image
                        fluid
                        centered
                        src={`/build/images/countries/${countryShort}.jpg`}
                        alt={countryShort}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/countries/country_no_profile.jpg";
                        }}
                      />
                    </Grid.Column>
                  </Grid>
                  <div className="stats-box">
                    {props.match.params.year && (
                      <>
                        <Segment basic textAlign="center">
                          <SectionPageHeader title={team} />
                          <Divider hidden fitted></Divider>
                          {results.length > 0 ? (
                            <>
                              <Statistic size="large" inverted>
                                <Statistic.Value>
                                  {results[0].place}
                                  {"./"}
                                  {results[0].points}
                                  <small className="very">
                                    {" "}
                                    <FormattedMessage id={"app.stats.pts"} />
                                  </small>
                                </Statistic.Value>
                                <Statistic.Label>
                                  <FormattedMessage id={"app.stats.season"} />{" "}
                                  {results[0].season}
                                </Statistic.Label>
                              </Statistic>
                            </>
                          ) : (
                            <>
                              <Header size="large" textAlign="center">
                                -
                              </Header>
                              <br />
                            </>
                          )}
                        </Segment>
                        <Divider></Divider>
                      </>
                    )}
                    {results.length > 0 ? (
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>
                            {bestResult == "MŚ" ? (
                              <>
                                {champCount}
                                <small className="very">x</small>{" "}
                                <FormattedMessage id={"app.stats.wc"} />
                              </>
                            ) : (
                              <>
                                {bestResult ? (
                                  <>
                                    {bestResult}
                                    <small className="very">
                                      ({bestResultSeason})
                                    </small>
                                  </>
                                ) : (
                                  <FormattedMessage id={"app.stats.nc"} />
                                )}
                              </>
                            )}
                          </Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.best.result"} />
                          </Statistic.Label>
                        </Statistic>
                        <div className="buttons">
                          <NavLink
                            className="primary"
                            to={`/driver-events/starts/${alias}`}
                          >
                            <FormattedMessage id={"app.button.history"} />
                          </NavLink>{" "}
                          <NavLink
                            className="secondary"
                            to={`/driver-models/${alias}`}
                          >
                            <FormattedMessage id={"app.button.models"} />
                          </NavLink>
                        </div>
                      </Segment>
                    ) : (
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>-</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.best.result"} />
                          </Statistic.Label>
                        </Statistic>
                      </Segment>
                    )}
                  </div>
                </div>
                <div className="info-box">
                  <Segment padded basic>
                    <Item.Group divided>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.nationality"} />
                          </Item.Header>
                          <Item.Meta>
                            <Flag name={countryCode} />
                            {country}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.born"} />
                          </Item.Header>
                          <Item.Meta>
                            {bornDate !== "" ? bornDate : "-"}{" "}
                            {Cookies.get("lang") == "pl" ?? bornPlace}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.webpage"} />
                          </Item.Header>
                          <Item.Meta>
                            {internet ? (
                              <a
                                href={`http://${internet}`}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {internet}
                              </a>
                            ) : (
                              "-"
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.f1.sesons"} />
                          </Item.Header>
                          <Item.Meta>{seasons}</Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Item.Header>
                          <Item.Meta>{results.length}</Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.teams"} />
                          </Item.Header>
                          <Item.Meta>
                            {allTeams.length > 0 && allTeams.length}
                            {" - "}
                            {allTeams.map((item, idx) =>
                              idx < allTeams.length - 1 ? (
                                <span key={item.id}>
                                  <NavLink to={`/team/${item.alias}`}>
                                    {item.name}
                                    {", "}
                                  </NavLink>
                                </span>
                              ) : (
                                <span key={item.id}>
                                  <NavLink to={`/team/${item.alias}`}>
                                    {item.name}
                                  </NavLink>
                                </span>
                              )
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstGP.id == null && "-"}
                            {firstGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstGP.alias}/${firstGP.year}`}
                                >
                                  {firstGP.year}
                                  {" - "}
                                  {firstGP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstGP.teamAlias}`}>
                                  {firstGP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastGP.id == null && "-"}
                            {lastGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastGP.alias}/${lastGP.year}`}
                                >
                                  {lastGP.year}
                                  {" - "}
                                  {lastGP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastGP.teamAlias}`}>
                                  {lastGP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.win"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstWin.id == null && "-"}
                            {firstWin.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstWin.alias}/${firstWin.year}`}
                                >
                                  {firstWin.year}
                                  {" - "}
                                  {firstWin.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstWin.teamAlias}`}>
                                  {firstWin.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.win"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastWin.id == null && "-"}
                            {lastWin.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastWin.alias}/${lastWin.year}`}
                                >
                                  {lastWin.year}
                                  {" - "}
                                  {lastWin.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastWin.teamAlias}`}>
                                  {lastWin.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.podium"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstPodium.id == null && "-"}
                            {firstPodium.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstPodium.alias}/${firstPodium.year}`}
                                >
                                  {firstPodium.year}
                                  {" - "}
                                  {firstPodium.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstPodium.teamAlias}`}>
                                  {firstPodium.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.podium"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastPodium.id == null && "-"}
                            {lastPodium.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastPodium.alias}/${lastPodium.year}`}
                                >
                                  {lastPodium.year}
                                  {" - "}
                                  {lastPodium.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastPodium.teamAlias}`}>
                                  {lastPodium.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstQual.id == null && "-"}
                            {firstQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstQual.alias}/${firstQual.year}`}
                                >
                                  {firstQual.year}
                                  {" - "}
                                  {firstQual.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstQual.teamAlias}`}>
                                  {firstQual.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastQual.id == null && "-"}
                            {lastQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastQual.alias}/${lastQual.year}`}
                                >
                                  {lastQual.year}
                                  {" - "}
                                  {lastQual.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastQual.teamAlias}`}>
                                  {lastQual.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstPP.id == null && "-"}
                            {firstPP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstPP.alias}/${firstPP.year}`}
                                >
                                  {firstPP.year}
                                  {" - "}
                                  {firstPP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstPP.teamAlias}`}>
                                  {firstPP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastPP.id == null && "-"}
                            {lastPP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastPP.alias}/${lastPP.year}`}
                                >
                                  {lastPP.year}
                                  {" - "}
                                  {lastPP.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastPP.teamAlias}`}>
                                  {lastPP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstBL.id == null && "-"}
                            {firstBL.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstBL.alias}/${firstBL.year}`}
                                >
                                  {firstBL.year}
                                  {" - "}
                                  {firstBL.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstBL.teamAlias}`}>
                                  {firstBL.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastBL.id == null && "-"}
                            {lastBL.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastBL.alias}/${lastBL.year}`}
                                >
                                  {lastBL.year}
                                  {" - "}
                                  {lastBL.gp}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastBL.teamAlias}`}>
                                  {lastBL.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                    </Item.Group>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                {starts === "0" && qual === "0" && (
                  <Segment basic padded>
                    <Message>
                      <Message.Header>
                        <FormattedMessage id={"app.message.header"} />
                      </Message.Header>
                      <p>
                        <FormattedMessage id={"app.page.driver.info"} />
                      </p>
                    </Message>
                  </Segment>
                )}
                {(starts > 0 || qual > 0) && (
                  <Segment basic>
                    <Grid stackable centered>
                      <Grid.Column mobile={16} tablet={11} computer={12}>
                        <Segment padded basic>
                          <div className="driver-stats">
                            <Grid>
                              <Grid.Row
                                textAlign="center"
                                columns={4}
                                only="mobile"
                              >
                                {this.renderStats()}
                              </Grid.Row>
                              <Grid.Row
                                textAlign="center"
                                columns={5}
                                only="tablet computer"
                              >
                                {this.renderStats()}
                              </Grid.Row>
                            </Grid>
                          </div>
                          <div className="driver-seasons-summary">
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.driver.seasons.summary.header"}
                                  values={{ value: results.length }}
                                />
                              }
                              type="secondary"
                            />
                            <Segment basic className="overflow-mobile">
                              <Table basic="very" celled unstackable>
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.season"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.season"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.team.short"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.team"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.gp"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.gp.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.qual"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.qual.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.race"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.race.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p1"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p1.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p2"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p2.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.p3"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.p3.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.podiums"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.podiums.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.polepos"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.polepos.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.bestlaps"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.bestlaps.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.compl"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.compl.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.inc"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.inc.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.points"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.points.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <Popup
                                        trigger={
                                          <div>
                                            <FormattedMessage
                                              id={"app.table.header.place"}
                                            />
                                          </div>
                                        }
                                        flowing
                                        hoverable
                                      >
                                        <FormattedMessage
                                          id={"app.table.header.place.long"}
                                        />
                                      </Popup>
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                  {this.renderDriverSeasonsSummary()}
                                </Table.Body>
                              </Table>
                            </Segment>
                            <Segment basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage
                                    id={"app.message.header.warning"}
                                  />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.driver.seasons.summary.info"}
                                  />
                                  {info}
                                </p>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.legend"} />
                                </Message.Header>
                                <p>
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.team.short"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.team"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.gp"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.gp"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.race"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.races"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.qual"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.qual"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p1"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.wins"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p2"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.second"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.p3"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.third"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.podiums"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.podium"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </b>{" "}
                                  -{" "}
                                  <FormattedMessage id={"app.stats.polepos"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.bestlaps"}
                                    />
                                  </b>{" "}
                                  -{" "}
                                  <FormattedMessage id={"app.stats.bestlaps"} />{" "}
                                  {/* <b>
                                    <FormattedMessage
                                      id={"app.table.header.points.races"}
                                    />
                                  </b>{" "}
                                  -{" "} 
                                  <FormattedMessage
                                    id={"app.stats.points.races"}
                                  />{" "} */}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.compl"}
                                    />
                                  </b>{" "}
                                  -{" "}
                                  <FormattedMessage
                                    id={"app.stats.completed"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.inc"}
                                    />
                                  </b>{" "}
                                  -{" "}
                                  <FormattedMessage
                                    id={"app.stats.incomplete"}
                                  />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.points"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.points"} />{" "}
                                  <b>
                                    <FormattedMessage
                                      id={"app.table.header.place"}
                                    />
                                  </b>{" "}
                                  - <FormattedMessage id={"app.stats.place"} />{" "}
                                </p>
                              </Message>
                              <Divider hidden fitted></Divider>
                            </Segment>
                          </div>
                          <div className="driver-season-details">
                            <Segment basic>
                              <SectionPageHeader
                                title={
                                  <FormattedMessage
                                    id={"app.page.driver.season.details.header"}
                                    values={{
                                      season: state.selectSeason.value,
                                    }}
                                  />
                                }
                                type="secondary"
                              />
                              <Segment basic textAlign="left" className="bold">
                                <Select
                                  ref={(ref) => {
                                    this.comboSeason = ref;
                                  }}
                                  isLoading={isLoadingExternally}
                                  name="seasons"
                                  multi={multi}
                                  ignoreCase={ignoreCase}
                                  ignoreAccents={ignoreAccents}
                                  value={state.selectSeason}
                                  onChange={this.onSeasonChange}
                                  options={results
                                    .map((s) => {
                                      return {
                                        value: s.season,
                                        label: s.season,
                                      };
                                    })
                                    .sort((a, b) => b.value - a.value)}
                                  clearable={clearable}
                                  labelKey="label"
                                  valueKey="value"
                                  placeholder={
                                    <FormattedMessage
                                      id={"app.placeholder.select.season"}
                                    />
                                  }
                                  noResultsText={
                                    <FormattedMessage
                                      id={"app.placeholder.no.results"}
                                    />
                                  }
                                  className="react-select-container-short"
                                  classNamePrefix="react-select"
                                />
                              </Segment>
                              <div className="hideForDesktop">
                                {this.renderDriverSeasonDetailsMobile()}
                              </div>
                              <div className="hideForMobile">
                                {this.renderDriverSeasonDetails()}
                              </div>
                            </Segment>
                          </div>
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.driver.stats.header"}
                              />
                            }
                            type="secondary"
                          />
                          <Tab
                            menu={{ secondary: true, pointing: true }}
                            panes={panes}
                          />
                        </Segment>
                      </Grid.Column>
                      <Grid.Column mobile={16} tablet={5} computer={4}>
                        <div className="driver-charts-all-seasons-content">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.driver.charts.class.title"}
                              />
                            }
                          />
                          {bestResult ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts config={configPlacesBySeasons} />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.driver.charts.class.info"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={
                                  "app.page.driver.charts.point.by.seasons.title"
                                }
                              />
                            }
                          />
                          {bestResult ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts config={configPointsBySeasons} />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={
                                      "app.page.driver.charts.point.by.seasons.info"
                                    }
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.driver.charts.places.title"}
                              />
                            }
                          />
                          {stats.completed != 0 ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts
                                config={configPlacesAllSeasons}
                              />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  {" "}
                                  <FormattedMessage
                                    id={"app.page.driver.charts.places.info"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.driver.charts.grid.title"}
                              />
                            }
                          />
                          {stats.grid != 0 ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts
                                config={configPolePosAllSeasons}
                              />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={"app.page.driver.charts.grid.info"}
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={
                                  "app.page.driver.charts.wins.from.grid.title"
                                }
                              />
                            }
                          />
                          {firstWin != "" ? (
                            <>
                              <Divider hidden></Divider>
                              <ReactHighcharts config={configWinsFromPlaces} />
                            </>
                          ) : (
                            <Segment padded basic>
                              <Message>
                                <Message.Header>
                                  <FormattedMessage id={"app.message.header"} />
                                </Message.Header>
                                <p>
                                  <FormattedMessage
                                    id={
                                      "app.page.driver.charts.wins.from.grid.info"
                                    }
                                  />
                                </p>
                              </Message>
                            </Segment>
                          )}
                        </div>
                        <div className="team-drivers-stats">
                          <Segment basic>
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.driver.team.stats.title"}
                                />
                              }
                            />
                            {bestTeamByStarts.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.races"} />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByStarts[0].name} -{" "}
                                  {bestTeamByStarts[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByStarts[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByStarts[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByStarts[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByStarts}
                                  seriesName={formatMessage({
                                    id: "app.stats.races",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByQualStarts.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.qual"} />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByQualStarts[0].name} -{" "}
                                  {bestTeamByQualStarts[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByQualStarts[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByQualStarts[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByQualStarts[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByQualStarts}
                                  seriesName={formatMessage({
                                    id: "app.stats.qual",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByPoints.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.points"} />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByPoints[0].name} -{" "}
                                  {bestTeamByPoints[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByPoints[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByPoints[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByPoints[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByPoints}
                                  seriesName={formatMessage({
                                    id: "app.stats.points",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByWins.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.wins"} />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByWins[0].name} -{" "}
                                  {bestTeamByWins[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByWins[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByWins[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByWins[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByWins}
                                  seriesName={formatMessage({
                                    id: "app.stats.wins",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByPodium.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage id={"app.stats.podium"} />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByPodium[0].name} -{" "}
                                  {bestTeamByPodium[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByPodium[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByPodium[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByPodium[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByPodium}
                                  seriesName={formatMessage({
                                    id: "app.stats.podium",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByPolepos.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.polepos"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByPolepos[0].name} -{" "}
                                  {bestTeamByPolepos[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByPolepos[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByPolepos[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByPolepos[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByPolepos}
                                  seriesName={formatMessage({
                                    id: "app.stats.polepos",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByBestlaps.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.bestlaps"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByBestlaps[0].name} -{" "}
                                  {bestTeamByBestlaps[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByBestlaps[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByBestlaps[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByBestlaps[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByBestlaps}
                                  seriesName={formatMessage({
                                    id: "app.stats.bestlaps",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                            {bestTeamByPointRaces.length !== 0 && (
                              <Segment basic>
                                <SectionPageHeader
                                  title={
                                    <FormattedMessage
                                      id={"app.stats.points.races"}
                                    />
                                  }
                                  type="quaternary"
                                />
                                <Header textAlign="center">
                                  {bestTeamByPointRaces[0].name} -{" "}
                                  {bestTeamByPointRaces[0].amount}
                                </Header>
                                <NavLink
                                  to={`/team/${bestTeamByPointRaces[0].alias}`}
                                >
                                  <Image
                                    size="tiny"
                                    centered
                                    src={`/build/images/teams/team_${bestTeamByPointRaces[0].team.toLowerCase()}_profile_logo.jpg`}
                                    alt={bestTeamByPointRaces[0].team.toLowerCase()}
                                    onError={(e) => {
                                      e.target.src =
                                        "/build/images/teams/team_no_profile_logo.jpg";
                                    }}
                                  />
                                </NavLink>
                                <ChartStatsBar
                                  values={bestTeamByPointRaces}
                                  seriesName={formatMessage({
                                    id: "app.stats.points.races",
                                  })}
                                  tooltipLabel="Zespół"
                                />
                              </Segment>
                            )}
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                  </Segment>
                )}
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

Driver.propTypes = {
  fetchDriver: PropTypes.func.isRequired,
  driver: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboDrivers: PropTypes.func.isRequired,
  comboDrivers: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  driverId: PropTypes.string,
};

function mapStateToProps({ driver, comboDrivers }) {
  return { driver, comboDrivers };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchDriver, fetchComboDrivers }, dispatch);
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Driver));
