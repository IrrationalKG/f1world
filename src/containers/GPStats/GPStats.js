/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Header,
  Segment,
  Item,
  Flag,
  Icon,
  Table,
  Statistic,
  Image,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import ChartStatsBar from "../../components/ChartStatsBar/ChartStatsBar";
import { fetchGPStats } from "../../actions/GPStatsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPStats.less";

class GPStats extends Component {
  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPStats(props.match.params.id, Cookies.get("lang"));
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { id } = props.match.params;
    const nextId = nextProps.match.params.id;
    if (id !== nextId) {
      props.fetchGPStats(nextId, Cookies.get("lang"));
    }
  }

  renderRaces() {
    const items = [];
    const { props } = this;
    const { alias } = props.gpstats.data;
    const { races } = props.gpstats.data.stats;
    let currentDate = "";
    let idx = 0;
    let pos = "-";
    races.forEach((item) => {
      if (currentDate !== item.date) {
        currentDate = item.date;
        idx += 1;
        pos = `${idx}.`;
      } else {
        pos = "-";
      }
      const circuitPic = `/build/images/circuits/circuit_${item.circuitAlias.toLowerCase()}_profile.jpg`;
      const driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      const element = (
        <Table.Row key={`${item.season}_${item.idDriver}`}>
          <Table.Cell data-title="#">{pos}</Table.Cell>
          <Table.Cell data-title="Data" className="no-wrap left bold">
            <NavLink to={`/gp-result/${alias}/${item.season}`}>
              {item.date}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Tor" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink
                  to={`/circuit/${item.circuitAlias}`}
                  className="image-link"
                >
                  <Image
                    size="tiny"
                    src={circuitPic}
                    alt={item.circuitAlias}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/circuit/${item.circuitAlias}`}>
                    {item.circuit}
                  </NavLink>
                </div>
                <div>
                  <small>{item.track}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink
                  to={`/driver/${item.driverAlias}`}
                  className="image-link"
                >
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>
                </div>
                <div>
                  <small>
                    <FormattedMessage id={"app.table.header.polepos"} />:{" "}
                    <NavLink to={`/driver/${item.ppAlias}`}>
                      {item.ppDriver}
                    </NavLink>
                  </small>
                </div>
                {item.info.includes("jazda wspólna") && (
                  <small className="gpstats-add-info">*{item.info}</small>
                )}
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`} className="image-link">
                  <Image
                    size="tiny"
                    src={`/build/images/teams/team_${item.idTeam.toLowerCase()}_profile_logo.jpg`}
                    alt={item.team}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/team/${item.teamAlias}`}>
                    <Flag name={item.teamCountryCode} /> {item.team}
                  </NavLink>
                </div>
                <div>
                  <small>{item.modelName}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Punkty" className="bold">
            {item.points}
          </Table.Cell>
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderRacesMobile() {
    const elements = [];
    const { props } = this;
    const { alias } = props.gpstats.data;
    const { races } = props.gpstats.data.stats;
    let currentDate = "";
    let idx = 0;
    let pos = "-";
    races.forEach((item) => {
      if (currentDate !== item.date) {
        currentDate = item.date;
        idx += 1;
        pos = `${idx}.`;
      } else {
        pos = "-";
      }
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      }
      const link = `/driver/${item.driverAlias}`;
      const element = (
        <Item key={`${item.season}_${item.idDriver}`}>
          <NavLink to={link} className="image-link">
            <Image
              size="tiny"
              src={filename}
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {pos} <Flag name={item.driverCountryCode} />
              {/* {item.info === "" && ( */}
              <NavLink to={`/driver/${item.driverAlias}`}>
                {item.driver}
              </NavLink>
              {/* )} */}
              {/* {item.info !== "" && (
                <Popup
                  trigger={
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      {item.driver}
                      {' '}
                      <Icon
                        size="tiny"
                        circular
                        inverted
                        color="black"
                        name="info"
                      />
                    </NavLink>
                  }
                  content={item.info}
                />
              )} */}
            </Item.Header>
            <Item.Description>
              <NavLink to={`/team/${item.teamAlias}`}>{item.team}</NavLink>
            </Item.Description>
            <Item.Description>
              <NavLink to={`/gp-result/${alias}/${item.season}`}>
                {item.circuit}
                {" ("}
                {item.date}
              </NavLink>
              {") "}
            </Item.Description>
            <Item.Extra>
              {item.info.includes("jazda wspólna") && (
                <div>
                  <small>*{item.info}</small>
                </div>
              )}
            </Item.Extra>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>{item.points}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  renderDriversStatsCharts() {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    const { drivers: starts } = props.gpstats.data.stats.starts;
    const { drivers: wins } = props.gpstats.data.stats.wins;
    const { drivers: podium } = props.gpstats.data.stats.podium;
    const { drivers: points } = props.gpstats.data.stats.points;
    const { drivers: polepos } = props.gpstats.data.stats.polepos;
    const { drivers: bestlaps } = props.gpstats.data.stats.bestlaps;

    const element = (
      <>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.races"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {starts[0].name.toUpperCase()} - {starts[0].amount}
            </Header>
            <NavLink to={`/driver/${starts[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${starts[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={starts}
              seriesName={formatMessage({ id: "app.stats.races" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.wins"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {wins[0].name.toUpperCase()} - {wins[0].amount}
            </Header>
            <NavLink to={`/driver/${wins[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${wins[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={wins}
              seriesName={formatMessage({ id: "app.stats.wins" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.podium"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {podium[0].name.toUpperCase()} - {podium[0].amount}
            </Header>
            <NavLink to={`/driver/${podium[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${podium[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={podium}
              seriesName={formatMessage({ id: "app.stats.podium" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.points"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {points[0].name.toUpperCase()} - {points[0].amount}
            </Header>
            <NavLink to={`/driver/${points[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${points[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={points}
              seriesName={formatMessage({ id: "app.stats.points" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.polepos"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {polepos[0].name.toUpperCase()} - {polepos[0].amount}
            </Header>
            <NavLink to={`/driver/${polepos[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${polepos[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={polepos}
              seriesName={formatMessage({ id: "app.stats.polepos" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.bestlaps"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {bestlaps[0].name.toUpperCase()} - {bestlaps[0].amount}
            </Header>
            <NavLink to={`/driver/${bestlaps[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/drivers/driver_${bestlaps[0].id}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={bestlaps}
              seriesName={formatMessage({ id: "app.stats.bestlaps" })}
              tooltipLabel="Kierowca"
            />
          </Segment>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderTeamsStatsCharts() {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    const { teams: starts } = props.gpstats.data.stats.starts;
    const { teams: wins } = props.gpstats.data.stats.wins;
    const { teams: podium } = props.gpstats.data.stats.podium;
    const { teams: points } = props.gpstats.data.stats.points;
    const { teams: polepos } = props.gpstats.data.stats.polepos;
    const { teams: bestlaps } = props.gpstats.data.stats.bestlaps;

    const element = (
      <>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.races"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {starts[0].name.toUpperCase()} - {starts[0].amount}
            </Header>
            <NavLink to={`/team/${starts[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/teams/team_${starts[0].id}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={starts}
              seriesName={formatMessage({ id: "app.stats.races" })}
              tooltipLabel="Zespół"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.wins"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {wins[0].name.toUpperCase()} - {wins[0].amount}
            </Header>
            <NavLink to={`/team/${wins[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/teams/team_${wins[0].id}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={wins}
              seriesName={formatMessage({ id: "app.stats.wins" })}
              tooltipLabel="Zespół"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.podium"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {podium[0].name.toUpperCase()} - {podium[0].amount}
            </Header>
            <NavLink to={`/team/${podium[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/teams/team_${podium[0].id}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={podium}
              seriesName={formatMessage({ id: "app.stats.podium" })}
              tooltipLabel="Zespół"
            />
          </Segment>
        </Grid.Column>
        {points && (
          <Grid.Column>
            <Segment basic>
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.points"} />}
                type="quaternary"
              />
              <Header size="small" textAlign="center">
                {points[0].name.toUpperCase()} - {points[0].amount}
              </Header>
              <NavLink to={`/team/${points[0].alias}`}>
                <Image
                  size="small"
                  centered
                  src={`/build/images/teams/team_${points[0].id}_profile_logo.jpg`}
                  onError={(e) => {
                    e.target.src = "/build/images/teams/team_no_profile.jpg";
                  }}
                />
              </NavLink>
              <ChartStatsBar
                values={points}
                seriesName={formatMessage({ id: "app.stats.points" })}
                tooltipLabel="Zespół"
              />
            </Segment>
          </Grid.Column>
        )}
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.polepos"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {polepos[0].name.toUpperCase()} - {polepos[0].amount}
            </Header>
            <NavLink to={`/team/${polepos[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/teams/team_${polepos[0].id}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={polepos}
              seriesName={formatMessage({ id: "app.stats.polepos" })}
              tooltipLabel="Zespół"
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader title="Naj. okrąenia" type="quaternary" />
            <Header size="small" textAlign="center">
              {bestlaps[0].name.toUpperCase()} - {bestlaps[0].amount}
            </Header>
            <NavLink to={`/team/${bestlaps[0].alias}`}>
              <Image
                size="small"
                centered
                src={`/build/images/teams/team_${bestlaps[0].id}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={bestlaps}
              seriesName={formatMessage({ id: "app.stats.bestlaps" })}
              tooltipLabel="Zespół"
            />
          </Segment>
        </Grid.Column>
      </>
    );
    return element;
  }

  render() {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpstats.data || props.gpstats.loading) {
      return (
        <section
          id="gpstats-details"
          name="gpstats-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { name, countryShort, prevId, nextId, seasons, grandPrix, circuits } =
      props.gpstats.data;
    const { races } = props.gpstats.data.stats;
    const picProfile = `../build/images/countries/${countryShort}.jpg`;
    const linkPrev = `/gp-stats/${prevId}`;
    const linkNext = `/gp-stats/${nextId}`;
    if (races.length === 0) {
      return (
        <section
          id="gpstats-details"
          name="gpstats-details"
          className="section-page"
        >
          <SectionPageBanner
            title={name}
            subtitle={<FormattedMessage id={"app.page.gp.stats.subtitle"} />}
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage
                  id={"app.page.gp.stats.info"}
                  values={{ gp: name }}
                />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="gpstats-details"
        name="gpstats-details"
        className="section-page"
      >
        <SectionPageBanner
          title={name}
          subtitle={<FormattedMessage id={"app.page.gp.stats.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <Grid centered className="stats-box">
                  <Grid.Column mobile={8} tablet={16} computer={16}>
                    <Image
                      className="gpstats-details-photo"
                      centered
                      src={picProfile}
                    />
                  </Grid.Column>
                  <Grid.Column
                    mobile={8}
                    tablet={16}
                    computer={16}
                    textAlign="center"
                    verticalAlign="middle"
                  >
                    <Statistic size="large" inverted>
                      <Statistic.Value>{grandPrix}</Statistic.Value>
                      <Statistic.Label>
                        <FormattedMessage id={"app.stats.gp"} />
                      </Statistic.Label>
                    </Statistic>
                  </Grid.Column>
                </Grid>
                <div className="gpstats-details-content">
                  <div className="info-box">
                    <Segment padded basic>
                      <Item.Group divided>
                        <Item>
                          <Item.Content verticalAlign="middle">
                            <Item.Header>
                              <FormattedMessage id={"app.stats.seasons"} />
                            </Item.Header>
                            <Item.Meta>
                              <span>{seasons}</span>
                            </Item.Meta>
                          </Item.Content>
                        </Item>
                        <Item>
                          <Item.Content verticalAlign="middle">
                            <Item.Header>
                              <FormattedMessage id={"app.stats.gp"} />
                            </Item.Header>
                            <Item.Meta>
                              <span>{grandPrix}</span>
                            </Item.Meta>
                          </Item.Content>
                        </Item>
                        <Item>
                          <Item.Content verticalAlign="middle">
                            <Item.Header>
                              <FormattedMessage id={"app.stats.circuits"} />
                            </Item.Header>
                            <Item.Meta>
                              {circuits.map((item) => (
                                <div key={item.alias}>
                                  <NavLink to={`/circuit/${item.alias}`}>
                                    {item.track}
                                    {" ("}
                                    {item.circuit}
                                    {") "}
                                    {item.seasons}
                                    {" ("}
                                    {item.amount}
                                    {") "}
                                  </NavLink>
                                </div>
                              ))}
                            </Item.Meta>
                          </Item.Content>
                        </Item>
                      </Item.Group>
                    </Segment>
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered>
                  <Grid.Column mobile={16} tablet={11} computer={12}>
                    <Segment basic padded>
                      <div className="gpstats-races">
                        <SectionPageHeader
                          title={<FormattedMessage id={"app.stats.winners"} />}
                          type="secondary"
                        />
                        <div className="hideForDesktop">
                          <Segment basic>
                            <Item.Group divided unstackable>
                              {this.renderRacesMobile()}
                            </Item.Group>
                          </Segment>
                        </div>
                        <div className="hideForMobile">
                          <Segment basic className="overflow">
                            <Table
                              basic="very"
                              celled
                              className="responsive-table center-aligned"
                            >
                              <Table.Header>
                                <Table.Row>
                                  <Table.HeaderCell>#</Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.date"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.circuit"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.driver"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell className="left">
                                    <FormattedMessage
                                      id={"app.table.header.team"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    {" "}
                                    <FormattedMessage
                                      id={"app.table.header.points.long"}
                                    />
                                  </Table.HeaderCell>
                                </Table.Row>
                              </Table.Header>
                              <Table.Body>{this.renderRaces()}</Table.Body>
                            </Table>
                          </Segment>
                        </div>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={5} computer={4}>
                    <div className="gpstats-content">
                      <Segment basic>
                        <SectionPageHeader
                          title={<FormattedMessage id={"app.stats.drivers"} />}
                        />
                        <Grid stackable>
                          <Grid.Row textAlign="center" columns={1}>
                            {this.renderDriversStatsCharts()}
                          </Grid.Row>
                        </Grid>
                        <SectionPageHeader
                          title={<FormattedMessage id={"app.stats.teams"} />}
                        />
                        <Grid stackable>
                          <Grid.Row textAlign="center" columns={1}>
                            {this.renderTeamsStatsCharts()}
                          </Grid.Row>
                        </Grid>
                      </Segment>
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

GPStats.propTypes = {
  fetchGPStats: PropTypes.func.isRequired,
  gpstats: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  id: PropTypes.string,
};

function mapStateToProps({ gpstats }) {
  return { gpstats };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchGPStats }, dispatch);
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPStats)
);
