<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="(SELECT max(season) maxYear FROM gp_season)";
$result = mysqli_query($dbhandle,$query);
$year;
while($r = mysqli_fetch_assoc($result)) {
  $year = $r["maxYear"];
}

//zwyciezcy sezonu - kierowcy
$query="SELECT drivers_class.season ,drivers.id_driver id,drivers.alias,CONCAT(drivers.name,' ',drivers.surname) name,
drivers.country,drivers.country_code countryCode,drivers.picture,drivers_class.points_class,
(SELECT COUNT(drivers_gp_results.id_drivers_gp) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_class.id_driver AND drivers_gp_results.race_pos=1 AND drivers_gp_results.season=drivers_class.season) wins,
(SELECT COUNT(gp_season.id_gp_season) FROM gp_season WHERE gp_season.season=drivers_class.season) races 
FROM drivers_class,drivers
where drivers_class.id_driver=drivers.id_driver and drivers_class.place=1 and drivers_class.season<=$year
and drivers_class.is_classified=1
GROUP by drivers_class.season order by drivers_class.season desc";
$result = mysqli_query($dbhandle,$query);
$seasonsWinnersTab = array();
while($r = mysqli_fetch_assoc($result)) {
	$seasonsWinnersTab[$r["season"]] = $r;
}
//zwyciezcy sezonu - zespoly
$query="SELECT teams_class.season, teams.id_team id, lower(teams.team) team, teams.alias, teams.name name, teams.country, teams.country_code countryCode, teams.picture, teams_class.points_class 
FROM teams_class, teams 
WHERE teams_class.id_team = teams.id_team AND teams_class.place = 1 AND teams_class.season <= $year
and teams_class.is_classified=1
GROUP BY teams_class.season 
ORDER BY teams_class.season DESC";
$result = mysqli_query($dbhandle,$query);
$seasonsTeamsWinnersTab = array();
while($r = mysqli_fetch_assoc($result)) {
	$seasonsTeamsWinnersTab[$r["season"]] = $r;
}
//liczba kierowcow w sezonie
$query="SELECT count(*) drivers, season FROM drivers_class group by season order by season desc";
$result = mysqli_query($dbhandle,$query);
$seasonsDriversTab = array();
while($r = mysqli_fetch_assoc($result)) {
	$seasonsDriversTab[$r["season"]] = $r["drivers"];
}
//liczba zespolow w sezonie
$query="SELECT count(*) teams, season FROM teams_class group by season order by season desc";
$result = mysqli_query($dbhandle,$query);
$seasonsTeamsTab = array();
while($r = mysqli_fetch_assoc($result)) {
	$seasonsTeamsTab[$r["season"]] = $r["teams"];
}
//liczba gp w sezonie
$query="SELECT count(*) gps, season FROM gp_season group by season order by season desc";
$result = mysqli_query($dbhandle,$query);
$seasonsGpsTab = array();
while($r = mysqli_fetch_assoc($result)) {
	$seasonsGpsTab[$r["season"]] = $r["gps"];
}
//liczba kierowcow z wygrana w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount, season FROM drivers_gp_results where drivers_gp_results.race_pos=1 group by drivers_gp_results.season ORDER BY drivers_gp_results.season DESC";
$result = mysqli_query($dbhandle,$query);
$winnersCount = array();
while($r = mysqli_fetch_assoc($result)) {
	$winnersCount[$r["season"]] = $r["amount"];
}
//liczba kierowcow na podium w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount, season FROM drivers_gp_results where drivers_gp_results.race_pos<4 group by drivers_gp_results.season ORDER BY drivers_gp_results.season DESC";
$result = mysqli_query($dbhandle,$query);
$podiumsCount = array();
while($r = mysqli_fetch_assoc($result)) {
	$podiumsCount[$r["season"]] = $r["amount"];
}
//liczba kierowcow z pole position w sezonie
$query="SELECT count(distinct drivers_gp_starting_grid.id_driver) amount, season FROM drivers_gp_starting_grid where drivers_gp_starting_grid.is_pp=1 group by drivers_gp_starting_grid.season ORDER BY drivers_gp_starting_grid.season DESC";
$result = mysqli_query($dbhandle,$query);
$ppCount = array();
while($r = mysqli_fetch_assoc($result)) {
	$ppCount[$r["season"]] = $r["amount"];
}
//liczba kierowcow z najlepszym okrazeniem w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount, season FROM drivers_gp_results where drivers_gp_results.race_best_lap=1 group by drivers_gp_results.season ORDER BY drivers_gp_results.season DESC";
$result = mysqli_query($dbhandle,$query);
$blCount = array();
while($r = mysqli_fetch_assoc($result)) {
	$blCount[$r["season"]] = $r["amount"];
}
//liczba kierowcow punktujacych w sezonie
$query="SELECT count(distinct drivers_gp_results.id_driver) amount, season FROM drivers_gp_results where drivers_gp_results.race_points>0 group by drivers_gp_results.season ORDER BY drivers_gp_results.season DESC";
$result = mysqli_query($dbhandle,$query);
$pointsCount = array();
while($r = mysqli_fetch_assoc($result)) {
	$pointsCount[$r["season"]] = $r["amount"];
}
// sezony
$query="SELECT DISTINCT season,'' winner,'' drivers, '' teams,'' gps
from drivers_class where drivers_class.season<=$year
and drivers_class.is_classified=1
ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$seasonsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gps"] = $seasonsGpsTab[$r["season"]];
  $r["drivers"] = $seasonsDriversTab[$r["season"]];
  $r["teams"] = $seasonsTeamsTab[$r["season"]];
  $r["winner"] = $seasonsWinnersTab[$r["season"]];
  $r["winnerTeam"] = $seasonsTeamsWinnersTab[$r["season"]];
  $r["winnersCount"] = $winnersCount[$r["season"]];
  $r["podiumsCount"] = $podiumsCount[$r["season"]];
  $r["ppCount"] = $ppCount[$r["season"]];
  $r["blCount"] = $blCount[$r["season"]];
  $r["pointsCount"] = $pointsCount[$r["season"]];
  $seasonsItems[] = $r;
}

$historySeasons["seasons"]=$seasonsItems;

$historySeasons["createTime"]=microtime(true)-$start_time;

// Response
$response = $historySeasons;

print json_encode($response);
mysqli_free_result($result);
?>
