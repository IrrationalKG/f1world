import { FETCH_CONTEST, FETCH_CONTEST_SUCCESS, FETCH_CONTEST_ERROR } from '../actions/ContestActions';

// export default (state = [], action) => {
//   switch (action.type) {
//     case FETCH_CONTEST:
//       return action.payload;
//     default:
//       return state;
//   }
// };
const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CONTEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CONTEST_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CONTEST_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
