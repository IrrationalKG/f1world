<?
header('Access-Control-Allow-Origin: *');

$year=$_GET['year'];
if ($year==null) $year=$_POST['year'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// max season
$query="SELECT max(sezon) maxYear FROM typy";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
$maxYear="";
while($r = mysqli_fetch_assoc($result)) {
  $contest["maxYear"]=$r["maxYear"];
  $maxYear=$r["maxYear"];
}

// grand prix
$query="SELECT gp.country_code name, gp.name gp from gp,gp_season where gp.id_gp=gp_season.id_gp and gp_season.season='$year' order by gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpItems[] = $r;
}

// klasyfikacja zwykla
$query="SELECT id_user id,CONCAT(name,' ',surname) name, alias, place, diffrence diff, points
FROM competition_places,users
WHERE users.id_user=competition_places.user AND competition_places.season='$year'
AND round = (SELECT MAX(round) FROM competition_places WHERE season='$year')
ORDER BY place, competition_places.points DESC,users.surname";
$result = mysqli_query($dbhandle,$query);
$classificationItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
  $query2="select distinct gp.name gp, gp.country_code name, gp.name_alias alias, coalesce(typy.place,'-') place, coalesce(typy.suma,'-') points from gp_season left join gp On gp_season.id_gp=gp.id_gp left join typy on typy.gp=gp.name_short and typy.gp=gp.name_short AND typy.sezon='$year' and typy.is_deleted=0 and typy.uczestnik='$id' where gp_season.season='$year' order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gp=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $gp[] = $r2;
  }
  $r["gp"]=$gp;
  $classificationItems[] = $r;
}

// klasyfikacja GP
$query="SELECT id_user id,CONCAT(name,' ',surname) name, alias, place, diffrence diff, points
FROM competition_places_gp,users
WHERE users.id_user=competition_places_gp.user AND competition_places_gp.season='$year'
AND round = (SELECT MAX(round) FROM competition_places_gp WHERE season='$year')
ORDER BY place, competition_places_gp.points DESC,users.surname";
$result = mysqli_query($dbhandle,$query);
$classificationGPItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
  $query2="select distinct gp.name gp, gp.country_code name, gp.name_alias alias, coalesce(typy.place,'-') place, coalesce(typy.pkt_gp,'-') points from gp_season 
  left join gp On gp_season.id_gp=gp.id_gp 
  left join typy on typy.gp=gp.name_short and typy.gp=gp.name_short AND typy.sezon='$year' and typy.is_deleted=0 and typy.uczestnik='$id' where gp_season.season='$year' order by gp_season.sort";
  $result2 = mysqli_query($dbhandle,$query2);
  $gp=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $gp[] = $r2;
  }
  $r["gp"]=$gp;
  $classificationGPItems[] = $r;
}

// Statystyki
$statsItems=array();
// sezon
$statsContestItems=array();
// liczba graczy
$query="SELECT count(distinct uczestnik) amount FROM typy WHERE sezon='$year'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["players"] = $r["amount"];
}
// liczba kolejek
$query="SELECT count(distinct gp) amount FROM competition_places WHERE season='$year'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["rounds"] = $r["amount"];
}
// średnia liczba graczy na kolejkę
$query="SELECT round(count(uczestnik)/count(distinct gp),2) amount FROM typy WHERE typy.gp IN (SELECT distinct(gp) FROM `competition_places` where season='$year') AND sezon='$year' and is_deleted=0;";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["averagePlayers"] = $r["amount"];
}
// liczba zwycięzców
$query="SELECT count(distinct uczestnik) amount FROM typy WHERE sezon='$year' and is_deleted=0 and place=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["winners"] = $r["amount"];
}
// liczba graczy na podium
$query="SELECT count(distinct uczestnik) amount FROM typy WHERE sezon='$year' and is_deleted=0 and place<4";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsContestItems["podium"] = $r["amount"];
}
$statsItems["contest"]=$statsContestItems;

// gracze
$statsPlayersItems=array();
// najwiecej punktow
$query="SELECT sum(suma) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["maxPoints"] = $r;
}
// najwiecej punktow gp
$query="SELECT sum(pkt_gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["maxPointsGP"] = $r;
}
// srednia punktow
$query="SELECT round(sum(suma)/count(gp),2) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["averagePoints"] = $r;
}
// srednia punktow GP
$query="SELECT round(sum(pkt_gp)/count(gp),2) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["averagePointsGP"] = $r;
}
// max punktow w kolejce
$query="SELECT sum(suma) amount,gp, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and is_deleted=0 GROUP BY uczestnik,gp ORDER BY 1 DESC, send_date ASC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["maxRoundPoints"] = $r;
}
// zwyciestwa
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=1 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["wins"] = $r;
}
// drugie miejsca
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=2 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["second"] = $r;
}
// trzecie miejsca
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place=3 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["third"] = $r;
}
// podium
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and place<4 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["podium"] = $r;
}
// top 10
$query="SELECT count(gp) amount, CONCAT(name,' ',surname) name, alias FROM typy,users WHERE typy.uczestnik=users.id_user AND sezon='$year' and place>0 and place<11 and is_deleted=0 GROUP BY uczestnik ORDER BY 1 DESC, send_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsPlayersItems["top10"] = $r;
}
$statsItems["players"]=$statsPlayersItems;

$contest["season"]=$year;
$contest["stats"]=$statsItems;
$contest["gp"]=$gpItems;
$contest["classification"]=$classificationItems;
$contest["classificationGP"]=$classificationGPItems;
$contest["createTime"]=microtime(true)-$start_time;

// Response
$response = $contest;

print json_encode($response);
mysqli_free_result($result);
?>
