import { FETCH_CLASS_DRIVERS_REQUEST, FETCH_CLASS_DRIVERS_SUCCESS, FETCH_CLASS_DRIVERS_ERROR } from '../actions/ClassDriversActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_CLASS_DRIVERS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_CLASS_DRIVERS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_CLASS_DRIVERS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
