// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_CONTEST_GP_REQUEST = "FETCH_COMBO_CONTEST_GP_REQUEST";
export const FETCH_COMBO_CONTEST_GP_SUCCESS = "FETCH_COMBO_CONTEST_GP_SUCCESS";
export const FETCH_COMBO_CONTEST_GP_ERROR = "FETCH_COMBO_CONTEST_GP_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_CONTEST_GP_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_CONTEST_GP_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_CONTEST_GP_ERROR,
    payload: error,
  };
}

export function fetchComboContestGP(season, lang) {
  const url = `${ROOT_URL}/get_combo_contest_gp.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
