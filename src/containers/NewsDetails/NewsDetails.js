/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Grid, Loader, Icon, GridRow, Segment } from "semantic-ui-react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import Disqus from "disqus-react";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { Adsense } from "@ctrl/react-adsense";
import { fetchNewsDetails } from "../../actions/NewsDetailsActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./NewsDetails.less";

class NewsDetails extends Component {
  componentDidMount() {
    const { props } = this;
    props.fetchNewsDetails(props.match.params.newsId, Cookies.get("lang"));
  }

  componentWillReceiveProps(nextProps) {
    const { props } = this;
    const nextNewsId = nextProps.match.params.newsId;

    if (props.match.params.newsId !== nextNewsId) {
      props.fetchNewsDetails(nextNewsId, Cookies.get("lang"));
    }
  }

  render() {
    const { props } = this;
    const { newsDetails } = props;
    if (!newsDetails.data) {
      return (
        <section id="news-details" name="news-details" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { id, idPrev, idNext, title, news, newsDate, photo } =
      newsDetails.data;
    const disqusShortname = `f1world-pl`;
    const disqusConfig = {
      url: `https://f1world.pl/news/${id}`,
      identifier: `news_${id}`,
      title: title,
      language: Cookies.get("lang"),
    };
    const regex = /<br\s*[/]?>/gi;
    const newText = news.replace(regex, "\n");
    let pic = `../build/images/news/${photo}`;
    const linkPrev = `/news/${idPrev}`;
    const linkNext = `/news/${idNext}`;

    const picReplace = "/build/images/news/news.jpg";
    let picp = "";
    let pic1024w = "";
    let pic1024wp = "";
    if (pic) {
      const picName = pic.substring(0, pic.length - 4);
      picp = `${picName}.webp`;

      const img = new Image();
      img.src = picp;
      if (img.height === 0 && picReplace !== undefined) {
        pic = picReplace;
      }

      pic1024w = `${picName}-1024w.jpg`;
      pic1024wp = `${picName}-1024w.webp`;
    }
    return (
      <section id="news-details" name="news-details" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.news.details.title"} />}
          subtitle={newsDate}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column mobile={16} tablet={4} computer={3}>
                <picture>
                  {/* <source
                    media="(max-width: 1024px)"
                    srcSet={pic1024wp}
                    type="image/webp"
                  />
                  <source media="(max-width: 1024px)" srcSet={pic1024w} /> */}
                  <source srcSet={picp} type="image/webp" />
                  <img
                    src={pic}
                    alt={`${title}-${newsDate}`}
                    className="news-details-photo"
                  />
                </picture>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <div className="section-page-content news-details-content">
                  <Grid stackable>
                    <GridRow>
                      <Grid.Column className="title">
                        <Grid verticalAlign="middle">
                          <Grid.Row stretched>
                            <Grid.Column width={2} textAlign="left">
                              {linkPrev && (
                                <NavLink to={linkPrev}>
                                  <Icon link name="chevron left" size="big" />
                                </NavLink>
                              )}
                            </Grid.Column>
                            <Grid.Column width={12} textAlign="center">
                              <h2>{title}</h2>
                            </Grid.Column>
                            <Grid.Column width={2} textAlign="right">
                              {linkNext && (
                                <NavLink to={linkNext}>
                                  <Icon link name="chevron right" size="big" />
                                </NavLink>
                              )}
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Grid.Column>
                    </GridRow>
                    <Grid.Row>
                      <Grid.Column className="news">
                        {newText}
                        <div className="buttons">
                          <NavLink className="secondary" to="/">
                            <FormattedMessage id={"app.button.back"} />
                          </NavLink>
                        </div>
                        {/* <div className="hideForMobile"> */}
                        <div className="adv">
                          <FormattedMessage id={"app.advert"} />
                        </div>
                        <Adsense
                          client="ca-pub-6611514323676676"
                          slot="7600802818"
                        />
                        {/* </div> */}
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column className="disqus">
                        <Disqus.DiscussionEmbed
                          shortname={disqusShortname}
                          config={disqusConfig}
                        />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

NewsDetails.propTypes = {
  fetchNewsDetails: PropTypes.func.isRequired,
  newsDetails: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  newsId: PropTypes.string,
};

function mapStateToProps({ newsDetails }) {
  return { newsDetails };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchNewsDetails }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);
