// const request = require('../json/contest_class.json');
//
// export const FETCH_CONTEST_CLASS_SUCCESS = 'FETCH_CONTEST_CLASS_SUCCESS';
//
// export function fetchContestClass() {
//   return {
//     type: FETCH_CONTEST_CLASS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_CONTEST_CLASS_REQUEST = 'FETCH_CONTEST_CLASS_REQUEST';
export const FETCH_CONTEST_CLASS_SUCCESS = 'FETCH_CONTEST_CLASS_SUCCESS';
export const FETCH_CONTEST_CLASS_ERROR = 'FETCH_CONTEST_CLASS_ERROR';

function requestData() {
  return {
    type: FETCH_CONTEST_CLASS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_CLASS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_CLASS_ERROR,
    payload: error
  };
}

export function fetchContestClass(year) {
  const url = `${ROOT_URL}/get_contest_class.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url, {
      params: {
        year: year
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
