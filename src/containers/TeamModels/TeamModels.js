/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderCountriesFlags"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Segment,
  Statistic,
  Label,
  Popup,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchTeamModels } from "../../actions/TeamModelsActions";
import { fetchComboTeams } from "../../actions/ComboTeamsActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./TeamModels.less";

class TeamModels extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.teamId },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchTeamModels(props.match.params.teamId, Cookies.get("lang"));
    props.fetchComboTeams(
      0,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { teamId } = props.match.params;
    const nextTeamId = nextProps.match.params.teamId;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextTeamId,
        label: nextProps.comboTeams.data?.find((e) => e.value == nextTeamId)
          ?.label,
      },
    });

    if (teamId !== nextTeamId) {
      props.fetchTeamModels(nextTeamId, Cookies.get("lang"));
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/team-models/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderTeamModels() {
    const elements = [];
    const { props } = this;
    const { models } = props.teamModels.data;

    models.forEach((item) => {
      const picCar = `/build/images/teams/models/team_${item.team.toLowerCase()}_${item.model
        .toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const element = (
        <Grid.Column key={item.id_team_model} textAlign="center">
          <Segment padded basic>
            <Segment basic>
              <Popup
                content={
                  item.teamName ? (
                    item.teamName.split(", ").map((t) => {
                      return <div key={t}>{t}</div>;
                    })
                  ) : (
                    <FormattedMessage id={"app.stats.private"} />
                  )
                }
                key={item.id_team_model}
                position="bottom center"
                wide="very"
                trigger={
                  <NavLink
                    to={`/team-model-events/gp/${item.id_team_model}/-/-`}
                  >
                    <Image
                      centered
                      src={picCar}
                      alt={`${item.name} ${item.engine} ${item.model.replaceAll(
                        "???",
                        "3"
                      )} ${item.season}`}
                      onError={(e) => {
                        e.target.src = "/build/images/teams/team_no_car.jpg";
                      }}
                    />
                    <div className="team-models-box-model">
                      {item.engine ? item.engine : item.name}{" "}
                      {item.model
                        ? item.model.replace("_", "/").replaceAll("?", "")
                        : ""}
                    </div>
                  </NavLink>
                }
              />
              <Label attached="top left" color="red">
                &nbsp;{item.season}&nbsp;
              </Label>
            </Segment>
          </Segment>
        </Grid.Column>
      );
      elements.push(element);
    });

    return elements;
  }

  render() {
    const { props, state } = this;
    if (!props.teamModels.data || props.teamModels.loading) {
      return (
        <section id="team-models" name="team-models" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const { name, id, aliasName, banner, prevId, nextId, models, fullname } =
      props.teamModels.data;
    const picLogo = `/build/images/teams/team_${id}_profile_logo.jpg`;

    const linkPrev = `/team-models/${prevId}`;
    const linkNext = `/team-models/${nextId}`;

    return (
      <section id="team-models" name="team-models" className="section-page">
        <SectionPageBanner
          title={name}
          subtitle={<FormattedMessage id={"app.page.team.models.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <div className="section-page-filters">
                  <Select
                    ref={(ref) => {
                      this.comboTeam = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="drivers"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectValue}
                    onChange={this.onInputChange}
                    options={props.comboTeams.data}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={
                      <FormattedMessage id={"app.placeholder.select.team"} />
                    }
                    noResultsText={
                      <FormattedMessage id={"app.placeholder.no.results"} />
                    }
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </div>
                <Grid centered>
                  <Grid.Column mobile={8} tablet={16} computer={16}>
                    {banner == 1 && (
                      <>
                        <Image centered src={picLogo} />
                      </>
                    )}
                  </Grid.Column>
                  <Grid.Column
                    mobile={8}
                    tablet={16}
                    computer={16}
                    textAlign="center"
                    className="info-box"
                  >
                    <div className="vertical-middle-aligned">
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>{models.length}</Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.models"} />
                          </Statistic.Label>
                        </Statistic>
                        <div className="buttons">
                          <NavLink
                            className="primary"
                            to={`/team/${aliasName}`}
                          >
                            <FormattedMessage id={"app.button.profile"} />
                          </NavLink>
                        </div>
                      </Segment>
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment basic>
                  <Grid stackable>
                    <Grid.Row textAlign="center" columns={1} only="mobile">
                      {this.renderTeamModels()}
                    </Grid.Row>
                    <Grid.Row textAlign="center" columns={2} only="tablet">
                      {this.renderTeamModels()}
                    </Grid.Row>
                    <Grid.Row textAlign="center" columns={3} only="computer">
                      {this.renderTeamModels()}
                    </Grid.Row>
                  </Grid>
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

TeamModels.propTypes = {
  fetchTeamModels: PropTypes.func.isRequired,
  teamModels: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboTeams: PropTypes.func.isRequired,
  comboTeams: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  teamId: PropTypes.string,
};

function mapStateToProps({ teamModels, comboTeams }) {
  return { teamModels, comboTeams };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchTeamModels, fetchComboTeams }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamModels);
