// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_LASTGP = "FETCH_LASTGP";
export const FETCH_LASTGP_SUCCESS = "FETCH_LASTGP_SUCCESS";
export const FETCH_LASTGP_ERROR = "FETCH_LASTGP_ERROR";

function requestData() {
  return {
    type: FETCH_LASTGP,
  };
}

function receiveData(data) {
  return {
    type: FETCH_LASTGP_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_LASTGP_ERROR,
    payload: error,
  };
}

export function fetchLastGP(year, id, lang) {
  const url = `${ROOT_URL}/get_last_gp.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
