// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GPSEASON_PLACES_REQUEST = "FETCH_GPSEASON_PLACES_REQUEST";
export const FETCH_GPSEASON_PLACES_SUCCESS = "FETCH_GPSEASON_PLACES_SUCCESS";
export const FETCH_GPSEASON_PLACES_ERROR = "FETCH_GPSEASON_PLACES_ERROR";

function requestData() {
  return {
    type: FETCH_GPSEASON_PLACES_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GPSEASON_PLACES_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GPSEASON_PLACES_ERROR,
    payload: error,
  };
}

export function fetchGPSeasonPlaces(year, lang) {
  const url = `${ROOT_URL}/get_gp_season_places.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
