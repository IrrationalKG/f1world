/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Segment,
  Item,
  Flag,
  Statistic,
  Table,
  Header,
  Label,
  Divider,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import ChartStatsBar from "../../components/ChartStatsBar/ChartStatsBar";
import Map from "../../components/Map/Map";
import { fetchCircuit } from "../../actions/CircuitActions";
import { fetchComboCircuits } from "../../actions/ComboCircuitsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./Circuit.less";

class Circuit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: {
        value: props.match.params.circuitId,
      },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchCircuit(
      props.match.params.circuitId,
      props.match.params.year,
      Cookies.get("lang")
    );
    if (!props.match.params.year) {
      props.fetchComboCircuits(
        0,
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        "-",
        Cookies.get("lang")
      );
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { circuitId } = props.match.params;
    const nextCircuitId = nextProps.match.params.circuitId;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextCircuitId,
        label: nextProps.comboCircuits.data?.find(
          (e) => e.value == nextCircuitId
        )?.label,
      },
    });

    if (circuitId !== nextCircuitId) {
      props.fetchCircuit(nextCircuitId, nextYear, Cookies.get("lang"));
    }
  }

  onInputChange(event) {
    const { props } = this;
    if (props.match.params.year) {
      props.history.push({
        pathname: `/circuit/${props.match.params.year}/${event.value}`,
      });
    } else {
      props.history.push({ pathname: `/circuit/${event.value}` });
    }
    this.setState({
      selectValue: event,
    });
  }

  renderSeasonsWinnersMobile() {
    const items = [];
    const { props } = this;
    const { winners } = props.circuit.data.career;

    winners.forEach((item) => {
      const filename = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      const element = (
        <Grid.Row key={`${item.season}_${item.gpAlias}_${item.idDriver}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/driver/${item.alias}`}>
                <Image
                  size="tiny"
                  src={filename}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/drivers/driver_no_profile.jpg";
                  }}
                />
              </NavLink>
              <Header size="large">
                <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
                  {item.season}
                </NavLink>
              </Header>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    <Flag name={item.driverCountryCode} />
                    <NavLink to={`/driver/${item.alias}`}>
                      {item.driver}
                    </NavLink>
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/gp-result/${item.gpAlias}/${item.season}`}>
                      {item.gp}{" "}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.team}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    {item.raceTime}
                    {item.race_add_info.includes("jazda wspólna") && (
                      <div>
                        <small>*{item.race_add_info}</small>
                      </div>
                    )}
                  </Item.Description>
                  <Item.Description>
                    <div>
                      <FormattedMessage id={"app.table.header.distance"} />:{" "}
                      {item.distance}
                      <small>
                        {" "}
                        km ({item.laps}{" "}
                        <FormattedMessage id={"app.stats.laps.short"} />)
                      </small>
                    </div>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right">
              <Statistic.Value>{item.points}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderDriversStatsCharts() {
    const { props } = this;
    const {
      drivers,
      driversCircuitWins,
      driversCircuitPoints,
      driversCircuitPodium,
      driversCircuitPolepos,
      driversCircuitBestlaps,
      driversCircuitStarts,
    } = props.circuit.data.stats;
    const { formatMessage } = this.props.intl;

    const element = (
      <>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.races"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.starts.name.toUpperCase()} - {drivers.starts.amount}
            </Header>
            <NavLink to={`/driver/${drivers.starts.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.starts.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitStarts}
              seriesName={formatMessage({ id: "app.stats.races" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.wins"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.wins.name.toUpperCase()} - {drivers.wins.amount}
            </Header>
            <NavLink to={`/driver/${drivers.wins.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.wins.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitWins}
              seriesName={formatMessage({ id: "app.stats.wins" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.podium"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.podium.name.toUpperCase()} - {drivers.podium.amount}
            </Header>
            <NavLink to={`/driver/${drivers.podium.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.podium.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitPodium}
              seriesName={formatMessage({ id: "app.stats.podium" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.points"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.points.name.toUpperCase()} - {drivers.points.amount}
            </Header>
            <NavLink to={`/driver/${drivers.points.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.points.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitPoints}
              seriesName={formatMessage({ id: "app.stats.points" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.polepos"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.polepos.name.toUpperCase()} - {drivers.polepos.amount}
            </Header>
            <NavLink to={`/driver/${drivers.polepos.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.polepos.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitPolepos}
              seriesName={formatMessage({ id: "app.stats.polepos" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.bestlaps"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {drivers.bestlaps.name.toUpperCase()} - {drivers.bestlaps.amount}
            </Header>
            <NavLink to={`/driver/${drivers.bestlaps.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/drivers/driver_${drivers.bestlaps.idDriver}_profile.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/drivers/driver_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={driversCircuitBestlaps}
              seriesName={formatMessage({ id: "app.stats.bestlaps" })}
            />
          </Segment>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderTeamsStatsCharts() {
    const { props } = this;
    const {
      teams,
      teamsCircuitWins,
      teamsCircuitPoints,
      teamsCircuitPodium,
      teamsCircuitPolepos,
      teamsCircuitBestlaps,
      teamsCircuitStarts,
    } = props.circuit.data.stats;
    const { formatMessage } = this.props.intl;

    const element = (
      <>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.races"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {teams.starts.name.toUpperCase()} - {teams.starts.amount}
            </Header>
            <NavLink to={`/team/${teams.starts.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/teams/team_${teams.starts.team}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={teamsCircuitStarts}
              seriesName={formatMessage({ id: "app.stats.races" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment padded basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.wins"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {teams.wins.name.toUpperCase()} - {teams.wins.amount}
            </Header>
            <NavLink to={`/team/${teams.wins.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/teams/team_${teams.wins.team}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={teamsCircuitWins}
              seriesName={formatMessage({ id: "app.stats.wins" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.podium"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {teams.podium.name.toUpperCase()} - {teams.podium.amount}
            </Header>
            <NavLink to={`/team/${teams.podium.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/teams/team_${teams.podium.team}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={teamsCircuitPodium}
              seriesName={formatMessage({ id: "app.stats.podium" })}
            />
          </Segment>
        </Grid.Column>
        {teams.points && (
          <Grid.Column>
            <Segment basic>
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.points"} />}
                type="quaternary"
              />
              <Header size="small" textAlign="center">
                {teams.points.name.toUpperCase()} - {teams.points.amount}
              </Header>
              <NavLink to={`/team/${teams.points.alias}`}>
                <Image
                  size="tiny"
                  centered
                  src={`/build/images/teams/team_${teams.points.team}_profile_logo.jpg`}
                  onError={(e) => {
                    e.target.src = "/build/images/teams/team_no_profile.jpg";
                  }}
                />
              </NavLink>
              <ChartStatsBar
                values={teamsCircuitPoints}
                seriesName={formatMessage({ id: "app.stats.points" })}
              />
            </Segment>
          </Grid.Column>
        )}
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.polepos"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {teams.polepos.name.toUpperCase()} - {teams.polepos.amount}
            </Header>
            <NavLink to={`/team/${teams.polepos.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/teams/team_${teams.polepos.team}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={teamsCircuitPolepos}
              seriesName={formatMessage({ id: "app.stats.polepos" })}
            />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Segment basic>
            <SectionPageHeader
              title={<FormattedMessage id={"app.stats.bestlaps"} />}
              type="quaternary"
            />
            <Header size="small" textAlign="center">
              {teams.bestlaps.name.toUpperCase()} - {teams.bestlaps.amount}
            </Header>
            <NavLink to={`/team/${teams.bestlaps.alias}`}>
              <Image
                size="tiny"
                centered
                src={`/build/images/teams/team_${teams.bestlaps.team}_profile_logo.jpg`}
                onError={(e) => {
                  e.target.src = "/build/images/teams/team_no_profile.jpg";
                }}
              />
            </NavLink>
            <ChartStatsBar
              values={teamsCircuitBestlaps}
              seriesName={formatMessage({ id: "app.stats.bestlaps" })}
            />
          </Segment>
        </Grid.Column>
      </>
    );
    return element;
  }

  renderCircuitVersions() {
    const elements = [];
    const { props } = this;
    const { versions, alias, circuit } = props.circuit.data;

    versions.forEach((item) => {
      const picMap = `/build/images/circuits/circuit_${alias}_map_${item.version}.jpg`;
      const seasons =
        item.minSeason == item.maxSeason
          ? item.maxSeason
          : item.minSeason + " - " + item.maxSeason;
      const element = (
        <Grid.Column
          key={item.version}
          textAlign="center"
          className="center-aligned"
          mobile={16}
          tablet={8}
          computer={5}
        >
          <Segment basic>
            <Divider hidden></Divider>
            <Image
              centered
              src={picMap}
              alt={`${circuit} ${seasons}`}
              onError={(e) => {
                e.target.src = "/build/images/circuits/circuit_no_map.jpg";
              }}
            />
            <Label attached="top left" color="red">
              &nbsp;
              {seasons}
              &nbsp;
            </Label>
            <div className="circuit-versions-box-model">
              <FormattedMessage id={"app.stats.length"} />: {item.length} km
            </div>
            <div className="center-aligned">
              <p>
                <small>{item.info}</small>
              </p>
            </div>
          </Segment>
        </Grid.Column>
      );
      elements.push(element);
    });

    return elements;
  }

  render() {
    const { props, state } = this;
    if (!props.circuit.data || props.circuit.loading) {
      return (
        <section
          id="circuit-details"
          name="circuit-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const {
      alias,
      circuit,
      track,
      lat,
      lng,
      prevId,
      nextId,
      countryCode,
      build,
      laps,
      length,
      distance,
      internet,
      picture,
    } = props.circuit.data;
    const {
      seasons,
      races,
      firstGP,
      lastGP,
      firstWin,
      lastWin,
      firstBestLap,
      lastBestLap,
      firstQual,
      lastQual,
      firstPP,
      lastPP,
      bestLap,
      bestQualTime,
      winners,
    } = props.circuit.data.career;

    let picProfile = "/build/images/circuits/circuit_no_profile.jpg";
    let picMap = "/build/images/circuits/circuit_no_map.jpg";
    if (picture === "1") {
      picMap = `/build/images/circuits/circuit_${alias}_map.jpg`;
      picProfile = `/build/images/circuits/circuit_${alias}_profile.jpg`;
    }
    let linkPrev = "";
    let linkNext = "";
    if (props.match.params.year) {
      linkPrev = `/circuit/${props.match.params.year}/${prevId}`;
      linkNext = `/circuit/${props.match.params.year}/${nextId}`;
    } else {
      linkPrev = `/circuit/${prevId}`;
      linkNext = `/circuit/${nextId}`;
    }

    return (
      <section
        id="circuit-details"
        name="circuit-details"
        className="section-page"
      >
        <SectionPageBanner
          title={circuit}
          subtitle={<FormattedMessage id={"app.page.circuit.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                {!props.match.params.year && (
                  <Segment basic padded>
                    <Select
                      ref={(ref) => {
                        this.comboCircuit = ref;
                      }}
                      isLoading={isLoadingExternally}
                      name="circuits"
                      multi={multi}
                      ignoreCase={ignoreCase}
                      ignoreAccents={ignoreAccents}
                      value={state.selectValue}
                      onChange={this.onInputChange}
                      options={props.comboCircuits.data}
                      clearable={clearable}
                      labelKey="label"
                      valueKey="value"
                      placeholder={
                        <FormattedMessage
                          id={"app.placeholder.select.circuit"}
                        />
                      }
                      noResultsText={
                        <FormattedMessage id={"app.placeholder.no.results"} />
                      }
                      className="react-select-container"
                      classNamePrefix="react-select"
                    />
                  </Segment>
                )}
                <div className="circuit-photo-content">
                  <Grid centered>
                    <Grid.Column mobile={8} tablet={16} computer={16}>
                      <Image
                        className="circuit-photo"
                        centered
                        src={picProfile}
                        alt={`${circuit} - ${track}`}
                      />
                    </Grid.Column>
                    <Grid.Column mobile={8} tablet={16} computer={16}>
                      {picture === "1" && (
                        <Image
                          className="circuit-photo"
                          centered
                          src={picMap}
                        />
                      )}
                    </Grid.Column>
                  </Grid>
                  <Segment basic textAlign="center">
                    {picture === "1" && lat != "0" && lng != "0" && (
                      <>
                        <div className="circuit-map">
                          <Map
                            title={circuit}
                            position={[parseFloat(lat), parseFloat(lng)]}
                          />
                        </div>
                      </>
                    )}
                  </Segment>
                </div>
                <div className="info-box">
                  <Segment padded basic>
                    <Item.Group divided>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.circuit"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <Flag name={countryCode} />
                              {track}
                              {" ("}
                              {circuit}
                              {")"}
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.seasons"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>{seasons}</span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>{races}</span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.laps"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>{laps}</span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.length"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              {length} <small className="very">km</small>
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.distance"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              {distance} <small className="very">km</small>
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.webpage"} />
                          </Item.Header>
                          <Item.Meta>
                            <span>
                              <a
                                href={`http://${internet}`}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {internet}
                              </a>
                            </span>
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstGP.id == null && "-"}
                            {firstGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstGP.alias}/${firstGP.year}`}
                                >
                                  {firstGP.raceDate}
                                </NavLink>
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.gp"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastGP.id == null && "-"}
                            {lastGP.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastGP.alias}/${lastGP.year}`}
                                >
                                  {lastGP.raceDate}
                                </NavLink>
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstQual.id == null && "-"}
                            {firstQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${firstQual.alias}/${firstQual.year}`}
                                >
                                  {firstQual.qualDate}
                                </NavLink>
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.qual"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastQual.id == null && "-"}
                            {lastQual.id != null && (
                              <span>
                                <NavLink
                                  to={`/gp-result/${lastQual.alias}/${lastQual.year}`}
                                >
                                  {lastQual.qualDate}
                                </NavLink>
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.winner"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstWin.id == null && "-"}
                            {firstWin.id != null && (
                              <span>
                                <NavLink to={`/driver/${firstWin.alias}`}>
                                  {firstWin.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstWin.teamAlias}`}>
                                  {firstWin.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.winner"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastWin.id == null && "-"}
                            {lastWin.id != null && (
                              <span>
                                <NavLink to={`/driver/${lastWin.alias}`}>
                                  {lastWin.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastWin.teamAlias}`}>
                                  {lastWin.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstBestLap.id == null && "-"}
                            {firstBestLap.id != null && (
                              <span>
                                <NavLink to={`/driver/${firstBestLap.alias}`}>
                                  {firstBestLap.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstBestLap.teamAlias}`}>
                                  {firstBestLap.team}
                                </NavLink>
                                {") - "}
                                {firstBestLap.bestLapTime}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.bestlap"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastBestLap.id == null && "-"}
                            {lastBestLap.id != null && (
                              <span>
                                <NavLink to={`/driver/${lastBestLap.alias}`}>
                                  {lastBestLap.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastBestLap.teamAlias}`}>
                                  {lastBestLap.team}
                                </NavLink>
                                {") - "}
                                {lastBestLap.bestLapTime}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.first.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {firstPP.id == null && "-"}
                            {firstPP.id != null && (
                              <span>
                                <NavLink to={`/driver/${firstPP.alias}`}>
                                  {firstPP.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${firstPP.teamAlias}`}>
                                  {firstPP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.last.polepos"} />
                          </Item.Header>
                          <Item.Meta>
                            {lastPP.id == null && "-"}
                            {lastPP.id != null && (
                              <span>
                                <NavLink to={`/driver/${lastPP.alias}`}>
                                  {lastPP.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${lastPP.teamAlias}`}>
                                  {lastPP.team}
                                </NavLink>
                                {")"}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage id={"app.stats.track.record"} />
                          </Item.Header>
                          <Item.Meta>
                            {bestLap.id == null && "-"}
                            {bestLap.id != null && (
                              <span>
                                <NavLink to={`/driver/${bestLap.alias}`}>
                                  {bestLap.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${bestLap.teamAlias}`}>
                                  {bestLap.team}
                                </NavLink>
                                {") - "}
                                {bestLap.time}
                                {" ("}
                                {bestLap.raceDate}
                                {") "}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                      <Item>
                        <Item.Content verticalAlign="middle">
                          <Item.Header>
                            <FormattedMessage
                              id={"app.stats.track.qual.record"}
                            />
                          </Item.Header>
                          <Item.Meta>
                            {bestQualTime.id == null && "-"}
                            {bestQualTime.id != null && (
                              <span>
                                <NavLink to={`/driver/${bestQualTime.alias}`}>
                                  {bestQualTime.driver}
                                </NavLink>
                                {" ("}
                                <NavLink to={`/team/${bestQualTime.teamAlias}`}>
                                  {bestQualTime.team}
                                </NavLink>
                                {") - "}
                                {bestQualTime.time}
                              </span>
                            )}
                          </Item.Meta>
                        </Item.Content>
                      </Item>
                    </Item.Group>
                  </Segment>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                {races !== "0" && (
                  <Grid stackable centered>
                    <Grid.Column mobile={16} tablet={11} computer={12}>
                      <Segment padded basic>
                        <div className="circuit-info-content">
                          <Segment basic>
                            <Grid>
                              <Grid.Row textAlign="center">
                                <Grid.Column mobile={8} tablet={4} computer={4}>
                                  <Statistic>
                                    <Statistic.Value>
                                      <div>{build}</div>
                                    </Statistic.Value>
                                    <Statistic.Label>
                                      <FormattedMessage
                                        id={"app.stats.debut"}
                                      />
                                    </Statistic.Label>
                                  </Statistic>
                                </Grid.Column>
                                <Grid.Column mobile={8} tablet={4} computer={4}>
                                  <Statistic>
                                    <Statistic.Value>{laps}</Statistic.Value>
                                    <Statistic.Label>
                                      <FormattedMessage id={"app.stats.laps"} />
                                    </Statistic.Label>
                                  </Statistic>
                                </Grid.Column>
                                <Grid.Column mobile={8} tablet={4} computer={4}>
                                  <Statistic>
                                    <Statistic.Value>
                                      {length}{" "}
                                      <small className="very">km</small>
                                    </Statistic.Value>
                                    <Statistic.Label>
                                      <FormattedMessage
                                        id={"app.stats.length"}
                                      />
                                    </Statistic.Label>
                                  </Statistic>
                                </Grid.Column>
                                <Grid.Column mobile={8} tablet={4} computer={4}>
                                  <Statistic>
                                    <Statistic.Value>
                                      {distance}{" "}
                                      <small className="very">km</small>
                                    </Statistic.Value>
                                    <Statistic.Label>
                                      <FormattedMessage
                                        id={"app.stats.distance"}
                                      />
                                    </Statistic.Label>
                                  </Statistic>
                                </Grid.Column>
                              </Grid.Row>
                            </Grid>
                          </Segment>
                        </div>
                        <div className="circuit-seasons-content">
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.circuit.seasons.title"}
                              />
                            }
                            type="secondary"
                          />
                          <div className="hideForDesktop">
                            <Segment basic>
                              <Grid columns={3} divided="vertically">
                                {this.renderSeasonsWinnersMobile()}
                              </Grid>
                            </Segment>
                          </div>
                          <div className="hideForMobile">
                            <Segment basic className="overflow">
                              <Table
                                basic="very"
                                celled
                                className="responsive-table center-aligned"
                              >
                                <Table.Header>
                                  <Table.Row>
                                    <Table.HeaderCell>
                                      <FormattedMessage
                                        id={"app.table.header.season"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <FormattedMessage
                                        id={"app.table.header.gp.long"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <FormattedMessage
                                        id={"app.table.header.circuit"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.driver"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.team"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.laps"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.distance"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell className="left">
                                      <FormattedMessage
                                        id={"app.table.header.time"}
                                      />
                                    </Table.HeaderCell>
                                    <Table.HeaderCell>
                                      <FormattedMessage id={"app.stats.pts"} />
                                    </Table.HeaderCell>
                                  </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                  {winners.map((item) => (
                                    <Table.Row
                                      key={`${item.season}_${item.gpAlias}_${item.idDriver}`}
                                    >
                                      <Table.Cell
                                        data-title="Sezon"
                                        className="bold"
                                      >
                                        <NavLink
                                          to={`/gp-season/${item.season}`}
                                        >
                                          {item.season}
                                        </NavLink>
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Grand Prix"
                                        className="no-wrap left"
                                      >
                                        <div className="box-image-name">
                                          <div>
                                            <NavLink
                                              to={`/gp-result/${item.gpAlias}/${item.season}`}
                                              className="image-link"
                                            >
                                              <Image
                                                size="tiny"
                                                src={`/build/images/countries/${item.gpNameShort.toLowerCase()}.jpg`}
                                                alt={item.gp}
                                                onError={(e) => {
                                                  e.target.src =
                                                    "/build/images/circuits/circuit_no_profile.jpg";
                                                }}
                                              />
                                            </NavLink>
                                          </div>
                                          <div>
                                            <div>
                                              <NavLink
                                                to={`/gp-result/${item.gpAlias}/${item.season}`}
                                              >
                                                {item.gp}{" "}
                                              </NavLink>
                                            </div>
                                            <div>
                                              <small>{item.raceDate} </small>
                                            </div>
                                          </div>
                                        </div>
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Tor"
                                        className="no-wrap left"
                                      >
                                        <Image
                                          size="tiny"
                                          src={`/build/images/circuits/circuit_${item.circuitAlias}_map_${item.version}.jpg`}
                                          alt={`${item.circuitAlias} ${item.season}`}
                                          onError={(e) => {
                                            e.target.src =
                                              "/build/images/circuits/circuit_no_map.jpg";
                                          }}
                                        />
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Kierowca"
                                        className="no-wrap left"
                                      >
                                        <div className="box-image-name">
                                          <div>
                                            <NavLink
                                              to={`/driver/${item.alias}`}
                                              className="image-link"
                                            >
                                              <Image
                                                size="tiny"
                                                src={`/build/images/drivers/driver_${item.idDriver}_profile.jpg`}
                                                alt={item.driver}
                                                onError={(e) => {
                                                  e.target.src =
                                                    "/build/images/drivers/driver_no_profile.jpg";
                                                }}
                                              />
                                            </NavLink>
                                          </div>
                                          <div>
                                            <div>
                                              <Flag
                                                name={item.driverCountryCode}
                                              />
                                              <NavLink
                                                to={`/driver/${item.alias}`}
                                              >
                                                {item.driver}
                                              </NavLink>
                                            </div>
                                            <div>
                                              <small>
                                                <FormattedMessage
                                                  id={
                                                    "app.table.header.polepos"
                                                  }
                                                />
                                                :{" "}
                                                <NavLink
                                                  to={`/driver/${item.ppDriverAlias}`}
                                                >
                                                  {item.ppDriver}
                                                </NavLink>
                                              </small>
                                            </div>
                                            {item.race_add_info.includes(
                                              "jazda wspólna"
                                            ) && (
                                              <small className="circuit-add-info">
                                                *{item.race_add_info}
                                              </small>
                                            )}
                                          </div>
                                        </div>
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Zespół"
                                        className="no-wrap left"
                                      >
                                        <div className="box-image-name">
                                          <div>
                                            <NavLink
                                              to={`/team/${item.teamAlias}`}
                                              className="image-link"
                                            >
                                              <Image
                                                size="tiny"
                                                src={`/build/images/teams/team_${item.idTeam.toLowerCase()}_profile_logo.jpg`}
                                                alt={item.team}
                                                onError={(e) => {
                                                  e.target.src =
                                                    "/build/images/teams/team_no_profile.jpg";
                                                }}
                                              />
                                            </NavLink>
                                          </div>
                                          <div>
                                            <div>
                                              <NavLink
                                                to={`/team/${item.teamAlias}`}
                                              >
                                                <Flag
                                                  name={item.teamCountryCode}
                                                />{" "}
                                                {item.team}
                                              </NavLink>
                                            </div>
                                            <div>
                                              <small>{item.modelName}</small>
                                            </div>
                                          </div>
                                        </div>
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Liczba okrąż."
                                        className="left"
                                      >
                                        {item.laps}
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Długość"
                                        className="left no-wrap"
                                      >
                                        <div>
                                          {item.length}
                                          <small> km</small>
                                        </div>
                                        <div>
                                          {item.distance}
                                          <small> km</small>
                                        </div>
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Czas"
                                        className="left"
                                      >
                                        {item.raceTime}
                                      </Table.Cell>
                                      <Table.Cell
                                        data-title="Punkty"
                                        className="bold"
                                      >
                                        {item.points}
                                      </Table.Cell>
                                    </Table.Row>
                                  ))}
                                </Table.Body>
                              </Table>
                            </Segment>
                          </div>
                        </div>
                        <div className="circuit-versions">
                          <Segment basic>
                            <SectionPageHeader
                              title={
                                <FormattedMessage
                                  id={"app.page.circuit.versions.title"}
                                />
                              }
                            />
                            <Grid stackable>
                              {this.renderCircuitVersions()}
                            </Grid>
                          </Segment>
                        </div>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={5} computer={4}>
                      <div className="circuit-stats-content">
                        <Segment basic>
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.circuit.stats.drivers.title"}
                              />
                            }
                          />
                          <Grid stackable>
                            <Grid.Row textAlign="center" columns={1}>
                              {this.renderDriversStatsCharts()}
                            </Grid.Row>
                          </Grid>
                          <SectionPageHeader
                            title={
                              <FormattedMessage
                                id={"app.page.circuit.stats.teams.title"}
                              />
                            }
                          />
                          <Grid stackable>
                            <Grid.Row textAlign="center" columns={1}>
                              {this.renderTeamsStatsCharts()}
                            </Grid.Row>
                          </Grid>
                        </Segment>
                      </div>
                    </Grid.Column>
                  </Grid>
                )}
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

Circuit.propTypes = {
  fetchCircuit: PropTypes.func.isRequired,
  circuit: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboCircuits: PropTypes.func.isRequired,
  comboCircuits: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  circuitId: PropTypes.string,
};

function mapStateToProps({ circuit, comboCircuits }) {
  return { circuit, comboCircuits };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchCircuit, fetchComboCircuits }, dispatch);
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(Circuit)
);
