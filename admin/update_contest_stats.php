<?
include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];
if ($id==null) $id=-1;

$year=$_GET['year'];
if ($year==null) $year=$_POST['year'];
if ($year==null) $year=-1;

// pobranie punktow i miejsc - klasyfikacja zwykla
echo "Klasyfikacja zwykła<br/>";
$query="SELECT uczestnik,sezon,SUM(suma) points, COUNT(id_typy) rounds,
(select count(place) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.place=1 and t1.is_deleted=0 and suma>0) p1,
(select count(place) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.place=2 and t1.is_deleted=0 and suma>0) p2,
(select count(place) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.place=3 and t1.is_deleted=0 and suma>0) p3,
(select count(place) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.place<4 and t1.is_deleted=0 and suma>0) pod,
(select count(place) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.place<11 and t1.is_deleted=0 and suma>0) top10,
(select COALESCE(max(suma),0) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.is_deleted=0 and suma>0) maxPoints,
(select round(COALESCE(avg(suma),0),0) from typy t1 where t1.uczestnik=t.uczestnik and t1.sezon=t.sezon and t1.is_deleted=0 and suma>0) avgPoints
FROM typy t WHERE is_deleted=0 ";
if ($id!=-1){
	$query.="AND uczestnik='$id' ";
}
if ($year!=-1){
	$query.="AND sezon='$year' ";
}
$query.="GROUP BY sezon,uczestnik ORDER BY sezon desc,points DESC,send_date";
$result = mysqli_query($dbhandle,$query);
$i=0;
$currentYear=0;
while($r = mysqli_fetch_assoc($result)) {
	$user = $r["uczestnik"];
	$season = $r["sezon"];
	$points =$r["points"];
	$rounds =$r["rounds"];
	$p1 =$r["p1"];
	$p2 =$r["p2"];
	$p3 =$r["p3"];
	$pod =$r["pod"];
	$top10 =$r["top10"];
	$maxPoints =$r["maxPoints"];
	$avgPoints =$r["avgPoints"];

	if ($currentYear != $season){
			echo "year:".$currentYear."<br/>";
			$i=0;
			$currentYear = $season;
	}
	$place=$i+1;

	// sprawdzenie czy uzytkownik juz istnieje w sezonie
	$query1="SELECT user FROM competition_stats WHERE user=$user AND season=$season";
	$result1 = mysqli_query($dbhandle,$query1);
	$rowcount=mysqli_num_rows($result1);
	if ($rowcount==0){
		// dodanie
		$query2 = "INSERT INTO competition_stats VALUES ('0',$user,$season,$place,$points,0,0,$rounds,$p1,$p2,$p3,$pod,$top10,$maxPoints,$avgPoints)";
		mysqli_query($dbhandle,$query2);
		echo "[".$place.".] ".$query2."<br/>";
	}else{
		// aktualizacja
		$query2 = "UPDATE competition_stats set place=$place, points=$points, rounds=$rounds, p1=$p1, p2=$p2, p3=$p3, pod=$pod, top10=$top10, max_points=$maxPoints, avg_points=$avgPoints WHERE user=$user AND season=$season";
		mysqli_query($dbhandle,$query2);
		echo "[".$place.".] ".$query2."<br/>";
	}
	mysqli_free_result($result1);
	$i++;
}

// pobranie punktow i miejsc - klasyfikacja gp
echo "Klasyfikacja GP<br/>";
$query="SELECT uczestnik,sezon,SUM(pkt_gp) pointsGP,MIN(send_date) FROM typy WHERE is_deleted=0 ";
if ($id!=-1){
	$query.="AND uczestnik='$id' ";
}
if ($year!=-1){
	$query.="AND sezon='$year' ";
}
$query.="GROUP BY sezon,uczestnik ORDER BY sezon desc,pointsGP DESC,MIN(send_date) ASC";
$result = mysqli_query($dbhandle,$query);
$i=0;
$currentYear=0;
while($r = mysqli_fetch_assoc($result)) {
	$user = $r["uczestnik"];
	$season = $r["sezon"];
	$pointsGP =$r["pointsGP"];

	if ($currentYear != $season){
		// echo "year:".$currentYear."<br/>";
		$i=0;
		$currentYear = $season;
	}
	$placeGP=$i+1;

	$query2 = "UPDATE competition_stats set place_gp=$placeGP, points_gp=$pointsGP WHERE user=$user AND season=$season";
	mysqli_query($dbhandle,$query2);
	echo "[".$placeGP.".] UPDATE USER[".$user."]SEASON[".$season."] - POINTSGP[".$pointsGP."]PLACEGP[".$placeGP."]<br/>";
	mysqli_free_result($result1);
	$i++;
}

mysqli_free_result($result);
?>
