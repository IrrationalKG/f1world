// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_TEAMS_REQUEST = "FETCH_COMBO_TEAMS_REQUEST";
export const FETCH_COMBO_TEAMS_SUCCESS = "FETCH_COMBO_TEAMS_SUCCESS";
export const FETCH_COMBO_TEAMS_ERROR = "FETCH_COMBO_TEAMS_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_TEAMS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_TEAMS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_TEAMS_ERROR,
    payload: error,
  };
}

export function fetchComboTeams(
  empty,
  driver,
  gp,
  circuit,
  engine,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_teams.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          driver: driver,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
