/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Segment,
  Flag,
  Form,
  Message,
  Table,
  Item,
  Statistic,
  Pagination,
  Image,
  Label,
  Icon,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchDriverEvents } from "../../actions/DriverEventsActions";
import { fetchComboDrivers } from "../../actions/ComboDriversActions";
import { fetchComboTeams } from "../../actions/ComboTeamsActions";
import { fetchComboGP } from "../../actions/ComboGPActions";
import { fetchComboCircuits } from "../../actions/ComboCircuitsActions";
import { fetchComboEngines } from "../../actions/ComboEnginesActions";
import { fetchComboModels } from "../../actions/ComboModelsActions";
import { fetchComboTires } from "../../actions/ComboTiresActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
const cmbEvents = require("../../json/combo_grouped_events.json");
const cmbEventsEn = require("../../json/combo_grouped_events_en.json");
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriverEvents.less";

class DriverEvents extends Component {
  constructor(props) {
    super(props);

    const comboEvents = Cookies.get("lang") == "pl" ? cmbEvents : cmbEventsEn;

    let paramEventValue = "-";
    let shouldShowInputPlace = false;
    let paramDriverValue = "-";
    let paramGPValue = "-";
    let paramTeamValue = "-";
    let paramCircuitValue = "-";
    let paramEngineValue = "-";
    let paramModelValue = "-";
    let paramTyreValue = "-";
    let paramSeasonValue = "-";
    let paramPlaceValue = "1";

    if (props.match) {
      if (
        props.match.params.event !== undefined &&
        props.match.params.event !== ""
      ) {
        paramEventValue = props.match.params.event;
        if (
          paramEventValue == "race-places" ||
          paramEventValue == "qual-places" ||
          paramEventValue == "grid-places" ||
          paramEventValue == "wins-from-place" ||
          paramEventValue == "podium-from-place" ||
          paramEventValue == "points-from-place" ||
          paramEventValue == "completed-from-place" ||
          paramEventValue == "incomplete-from-place" ||
          paramEventValue == "finished-from-place" ||
          paramEventValue == "retirement-from-place"
        ) {
          shouldShowInputPlace = true;
        } else {
          shouldShowInputPlace = false;
        }
      }

      if (
        props.match.params.driverId !== undefined &&
        props.match.params.driverId !== ""
      ) {
        paramDriverValue = props.match.params.driverId;
      }

      if (
        props.match.params.teamId !== undefined &&
        props.match.params.teamId !== ""
      ) {
        paramTeamValue = props.match.params.teamId;
      }

      if (
        props.match.params.gpId !== undefined &&
        props.match.params.gpId !== ""
      ) {
        paramGPValue = props.match.params.gpId;
      }

      if (
        props.match.params.circuitId !== undefined &&
        props.match.params.circuitId !== ""
      ) {
        paramCircuitValue = props.match.params.circuitId;
      }

      if (
        props.match.params.engine !== undefined &&
        props.match.params.engine !== ""
      ) {
        paramEngineValue = props.match.params.engine;
      }

      if (
        props.match.params.model !== undefined &&
        props.match.params.model !== ""
      ) {
        paramModelValue = props.match.params.model;
      }

      if (
        props.match.params.tyre !== undefined &&
        props.match.params.tyre !== ""
      ) {
        paramTyreValue = props.match.params.tyre;
      }

      if (
        props.match.params.season !== undefined &&
        props.match.params.season !== ""
      ) {
        paramSeasonValue = props.match.params.season;
      }

      if (
        props.match.params.place !== undefined &&
        props.match.params.place !== ""
      ) {
        paramPlaceValue = props.match.params.place;
      }
    }
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      shouldShowInputPlace: shouldShowInputPlace,
      inputPlaceWidth: "2",

      paramEventValue: paramEventValue,
      paramDriverValue: paramDriverValue,
      paramTeamValue: paramTeamValue,
      paramGPValue: paramGPValue,
      paramCircuitValue: paramCircuitValue,
      paramEngineValue: paramEngineValue,
      paramModelValue: paramModelValue,
      paramTyreValue: paramTyreValue,
      paramSeasonValue: paramSeasonValue,
      paramPlaceValue: paramPlaceValue,

      comboEvents: comboEvents,

      selectEventsValue: {
        value: paramEventValue,
        label: props.match
          ? this.findObjectById(comboEvents, props.match.params.event).label
          : "",
      },
      selectDriversValue: {
        value: paramDriverValue,
      },
      selectTeamsValue: {
        value: paramTeamValue,
      },
      selectGPValue: {
        value: paramGPValue,
      },
      selectCircuitsValue: {
        value: paramCircuitValue,
      },
      selectEnginesValue: {
        value: paramEngineValue,
      },
      selectModelsValue: {
        value: paramModelValue,
      },
      selectTiresValue: {
        value: paramTyreValue,
      },
      selectPlaceValue: paramPlaceValue,
      selectSeasonValue: {
        value: paramSeasonValue,
      },

      activePage: 1,
      boundaryRange: 1,
      siblingRange: 2,
      isSubmited: false,
    };
    this.handleDriversChange = this.handleDriversChange.bind(this);
    this.handleEventsChange = this.handleEventsChange.bind(this);
    this.handlePlaceChange = this.handlePlaceChange.bind(this);
    this.handleTeamsChange = this.handleTeamsChange.bind(this);
    this.handleGPChange = this.handleGPChange.bind(this);
    this.handleCircuitsChange = this.handleCircuitsChange.bind(this);
    this.handleEnginesChange = this.handleEnginesChange.bind(this);
    this.handleModelsChange = this.handleModelsChange.bind(this);
    this.handleTiresChange = this.handleTiresChange.bind(this);
    this.handleSeasonChange = this.handleSeasonChange.bind(this);

    this.resetEventFilter = this.resetEventFilter.bind(this);
    this.resetTeamFilter = this.resetTeamFilter.bind(this);
    this.resetGPFilter = this.resetGPFilter.bind(this);
    this.resetCircuitFilter = this.resetCircuitFilter.bind(this);
    this.resetEnginesFilter = this.resetEnginesFilter.bind(this);
    this.resetModelsFilter = this.resetModelsFilter.bind(this);
    this.resetTiresFilter = this.resetTiresFilter.bind(this);
    this.resetSeasonFilter = this.resetSeasonFilter.bind(this);

    this.handlePaginationChange = this.handlePaginationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { props, state } = this;
    props.fetchDriverEvents(
      "1",
      state.selectDriversValue?.value,
      state.selectEventsValue?.value,
      state.selectPlaceValue,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboDrivers(
      0,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
    props.fetchComboTeams(
      1,
      state.selectDriversValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      Cookies.get("lang")
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (state.isSubmited !== prevState.isSubmited && state.isSubmited) {
      this.setState({
        isSubmited: false,
      });
      props.fetchComboTeams(
        1,
        state.selectDriversValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboGP(
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboCircuits(
        1,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboEngines(
        1,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboModels(
        1,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboTires(
        1,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
      props.fetchComboSeasons(
        true,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        Cookies.get("lang")
      );
      props.fetchDriverEvents(
        "1",
        state.selectDriversValue?.value,
        state.selectEventsValue?.value,
        state.selectPlaceValue,
        state.selectTeamsValue?.value,
        state.selectGPValue?.value,
        state.selectCircuitsValue?.value,
        state.selectEnginesValue?.value,
        state.selectModelsValue?.value,
        state.selectTiresValue?.value,
        state.selectSeasonValue?.value,
        Cookies.get("lang")
      );
    }
    if (prevProps.comboDrivers.data !== this.props.comboDrivers.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.driverId !== undefined &&
        props.match?.params.driverId !== "-"
      ) {
        const paramValue =
          props.match?.params.driverId != "-"
            ? props.match?.params.driverId
            : "";
        param = {
          value: paramValue,
          label: this.props.comboDrivers.data.find((e) => e.value == paramValue)
            ?.label,
        };
      }
      this.setState({ selectDriversValue: param });
    }
    if (prevProps.comboTeams.data !== this.props.comboTeams.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.teamId !== undefined &&
        props.match?.params.teamId !== "-"
      ) {
        const paramValue =
          props.match?.params.teamId != "-" ? props.match?.params.teamId : "";
        param = {
          value: paramValue,
          label: this.props.comboTeams.data.find((e) => e.value == paramValue)
            ?.label,
        };
      } else {
        if (this.state.selectTeamsValue.value !== "-") {
          param = {
            value: this.state.selectTeamsValue.value,
            label: this.props.comboTeams.data.find(
              (e) => e.value == this.state.selectTeamsValue.value
            )?.label,
          };
        }
      }
      this.setState({
        selectTeamsValue: param,
      });
    }
    if (prevProps.comboGPS.data !== this.props.comboGPS.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.gpId !== undefined &&
        props.match?.params.gpId !== "-"
      ) {
        const paramValue =
          props.match?.params.gpId != "-" ? props.match?.params.gpId : "";
        param = {
          value: paramValue,
          label: this.props.comboGPS.data.find((e) => e.value == paramValue)
            ?.label,
        };
      } else {
        if (this.state.selectGPValue.value !== "-") {
          param = {
            value: this.state.selectGPValue.value,
            label: this.props.comboGPS.data.find(
              (e) => e.value == this.state.selectGPValue.value
            )?.label,
          };
        }
      }
      this.setState({
        selectGPValue: param,
      });
    }
    if (prevProps.comboCircuits.data !== this.props.comboCircuits.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.circuitId !== undefined &&
        props.match?.params.circuitId !== "-"
      ) {
        const paramValue =
          props.match?.params.circuitId != "-"
            ? props.match?.params.circuitId
            : "";
        param = {
          value: paramValue,
          label: this.props.comboCircuits.data.find(
            (e) => e.value == paramValue
          )?.label,
        };
      } else {
        if (this.state.selectCircuitsValue.value !== "-") {
          param = {
            value: this.state.selectCircuitsValue.value,
            label: this.props.comboCircuits.data.find(
              (e) => e.value == this.state.selectCircuitsValue.value
            )?.label,
          };
        }
      }
      this.setState({
        selectCircuitsValue: param,
      });
    }
    if (prevProps.comboEngines.data !== this.props.comboEngines.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (this.state.selectEnginesValue.value !== "-") {
        param = {
          value: this.state.selectEnginesValue.value,
          label: this.props.comboEngines.data.find(
            (e) => e.value == this.state.selectEnginesValue.value
          )?.label,
        };
      }
      this.setState({
        selectEnginesValue: param,
      });
    }
    if (prevProps.comboModels.data !== this.props.comboModels.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (this.state.selectModelsValue.value !== "-") {
        param = {
          value: this.state.selectModelsValue.value,
          label: this.props.comboModels.data.find(
            (e) => e.value == this.state.selectModelsValue.value
          )?.label,
        };
      }
      this.setState({
        selectModelsValue: param,
      });
    }
    if (prevProps.comboTires.data !== this.props.comboTires.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (this.state.selectTiresValue.value !== "-") {
        param = {
          value: this.state.selectTiresValue.value,
          label: this.props.comboTires.data.find(
            (e) => e.value == this.state.selectTiresValue.value
          )?.label,
        };
      }
      this.setState({
        selectTiresValue: param,
      });
    }
    if (prevProps.comboSeasons.data !== this.props.comboSeasons.data) {
      let param = {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      };
      if (
        props.match?.params.season !== undefined &&
        props.match?.params.season !== "-"
      ) {
        const paramValue =
          props.match?.params.season != "-" ? props.match?.params.season : "";
        param = {
          value: paramValue,
          label: this.props.comboSeasons.data.find((e) => e.value == paramValue)
            ?.label,
        };
      } else {
        if (this.state.selectSeasonValue.value !== "-") {
          param = {
            value: this.state.selectSeasonValue.value,
            label: this.props.comboSeasons.data.find(
              (e) => e.value == this.state.selectSeasonValue.value
            )?.label,
          };
        }
      }
      this.setState({
        selectSeasonValue: param,
      });
    }
    if (
      prevState.selectEventsValue !== this.state.selectEventsValue ||
      prevState.selectTeamsValue !== this.state.selectTeamsValue ||
      prevState.selectGPValue !== this.state.selectGPValue ||
      prevState.selectCircuitsValue !== this.state.selectCircuitsValue ||
      prevState.selectEnginesValue !== this.state.selectEnginesValue ||
      prevState.selectModelsValue !== this.state.selectModelsValue ||
      prevState.selectTiresValue !== this.state.selectTiresValue ||
      prevState.selectSeasonValue !== this.state.selectSeasonValue ||
      prevState.selectPlaceValue !== this.state.selectPlaceValue
    ) {
      const url = `/driver-events/${
        state.selectEventsValue?.value || "starts"
      }/${state.selectDriversValue?.value || "-"}/${
        state.selectTeamsValue?.value || "-"
      }/${state.selectGPValue?.value || "-"}/${
        state.selectCircuitsValue?.value || "-"
      }/${state.selectEnginesValue?.value || "-"}/${
        state.selectModelsValue?.value || "-"
      }/${state.selectTiresValue?.value || "-"}/${
        state.selectSeasonValue?.value || "-"
      }/${state.selectPlaceValue}/`;
      props.history.replace({ pathname: url });
    }
  }

  resetEventFilter() {
    this.handleEventsChange({
      value: "starts",
      label: "Starty",
    });
  }

  resetTeamFilter() {
    const { formatMessage } = this.props.intl;
    this.handleTeamsChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetGPFilter() {
    const { formatMessage } = this.props.intl;
    this.handleGPChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetCircuitFilter() {
    const { formatMessage } = this.props.intl;
    this.handleCircuitsChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetEnginesFilter() {
    const { formatMessage } = this.props.intl;
    this.handleEnginesChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetModelsFilter() {
    const { formatMessage } = this.props.intl;
    this.handleModelsChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetTiresFilter() {
    const { formatMessage } = this.props.intl;
    this.handleTiresChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  resetSeasonFilter() {
    const { formatMessage } = this.props.intl;
    this.handleSeasonChange({
      value: "-",
      label: formatMessage({ id: "app.placeholder.select.all" }),
    });
  }

  handlePaginationChange = (e, { activePage }) => {
    const { props, state } = this;
    this.setState({ activePage });
    props.fetchDriverEvents(
      activePage,
      state.selectDriversValue?.value,
      state.selectEventsValue?.value,
      state.selectPlaceValue,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
  };

  handleDriversChange(event) {
    const { formatMessage } = this.props.intl;
    this.setState({
      isSubmited: true,
      paramDriverValue: event.value,
      selectDriversValue: event,
      selectTeamsValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectGPValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectCircuitsValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectEnginesValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectModelsValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectTiresValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectSeasonValue: {
        value: "-",
        label: formatMessage({ id: "app.placeholder.select.all" }),
      },
      selectPlaceValue: "1",
      activePage: 1,
    });
    this.handleSubmit();
  }

  handleEventsChange(event) {
    let showInputPlace = false;
    switch (event.value) {
      case "race-places":
      case "qual-places":
      case "grid-places":
      case "wins-from-place":
      case "podium-from-place":
      case "points-from-place":
      case "completed-from-place":
      case "incomplete-from-place":
      case "finished-from-place":
      case "retirement-from-place":
        showInputPlace = true;
        break;
      default:
        break;
    }
    this.setState({
      isSubmited: true,
      paramEventValue: event.value,
      selectEventsValue: event,
      shouldShowInputPlace: showInputPlace,
    });
    this.handleSubmit();
  }

  handlePlaceChange(event) {
    let paramValue = "1";
    if (event.target.value !== undefined && event.target.value !== "") {
      paramValue = event.target.value;
    }
    this.setState({
      paramPlaceValue: paramValue,
      selectPlaceValue: paramValue,
    });
    this.handleSubmit();
  }

  handleTeamsChange(event) {
    const { props, state } = this;
    this.setState({
      paramTeamValue: event.value,
      selectTeamsValue: event,
    });
    props.fetchComboGP(
      event.value,
      state.selectTeamsValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      event.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      event.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      event.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      event.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      event.value,
      state.selectTeamsValue?.value,
      state.selectGPValue?.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleGPChange(event) {
    const { props, state } = this;
    this.setState({
      paramGPValue: event.value,
      selectGPValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      event.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      event.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      event.value,
      state.selectCircuitsValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      event.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectTiresValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      event.value,
      state.selectCircuitsValue?.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectSeasonValue?.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      event.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue?.value,
      state.selectModelsValue?.value,
      state.selectTiresValue?.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleCircuitsChange(event) {
    const { props, state } = this;
    this.setState({
      paramCircuitValue: event.value,
      selectCircuitsValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      event.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectEnginesValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleEnginesChange(event) {
    const { props, state } = this;
    this.setState({
      paramEngineValue: event.value,
      selectEnginesValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleModelsChange(event) {
    const { props, state } = this;
    this.setState({
      paramModelValue: event.value,
      selectModelsValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      event.value,
      state.selectTiresValue.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectTiresValue.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleTiresChange(event) {
    const { props, state } = this;
    this.setState({
      paramTyreValue: event.value,
      selectTiresValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectModelsValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      event.value,
      state.selectSeasonValue.value,
      Cookies.get("lang")
    );
    props.fetchComboSeasons(
      true,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      event.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleSeasonChange(event) {
    const { props, state } = this;
    this.setState({
      paramSeasonValue: event.value,
      selectSeasonValue: event,
    });
    props.fetchComboTeams(
      1,
      state.selectDriversValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      event.value,
      Cookies.get("lang")
    );
    props.fetchComboGP(
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      event.value,
      Cookies.get("lang")
    );
    props.fetchComboCircuits(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      event.value,
      Cookies.get("lang")
    );
    props.fetchComboEngines(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectModelsValue.value,
      state.selectTiresValue.value,
      event.value,
      Cookies.get("lang")
    );
    props.fetchComboModels(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectTiresValue.value,
      event.value,
      Cookies.get("lang")
    );
    props.fetchComboTires(
      1,
      state.selectDriversValue.value,
      state.selectTeamsValue.value,
      state.selectGPValue.value,
      state.selectCircuitsValue.value,
      state.selectEnginesValue.value,
      state.selectModelsValue.value,
      event.value,
      Cookies.get("lang")
    );
    this.handleSubmit();
  }

  handleSubmit() {
    this.setState({
      isSubmited: true,
      activePage: 1,
    });
  }

  findObjectById(arr, value) {
    for (const obj of arr) {
      if (obj.value === value) {
        return obj;
      }
      if (obj.options && obj.options.length > 0) {
        const result = this.findObjectById(obj.options, value);
        if (result) {
          return result;
        }
      }
    }
    return null;
  }

  renderEvents() {
    const items = [];
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    const { results, total } = props.driverEvents.data;
    let cnt = total;
    results.forEach((item) => {
      const markedPPCell = item.gridPos == "1" ? "cell-1" : "";
      const markedGPCell =
        item.race == "1" || item.race == "2" || item.race == "3"
          ? `cell-${item.race}`
          : "";
      const markedSPCell =
        item.sprintPos == "1" || item.sprintPos == "2" || item.sprintPos == "3"
          ? `cell-${item.sprintPos}`
          : "";
      const markedBLCell = item.bestLap == "1" ? "cell-bl" : "";
      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const teamPic = `/build/images/teams/team_${item.team.toLowerCase()}_profile_logo.jpg`;
      const element = (
        <Table.Row key={`${item.season}_${item.alias}_${item.teamAlias}`}>
          <Table.Cell data-title="#" className="bold">
            {item.place}.
          </Table.Cell>
          <Table.Cell data-title="Data" className="no-wrap left bold">
            <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
              {item.raceDate}
            </NavLink>
          </Table.Cell>
          <Table.Cell data-title="Grand Prix" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                  <Image
                    size="tiny"
                    src={gpPic}
                    alt={item.circuitAlias}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/circuits/circuit_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                    {item.gpname}{" "}
                  </NavLink>
                </div>
                <div>
                  <small>{item.circuit}</small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Numer" className="no-wrap">
            {item.number}
          </Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.teamName}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>
                    {item.teamName}
                  </NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
                {item.sharedDrive != "" && (
                  <div>
                    <small className="cell-info">
                      <Icon name="linkify" size="small" />{" "}
                      {`${formatMessage({
                        id: "app.stats.shared.drive",
                      })}: ${item.sharedDrive}`}
                    </small>
                  </div>
                )}
              </div>
            </div>
          </Table.Cell>
          <Table.Cell data-title="Silnik" className="no-wrap left">
            {item.engine ? item.engine : item.teamName}
          </Table.Cell>
          <Table.Cell data-title="Model" className="no-wrap left">
            {item.model?.replace("_", "/").replace("?", "")}
          </Table.Cell>
          <Table.Cell data-title="Opony" className="no-wrap left">
            {item.tyre}
          </Table.Cell>
          {state.selectEventsValue.value === "sprints" ||
          state.selectEventsValue.value === "sprint-wins" ||
          state.selectEventsValue.value === "sprint-podiums" ||
          state.selectEventsValue.value === "sprint-points" ||
          state.selectEventsValue.value === "sprint-completed" ||
          state.selectEventsValue.value === "sprint-incompleted" ? (
            <>
              <Table.Cell data-title="Sprint" className={markedSPCell}>
                {item.sprintCompleted > 0 && item.sprintPos}
                {item.sprintPos != null &&
                  item.sprintCompleted < 1 &&
                  item.sprintInfo === "dyskwalifikacja" && (
                    <FormattedMessage id={"app.table.header.disq"} />
                  )}
                {item.sprintPos != null &&
                  item.sprintCompleted < 1 &&
                  item.sprintInfo !== "dyskwalifikacja" &&
                  "-"}
              </Table.Cell>
              <Table.Cell data-title="Punkty">
                {item.sprintPoints > 0 ? item.sprintPoints : "-"}
              </Table.Cell>
              <Table.Cell data-title="Info" className="left">
                {item.sprintInfo !== "" && (
                  <div>
                    <small className="cell-info">{item.sprintInfo}</small>
                  </div>
                )}
              </Table.Cell>
            </>
          ) : (
            <>
              <Table.Cell data-title="Kwalifikacje">
                {item.qual == null && "-"}
                {item.qual != null && item.qualCompleted > 0 && item.qual}
                {item.qual != null && item.qualCompleted < 1 && "-"}
              </Table.Cell>
              <Table.Cell data-title="Pole Position" className={markedPPCell}>
                {item.gridPos ? item.gridPos : "-"}
              </Table.Cell>
              <Table.Cell data-title="Wyścig" className={markedGPCell}>
                {item.race == null && item.notQualified == 1 && (
                  <FormattedMessage id={"app.stats.nq"} />
                )}
                {item.race == null && item.notStarted == 1 && (
                  <FormattedMessage id={"app.stats.ns"} />
                )}
                {item.race != null && item.raceCompleted > 0 && item.race}
                {item.race != null &&
                  item.raceCompleted < 1 &&
                  item.disq == 1 && (
                    <FormattedMessage id={"app.table.header.disq"} />
                  )}
                {item.race != null &&
                  item.raceCompleted < 1 &&
                  item.disq == 0 &&
                  "-"}
              </Table.Cell>
              <Table.Cell
                data-title="Naj. okrąż."
                id="bestLap"
                className={markedBLCell}
              >
                {item.bestLap > 0 && (
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                )}
                {item.bestLap < 1 && "-"}
              </Table.Cell>
              <Table.Cell data-title="Punkty">
                {item.excluded === "0"
                  ? item.racePoints > 0
                    ? item.racePoints
                    : item.sprintPoints == 0
                    ? "-"
                    : ""
                  : `(${item.points})`}
                {item.sprintPoints > 0 && item.racePoints > 0 && (
                  <>
                    {"+"}
                    {item.sprintPoints}
                    <small>
                      <sup>SP</sup>
                    </small>
                  </>
                )}
                {item.sprintPoints > 0 && item.racePoints == 0 && (
                  <>
                    {item.sprintPoints}
                    <small>
                      <sup>SP</sup>
                    </small>
                  </>
                )}
              </Table.Cell>
              <Table.Cell data-title="Info" className="left">
                {item.qualInfo !== "" && (
                  <div>
                    <small className="cell-info">{item.qualInfo}</small>
                  </div>
                )}
                {item.gridInfo !== "" && item.gridInfo !== item.qualInfo && (
                  <div>
                    <small className="cell-info">{item.gridInfo}</small>
                  </div>
                )}
                {item.info !== "" && (
                  <div>
                    <small className="cell-info">
                      {`${item.info} (${formatMessage({
                        id: "app.stats.lap",
                      })} ${item.laps})`}
                    </small>
                  </div>
                )}
                {item.sprintPoints == 1 && (
                  <div>
                    <small className="cell-text">
                      {formatMessage({
                        id: "app.stats.points.sprint.part1b",
                      }) +
                        item.sprintPoints +
                        formatMessage({
                          id: "app.stats.points.sprint.part2",
                        }) +
                        item.sprintPos +
                        formatMessage({
                          id: "app.stats.points.sprint.part3",
                        })}
                    </small>
                  </div>
                )}
                {item.sprintPoints > 1 && (
                  <div>
                    <small className="cell-text">
                      {formatMessage({
                        id: "app.stats.points.sprint.part1",
                      }) +
                        item.sprintPoints +
                        formatMessage({
                          id: "app.stats.points.sprint.part2",
                        }) +
                        item.sprintPos +
                        formatMessage({
                          id: "app.stats.points.sprint.part3",
                        })}
                    </small>
                  </div>
                )}
              </Table.Cell>
            </>
          )}
        </Table.Row>
      );
      items.push(element);
      cnt -= 1;
    });
    return items;
  }

  renderEventsMobile() {
    const elements = [];
    const { props, state } = this;
    const { results } = props.driverEvents.data;
    const { formatMessage } = this.props.intl;

    results.forEach((item) => {
      const markedPPCell = item.gridPos == "1" ? "cell-1" : "";
      const markedGPCell =
        item.race == "1" || item.race == "2" || item.race == "3"
          ? `cell-${item.race}`
          : "";
      const markedSPCell =
        item.sprintPos == "1" || item.sprintPos == "2" || item.sprintPos == "3"
          ? `cell-${item.sprintPos}`
          : "";
      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Grid.Row key={`${item.season}_${item.alias}_${item.teamAlias}`}>
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                <Image
                  size="tiny"
                  src={gpPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.place}.{" "}
                    <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                      {item.gpname}
                    </NavLink>
                    {" ("}
                    <NavLink to={`/circuit/${item.circuitAlias}`}>
                      {item.circuit}
                    </NavLink>
                    {") "}
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                      {item.raceDate}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    <NavLink to={`/team/${item.teamAlias}`}>
                      {item.teamName} {item.engine ? item.engine : ""}{" "}
                      {item.model?.replace("_", "/").replace("?", "")}
                    </NavLink>
                  </Item.Description>
                  <Item.Description></Item.Description>
                  <Item.Description>{item.tyre}</Item.Description>
                  {state.selectEventsValue.value === "sprints" ||
                  state.selectEventsValue.value === "sprint-wins" ||
                  state.selectEventsValue.value === "sprint-podiums" ||
                  state.selectEventsValue.value === "sprint-points" ||
                  state.selectEventsValue.value === "sprint-completed" ||
                  state.selectEventsValue.value === "sprint-incompleted" ? (
                    <>
                      <Item.Description>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.sprint"} />
                          </div>
                          <div
                            className={`key-value-box-value ${markedSPCell}`}
                          >
                            {item.sprintCompleted > 0 && item.sprintPos}
                            {item.sprintPos != null &&
                              item.sprintCompleted < 1 &&
                              item.sprintInfo === "dyskwalifikacja" && (
                                <FormattedMessage
                                  id={"app.table.header.disq"}
                                />
                              )}
                            {item.sprintPos != null &&
                              item.sprintCompleted < 1 &&
                              item.sprintInfo !== "dyskwalifikacja" &&
                              "-"}
                          </div>
                        </span>
                      </Item.Description>
                      {item.sprintInfo !== "" && (
                        <Item.Extra>
                          <small className="cell-info">{item.sprintInfo}</small>
                        </Item.Extra>
                      )}
                    </>
                  ) : (
                    <>
                      <Item.Description>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.qual == null && "-"}
                            {item.qual != null &&
                              item.qualCompleted > 0 &&
                              item.qual}
                            {item.qual != null && item.qualCompleted < 1 && "-"}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.grid"} />
                          </div>
                          <div
                            className={`key-value-box-value ${markedPPCell}`}
                          >
                            {item.gridPos ? item.gridPos : "-"}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.race"} />
                          </div>
                          <div
                            className={`key-value-box-value ${markedGPCell}`}
                          >
                            {item.race == null && item.notQualified == 1 && (
                              <FormattedMessage id={"app.stats.nq"} />
                            )}
                            {item.race == null && item.notStarted == 1 && (
                              <FormattedMessage id={"app.stats.ns"} />
                            )}
                            {item.race != null &&
                              item.raceCompleted > 0 &&
                              item.race}
                            {item.race != null &&
                              item.raceCompleted < 1 &&
                              item.disq == 1 && (
                                <FormattedMessage
                                  id={"app.table.header.disq"}
                                />
                              )}
                            {item.race != null &&
                              item.raceCompleted < 1 &&
                              item.disq == 0 &&
                              "-"}
                          </div>
                        </span>
                        {item.bestLap == "1" && (
                          <span className="key-value-box">
                            <div className="key-value-box-header">
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </div>
                            <div className="key-value-box-value cell-bl">1</div>
                          </span>
                        )}
                      </Item.Description>
                      {item.sharedDrive != "" && (
                        <Item.Extra>
                          <small className="cell-info">
                            <Icon name="linkify" size="small" />{" "}
                            {`${formatMessage({
                              id: "app.stats.shared.drive",
                            })}: ${item.sharedDrive}`}
                          </small>
                        </Item.Extra>
                      )}
                      {item.qualInfo !== "" && (
                        <Item.Extra>
                          <small className="cell-info">{item.qualInfo}</small>
                        </Item.Extra>
                      )}
                      {item.gridInfo !== "" &&
                        item.gridInfo !== item.qualInfo && (
                          <Item.Extra>
                            <small className="cell-info">{item.gridInfo}</small>
                          </Item.Extra>
                        )}
                      {item.info !== "" && (
                        <Item.Extra>
                          <small className="cell-info">
                            {" "}
                            {`${item.info} (${formatMessage({
                              id: "app.stats.lap",
                            })} ${item.laps})`}
                          </small>
                        </Item.Extra>
                      )}
                      {item.sprintPoints == 1 && (
                        <Item.Description>
                          <small>
                            {formatMessage({
                              id: "app.stats.points.sprint.part1b",
                            }) +
                              item.sprintPoints +
                              formatMessage({
                                id: "app.stats.points.sprint.part2",
                              }) +
                              item.sprintPos +
                              formatMessage({
                                id: "app.stats.points.sprint.part3",
                              })}
                          </small>
                        </Item.Description>
                      )}
                      {item.sprintPoints > 1 && (
                        <Item.Description>
                          <small>
                            {formatMessage({
                              id: "app.stats.points.sprint.part1",
                            }) +
                              item.sprintPoints +
                              formatMessage({
                                id: "app.stats.points.sprint.part2",
                              }) +
                              item.sprintPos +
                              formatMessage({
                                id: "app.stats.points.sprint.part3",
                              })}
                          </small>
                        </Item.Description>
                      )}
                    </>
                  )}
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right">
              <Statistic.Value>
                {state.selectEventsValue.value === "sprints" ||
                state.selectEventsValue.value === "sprint-wins" ||
                state.selectEventsValue.value === "sprint-podiums" ||
                state.selectEventsValue.value === "sprint-points" ||
                state.selectEventsValue.value === "sprint-completed" ||
                state.selectEventsValue.value === "sprint-incompleted" ? (
                  <>{item.sprintPoints}</>
                ) : (
                  <>
                    {item.excluded === "0"
                      ? item.racePoints > 0
                        ? item.racePoints
                        : item.sprintPoints == 0
                        ? "-"
                        : ""
                      : `(${item.points})`}
                    {item.sprintPoints > 0 && item.racePoints > 0 && (
                      <>
                        {"+"}
                        {item.sprintPoints}
                        <small className="very">
                          <sup>SP</sup>
                        </small>
                      </>
                    )}
                    {item.sprintPoints > 0 && item.racePoints == 0 && (
                      <>
                        {item.sprintPoints}
                        <small className="very">
                          <sup>SP</sup>
                        </small>
                      </>
                    )}
                  </>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const {
      driverEvents,
      comboDrivers,
      comboTeams,
      comboGPS,
      comboCircuits,
      comboEngines,
      comboModels,
      comboTires,
      comboSeasons,
    } = this.props;
    const { state } = this;
    const { formatMessage } = this.props.intl;

    if (!driverEvents.data || driverEvents.loading) {
      return (
        <section id="events" name="events" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const {
      isLoadingExternally,
      multi,
      ignoreCase,
      ignoreAccents,
      clearable,
      selectEventsValue,
      selectPlaceValue,
      selectDriversValue,
      selectTeamsValue,
      selectGPValue,
      selectCircuitsValue,
      selectEnginesValue,
      selectModelsValue,
      selectTiresValue,
      selectSeasonValue,
      shouldShowInputPlace,
      activePage,
      boundaryRange,
      siblingRange,
    } = this.state;

    const picProfile = `/build/images/drivers/driver_${driverEvents.data.driver}_profile.jpg`;

    const groupStyles = {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      color: "gray",
      fontSize: "0.9rem",
    };
    const formatGroupLabel = (data) => (
      <div style={groupStyles}>
        <span>{data.label}</span>
      </div>
    );

    return (
      <section id="events" name="events" className="section-page">
        <SectionPageBanner
          title={driverEvents.data.driverName}
          subtitle={<FormattedMessage id={"app.page.driver.events.subtitle"} />}
        />
        <Grid stackable centered>
          <Grid.Column
            mobile={16}
            tablet={4}
            computer={3}
            className="left-sidebar"
          >
            <Grid centered className="stats-box">
              <Grid.Column mobile={8} tablet={16} computer={16}>
                <Image
                  centered
                  src={picProfile}
                  alt={driverEvents.data.driverName}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/drivers/driver_no_profile.jpg";
                  }}
                />
              </Grid.Column>
              <Grid.Column
                mobile={8}
                tablet={16}
                computer={16}
                textAlign="center"
                verticalAlign="middle"
              >
                <div>
                  <Statistic size="large" inverted>
                    <Statistic.Value>{driverEvents.data.total}</Statistic.Value>
                    <Statistic.Label>
                      <FormattedMessage id={"app.stats.gp"} />
                    </Statistic.Label>
                  </Statistic>
                  <div className="buttons">
                    <NavLink
                      className="primary"
                      to={`/driver/${selectDriversValue.value}`}
                    >
                      <FormattedMessage id={"app.button.profile"} />
                    </NavLink>
                    <NavLink
                      className="secondary"
                      to={`/drivers-records/${
                        selectEventsValue.value || "starts"
                      }/${selectTeamsValue.value || "-"}/${
                        selectGPValue.value || "-"
                      }/${selectCircuitsValue.value || "-"}/${
                        selectSeasonValue.value || "-"
                      }/-/0/1/`}
                    >
                      <FormattedMessage id={"app.button.records"} />
                    </NavLink>
                  </div>
                </div>
              </Grid.Column>
            </Grid>
            <div className="section-page-filters events-filters">
              <form onSubmit={this.handleSubmit}>
                <Grid stackable padded>
                  <Grid.Row columns={1}>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="events">
                          <FormattedMessage id={"app.stats.statistics"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="events"
                            name="events"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectEventsValue}
                            onChange={this.handleEventsChange}
                            options={state.comboEvents}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={formatMessage({
                              id: "app.placeholder.select.event",
                            })}
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            formatGroupLabel={formatGroupLabel}
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column className={shouldShowInputPlace ? "" : "hide"}>
                      <Segment basic>
                        <Form.Input
                          fluid
                          label={<FormattedMessage id={"app.stats.place"} />}
                          onChange={this.handlePlaceChange}
                          defaultValue={selectPlaceValue}
                          placeholder={
                            <FormattedMessage id={"app.placeholder.place"} />
                          }
                        />
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="driver">
                          <FormattedMessage id={"app.stats.driver"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="driver"
                            name="driver"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectDriversValue}
                            onChange={this.handleDriversChange}
                            options={comboDrivers.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="teams">
                          <FormattedMessage id={"app.stats.team"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="teams"
                            name="teams"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectTeamsValue}
                            onChange={this.handleTeamsChange}
                            options={comboTeams.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="gp">
                          <FormattedMessage id={"app.stats.gp"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="gp"
                            name="gp"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectGPValue}
                            onChange={this.handleGPChange}
                            options={comboGPS.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="circuits">
                          <FormattedMessage id={"app.stats.circuit"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="circuits"
                            name="circuits"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectCircuitsValue}
                            onChange={this.handleCircuitsChange}
                            options={comboCircuits.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="engine">
                          <FormattedMessage id={"app.stats.engine"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="engine"
                            name="engine"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectEnginesValue}
                            onChange={this.handleEnginesChange}
                            options={comboEngines.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="engine">
                          <FormattedMessage
                            id={"app.table.header.model.long"}
                          />
                          <Select
                            isLoading={isLoadingExternally}
                            id="model"
                            name="model"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectModelsValue}
                            onChange={this.handleModelsChange}
                            options={comboModels.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="engine">
                          <FormattedMessage id={"app.table.header.tires"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="tires"
                            name="tires"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectTiresValue}
                            onChange={this.handleTiresChange}
                            options={comboTires.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <label htmlFor="startYear">
                          <FormattedMessage id={"app.stats.season"} />
                          <Select
                            isLoading={isLoadingExternally}
                            id="startYear"
                            name="startYear"
                            multi={multi}
                            ignoreCase={ignoreCase}
                            ignoreAccents={ignoreAccents}
                            value={selectSeasonValue}
                            onChange={this.handleSeasonChange}
                            options={comboSeasons.data}
                            clearable={clearable}
                            labelKey="label"
                            valueKey="value"
                            placeholder={
                              <FormattedMessage
                                id={"app.placeholder.select.all"}
                              />
                            }
                            noResultsText={
                              <FormattedMessage
                                id={"app.placeholder.no.results"}
                              />
                            }
                            className="react-select-container"
                            classNamePrefix="react-select"
                          />
                        </label>
                      </Segment>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </form>
            </div>
            <Segment basic padded>
              <Message className="inverted">
                <Message.Header>
                  <FormattedMessage id={"app.message.header"} />
                </Message.Header>
                <p>
                  <FormattedMessage id={"app.table.header.sprint"} /> -{" "}
                  <FormattedMessage id={"app.stats.sprint"} />
                  <br />
                  <FormattedMessage id={"app.table.header.qual"} /> -{" "}
                  <FormattedMessage id={"app.stats.qual"} />
                  <br />
                  <FormattedMessage id={"app.table.header.grid"} /> -{" "}
                  <FormattedMessage id={"app.stats.grid"} />
                  <br />
                  <FormattedMessage id={"app.table.header.race"} /> -{" "}
                  <FormattedMessage id={"app.stats.race"} />
                  <br />
                  <FormattedMessage id={"app.table.header.bestlaps"} /> -{" "}
                  <FormattedMessage id={"app.stats.bestlap"} />
                  <br />
                  <FormattedMessage id={"app.stats.nq"} /> -{" "}
                  <FormattedMessage id={"app.stats.not.qualified"} />
                  <br />
                  <FormattedMessage id={"app.stats.ns"} /> -{" "}
                  <FormattedMessage id={"app.stats.not.started"} />
                  <br />
                  <FormattedMessage id={"app.table.header.disq"} /> -{" "}
                  <FormattedMessage id={"app.stats.disq"} />
                  <br />
                  <br />
                  <FormattedMessage id={"app.page.driver.events.info"} />
                </p>
              </Message>
            </Segment>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={12} computer={13}>
            <div className="section-page-content events">
              <Segment basic>
                <SectionPageHeader
                  title={state.selectEventsValue.label}
                  type="secondary"
                />
                <SectionPageHeader
                  title={`${formatMessage({
                    id: "app.placeholder.records.found",
                  })}: ${driverEvents.data.total}`}
                  className="left-aligned"
                />
                <Segment basic padded>
                  {state.selectEventsValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectEventsValue.label}
                      {state.selectEventsValue.value != "starts" && (
                        <Icon name="delete" onClick={this.resetEventFilter} />
                      )}
                    </Label>
                  )}
                  {state.selectTeamsValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectTeamsValue.label}
                      <Icon name="delete" onClick={this.resetTeamFilter} />
                    </Label>
                  )}
                  {state.selectGPValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectGPValue.label}
                      <Icon name="delete" onClick={this.resetGPFilter} />
                    </Label>
                  )}
                  {state.selectCircuitsValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectCircuitsValue.label}
                      <Icon name="delete" onClick={this.resetCircuitFilter} />
                    </Label>
                  )}
                  {state.selectEnginesValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectEnginesValue.label}
                      <Icon name="delete" onClick={this.resetEnginesFilter} />
                    </Label>
                  )}
                  {state.selectModelsValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectModelsValue.label}
                      <Icon name="delete" onClick={this.resetModelsFilter} />
                    </Label>
                  )}
                  {state.selectTiresValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectTiresValue.label}
                      <Icon name="delete" onClick={this.resetTiresFilter} />
                    </Label>
                  )}
                  {state.selectSeasonValue.value != "-" && (
                    <Label color="yellow">
                      {state.selectSeasonValue.label}
                      <Icon name="delete" onClick={this.resetSeasonFilter} />
                    </Label>
                  )}
                </Segment>
                {driverEvents.data.pages > 1 && (
                  <Segment padded="very" basic textAlign="center">
                    <>
                      <div className="hideForDesktop">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driverEvents.data.pages}
                          ellipsisItem={null}
                          boundaryRange="0"
                          siblingRange="1"
                          size="small"
                        />
                      </div>
                      <div className="hideForMobile">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driverEvents.data.pages}
                          ellipsisItem={null}
                          boundaryRange={boundaryRange}
                          siblingRange={siblingRange}
                          size="small"
                        />
                      </div>
                    </>
                  </Segment>
                )}
                <div className="hideForDesktop">
                  <Grid columns={2} divided="vertically">
                    {this.renderEventsMobile()}
                  </Grid>
                </div>
                <div className="hideForMobile">
                  <Segment basic padded className="overflow">
                    <Table
                      basic="very"
                      celled
                      className="responsive-table center-aligned"
                    >
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>#</Table.HeaderCell>
                          <Table.HeaderCell>
                            <FormattedMessage id={"app.table.header.date"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage id={"app.table.header.gp.long"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell>#</Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage id={"app.table.header.team"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage
                              id={"app.table.header.engine.long"}
                            />
                          </Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage
                              id={"app.table.header.model.long"}
                            />
                          </Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage id={"app.table.header.tires"} />
                          </Table.HeaderCell>
                          {selectEventsValue.value === "sprints" ||
                          selectEventsValue.value === "sprint-wins" ||
                          selectEventsValue.value === "sprint-podiums" ||
                          selectEventsValue.value === "sprint-points" ||
                          selectEventsValue.value === "sprint-completed" ||
                          selectEventsValue.value === "sprint-incompleted" ? (
                            <Table.HeaderCell>
                              <FormattedMessage
                                id={"app.table.header.sprint"}
                              />
                            </Table.HeaderCell>
                          ) : (
                            <>
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.qual"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.grid"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.race"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.bestlaps"}
                                />
                              </Table.HeaderCell>
                            </>
                          )}
                          <Table.HeaderCell>
                            <FormattedMessage id={"app.table.header.points"} />
                          </Table.HeaderCell>
                          <Table.HeaderCell className="left">
                            <FormattedMessage id={"app.table.header.info"} />
                          </Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>{this.renderEvents()}</Table.Body>
                    </Table>
                  </Segment>
                </div>
                {driverEvents.data.pages > 1 && (
                  <Segment padded="very" basic textAlign="center">
                    <>
                      <div className="hideForDesktop">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driverEvents.data.pages}
                          ellipsisItem={null}
                          boundaryRange="0"
                          siblingRange="1"
                          size="small"
                        />
                      </div>
                      <div className="hideForMobile">
                        <Pagination
                          activePage={activePage}
                          onPageChange={this.handlePaginationChange}
                          totalPages={driverEvents.data.pages}
                          ellipsisItem={null}
                          boundaryRange={boundaryRange}
                          siblingRange={siblingRange}
                          size="small"
                        />
                      </div>
                    </>
                  </Segment>
                )}
              </Segment>
            </div>
          </Grid.Column>
        </Grid>
      </section>
    );
  }
}

DriverEvents.propTypes = {
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      teamId: PropTypes.string,
      event: PropTypes.string,
      driverId: PropTypes.string,
      gpId: PropTypes.string,
      circuitId: PropTypes.string,
      engine: PropTypes.string,
      models: PropTypes.string,
      tires: PropTypes.string,
      season: PropTypes.string,
      place: PropTypes.string,
    }),
  }),
  fetchDriverEvents: PropTypes.func.isRequired,
  fetchComboDrivers: PropTypes.func.isRequired,
  fetchComboTeams: PropTypes.func.isRequired,
  fetchComboGP: PropTypes.func.isRequired,
  fetchComboCircuits: PropTypes.func.isRequired,
  fetchComboEngines: PropTypes.func.isRequired,
  fetchComboModels: PropTypes.func.isRequired,
  fetchComboTires: PropTypes.func.isRequired,
  fetchComboSeasons: PropTypes.func.isRequired,
  driverEvents: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  comboTeams: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboDrivers: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboGPS: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboCircuits: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboEngines: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboModels: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboTires: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
};

function mapStateToProps({
  driverEvents,
  comboDrivers,
  comboTeams,
  comboGPS,
  comboCircuits,
  comboEngines,
  comboModels,
  comboTires,
  comboSeasons,
}) {
  return {
    driverEvents,
    comboDrivers,
    comboTeams,
    comboGPS,
    comboCircuits,
    comboEngines,
    comboModels,
    comboTires,
    comboSeasons,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchDriverEvents,
      fetchComboDrivers,
      fetchComboTeams,
      fetchComboGP,
      fetchComboCircuits,
      fetchComboEngines,
      fetchComboModels,
      fetchComboTires,
      fetchComboSeasons,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(DriverEvents)
);
