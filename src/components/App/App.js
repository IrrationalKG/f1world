import React, { Component } from "react";
import LoadingBar from "react-redux-loading-bar";
import ToTop from "react-scroll-top";
import PropTypes from "prop-types";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { IntlProvider } from "react-intl";
import Cookies from "js-cookie";
import { Flag, Button } from "semantic-ui-react";
import messages_en from "../../locales/en.json";
import messages_pl from "../../locales/pl.json";
import styles from "./App.less";

const messages = {
  en: messages_en,
  pl: messages_pl,
};

export default class App extends Component {
  constructor(props) {
    super(props);
    const browserLang =
      (navigator.language || navigator.userLanguage) != "pl" ? "en" : "pl";
    const cookieLang = Cookies.get("lang");
    const userLang = cookieLang ? cookieLang : browserLang;
    
    this.state = {
      locale: userLang,
    };
    this.handleLanguageChange = this.handleLanguageChange.bind(this);
  }

  componentDidMount() {
    const domain = window.location.hostname;
    if (domain.includes("en.f1world.pl") && !Cookies.get("lang")) {
      Cookies.set("lang", "en", {
        expires: 365,
      });
      window.location.reload();
    }
    if (!Cookies.get("lang")) {
      Cookies.set("lang", navigator.language == "pl" ? "pl" : "en", {
        expires: 365,
      });
    }
  }

  handleLanguageChange = (e) => {
    Cookies.set("lang", e, { expires: 365 });
    this.setState({
      locale: e,
    });
  };

  render() {
    const toTopStyle = {
      position: "fixed",
      width: "45px",
      height: "45px",
      backgroundColor: "rgba(0, 0, 0, 0.5)",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      borderRadius: "50%",
      cursor: "pointer",
      bottom: "15px",
      right: "15px",
      zIndex: 1,
    };
    const { state } = this;
    const { children } = this.props;
    return (
      <IntlProvider locale={state.locale} messages={messages[state.locale]}>
        <div key={state.locale}>
          <LoadingBar />
          <div className="lang-container">
            <Button size="mini" onClick={() => this.handleLanguageChange("pl")}>
              <Flag name="pl" /> PL
            </Button>
            <Button size="mini" onClick={() => this.handleLanguageChange("en")}>
              <Flag name="gb" /> EN
            </Button>
          </div>
          <Header />

          {children}
          <ToTop hideAt={160} position="bottom" style={toTopStyle} />
          <Footer />
        </div>
      </IntlProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired,
};
