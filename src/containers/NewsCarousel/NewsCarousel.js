/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import OwlCarousel from "react-owl-carousel";
import {
  CardGroup,
  Card,
  CardContent,
  CardHeader,
  CardMeta,
  CardDescription,
  Image,
  Loader,
} from "semantic-ui-react";
import { BrowserView, MobileView } from "react-device-detect";
import { Events, scrollSpy } from "react-scroll";
import { fetchNews } from "../../actions/NewsActions";
import { Adsense } from "@ctrl/react-adsense";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./NewsCarousel.less";

class NewsCarousel extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      itemNo: 1,
      loop: true,
      lazyLoad: true,
      autoHeight: true,
    };

    this.renderNews = this.renderNews.bind(this);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchNews(Cookies.get("lang"));

    Events.scrollEvent.register("begin", () => {});
    Events.scrollEvent.register("end", () => {});

    scrollSpy.update();
  }

  componentWillUnmount() {
    Events.scrollEvent.remove("begin");
    Events.scrollEvent.remove("end");
  }

  scrollTo() {
    this.scroll.scrollTo(100);
  }

  renderNewsCarousel() {
    const newsItems = [];
    const { props } = this;
    const news = props.news.data;
    news.forEach((item) => {
      const pic = `build/images/news/${item.filename}`;
      const picName = pic.substring(0, pic.length - 4);
      const picp = `${picName}.webp`;
      // const pic1024w = `${picName}-1024w.jpg`;
      // const pic1024wp = `${picName}-1024w.webp`;

      const link = `/news/${item.id_news}`;
      const newsItem = (
        <div key={item.id_news} className="item">
          <div className="image-content">
            <picture>
              {/* <source
              className="owl-lazy"
              media="(max-width: 1024px)"
              data-srcset={pic1024wp}
              type="image/webp"
            />
            <source
              className="owl-lazy"
              media="(max-width: 1024px)"
              data-srcset={pic1024w}
            /> */}
              <source
                className="owl-lazy"
                data-srcset={picp}
                type="image/webp"
              />
              <img
                data-src={pic}
                alt={item.title}
                className="owl-lazy next-gp-banner-pic"
              />
            </picture>
          </div>
          <div className="text-content">
            <div>{item.title}</div>
            <p>{item.news}</p>
          </div>
          <div className="news-stats">
            <NavLink className="primary" to={link}>
              <FormattedMessage id={"app.button.details"} />
            </NavLink>
          </div>
        </div>
      );
      newsItems.push(newsItem);
    });
    return newsItems;
  }

  renderNews(count) {
    const newsItems = [];
    const { props } = this;
    const news = props.news.data;
    news.slice(1, count + 1).forEach((item) => {
      const pic = `build/images/news/${item.filename}`;
      const link = `/news/${item.id_news}`;
      const newsItem = (
        <Card key={item.id_news}>
          <div className="news-image-content">
            <NavLink to={link}>
              <Image src={pic} alt={item.title} />
            </NavLink>
          </div>
          <CardContent className="news-container">
            <CardHeader>{item.title}</CardHeader>
            <CardMeta>{item.news_date}</CardMeta>
            <CardDescription>
              {item.news.length > 400
                ? `${item.news.slice(0, 400)}...`
                : item.news}
            </CardDescription>
          </CardContent>
          <div className="buttons news-buttons">
            <NavLink className="primary" to={link}>
              <FormattedMessage id={"app.button.more"} />
            </NavLink>
          </div>
        </Card>
      );
      newsItems.push(newsItem);
    });
    return newsItems;
  }

  render() {
    const { props, state } = this;
    const { news } = props;
    const options = {
      items: state.itemNo,
      loop: state.loop,
      lazyLoad: state.lazyLoad,
    };
    if (!news.data || news.loading) {
      return (
        <section id="news-carousel">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    return (
      <section id="news-carousel">
        <OwlCarousel
          className="owl-theme"
          ref={(c) => {
            this.home = c;
          }}
          items={options.items}
          loop={options.loop}
          lazyLoad={options.lazyLoad}
        >
          {this.renderNewsCarousel()}
        </OwlCarousel>
        <BrowserView>
          <CardGroup itemsPerRow={3}>{this.renderNews(3)}</CardGroup>
        </BrowserView>
        <MobileView>
          <CardGroup itemsPerRow={2}>{this.renderNews(2)}</CardGroup>
        </MobileView>
        <div className="hideForMobile">
          <Adsense
            client="ca-pub-6611514323676676"
            slot="1654239932"
            className="adv-horizontal-slim"
          />
        </div>
        <div className="hideForDesktop">
          <Adsense
            client="ca-pub-6611514323676676"
            slot="1654239932"
            className="adv-horizontal-mobile"
          />
        </div>
      </section>
    );
  }
}

NewsCarousel.propTypes = {
  fetchNews: PropTypes.func.isRequired,
  news: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  year: PropTypes.string,
  id: PropTypes.string,
};

function mapStateToProps({ news }) {
  return { news };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchNews }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsCarousel);
