<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

$addEmpty=isset($_GET['empty']) ? $_GET['empty'] : null;
if ($addEmpty==null) $idaddEmpty=isset($_POST['empty']) ? $_POST['empty'] : null;
if ($addEmpty==null) $addEmpty=-1;
if ($addEmpty==false) $addEmpty=-1;

$driver=isset($_GET['driver']) ? $_GET['driver'] : null;
if ($driver==null) $driver=isset($_POST['driver']) ? $_POST['driver'] : null;
if ($driver==null) $driver=-1;
if ($driver=='-') $driver=-1;

$team=isset($_GET['team']) ? $_GET['team'] : null;
if ($team==null) $team=isset($_POST['team']) ? $_POST['team'] : null;
if ($team==null) $team=-1;
if ($team=='-') $team=-1;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;
if ($gp==null) $gp=-1;
if ($gp=='-') $gp=-1;

$circuit=isset($_GET['circuit']) ? $_GET['circuit'] : null;
if ($circuit==null) $circuit=isset($_POST['circuit']) ? $_POST['circuit'] : null;
if ($circuit==null) $circuit=-1;
if ($circuit=='-') $circuit=-1;

$engine=isset($_GET['engine']) ? $_GET['engine'] : null;
if ($engine==null) $engine=isset($_POST['engine']) ? $_POST['engine'] : null;
if ($engine==null) $engine=-1;
if ($engine=='-') $engine=-1;

$tyre=isset($_GET['tyre']) ? $_GET['tyre'] : null;
if ($tyre==null) $tyre=isset($_POST['tyre']) ? $_POST['tyre'] : null;
if ($tyre==null) $tyre=-1;
if ($tyre=='-') $tyre=-1;

$season=isset($_GET['season']) ? $_GET['season'] : null;
if ($season==null) $season=isset($_POST['season']) ? $_POST['season'] : null;
if ($season==null) $season=-1;
if ($season=='-') $season=-1;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// pobranie id
$query="SELECT id_driver id FROM drivers WHERE alias='$driver'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $driver = $r["id"];
}

// model
$query_select="SELECT teams_models.model value, CONCAT(REPLACE(teams_models.model,'_','/'),' (',teams_models.season,')') label FROM teams_models ";
$query_conditions="";
if ($driver!=-1 || $team!=-1 || $gp!=-1 || $circuit!=-1 || $engine!=-1 || $tyre!=-1 || $season!=-1) {
  $query_conditions.=" LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team_model=teams_models.id_team_model WHERE drivers_gp_involvements.id_driver>0";
}
if ($driver!=-1){
  $query_conditions.=" AND drivers_gp_involvements.id_driver=$driver";
}
if ($team!=-1){
  $query_conditions.=" AND drivers_gp_involvements.id_team IN (SELECT id_team id FROM teams WHERE alias_name='$team')";
}
if ($gp!=-1 && $circuit==-1){
  $query_conditions.=" AND drivers_gp_involvements.id_gp IN (SELECT id_gp id FROM gp WHERE name_alias='$gp')";
}
if ($circuit!=-1){
  $query_conditions.=" AND drivers_gp_involvements.id_gp IN (SELECT id_gp id FROM gp WHERE circuit_alias='$circuit')";
}
if ($engine!=-1){
  $query_conditions.=" AND drivers_gp_involvements.id_team IN (SELECT id_team id FROM teams WHERE engine='$engine' OR teams.name='$engine')";
}
if ($tyre!=-1){
  $query_conditions.=" AND drivers_gp_involvements.id_tyre=$tyre";
}
if ($season!=-1){
  $query_conditions.=" AND drivers_gp_involvements.season='$season'";
}
$query_order=" GROUP BY teams_models.model ORDER BY teams_models.season desc, teams_models.model";
$query="$query_select$query_conditions$query_order";

$result = mysqli_query($dbhandle,$query);
$models=array();

if ($addEmpty==1){
  if ($lang=='pl') {
    $emptyObj = (object) array(
      'value' => '',
      'label' => 'Wszystkie',
    );
  }else{
    $emptyObj = (object) array(
      'value' => '',
      'label' => 'All',
    );
  }
  $models[] = $emptyObj;
}

while($r = mysqli_fetch_assoc($result)) {
  $models[] = $r;
}
// Response
$response = $models;

print json_encode($response);
mysqli_free_result($result);
?>
