/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubgp","renderDriverSubgp"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Grid,
  Loader,
  Header,
  Divider,
  Statistic,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchCircuitsStats } from "../../actions/CircuitsStatsActions";
import { FormattedMessage } from "react-intl";
import styles from "./CircuitsStats.less";

class CircuitsStats extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderCircuitsStats = this.renderCircuitsStats.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchCircuitsStats(props.match.params.year);
  }

  renderCircuitsStats() {
    const items = [];
    const { props } = this;
    const { data } = props.circuitsStats;
    if (data) {
      data.forEach((item) => {
        const { id, alias, name, track, races, seasons } = item;
        const filename = `/build/images/circuits/circuit_${alias}_profile.jpg`;
        const detailsLink = `/circuit/${alias}`;
        const record = (
          <Grid.Column
            textAlign="center"
            key={id}
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column verticalAlign="top">
                  <NavLink to={detailsLink}>
                    <Image
                      centered
                      src={filename}
                      alt={name}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/circuits/circuit_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content>
                        {name}
                      </Header.Content>
                    </Header>
                    <Statistic size="large">
                      <Statistic.Value>{races}</Statistic.Value>
                      <Statistic.Label>
                        <FormattedMessage id={"app.stats.races"} />
                      </Statistic.Label>
                    </Statistic>
                    <Divider></Divider>
                    <Header as="h2">
                      <Header.Content>
                        <Header.Subheader>{seasons}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <div className="buttons">
                    <NavLink className="primary" to={detailsLink}>
                      <FormattedMessage id={"app.button.details"} />
                    </NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { circuitsStats } = this.props;
    if (!circuitsStats.data || circuitsStats.loading) {
      return (
        <section
          id="circuits-stats"
          name="circuits-stats"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }

    return (
      <section
        id="circuits-stats"
        name="circuits-stats"
        className="section-page"
      >
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.circuits.stats.list.title"} />}
          subtitle={
            <FormattedMessage id={"app.page.circuits.stats.list.subtitle"} />
          }
        />
        <div className="section-page-content">
          <Grid>{this.renderCircuitsStats()}</Grid>
        </div>
      </section>
    );
  }
}
CircuitsStats.propTypes = {
  fetchCircuitsStats: PropTypes.func.isRequired,
  circuitsStats: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      year: PropTypes.string,
    }).isRequired,
  }).isRequired,
  year: PropTypes.string,
};

function mapStateToProps({ circuitsStats }) {
  return { circuitsStats };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchCircuitsStats }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CircuitsStats);
