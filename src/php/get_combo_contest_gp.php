<?
header('Access-Control-Allow-Origin: *');

$season=isset($_GET['season']) ? $_GET['season'] : null;
if ($season==null) $season=isset($_POST['season']) ? $_POST['season'] : "pl";

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// pobranie id
$query="SELECT id_driver id FROM drivers WHERE alias='$driver'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $driver = $r["id"];
}

// gp
$query="SELECT name_alias value,";
if ($lang=='pl') {
  $query.="gp.name label ";
}else{
  $query.="gp.name_en as label ";
}  
$query.="FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp AND gp_season.season=$season ORDER BY sort";
$result = mysqli_query($dbhandle,$query);
$gps=array();
while($r = mysqli_fetch_assoc($result)) {
  $gps[] = $r;
}
//$gps["query"] = $query;
// Response
$response = $gps;

print json_encode($response);
mysqli_free_result($result);
?>
