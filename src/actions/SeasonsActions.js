// const request = require('../json/seasons.json');
//
// export const FETCH_SEASONS_SUCCESS = 'FETCH_SEASONS_SUCCESS';
//
// export function fetchSeasons() {
//   return {
//     type: FETCH_SEASONS_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_SEASONS_REQUEST = 'FETCH_SEASONS_REQUEST';
export const FETCH_SEASONS_SUCCESS = 'FETCH_SEASONS_SUCCESS';
export const FETCH_SEASONS_ERROR = 'FETCH_SEASONS_ERROR';

function requestData() {
  return {
    type: FETCH_SEASONS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_SEASONS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_SEASONS_ERROR,
    payload: error
  };
}

export function fetchSeasons() {
  const url = `${ROOT_URL}/get_seasons.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
