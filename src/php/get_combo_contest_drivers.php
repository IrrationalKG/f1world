<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

// kierowcy
$query="SELECT drivers.id_driver 'key',competition_drivers.short_name value, CONCAT(surname,' ',name) text
FROM competition_drivers,drivers
WHERE drivers.id_driver=competition_drivers.id_driver
ORDER BY surname";
$result = mysqli_query($dbhandle,$query);
$drivers=array();
while($r = mysqli_fetch_assoc($result)) {
  $drivers[] = $r;
}
//$drivers["query"] = $query;
// Response
$response = $drivers;

print json_encode($response);
mysqli_free_result($result);
?>
