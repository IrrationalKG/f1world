import {
  FETCH_COMBO_CIRCUITS_REQUEST,
  FETCH_COMBO_CIRCUITS_SUCCESS,
  FETCH_COMBO_CIRCUITS_ERROR,
} from "../actions/ComboCircuitsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_COMBO_CIRCUITS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_COMBO_CIRCUITS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_COMBO_CIRCUITS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
