<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

// pobranie id gp
$query="SELECT DISTINCT gp.id_gp FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp and gp.name_short='$id' AND gp_season.season='$year'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id_gp"];
}

//lata wyscigow
$query="select MAX(SUBSTRING(race_date,1,4)) maxYear,MIN(SUBSTRING(race_date,1,4)) minYear from drivers_gp_results where id_gp=$id";
$result = mysqli_query($dbhandle,$query);
$yearsInF1="Niesklasyfikowany";
while($r = mysqli_fetch_row($result)) {
   $yearsInF1 = $r[1]." - ".$r[0];
}

//liczba sezonow
$query="select COUNT(DISTINCT race_date) as race_no,circuit from gp,drivers_gp_results where gp.id_gp=drivers_gp_results.id_gp and gp.id_gp=$id group by gp.id_gp";
$result = mysqli_query($dbhandle,$query);
$gpNumber="0";
while($r = mysqli_fetch_row($result)) {
   $gpNumber = " (".$r[0]." Grand Prix)";
}

//ostatni zwyciezca
$query="select SUBSTRING(race_date,1,4) as raceyear, race_date,drivers.id_driver,CONCAT(drivers.name,' ',drivers.surname) driver,teams.team,teams.name as teamname,teams.engine from drivers_gp_results,drivers,teams where drivers_gp_results.id_driver=drivers.id_driver and drivers_gp_results.id_team=teams.id_team and id_gp=$id and race_pos=1 order by race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$lastWinner="-";
while($r = mysqli_fetch_row($result)) {
    $lastWinner = $r[1]." - ".$r[3]." (".$r[5].")";
}

//rekord toru
$query="select race_best_lap_time,race_date,drivers.id_driver,CONCAT(drivers.name,' ',drivers.surname) driver,teams.team,teams.name as teamname,teams.engine from drivers_gp_results,drivers,teams where drivers_gp_results.id_driver=drivers.id_driver and drivers_gp_results.id_team=teams.id_team and race_best_lap=1 and id_gp=$id order by race_best_lap_time LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$record="-";
while($r = mysqli_fetch_row($result)) {
  $record = $r[0]." - ".$r[3]." (".$r[5].")";
}

/**
* Statystyki
*/
$statsItems;

// starty na torze - kierowcy
$query="SELECT drivers.id_driver idDriver,drivers.surname name, drivers.alias,
drivers.country_code countryCode, COUNT(drivers_gp_results.race_pos) amount
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.id_gp='$id'
GROUP BY drivers_gp_results.id_driver
ORDER BY 5 DESC, 2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitStarts=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitStarts = $r;
}
$statsItems["starts"]=$driversCircuitStarts;

// zwyciestwa na torze - kierowcy
$query="SELECT drivers.id_driver idDriver,drivers.surname name, drivers.alias,
drivers.country_code countryCode, COUNT(drivers_gp_results.race_pos) amount
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.id_gp='$id' AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 5 DESC, 2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitWins=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitWins = $r;
}
$statsItems["wins"]=$driversCircuitWins;

// podium na torze - kierowcy
$query="SELECT drivers.id_driver idDriver,drivers.surname name, drivers.alias,
drivers.country_code countryCode, COUNT(drivers_gp_results.race_pos) amount
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.id_gp='$id'
AND drivers_gp_results.race_pos>0 AND drivers_gp_results.race_pos<4
GROUP BY drivers_gp_results.id_driver
ORDER BY 5 DESC, 2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPodium=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPodium = $r;
}
$statsItems["podium"]=$driversCircuitPodium;

// punkty na torze - kierowcy
$query="SELECT
drivers.id_driver idDriver,
drivers.surname name,
drivers.alias,
drivers.country_code countryCode,
SUM(COALESCE(drivers_gp_results.race_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) amount
FROM drivers
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_driver=drivers.id_driver AND drivers_gp_results.id_gp=$id AND excluded_from_class = 0
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_driver=drivers.id_driver AND drivers_sprint_results.id_gp=$id and drivers_sprint_results.season=drivers_gp_results.season
GROUP BY drivers.id_driver
ORDER BY 5 DESC,2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPoints=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPoints = $r;
}
$statsItems["points"]=$driversCircuitPoints;

// pole position na torze - kierowcy
$query="SELECT drivers.id_driver idDriver,drivers.surname name, drivers.alias,
drivers.country_code countryCode, COUNT(drivers_pp_results.qual_pos) amount
FROM drivers_pp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_pp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_pp_results.id_gp
WHERE gp.id_gp='$id'
AND drivers_pp_results.qual_pos=1
GROUP BY drivers_pp_results.id_driver
ORDER BY 5 DESC, 2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitPolepos=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitPolepos = $r;
}
$statsItems["polepos"]=$driversCircuitPolepos;

// naj okrążenie na torze - kierowcy
$query="SELECT drivers.id_driver idDriver,drivers.surname name, drivers.alias,
drivers.country_code countryCode, COUNT(drivers_gp_results.race_best_lap) amount
FROM drivers_gp_results
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver
LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
WHERE gp.id_gp='$id'
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 5 DESC, 2
LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$driversCircuitBestlaps=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversCircuitBestlaps = $r;
}
$statsItems["bestlaps"]=$driversCircuitBestlaps;

$query="SELECT gp.circuit_alias alias,gp.id_gp id,";
if ($lang=='pl') {
  $query.="gp.name,";
}else{
  $query.="gp.name_en as name,";
}  
$query.="gp.name_alias gpAlias, lower(gp.name_short) country,gp.country_code countryCode,circuit,
track,
CONCAT('circuit_',LOWER(gp.name_short),'_bg.jpg') circuitPhoto,
CONCAT('circuit_',LOWER(gp.name_short),'_map.jpg') trackPhoto,
'' yearsInF1,'' lastWinner,'' record, '' stats,
SUBSTRING(gp_season.date,1,16) date, gp_season.sort round
FROM gp 
LEFT JOIN gp_season on gp.id_gp=gp_season.id_gp where gp_season.id_gp='$id' and gp_season.season='$year'";
$result = mysqli_query($dbhandle,$query);
$gpItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["yearsInF1"]=$yearsInF1.$gpNumber;
   $r["lastWinner"]=$lastWinner;
   $r["record"]=$record;
   $r["stats"]=$statsItems;
   $gpItems = $r;
}

//$gpItems["stats"]=$statsItems;

// Response
$response = $gpItems;


print json_encode($response);
mysqli_free_result($result);
?>
