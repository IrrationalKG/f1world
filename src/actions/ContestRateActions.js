// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const SAVE_CONTEST_RATE = 'SAVE_CONTEST_RATE';
export const SAVE_CONTEST_RATE_SUCCESS = 'SAVE_CONTEST_RATE_SUCCESS';
export const SAVE_CONTEST_RATE_ERROR = 'SAVE_CONTEST_RATE_ERROR';

function requestData() {
  return {
    type: SAVE_CONTEST_RATE
  };
}

function receiveData(data) {
  return {
    type: SAVE_CONTEST_RATE_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: SAVE_CONTEST_RATE_ERROR,
    payload: error
  };
}

export function saveContestRate(data) {
  const url = `${ROOT_URL}/post_contest_rate.php`;
  const rate = JSON.stringify(data);
  return (dispatch) => {
    dispatch(requestData());
    axios.post(url, rate, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
