/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderAwards"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Header,
  Segment,
  Item,
  Icon,
  Popup,
  Loader,
  Statistic,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchContest } from "../../actions/ContestActions";
import { fetchContestAwards } from "../../actions/ContestAwardsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import styles from "./Contest.less";

class Contest extends Component {
  componentDidMount() {
    const { props } = this;
    props.fetchContest(props.year, props.gp);
    props.fetchContestAwards();
  }

  renderAwards(awardsContent, type) {
    const { formatMessage } = this.props.intl;
    const awardsItems = [];
    if (awardsContent) {
      awardsContent.forEach((award) => {
        let content = `${award.place}. ${formatMessage({
          id: "app.page.home.contest.season.place",
        })} ${award.season}`;

        if (type === "gp") {
          content = `${award.place}. ${formatMessage({
            id: "app.page.home.contest.season.place",
          })} ${award.season} (GP)`;
        }
        let awardColor = "";
        switch (award.place) {
          case "1":
            awardColor = "yellow";
            break;
          case "2":
            awardColor = "grey";
            break;
          case "3":
            awardColor = "brown";
            break;
          default:
            return "";
        }
        let awardItem;
        const key = `${type}_${award.season}_${award.place}`;
        if (award.season === "-1") {
          content = `${award.place}. ${formatMessage({
            id: "app.page.home.contest.season.alltime",
          })}`;
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="trophy" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        } else {
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="star" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        }
        awardsItems.push(awardItem);
        return awardsItems;
      });
    }
    return awardsItems;
  }

  renderAwardsNormal(item) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[0];
      const awardsContent = data[item.id];
      return this.renderAwards(awardsContent, "normal");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderAwardsGP(item) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[1];
      const awardsContent = data[item.id];
      return this.renderAwards(awardsContent, "gp");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderContestResult() {
    const { props } = this;
    const { formatMessage } = this.props.intl;

    if (props.contest.data.length > 0) {
      const resultsItems = [];
      const data = props.contest.data[0];
      let place = 0;
      data.items.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}/${data.season}`;
        const resultItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {place}
                  {". "}
                  {item.name} {item.surname}
                </NavLink>{" "}
                {this.renderAwardsNormal(item)}
              </Item.Header>
              <Item.Extra>
                {formatMessage({
                  id: "app.stats.season.short",
                })}
                {": "}
                {item.seasons}
                {", P1: "}
                {item.first}
                {", P2: "}
                {item.second}
                {", P3: "}
                {item.third}
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right">
                <Statistic.Value>
                  {item.points}{" "}
                  <small className="very">
                    {" ("}
                    {item.pointsgp}
                    {")"}
                  </small>
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        resultsItems.push(resultItem);
      });
      return resultsItems;
    }
    return "";
  }

  renderContestClass() {
    const { props } = this;
    if (props.contest.data.length > 0) {
      const classItems = [];
      const data = props.contest.data[1];
      let place = 0;
      data.items.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}/${data.season}`;
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {place}
                  {". "}
                  {item.name} {item.surname}
                </NavLink>{" "}
                {this.renderAwardsNormal(item)}
              </Item.Header>
              <Item.Extra>
                {" P1: "}
                {item.first}
                {", P2: "}
                {item.second}
                {", P3: "}
                {item.third}
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right">
                <Statistic.Value>{item.points}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderContestClassGP() {
    const { props } = this;
    if (props.contest.data.length > 0) {
      const classGPItems = [];
      const data = props.contest.data[2];
      let place = 0;
      data.items.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}/${data.season}`;
        const classGPItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {place}
                  {". "}
                  {item.name} {item.surname}
                </NavLink>{" "}
                {this.renderAwardsGP(item)}
              </Item.Header>
              <Item.Extra>
                {" P1: "}
                {item.first}
                {", P2: "}
                {item.second}
                {", P3: "}
                {item.third}
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right">
                <Statistic.Value>{item.points}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classGPItems.push(classGPItem);
      });
      return classGPItems;
    }
    return "";
  }

  render() {
    const { props } = this;
    if (!props.contest.data || props.contest.data.length === 0) {
      return (
        <section id="contest" name="contest" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const pic = "build/images/contest/contest_banner.jpg";
    const picName = pic.substring(0, pic.length - 4);
    const picp = `${picName}.webp`;
    const pic1024w = `${picName}-1024w.jpg`;
    const pic1024wp = `${picName}-1024w.webp`;
    const linkClassNormal = `/contest/class/${props.year}`;
    const linkClassGP = `/contest/class-gp/${props.year}`;
    const linkGPRateResults = `/contest/gprate/${props.year}/${props.lastGPAlias}`;
    return (
      <section id="contest" name="contest">
        <div>
          <div className="contest-header">
            <h2>
              <FormattedMessage id={"app.page.home.contest.title"} />
            </h2>
            <h3>
              <FormattedMessage id={"app.stats.season"} /> {props.year}
            </h3>
          </div>
          <div className="contest-banner">
            <picture>
              {/* <source
                media="(max-width: 1024px)"
                srcSet={pic1024wp}
                type="image/webp"
              />
              <source media="(max-width: 1024px)" srcSet={pic1024w} /> */}
              <source srcSet={picp} type="image/webp" />
              <img
                src={pic}
                alt="Konkurs typowania"
                className="contest-banner-pic"
              />
            </picture>
          </div>
        </div>
        <div className="contest-content">
          <Grid stackable centered columns="equal">
            <Grid.Row columns={3} only="computer mobile">
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-results-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.results"} />
                      <Header.Subheader>
                        {props.contest.data.length > 0 &&
                          props.contest.data[0].gpname}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-results-container"
                  >
                    {this.renderContestResult()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkGPRateResults}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-class-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.class"} />
                      <Header.Subheader>
                        <FormattedMessage
                          id={"app.page.home.contest.class.standard"}
                        />
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-class-container"
                  >
                    {this.renderContestClass()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkClassNormal}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-class-gp-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.class"} />
                      <Header.Subheader>
                        <FormattedMessage
                          id={"app.page.home.contest.class.gp"}
                        />
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-class-gp-container"
                  >
                    {this.renderContestClassGP()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkClassGP}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1} only="tablet">
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-results-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.results"} />
                      <Header.Subheader>
                        {props.contest.data.length > 0 &&
                          props.contest.data[0].gpname}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-results-container"
                  >
                    {this.renderContestResult()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkGPRateResults}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-class-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.class"} />
                      <Header.Subheader>
                        <FormattedMessage
                          id={"app.page.home.contest.class.standard"}
                        />
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-class-container"
                  >
                    {this.renderContestClass()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkClassNormal}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment basic>
                  <Header
                    size="large"
                    textAlign="center"
                    className="contest-class-gp-header"
                    inverted
                  >
                    <Header.Content>
                      <FormattedMessage id={"app.page.home.contest.class"} />
                      <Header.Subheader>
                        <FormattedMessage
                          id={"app.page.home.contest.class.gp"}
                        />
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Item.Group
                    divided
                    unstackable
                    className="contest-class-gp-container"
                  >
                    {this.renderContestClassGP()}
                    <div className="buttons">
                      <NavLink className="primary" to={linkClassGP}>
                        <FormattedMessage id={"app.button.details"} />
                      </NavLink>
                    </div>
                  </Item.Group>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </section>
    );
  }
}

Contest.propTypes = {
  fetchContest: PropTypes.func.isRequired,
  fetchContestAwards: PropTypes.func.isRequired,
  contest: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.object])
  ),
  year: PropTypes.string,
  gp: PropTypes.string,
  lastGPAlias: PropTypes.string,
  nextGPAlias: PropTypes.string,
  awards: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.object])
  ),
  round: PropTypes.string,
};

function mapStateToProps({ contest, awards }) {
  return {
    contest,
    awards,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchContest, fetchContestAwards }, dispatch);
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(Contest)
);
