import {
  FETCH_GPSEASON_TEAMS_REQUEST,
  FETCH_GPSEASON_TEAMS_SUCCESS,
  FETCH_GPSEASON_TEAMS_ERROR,
} from "../actions/GPSeasonTeamsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_GPSEASON_TEAMS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_GPSEASON_TEAMS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_GPSEASON_TEAMS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
