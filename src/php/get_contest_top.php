<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$query="SELECT id_competition_history id, users.alias, competition_history.user as name, `1999` s1999,`2000` s2000,`2001` s2001,`2002` s2002,`2003` s2003,`2004` s2004,`2005` s2005,`2006` s2006,`2007` s2007,`2008` s2008,`2009` s2009,`2010` s2010,`2011` s2011,`2012` s2012,`2013` s2013,`2014` s2014,`2015` s2015,`2016` s2016,`2017` s2017,`2018` s2018,`2019` s2019,`2020` s2020,`2021` s2021,`2022` s2022,`2023` s2023,`2024` s2024, competition_history.total points, competition_history.place FROM competition_history,users WHERE competition_history.id_user=users.id_user ORDER BY competition_history.place,competition_history.user LIMIT 100";
$result = mysqli_query($dbhandle,$query);
$recordItems=array();
while($r = mysqli_fetch_assoc($result)) {
    $recordItems[] = $r;
}
$recordsItems["users"]=$recordItems;
$recordsItems["query"]=$query;
$recordsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $recordsItems;

print json_encode($response);
mysqli_free_result($result);
?>
