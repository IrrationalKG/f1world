/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { Link } from "react-router-dom";
import "smartmenus";
import { FormattedMessage } from "react-intl";
import styles from "./Logo.less";

export default class Logo extends Component {
  render() {
    return (
      <section id="logo">
        <Link to="/">
          <div>
            <span id="logo-left">F1</span>
            <span id="logo-center">WORLD</span>
            <span id="logo-right">2025</span>
          </div>
        </Link>
        <h1>
          <FormattedMessage id="app.subtitle" />
        </h1>
      </section>
    );
  }
}
