<?
header('Access-Control-Allow-Origin: *');

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$gp=isset($_GET['gp']) ? $_GET['gp'] : null;
if ($gp==null) $gp=isset($_POST['gp']) ? $_POST['gp'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// pobranie id gp
$query="SELECT DISTINCT gp.id_gp,gp.name_short,name_alias alias,";
if ($lang=='pl') {
  $query.="gp.name ";
}else{
  $query.="gp.name_en as name ";
}  
$query.="FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp and gp.name_alias='$gp'
AND gp_season.season='$year'";
$result = mysqli_query($dbhandle,$query);
$id;
$name;
$alias;
$nameShort;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id_gp"];
  $nameShort = $r["name_short"];
  $name = $r["name"];
  $alias = $r["alias"];
}

// pobranie pierwszego gp z sezonu
// if ($id == null || $id == ''){
//   $query="SELECT DISTINCT gp.id_gp,gp.name_short,name_alias alias,name FROM gp,gp_season WHERE gp.id_gp=gp_season.id_gp AND gp_season.season='$year' ORDER BY sort LIMIT 1";
//   $result = mysqli_query($dbhandle,$query);
//   while($r = mysqli_fetch_assoc($result)) {
//     $id = $r["id_gp"];
//     $nameShort = $r["name_short"];
//     $name = $r["name"];
//     $alias = $r["alias"];
//   }
// }


// min pozycja
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp AND season=$year-1 order by sort desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp AND season=$year+1 order by sort asc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzednia gp
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
AND sort=(SELECT sort FROM gp_season WHERE id_gp='$id' AND season=$year)-1 AND season=$year";
$result = mysqli_query($dbhandle,$query);
$prevId=$min;
$prevYear = strval($year-1);
while($r = mysqli_fetch_assoc($result)) {
    $prevId = $r["alias"];
    $prevYear = $year;
}

// nastepna gp
$query="SELECT name_alias alias,sort FROM gp_season,gp WHERE gp_season.id_gp=gp.id_gp
AND sort=(SELECT sort FROM gp_season WHERE id_gp='$id' AND season=$year)+1 AND season=$year";
$result = mysqli_query($dbhandle,$query);
$nextId = $max;
$nextYear = strval($year+1);
while($r = mysqli_fetch_assoc($result)) {
  $nextId = $r["alias"];
  $nextYear = $year;
}

// klasyfikacja
$query="SELECT place, uczestnik id, CONCAT(name,' ',surname) name, alias, pp, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, pkt points, premia bonus, suma sum,send_date sendDate
FROM typy,users WHERE users.id_user=typy.uczestnik AND sezon= '$year' AND gp='$nameShort' AND is_deleted=0
GROUP BY uczestnik,name,surname ORDER BY place,sendDate";
$result = mysqli_query($dbhandle,$query);
$ratesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $ratesItems[] = $r;
}

// wyniki gp
$query="SELECT pp, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10 FROM gp_winners WHERE gp='$nameShort' AND sezon='$year'";
$result = mysqli_query($dbhandle,$query);
$gpResults;
while($r = mysqli_fetch_assoc($result)) {
  $gpResults = $r;
}

$rates["season"]=$year;
$rates["name"]=$name;
$rates["alias"]=$alias;
$rates["prevId"]=$prevId;
$rates["prevYear"]=$prevYear;
$rates["nextId"]=$nextId;
$rates["nextYear"]=$nextYear;
$rates["rates"]=$ratesItems;
$rates["gpResults"]=$gpResults;
$rates["createTime"]=microtime(true)-$start_time;

// Response
$response = $rates;

print json_encode($response);
mysqli_free_result($result);
?>
