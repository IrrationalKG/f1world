// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_DRIVERS_RECORDS_REQUEST = "FETCH_DRIVERS_RECORDS_REQUEST";
export const FETCH_DRIVERS_RECORDS_SUCCESS = "FETCH_DRIVERS_RECORDS_SUCCESS";
export const FETCH_DRIVERS_RECORDS_ERROR = "FETCH_DRIVERS_RECORDS_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVERS_RECORDS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVERS_RECORDS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVERS_RECORDS_ERROR,
    payload: error,
  };
}

export function fetchDriversRecords(
  page,
  stats,
  pos,
  team,
  gp,
  circuit,
  beginYear,
  endYear,
  status,
  lang
) {
  const url = `${ROOT_URL}/get_drivers_records.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          page: page,
          stats: stats,
          pos: pos,
          team: team,
          gp: gp,
          circuit: circuit,
          beginYear: beginYear,
          endYear: endYear,
          status: status,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
