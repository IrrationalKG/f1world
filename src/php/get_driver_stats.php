<?
header('Access-Control-Allow-Origin: *');

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";
include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// aktualny rok
$query="SELECT max(season) maxYear FROM drivers_class";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}
// $prevYear=$currentYear-1;

// pobranie kolejnosci kierowcow
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc";
}else{
  $query="SELECT alias FROM drivers order by surname asc";
}
$result = mysqli_query($dbhandle,$query);
$driversTab = array();
$cnt=0;
$currentDriverCnt=0;
$maxDriverCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $driversTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentDriverCnt = $cnt;
  }
  $cnt+=1;
}
$maxDriverCnt = $cnt;

// min pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc LIMIT 1";
}else{
  $query="SELECT alias FROM drivers order by surname asc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname desc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias FROM drivers order by surname desc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzedni kierowca
if (($currentDriverCnt-1)<0){
  $prevId=$max;
}else{
  $prevId = $driversTab[$currentDriverCnt-1];
}
// nastepny kierowca
if (($currentDriverCnt+1)>=$maxDriverCnt){
  $nextId=$min;
}else{
  $nextId = $driversTab[$currentDriverCnt+1];
}

// pobranie id
$query="SELECT id_driver id FROM drivers WHERE alias='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
}

// zespoły
$driversTeamsTabM = array();
$driversTeamsShortTabM = array();
if ($year) {
  $query="SELECT id_driver,teams.name,teams.team FROM drivers_class,teams WHERE drivers_class.id_team=teams.id_team
  AND drivers_class.id_driver='$id' ORDER BY drivers_class.season desc LIMIT 1";
  $resultM = mysqli_query($dbhandle,$query);
  while($r = mysqli_fetch_assoc($resultM)) {
    $tmp_id_driver = $r["id_driver"];
    $driversTeamsTabM[$tmp_id_driver] = $r["name"];
    $driversTeamsShortTabM[$tmp_id_driver] = $r["team"];
  }
}

// informacje o kierowcy
$query="SELECT alias,id_driver id,CONCAT(name,' ',surname) name, number, team_color teamColor, born_date bornDate, born_place bornPlace,";
if ($lang=='pl') {
  $query.="country";
}else{
  $query.="country_en as country";
}
$query.=", country_code countryCode, lower(country_short) countryShort, height, weight, helmet, internet, picture, banner, info, '' team,'' prevId, '' nextId 
FROM drivers WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
$driverItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["team"]=$driversTeamsTabM[$r["id"]];
   $r["teamShort"]=$driversTeamsShortTabM[$r["id"]];
   $r["prevId"]=$prevId;
   $r["nextId"]=$nextId;
   $driverItems = $r;
}

/**
* Statystyki
**/
$statsItems;

//sezony
$query="SELECT COUNT(DISTINCT drivers_gp_involvements.season) seasons FROM drivers_gp_involvements WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["seasons"] = $r["seasons"];
}

//liczba grand prix
$query="SELECT COUNT(DISTINCT CONCAT(SEASON,id_gp)) gp FROM drivers_gp_involvements WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["gp"] = $r["gp"];
}

//liczba kwalifikacji
$query="SELECT COUNT(DISTINCT qual_date) qual FROM drivers_pp_results WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["qual"] = $r["qual"];
}

//liczba wyscigow
$query="SELECT COUNT(DISTINCT race_date) starts FROM drivers_gp_results WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["starts"] = $r["starts"];
}

//liczba sprintow
$query="SELECT COUNT(DISTINCT sprint_date) sprints FROM drivers_sprint_results WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["sprints"] = $r["sprints"];
}

//zwyciestwa
$query="SELECT COUNT(DISTINCT race_date) wins FROM drivers_gp_results WHERE id_driver=$id AND race_pos=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["wins"] = $r["wins"];
}

//punkty
$query="SELECT COALESCE(SUM(points),0) points FROM drivers_class WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["points"] = $r["points"];
}

//podium
$query="SELECT COUNT(DISTINCT race_date) podium FROM drivers_gp_results WHERE id_driver=$id AND race_pos<4";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["podium"] = $r["podium"];
}

//pole positions
$query="SELECT COUNT(DISTINCT id_starting_grid) polepos FROM drivers_gp_starting_grid WHERE id_driver=$id AND is_pp=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["polepos"] = $r["polepos"];
}

//najlepsze okrazenia
$query="SELECT COUNT(DISTINCT race_date) bestlaps FROM drivers_gp_results WHERE id_driver=$id AND race_best_lap=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["bestlaps"] = $r["bestlaps"];
}

//z punktami
$query="SELECT COUNT(DISTINCT race_date) pointPlaces FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
WHERE drivers_gp_involvements.id_driver=$id AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0)";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["pointPlaces"] = $r["pointPlaces"];
}

//bez punktow
$query="SELECT COUNT(DISTINCT race_date) noPointPlaces FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
WHERE drivers_gp_involvements.id_driver=$id AND (drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL))";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["noPointPlaces"] = $r["noPointPlaces"];
}

//niezakwalifikowany
$query="SELECT COUNT(DISTINCT qual_date) notQualified FROM drivers_pp_results WHERE id_driver=$id AND not_qualified=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["notQualified"] = $r["notQualified"];
}

//nie wystartowal
$query="SELECT COUNT(DISTINCT qual_date) notStarted FROM drivers_pp_results WHERE id_driver=$id AND not_started=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["notStarted"] = $r["notStarted"];
}

//z kara
$query="SELECT COUNT(DISTINCT race_date) penalty FROM drivers_gp_results WHERE id_driver=$id AND penalty=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["penalty"] = $r["penalty"];
}

//sklasyfikowany
$query="SELECT COUNT(DISTINCT race_date) completed FROM drivers_gp_results WHERE id_driver=$id AND race_completed=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["completed"] = $r["completed"];
}

//niesklasyfikowany
$query="SELECT COUNT(DISTINCT race_date) incomplete FROM drivers_gp_results WHERE id_driver=$id AND race_completed=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["incomplete"] = $r["incomplete"];
}

//wyscigi ukonczone
$query="SELECT COUNT(DISTINCT race_date) finished FROM drivers_gp_results WHERE id_driver=$id AND race_finished=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["finished"] = $r["finished"];
}

//wys nieukonczone
$query="SELECT COUNT(DISTINCT race_date) retirement FROM drivers_gp_results WHERE id_driver=$id  AND race_finished=0 AND drivers_gp_results.disq=0";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["retirement"] = $r["retirement"];
}

//dyskwalifikacje
$query="SELECT COUNT(DISTINCT race_date) disq FROM drivers_gp_results WHERE id_driver=$id AND disq=1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["disq"] = $r["disq"];
}

//liczba pol startowych
$query="SELECT COUNT(DISTINCT grid_pos) grid FROM drivers_gp_starting_grid WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["grid"] = $r["grid"];
}

$driverItems["stats"]=$statsItems;

/**
* Ranking - miejsca
**/
$rankingItems;

//sezony
$query="SELECT COUNT(DISTINCT drivers_gp_involvements.season) seasons, drivers_gp_involvements.id_driver FROM drivers_gp_involvements,drivers WHERE  drivers_gp_involvements.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["seasons"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba grand prix
$query="SELECT COUNT(DISTINCT CONCAT(SEASON,id_gp)) gp, drivers.id_driver,surname FROM drivers_gp_involvements,drivers WHERE drivers_gp_involvements.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["gp"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba kwalifikacji
$query="SELECT COUNT(DISTINCT qual_date) qual, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["qual"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba wyscigow
$query="SELECT COUNT(DISTINCT race_date) starts, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["starts"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba sprintow
$query="SELECT COUNT(DISTINCT sprint_date) sprints, drivers.id_driver,surname FROM drivers_sprint_results,drivers WHERE drivers_sprint_results.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["sprints"] = $pos;
    break;
  }
  $pos+=1;
}

//zwyciestwa
$query="SELECT COUNT(DISTINCT race_date) wins, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["wins"] = $pos;
    break;
  }
  $pos+=1;
}

//punkty
$query="SELECT SUM(points) points, drivers.id_driver,surname FROM drivers_class,drivers WHERE drivers_class.id_driver=drivers.id_driver group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["points"] = $pos;
    break;
  }
  $pos+=1;
}

//podium
$query="SELECT COUNT(DISTINCT race_date) podium, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_pos>0 AND race_pos<4 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["podium"] = $pos;
    break;
  }
  $pos+=1;
}

//pole positions
$query="SELECT COUNT(DISTINCT id_starting_grid) polepos, drivers.id_driver,surname FROM drivers_gp_starting_grid,drivers WHERE drivers_gp_starting_grid.id_driver=drivers.id_driver AND is_pp=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["polepos"] = $pos;
    break;
  }
  $pos+=1;
}

//najlepsze okrazenia
$query="SELECT COUNT(DISTINCT race_date) bestlaps, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_best_lap=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["bestlaps"] = $pos;
    break;
  }
  $pos+=1;
}

//wyscigi z punktami
$query="SELECT COUNT(DISTINCT race_date) pointPlaces, drivers.id_driver,surname 
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
WHERE drivers_gp_results.id_driver=drivers.id_driver AND (drivers_gp_results.race_points>0 OR drivers_sprint_results.sprint_points>0) group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["pointPlaces"] = $pos;
    break;
  }
  $pos+=1;
}

//wyscigi bez punktow
$query="SELECT COUNT(DISTINCT race_date) noPointPlaces, drivers.id_driver,surname 
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
WHERE drivers_gp_results.id_driver=drivers.id_driver AND (drivers_gp_results.race_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["noPointPlaces"] = $pos;
    break;
  }
  $pos+=1;
}

//nie zakwalifikowany
$query="SELECT COUNT(DISTINCT qual_date) notQualified, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver AND not_qualified=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["notQualified"] = $pos;
    break;
  }
  $pos+=1;
}

//nie wystartowal
$query="SELECT COUNT(DISTINCT qual_date) notStarted, drivers.id_driver,surname FROM drivers_pp_results,drivers WHERE drivers_pp_results.id_driver=drivers.id_driver AND not_started=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["notStarted"] = $pos;
    break;
  }
  $pos+=1;
}

//z kara
$query="SELECT COUNT(DISTINCT race_date) penalty, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND penalty=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["penalty"] = $pos;
    break;
  }
  $pos+=1;
}

//sklasyfikowany
$query="SELECT COUNT(DISTINCT race_date) completed, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["completed"] = $pos;
    break;
  }
  $pos+=1;
}

//niesklasyfikowany
$query="SELECT COUNT(DISTINCT race_date) incomplete, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_completed=0 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["incomplete"] = $pos;
    break;
  }
  $pos+=1;
}

//ukonczone
$query="SELECT COUNT(DISTINCT race_date) finished, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_finished=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["finished"] = $pos;
    break;
  }
  $pos+=1;
}

//nieukonczone
$query="SELECT COUNT(DISTINCT race_date) retirement, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND race_finished=0  AND drivers_gp_results.disq=0 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["retirement"] = $pos;
    break;
  }
  $pos+=1;
}

//dyskwalifikacje
$query="SELECT COUNT(DISTINCT race_date) disq, drivers.id_driver,surname FROM drivers_gp_results,drivers WHERE drivers_gp_results.id_driver=drivers.id_driver AND disq=1 group by drivers.id_driver order by 1 desc, surname";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["id_driver"] === $id){
    $rankingItems["disq"] = $pos;
    break;
  }
  $pos+=1;
}

$driverItems["ranking"]=$rankingItems;

/**
* Kariera
**/
$careerItems;

//sezony
$query="SELECT MAX(season) maxYear,MIN(season) minYear, (MAX(season) - MIN(season) + 1) years FROM drivers_gp_involvements WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["seasons"] = $r["minYear"]." - ".$r["maxYear"];
}

//najlepszy wynik
$query="SELECT count(place) amount, MIN(place) as bestResult, GROUP_CONCAT(DISTINCT season ORDER BY season DESC SEPARATOR ', ') seasons,
MAX(season) bestResultSeason
FROM drivers_class where id_driver=$id and season<=$currentYear and is_classified=1 group by place LIMIT 1";
$resultM = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($resultM)) {

  if ($r["bestResult"] === '1'){
    // $careerItems["bestResult"] = "MŚ (".$r["amount"]."x)";
    $careerItems["bestResult"] = "MŚ";
    $careerItems["champCount"] = $r["amount"];
  }else {
    $careerItems["bestResult"] = $r["bestResult"].".";
    $careerItems["bestResultSeason"] = $r["bestResultSeason"];
  }
}

//zespoly w ktorych startowal aktualny kierowca
$query="SELECT distinct teams.alias_name alias,teams.id_team id,CONCAT(teams.name,' ',coalesce(teams.engine,'')) name
FROM drivers_gp_involvements,teams WHERE drivers_gp_involvements.id_team=teams.id_team AND id_driver=$id order by name";
$result = mysqli_query($dbhandle,$query);
$allTeams = array();
while($r = mysqli_fetch_assoc($result)) {
  $allTeams[] = $r;
}
$careerItems["allTeams"] = $allTeams;

/**
* Sezony
**/
//gp w sezonach
$query="SELECT drivers_gp_involvements.season,count(DISTINCT id_gp) gp from drivers_gp_involvements where id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversGPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversGPTab[$tmp_season] = $r["gp"];
}
//wyscigi w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) starts from drivers_gp_results where id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversStartsTab[$tmp_season] = $r["starts"];
}
//kwalifikacje w sezonach
$query="SELECT drivers_pp_results.season,count(id_drivers_pp) qual from drivers_pp_results where id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversQualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversQualTab[$tmp_season] = $r["qual"];
}
//zwyciestwa w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) wins from drivers_gp_results where race_pos=1 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversWinsTab[$tmp_season] = $r["wins"];
}
//drugie miejsca w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) second from drivers_gp_results where race_pos=2 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversSecondTab[$tmp_season] = $r["second"];
}
//trzecie miejsca w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) third from drivers_gp_results where race_pos=3 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversThirdTab[$tmp_season] = $r["third"];
}
//podium w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) podiums from drivers_gp_results where race_pos<4 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversPodiumsTab[$tmp_season] = $r["podiums"];
}
//pole position w sezonach
$query="SELECT drivers_gp_starting_grid.season season,count(id_starting_grid) pp from drivers_gp_starting_grid where is_pp=1 and id_driver=$id group by 1 order by 1;";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversPolePosTab[$tmp_season] = $r["pp"];
}
//best laps w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) bestlaps from drivers_gp_results where race_best_lap=1 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversBLTab[$tmp_season] = $r["bestlaps"];
}
//gp z punktami w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) pointsPlaces from drivers_gp_results where race_points>0  and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversPointsPlacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversPointsPlacesTab[$tmp_season] = $r["pointsPlaces"];
}
//ukonczone gp w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) completed from drivers_gp_results where race_completed=1  and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversCompletedTab[$tmp_season] = $r["completed"];
}
//nieukonczone gp w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) incomplete from drivers_gp_results where race_completed=0  and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversIncompleteTab[$tmp_season] = $r["incomplete"];
}
//dyskwalifikacje w sezonach
$query="SELECT drivers_gp_results.season,count(DISTINCT race_date) disq from drivers_gp_results where disq=1 and id_driver=$id group by 1 order by 1";
$result = mysqli_query($dbhandle,$query);
$driversDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversDisqTab[$tmp_season] = $r["disq"];
}
//zespoły w sezonach (nazwa)
$query="SELECT drivers_gp_involvements.season,GROUP_CONCAT(distinct CONCAT(teams.name,' ',COALESCE(teams.engine,'')) SEPARATOR ', ') teamsNames from drivers_gp_involvements,teams where id_driver=$id and drivers_gp_involvements.id_team=teams.id_team group by 1";
$result = mysqli_query($dbhandle,$query);
$driversTeamsNamesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversTeamsNamesTab[$tmp_season] = $r["teamsNames"];
}
//zespoły w sezonach (alias)
$query="SELECT drivers_gp_involvements.season,GROUP_CONCAT(distinct teams.alias_name SEPARATOR ', ') teamsAliases from drivers_gp_involvements,teams where id_driver=$id and drivers_gp_involvements.id_team=teams.id_team group by 1";
$result = mysqli_query($dbhandle,$query);
$driversTeamsAliasesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversTeamsAliasesTab[$tmp_season] = $r["teamsAliases"];
}
//zespoły w sezonach (team)
$query="SELECT drivers_gp_involvements.season,GROUP_CONCAT(distinct lower(teams.team) SEPARATOR ', ') teams from drivers_gp_involvements,teams where id_driver=$id and drivers_gp_involvements.id_team=teams.id_team group by 1";
$result = mysqli_query($dbhandle,$query);
$driversTeamsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $driversTeamsTab[$tmp_season] = $r["teams"];
}

//bolidy w sezonach
$query="SELECT DISTINCT drivers_gp_involvements.season,
lower(teams.team) teams,
teams.alias_name teamsAliases,
CONCAT(teams.name,' ',COALESCE(teams.engine,'')) teamsNames,
teams.engine,
model teamsModelNames,    
teams_models.id_team_model idTeamModel,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamName,
lower(teams.country_code) countryCode    
from drivers_gp_involvements,teams,teams_models 
where drivers_gp_involvements.id_driver='$id' 
and drivers_gp_involvements.id_team=teams.id_team
and teams_models.id_team_model=drivers_gp_involvements.id_team_model 
GROUP BY drivers_gp_involvements.season, teams_models.model
order by 1,3";
$result = mysqli_query($dbhandle,$query);
$driversCarModelsTab = array();
$driversCarModelsTabSubTab = array();
$currSeason = "";
$teamsCarModelsSubTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  if ($currSeason=="") {
    $currSeason==$tmp_season;
  }
  
  $item = array();    
  $item["team"] = $r["teams"];
  $item["engine"] = $r["engine"];
  $item["model"] = $r["teamsModelNames"];
  $item["alias"] = $r["teamsAliases"];
  $item["name"] = $r["teamsNames"];
  $item["idTeamModel"] = $r["idTeamModel"];
  $item["teamName"] = $r["teamName"];
  $item["countryCode"] = $r["countryCode"];
  if ($currSeason==$tmp_season) {
    $driversCarModelsTabSubTab[]=$item;
  }else{
    $currSeason=$tmp_season;
    $driversCarModelsTabSubTab = array();
    $driversCarModelsTabSubTab[] = $item;
  }
  $driversCarModelsTab[$tmp_season] = $driversCarModelsTabSubTab;  
}

// miejsca w wyscigach - wg sezonow
$query="SELECT gp.season, gp.race_pos name,
(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos=gp.race_pos AND id_driver=$id AND season=gp.season) y 
FROM drivers_gp_results gp WHERE gp.season IN (SELECT season FROM drivers_class WHERE id_driver=$id) 
GROUP BY gp.season, gp.race_pos ORDER BY 1,2";
$result = mysqli_query($dbhandle,$query);
$gpPlacesBySeason = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesBySeason[] = $r;
}
$careerItems["gpPlaces"] = $gpPlacesBySeason;

// miejsca w kwalifikacjach - wg sezonow
$query="SELECT gp.season, gp.grid_pos name,
(SELECT COUNT(*) FROM drivers_gp_starting_grid WHERE grid_pos=gp.grid_pos AND id_driver=$id AND season=gp.season) y 
FROM drivers_gp_starting_grid gp WHERE gp.season IN (SELECT season FROM drivers_class WHERE id_driver=$id) 
GROUP BY gp.season, gp.grid_pos ORDER BY 1,2";
$result = mysqli_query($dbhandle,$query);
$ppPlacesBySeason = array();
while($r = mysqli_fetch_assoc($result)) {
  $ppPlacesBySeason[] = $r;
}
$careerItems["ppPlaces"] = $ppPlacesBySeason;

// sezony
$query="SELECT gp.season, drivers.alias alias, drivers.id_driver id, CONCAT(drivers.name,' ',drivers.surname) name, drivers.country_code country,
COALESCE((SELECT place FROM drivers_class WHERE id_driver=gp.id_driver AND season=gp.season and is_classified=1),'NK') place,
COALESCE((SELECT points FROM drivers_class WHERE id_driver=gp.id_driver AND season=gp.season),0) points,
COALESCE((SELECT points_class FROM drivers_class WHERE id_driver=gp.id_driver AND season=gp.season),0) pointsClass,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' teams
FROM drivers_gp_involvements gp,drivers where drivers.id_driver=$id AND drivers.id_driver=gp.id_driver AND gp.id_driver=$id 
GROUP BY 1 ORDER BY 1 desc";
$result = mysqli_query($dbhandle,$query);
$driverSeasonsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $r["gp"]=!isset($driversGPTab[$r["season"]]) ? 0 : $driversGPTab[$r["season"]];
  $r["starts"]=!isset($driversStartsTab[$r["season"]]) ? 0 : $driversStartsTab[$r["season"]];
  $r["qual"]=!isset($driversQualTab[$r["season"]]) ? 0 : $driversQualTab[$r["season"]];
  $r["wins"]=!isset($driversWinsTab[$r["season"]]) ? 0 : $driversWinsTab[$r["season"]];
  $r["second"]=!isset($driversSecondTab[$r["season"]]) ? 0 : $driversSecondTab[$r["season"]];
  $r["third"]=!isset($driversThirdTab[$r["season"]]) ? 0 : $driversThirdTab[$r["season"]];
  $r["podium"]=!isset($driversPodiumsTab[$r["season"]]) ? 0 : $driversPodiumsTab[$r["season"]];
  $r["polepos"]=!isset($driversPolePosTab[$r["season"]]) ? 0 : $driversPolePosTab[$r["season"]];
  $r["bestlaps"]=!isset($driversBLTab[$r["season"]]) ? 0 : $driversBLTab[$r["season"]];
  $r["pointsPlaces"]=!isset($driversPointsPlacesTab[$r["season"]]) ? 0 : $driversPointsPlacesTab[$r["season"]];
  $r["completed"]=!isset($driversCompletedTab[$r["season"]]) ? 0 : $driversCompletedTab[$r["season"]];
  $r["incomplete"]=!isset($driversIncompleteTab[$r["season"]]) ? 0 : $driversIncompleteTab[$r["season"]];
  $r["disq"]=!isset($driversDisqTab[$r["season"]]) ? 0 : $driversDisqTab[$r["season"]];
  $r["teamsNames"]=!isset($driversTeamsNamesTab[$r["season"]]) ? 0 : trim($driversTeamsNamesTab[$r["season"]]);
  $r["teamsAliases"]=!isset($driversTeamsAliasesTab[$r["season"]]) ? 0 : trim($driversTeamsAliasesTab[$r["season"]]);
  $r["teams"]=!isset($driversTeamsTab[$r["season"]]) ? 0 : trim($driversTeamsTab[$r["season"]]);
  $r["carModels"]=$driversCarModelsTab[$r["season"]];

  //zespoły w sezonach
  $query2="SELECT DISTINCT drivers_gp_involvements.season,
  COUNT(drivers_gp_involvements.id_team_model) starts,
  lower(teams.team) team,
  teams.country_code countryCode,
  teams.alias_name alias,
  teams.name,
  teams.engine,
  teams_models.id_team_model idTeamModel,  
  GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamModelName,  
  teams_models.model    
  from drivers_gp_involvements,teams,teams_models 
  where id_driver=$id 
  and drivers_gp_involvements.id_team=teams.id_team
  and teams_models.id_team_model=drivers_gp_involvements.id_team_model
  and drivers_gp_involvements.season=$tmp_season
  group BY teams_models.model
  order by drivers_gp_involvements.season DESC";
  $result2 = mysqli_query($dbhandle,$query2);
  $driverTeamsTab = array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    $driverTeamsTab[] = $r2;
  }
  $r["teams"]=$driverTeamsTab;

  // miejsca w gp 
  $query2="SELECT
  gp.country_code name,
  LOWER(gp.name_short) nameShort,
  gp.name_alias alias,";
  if ($lang=='pl') {
    $query2.="gp.name gp";
  }else{
    $query2.="gp.name_en as gp";
  }
  $query2.=",gp.circuit,
  gp.circuit_alias circuitAlias,
  SUBSTRING(gp_season.date,1,10) raceDate,
  gp_season.sprint,
  race_pos place,
  race_completed completed,
  race_time time,
  race_best_lap bestLap,
  race_laps laps,
  disq,";
  if ($lang=='pl') {
    $query2.="drivers_gp_results.race_add_info info,
    drivers_pp_results.qual_add_info qualInfo,
    drivers_gp_starting_grid.grid_add_info gridInfo,";
  }else{
    $query2.="drivers_gp_results.race_add_info_en info,
    drivers_pp_results.qual_add_info_en qualInfo,
    drivers_gp_starting_grid.grid_add_info_en gridInfo,";
  }
  $query2.="
  excluded_from_class excluded,
  race_points racePoints,
  drivers_sprint_results.sprint_points sprintPoints,
  drivers_sprint_results.sprint_pos sprintPos,
  drivers_pp_results.qual_pos qual,
  drivers_pp_results.not_qualified notQualified,
  drivers_pp_results.not_started notStarted,
  drivers_gp_starting_grid.grid_pos grid,
  SUM(COALESCE(race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  COALESCE((SELECT SUM(COALESCE(race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) FROM drivers_gp_results WHERE drivers_gp_results.id_driver=drivers_gp_involvements.id_driver
  AND drivers_gp_results.season=gp_season.season and drivers_gp_results.id_gp=gp_season.id_gp  and drivers_gp_results.excluded_from_class=0),0) classPoints,
  lower(drivers_gp_involvements.team) team,
  teams.name teamFullName,
  teams.alias_name teamAlias,
  teams_models.model modelName,
  teams_models.team_name teamName
  FROM
      gp_season
  LEFT JOIN gp ON gp.id_gp = gp_season.id_gp
  LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_gp=gp_season.id_gp 
  AND drivers_gp_involvements.id_driver = $id 
  AND drivers_gp_involvements.season = gp_season.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.season =gp_season.season
  LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model and teams_models.season=gp_season.season
  LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
  WHERE
      gp_season.season = $tmp_season
  GROUP BY
      gp.id_gp
  ORDER BY
      gp_season.sort,
      race_pos";
  $result2 = mysqli_query($dbhandle,$query2);
  
  $gpResults=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    if($r2["completed"]=='0') {
        if ($r2["time"]=='DNQ') $r2["place"]="NQ";
        else {
            if ($r2["disq"] === 0){
              $r2["place"]="-";
            }else{
              $r2["place"]="DS";
            }
        }
        if ($r2["time"]=='DNS') $r2["place"]="";
    }else if($r2["completed"]=="1") {
      $r2["place"]=$r2["place"];
    }else{
      $r2["place"]="";
    }
    $gpResults[] = $r2;
  }
  $r["gpResults"]=$gpResults;

  $driverSeasonsItems[] = $r;
}
$careerItems["results"] = $driverSeasonsItems;

//pierwsza grand prix
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstGP"] = $r;
}

//ostatnia grand prix
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastGP"] = $r;
}

//pierwsze zwyciestwo
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_pos=1 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstWin"] = $r;
}

//ostatnie zwyciestwo
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_pos=1 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastWin"] = $r;
}

//pierwsze podium
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_pos<4 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstPodium"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstPodium"] = $r;
}

//ostatnie podium
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_pos<4 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastPodium"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastPodium"] = $r;
}

//pierwsze kwalifikacje
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,qual_date,SUBSTRING(qual_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_pp_results,gp,teams WHERE drivers_pp_results.id_gp=gp.id_gp AND drivers_pp_results.id_team=teams.id_team AND id_driver=$id ORDER BY qual_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstQual"] = $r;
}

//ostatnie kwalifikacje
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,qual_date,SUBSTRING(qual_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_pp_results,gp,teams WHERE drivers_pp_results.id_gp=gp.id_gp AND drivers_pp_results.id_team=teams.id_team AND id_driver=$id ORDER BY qual_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastQual"] = $r;
}

//pierwsze pole position
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id, gp_season.season year, teams.alias_name teamAlias, teams.id_team, teams.name team, teams.engine FROM drivers_gp_starting_grid, gp, teams, gp_season WHERE drivers_gp_starting_grid.id_gp = gp.id_gp  AND drivers_gp_starting_grid.id_team = teams.id_team  AND drivers_gp_starting_grid.id_driver = $id AND drivers_gp_starting_grid.grid_pos = 1 AND gp_season.id_gp=drivers_gp_starting_grid.id_gp AND gp_season.season=drivers_gp_starting_grid.season ORDER BY drivers_gp_starting_grid.season, gp_season.sort limit 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstPP"] = $r;
}

//ostatnie pole position
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id, gp_season.season year, teams.alias_name teamAlias, teams.id_team, teams.name team, teams.engine FROM drivers_gp_starting_grid, gp, teams, gp_season WHERE drivers_gp_starting_grid.id_gp = gp.id_gp  AND drivers_gp_starting_grid.id_team = teams.id_team  AND drivers_gp_starting_grid.id_driver = $id AND drivers_gp_starting_grid.grid_pos = 1 AND gp_season.id_gp=drivers_gp_starting_grid.id_gp AND gp_season.season=drivers_gp_starting_grid.season ORDER BY drivers_gp_starting_grid.season DESC, gp_season.sort DESC limit 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastPP"] = $r;
}

//pierwsze najlepsze okrazenie
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_best_lap=1 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstBL"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstBL"] = $r;
}

//ostatnie najlepsze okrazenie
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,teams.alias_name teamAlias,teams.id_team,teams.name team,teams.engine FROM drivers_gp_results,gp,teams WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND id_driver=$id AND drivers_gp_results.race_best_lap=1 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastBL"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastBL"] = $r;
}

//miejsca punktowane
$query="SELECT COUNT(*) gpPlacesWithPoints FROM drivers_gp_results WHERE id_driver=$id AND race_points>0";
$result = mysqli_query($dbhandle,$query);
$careerItems["gpPlacesWithPoints"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["gpPlacesWithPoints"] = $r["gpPlacesWithPoints"];
}

// miejsca w wyscigach - wszystkie sezony
$query="SELECT gp.race_pos name,(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos=gp.race_pos AND id_driver=$id) y
FROM drivers_gp_results gp WHERE gp.race_pos<21 AND id_driver=$id GROUP BY gp.race_pos ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPlacesTotal = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesTotal[] = $r;
}
$query="SELECT '>20' name,(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos>20 AND id_driver=$id) y
FROM drivers_gp_results gp WHERE gp.race_pos=21 AND id_driver=$id GROUP BY gp.race_pos";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesTotal[] = $r;
}
$careerItems["gpPlacesTotal"] = $gpPlacesTotal;

// miejsca w kwalifikacjach - wszystkie sezony
$query="SELECT pp.grid_pos name,(SELECT COUNT(*) FROM drivers_gp_starting_grid  WHERE grid_pos=pp.grid_pos AND id_driver=$id) y
FROM drivers_gp_starting_grid pp WHERE pp.grid_pos<21 AND id_driver=$id GROUP BY pp.grid_pos ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$ppPlacesTotal = array();
while($r = mysqli_fetch_assoc($result)) {
  $ppPlacesTotal[] = $r;
}
$query="SELECT '>20' name,(SELECT COUNT(*) FROM drivers_gp_starting_grid  WHERE grid_pos>20 AND id_driver=$id) y
FROM drivers_gp_starting_grid pp WHERE pp.grid_pos=21 AND id_driver=$id GROUP BY pp.grid_pos";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $ppPlacesTotal[] = $r;
}
$careerItems["ppPlacesTotal"] = $ppPlacesTotal;

// miejsca w klasyfikacji - wg sezonow
$query="SELECT season name, 
(CASE
    WHEN is_classified = 0 THEN null
    ELSE place
END) y FROM drivers_class WHERE id_driver=$id ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPlacesBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesBySeasons[] = $r;
}
$careerItems["gpPlacesBySeasons"] = $gpPlacesBySeasons;

// punkty w klasyfikacji - wg sezonow
$query="SELECT season name, points_class y FROM drivers_class WHERE id_driver=$id ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPointsClassBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPointsClassBySeasons[] = $r;
}
$careerItems["gpPointsClassBySeasons"] = $gpPointsClassBySeasons;

// punkty w wyscigach - wg sezonow (wszystkie)
$query="SELECT season name, points y, points - points_class sy FROM drivers_class WHERE id_driver=$id ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPointsBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPointsBySeasons[] = $r;
}
$careerItems["gpPointsBySeasons"] = $gpPointsBySeasons;

// wygrane z miejsc
$query="SELECT
CAST(gp.place AS UNSIGNED) AS name,
(
SELECT
    COUNT(
        drivers_gp_results.id_drivers_gp
    )
FROM
    drivers_gp_results
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_results.id_driver 
AND drivers_gp_starting_grid.id_gp = drivers_gp_results.id_gp 
AND drivers_gp_starting_grid.season=drivers_gp_results.season
AND drivers_gp_starting_grid.id_driver = $id
WHERE
    drivers_gp_results.race_completed = 1 AND drivers_gp_results.race_pos = 1 AND drivers_gp_results.id_driver = $id AND drivers_gp_starting_grid.grid_pos = gp.place
) y
FROM drivers_class gp
WHERE gp.place < 21
GROUP BY gp.place ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpWinsFromPlaces = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpWinsFromPlaces[] = $r;
}
$query="SELECT '>20' name,
(SELECT COUNT(drivers_gp_results.id_drivers_gp)
FROM drivers_gp_results
LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_driver = drivers_gp_results.id_driver 
AND drivers_gp_starting_grid.id_gp = drivers_gp_results.id_gp 
AND drivers_gp_starting_grid.season=drivers_gp_results.season
AND drivers_gp_starting_grid.id_driver = $id
WHERE
    drivers_gp_results.race_completed = 1 AND drivers_gp_results.race_pos = 1 AND drivers_gp_results.id_driver = $id AND drivers_gp_starting_grid.grid_pos > 20
) y
FROM drivers_class gp
WHERE gp.place = 21
GROUP BY gp.place ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $gpWinsFromPlaces[] = $r;
}
$careerItems["gpWinsFromPlaces"] = $gpWinsFromPlaces;

$driverItems["career"]=$careerItems;

/**
* Zespoły
**/
//gp w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_gp) as gp
from drivers_gp_involvements,teams where id_driver=$id and teams.id_team=drivers_gp_involvements.id_team group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversGPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversGPTab[$tmp_alias] = $r["gp"];
}
//wysigi w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as starts
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversStartsTab[$tmp_alias] = $r["starts"];
}
//starty w kwalifikacjacg zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_pp) as qual
from drivers_pp_results,teams where id_driver=$id and teams.id_team=drivers_pp_results.id_team group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversQualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversQualTab[$tmp_alias] = $r["qual"];
}
//zwyciestwa w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as wins
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_pos=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversWinsTab[$tmp_alias] = $r["wins"];
}
//drugie miejsca w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as second
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_pos=2 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversSecondTab[$tmp_alias] = $r["second"];
}
//trzecie miejsca w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as third
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_pos=3 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversThirdTab[$tmp_alias] = $r["third"];
}
//podium w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as podiums
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_pos<4 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPodiumsTab[$tmp_alias] = $r["podiums"];
}
//pole position w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_starting_grid) as pp
from drivers_gp_starting_grid,teams where id_driver=$id and teams.id_team=drivers_gp_starting_grid.id_team and is_pp=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPolePosTab[$tmp_alias] = $r["pp"];
}
//best laps w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as bestlaps
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_best_lap=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversBLTab[$tmp_alias] = $r["bestlaps"];
}
//gp z punktami w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as pointsPlaces
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_points>0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsPlacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsPlacesTab[$tmp_alias] = $r["pointsPlaces"];
}
//ukonczone gp w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as completed
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_completed=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversCompletedTab[$tmp_alias] = $r["completed"];
}
//nieukonczone gp w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as incomplete
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_completed=0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversIncompleteTab[$tmp_alias] = $r["incomplete"];
}
//dyskwalifikacje w zespołach
$query="SELECT teams.alias_name alias,COUNT(id_drivers_gp) as disq
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and disq=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversDisqTab[$tmp_alias] = $r["disq"];
}
//punkty w zespołach
$query="SELECT teams.alias_name alias,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_team=teams.id_team),0) as points
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_points>0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsTab[$tmp_alias] = $r["points"];
}
//punkty w zespołach do klasyfikacji
$query="SELECT teams.alias_name alias,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_team=teams.id_team),0) as points
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_points>0 and excluded_from_class=0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsClassTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsClassTab[$tmp_alias] = $r["points"];
}

// zespoly -wykresy
$teamsStatsItems;

// grand prix
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_gp) amount, COUNT(id_gp) y
from drivers_gp_involvements,teams where id_driver=$id and teams.id_team=drivers_gp_involvements.id_team 
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByGP"]=$statsItems;

// wyscigi
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByStarts"]=$statsItems;

// starty - kwalifikacje
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_pp) amount, COUNT(id_drivers_pp) y
from drivers_pp_results,teams where id_driver=$id and teams.id_team=drivers_pp_results.id_team 
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByQualStarts"]=$statsItems;

// zwyciestwa
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_pos=1
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByWins"]=$statsItems;

// 2 miejsca
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_pos=2
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamBySecondPlaces"]=$statsItems;

// 3 miejsca
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_pos=3
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByThirdPlaces"]=$statsItems;

// podium
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_pos>0 AND race_pos<4
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByPodium"]=$statsItems;

// pole positions
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_starting_grid) amount, COUNT(id_starting_grid) y
from drivers_gp_starting_grid,teams where id_driver=$id and teams.id_team=drivers_gp_starting_grid.id_team 
AND is_pp=1
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByPolepos"]=$statsItems;

// naj. okrążenia
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_best_lap=1
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByBestlaps"]=$statsItems;

// wys ukonczone
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_completed=1
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByCompleted"]=$statsItems;

// wys nieukonczone
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_completed=0
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByIncompleted"]=$statsItems;

// dyskwalifikacje
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND disq=1
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByDisq"]=$statsItems;

// wyc punktowane
$query="SELECT teams.name, teams.team, teams.alias_name alias, COUNT(id_drivers_gp) amount, COUNT(id_drivers_gp) y
from drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team 
AND race_points>0
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByPointRaces"]=$statsItems;

// punkty
$query="SELECT teams.name, teams.team, teams.alias_name alias,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_team=teams.id_team),0) as amount,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_team=teams.id_team),0) as y
FROM drivers_gp_results,teams where id_driver=$id and teams.id_team=drivers_gp_results.id_team and race_points>0 
GROUP BY 1
ORDER BY 5 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$teamsStatsItems["bestTeamByPoints"]=$statsItems;

$driverItems["teamsStats"]=$teamsStatsItems;

// zespoły
$query="SELECT distinct teams.alias_name alias,teams.country_code country,teams.name,lower(teams.team) team, GROUP_CONCAT(distinct teams_models.team_name) teamModelName,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed,
'' incomplete, '' disq, '' points, '' pointsClass,
MIN(drivers_gp_involvements.season) seasonMin,
MAX(drivers_gp_involvements.season) seasonMax,
COUNT(DISTINCT drivers_gp_involvements.season) seasons
from drivers_gp_involvements,teams, teams_models where id_driver='$id' and teams.id_team=drivers_gp_involvements.id_team
and teams_models.team=teams.team AND teams_models.season=drivers_gp_involvements.season
group by teams.team
ORDER BY seasonMax DESC, seasonMin DESC, teams.name";
$result = mysqli_query($dbhandle,$query);
$driverTeamsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($driversGPTab[$r["alias"]]) ? 0 : $driversGPTab[$r["alias"]];
  $r["starts"]=!isset($driversStartsTab[$r["alias"]]) ? 0 : $driversStartsTab[$r["alias"]];
  $r["qual"]=!isset($driversQualTab[$r["alias"]]) ? 0 : $driversQualTab[$r["alias"]];
  $r["wins"]=!isset($driversWinsTab[$r["alias"]]) ? 0 : $driversWinsTab[$r["alias"]];
  $r["second"]=!isset($driversSecondTab[$r["alias"]]) ? 0 : $driversSecondTab[$r["alias"]];
  $r["third"]=!isset($driversThirdTab[$r["alias"]]) ? 0 : $driversThirdTab[$r["alias"]];
  $r["podium"]=!isset($driversPodiumsTab[$r["alias"]]) ? 0 : $driversPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($driversPolePosTab[$r["alias"]]) ? 0 : $driversPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($driversBLTab[$r["alias"]]) ? 0 : $driversBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($driversPointsPlacesTab[$r["alias"]]) ? 0 : $driversPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($driversCompletedTab[$r["alias"]]) ? 0 : $driversCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($driversIncompleteTab[$r["alias"]]) ? 0 : $driversIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($driversDisqTab[$r["alias"]]) ? 0 : $driversDisqTab[$r["alias"]];
  $r["points"]=!isset($driversPointsTab[$r["alias"]]) ? 0 : $driversPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($driversPointsClassTab[$r["alias"]]) ? 0 : $driversPointsClassTab[$r["alias"]];
  $driverTeamsItems[] = $r;
}
$driverItems["teams"]=$driverTeamsItems;

/**
* Grand Prix
**/
//udzialy w gp
$query="SELECT gp.name_alias alias,count(DISTINCT CONCAT(drivers_gp_involvements.season,drivers_gp_involvements.id_gp)) amount
from drivers_gp_involvements,gp where id_driver=$id and gp.id_gp=drivers_gp_involvements.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversGPTab = array();
$driverGPStatsGPItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversGPTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsGPItems = $r;
  }
  $cnt+=1;
}
//wyscigi w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversStartsTab = array();
$driverGPStatsStartsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversStartsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsStartsItems = $r;
  }
  $cnt+=1;
}
//starty w kwalifikacjach
$query="SELECT gp.name_alias alias,COUNT(DISTINCT qual_date) amount
from drivers_pp_results,gp where id_driver=$id and gp.id_gp=drivers_pp_results.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversQualTab = array();
$driverGPStatsQualItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversQualTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsQualItems = $r;
  }
  $cnt+=1;
}
//pola startowe
$query="SELECT gp.name_alias alias,COUNT(DISTINCT id_involvement) amount
from drivers_gp_involvements,gp where id_driver=$id and gp.id_gp=drivers_gp_involvements.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversGridTab = array();
$driverGPStatsGridItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversGridTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsGridItems = $r;
  }
  $cnt+=1;
}
//zwyciestwa w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
$driverGPStatsWinsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversWinsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsWinsItems = $r;
  }
  $cnt+=1;
}
//drugie miejsca w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=2 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversSecondTab = array();
$driverGPStatsSecondItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversSecondTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsSecondItems = $r;
  }
  $cnt+=1;
}
//trzecie miejsca w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=3 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversThirdTab = array();
$driverGPStatsThirdItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversThirdTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsThirdItems = $r;
  }
  $cnt+=1;
}
//podium w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos<4 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
$driverGPStatsPodiumItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPodiumsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsPodiumItems = $r;
  }
  $cnt+=1;
}
//pole position w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,COUNT(id_starting_grid) amount
from drivers_gp_starting_grid,gp where id_driver=$id and gp.id_gp=drivers_gp_starting_grid.id_gp and is_pp=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
$driverGPStatsPoleposItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPolePosTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsPoleposItems = $r;
  }
  $cnt+=1;
}
//best laps w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_best_lap=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
$driverGPStatsBLItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversBLTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsBLItems = $r;
  }
  $cnt+=1;
}
//gp z punktami w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsPlacesTab = array();
$driverGPStatsPointsPlacesItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsPlacesTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsPointsPlacesItems = $r;
  }
  $cnt+=1;
}
//ukonczone gp w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_completed=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversCompletedTab = array();
$driverGPStatsCompletedItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversCompletedTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsCompletedItems = $r;
  }
  $cnt+=1;
}
//nieukonczone gp w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_completed=0 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversIncompleteTab = array();
$driverGPStatsIncompleteItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversIncompleteTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsIncompleteItems = $r;
  }
  $cnt+=1;
}
//dyskwalifikacje w gp
$query="SELECT gp.name_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and disq=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversDisqTab = array();
$driverGPStatsDisqItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversDisqTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsDisqItems = $r;
  }
  $cnt+=1;
}
//punkty w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_gp=gp.id_gp),0) as amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsTab = array();
$driverGPStatsPointsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverGPStatsPointsItems = $r;
  }
  $cnt+=1;
}
//punkty w gp do klasyfikacji
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_gp=gp.id_gp),0) as amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 and excluded_from_class=0 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsClassTab = array();
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsClassTab[$tmp_alias] = $r["amount"];
  $cnt+=1;
}
// grand prix
if ($lang=='pl') {
  $query="SELECT distinct gp.name";
}else{
  $query="SELECT distinct gp.name_en as name";
}
$query.=",gp.name_alias alias, gp.country_code country, gp.name_short nameShort, GROUP_CONCAT(distinct gp.circuit separator ', ') circuits,
SUBSTRING(gp_season.date,1,10) raceDate,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed,
'' incomplete, '' disq, '' points, '' pointsClass
from drivers_gp_involvements,gp,gp_season where id_driver='$id' and gp.id_gp=gp_season.id_gp and gp.id_gp=drivers_gp_involvements.id_gp
group by name_short
order by name";
$result = mysqli_query($dbhandle,$query);
$driverGPItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($driversGPTab[$r["alias"]]) ? 0 : $driversGPTab[$r["alias"]];
  $r["starts"]=!isset($driversStartsTab[$r["alias"]]) ? 0 : $driversStartsTab[$r["alias"]];
  $r["qual"]=!isset($driversQualTab[$r["alias"]]) ? 0 : $driversQualTab[$r["alias"]];
  $r["wins"]=!isset($driversWinsTab[$r["alias"]]) ? 0 : $driversWinsTab[$r["alias"]];
  $r["second"]=!isset($driversSecondTab[$r["alias"]]) ? 0 : $driversSecondTab[$r["alias"]];
  $r["third"]=!isset($driversThirdTab[$r["alias"]]) ? 0 : $driversThirdTab[$r["alias"]];
  $r["podium"]=!isset($driversPodiumsTab[$r["alias"]]) ? 0 : $driversPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($driversPolePosTab[$r["alias"]]) ? 0 : $driversPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($driversBLTab[$r["alias"]]) ? 0 : $driversBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($driversPointsPlacesTab[$r["alias"]]) ? 0 : $driversPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($driversCompletedTab[$r["alias"]]) ? 0 : $driversCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($driversIncompleteTab[$r["alias"]]) ? 0 : $driversIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($driversDisqTab[$r["alias"]]) ? 0 : $driversDisqTab[$r["alias"]];
  $r["points"]=!isset($driversPointsTab[$r["alias"]]) ? 0 : $driversPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($driversPointsClassTab[$r["alias"]]) ? 0 : $driversPointsClassTab[$r["alias"]];
  $driverGPItems[] = $r;
}
$driverItems["gp"]=$driverGPItems;

// grand prix - statystyki
$driverGPStatsItems=array();
$driverGPStatsItems['gp']=$driverGPStatsGPItems;
$driverGPStatsItems['starts']=$driverGPStatsStartsItems;
$driverGPStatsItems['qual']=$driverGPStatsQualItems;
$driverGPStatsItems['grid']=$driverGPStatsGridItems;
$driverGPStatsItems['wins']=$driverGPStatsWinsItems;
$driverGPStatsItems['second']=$driverGPStatsSecondItems;
$driverGPStatsItems['third']=$driverGPStatsThirdItems;
$driverGPStatsItems['podium']=$driverGPStatsPodiumItems;
$driverGPStatsItems['polepos']=$driverGPStatsPoleposItems;
$driverGPStatsItems['bestlaps']=$driverGPStatsBLItems;
$driverGPStatsItems['pointsPlaces']=$driverGPStatsPointsPlacesItems;
$driverGPStatsItems['completed']=$driverGPStatsCompletedItems;
$driverGPStatsItems['incomplete']=$driverGPStatsIncompleteItems;
$driverGPStatsItems['disq']=$driverGPStatsDisqItems;
$driverGPStatsItems['points']=$driverGPStatsPointsItems;
$driverItems["gpStats"]=$driverGPStatsItems;

/**
* Tory
**/
//grand prix na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT CONCAT(drivers_gp_involvements.season,drivers_gp_involvements.id_gp)) amount
from drivers_gp_involvements,gp where id_driver=$id and gp.id_gp=drivers_gp_involvements.id_gp group by 1 desc order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversGPTab = array();
$driverCircuitsStatsGPItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversGPTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsGPItems = $r;
  }
  $cnt+=1;
}
//wyscigi na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp group by 1 desc order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversStartsTab = array();
$driverCircuitsStatsStartsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversStartsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsStartsItems = $r;
  }
  $cnt+=1;
}
//starty w kwalifikacjach na torach
$query="SELECT gp.circuit_alias alias,COUNT(DISTINCT qual_date) amount
from drivers_pp_results,gp where id_driver=$id and gp.id_gp=drivers_pp_results.id_gp group by 1 desc order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversQualTab = array();
$driverCircuitsStatsQualItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversQualTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsQualItems = $r;
  }
  $cnt+=1;
}
//zwyciestwa na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversWinsTab = array();
$driverCircuitsStatsWinsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversWinsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsWinsItems = $r;
  }
  $cnt+=1;
}
//drugie miejsca na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=2 group by 1 desc order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversSecondTab = array();
$driverCircuitsStatsSecondItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversSecondTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsSecondItems = $r;
  }
  $cnt+=1;
}
//trzecie miejsca na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos=3 group by 1 desc order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversThirdTab = array();
$driverCircuitsStatsThirdItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversThirdTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsThirdItems = $r;
  }
  $cnt+=1;
}
//podium na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_pos<4 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPodiumsTab = array();
$driverCircuitsStatsPodiumItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPodiumsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsPodiumItems = $r;
  }
  $cnt+=1;
}
//pole position na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_starting_grid) amount
from drivers_gp_starting_grid,gp where id_driver=$id and gp.id_gp=drivers_gp_starting_grid.id_gp and is_pp=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPolePosTab = array();
$driverCircuitsStatsPoleposItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPolePosTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsPoleposItems = $r;
  }
  $cnt+=1;
}
//best laps na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_best_lap=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversBLTab = array();
$driverCircuitsStatsBLItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversBLTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsBLItems = $r;
  }
  $cnt+=1;
}
//gp z punktami na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsPlacesTab = array();
$driverCircuitsStatsPointsPlacesItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsPlacesTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsPointsPlacesItems = $r;
  }
  $cnt+=1;
}
//ukonczone gp na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_completed=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversCompletedTab = array();
$driverCircuitsStatsCompletedItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversCompletedTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsCompletedItems = $r;
  }
  $cnt+=1;
}
//nieukonczone gp na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_completed=0 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversIncompleteTab = array();
$driverCircuitsStatsIncompleteItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversIncompleteTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsIncompleteItems = $r;
  }
  $cnt+=1;
}
//dyskwalifikacje na torach
$query="SELECT gp.circuit_alias alias,count(DISTINCT race_date) amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and disq=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$driversDisqTab = array();
$driverCircuitsStatsDisqItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversDisqTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsDisqItems = $r;
  }
  $cnt+=1;
}
//punkty na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_gp=gp.id_gp),0) as amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsTab = array();
$driverCircuitsStatsPointsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $driverCircuitsStatsPointsItems = $r;
  }
  $cnt+=1;
}
//punkty na torach do klasyfikacji
$query="SELECT gp.circuit_alias alias,gp.circuit,
SUM(race_points) + COALESCE((SELECT sum(sprint_points) FROM drivers_sprint_results WHERE drivers_sprint_results.id_driver=drivers_gp_results.id_driver and drivers_sprint_results.id_gp=gp.id_gp),0) as amount
from drivers_gp_results,gp where id_driver=$id and gp.id_gp=drivers_gp_results.id_gp and race_points>0 and excluded_from_class=0 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$driversPointsClassTab = array();
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $driversPointsClassTab[$tmp_alias] = $r["amount"];
  $cnt+=1;
}
// tory
$query="SELECT distinct gp.circuit_alias alias,gp.circuit name, 
GROUP_CONCAT(DISTINCT gp.country_code SEPARATOR ', ') countries,";
if ($lang=='pl') {
  $query.="GROUP_CONCAT(DISTINCT gp.name SEPARATOR ', ') grandPrix";
}else{
  $query.="GROUP_CONCAT(DISTINCT gp.name_en SEPARATOR ', ') grandPrix";
} 
$query.=",'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed,
'' incomplete, '' disq, '' points, '' pointsClass
from drivers_gp_involvements,gp where id_driver='$id' and gp.id_gp=drivers_gp_involvements.id_gp
GROUP BY 1
order by name";
$result = mysqli_query($dbhandle,$query);
$driverCircuitsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($driversGPTab[$r["alias"]]) ? 0 : $driversGPTab[$r["alias"]];
  $r["starts"]=!isset($driversStartsTab[$r["alias"]]) ? 0 : $driversStartsTab[$r["alias"]];
  $r["qual"]=!isset($driversQualTab[$r["alias"]]) ? 0 : $driversQualTab[$r["alias"]];
  $r["wins"]=!isset($driversWinsTab[$r["alias"]]) ? 0 : $driversWinsTab[$r["alias"]];
  $r["second"]=!isset($driversSecondTab[$r["alias"]]) ? 0 : $driversSecondTab[$r["alias"]];
  $r["third"]=!isset($driversThirdTab[$r["alias"]]) ? 0 : $driversThirdTab[$r["alias"]];
  $r["podium"]=!isset($driversPodiumsTab[$r["alias"]]) ? 0 : $driversPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($driversPolePosTab[$r["alias"]]) ? 0 : $driversPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($driversBLTab[$r["alias"]]) ? 0 : $driversBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($driversPointsPlacesTab[$r["alias"]]) ? 0 : $driversPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($driversCompletedTab[$r["alias"]]) ? 0 : $driversCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($driversIncompleteTab[$r["alias"]]) ? 0 : $driversIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($driversDisqTab[$r["alias"]]) ? 0 : $driversDisqTab[$r["alias"]];
  $r["points"]=!isset($driversPointsTab[$r["alias"]]) ? 0 : $driversPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($driversPointsClassTab[$r["alias"]]) ? 0 : $driversPointsClassTab[$r["alias"]];
  $driverCircuitsItems[] = $r;
}
$driverItems["circuits"]=$driverCircuitsItems;

// tory - statystyki
$driverCircuitsStatsItems=array();
$driverCircuitsStatsItems['gp']=$driverCircuitsStatsGPItems;
$driverCircuitsStatsItems['starts']=$driverCircuitsStatsStartsItems;
$driverCircuitsStatsItems['qual']=$driverCircuitsStatsQualItems;
$driverCircuitsStatsItems['wins']=$driverCircuitsStatsWinsItems;
$driverCircuitsStatsItems['second']=$driverCircuitsStatsSecondItems;
$driverCircuitsStatsItems['third']=$driverCircuitsStatsThirdItems;
$driverCircuitsStatsItems['podium']=$driverCircuitsStatsPodiumItems;
$driverCircuitsStatsItems['polepos']=$driverCircuitsStatsPoleposItems;
$driverCircuitsStatsItems['bestlaps']=$driverCircuitsStatsBLItems;
$driverCircuitsStatsItems['pointsPlaces']=$driverCircuitsStatsPointsPlacesItems;
$driverCircuitsStatsItems['completed']=$driverCircuitsStatsCompletedItems;
$driverCircuitsStatsItems['incomplete']=$driverCircuitsStatsIncompleteItems;
$driverCircuitsStatsItems['disq']=$driverCircuitsStatsDisqItems;
$driverCircuitsStatsItems['points']=$driverCircuitsStatsPointsItems;
$driverItems["circuitsStats"]=$driverCircuitsStatsItems;

$driverItems["createTime"]=microtime(true)-$start_time;

// teammates
$query="SELECT drivers.id_driver id, drivers.name, drivers.surname, drivers.country_code countryCode, drivers.alias,
GROUP_CONCAT(DISTINCT season SEPARATOR ', ') seasons,
GROUP_CONCAT(DISTINCT team SEPARATOR ', ') teams 
FROM drivers_gp_results 
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver 
WHERE race_date IN (SELECT race_date FROM drivers_gp_results WHERE id_driver=$id) AND drivers_gp_results.id_driver!=$id 
AND drivers_gp_results.id_team IN (SELECT id_team FROM drivers_gp_results dgr WHERE dgr.id_driver=$id AND dgr.season=drivers_gp_results.season AND dgr.id_gp=drivers_gp_results.id_gp AND dgr.co_driver=0) 
AND drivers_gp_results.co_driver=0
GROUP BY drivers_gp_results.id_driver 
ORDER BY drivers.surname ASC";
$result = mysqli_query($dbhandle,$query);
$driverTeammatesItems=array();
$teammates=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_driver_id = $r["id"];
  $teammates["id"] = $r["id"];  
  $teammates["name"] = $r["name"];  
  $teammates["surname"] = $r["surname"];  
  $teammates["countryCode"] = $r["countryCode"];  
  $teammates["alias"] = $r["alias"]; 
  $teammates["seasons"] = $r["seasons"]; 
  $teammates["teams"] = $r["teams"];  
  
  $query2="SELECT 
  SUM(race_pos<(SELECT race_pos FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id AND dgr.co_driver=0)) driverWins,
  SUM(race_pos>(SELECT race_pos FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id AND dgr.co_driver=0)) teammateWins
  FROM drivers_gp_results WHERE id_driver=$id AND co_driver=0";
  $result2 = mysqli_query($dbhandle,$query2);
  while($r2 = mysqli_fetch_assoc($result2)) {
    $teammates["teammateWins"] = $r2["teammateWins"];
    $teammates["driverWins"] = $r2["driverWins"];
    $teammates["total"] = $r2["teammateWins"] + $r2["driverWins"];
  }
  
  $query3="SELECT 
  SUM(qual_pos<(SELECT qual_pos FROM drivers_pp_results dgr WHERE dgr.id_gp=drivers_pp_results.id_gp AND dgr.id_team=drivers_pp_results.id_team AND dgr.season=drivers_pp_results.season AND dgr.id_driver=$tmp_driver_id)) driverPP,
  SUM(qual_pos>(SELECT qual_pos FROM drivers_pp_results dgr WHERE dgr.id_gp=drivers_pp_results.id_gp AND dgr.id_team=drivers_pp_results.id_team AND dgr.season=drivers_pp_results.season AND dgr.id_driver=$tmp_driver_id)) teammatePP
  FROM drivers_pp_results WHERE id_driver=$id";
  $result3 = mysqli_query($dbhandle,$query3);
  while($r3 = mysqli_fetch_assoc($result3)) {
    $teammates["teammatePP"] = $r3["teammatePP"];
    $teammates["driverPP"] = $r3["driverPP"];
    $teammates["totalPP"] = $r3["teammatePP"] + $r3["driverPP"];
  }

  mysqli_free_result($result2);
  $driverTeammatesItems[] = $teammates;  
}
$driverItems["teammates"]=$driverTeammatesItems;

// modele 
$query="SELECT teams_models.id_team_model, teams_models.team, teams.name, teams.alias_name alias,
GROUP_CONCAT(DISTINCT teams.engine ORDER BY teams.engine SEPARATOR ', ') engine,
GROUP_CONCAT(DISTINCT teams_models.season ORDER BY teams_models.season SEPARATOR ', ') season,
teams_models.model, GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamName
FROM teams_models
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_team_model = teams_models.id_team_model AND drivers_gp_involvements.season = teams_models.season
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_involvements.id_driver = '$id'
GROUP BY teams_models.model
ORDER BY teams_models.season DESC, teams_models.model DESC";
$result = mysqli_query($dbhandle,$query);
$teamModelsItems=array();
while($r = mysqli_fetch_assoc($result)) {
   $teamModelsItems[] = $r;
}
$driverItems["teamModels"]=$teamModelsItems;

// Response
$response = $driverItems;

print json_encode($response);
mysqli_free_result($result);
?>


