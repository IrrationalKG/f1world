import { FETCH_NEWS_DETAILS, FETCH_NEWS_DETAILS_SUCCESS, FETCH_NEWS_DETAILS_ERROR } from '../actions/NewsDetailsActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_NEWS_DETAILS:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_NEWS_DETAILS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_NEWS_DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
