<?
header('Access-Control-Allow-Origin: *');

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

$awards = array();

// klasyfikacja wszechczasow
$query="SELECT user FROM competition_stats WHERE (place>0 AND place<4) OR (place_gp>0 AND place_gp<4) GROUP BY user";
$result = mysqli_query($dbhandle,$query);
$normalAwards = array();
$normalAwards["id"]="normal";
$gpAwards = array();
$gpAwards["id"]="gp";
while($r = mysqli_fetch_assoc($result)) {
  $user = $r["user"];
  $normalAwardsItems = array();
  $gpAwardsItems = array();

  $isNormalAwards=false;
  $isGPAwards=false;
  // Klasyfikacja wszechczasow
  $query1="SELECT '-1' season, place FROM competition_history WHERE id_user=$user AND place<4";
  $result1 = mysqli_query($dbhandle,$query1);
  while($r1 = mysqli_fetch_assoc($result1)) {
     $normalAwardsItems[] = $r1;
     $isNormalAwards=true;
  }
  // Klasyfikacja zwykla
  $query2="SELECT season, place FROM competition_stats WHERE user=$user AND place>0 AND place<4 order by place, season";
  $result2 = mysqli_query($dbhandle,$query2);
  while($r2 = mysqli_fetch_assoc($result2)) {
     $normalAwardsItems[] = $r2;
     $isNormalAwards=true;
  }
  // Klasyfikacja gp
  $query3="SELECT season, place_gp place FROM competition_stats WHERE user=$user AND place_gp>0 AND place_gp<4 order by place, season";
  $result3 = mysqli_query($dbhandle,$query3);
  while($r3 = mysqli_fetch_assoc($result3)) {
     $gpAwardsItems[] = $r3;
     $isGPAwards=true;
  }
  if ($isNormalAwards) {
    $normalAwards[$user] = $normalAwardsItems;
  }
  if ($isGPAwards) {
    $gpAwards[$user] = $gpAwardsItems;
  }
}
// $awards = $normalAwards;
// $awards = $gpAwards;

// Wyniki rundy
// $query="SELECT uczestnik id,SUBSTRING(name,1,30) name,surname,short_name,suma points,pkt_gp pointsgp,
// '' seasons,'' first,'' second,'' third
// from typy,users where users.id_user=typy.uczestnik and sezon= '$year' and gp='$gp' and is_deleted=0
// order by suma desc,pkt_gp desc,surname LIMIT 10";
// $result = mysqli_query($dbhandle,$query);
// $resultsItems = array();
// while($r = mysqli_fetch_assoc($result)) {
//    $r["seasons"] = ($normalAwards[$r["id"]]==null) ? 0 : $normalAwards[$r["id"]];
//    $r["first"] = ($first[$r["id"]]==null) ? 0 : $first[$r["id"]];
//    $r["second"] = ($second[$r["id"]]==null) ? 0 : $second[$r["id"]];
//    $r["third"] = ($third[$r["id"]]==null) ? 0 : $third[$r["id"]];
//    $resultsItems[] = $r;
// }

// Response
//$response = $awards;
$response[0] = $normalAwards;
$response[1] = $gpAwards;
// $response[2] = $classgp;


print json_encode($response);
mysqli_free_result($result);
?>
