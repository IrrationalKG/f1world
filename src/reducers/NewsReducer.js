import { FETCH_NEWS, FETCH_NEWS_SUCCESS, FETCH_NEWS_ERROR } from '../actions/NewsActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_NEWS:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_NEWS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_NEWS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
