import {
  FETCH_TEAM_MODELS_REQUEST,
  FETCH_TEAM_MODELS_SUCCESS,
  FETCH_TEAM_MODELS_ERROR,
} from "../actions/TeamModelsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_TEAM_MODELS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_TEAM_MODELS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_TEAM_MODELS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
