import { FETCH_GPRESULT_REQUEST, FETCH_GPRESULT_SUCCESS, FETCH_GPRESULT_ERROR } from '../actions/GPResultActions';

const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_GPRESULT_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_GPRESULT_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_GPRESULT_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
