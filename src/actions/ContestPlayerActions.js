// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_CONTEST_PLAYER_REQUEST = "FETCH_CONTEST_PLAYER_REQUEST";
export const FETCH_CONTEST_PLAYER_SUCCESS = "FETCH_CONTEST_PLAYER_SUCCESS";
export const FETCH_CONTEST_PLAYER_ERROR = "FETCH_CONTEST_PLAYER_ERROR";

function requestData() {
  return {
    type: FETCH_CONTEST_PLAYER_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_PLAYER_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_PLAYER_ERROR,
    payload: error,
  };
}

export function fetchContestPlayer(id, year, lang) {
  const url = `${ROOT_URL}/get_contest_player.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
