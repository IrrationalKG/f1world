// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_STATS_REQUEST = 'FETCH_STATS_REQUEST';
export const FETCH_STATS_SUCCESS = 'FETCH_STATS_SUCCESS';
export const FETCH_STATS_ERROR = 'FETCH_STATS_ERROR';

function requestData() {
  return {
    type: FETCH_STATS_REQUEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_STATS_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_STATS_ERROR,
    payload: error
  };
}

export function fetchStats() {
  const url = `${ROOT_URL}/get_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url)
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
