// const request = require('../json/circuit.json');
//
// export const FETCH_CIRCUIT_SUCCESS = 'FETCH_CIRCUIT_SUCCESS';
//
// export function fetchCircuit() {
//   return {
//     type: FETCH_CIRCUIT_SUCCESS,
//     payload: request
//   };
// }

// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_CIRCUIT_REQUEST = "FETCH_CIRCUIT_REQUEST";
export const FETCH_CIRCUIT_SUCCESS = "FETCH_CIRCUIT_SUCCESS";
export const FETCH_CIRCUIT_ERROR = "FETCH_CIRCUIT_ERROR";

function requestData() {
  return {
    type: FETCH_CIRCUIT_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_CIRCUIT_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_CIRCUIT_ERROR,
    payload: error,
  };
}

export function fetchCircuit(id, year, lang) {
  const url = `${ROOT_URL}/get_circuit.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
