<?
header('Access-Control-Allow-Origin: *');

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

//query fire
$response = array();

$start_time = microtime(true);

// aktualny rok
$query="SELECT max(season) maxYear FROM drivers_class";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}
$prevYear=$currentYear-1;

// pobranie kolejnosci kierowcow
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc";
}else{
  $query="SELECT alias FROM drivers order by surname asc";
}
$result = mysqli_query($dbhandle,$query);
$driversTab = array();
$cnt=0;
$currentDriverCnt=0;
$maxDriverCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $driversTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentDriverCnt = $cnt;
  }
  $cnt+=1;
}
$maxDriverCnt = $cnt;

// min pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname asc LIMIT 1";
}else{
  $query="SELECT alias FROM drivers order by surname asc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT alias FROM drivers,drivers_class WHERE drivers.id_driver=drivers_class.id_driver AND drivers_class.season={$year} order by surname desc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias FROM drivers order by surname desc LIMIT 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzedni kierowca
if (($currentDriverCnt-1)<0){
  $prevId=$max;
}else{
  $prevId = $driversTab[$currentDriverCnt-1];
}
// nastepny kierowca
if (($currentDriverCnt+1)>=$maxDriverCnt){
  $nextId=$min;
}else{
  $nextId = $driversTab[$currentDriverCnt+1];
}

// pobranie id
$query="SELECT id_driver id FROM drivers WHERE alias='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
}

// informacje o kierowcy
$query="SELECT alias,id_driver id, name, surname, country, country_code countryCode, lower(country_short) countryShort, '' prevId, '' nextId 
FROM drivers WHERE id_driver=$id";
$result = mysqli_query($dbhandle,$query);
$name;
$surname;
$alias;
$countryShort;
while($r = mysqli_fetch_assoc($result)) {
   $name=$r["name"];
   $surname=$r["surname"];
   $alias=$r["alias"];
   $countryShort=$r["countryShort"];
}

// teammates
$query="SELECT drivers.id_driver id, drivers.name, drivers.surname, drivers.country_code countryCode, drivers.alias,
GROUP_CONCAT(DISTINCT season SEPARATOR ', ') seasons,
GROUP_CONCAT(DISTINCT team SEPARATOR ', ') teams 
FROM drivers_gp_results 
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_results.id_driver 
WHERE race_date IN (SELECT race_date FROM drivers_gp_results WHERE id_driver=$id) AND drivers_gp_results.id_driver!=$id 
AND drivers_gp_results.id_team IN (SELECT id_team FROM drivers_gp_results dgr WHERE dgr.id_driver=$id AND dgr.season=drivers_gp_results.season AND dgr.id_gp=drivers_gp_results.id_gp AND dgr.co_driver=0) 
AND drivers_gp_results.co_driver=0
GROUP BY drivers_gp_results.id_driver 
ORDER BY drivers.surname ASC";
$result = mysqli_query($dbhandle,$query);
$driverTeammatesItems=array();
$teammates=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_driver_id = $r["id"];
  $teammates["id"] = $r["id"];  
  $teammates["name"] = $r["name"];  
  $teammates["surname"] = $r["surname"];  
  $teammates["countryCode"] = $r["countryCode"];  
  $teammates["alias"] = $r["alias"]; 
  $teammates["seasons"] = $r["seasons"]; 
  $teammates["teams"] = $r["teams"];  
  
  $query2="SELECT 
  SUM(race_pos<(SELECT race_pos FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id  AND dgr.co_driver=0)) driverWins,
  SUM(race_pos>(SELECT race_pos FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id  AND dgr.co_driver=0)) teammateWins
  FROM drivers_gp_results WHERE id_driver=$id AND co_driver=0";
  $result2 = mysqli_query($dbhandle,$query2);
  while($r2 = mysqli_fetch_assoc($result2)) {
    $teammates["teammateWins"] = $r2["teammateWins"];
    $teammates["driverWins"] = $r2["driverWins"];
    $teammates["total"] = $r2["teammateWins"] + $r2["driverWins"];
  }
  
  $query3="SELECT 
  SUM(qual_pos<(SELECT qual_pos FROM drivers_pp_results dgr WHERE dgr.id_gp=drivers_pp_results.id_gp AND dgr.id_team=drivers_pp_results.id_team AND dgr.season=drivers_pp_results.season AND dgr.id_driver=$tmp_driver_id)) driverPP,
  SUM(qual_pos>(SELECT qual_pos FROM drivers_pp_results dgr WHERE dgr.id_gp=drivers_pp_results.id_gp AND dgr.id_team=drivers_pp_results.id_team AND dgr.season=drivers_pp_results.season AND dgr.id_driver=$tmp_driver_id)) teammatePP
  FROM drivers_pp_results WHERE id_driver=$id";
  $result3 = mysqli_query($dbhandle,$query3);
  while($r3 = mysqli_fetch_assoc($result3)) {
    $teammates["teammatePP"] = $r3["teammatePP"];
    $teammates["driverPP"] = $r3["driverPP"];
    $teammates["totalPP"] = $r3["teammatePP"] + $r3["driverPP"];
  }

  $query4="SELECT 
  drivers_gp_results.season,
  drivers_gp_results.race_date raceDate,
  drivers_gp_results.id_gp gpId,
  gp.name gpName,
  gp.name_alias gpAlias,
  gp.name_short nameShort,
  gp.country_code gpCountryCode,
  gp.circuit circuit,
  gp.circuit_alias circuitAlias,
  drivers_gp_results.id_team teamId,
  teams.name teamName,
  teams.engine teamEngine,
  teams.alias_name teamAlias,
  drivers_gp_results.race_pos driverPos,
  drivers_gp_results.race_completed driverRaceCompleted,
  (SELECT race_pos FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id AND dgr.co_driver=0) teammatePos,
  (SELECT race_completed FROM drivers_gp_results dgr WHERE dgr.id_gp=drivers_gp_results.id_gp AND dgr.id_team=drivers_gp_results.id_team AND dgr.season=drivers_gp_results.season AND dgr.id_driver=$tmp_driver_id AND dgr.co_driver=0) teammateRaceCompleted,
  drivers_pp_results.qual_pos driverQualPos,
  (SELECT qual_pos FROM drivers_pp_results dgr WHERE dgr.id_gp=drivers_pp_results.id_gp AND dgr.id_team=drivers_pp_results.id_team AND dgr.season=drivers_pp_results.season AND dgr.id_driver=$tmp_driver_id) teammateQualPos  
  FROM drivers_gp_results 
  LEFT JOIN gp ON gp.id_gp=drivers_gp_results.id_gp
  LEFT JOIN teams ON teams.id_team=drivers_gp_results.id_team
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp=drivers_gp_results.id_drivers_pp
  WHERE drivers_gp_results.id_driver=$id
  AND drivers_gp_results.co_driver=0
  HAVING teammatePos is not null
  ORDER By drivers_gp_results.race_date DESC";
  $driverTeammatesGpItems=array();
  $result4 = mysqli_query($dbhandle,$query4);
  while($r4 = mysqli_fetch_assoc($result4)) {
    $driverTeammatesGpItems[] = $r4;
  }
  $teammates["gp"] = $driverTeammatesGpItems;

  mysqli_free_result($result2);
  mysqli_free_result($result3);
  mysqli_free_result($result4);

  $driverTeammatesItems[] = $teammates;  
}
$items["prevId"]=$prevId;
$items["nextId"]=$nextId;
$items["id"]=$id;
$items["name"]=$name;
$items["surname"]=$surname;
$items["alias"]=$alias;
$items["countryShort"]=$countryShort;
$items["teammates"]=$driverTeammatesItems;

// Response
$response = $items;

print json_encode($response);
mysqli_free_result($result);
?>
