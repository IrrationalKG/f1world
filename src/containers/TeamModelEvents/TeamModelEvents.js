/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Grid,
  Loader,
  Segment,
  Flag,
  Form,
  Table,
  Item,
  Statistic,
  Pagination,
  Message,
  Header,
  Image,
  Divider,
  Label,
  Icon,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchTeamModelEvents } from "../../actions/TeamModelEventsActions";
const cmbEvents = require("../../json/combo_grouped_events.json");
const cmbEventsEn = require("../../json/combo_grouped_events_en.json");
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./TeamModelEvents.less";

class TeamModelEvents extends Component {
  constructor(props) {
    super(props);

    const comboEvents = Cookies.get("lang") == "pl" ? cmbEvents : cmbEventsEn;

    let paramTeamModelIdValue = "-";
    if (
      props.match.params.teamModelId !== undefined &&
      props.match.params.teamModelId !== ""
    ) {
      paramTeamModelIdValue = props.match.params.teamModelId;
    }

    let paramSeasonValue = "1";
    if (
      props.match.params.season !== undefined &&
      props.match.params.season !== ""
    ) {
      paramSeasonValue = props.match.params.season;
    }

    let paramDriverValue = "1";
    if (
      props.match.params.driverId !== undefined &&
      props.match.params.driverId !== ""
    ) {
      paramDriverValue = props.match.params.driverId;
    }

    let paramTeamValue = "1";
    if (
      props.match.params.teamId !== undefined &&
      props.match.params.teamId !== ""
    ) {
      paramTeamValue = props.match.params.teamId;
    }

    let paramEventValue = "-";
    let shouldShowInputPlace = false;
    if (
      props.match.params.event !== undefined &&
      props.match.params.event !== ""
    ) {
      paramEventValue = props.match.params.event;
      if (
        paramEventValue == "race-places" ||
        paramEventValue == "qual-places" ||
        paramEventValue == "grid-places" ||
        paramEventValue == "wins-from-place" ||
        paramEventValue == "podium-from-place" ||
        paramEventValue == "points-from-place" ||
        paramEventValue == "completed-from-place" ||
        paramEventValue == "incomplete-from-place" ||
        paramEventValue == "finished-from-place" ||
        paramEventValue == "retirement-from-place"
      ) {
        shouldShowInputPlace = true;
      } else {
        shouldShowInputPlace = false;
      }
    }

    let paramPlaceValue = "1";
    if (
      props.match.params.place !== undefined &&
      props.match.params.place !== ""
    ) {
      paramPlaceValue = props.match.params.place;
    }

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      shouldShowInputPlace: shouldShowInputPlace,
      comboEventsWidth: "16",
      comboTeamsWidth: "16",
      inputPlaceWidth: "2",

      paramTeamModelIdValue: paramTeamModelIdValue,
      paramTeamValue: paramTeamValue,
      paramDriverValue: paramDriverValue,
      paramSeasonValue: paramSeasonValue,
      paramEventValue: paramEventValue,
      paramPlaceValue: paramPlaceValue,

      comboEvents: comboEvents,

      selectTeamModelIdsValue: {
        value: paramTeamModelIdValue,
      },
      selectTeamsValue: {
        value: paramTeamValue,
      },
      selectDriversValue: {
        value: paramDriverValue,
      },
      selectSeasonValue: {
        value: paramSeasonValue,
      },
      selectEventsValue: {
        value: paramEventValue,
        label: this.findObjectById(comboEvents, props.match.params.event).label,
      },
      selectPlaceValue: paramPlaceValue,

      activePage: 1,
      boundaryRange: 1,
      siblingRange: 2,
      isSubmited: false,
    };
    this.handleEventsChange = this.handleEventsChange.bind(this);
    this.handlePlaceChange = this.handlePlaceChange.bind(this);
    this.handlePaginationChange = this.handlePaginationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { props, state } = this;
    props.fetchTeamModelEvents(
      "1",
      state.selectTeamModelIdsValue?.value,
      state.selectEventsValue?.value,
      state.selectPlaceValue,
      state.selectSeasonValue?.value,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value,
      Cookies.get("lang")
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    if (state.isSubmited !== prevState.isSubmited && state.isSubmited) {
      this.setState({
        isSubmited: false,
      });
      props.fetchTeamModelEvents(
        "1",
        state.selectTeamModelIdsValue?.value,
        state.selectEventsValue?.value,
        state.selectPlaceValue,
        state.selectSeasonValue?.value,
        state.selectDriversValue?.value,
        state.selectTeamsValue?.value,
        Cookies.get("lang")
      );
    }
  }

  handlePaginationChange = (e, { activePage }) => {
    const { props, state } = this;
    this.setState({ activePage });
    props.fetchTeamModelsEvents(
      activePage,
      state.selectTeamModelIdsValue?.value,
      state.selectEventsValue?.value,
      state.selectPlaceValue,
      state.selectSeasonValue?.value,
      state.selectDriversValue?.value,
      state.selectTeamsValue?.value
    );
  };

  handleEventsChange(event) {
    let showInputPlace = false;
    let comboEventsWidth = "16";
    let comboTeamsWidth = "16";
    switch (event.value) {
      case "race-places":
      case "qual-places":
      case "grid-places":
      case "wins-from-place":
      case "podium-from-place":
      case "points-from-place":
      case "completed-from-place":
      case "incomplete-from-place":
        showInputPlace = true;
        comboEventsWidth = "16";
        comboTeamsWidth = "16";
        break;
      default:
        break;
    }
    this.setState({
      isSubmited: true,
      paramEventValue: event.value,
      selectEventsValue: event,
      shouldShowInputPlace: showInputPlace,
      comboEventsWidth: comboEventsWidth,
      comboTeamsWidth: comboTeamsWidth,
    });
  }

  handlePlaceChange(event) {
    let paramValue = "1";
    if (event.target.value !== undefined && event.target.value !== "") {
      paramValue = event.target.value;
    }
    this.setState({
      paramPlaceValue: paramValue,
      selectPlaceValue: paramValue,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { props, state } = this;
    this.setState({
      isSubmited: true,
      activePage: 1,
    });
    props.history.push(
      `/team-model-events/${state.selectEventsValue.value}/${state.selectTeamModelIdsValue.value}/${state.selectSeasonValue.value}/${state.selectDriversValue.value}/${state.selectTeamsValue.value}`
    );
  }

  findObjectById(arr, value) {
    for (const obj of arr) {
      if (obj.value === value) {
        return obj;
      }
      if (obj.options && obj.options.length > 0) {
        const result = this.findObjectById(obj.options, value);
        if (result) {
          return result;
        }
      }
    }
    return null;
  }

  renderEvents() {
    const items = [];
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    const { results } = props.teamModelEvents.data;
    results.forEach((item, idx) => {
      let samePlace = false;
      if (idx > 0) {
        samePlace = item.place === results[idx - 1].place ? true : false;
      }
      let isMoreThanOnePlace = false;
      if (idx + 1 < results.length) {
        isMoreThanOnePlace =
          item.place === results[idx + 1].place ? true : false;
      }
      const cellBorder = isMoreThanOnePlace
        ? " cell-bottom-border cell-top-no-border"
        : "";
      const firstCellBorder = isMoreThanOnePlace
        ? " first-cell-bottom-border"
        : " ";
      const markedPPCell = item.gridPos == "1" ? "cell-1" : "";
      const markedGPCell =
        item.race == "1" || item.race == "2" || item.race == "3"
          ? `cell-${item.race}`
          : "";
      const markedSPCell =
        item.sprintPos == "1" || item.sprintPos == "2" || item.sprintPos == "3"
          ? `cell-${item.sprintPos}`
          : "";
      const markedBLCell = item.bestLap == "1" ? "cell-bl" : "";
      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const teamPic = `/build/images/teams/team_${item.team.toLowerCase()}_profile_logo.jpg`;
      const driverPic = `/build/images/drivers/driver_${item.idDriver}_profile.jpg`;
      const element = (
        <Table.Row key={`${item.season}_${item.alias}_${item.driverAlias}`}>
          <Table.Cell
            data-title="#"
            className={`no-wrap${firstCellBorder} bold`}
          >
            {samePlace ? "" : `${item.place}.`}
          </Table.Cell>
          {samePlace ? (
            <Table.Cell
              data-title="Data"
              className={`${cellBorder}`}
            ></Table.Cell>
          ) : (
            <Table.Cell
              data-title="Data"
              className={`left no-wrap${cellBorder} bold`}
            >
              <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                {item.raceDate}
              </NavLink>
            </Table.Cell>
          )}
          {samePlace ? (
            <Table.Cell
              data-title="Grand Prix"
              className={cellBorder}
            ></Table.Cell>
          ) : (
            <Table.Cell
              data-title="Grand Prix"
              className={`left no-wrap${cellBorder}`}
            >
              <div className="box-image-name">
                <div>
                  <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                    <Image
                      size="tiny"
                      src={gpPic}
                      alt={item.circuitAlias}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/circuits/circuit_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                </div>
                <div>
                  <div>
                    <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                      {item.gpname}{" "}
                    </NavLink>
                  </div>
                  <div>
                    <small>{item.circuit}</small>
                  </div>
                </div>
              </div>
            </Table.Cell>
          )}
          <Table.Cell
            data-title="Zespół"
            className={`left no-wrap ${cellBorder}`}
          >
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.teamAlias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.teamName}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.teamCountryCode} />{" "}
                  <NavLink to={`/team/${item.teamAlias}`}>
                    {item.teamName}
                  </NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          <Table.Cell
            data-title="Silnik"
            className={`left no-wrap${cellBorder}`}
          >
            {item.engine ? item.engine : item.teamName}
          </Table.Cell>
          <Table.Cell
            data-title="Model"
            className={`left no-wrap${cellBorder}`}
          >
            {item.model?.replace("_", "/").replace("?", "")}
          </Table.Cell>
          <Table.Cell
            data-title="Opony"
            className={`left no-wrap${cellBorder}`}
          >
            {item.tyre}
          </Table.Cell>
          <Table.Cell
            data-title="Numer"
            className={`left no-wrap${cellBorder}`}
          >
            {item.number}
          </Table.Cell>
          <Table.Cell
            data-title="Kierowca"
            className={`left no-wrap${cellBorder}`}
          >
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.driverAlias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.driver}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.driverCountryCode} />
                  <NavLink to={`/driver/${item.driverAlias}`}>
                    {item.driver}
                  </NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName ? (
                      item.modelName
                    ) : (
                      <FormattedMessage id={"app.stats.private"} />
                    )}
                  </small>
                </div>
                {item.sharedDrive != "" && (
                  <div>
                    <small className="cell-info">
                      <Icon name="linkify" size="small" />{" "}
                      {`${formatMessage({
                        id: "app.stats.shared.drive",
                      })}: ${item.sharedDrive}`}
                    </small>
                  </div>
                )}
              </div>
            </div>
          </Table.Cell>
          {state.selectEventsValue.value === "sprints" ||
          state.selectEventsValue.value === "sprint-wins" ||
          state.selectEventsValue.value === "sprint-podiums" ||
          state.selectEventsValue.value === "sprint-points" ||
          state.selectEventsValue.value === "sprint-completed" ||
          state.selectEventsValue.value === "sprint-incompleted" ? (
            <>
              <Table.Cell
                data-title="Sprint"
                className={`no-wrap ${cellBorder} ${markedSPCell}`}
              >
                {item.sprintCompleted > 0 && item.sprintPos}
                {item.sprintPos != null &&
                  item.sprintCompleted < 1 &&
                  item.sprintInfo === "dyskwalifikacja" && (
                    <FormattedMessage id={"app.table.header.disq"} />
                  )}
                {item.sprintPos != null &&
                  item.sprintCompleted < 1 &&
                  item.sprintInfo !== "dyskwalifikacja" &&
                  "-"}
              </Table.Cell>
              <Table.Cell
                data-title="Punkty"
                className={`no-wrap ${cellBorder}`}
              >
                {item.sprintPoints}
              </Table.Cell>
              <Table.Cell data-title="Info" className={`left ${cellBorder}`}>
                {item.sprintInfo !== "" && (
                  <div>
                    <small className="cell-info">{item.sprintInfo}</small>
                  </div>
                )}
              </Table.Cell>
            </>
          ) : (
            <>
              <Table.Cell
                data-title="Kwalifikacje"
                className={`no-wrap${cellBorder}`}
              >
                {item.qual == null && "-"}
                {item.qual != null && item.qualCompleted > 0 && item.qual}
                {item.qual != null && item.qualCompleted < 1 && "-"}
              </Table.Cell>
              <Table.Cell
                data-title="Pole Position"
                className={`no-wrap${cellBorder} ${markedPPCell}`}
              >
                {item.gridPos == null && "-"}
                {item.gridPos != null && item.gridPos}
              </Table.Cell>
              <Table.Cell
                data-title="Wyścig"
                className={`no-wrap${cellBorder} ${markedGPCell}`}
              >
                {item.race == null && item.notQualified == 1 && (
                  <FormattedMessage id={"app.stats.nq"} />
                )}
                {item.race == null && item.notStarted == 1 && (
                  <FormattedMessage id={"app.stats.ns"} />
                )}
                {item.race != null && item.raceCompleted > 0 && item.race}
                {item.race != null &&
                  item.raceCompleted < 1 &&
                  item.disq == 1 && (
                    <FormattedMessage id={"app.table.header.disq"} />
                  )}
                {item.race != null &&
                  item.raceCompleted < 1 &&
                  item.disq == 0 &&
                  "-"}
              </Table.Cell>
              <Table.Cell
                data-title="Naj. okrąż."
                id="bestLap"
                className={`no-wrap${cellBorder} ${markedBLCell}`}
              >
                {item.bestLap > 0 && (
                  <FormattedMessage id={"app.table.header.bestlaps"} />
                )}
                {item.bestLap < 1 && "-"}
              </Table.Cell>
              <Table.Cell
                data-title="Punkty"
                className={`no-wrap${cellBorder}`}
              >
                {item.season > 1957
                  ? item.excluded === "0"
                    ? item.racePoints > 0
                      ? item.racePoints
                      : item.sprintPoints == 0
                      ? "-"
                      : ""
                    : `(${item.points})`
                  : "-"}
                {item.sprintPoints > 0 && item.racePoints > 0 && (
                  <>
                    {"+"}
                    {item.sprintPoints}
                    <small>
                      <sup>SP</sup>
                    </small>
                  </>
                )}
                {item.sprintPoints > 0 && item.racePoints == 0 && (
                  <>
                    {item.sprintPoints}
                    <small>
                      <sup>SP</sup>
                    </small>
                  </>
                )}
              </Table.Cell>
              <Table.Cell data-title="Info" className={`left ${cellBorder}`}>
                {item.qualInfo !== "" && (
                  <div>
                    <small className="cell-info">{item.qualInfo}</small>
                  </div>
                )}
                {item.gridInfo !== "" && item.gridInfo !== item.qualInfo && (
                  <div>
                    <small className="cell-info">{item.gridInfo}</small>
                  </div>
                )}
                {item.info !== "" && (
                  <div>
                    <small className="cell-info">
                      {" "}
                      {`${item.info} (${formatMessage({
                        id: "app.stats.lap",
                      })} ${item.laps})`}
                    </small>
                  </div>
                )}
                {item.sprintPoints == 1 && (
                  <div>
                    <small className="cell-text">
                      {formatMessage({
                        id: "app.stats.points.sprint.part1b",
                      }) +
                        item.sprintPoints +
                        formatMessage({
                          id: "app.stats.points.sprint.part2",
                        }) +
                        item.sprintPos +
                        formatMessage({
                          id: "app.stats.points.sprint.part3",
                        })}
                    </small>
                  </div>
                )}
                {item.sprintPoints > 1 && (
                  <div>
                    <small className="cell-text">
                      {formatMessage({
                        id: "app.stats.points.sprint.part1",
                      }) +
                        item.sprintPoints +
                        formatMessage({
                          id: "app.stats.points.sprint.part2",
                        }) +
                        item.sprintPos +
                        formatMessage({
                          id: "app.stats.points.sprint.part3",
                        })}
                    </small>
                  </div>
                )}
              </Table.Cell>
            </>
          )}
        </Table.Row>
      );
      items.push(element);
    });
    return items;
  }

  renderEventsMobile() {
    const elements = [];
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    const { results } = props.teamModelEvents.data;
    results.forEach((item) => {
      const markedSPCell =
      item.sprintPos == "1" || item.sprintPos == "2" || item.sprintPos == "3"
        ? `cell-${item.sprintPos}`
        : "";
      const gpPic = `/build/images/countries/${item.nameShort.toLowerCase()}.jpg`;
      const element = (
        <Grid.Row
          key={`${item.season}_${item.raceDate}_${item.alias}_${item.driverAlias}`}
        >
          <Grid.Column width={3}>
            <Segment basic padded textAlign="center">
              <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                <Image
                  size="tiny"
                  src={gpPic}
                  alt={item.circuitAlias}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/circuits/circuit_no_profile.jpg";
                  }}
                />
              </NavLink>
            </Segment>
          </Grid.Column>
          <Grid.Column width={11}>
            <Item.Group>
              <Item>
                <Item.Content verticalAlign="middle">
                  <Item.Header>
                    {item.place}.{" "}
                    <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                      {item.gpname}
                    </NavLink>
                    {" ("}
                    <NavLink to={`/circuit/${item.circuitAlias}`}>
                      {item.circuit}
                    </NavLink>
                    {") "}
                  </Item.Header>
                  <Item.Description>
                    <NavLink to={`/gp-result/${item.alias}/${item.season}`}>
                      {item.raceDate}
                    </NavLink>
                  </Item.Description>
                  <Item.Description>
                    <NavLink to={`/driver/${item.driverAlias}`}>
                      {item.driver}
                    </NavLink>
                    {item.sharedDrive != "" && (
                      <div>
                        <small className="cell-info">
                          <Icon name="linkify" size="small" />{" "}
                          {`${formatMessage({
                            id: "app.stats.shared.drive",
                          })}: ${item.sharedDrive}`}
                        </small>
                      </div>
                    )}
                  </Item.Description>
                  <Item.Description>
                    {item.engine ? item.engine : item.teamName}{" "}
                    {item.model?.replace("_", "/").replace("?", "")}
                  </Item.Description>
                  <Item.Description>{item.tyre}</Item.Description>
                  {state.selectEventsValue.value === "sprints" ||
                  state.selectEventsValue.value === "sprint-wins" ||
                  state.selectEventsValue.value === "sprint-podiums" ||
                  state.selectEventsValue.value === "sprint-points" ||
                  state.selectEventsValue.value === "sprint-completed" ||
                  state.selectEventsValue.value === "sprint-incompleted" ? (
                    <>
                      <Item.Description>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.sprint"} />
                          </div>
                          <div
                            className={`key-value-box-value ${markedSPCell}`}
                          >
                            {item.sprintCompleted > 0 && item.sprintPos}
                            {item.sprintPos != null &&
                              item.sprintCompleted < 1 &&
                              item.sprintInfo === "dyskwalifikacja" && (
                                <FormattedMessage
                                  id={"app.table.header.disq"}
                                />
                              )}
                            {item.sprintPos != null &&
                              item.sprintCompleted < 1 &&
                              item.sprintInfo !== "dyskwalifikacja" &&
                              "-"}
                          </div>
                        </span>
                      </Item.Description>
                      {item.sprintInfo !== "" && (
                        <Item.Extra>
                          <small className="cell-info">{item.sprintInfo}</small>
                        </Item.Extra>
                      )}
                    </>
                  ) : (
                    <>
                      <Item.Description>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.qual"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.qual == null && "-"}
                            {item.qual != null &&
                              item.qualCompleted > 0 &&
                              item.qual}
                            {item.qual != null && item.qualCompleted < 1 && "-"}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.polepos"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.gridPos == null && "-"}
                            {item.gridPos != null && item.gridPos}
                          </div>
                        </span>
                        <span className="key-value-box">
                          <div className="key-value-box-header">
                            <FormattedMessage id={"app.table.header.gp"} />
                          </div>
                          <div className="key-value-box-value">
                            {item.race == null && item.notQualified == 1 && (
                              <FormattedMessage id={"app.stats.nq"} />
                            )}
                            {item.race == null && item.notStarted == 1 && (
                              <FormattedMessage id={"app.stats.ns"} />
                            )}
                            {item.race != null &&
                              item.raceCompleted > 0 &&
                              item.race}
                            {item.race != null &&
                              item.raceCompleted < 1 &&
                              item.disq == 1 && (
                                <FormattedMessage
                                  id={"app.table.header.disq"}
                                />
                              )}
                            {item.race != null &&
                              item.raceCompleted < 1 &&
                              item.disq == 0 &&
                              "-"}
                          </div>
                        </span>
                        {item.bestLap == "1" && (
                          <span className="key-value-box">
                            <div className="key-value-box-header">
                              <FormattedMessage
                                id={"app.table.header.bestlaps"}
                              />
                            </div>
                            <div className="key-value-box-value">1</div>
                          </span>
                        )}
                      </Item.Description>
                      {item.qualInfo !== "" && (
                        <Item.Extra>
                          <small className="cell-info">{item.qualInfo}</small>
                        </Item.Extra>
                      )}
                      {item.gridInfo !== "" &&
                        item.gridInfo !== item.qualInfo && (
                          <Item.Extra>
                            <small className="cell-info">{item.gridInfo}</small>
                          </Item.Extra>
                        )}
                      {item.info !== "" && (
                        <Item.Extra>
                          <small className="cell-info">
                            {" "}
                            {`${item.info} (${formatMessage({
                              id: "app.stats.lap",
                            })} ${item.laps})`}
                          </small>
                        </Item.Extra>
                      )}
                      {item.sprintPoints == 1 && (
                        <Item.Description>
                          <small>
                            {formatMessage({
                              id: "app.stats.points.sprint.part1b",
                            }) +
                              item.sprintPoints +
                              formatMessage({
                                id: "app.stats.points.sprint.part2",
                              }) +
                              item.sprintPos +
                              formatMessage({
                                id: "app.stats.points.sprint.part3",
                              })}
                          </small>
                        </Item.Description>
                      )}
                      {item.sprintPoints > 1 && (
                        <Item.Description>
                          <small>
                            {formatMessage({
                              id: "app.stats.points.sprint.part1",
                            }) +
                              item.sprintPoints +
                              formatMessage({
                                id: "app.stats.points.sprint.part2",
                              }) +
                              item.sprintPos +
                              formatMessage({
                                id: "app.stats.points.sprint.part3",
                              })}
                          </small>
                        </Item.Description>
                      )}{" "}
                    </>
                  )}
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={2}>
            <Statistic floated="right">
              <Statistic.Value>
                {state.selectEventsValue.value === "sprints" ||
                state.selectEventsValue.value === "sprint-wins" ||
                state.selectEventsValue.value === "sprint-podiums" ||
                state.selectEventsValue.value === "sprint-points" ||
                state.selectEventsValue.value === "sprint-completed" ||
                state.selectEventsValue.value === "sprint-incompleted" ? (
                  <>{item.sprintPoints}</>
                ) : (
                  <>
                    {item.season > 1957
                      ? item.excluded === "0"
                        ? item.racePoints > 0
                          ? item.racePoints
                          : item.sprintPoints == 0
                          ? "-"
                          : ""
                        : `(${item.points})`
                      : "-"}
                    {item.sprintPoints > 0 && item.racePoints > 0 && (
                      <>
                        {"+"}
                        {item.sprintPoints}
                        <small className="very">
                          <sup>SP</sup>
                        </small>
                      </>
                    )}
                    {item.sprintPoints > 0 && item.racePoints == 0 && (
                      <>
                        {item.sprintPoints}
                        <small className="very">
                          <sup>SP</sup>
                        </small>
                      </>
                    )}
                  </>
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Grid.Column>
        </Grid.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { teamModelEvents } = this.props;
    const { state } = this;
    const { formatMessage } = this.props.intl;

    if (!teamModelEvents.data || teamModelEvents.loading) {
      return (
        <section id="events" name="events" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const {
      isLoadingExternally,
      multi,
      ignoreCase,
      ignoreAccents,
      clearable,
      selectEventsValue,
      selectSeasonValue,
      selectPlaceValue,
      selectDriversValue,
      shouldShowInputPlace,
      comboEventsWidth,
      inputPlaceWidth,
      activePage,
      boundaryRange,
      siblingRange,
    } = this.state;

    const picProfile = `/build/images/teams/team_${teamModelEvents.data.teamShortName?.toLowerCase()}_profile_logo.jpg`;
    return (
      <section
        id="team-model-events"
        name="team-model-events"
        className="section-page"
      >
        <SectionPageBanner
          title={teamModelEvents.data.teamName}
          subtitle={
            <FormattedMessage id={"app.page.team.model.events.subtitle"} />
          }
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                {teamModelEvents.data.models.map((model, idx) => (
                  <NavLink
                    key={idx}
                    to={`/team/${teamModelEvents.data.teamAlias}`}
                  >
                    <Segment basic>
                      <Image
                        centered
                        src={`/build/images/teams/models/team_${
                          teamModelEvents.data.teamShortName
                        }_${model
                          ?.toLowerCase()
                          .replace("???", "3")
                          .replace("??", "2")
                          .replace("?", "1")}.jpg`}
                        alt={`${
                          teamModelEvents.data.teamName
                        } ${model?.replaceAll("???", "3")}`}
                        onError={(e) => {
                          e.target.src = "/build/images/teams/team_no_car.jpg";
                        }}
                      />
                      <Label attached="top left" color="red">
                        {teamModelEvents.data.teamName} {model}
                      </Label>
                    </Segment>
                  </NavLink>
                ))}
                <Grid centered className="stats-box">
                  <Grid.Column mobile={8} tablet={16} computer={16}>
                    <NavLink to={`/team/${teamModelEvents.data.teamAlias}`}>
                      <Image
                        centered
                        src={picProfile}
                        alt={teamModelEvents.data.teamName}
                        onError={(e) => {
                          e.target.src =
                            "/build/images/teams/team_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                  </Grid.Column>
                  <Grid.Column
                    mobile={8}
                    tablet={16}
                    computer={16}
                    textAlign="center"
                    verticalAlign="middle"
                  >
                    <div>
                      <Header size="large" textAlign="center" inverted>
                        {teamModelEvents.data.teamName}{" "}
                        {teamModelEvents.data.model?.replaceAll("???", "3")}
                      </Header>
                      {selectSeasonValue.value != "-" && (
                        <Header textAlign="center" inverted>
                          <FormattedMessage id={"app.stats.season"} />{" "}
                          {selectSeasonValue.value}
                        </Header>
                      )}
                      <Segment basic textAlign="center">
                        <Statistic size="large" inverted>
                          <Statistic.Value>
                            {teamModelEvents.data.total}
                          </Statistic.Value>
                          <Statistic.Label>
                            <FormattedMessage id={"app.stats.gp"} />
                          </Statistic.Label>
                        </Statistic>
                      </Segment>
                      {selectDriversValue.value == "-" ? (
                        <div className="buttons">
                          <NavLink
                            className="primary"
                            to={`/team/${teamModelEvents.data.teamAlias}`}
                          >
                            <FormattedMessage id={"app.button.profile"} />
                          </NavLink>{" "}
                          <NavLink
                            className="secondary"
                            to={`/team-models/${teamModelEvents.data.teamAlias}`}
                          >
                            <FormattedMessage id={"app.button.models"} />
                          </NavLink>
                        </div>
                      ) : (
                        <div className="buttons">
                          <NavLink
                            className="primary"
                            to={`/driver/${selectDriversValue.value}`}
                          >
                            <FormattedMessage id={"app.button.profile"} />
                          </NavLink>{" "}
                          <NavLink
                            className="secondary"
                            to={`/driver-models/${selectDriversValue.value}`}
                          >
                            <FormattedMessage id={"app.button.models"} />
                          </NavLink>
                        </div>
                      )}
                    </div>
                  </Grid.Column>
                </Grid>
                <div className="section-page-filters events-filters">
                  <form onSubmit={this.handleSubmit}>
                    <Grid stackable padded>
                      <Grid.Row columns={1}>
                        <Grid.Column width={comboEventsWidth}>
                          <Segment basic>
                            <label htmlFor="events">
                              <FormattedMessage id={"app.stats.statistics"} />
                              <Select
                                isLoading={isLoadingExternally}
                                id="events"
                                name="events"
                                multi={multi}
                                ignoreCase={ignoreCase}
                                ignoreAccents={ignoreAccents}
                                value={selectEventsValue}
                                onChange={this.handleEventsChange}
                                options={state.comboEvents}
                                clearable={clearable}
                                labelKey="label"
                                valueKey="value"
                                placeholder={formatMessage({
                                  id: "app.placeholder.select.event",
                                })}
                                noResultsText={
                                  <FormattedMessage
                                    id={"app.placeholder.no.results"}
                                  />
                                }
                                className="react-select-container"
                                classNamePrefix="react-select"
                              />
                            </label>
                          </Segment>
                        </Grid.Column>
                        <Grid.Column
                          width={inputPlaceWidth}
                          className={shouldShowInputPlace ? "" : "hide"}
                        >
                          <Segment basic>
                            <Form.Input
                              fluid
                              label={
                                <FormattedMessage id={"app.stats.place"} />
                              }
                              onChange={this.handlePlaceChange}
                              defaultValue={selectPlaceValue}
                              placeholder={
                                <FormattedMessage
                                  id={"app.placeholder.place"}
                                />
                              }
                            />
                          </Segment>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                    <Divider hidden></Divider>
                  </form>
                </div>
                <Segment basic padded>
                  <Message className="inverted">
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <p>
                      <FormattedMessage id={"app.table.header.sprint"} /> -{" "}
                      <FormattedMessage id={"app.stats.sprint"} />
                      <br />
                      <FormattedMessage id={"app.table.header.qual"} /> -{" "}
                      <FormattedMessage id={"app.stats.qual"} />
                      <br />
                      <FormattedMessage id={"app.table.header.grid"} /> -{" "}
                      <FormattedMessage id={"app.stats.grid"} />
                      <br />
                      <FormattedMessage id={"app.table.header.race"} /> -{" "}
                      <FormattedMessage id={"app.stats.race"} />
                      <br />
                      <FormattedMessage
                        id={"app.table.header.bestlaps"}
                      /> - <FormattedMessage id={"app.stats.bestlap"} />
                      <br />
                      <FormattedMessage id={"app.stats.nq"} /> -{" "}
                      <FormattedMessage id={"app.stats.not.qualified"} />
                      <br />
                      <FormattedMessage id={"app.stats.ns"} /> -{" "}
                      <FormattedMessage id={"app.stats.not.started"} />
                      <br />
                      <FormattedMessage id={"app.table.header.disq"} /> -{" "}
                      <FormattedMessage id={"app.stats.disq"} />
                      <br />
                      <br />
                      <FormattedMessage id={"app.page.team.events.info"} />
                    </p>
                  </Message>
                </Segment>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <div className="section-page-content events">
                  <Segment basic>
                    <SectionPageHeader
                      title={state.selectEventsValue.label}
                      type="secondary"
                    />
                    {teamModelEvents.data.pages > 1 && (
                      <Segment padded="very" basic textAlign="center">
                        <>
                          <div className="hideForDesktop">
                            <Pagination
                              activePage={activePage}
                              onPageChange={this.handlePaginationChange}
                              totalPages={teamModelEvents.data.pages}
                              ellipsisItem={null}
                              boundaryRange="0"
                              siblingRange="1"
                              size="small"
                            />
                          </div>
                          <div className="hideForMobile">
                            <Pagination
                              activePage={activePage}
                              onPageChange={this.handlePaginationChange}
                              totalPages={teamModelEvents.data.pages}
                              ellipsisItem={null}
                              boundaryRange={boundaryRange}
                              siblingRange={siblingRange}
                              size="small"
                            />
                          </div>
                        </>
                      </Segment>
                    )}
                    <SectionPageHeader
                      title={`${formatMessage({
                        id: "app.placeholder.records.found",
                      })}: ${teamModelEvents.data.total}`}
                    />
                    <div className="hideForDesktop">
                      <Grid columns={2} divided="vertically">
                        {this.renderEventsMobile()}
                      </Grid>
                    </div>
                    <div className="hideForMobile">
                      <Segment basic padded className="overflow">
                        <Table
                          basic="very"
                          celled
                          className="responsive-table center-aligned"
                        >
                          <Table.Header>
                            <Table.Row>
                              <Table.HeaderCell>#</Table.HeaderCell>
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.date"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.gp.long"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.team"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.engine.long"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.model.long"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.tires"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell>#</Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.driver"}
                                />
                              </Table.HeaderCell>
                              {selectEventsValue.value === "sprints" ||
                              selectEventsValue.value === "sprint-wins" ||
                              selectEventsValue.value === "sprint-podiums" ||
                              selectEventsValue.value === "sprint-points" ||
                              selectEventsValue.value === "sprint-completed" ||
                              selectEventsValue.value ===
                                "sprint-incompleted" ? (
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sprint"}
                                  />
                                </Table.HeaderCell>
                              ) : (
                                <>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.qual"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.polepos"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.gp"}
                                    />
                                  </Table.HeaderCell>
                                  <Table.HeaderCell>
                                    <FormattedMessage
                                      id={"app.table.header.bestlaps"}
                                    />
                                  </Table.HeaderCell>
                                </>
                              )}
                              <Table.HeaderCell>
                                <FormattedMessage
                                  id={"app.table.header.points"}
                                />
                              </Table.HeaderCell>
                              <Table.HeaderCell className="left">
                                <FormattedMessage
                                  id={"app.table.header.info"}
                                />
                              </Table.HeaderCell>
                            </Table.Row>
                          </Table.Header>
                          <Table.Body>{this.renderEvents()}</Table.Body>
                        </Table>
                      </Segment>
                    </div>
                    {teamModelEvents.data.pages > 1 && (
                      <Segment padded="very" basic textAlign="center">
                        <>
                          <div className="hideForDesktop">
                            <Pagination
                              activePage={activePage}
                              onPageChange={this.handlePaginationChange}
                              totalPages={teamModelEvents.data.pages}
                              ellipsisItem={null}
                              boundaryRange="0"
                              siblingRange="1"
                              size="small"
                            />
                          </div>
                          <div className="hideForMobile">
                            <Pagination
                              activePage={activePage}
                              onPageChange={this.handlePaginationChange}
                              totalPages={teamModelEvents.data.pages}
                              ellipsisItem={null}
                              boundaryRange={boundaryRange}
                              siblingRange={siblingRange}
                              size="small"
                            />
                          </div>
                        </>
                      </Segment>
                    )}
                  </Segment>
                </div>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

TeamModelEvents.propTypes = {
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      teamId: PropTypes.string,
      event: PropTypes.string,
      season: PropTypes.string,
      place: PropTypes.string,
    }),
  }),
  fetchTeamModelEvents: PropTypes.func.isRequired,
  teamModelEvents: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
};

function mapStateToProps({ teamModelEvents }) {
  return {
    teamModelEvents,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchTeamModelEvents,
    },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(TeamModelEvents)
);
