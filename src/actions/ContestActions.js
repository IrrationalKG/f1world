// JSON
// const request = require('../json/contest.json');
// export const FETCH_CONTEST_JSON = 'FETCH_CONTEST_JSON';
// export function fetchContest() {
//   return {
//     type: FETCH_CONTEST_JSON,
//     payload: request
//   };
// }

// PHP
import axios from 'axios';
import { ROOT_URL } from '../config/config';

export const FETCH_CONTEST = 'FETCH_CONTEST';
export const FETCH_CONTEST_SUCCESS = 'FETCH_CONTEST_SUCCESS';
export const FETCH_CONTEST_ERROR = 'FETCH_CONTEST_ERROR';

function requestData() {
  return {
    type: FETCH_CONTEST
  };
}

function receiveData(data) {
  return {
    type: FETCH_CONTEST_SUCCESS,
    payload: data
  };
}

function receiveError(error) {
  return {
    type: FETCH_CONTEST_ERROR,
    payload: error
  };
}

export function fetchContest(year, gp) {
  const url = `${ROOT_URL}/get_contest.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios.get(url, {
      params: {
        year: year,
        gp: gp
      }
    })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
