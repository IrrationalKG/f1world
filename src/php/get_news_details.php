<?
header('Access-Control-Allow-Origin: *');

$id=$_GET['id'];
if ($id==null) $id=$_POST['id'];

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

// min news
$query="SELECT id_news id FROM news ORDER BY news_date asc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["id"];
}

// max news
$query="SELECT id_news id FROM news ORDER BY news_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["id"];
}

// poprzedni news
$query="SELECT id_news id,news_date,title FROM news WHERE news_date>=(SELECT news_date FROM news WHERE id_news=$id) order by news_date asc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$prevId=$max;
while($r = mysqli_fetch_assoc($result)) {
    $prevId = $r["id"];
}
// nastepny news
$query="SELECT id_news id,news_date,title FROM news WHERE news_date<=(SELECT news_date FROM news WHERE id_news=$id) order by news_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$nextId = $min;
while($r = mysqli_fetch_assoc($result)) {
  $nextId = $r["id"];
}

$query="SELECT id_news,id_news id,filename photo,";
if ($lang=='pl') {
  $query.="title,news,";
}else{
  $query.="title_en title, news_en news,";
} 
$query.="news_date newsDate,source,SUBSTRING(news_date,1,4) as newsyear,SUBSTRING(news_date,6,2) as newsmonth from news where news_type<>2 and id_news=$id";
$result = mysqli_query($dbhandle,$query);
$newsItems;
while($r = mysqli_fetch_assoc($result)) {

  //$news = str_replace("\r", "<br/>", $r["news"]);
  //$news = str_replace("<br/>", "", $news);
  $news = $r["news"];
  $news = str_replace("--Results--", "", $news);
  $endPos = strrpos($news, "--Results--");

  $r["idPrev"]=$prevId;
  $r["idNext"]=$nextId;
  $r["news"]=$news;
  $r["query"]=$query;
  $newsItems = $r;
}

// Response
$response = $newsItems;

print json_encode($response);
mysqli_free_result($result);
?>
