import React from "react";
import { render } from "react-dom";
// import ReactDOM from "react-dom/client";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { loadingBarMiddleware } from "react-redux-loading-bar";
import logger from "redux-logger";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import ReduxPromise from "redux-promise";
import thunk from "redux-thunk";
import reducers from "./reducers";
import routes from "./routes";
import "./styles/semantic-ui/semantic.less";
import "./styles/main.less";

const reducer = combineReducers(reducers);

const middlewares = [
  ReduxPromise,
  thunk,
  loadingBarMiddleware({
    promiseTypeSuffixes: ["REQUEST", "SUCCESS", "ERROR"],
  }),
];

if (process.env.NODE_ENV === "development") {
  // middlewares.push(logger);
}

const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
const store = createStoreWithMiddleware(reducer);

render(
  <Provider store={store}>
    <BrowserRouter basename="/">
      {routes}
    </BrowserRouter>
  </Provider>,
  document.querySelector('#app')
);
// const root = ReactDOM.createRoot(document.querySelector('#app'));
// root.render(
//   <Provider store={store}>
//     <BrowserRouter basename="/">{routes}</BrowserRouter>
//   </Provider>
// );
