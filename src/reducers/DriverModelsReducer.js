import {
  FETCH_DRIVER_MODELS_REQUEST,
  FETCH_DRIVER_MODELS_SUCCESS,
  FETCH_DRIVER_MODELS_ERROR,
} from "../actions/DriverModelsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_DRIVER_MODELS_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_DRIVER_MODELS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_DRIVER_MODELS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
