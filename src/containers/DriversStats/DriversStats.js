/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Flag,
  Grid,
  Loader,
  Item,
  Header,
  Divider,
  Label,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import {
  fetchDriversStats,
  fetchDriversStatsByCountry,
} from "../../actions/DriversStatsActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriversStats.less";

class DriversStats extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderDriversStats = this.renderDriversStats.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    if (props.match.params.letter) {
      props.fetchDriversStats(props.match.params.letter, Cookies.get("lang"));
    }
    if (props.match.params.country) {
      props.fetchDriversStatsByCountry(
        props.match.params.country,
        Cookies.get("lang")
      );
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    const { letter } = props.match.params;
    const nextLetter = nextProps.match.params.letter;
    if (nextLetter && letter !== nextLetter) {
      props.fetchDriversStats(nextLetter, Cookies.get("lang"));
    }

    const { country } = props.match.params;
    const nextCountry = nextProps.match.params.country;
    if (nextCountry && country !== nextCountry) {
      props.fetchDriversStatsByCountry(nextCountry, Cookies.get("lang"));
    }
  }

  renderDriversStats() {
    const items = [];
    const { props } = this;
    const { drivers } = props.driversStats.data;
    const { formatMessage } = this.props.intl;

    drivers.forEach((item) => {
      let filename = "/build/images/drivers/driver_no_profile.jpg";
      if (item.picture === "1") {
        filename = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      }
      const detailsLink = `/driver/${item.alias}`;
      const record = (
        <Grid.Column
          key={item.alias}
          textAlign="center"
          mobile={8}
          tablet={4}
          computer={2}
          className="menu-box-container"
        >
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Segment basic>
                  <NavLink to={`/driver/${item.alias}`}>
                    <Image
                      centered
                      src={filename}
                      alt={item.name}
                      onError={(e) => {
                        e.target.src =
                          "/build/images/drivers/driver_no_profile.jpg";
                      }}
                    />
                  </NavLink>
                  {item.champ && (
                    <Label attached="bottom" color="red">
                      {item.champ.replace(
                        "Mistrz Świata",
                        formatMessage({ id: "app.stats.wc" })
                      )}
                    </Label>
                  )}
                </Segment>
                <Segment basic className="menu-box-flex">
                  <Divider hidden fitted></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <Flag name={item.countryCode} />
                      <NavLink to={`/driver/${item.alias}`}>
                        {item.name}
                      </NavLink>
                      <Header.Subheader>{item.country}</Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider></Divider>
                  <Header as="h2">
                    <Header.Content>
                      <Header.Subheader>{item.years}</Header.Subheader>
                    </Header.Content>
                  </Header>
                  <Divider hidden fitted></Divider>
                </Segment>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="left" className="info-box-light">
                <Item.Group divided>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.races"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/starts/${item.alias}`}
                          >
                            {item.starts}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.qual"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/qual/${item.alias}`}
                          >
                            {item.qual}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.points"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/points/${item.alias}`}
                          >
                            {item.points}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.wins"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/wins/${item.alias}`}
                          >
                            {item.wins}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.podium"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/podium/${item.alias}`}
                          >
                            {item.podium}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.polepos"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/polepos/${item.alias}`}
                          >
                            {item.polepos}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                  <Item>
                    <Item.Content verticalAlign="middle">
                      <Item.Header>
                        <FormattedMessage id={"app.stats.bestlaps"} />
                      </Item.Header>
                      <Item.Meta>
                        <span>
                          <NavLink
                            className="link"
                            to={`/driver-events/bestlaps/${item.alias}`}
                          >
                            {item.bestlaps}
                          </NavLink>
                        </span>
                      </Item.Meta>
                    </Item.Content>
                  </Item>
                </Item.Group>
                <div className="buttons">
                  <NavLink className="primary" to={detailsLink}>
                    <FormattedMessage id={"app.button.details"} />
                  </NavLink>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      );
      items.push(record);
    });
    return items;
  }

  renderLettersLabels() {
    const items = [];
    const { props } = this;
    const { letters } = props.driversStats.data;
    letters.forEach((item) => {
      const record = (
        <Label image key={item.letter}>
          <NavLink to={`/drivers-stats-list/${item.letter}`}>
            {item.letter}
          </NavLink>
          <Label.Detail>{item.amount}</Label.Detail>
        </Label>
      );
      items.push(record);
    });
    return items;
  }

  renderCountriesLabels() {
    const items = [];
    const { props } = this;
    const { countries } = props.driversStats.data;
    countries.forEach((item) => {
      const record = (
        <Label image key={item.countryCode}>
          <Flag name={item.countryCode} />
          <NavLink to={`/drivers-stats-list/country/${item.countryCode}`}>
            {item.countryShort}
          </NavLink>
          <Label.Detail>{item.amount}</Label.Detail>
        </Label>
      );
      items.push(record);
    });
    return items;
  }

  render() {
    const { props } = this;
    if (!props.driversStats.data || props.driversStats.loading) {
      return (
        <section
          id="drivers-stats"
          name="drivers-stats"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "/build/images/history/history_banner.jpg";

    return (
      <section id="drivers-stats" name="drivers-stats" className="section-page">
        <SectionPageBanner
          title={<FormattedMessage id={"app.page.drivers.stats.title"} />}
          subtitle={<FormattedMessage id={"app.page.drivers.stats.subtitle"} />}
        />
        <Segment basic>
          <Segment padded basic className="drivers-stats-letters">
            {this.renderLettersLabels()}
            <Label>
              <NavLink to="/drivers-stats-list/2025">2025</NavLink>
            </Label>
          </Segment>
          <Segment padded basic className="drivers-stats-countries hideMobile">
            {this.renderCountriesLabels()}
          </Segment>
          <div className="section-page-content">
            <Grid>
              {this.renderDriversStats()}
            </Grid>
          </div>
        </Segment>
      </section>
    );
  }
}
DriversStats.propTypes = {
  fetchDriversStats: PropTypes.func.isRequired,
  fetchDriversStatsByCountry: PropTypes.func.isRequired,
  driversStats: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  letter: PropTypes.string,
};

function mapStateToProps({ driversStats }) {
  return { driversStats };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchDriversStats, fetchDriversStatsByCountry },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(DriversStats)
);
