<?
header ("Access-Control-Allow-Origin: *");
header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
header ("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

// $userJson = $_GET["user"];
// if ($userJson == null){
//   $userJson = file_get_contents('php://input');
//   $user = json_decode($userJson, false);
//   $method = "POST";
// }else{
//   $user = json_decode($userJson, false);
//   $method = "GET";
// }

$userContent = file_get_contents('php://input');
$userJson = json_decode($userContent, false);
$method = "POST";

$name = $userJson->name;
$surname = $userJson->surname;
$email = $userJson->email;
$passwd = $userJson->password;
$country = $userJson->country;

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();
$response["method"] = $method;
$response["userContent"] = $userContent;
$response["userJson"] = $userJson;
$response["name"] = $name;
$response["surname"] = $surname;
$response["password"] = $passwd;
$response["email"] = $email;
$response["country"] = $country;

$query="select * from users where name='$name' and surname='$surname'";
$result = mysqli_query($dbhandle,$query);
$num=mysqli_num_rows($result);

if ($num>0) {
  $response["status"] = "ERROR-PLAYER-ALREADY-EXISTS";
}else{
  if ($surname=='') {
		$surname=$name;
		$short_name=$name;
		$name="";
    $alias=$surname;
    $user=$surname;
	}else{
		$s_name=substr($name,0,1);
		$short_name="$s_name.$surname";
    $alias=$name.'-'.$surname;
    $user=$name.' '.$surname;
	}
  setlocale(LC_CTYPE, 'pl_PL');
  $alias = iconv('UTF-8','ASCII//TRANSLIT',$alias);

  // dodanie gracza do bazy
  if ($surname=='' || $alias=='') {
    $response["status"] = "ERROR-PLAYER-NOT-SAVED";
  }else{
    $query2 = "INSERT INTO users (name,surname,user,alias,short_name,email,password,country) VALUES ('$name','$surname','$user','$alias','$short_name','$email','$passwd','$country')";
    mysqli_query($dbhandle,$query2);

    // sprawdzenie czy sie dodał
    $query3="SELECT id_user FROM users WHERE name='$name' AND surname='$surname' AND short_name='$short_name' AND email='$email' AND password='$passwd'";
    $result3=mysqli_query($dbhandle,$query3);
    $num3=mysqli_num_rows($result3);

    if ($num3>0) {
    	$response["status"] = "SUCCESS";
      mysqli_free_result($result3);
    }else{
      $response["status"] = "ERROR-PLAYER-NOT-SAVED";
    }
  }
}
mysqli_free_result($result);
print json_encode($response);
?>
