## Wymagania:
1. Zainstalowane node/npm  
    ```
    brew install node
    ```

## Instrukcja:
1. UtworzyÄ katalog projektu
  ```
  mkdir FOW2017
  ```

2. ZainstalowaÄÂ dependencies
  ```
  npm install
  ```

3. Uruchomienie serwera
  ```
  npm start
  ```

4. Aktualizacja zależności
  ```
  sudo npm cache clean -f
  sudo npm install -g n
  sudo n stable

  sudo npm i -g npm-check-updates
  ncu -u
  npm install
  ```
5. Informacje
react-select 2.4.2 nie działa (problem ze stylami)
less >3.0.0 nie działają aliasy

6. Nosync node_modules

rename the folder or file:
mv node_modules node_modules.nosync

Create a symlink that get's synced:
ln -s node_modules.nosync node_modules