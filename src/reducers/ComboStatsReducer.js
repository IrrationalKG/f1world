import { FETCH_COMBO_STATS_SUCCESS } from "../actions/ComboStatsActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_COMBO_STATS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    default:
      return state;
  }
};
