/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubgp","renderDriverSubgp"] }] */
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import {
  CardGroup,
  CardHeader,
  CardDescription,
  CardContent,
  Card,
} from "semantic-ui-react";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
const comboGroupedStats = require("../../json/combo_grouped_stats.json");
const comboGroupedStatsEn = require("../../json/combo_grouped_stats_en.json");
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriversRecordsList.less";

class DriversRecordsList extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderDriversRecordsList = this.renderDriversRecordsList.bind(this);
  }

  renderDriversRecordsList() {
    const items = [];
    const comboStats =
      Cookies.get("lang") == "pl" ? comboGroupedStats : comboGroupedStatsEn;
    if (comboStats) {
      comboStats.forEach((item) => {
        const { label, options } = item;
        const record = (
          <Card key={label}>
            <CardContent>
              <CardHeader>{label}</CardHeader>
              <CardDescription className="drivers-records-box">
                <ul>
                  {options.map((e) => {
                    return (
                      <NavLink
                        key={e.value}
                        to={`drivers-records/${e.value}/-/-/-/-/-/0/1/`}
                      >
                        <li>{e.label}</li>
                      </NavLink>
                    );
                  })}
                </ul>
              </CardDescription>
            </CardContent>
          </Card>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    return (
      <section
        id="drivers-records-list"
        name="drivers-records-list"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage id={"app.page.drivers.records.list.title"} />
          }
          subtitle={
            <FormattedMessage id={"app.page.drivers.records.list.subtitle"} />
          }
        />
        <div className="section-page-content">
          <CardGroup itemsPerRow={5} className="hideMobile hideTablet">
            {this.renderDriversRecordsList()}
          </CardGroup>
          <CardGroup itemsPerRow={3} className="hideMobile hideDesktop">
            {this.renderDriversRecordsList()}
          </CardGroup>
          <CardGroup itemsPerRow={1} className="hideDesktop hideTablet">
            {this.renderDriversRecordsList()}
          </CardGroup>
        </div>
      </section>
    );
  }
}

export default DriversRecordsList;
