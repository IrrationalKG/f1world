/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { Segment, Message, Divider } from "semantic-ui-react";
import { FormattedMessage } from "react-intl";
import styles from "./Regulations.less";

class Regulations extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderRegulations = this.renderRegulations.bind(this);
  }

  renderRegulations() {
    const { props } = this;
    const regulations = props.regulations;

    let idx = 0;
    const regulationsText = regulations.split("\n").map((item) => {
      idx += 1;
      if (item !== "\n") {
        if (
          item.indexOf("Kierowcy:") > -1 ||
          item.indexOf("Zespoły:") > -1 ||
          item.indexOf("Drivers:") > -1 ||
          item.indexOf("Teams:") > -1
        ) {
          return (
            <p key={idx}>
              <b>{item}</b>
            </p>
          );
        } else {
          return <p key={idx}>{item}</p>;
        }
      }
      return (
        <p key={idx}>
          <br />
        </p>
      );
    });
    return regulationsText;
  }

  render() {
    const { props } = this;
    const inverted = props.inverted ? "inverted" : "";

    return (
      <Segment basic padded>
        <Message className={inverted}>
          <Message.Header>
            <FormattedMessage id={"app.page.regulations.header"} />
          </Message.Header>
          <Divider hidden fitted></Divider>
          {this.renderRegulations()}
        </Message>
      </Segment>
    );
  }
}

export default Regulations;
