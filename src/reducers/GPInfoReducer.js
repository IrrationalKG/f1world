import { FETCH_GP_INFO, FETCH_GP_INFO_SUCCESS, FETCH_GP_INFO_ERROR } from '../actions/GPInfoActions';

// export default (state = [], action) => {
//   switch (action.type) {
//     case FETCH_GP_INFO:
//       return action.payload;
//     default:
//       return state;
//   }
// };
const initial = {
  data: null,
  loading: false,
  loaded: false
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_GP_INFO:
      return {
        ...state,
        data: [],
        loading: true,
        loaded: false,
        error: null
      };
    case FETCH_GP_INFO_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null
      };
    case FETCH_GP_INFO_ERROR:
      return {
        ...state,
        data: [],
        loading: false,
        loaded: true,
        error: action.payload
      };
    default:
      return state;
  }
};
