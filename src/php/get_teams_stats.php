<?
header('Access-Control-Allow-Origin: *');

$letter=$_GET['letter'];
if ($letter==null) $letter=$_POST['letter'];
if ($letter==null) $letter="A";

$country=$_GET['country'];
if ($country==null) $country=$_POST['country'];
if ($country==null) $country="";
if (strpos($country, 'gb+sct') !== false) {
  $country="gb sct";
}

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

// mistrzowie swiata
$query="SELECT lower(teams.team) id,count(teams.alias) champs from teams_class,teams
where teams_class.id_team=teams.id_team and teams_class.place=1
and teams_class.season>'1957' and teams_class.season<'2019'
group by teams.team ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$champsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $champsTab[$r["id"]] = $r["champs"];
}

// lata startow - gp
$query="SELECT CONCAT(MIN(SUBSTRING(race_date,1,4)),' - ',MAX(SUBSTRING(race_date,1,4))) years,lower(teams.team) id,teams.name
from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team GROUP BY teams.team";
$result = mysqli_query($dbhandle,$query);
$yearsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $yearsTab[$r["id"]] = $r["years"];
}

// lata startow - pp
$query="SELECT CONCAT(MIN(SUBSTRING(qual_date,1,4)),' - ',MAX(SUBSTRING(qual_date,1,4))) years,lower(teams.team) id,teams.name
from drivers_pp_results,teams where drivers_pp_results.id_team=teams.id_team GROUP BY teams.team";
$result = mysqli_query($dbhandle,$query);
$yearsPPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $yearsPPTab[$r["id"]] = $r["years"];
}

// starty
$query="SELECT lower(teams.team) id,count(distinct id_gp,race_date) as starts
from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team GROUP BY teams.team order by starts desc";
$result = mysqli_query($dbhandle,$query);
$startsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $startsTab[$r["id"]] = $r["starts"];
}

// kwalifikacje
$query="SELECT lower(teams.team) id,count(distinct id_gp,qual_date) as qual
from drivers_pp_results,teams where drivers_pp_results.id_team=teams.id_team GROUP BY teams.team order by qual desc";
$result = mysqli_query($dbhandle,$query);
$qualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $qualTab[$r["id"]] = $r["qual"];
}

// zwyciestwa
$query="SELECT lower(teams.team) id,count(distinct race_date) as wins
from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team and race_pos=1 GROUP BY teams.team order by wins desc";
$result = mysqli_query($dbhandle,$query);
$winsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $winsTab[$r["id"]] = $r["wins"];
}

// punkty
$query="SELECT lower(teams.team) id,SUM(points) points FROM teams_class,teams WHERE teams_class.id_team=teams.id_team and points>0
GROUP BY teams.team order by points desc";
$result = mysqli_query($dbhandle,$query);
$pointsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $pointsTab[$r["id"]] = $r["points"];
}

// podium
$query="SELECT lower(teams.team) id,count(race_date) as podium
from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team and race_pos<4 GROUP BY teams.team order by podium desc";
$result = mysqli_query($dbhandle,$query);
$podiumTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $podiumTab[$r["id"]] = $r["podium"];
}

// pole position
$query="SELECT lower(teams.team) id,count(distinct id_starting_grid) as polepos
from drivers_gp_starting_grid,teams where drivers_gp_starting_grid.id_team=teams.id_team AND is_pp=1 GROUP BY teams.team order by polepos desc";
$result = mysqli_query($dbhandle,$query);
$poleposTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $poleposTab[$r["id"]] = $r["polepos"];
}

// best laps
$query="SELECT lower(teams.team) id,count(race_date) as bestlaps
from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team and race_best_lap=1 GROUP BY teams.team order by bestlaps desc";
$result = mysqli_query($dbhandle,$query);
$bestlapsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $bestlapsTab[$r["id"]] = $r["bestlaps"];
}

/**
* Ranking
*/

//zwyciestwa
$query="SELECT COUNT(*) wins, lower(teams.team) team, name FROM drivers_gp_results,teams WHERE drivers_gp_results.id_team=teams.id_team AND race_pos=1 group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$winsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $winsRank[$r["team"]] = $place;
}
//punkty
$query="SELECT SUM(points_class) points, lower(teams.team) team, name FROM teams_class,teams WHERE teams_class.id_team=teams.id_team and points_class>0 group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$pointsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $pointsRank[$r["team"]] = $place;
}
//podium
$query="SELECT COUNT(*) podium, lower(teams.team) team, name FROM drivers_gp_results,teams WHERE drivers_gp_results.id_team=teams.id_team AND race_pos>0 AND race_pos<4 group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$podiumRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $podiumRank[$r["team"]] = $place;
}
//pole pos
$query="SELECT COUNT(*) polepos, lower(teams.team) team, name FROM drivers_gp_starting_grid,teams WHERE drivers_gp_starting_grid.id_team=teams.id_team AND is_pp=1 group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$ppRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $ppRank[$r["team"]] = $place;
}
//naj. okrazenia
$query="SELECT COUNT(*) bestlaps, lower(teams.team) team, name FROM drivers_gp_results,teams WHERE drivers_gp_results.id_team=teams.id_team AND race_best_lap=1 group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$blRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $blRank[$r["team"]] = $place;
}
//starty
$query="SELECT COUNT(distinct race_date) starts, lower(teams.team) team, name FROM drivers_gp_results,teams WHERE drivers_gp_results.id_team=teams.id_team group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$startsRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $startsRank[$r["team"]] = $place;
}
//kwalifikacje
$query="SELECT COUNT(distinct qual_date) qual, lower(teams.team) team, name FROM drivers_pp_results,teams WHERE drivers_pp_results.id_team=teams.id_team group by teams.team order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$qualRank = array();
$place = 0;
while($r = mysqli_fetch_assoc($result)) {
  $place += 1;
  $qualRank[$r["team"]] = $place;
}

if ($country!=null && strlen($country) > 1){
  $query="SELECT DISTINCT lower(team) id,name,
  (SELECT alias_name FROM teams t WHERE t.team=teams.team ORDER BY t.id_team desc LIMIT 1) alias,picture,banner,";
  if ($lang=='pl') {
    $query.="(SELECT GROUP_CONCAT(distinct country ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country,";
  }else{
    $query.="(SELECT GROUP_CONCAT(distinct country_en ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country,";
  }  
  $query.="(SELECT GROUP_CONCAT(distinct country_code ORDER BY t.id_team desc SEPARATOR ',') FROM teams t WHERE t.team=teams.team) countryCode,
  (SELECT COUNT(DISTINCT season) FROM drivers_gp_involvements WHERE drivers_gp_involvements.id_team IN (SELECT t.id_team FROM teams t WHERE t.team=teams.team)) seasons,
  (SELECT COUNT(DISTINCT drivers_gp_results.id_driver) FROM drivers_gp_results,teams t WHERE drivers_gp_results.id_team=t.id_team AND t.team=teams.team) drivers,
  '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ
  from teams where teams.country_code LIKE '$country%' order by name";
}else if (strlen($letter) == 1){
  $query="SELECT DISTINCT lower(team) id,name,
  (SELECT alias_name FROM teams t WHERE t.team=teams.team ORDER BY t.id_team desc LIMIT 1) alias,picture,banner,";
  if ($lang=='pl') {
    $query.="(SELECT GROUP_CONCAT(distinct country ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country ,";
  }else{
    $query.="(SELECT GROUP_CONCAT(distinct country_en ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country ,";
  }  
  $query.="(SELECT GROUP_CONCAT(distinct country_code ORDER BY t.id_team desc SEPARATOR ',') FROM teams t WHERE t.team=teams.team) countryCode,
  (SELECT COUNT(DISTINCT season) FROM drivers_gp_involvements WHERE drivers_gp_involvements.id_team IN (SELECT t.id_team FROM teams t WHERE t.team=teams.team)) seasons,
  (SELECT COUNT(DISTINCT drivers_gp_results.id_driver) FROM drivers_gp_results,teams t WHERE drivers_gp_results.id_team=t.id_team AND t.team=teams.team) drivers,
  '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ
  from teams where teams.name LIKE '$letter%' order by name";
}else{
  $query="SELECT DISTINCT lower(teams.team) id,name,
  (SELECT alias_name FROM teams t WHERE t.team=teams.team ORDER BY t.id_team desc LIMIT 1) alias,picture,banner,";
  if ($lang=='pl') {
    $query.="(SELECT GROUP_CONCAT(distinct country ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country ,";
  }else{
    $query.="(SELECT GROUP_CONCAT(distinct country_en ORDER BY t.id_team desc SEPARATOR ' / ') FROM teams t WHERE t.team=teams.team) country ,";
  }  
  $query.="(SELECT GROUP_CONCAT(distinct country_code ORDER BY t.id_team desc SEPARATOR ',') FROM teams t WHERE t.team=teams.team) countryCode,
  (SELECT COUNT(DISTINCT season) FROM drivers_gp_involvements WHERE drivers_gp_involvements.id_team IN (SELECT t.id_team FROM teams t WHERE t.team=teams.team)) seasons,
  (SELECT COUNT(DISTINCT drivers_gp_results.id_driver) FROM drivers_gp_results,teams t WHERE drivers_gp_results.id_team=t.id_team AND t.team=teams.team) drivers,
  '' years, '' starts, '' qual, '' wins, '' points, '' podium, '' polepos, '' bestlaps, '' champ
  from teams, teams_class where teams.id_team=teams_class.id_team and teams_class.season LIKE '$letter' order by name";
}
$result = mysqli_query($dbhandle,$query);
$teamItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["champ"]=$champsTab[$r["id"]]==null ? '' : $champsTab[$r["id"]]."x Mistrz Świata";
  $r["years"]=$yearsTab[$r["id"]]==null ? $yearsPPTab[$r["id"]] : $yearsTab[$r["id"]];
  $r["starts"]=$startsTab[$r["id"]]==null ? '0' : $startsTab[$r["id"]]." (".$startsRank[$r["id"]].".)";
  $r["qual"]=$qualTab[$r["id"]]==null ? '0' : $qualTab[$r["id"]]." (".$qualRank[$r["id"]].".)";
  $r["wins"]=$winsTab[$r["id"]]==null ? '0' : $winsTab[$r["id"]]." (".$winsRank[$r["id"]].".)";
  $r["points"]=$pointsTab[$r["id"]]==null ? '0' : $pointsTab[$r["id"]]." (".$pointsRank[$r["id"]].".)";
  $r["podium"]=$podiumTab[$r["id"]]==null ? '0' : $podiumTab[$r["id"]]." (".$podiumRank[$r["id"]].".)";
  $r["polepos"]=$poleposTab[$r["id"]]==null ? '0' : $poleposTab[$r["id"]]." (".$ppRank[$r["id"]].".)";
  $r["bestlaps"]=$bestlapsTab[$r["id"]]==null ? '0' : $bestlapsTab[$r["id"]]." (".$blRank[$r["id"]].".)";
  $teamItems[] = $r;
}
$teamsStatsItems["teams"]=$teamItems;

$query="SELECT count(distinct teams.team) amount,teams.country_code countryCode, teams.country country, teams.country_short countryShort FROM teams group by teams.country_code order by 1 DESC";
$result = mysqli_query($dbhandle,$query);
$countryItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $countryItems[] = $r;
}
$teamsStatsItems["countries"]=$countryItems;

$query="SELECT count(distinct teams.name) amount,left(UPPER(teams.name), 1) letter FROM `teams` group by left(teams.name, 1) order by 2 ASC";
$result = mysqli_query($dbhandle,$query);
$lettersItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $lettersItems[] = $r;
}
$teamsStatsItems["letters"]=$lettersItems;

$teamsStatsItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $teamsStatsItems;

print json_encode($response);
mysqli_free_result($result);
?>
