// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_SEASONS_REQUEST = "FETCH_COMBO_SEASONS_REQUEST";
export const FETCH_COMBO_SEASONS_SUCCESS = "FETCH_COMBO_SEASONS_SUCCESS";
export const FETCH_COMBO_SEASONS_ERROR = "FETCH_COMBO_SEASONS_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_SEASONS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_SEASONS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_SEASONS_ERROR,
    payload: error,
  };
}

export function fetchComboSeasons(
  showAll,
  driver,
  team,
  gp,
  circuit,
  engine,
  model,
  tyre,
  lang
) {
  const url = `${ROOT_URL}/get_combo_seasons.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          showAll: showAll,
          driver: driver,
          team: team,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          tyre: tyre,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
