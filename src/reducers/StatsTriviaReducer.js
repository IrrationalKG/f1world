import {
  FETCH_STATS_TRIVIA_REQUEST,
  FETCH_STATS_TRIVIA_SUCCESS,
  FETCH_STATS_TRIVIA_ERROR,
} from "../actions/StatsTriviaActions";

const initial = {
  data: null,
  loading: false,
  loaded: false,
};
export default (state = initial, action) => {
  switch (action.type) {
    case FETCH_STATS_TRIVIA_REQUEST:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case FETCH_STATS_TRIVIA_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        loaded: true,
        error: null,
      };
    case FETCH_STATS_TRIVIA_ERROR:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload,
      };
    default:
      return state;
  }
};
