/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
import PropTypes from "prop-types";
import styles from "./ChartStatsColumn.less";

export default class ChartStatsColumn extends Component {
  render() {
    const { values, seriesName, tooltipLabel } = this.props;

    const columnColors = (function () {
      const colors = [];
      // const base = ReactHighcharts.Highcharts.getOptions().colors[1];
      let i;
      for (i = 0; i < 5; i += 1) {
        colors.push(ReactHighcharts.Highcharts.Color("#ebdda7").get());
      }
      return colors;
    })();

    const chartData = [];
    values.map((item) => {
      const value = { name: "", y: 0 };
      value.name = item.name;
      value.y = parseInt(item.y, 0);
      return chartData.push(value);
    });

    const configChart = {
      chart: {
        height: 370,
        type: "column",
      },
      title: {
        text: "",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: 0,
        },
      },
      yAxis: {
        min: 0,
        allowDecimals: false,
        title: {
          text: " ",
        },
        labels: {
          enabled: false,
        },
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: false,
        itemMarginBottom: 50,
      },
      tooltip: {
        headerFormat: `<span style="font-size:1rem">${tooltipLabel}: {point.key}</span><table>`,
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          colorByPoint: true,
          colors: columnColors,
        },
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },
      series: [
        {
          name: seriesName,
          maxPointWidth: 20,
          data: chartData,
        },
      ],
    };

    return (
      <div className="section-page-content charts-content">
        <ReactHighcharts config={configChart} />
      </div>
    );
  }
}

ChartStatsColumn.propTypes = {
  values: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  seriesName: PropTypes.string,
  tooltipLabel: PropTypes.string,
};
