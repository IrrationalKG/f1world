import React from 'react';
import PropTypes from 'prop-types';
import {
  Map, Marker, Popup, TileLayer
} from 'react-leaflet';

const map = ({
  position, circuit
}) => (
  <Map center={position} zoom={14}>
    <TileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
    />
    <Marker position={position}>
      <Popup>
        {circuit}
      </Popup>
    </Marker>
  </Map>
);

map.propTypes = {
  circuit: PropTypes.string,
  position: PropTypes.arrayOf(PropTypes.number)
};

export default map;
