<?
header('Access-Control-Allow-Origin: *');

$year=$_GET['year'];
if ($year==null) $year=$_POST['year'];

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();

/**
*-------------
* Kierowcy
*-------------
**/
// zespoły
$query="select id_driver,teams.name from drivers_class,teams where drivers_class.id_team=teams.id_team and drivers_class.season='$year' group by id_driver,teams.name";
$resultM = mysqli_query($dbhandle,$query);
$driversTeamsTabM = array();
while($r = mysqli_fetch_assoc($resultM)) {
	$tmp_id_driver = $r["id_driver"];
	$driversTeamsTabM[$tmp_id_driver] = $r["name"];
}

$driversItems = array();
$query="select drivers.alias,drivers.id_driver id,place,CONCAT(name,' ',surname) name,photo,country_code country,'' team,place seasonPlace,points seasonPoints from drivers_class,drivers where drivers.id_driver=drivers_class.id_driver and season='$year' order by place";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $r["team"]=$driversTeamsTabM[$r["id"]];
   $driversItems[] = $r;
}

$drivers = array();
$drivers['id'] = "drivers";
$drivers['items'] = $driversItems;

/**
*-------------
* Zespoły
*-------------
**/
$teamsItems = array();
$query="SELECT distinct teams.alias_name alias,lower(teams.team) id,teams.name,photo,picture,country_code country,COALESCE(teams.engine,teams.name) engine,teams.team,
place seasonPlace,points_class seasonPoints from teams_class,teams where teams.id_team=teams_class.id_team and season='$year'
order by place";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
   $teamsItems[] = $r;
}

$teams = array();
$teams['id'] = "teams";
$teams['items'] = $teamsItems;

// /**
// *-------------
// * Progres - wykres
// *-------------
// **/
// $progresItems = array();
// $query="SELECT drivers.id_driver,drivers.surname,drivers.color,drivers.marker
// FROM drivers_class,drivers
// WHERE drivers_class.id_driver=drivers.id_driver AND season='$year'
// order by sort";
// $result = mysqli_query($dbhandle,$query);
// while($r = mysqli_fetch_assoc($result)) {
//   $id_driver = $r["id_driver"];
//   $driver = $r["surname"];
//   $color = $r["color"];
//   $marker = $r["marker"];

//   $driverItems = array();
//   $dataItems = array();
//   $query2="SELECT gp.name_short name, drivers_gp_results.race_pos y
//   FROM gp
//   LEFT JOIN gp_season ON gp_season.id_gp=gp.id_gp AND gp_season.season='$year'
//   LEFT JOIN drivers_gp_results ON drivers_gp_results.id_gp=gp.id_gp
//   AND drivers_gp_results.race_date>='$year-01-01' AND drivers_gp_results.race_date<='$year-12-31'
//   /*AND drivers_gp_results.race_completed=1 */
//   AND drivers_gp_results.id_driver=$id_driver
//   WHERE gp_season.season='$year' ORDER BY gp_season.sort";
//   $result2 = mysqli_query($dbhandle,$query2);
//   while($r2 = mysqli_fetch_assoc($result2)) {
//     $dataItems[] = $r2;
//   }

//   $driverItems['name'] = $driver;
//   $driverItems['color'] = $color;
//   $driverItems['marker'] = $marker;
//   $driverItems['data'] = $dataItems;

//   $progresItems[] = $driverItems;
// }

// $progres = array();
// $progres['id'] = "progres";
// $progres['items'] = $progresItems;

// Response
$response[0] = $drivers;
$response[1] = $teams;
$response[2] = $progres;

print json_encode($response);
mysqli_free_result($result);
?>
