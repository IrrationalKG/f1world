// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_DRIVER_REQUEST = "FETCH_DRIVER_REQUEST";
export const FETCH_DRIVER_SUCCESS = "FETCH_DRIVER_SUCCESS";
export const FETCH_DRIVER_ERROR = "FETCH_DRIVER_ERROR";

function requestData() {
  return {
    type: FETCH_DRIVER_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_DRIVER_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_DRIVER_ERROR,
    payload: error,
  };
}

export function fetchDriver(id, year, lang) {
  const url = `${ROOT_URL}/get_driver_stats.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
