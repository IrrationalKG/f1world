/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { fetchParams } from "../../actions/ParamsActions";
import Menu from "../../containers/Menu/Menu";
import Cookies from "js-cookie";
import styles from "./Header.less";

export class Header extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { props } = this;
    props.fetchParams(Cookies.get("lang"));
  }

  render() {
    const { params } = this.props;
    if (!params?.data || params?.loading) {
      return (
        <section id="header">
          <div className="full-height" />
        </section>
      );
    }
    return (
      <header id="header">
        <Menu year={params.data.nextGPYear} gp={params.data.nextGPAlias} />
      </header>
    );
  }
}

Header.propTypes = {
  fetchParams: PropTypes.func.isRequired,
  params: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  nextGPYear: PropTypes.string,
  nextGPAlias: PropTypes.string,
};

function mapStateToProps({ params }) {
  return { params };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchParams }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
