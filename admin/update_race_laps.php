<?
include("dbinfo.inc.php");

if(isset($_GET['season'])){
    $season = $_GET['season'];
}else{
    $season = "2025";
}
if(isset($_GET['id_gp'])){
    $id_gp = $_GET['id_gp'];
}else{
    $id_gp = "-1";
}
if(isset($_GET['laps'])){
    $laps = $_GET['laps'];
}else{
    $laps = "-1";
}

$dbhandle = mysqli_connect('localhost',$username,$password);
@mysqli_select_db($dbhandle, $database) or die( "Unable to select database");

$query="SELECT DISTINCT season FROM gp_season order by season";
$result_s=mysqli_query($dbhandle, $query);
$num_s=mysqli_num_rows($result_s);

$query="SELECT * FROM gp order by name";
$result_c=mysqli_query($dbhandle, $query);
$num_c=mysqli_num_rows($result_c);

mysqli_close($dbhandle);

?>
<html>

<head>
    <title>Untitled Document</title>
    <script language="javascript" type="text/javascript">
    function doReload(season, id_gp) {
        document.location = 'update_race_laps_and_pole_positions.php?season=' + season + '&id_gp=' + id_gp;
    }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
    <form method="post" action="insert_race_laps_and_pole_positions.php">
        <p>Update GP laps and pole position<br>
        Sezon:
        <select name="season" onChange="doReload(this.value,<? echo "$id_gp"; ?>);">
        <?
        while ($r = mysqli_fetch_assoc($result_s)) {
            $year=$r["season"];
        ?>
            <option value="<? echo "$year"; ?>"
                <?  if ($year == $season) { echo "selected";}else{ echo "";} ?>>
                <? echo "$year"; ?>
            </option>
            <?
        }
        mysqli_free_result($result_s);
        ?>
        </select>
        GP:
        <select name="id_gp" onChange="doReload(<? echo "$season"; ?>,this.value);">
            <?
        $i=0;
        while ($r = mysqli_fetch_assoc($result_c)) {
            $gp=$r["id_gp"];
            $name=$r["name"];
            $circuit=$r["circuit"];
        ?>
            <option value="<? echo " $gp"; ?>"
                <?  if ($gp == $id_gp) { echo "selected";}else{ echo "";} ?>>
                <? echo "$name"; ?>&nbsp;(
                <? echo "$circuit"; ?>)
                [<? echo "$gp"; ?>]
            </option>
            <?
        }
        mysqli_free_result($result_c);
        ?>
        </select>
        Laps:  <input type="text" name="laps" value="" size="2">
        <br>
        <input type="submit" name="Submit" value="Submit">
        </p>
    </form>
</body>

</html>