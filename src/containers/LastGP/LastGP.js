/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  GridColumn,
  Loader,
  Segment,
  Item,
  Flag,
  Header,
  Statistic,
  Divider,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchLastGP } from "../../actions/LastGPActions";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./LastGP.less";

class LastGP extends Component {
  componentDidMount() {
    const { props } = this;
    const { id, year } = props;
    props.fetchLastGP(year, id, Cookies.get("lang"));
  }

  renderGPResults() {
    const driversItems = [];
    const { props } = this;
    const { gpResults } = props.lastGP.data;
    let cnt = 0;
    gpResults.forEach((item) => {
      const filename = `build/images/drivers/driver_${item.id}_profile.jpg`;
      const link = `/driver/${item.alias}`;
      const driverItem = (
        <Item key={item.id}>
          <NavLink to={link} className="image-link">
            <Image size="small" src={filename} alt={item.name} />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.position}
                {". "}
                <Flag name={item.country} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.team}</Item.Description>
            <Item.Meta>{item.time}</Item.Meta>
            {item.info && <Item.Extra>{item.info}</Item.Extra>}
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value>{item.points}</Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      driversItems.push(driverItem);

      cnt += 1;
    });
    return driversItems;
  }

  renderQualResults() {
    const driversItems = [];
    const { props } = this;
    const { qualResults } = props.lastGP.data;
    let cnt = 0;
    qualResults.forEach((item) => {
      const filename = `build/images/drivers/driver_${item.id}_profile.jpg`;
      const link = `/driver/${item.alias}`;
      const driverItem = (
        <Item key={item.id}>
          <NavLink to={link} className="image-link">
            <Image size="small" src={filename} alt={item.name} />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.position}
                {". "}
                <Flag name={item.country} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.team}</Item.Description>
            <Item.Meta>{item.time}</Item.Meta>
            {item.info && <Item.Extra>{item.info}</Item.Extra>}
          </Item.Content>
        </Item>
      );
      driversItems.push(driverItem);

      cnt += 1;
    });
    return driversItems;
  }

  render() {
    const { lastGP, year } = this.props;
    if (!lastGP.data) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    const {
      name,
      circuit,
      country,
      winner,
      second,
      third,
      alias,
      nameAlias,
      round,
    } = lastGP.data;
    const pic = `build/images/circuits/circuit_${alias}_banner.jpg`;
    const picName = pic.substring(0, pic.length - 4);
    const picp = `${picName}.webp`;
    const pic1024w = `${picName}-1024w.jpg`;
    const pic1024wp = `${picName}-1024w.webp`;
    const winnerPic = `build/images/drivers/driver_${winner.id}_profile.jpg`;
    const secondPic = `build/images/drivers/driver_${second.id}_profile.jpg`;
    const thirdPic = `build/images/drivers/driver_${third.id}_profile.jpg`;
    const trackPic = `build/images/circuits/circuit_${alias}_map.jpg`;
    const link = `/gp-result/${nameAlias}/${year}`;
    return (
      <section id="last-gp" name="last-gp">
        <div className="last-gp-header">
          <h2>
            <FormattedMessage id={"app.page.home.lastgp.title"} />
          </h2>
          <h3>{name}</h3>
        </div>
        <div className="last-gp-subheader">
          <Grid verticalAlign="middle">
            <GridColumn
              mobile={14}
              tablet={13}
              computer={13}
              className="last-gp-subheader-circuit-name"
            >
              <NavLink to={link}>{circuit}</NavLink>
            </GridColumn>
            <GridColumn mobile={2} tablet={3} computer={3}>
              <NavLink to={link}>
                <Image
                  src={`/build/images/countries/${country}.jpg`}
                  alt={country}
                  onError={(e) => {
                    e.target.src =
                      "/build/images/countries/countries_no_profile.jpg";
                  }}
                />
              </NavLink>
            </GridColumn>
          </Grid>
        </div>
        <div className="last-gp-banner">
          <NavLink to={link}>
            <picture>
              <source
                media="(max-width: 1024px)"
                srcSet={pic1024wp}
                type="image/webp"
              />
              <source media="(max-width: 1024px)" srcSet={pic1024w} />
              <source srcSet={picp} type="image/webp" />
              <img src={pic} alt="Następna Grand Prix" />
            </picture>
          </NavLink>
          <div className="last-gp-season">
            <div className="last-gp-season-title">
              <FormattedMessage id={"app.page.home.lastgp.records.round"} />
            </div>
            <div className="last-gp-season-year">{round}</div>
          </div>
        </div>
        <div className="last-gp-content">
          <Segment basic className="last-gp-info">
            <NavLink to={link}>
              <Image className="next-gp-track" src={trackPic} alt={alias} />
            </NavLink>
            <div className="buttons">
              <NavLink className="primary" to={link}>
                <FormattedMessage id={"app.button.details"} />
              </NavLink>
            </div>
            <Header
              size="large"
              textAlign="center"
              className="sidebar-box-header-primary"
            >
              <FormattedMessage id={"app.stats.race"} />
            </Header>
            <div className="info-box">
              <Item.Group divided unstackable>
                {this.renderGPResults()}
              </Item.Group>
            </div>
            <Header
              size="large"
              textAlign="center"
              className="sidebar-box-header-primary"
            >
              <FormattedMessage id={"app.stats.qual"} />
            </Header>
            <div className="info-box">
              <Item.Group divided unstackable>
                {this.renderQualResults()}
              </Item.Group>
            </div>
          </Segment>
          <Segment basic className="last-gp-info-mobile">
            <Grid>
              <Grid.Row textAlign="center" columns={3}>
                <Grid.Column textAlign="center">
                  <div className="last-gp-driver-box">
                    <NavLink to={`/driver/${second.alias}`}>
                      <Image
                        className="center-aligned"
                        src={secondPic}
                        alt={second.name}
                      />
                    </NavLink>
                    <Header size="huge">2</Header>
                    <Header size="small">
                      <NavLink to={`/driver/${second.alias}`}>
                        <Flag name={second.country} />
                        {second.name}
                      </NavLink>
                      <Header.Subheader>{second.team}</Header.Subheader>
                      <p>{second.time}</p>
                    </Header>
                  </div>
                </Grid.Column>
                <Grid.Column textAlign="center">
                  <div className="last-gp-driver-box">
                    <NavLink to={`/driver/${winner.alias}`}>
                      <Image
                        className="center-aligned"
                        src={winnerPic}
                        alt={winner.name}
                      />
                    </NavLink>
                    <Header size="huge">1</Header>
                    <Header size="small">
                      <NavLink to={`/driver/${winner.alias}`}>
                        <Flag name={winner.country} />
                        {winner.name}
                      </NavLink>
                      <Header.Subheader>{winner.team}</Header.Subheader>
                      <p>{winner.time}</p>
                    </Header>
                  </div>
                </Grid.Column>
                <Grid.Column textAlign="center">
                  <div className="last-gp-driver-box">
                    <NavLink to={`/driver/${third.alias}`}>
                      <Image
                        className="center-aligned"
                        src={thirdPic}
                        alt={third.name}
                      />
                    </NavLink>
                    <Header size="huge">3</Header>
                    <Header size="small">
                      <NavLink to={`/driver/${third.alias}`}>
                        <Flag name={third.country} />
                        {third.name}
                      </NavLink>
                      <Header.Subheader>{third.team}</Header.Subheader>
                      <p>{third.time}</p>
                    </Header>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <div className="buttons">
              <NavLink className="primary" to={link}>
                <FormattedMessage id={"app.button.details"} />
              </NavLink>
            </div>
          </Segment>
        </div>
      </section>
    );
  }
}

LastGP.propTypes = {
  fetchLastGP: PropTypes.func.isRequired,
  lastGP: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  id: PropTypes.string,
};

function mapStateToProps({ lastGP }) {
  return { lastGP };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchLastGP }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LastGP);
