<?
header('Access-Control-Allow-Origin: *');

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$SELECTed = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT database");

//query fire
$response = array();

$start_time = microtime(true);

$year=2025;

// mistrzostwa - kierowcy
$query="SELECT drivers.id_driver id,drivers.alias,CONCAT(drivers.name,' ',drivers.surname) name, drivers.picture,";
if ($lang=='pl') {
  $query.="drivers.country,";
}else{
  $query.="drivers.country_en as country,";
}  
$query.="drivers.country_code countryCode,count(id_drivers_class) champs,
GROUP_CONCAT(' ',season ORDER BY season ASC) champSeasons
from drivers_class,drivers 
where drivers.id_driver=drivers_class.id_driver 
and place=1 
and season<$year
group by drivers.id_driver order by champs desc,surname";
$result = mysqli_query($dbhandle,$query);
$driversTitlesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $driversTitlesItems[] = $r;
}
$mostTitlesItems["drivers"]=$driversTitlesItems;

// mistrzostwa - zespoly
$query="SELECT lower(teams.team) id,teams.alias_name alias,teams.name name,teams.engine,teams.picture,";
if ($lang=='pl') {
  $query.="teams.country,";
}else{
  $query.="teams.country_en as country,";
}  
$query.="teams.country_code countryCode,count(teams_class.team) champs,
GROUP_CONCAT(' ',season ORDER BY season ASC) champSeasons
from teams_class,teams where teams.id_team=teams_class.id_team and place=1 and season>1957 and season<$year
group by teams_class.team,teams.name,teams.country_short order by champs desc,name";
$result = mysqli_query($dbhandle,$query);
$teamsTitlesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $teamsTitlesItems[] = $r;
}
$mostTitlesItems["teams"]=$teamsTitlesItems;

// mistrzostwa - kraje
$query="SELECT ";
if ($lang=='pl') {
  $query.="drivers.country as name,";
}else{
  $query.="drivers.country_en as name,";
}  
$query.="lower(drivers.country_short) id,drivers.country_code countryCode,count(drivers.country_short) champs,
GROUP_CONCAT(' ',season ORDER BY season ASC) champSeasons
from drivers_class,drivers 
where drivers.id_driver=drivers_class.id_driver and place=1 and season<$year
group by country,drivers.country_short order by champs desc,country";
$result = mysqli_query($dbhandle,$query);
$countriesTitlesItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $countriesTitlesItems[] = $r;
}
$mostTitlesItems["countries"]=$countriesTitlesItems;

//starty w sezonach - kierowcy
$query="SELECT COUNT(distinct id_gp,race_date) as starts,SUBSTRING(race_date,1,4) as season,drivers_class.id_driver
from drivers_gp_results,drivers_class where drivers_gp_results.id_driver=drivers_class.id_driver and drivers_class.place=1
and drivers_class.season=SUBSTRING(race_date,1,4) and drivers_class.season<$year
group by SUBSTRING(race_date,1,4),drivers_class.id_driver order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$driversStartsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_id_driver=$r["id_driver"];
	$driversStartsTab = Array();
	$driversStartsTab[$_id_driver] = $r["starts"];
	$driversStartsSeasonsTab[$i][0] = $season;
	$driversStartsSeasonsTab[$i][1] = $driversStartsTab;
	$i++;
}
//zwyciestwa w sezonach - kierowcy
$query="SELECT COUNT(distinct id_gp,race_date) as wins,SUBSTRING(race_date,1,4) as season,drivers_class.id_driver
from drivers_gp_results,drivers_class where drivers_gp_results.id_driver=drivers_class.id_driver and drivers_class.place=1
and drivers_class.season=SUBSTRING(race_date,1,4) and drivers_gp_results.race_pos=1 and drivers_class.season<$year
group by SUBSTRING(race_date,1,4),drivers_class.id_driver order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$driversWinsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_id_driver=$r["id_driver"];
	$driversWinsTab = Array();
	$driversWinsTab[$_id_driver] = $r["wins"];
	$driversWinsSeasonsTab[$i][0] = $season;
	$driversWinsSeasonsTab[$i][1] = $driversWinsTab;
	$i++;
}
//podium w sezonach - kierowcy
$query="SELECT COUNT(distinct id_gp,race_date) as podiums,SUBSTRING(race_date,1,4) as season,drivers_class.id_driver
from drivers_gp_results,drivers_class where drivers_gp_results.id_driver=drivers_class.id_driver and drivers_class.place=1
and drivers_class.season=SUBSTRING(race_date,1,4) and drivers_gp_results.race_pos<4 and drivers_class.season<$year
group by SUBSTRING(race_date,1,4),drivers_class.id_driver order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$driversPodiumsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_id_driver=$r["id_driver"];
	$driversPodiumsTab = Array();
	$driversPodiumsTab[$_id_driver] = $r["podiums"];
	$driversPodiumsSeasonsTab[$i][0] = $season;
	$driversPodiumsSeasonsTab[$i][1] = $driversPodiumsTab;
	$i++;
}
//pole positions w sezonach - kierowcy
$query="SELECT COUNT(distinct id_gp,qual_date) as pps,SUBSTRING(qual_date,1,4) as season,drivers_class.id_driver
from drivers_pp_results,drivers_class where drivers_pp_results.id_driver=drivers_class.id_driver and drivers_class.place=1
and drivers_class.season=SUBSTRING(qual_date,1,4) and drivers_pp_results.qual_pos=1 and drivers_class.season<$year
group by SUBSTRING(qual_date,1,4),drivers_class.id_driver order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$driversPPSSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_id_driver=$r["id_driver"];
	$driversPPSTab = Array();
	$driversPPSTab[$_id_driver] = $r["pps"];
	$driversPPSSeasonsTab[$i][0] = $season;
	$driversPPSSeasonsTab[$i][1] = $driversPPSTab;
	$i++;
}
//najlepsze okrazenia w sezonach - kierowcy
$query="SELECT COUNT(distinct id_gp,race_date) as bestlaps,SUBSTRING(race_date,1,4) as season,drivers_class.id_driver
from drivers_gp_results,drivers_class where drivers_gp_results.id_driver=drivers_class.id_driver and drivers_class.place=1
and drivers_class.season=SUBSTRING(race_date,1,4) and drivers_gp_results.race_best_lap=1 and drivers_class.season<$year
group by SUBSTRING(race_date,1,4),drivers_class.id_driver order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$driversBestlapsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_id_driver=$r["id_driver"];
	$driversBestlapsTab = Array();
	$driversBestlapsTab[$_id_driver] = $r["bestlaps"];
	$driversBestlapsSeasonsTab[$i][0] = $season;
	$driversBestlapsSeasonsTab[$i][1] = $driversBestlapsTab;
	$i++;
}
// mistrzostwa kierowcow wg sezonow
$query="SELECT drivers_class.season,drivers.id_driver id,CONCAT(drivers.name,' ',drivers.surname) name,drivers.alias,
drivers.country_code countryCode,";
if ($lang=='pl') {
  $query.="country,";
}else{
  $query.="country_en as country,";
}  
$query.="drivers.picture,'' starts,'' wins, '' podium,'' polepos,'' bestlaps, points, points_class pointsClass
from drivers_class,drivers where drivers_class.id_driver=drivers.id_driver and drivers_class.place=1
and drivers_class.season<$year ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$driversItems=array();
while($r = mysqli_fetch_assoc($result)) {
  //starty w sezonie
  foreach ($driversStartsSeasonsTab as $seasonTab) {
    $r["starts"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $startsTab=$seasonTab[1];
      if ($startsTab[$r["id"]]!=""){
        $r["starts"] = $startsTab[$r["id"]];
        break;
      }
    }
  }
  //zwyciestwa w sezonie
  foreach ($driversWinsSeasonsTab as $seasonTab) {
    $r["wins"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["wins"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //podium w sezonie
  foreach ($driversPodiumsSeasonsTab as $seasonTab) {
    $r["podium"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["podium"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //pole positions w sezonie
  foreach ($driversPPSSeasonsTab as $seasonTab) {
    $r["polepos"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["polepos"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //najlepsze okrazenia w sezonie
  foreach ($driversBestlapsSeasonsTab as $seasonTab) {
    $r["bestlaps"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["bestlaps"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  $driversItems[] = $r;
}
$seasonsItems["drivers"]=$driversItems;

/**
* ZESPOŁY
**/
//starty w sezonach - teamy
$query="SELECT COUNT(distinct id_gp,race_date) as starts,SUBSTRING(race_date,1,4) as season,lower(teams_class.team) team
from drivers_gp_results,teams_class where drivers_gp_results.id_team=teams_class.id_team and teams_class.place=1
and teams_class.season=SUBSTRING(race_date,1,4) and teams_class.season<$year
group by SUBSTRING(race_date,1,4),teams_class.team order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$teamsStartsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_team=$r["team"];
	$teamsStartsTab = Array();
	$teamsStartsTab[$_team] = $r["starts"];
	$teamsStartsSeasonsTab[$i][0] = $season;
	$teamsStartsSeasonsTab[$i][1] = $teamsStartsTab;
	$i++;
}
//zwyciestwa w sezonach - teamy
$query="SELECT COUNT(*) as wins,SUBSTRING(race_date,1,4) as season,lower(teams_class.team) team
from drivers_gp_results,teams_class where drivers_gp_results.id_team=teams_class.id_team and teams_class.place=1
and teams_class.season=SUBSTRING(race_date,1,4) and race_pos=1 and teams_class.season<$year
group by SUBSTRING(race_date,1,4),teams_class.team order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$teamsWinsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_team=$r["team"];
	$teamsWinsTab = Array();
	$teamsWinsTab[$_team] = $r["wins"];
	$teamsWinsSeasonsTab[$i][0] = $season;
	$teamsWinsSeasonsTab[$i][1] = $teamsWinsTab;
	$i++;
}
//podium w sezonach - teamy
$query="SELECT COUNT(*) as podiums,SUBSTRING(race_date,1,4) as season,lower(teams_class.team) team
from drivers_gp_results,teams_class where drivers_gp_results.id_team=teams_class.id_team and teams_class.place=1
and teams_class.season=SUBSTRING(race_date,1,4) and race_pos<4 and teams_class.season<$year
group by SUBSTRING(race_date,1,4),teams_class.team order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$teamsPodiumsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_team=$r["team"];
	$teamsPodiumsTab = Array();
	$teamsPodiumsTab[$_team] = $r["podiums"];
	$teamsPodiumsSeasonsTab[$i][0] = $season;
	$teamsPodiumsSeasonsTab[$i][1] = $teamsPodiumsTab;
	$i++;
}
//pole positions w sezonach - teamy
$query="SELECT COUNT(*) as pps,SUBSTRING(qual_date,1,4) as season,lower(teams_class.team) team
from drivers_pp_results,teams_class where drivers_pp_results.id_team=teams_class.id_team and teams_class.place=1
and teams_class.season=SUBSTRING(qual_date,1,4) and qual_pos=1 and teams_class.season<$year
group by SUBSTRING(qual_date,1,4),teams_class.team order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$teamsPPSSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_team=$r["team"];
	$teamsPPSTab = Array();
	$teamsPPSTab[$_team] = $r["pps"];
	$teamsPPSSeasonsTab[$i][0] = $season;
	$teamsPPSSeasonsTab[$i][1] = $teamsPPSTab;
	$i++;
}
//najlepsze okrazenia w sezonach - teamy
$query="SELECT COUNT(*) as bestlaps,SUBSTRING(race_date,1,4) as season,lower(teams_class.team) team
from drivers_gp_results,teams_class where drivers_gp_results.id_team=teams_class.id_team and teams_class.place=1
and teams_class.season=SUBSTRING(race_date,1,4) and race_best_lap=1 and teams_class.season<$year
group by SUBSTRING(race_date,1,4),teams_class.team order by season desc";
$result = mysqli_query($dbhandle,$query);
$i=0;
$teamsBestlapsSeasonsTab = Array();
while($r = mysqli_fetch_assoc($result)) {
	$season=$r["season"];
	$_team=$r["team"];
	$teamsBestlapsTab = Array();
	$teamsBestlapsTab[$_team] = $r["bestlaps"];
	$teamsBestlapsSeasonsTab[$i][0] = $season;
	$teamsBestlapsSeasonsTab[$i][1] = $teamsBestlapsTab;
	$i++;
}
// mistrzostwa zespolow wg sezonow
$query="SELECT teams_class.season,lower(teams.team) id,teams.name,teams.alias_name alias, teams.engine,teams.
country_code countryCode,";
if ($lang=='pl') {
  $query.="country,";
}else{
  $query.="country_en as country,";
}  
$query.="teams.picture,'' starts,'' wins, '' podium,'' polepos,'' bestlaps, points, points_class pointsClass
from teams_class,teams where teams_class.id_team=teams.id_team and teams_class.place=1 and teams_class.season<$year
ORDER BY season desc";
$result = mysqli_query($dbhandle,$query);
$teamsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  //starty w sezonie
  foreach ($teamsStartsSeasonsTab as $seasonTab) {
    $r["starts"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $startsTab=$seasonTab[1];
      if ($startsTab[$r["id"]]!=""){
        $r["starts"] = $startsTab[$r["id"]];
        break;
      }
    }
  }
  //zwyciestwa w sezonie
  foreach ($teamsWinsSeasonsTab as $seasonTab) {
    $r["wins"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["wins"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //podium w sezonie
  foreach ($teamsPodiumsSeasonsTab as $seasonTab) {
    $r["podium"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["podium"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //pole positions w sezonie
  foreach ($teamsPPSSeasonsTab as $seasonTab) {
    $r["polepos"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["polepos"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  //najlepsze okrazenia w sezonie
  foreach ($teamsBestlapsSeasonsTab as $seasonTab) {
    $r["bestlaps"] = "0";
    if ($seasonTab[0]==$r["season"]){
      $pointsTab=$seasonTab[1];
      if ($pointsTab[$r["id"]]!=""){
        $r["bestlaps"] = $pointsTab[$r["id"]];
        break;
      }
    }
  }
  $teamsItems[] = $r;
}
$seasonsItems["teams"]=$teamsItems;

$historyChamps["mostTitles"]=$mostTitlesItems;
$historyChamps["seasons"]=$seasonsItems;

$historyChamps["createTime"]=microtime(true)-$start_time;

// Response
$response = $historyChamps;

print json_encode($response);
mysqli_free_result($result);
?>
