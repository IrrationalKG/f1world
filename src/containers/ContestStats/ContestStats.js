/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderAwards","renderPlacesStats"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Table,
  Popup,
  Grid,
  Statistic,
  Label,
  Item,
  Button,
  Divider,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchContestStats } from "../../actions/ContestStatsActions";
import { fetchComboContestSeasons } from "../../actions/ComboContestSeasonsActions";
import { fetchContestAwards } from "../../actions/ContestAwardsActions";
import { fetchContestPlacesStats } from "../../actions/ContestPlacesStatsActions";
import { FormattedMessage } from "react-intl";
import styles from "./ContestStats.less";

const LIMIT = 20;

class ContestStats extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: props.match.params.year || "0",
      showAllSeasonWins: false,
      showAllSeasonSecond: false,
      showAllSeasonThird: false,
      showAllSeasonPodiums: false,
      showAllSeasonWinsGP: false,
      showAllSeasonSecondGP: false,
      showAllSeasonThirdGP: false,
      showAllSeasonPodiumsGP: false,
      showAllRoundWins: false,
      showAllRoundSecond: false,
      showAllRoundThird: false,
      showAllRoundPodium: false,
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.handleShowAllSeasonWins = this.handleShowAllSeasonWins.bind(this);
    this.handleShowAllSeasonSecond = this.handleShowAllSeasonSecond.bind(this);
    this.handleShowAllSeasonThird = this.handleShowAllSeasonThird.bind(this);
    this.handleShowAllSeasonPodiums =
      this.handleShowAllSeasonPodiums.bind(this);
    this.handleShowAllSeasonWinsGP = this.handleShowAllSeasonWinsGP.bind(this);
    this.handleShowAllSeasonSecondGP =
      this.handleShowAllSeasonSecondGP.bind(this);
    this.handleShowAllSeasonThirdGP =
      this.handleShowAllSeasonThirdGP.bind(this);
    this.handleShowAllSeasonPodiumsGP =
      this.handleShowAllSeasonPodiumsGP.bind(this);
    this.handleShowAllRoundWins = this.handleShowAllRoundWins.bind(this);
    this.handleShowAllRoundSecond = this.handleShowAllRoundSecond.bind(this);
    this.handleShowAllRoundThird = this.handleShowAllRoundThird.bind(this);
    this.handleShowAllRoundPodium = this.handleShowAllRoundPodium.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchContestAwards();
    props.fetchContestStats(props.match.params.year);
    props.fetchContestPlacesStats();
    props.fetchComboContestSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;
    this.setState({
      isLoadingExternally: false,
      selectValue: props.match.params.year || "0",
    });

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;
    if (year !== nextYear) {
      props.fetchContestStats(nextYear);
    }
  }

  onInputChange(event) {
    const { props } = this;
    if (event.value != "0") {
      props.history.push({ pathname: `/contest/stats/${event.value}` });
    } else {
      props.history.push({ pathname: `/contest/stats` });
    }
    this.setState({
      selectValue: event.value,
    });
  }

  handleShowAllSeasonWinsGP() {
    const { state } = this;
    this.setState({
      showAllSeasonWinsGP: !state.showAllSeasonWinsGP,
    });
  }

  handleShowAllSeasonSecondGP() {
    const { state } = this;
    this.setState({
      showAllSeasonSecondGP: !state.showAllSeasonSecondGP,
    });
  }

  handleShowAllSeasonThirdGP() {
    const { state } = this;
    this.setState({
      showAllSeasonThirdGP: !state.showAllSeasonThirdGP,
    });
  }

  handleShowAllSeasonPodiumsGP() {
    const { state } = this;
    this.setState({
      showAllSeasonPodiumsGP: !state.showAllSeasonPodiumsGP,
    });
  }

  handleShowAllSeasonWins() {
    const { state } = this;
    this.setState({
      showAllSeasonWins: !state.showAllSeasonWins,
    });
  }

  handleShowAllSeasonSecond() {
    const { state } = this;
    this.setState({
      showAllSeasonSecond: !state.showAllSeasonSecond,
    });
  }

  handleShowAllSeasonThird() {
    const { state } = this;
    this.setState({
      showAllSeasonThird: !state.showAllSeasonThird,
    });
  }

  handleShowAllSeasonPodiums() {
    const { state } = this;
    this.setState({
      showAllSeasonPodiums: !state.showAllSeasonPodiums,
    });
  }

  handleShowAllRoundWins() {
    const { state } = this;
    this.setState({
      showAllRoundWins: !state.showAllRoundWins,
    });
  }

  handleShowAllRoundSecond() {
    const { state } = this;
    this.setState({
      showAllRoundSecond: !state.showAllRoundSecond,
    });
  }

  handleShowAllRoundThird() {
    const { state } = this;
    this.setState({
      showAllRoundThird: !state.showAllRoundThird,
    });
  }

  handleShowAllRoundPodium() {
    const { state } = this;
    this.setState({
      showAllRoundPodium: !state.showAllRoundPodium,
    });
  }

  renderAwards(awardsContent, type) {
    const awardsItems = [];
    if (awardsContent) {
      awardsContent.forEach((award) => {
        let content = `${award.place}. miejsce w sezonie ${award.season}`;
        let awardColor = "";
        switch (award.place) {
          case "1":
            awardColor = "yellow";
            break;
          case "2":
            awardColor = "grey";
            break;
          case "3":
            awardColor = "brown";
            break;
          default:
            return "";
        }
        let awardItem;
        const key = `${type}_${award.season}_${award.place}`;
        if (award.season === "-1") {
          content = `${award.place}. miejsce wszechczasów`;
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="trophy" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        } else {
          awardItem = (
            <Popup
              key={key}
              trigger={<Icon name="star" size="small" color={awardColor} />}
              content={content}
              size="small"
            />
          );
        }
        awardsItems.push(awardItem);
        return awardsItems;
      });
    }
    return awardsItems;
  }

  renderAwardsNormal(item) {
    const { props } = this;
    if (props.awards.data != null) {
      const data = props.awards.data[0];
      const awardsContent = data[item.id];
      return this.renderAwards(awardsContent, "normal");
    }
    const awardsItems = [];
    return awardsItems;
  }

  renderPlacesStats(item) {
    const { props } = this;
    const { wins, second, third } = props.contestPlacesStats.data;
    const winsContent = wins[item.alias];
    const secondContent = second[item.alias];
    const thirdContent = third[item.alias];
    const placesStatsItems = [];
    if (winsContent) {
      const content = `Wygrane kolejki konkursu: ${winsContent}`;
      const firstPlaces = (
        <Label htmlFor="places-stats-wins" circular size="small" color="yellow">
          {winsContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`wins_${item.id}`}
          trigger={firstPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    if (secondContent) {
      const content = `Drugie miejsca w kolejce konkursu: ${secondContent}`;
      const secondPlaces = (
        <Label htmlFor="places-stats-second" circular size="small" color="grey">
          {secondContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`second_${item.id}`}
          trigger={secondPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    if (thirdContent) {
      const content = `Trzecie miejsca w kolejce konkursu: ${thirdContent}`;
      const thirdPlaces = (
        <Label htmlFor="places-stats-third" circular size="small" color="brown">
          {thirdContent}
        </Label>
      );
      const placesStatsItem = (
        <Popup
          key={`third_${item.id}`}
          trigger={thirdPlaces}
          content={content}
          size="small"
        />
      );
      placesStatsItems.push(placesStatsItem);
    }
    return placesStatsItems;
  }

  renderPoints() {
    const { props } = this;
    const { classification, maxYear } = props.contestStats.data;
    if (classification.length > 0) {
      const classItems = [];
      let place = 0;
      classification.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const classItem = (
          <Item key={item.id}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {place}
                  {". "}
                  {item.name}
                </NavLink>
                {maxYear === props.match.params.year &&
                  (item.diff > 0 ? (
                    <Icon color="green" name="caret up" />
                  ) : (
                    <Icon color="red" name="caret down" />
                  ))}
                {maxYear === props.match.params.year && item.diff}
              </Item.Header>
              <div className="meta">{this.renderAwardsNormal(item)}</div>
              <Item.Extra>
                <FormattedMessage id={"app.stats.gp"} />:{" "}
                <Label htmlFor={item.name}>
                  <Flag name="au" />
                  {item.points}
                  <Label.Detail>{item.name}</Label.Detail>
                </Label>
              </Item.Extra>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right" size="small">
                <Statistic.Value>{item.points}</Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.points"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        classItems.push(classItem);
      });
      return classItems;
    }
    return "";
  }

  renderSeasonsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasons } = stats.players;
    if (seasons.length > 0) {
      const items = [];
      let place = 0;
      seasons.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasons() {
    const elements = [];
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasons } = stats.players;
    let cnt = 0;
    seasons.forEach((item) => {
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Sezony">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderRoundsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { rounds } = stats.players;
    if (rounds.length > 0) {
      const items = [];
      let place = 0;
      rounds.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderRounds() {
    const elements = [];
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { rounds } = stats.players;
    let cnt = 0;
    rounds.forEach((item) => {
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Rundy">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderPointsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { points } = stats.players;
    if (points.length > 0) {
      const items = [];
      let place = 0;
      points.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderPoints() {
    const elements = [];
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { points } = stats.players;
    let cnt = 0;
    points.forEach((item) => {
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderPointsGPMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { pointsGP } = stats.players;
    if (pointsGP.length > 0) {
      const items = [];
      let place = 0;
      pointsGP.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonWinsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonWins } = stats.players;
    if (seasonWins.length > 0) {
      const items = [];
      let place = 0;
      seasonWins.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonWins() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonWins } = state;
    const { stats } = props.contestStats.data;
    const { seasonWins } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonWins ? 1000 : LIMIT;

    seasonWins.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonSecondMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonSecond } = stats.players;
    if (seasonSecond.length > 0) {
      const items = [];
      let place = 0;
      seasonSecond.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonSecond() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonSecond } = state;
    const { stats } = props.contestStats.data;
    const { seasonSecond } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonSecond ? 1000 : LIMIT;

    seasonSecond.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonThirdMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonThird } = stats.players;
    if (seasonThird.length > 0) {
      const items = [];
      let place = 0;
      seasonThird.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonThird() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonThird } = state;
    const { stats } = props.contestStats.data;
    const { seasonThird } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonThird ? 1000 : LIMIT;

    seasonThird.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonPodiumsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonPodiums } = stats.players;
    if (seasonPodiums.length > 0) {
      const items = [];
      let place = 0;
      seasonPodiums.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonPodiums() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonPodiums } = state;
    const { stats } = props.contestStats.data;
    const { seasonPodiums } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonPodiums ? 1000 : LIMIT;

    seasonPodiums.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonWinsGPMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonWinsGP } = stats.players;
    if (seasonWinsGP.length > 0) {
      const items = [];
      let place = 0;
      seasonWinsGP.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonWinsGP() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonWinsGP } = state;
    const { stats } = props.contestStats.data;
    const { seasonWinsGP } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonWinsGP ? 1000 : LIMIT;

    seasonWinsGP.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonSecondGPMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonSecondGP } = stats.players;
    if (seasonSecondGP.length > 0) {
      const items = [];
      let place = 0;
      seasonSecondGP.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonSecondGP() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonSecondGP } = state;
    const { stats } = props.contestStats.data;
    const { seasonSecondGP } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonSecondGP ? 1000 : LIMIT;

    seasonSecondGP.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonThirdGPMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonThirdGP } = stats.players;
    if (seasonThirdGP.length > 0) {
      const items = [];
      let place = 0;
      seasonThirdGP.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonThirdGP() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonThirdGP } = state;
    const { stats } = props.contestStats.data;
    const { seasonThirdGP } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonThirdGP ? 1000 : LIMIT;

    seasonThirdGP.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderSeasonPodiumsGPMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { seasonPodiumsGP } = stats.players;
    if (seasonPodiumsGP.length > 0) {
      const items = [];
      let place = 0;
      seasonPodiumsGP.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderSeasonPodiumsGP() {
    const elements = [];
    const { props, state } = this;
    const { showAllSeasonPodiumsGP } = state;
    const { stats } = props.contestStats.data;
    const { seasonPodiumsGP } = stats.players;
    let cnt = 0;
    const limit = showAllSeasonPodiumsGP ? 1000 : LIMIT;

    seasonPodiumsGP.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderRoundWinsMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { roundWins } = stats.players;
    if (roundWins.length > 0) {
      const items = [];
      let place = 0;
      roundWins.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderRoundWins() {
    const elements = [];
    const { props, state } = this;
    const { showAllRoundWins } = state;
    const { stats } = props.contestStats.data;
    const { roundWins } = stats.players;
    let cnt = 0;
    const limit = showAllRoundWins ? 1000 : LIMIT;

    roundWins.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Wygrane">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderRoundSecondMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { roundSecond } = stats.players;
    if (roundSecond.length > 0) {
      const items = [];
      let place = 0;
      roundSecond.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderRoundSecond() {
    const elements = [];
    const { props, state } = this;
    const { showAllRoundSecond } = state;
    const { stats } = props.contestStats.data;
    const { roundSecond } = stats.players;
    let cnt = 0;
    const limit = showAllRoundSecond ? 1000 : LIMIT;

    roundSecond.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Drugie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderRoundThirdMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { roundThird } = stats.players;
    if (roundThird.length > 0) {
      const items = [];
      let place = 0;
      roundThird.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderRoundThird() {
    const elements = [];
    const { props, state } = this;
    const { showAllRoundThird } = state;
    const { stats } = props.contestStats.data;
    const { roundThird } = stats.players;
    let cnt = 0;
    const limit = showAllRoundThird ? 1000 : LIMIT;

    roundThird.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Trzecie miejsca">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderRoundPodiumMobile() {
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { roundPodiums } = stats.players;
    if (roundPodiums.length > 0) {
      const items = [];
      let place = 0;
      roundPodiums.forEach((item) => {
        place += 1;
        const link = `/contest/player/${item.alias}`;
        const element = (
          <Item key={item.alias}>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                {place}
                {". "}
                <NavLink to={link}>{item.name}</NavLink>
              </Item.Header>
            </Item.Content>
            <div floated="right">{item.amount}</div>
          </Item>
        );
        items.push(element);
      });
      return items;
    }
    return "";
  }

  renderRoundPodium() {
    const elements = [];
    const { props, state } = this;
    const { showAllRoundPodium } = state;
    const { stats } = props.contestStats.data;
    const { roundPodiums } = stats.players;
    let cnt = 0;
    const limit = showAllRoundPodium ? 1000 : LIMIT;

    roundPodiums.every((item) => {
      if (cnt == limit) {
        return false;
      }
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Podium">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
      return true;
    });
    return elements;
  }

  renderPointsGP() {
    const elements = [];
    const { props } = this;
    const { stats } = props.contestStats.data;
    const { pointsGP } = stats.players;
    let cnt = 0;
    pointsGP.forEach((item) => {
      cnt += 1;
      const element = (
        <Table.Row key={`class-${item.alias}`}>
          <Table.Cell data-title="Miejsce">{cnt}.</Table.Cell>
          <Table.Cell data-title="Gracz" className="left">
            <NavLink to={`/contest/player/${item.alias}`}>{item.name}</NavLink>
          </Table.Cell>
          <Table.Cell data-title="Punkty">
            {item.amount === null ? String.fromCharCode(160) : item.amount}
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    if (!props.contestStats.data || props.contestStats.loading) {
      return (
        <section
          id="contest-stats-details"
          name="contest-stats-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const {
      showAllRoundWins,
      showAllRoundSecond,
      showAllRoundThird,
      showAllRoundPodium,
      showAllSeasonWins,
      showAllSeasonSecond,
      showAllSeasonThird,
      showAllSeasonPodiums,
      showAllSeasonWinsGP,
      showAllSeasonSecondGP,
      showAllSeasonThirdGP,
      showAllSeasonPodiumsGP,
    } = state;
    const { year } = props.match.params;
    const { stats, season } = props.contestStats.data;

    if (season != null && stats.players.length === 0) {
      return (
        <section
          id="contest-stats-details"
          name="contest-stats-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.contest.stats.title"}
                values={{ year: year }}
              />
            }
            subtitle={
              <FormattedMessage
                id={"app.page.contest.stats.subtitle2"}
                values={{ year: year }}
              />
            }
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage
                  id={"app.page.contest.stats.info1"}
                  values={{ year: year }}
                />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="contest-stats-details"
        name="contest-stats-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.contest.stats.title"}
              values={{ year: year }}
            />
          }
          subtitle={
            <FormattedMessage
              id={"app.page.contest.stats.subtitle"}
              values={{ minSeason: "2004", maxSeason: "2024" }}
            />
          }
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered columns="equal">
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                {stats.players.seasons.length >= 0 && (
                  <>
                    <div className="info-box">
                      <Segment basic>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-seasons" inverted>
                            <Statistic.Value>
                              {stats.contest.seasons}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.seasons"} />
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-players" inverted>
                            <Statistic.Value>
                              {stats.contest.players}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.players"} />
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-winners" inverted>
                            <Statistic.Value>
                              {Math.round(stats.contest.winners, 1)}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.1st"} />
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-second" inverted>
                            <Statistic.Value>
                              {Math.round(stats.contest.second, 1)}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.2nd"} />
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-third" inverted>
                            <Statistic.Value>
                              {Math.round(stats.contest.third, 1)}
                            </Statistic.Value>
                            <Statistic.Label>
                              <FormattedMessage id={"app.stats.3rd"} />
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                        <Segment basic textAlign="center">
                          <Statistic size="large" id="contest-podiums" inverted>
                            <Statistic.Value>
                              {stats.contest.podium}
                            </Statistic.Value>
                            <Statistic.Label>
                              {<FormattedMessage id={"app.stats.podium"} />}
                            </Statistic.Label>
                          </Statistic>
                          <Divider></Divider>
                        </Segment>
                      </Segment>
                    </div>
                  </>
                )}
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Grid stackable centered columns="equal">
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.seasons"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderSeasons()}</Table.Body>
                          </Table>
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.rounds"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderRoundsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderRounds()}</Table.Body>
                          </Table>
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.points"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderPointsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderPoints()}</Table.Body>
                          </Table>
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.points.gp"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderPointsGPMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderPointsGP()}</Table.Body>
                          </Table>
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid>
                <Segment padded basic>
                  <SectionPageHeader
                    title={<FormattedMessage id={"app.stats.class.standard"} />}
                    type="tertiary"
                  />
                </Segment>
                <Grid stackable centered columns="equal">
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={
                          <FormattedMessage id={"app.stats.season.wins"} />
                        }
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonWinsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderSeasonWins()}</Table.Body>
                          </Table>
                          {stats.players.seasonWins.length > LIMIT && (
                            <>
                              {showAllSeasonWins ? (
                                <Button onClick={this.handleShowAllSeasonWins}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllSeasonWins}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.2nd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonSecondMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderSeasonSecond()}</Table.Body>
                          </Table>
                          {stats.players.seasonSecond.length > LIMIT && (
                            <>
                              {showAllSeasonSecond ? (
                                <Button
                                  onClick={this.handleShowAllSeasonSecond}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonSecond}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.3rd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonThirdMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderSeasonThird()}</Table.Body>
                          </Table>
                          {stats.players.seasonThird.length > LIMIT && (
                            <>
                              {showAllSeasonThird ? (
                                <Button onClick={this.handleShowAllSeasonThird}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllSeasonThird}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.podium"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonPodiumsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {this.renderSeasonPodiums()}
                            </Table.Body>
                          </Table>
                          {stats.players.seasonPodiums.length > LIMIT && (
                            <>
                              {showAllSeasonPodiums ? (
                                <Button
                                  onClick={this.handleShowAllSeasonPodiums}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonPodiums}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid>
                <Segment padded basic>
                  <SectionPageHeader
                    title={<FormattedMessage id={"app.stats.class.gp"} />}
                    type="tertiary"
                  />
                </Segment>
                <Grid stackable centered columns="equal">
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={
                          <FormattedMessage id={"app.stats.season.wins"} />
                        }
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonWinsGPMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderSeasonWinsGP()}</Table.Body>
                          </Table>
                          {stats.players.seasonWinsGP.length > LIMIT && (
                            <>
                              {showAllSeasonWinsGP ? (
                                <Button
                                  onClick={this.handleShowAllSeasonWinsGP}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonWinsGP}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.2nd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonSecondGPMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {this.renderSeasonSecondGP()}
                            </Table.Body>
                          </Table>
                          {stats.players.seasonSecondGP.length > LIMIT && (
                            <>
                              {showAllSeasonSecondGP ? (
                                <Button
                                  onClick={this.handleShowAllSeasonSecondGP}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonSecondGP}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.3rd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonThirdGPMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {this.renderSeasonThirdGP()}
                            </Table.Body>
                          </Table>
                          {stats.players.seasonThirdGP.length > LIMIT && (
                            <>
                              {showAllSeasonThirdGP ? (
                                <Button
                                  onClick={this.handleShowAllSeasonThirdGP}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonThirdGP}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.podium"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderSeasonPodiumsGPMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>
                              {this.renderSeasonPodiumsGP()}
                            </Table.Body>
                          </Table>
                          {stats.players.seasonPodiumsGP.length > LIMIT && (
                            <>
                              {showAllSeasonPodiumsGP ? (
                                <Button
                                  onClick={this.handleShowAllSeasonPodiumsGP}
                                >
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button
                                  onClick={this.handleShowAllSeasonPodiumsGP}
                                >
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid>
                <Segment padded basic>
                  <SectionPageHeader
                    title={<FormattedMessage id={"app.stats.rounds"} />}
                    type="tertiary"
                  />
                </Segment>
                <Grid stackable centered columns="equal">
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.1st"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderRoundWinsMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderRoundWins()}</Table.Body>
                          </Table>
                          {stats.players.roundWins.length > LIMIT && (
                            <>
                              {showAllRoundWins ? (
                                <Button onClick={this.handleShowAllRoundWins}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllRoundWins}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.2nd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderRoundSecondMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderRoundSecond()}</Table.Body>
                          </Table>
                          {stats.players.roundSecond.length > LIMIT && (
                            <>
                              {showAllRoundSecond ? (
                                <Button onClick={this.handleShowAllRoundSecond}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllRoundSecond}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.3rd"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderRoundThirdMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderRoundThird()}</Table.Body>
                          </Table>
                          {stats.players.roundThird.length > LIMIT && (
                            <>
                              {showAllRoundThird ? (
                                <Button onClick={this.handleShowAllRoundThird}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllRoundThird}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={8} computer={4}>
                    <Segment padded basic>
                      <SectionPageHeader
                        title={<FormattedMessage id={"app.stats.podium"} />}
                      />
                      <div>
                        <Segment basic className="class-mobile">
                          <Item.Group divided unstackable>
                            {this.renderRoundPodiumMobile()}
                          </Item.Group>
                        </Segment>
                        <Segment basic className="class-desktop overflow">
                          <Table
                            basic="very"
                            celled
                            className="responsive-table center-aligned"
                          >
                            <Table.Header>
                              <Table.Row>
                                <Table.HeaderCell>#</Table.HeaderCell>
                                <Table.HeaderCell className="left">
                                  <FormattedMessage
                                    id={"app.table.header.player"}
                                  />
                                </Table.HeaderCell>
                                <Table.HeaderCell>
                                  <FormattedMessage
                                    id={"app.table.header.sum"}
                                  />
                                </Table.HeaderCell>
                              </Table.Row>
                            </Table.Header>
                            <Table.Body>{this.renderRoundPodium()}</Table.Body>
                          </Table>
                          {stats.players.roundPodiums.length > LIMIT && (
                            <>
                              {showAllRoundPodium ? (
                                <Button onClick={this.handleShowAllRoundPodium}>
                                  <FormattedMessage
                                    id={"app.button.collapse"}
                                  />
                                </Button>
                              ) : (
                                <Button onClick={this.handleShowAllRoundPodium}>
                                  <FormattedMessage id={"app.button.expand"} />
                                </Button>
                              )}
                            </>
                          )}
                        </Segment>
                      </div>
                    </Segment>
                  </Grid.Column>
                </Grid>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

ContestStats.propTypes = {
  fetchContestAwards: PropTypes.func.isRequired,
  fetchContestPlacesStats: PropTypes.func.isRequired,
  fetchContestStats: PropTypes.func.isRequired,
  contestStats: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboContestSeasons: PropTypes.func.isRequired,
  comboContestSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
  awards: PropTypes.object,
  contestPlacesStats: PropTypes.object,
};

function mapStateToProps({
  contestStats,
  comboContestSeasons,
  awards,
  contestPlacesStats,
}) {
  return {
    contestStats,
    comboContestSeasons,
    awards,
    contestPlacesStats,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchContestStats,
      fetchComboContestSeasons,
      fetchContestAwards,
      fetchContestPlacesStats,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ContestStats);
