/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleSubmit"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Loader, Message } from "semantic-ui-react";
import OwlCarousel from "react-owl-carousel";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { fetchStatsTrivia } from "../../actions/StatsTriviaActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./StatsWidget.less";

class StatsWidget extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchStatsTrivia(Cookies.get("lang"));
  }

  renderStatsTrivia() {
    const elements = [];
    const { statsTrivia } = this.props;

    if (statsTrivia.data) {
      statsTrivia.data.forEach((item, idx) => {
        const element = (
          <div key={idx}>
            <Message
              icon="question circle"
              color="blue"
              header={
                <FormattedMessage id={"app.page.home.stats.widget.title"} />
              }
              content={item.text}
            />
            <NavLink key={idx} to={item.link}>
              <div className="link">
                {" "}
                <FormattedMessage id={"app.page.home.stats.widget.footer"} />
              </div>
            </NavLink>
          </div>
        );
        elements.push(element);
      });
    }
    return elements;
  }

  render() {
    const { props } = this;
    if (!props.statsTrivia.data || props.statsTrivia.loading) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    return (
      <section id="stats-widget" name="stats-widget" className="section-page">
        <div className="stats-widget-content">
          <OwlCarousel
            className="owl-theme"
            ref={(c) => {
              this.home = c;
            }}
            items={1}
            loop={false}
            dots={false}
          >
            {this.renderStatsTrivia()}
          </OwlCarousel>
        </div>
      </section>
    );
  }
}

StatsWidget.propTypes = {
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      statId: PropTypes.string,
    }),
  }),
  fetchStatsTrivia: PropTypes.func.isRequired,
  statsTrivia: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
};

function mapStateToProps({ statsTrivia }) {
  return {
    statsTrivia,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchStatsTrivia,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(StatsWidget);
