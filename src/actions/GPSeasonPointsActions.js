// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_GPSEASON_POINTS_REQUEST = "FETCH_GPSEASON_POINTS_REQUEST";
export const FETCH_GPSEASON_POINTS_SUCCESS = "FETCH_GPSEASON_POINTS_SUCCESS";
export const FETCH_GPSEASON_POINTS_ERROR = "FETCH_GPSEASON_POINTS_ERROR";

function requestData() {
  return {
    type: FETCH_GPSEASON_POINTS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_GPSEASON_POINTS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_GPSEASON_POINTS_ERROR,
    payload: error,
  };
}

export function fetchGPSeasonPoints(year, lang) {
  const url = `${ROOT_URL}/get_gp_season_points.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
