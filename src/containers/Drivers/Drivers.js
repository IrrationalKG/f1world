/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderSubmenu","renderDriverSubmenu"] }] */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavLink } from "react-router-dom";
import {
  Image,
  Segment,
  Flag,
  Grid,
  Loader,
  Item,
  Header,
  Divider,
  Label,
} from "semantic-ui-react";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchMenu } from "../../actions/MenuActions";
import { FormattedMessage } from "react-intl";
import styles from "./Drivers.less";

class Drivers extends Component {
  constructor(props, context) {
    super(props, context);
    this.renderDrivers = this.renderDrivers.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchMenu(props.match.params.year);
  }

  renderDrivers() {
    const items = [];
    const { props } = this;
    const { data } = props.menu;
    if (data) {
      data[0].items.forEach((item) => {
        const {
          id,
          alias,
          name,
          surname,
          number,
          team_color,
          seasonPlace,
          seasonPoints,
          country,
          team,
          bestResult,
          wins,
          points,
          podium,
          starts,
          polePosition,
          bestLaps,
          seasons,
        } = item;
        const filename = `/build/images/drivers/driver_${id}_profile.jpg`;
        const detailsLink = `/driver/${props.match.params.year}/${alias}`;
        const record = (
          <Grid.Column
            key={id}
            textAlign="center"
            mobile={8}
            tablet={4}
            computer={2}
            className="menu-box-container"
          >
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <Segment basic>
                    <Image
                      fluid
                      label={{
                        corner: "left",
                        content: `#${number}`,
                        size: "large",
                        horizontal: true,
                        className: `${team_color}`,
                      }}
                    />
                    <NavLink to={detailsLink}>
                      <Image
                        centered
                        src={filename}
                        alt={`${name} ${surname}`}
                        className="menu-link"
                        onError={(e) => {
                          e.target.src =
                            "/build/images/drivers/driver_no_profile.jpg";
                        }}
                      />
                    </NavLink>
                    <Label attached="bottom" color="red">
                      {bestResult == "Debiutant" ? (
                        <FormattedMessage id={"app.stats.debut"} />
                      ) : bestResult.includes("MŚ") ? (
                        <>
                          {bestResult.substring(0, 2)}{" "}
                          <FormattedMessage id={"app.stats.wc"} />
                        </>
                      ) : (
                        bestResult
                      )}
                    </Label>
                  </Segment>
                  <Segment basic className="menu-box-flex">
                    <Divider hidden fitted></Divider>
                    <Header as="h2">
                      <Header.Content>
                        <Flag name={country} /> {name} {surname}
                        <Header.Subheader>{team}</Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider></Divider>
                    <Header as="h3">
                      <Header.Content>
                        <FormattedMessage id={"app.stats.season"} />{" "}
                        {props.year}
                        <Header.Subheader>
                          {seasonPlace}
                          {". ("}
                          {seasonPoints}{" "}
                          <FormattedMessage id={"app.stats.pts"} />
                          {")"}
                        </Header.Subheader>
                      </Header.Content>
                    </Header>
                    <Divider hidden fitted></Divider>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column textAlign="left" className="info-box-light">
                  <Item.Group divided>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.seasons"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/starts/${alias}`}
                            >
                              {seasons}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.gp"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/starts/${alias}`}
                            >
                              {starts}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.wins"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/wins/${alias}`}
                            >
                              {wins}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.points"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/points/${alias}`}
                            >
                              {points}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.podium"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/podium/${alias}`}
                            >
                              {podium}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.polepos"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/polepos/${alias}`}
                            >
                              {polePosition}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                    <Item>
                      <Item.Content verticalAlign="middle">
                        <Item.Header>
                          <FormattedMessage id={"app.stats.bestlaps"} />
                        </Item.Header>
                        <Item.Meta>
                          <span>
                            <NavLink
                              className="link"
                              to={`/driver-events/bestlaps/${alias}`}
                            >
                              {bestLaps}
                            </NavLink>
                          </span>
                        </Item.Meta>
                      </Item.Content>
                    </Item>
                  </Item.Group>
                  <div className="buttons">
                    <NavLink className="details menu-link" to={detailsLink}>
                      <FormattedMessage id={"app.button.details"} />
                    </NavLink>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        );
        items.push(record);
      });
    }
    return items;
  }

  render() {
    const { props } = this;
    if (!props.menu.data || props.menu.loading) {
      return (
        <section id="drivers" name="drivers" className="section-page">
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const picBanner = "../build/images/drivers/drivers_banner.jpg";
    return (
      <section id="drivers" name="drivers" className="section-page">
        <SectionPageBanner
          pic={picBanner}
          title={<FormattedMessage id={"app.page.drivers.title"} />}
          subtitle={
            <FormattedMessage
              id={"app.page.drivers.subtitle"}
              values={{ season: props.match.params.year }}
            />
          }
        />
        <div>
          <Grid verticalAlign="top" centered>
            {this.renderDrivers()}
          </Grid>
        </div>
      </section>
    );
  }
}

Drivers.propTypes = {
  fetchMenu: PropTypes.func.isRequired,
  menu: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.array, PropTypes.bool, PropTypes.string])
  ),
  match: PropTypes.shape({
    params: PropTypes.shape({
      year: PropTypes.string,
    }).isRequired,
  }).isRequired,
  year: PropTypes.string,
};

function mapStateToProps({ menu }) {
  return { menu };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMenu }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Drivers);
