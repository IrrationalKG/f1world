// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TEAM_MODEL_EVENTS_REQUEST =
  "FETCH_TEAM_MODEL_EVENTS_REQUEST";
export const FETCH_TEAM_MODEL_EVENTS_SUCCESS =
  "FETCH_TEAM_MODEL_EVENTS_SUCCESS";
export const FETCH_TEAM_MODEL_EVENTS_ERROR = "FETCH_TEAM_MODEL_EVENTS_ERROR";

function requestData() {
  return {
    type: FETCH_TEAM_MODEL_EVENTS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TEAM_MODEL_EVENTS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TEAM_MODEL_EVENTS_ERROR,
    payload: error,
  };
}

export function fetchTeamModelEvents(
  page,
  idTeamModel,
  event,
  pos,
  season,
  driver,
  team,
  lang
) {
  const url = `${ROOT_URL}/get_team_model_events.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          page: page,
          idTeamModel: idTeamModel,
          event: event,
          pos: pos,
          season: season,
          driver: driver,
          team: team,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
