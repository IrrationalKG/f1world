/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Header,
  Segment,
  Item,
  Flag,
  Loader,
  Statistic,
  Image,
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import ReactHighcharts from "react-highcharts";
import highcharts3d from "highcharts-3d";

highcharts3d(ReactHighcharts.Highcharts);
require("highcharts-series-label")(ReactHighcharts.Highcharts);

import PropTypes from "prop-types";
import { fetchSeasonClass } from "../../actions/SeasonClassActions";
import { FormattedMessage } from "react-intl";
import styles from "./SeasonClass.less";

class SeasonClass extends Component {
  constructor(props, context) {
    super(props, context);

    this.renderDriversClass = this.renderDriversClass.bind(this);
    this.renderTeamsClass = this.renderTeamsClass.bind(this);
  }

  componentDidMount() {
    const { props } = this;
    props.fetchSeasonClass(props.year);
  }

  renderDriversClass() {
    const driversItems = [];
    const { props } = this;
    const { data } = props.seasonClass;
    let cnt = 0;
    data[0].items.forEach((item) => {
      const filename = `build/images/drivers/driver_${item.id}_profile.jpg`;
      const link = `/driver/${item.alias}`;
      const driverItem = (
        <Item key={item.id}>
          <NavLink to={link} className="image-link">
            <Image size="small" src={filename} alt={item.name} />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              <NavLink to={link}>
                {item.seasonPlace}
                {". "}
                <Flag name={item.country} />
                {item.name}
              </NavLink>
            </Item.Header>
            <Item.Description>{item.team}</Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right" inverted>
              <Statistic.Value>
                {item.seasonPoints > 0 ? (
                  <NavLink
                    to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${props.year}/1`}
                  >
                    {item.seasonPoints}
                  </NavLink>
                ) : (
                  item.seasonPoints
                )}
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      driversItems.push(driverItem);

      cnt += 1;
    });
    return driversItems;
  }

  renderTeamsClass() {
    const teamsItems = [];
    const { props } = this;
    const { data } = props.seasonClass;
    let cnt = 0;
    data[1].items.forEach((item) => {
      if (cnt < 10) {
        let filename = "/build/images/teams/team_no_profile.jpg";
        if (item.picture === "1") {
          filename = `/build/images/teams/team_${item.id}_profile_logo.jpg`;
        }
        const link = `/team/${item.alias}`;
        const teamItem = (
          <Item key={item.id}>
            <NavLink to={link} className="image-link">
              <Image size="small" src={filename} alt={item.name} />
            </NavLink>
            <Item.Content verticalAlign="middle">
              <Item.Header>
                <NavLink to={link}>
                  {item.seasonPlace}
                  {". "}
                  <Flag name={item.country} />
                  {item.name}
                </NavLink>
              </Item.Header>
              <Item.Description>{item.engine}</Item.Description>
            </Item.Content>
            <Item.Content verticalAlign="middle">
              <Statistic floated="right" inverted>
                <Statistic.Value>
                  {item.seasonPoints > 0 ? (
                    <NavLink
                      to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${props.year}/1`}
                    >
                      {item.seasonPoints}
                    </NavLink>
                  ) : (
                    item.seasonPoints
                  )}
                </Statistic.Value>
                <Statistic.Label>
                  <FormattedMessage id={"app.stats.pts"} />
                </Statistic.Label>
              </Statistic>
            </Item.Content>
          </Item>
        );
        teamsItems.push(teamItem);
      }
      cnt += 1;
    });
    return teamsItems;
  }

  render() {
    const { props } = this;
    if (!props.seasonClass.data) {
      return (
        <Loader style={{ display: "none" }} active inline="centered">
          <FormattedMessage id={"app.loading"} />
        </Loader>
      );
    }
    const pic = "build/images/drivers/drivers_banner.jpg";
    const picName = pic.substring(0, pic.length - 4);
    const picp = `${picName}.webp`;
    const pic1024w = `${picName}-1024w.jpg`;
    const pic1024wp = `${picName}-1024w.webp`;

    const picTeams = "build/images/teams/teams_banner.jpg";
    const picTeamsName = picTeams.substring(0, picTeams.length - 4);
    const picTeamsp = `${picTeamsName}.webp`;
    const picTeams1024w = `${picTeamsName}-1024w.jpg`;
    const picTeams1024wp = `${picTeamsName}-1024w.webp`;

    const linkDrivers = `/classification/drivers-places/${props.year}`;
    const linkTeams = `/classification/teams-places/${props.year}`;

    return (
      <section id="season-class" name="season-class">
        <div className="season-class-header">
          <h2>
            <FormattedMessage id={"app.stats.class"} />
          </h2>
          <h3>
            <FormattedMessage id={"app.stats.season"} /> {props.year}
          </h3>
        </div>
        <div>
          <div className="season-class-content info-box">
            <div className="season-class-banner">
              <picture>
                <source
                  media="(max-width: 1024px)"
                  srcSet={pic1024wp}
                  type="image/webp"
                />
                <source media="(max-width: 1024px)" srcSet={pic1024w} />
                <source srcSet={picp} type="image/webp" />
                <img src={pic} alt="Klasyfikacja" />
              </picture>
            </div>
            <Header
              size="large"
              textAlign="center"
              className="sidebar-box-header-primary"
            >
              <FormattedMessage id={"app.stats.drivers"} />
            </Header>
            <Segment basic>
              <Item.Group divided unstackable>
                {this.renderDriversClass()}
              </Item.Group>
              <div className="buttons">
                <NavLink className="tertiary" to={linkDrivers}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Segment>
          </div>
          <div className="season-class-content info-box">
            <div className="season-class-banner">
              <picture>
                <source
                  media="(max-width: 1024px)"
                  srcSet={picTeams1024wp}
                  type="image/webp"
                />
                <source media="(max-width: 1024px)" srcSet={picTeams1024w} />
                <source srcSet={picTeamsp} type="image/webp" />
                <img src={picTeams} alt="Klasyfikacja" />
              </picture>
            </div>
            <Header
              size="large"
              textAlign="center"
              className="sidebar-box-header-primary"
            >
              <FormattedMessage id={"app.stats.teams"} />
            </Header>
            <Segment basic>
              <Item.Group divided unstackable>
                {this.renderTeamsClass()}
              </Item.Group>
              <div className="buttons">
                <NavLink className="tertiary" to={linkTeams}>
                  <FormattedMessage id={"app.button.details"} />
                </NavLink>
              </div>
            </Segment>
          </div>
        </div>
      </section>
    );
  }
}

SeasonClass.propTypes = {
  fetchSeasonClass: PropTypes.func.isRequired,
  seasonClass: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.object,
    ])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ seasonClass }) {
  return { seasonClass };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchSeasonClass }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SeasonClass);
