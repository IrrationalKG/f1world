<?
header('Access-Control-Allow-Origin: *');

$id=isset($_GET['id']) ? $_GET['id'] : null;
if ($id==null) $id=isset($_POST['id']) ? $_POST['id'] : null;

$year=isset($_GET['year']) ? $_GET['year'] : null;
if ($year==null) $year=isset($_POST['year']) ? $_POST['year'] : null;

$selectedSeason=isset($_GET['selectedSeason']) ? $_GET['selectedSeason'] : null;
if ($selectedSeason==null) $selectedSeason=isset($_POST['selectedSeason']) ? $_POST['selectedSeason'] : null;

$lang=isset($_GET['lang']) ? $_GET['lang'] : null;
if ($lang==null) $lang=isset($_POST['lang']) ? $_POST['lang'] : "pl";
include("dbinfo.inc.php");

include("dbinfo.inc.php");

$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");

if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
}

$selected = mysqli_SELECT_db($dbhandle, $database)
or die("Could not SELECT examples");

$response = array();

$start_time = microtime(true);
$startTime1 = microtime(true);

// aktualny rok
$query="SELECT max(season) maxYear FROM teams_class";
$result = mysqli_query($dbhandle,$query);
$currentYear;
while($r = mysqli_fetch_assoc($result)) {
  $currentYear = $r["maxYear"];
}

// pobranie kolejnosci zespołów
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name asc";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams group by team order by name asc";
}
$result = mysqli_query($dbhandle,$query);
$teamsTab = array();
$cnt=0;
$currentTeamCnt=0;
$maxTeamCnt=0;
while($r = mysqli_fetch_assoc($result)) {
  $teamsTab[$cnt] = $r["alias"];
  if ($id == $r["alias"]){
    $currentTeamCnt = $cnt;
  }
  $cnt+=1;
}
$maxTeamCnt = $cnt;

// min pozycja
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name asc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams order by name asc limit 1";
}
$result = mysqli_query($dbhandle,$query);
$min;
while($r = mysqli_fetch_assoc($result)) {
  $min = $r["alias"];
}

// max pozycja
if ($year) {
  $query="SELECT alias_name alias FROM teams,teams_class WHERE teams.id_team=teams_class.id_team AND teams_class.season=$year order by name desc LIMIT 1";
}else{
  $query="SELECT DISTINCT alias_name alias FROM teams order by name desc limit 1";
}
$result = mysqli_query($dbhandle,$query);
$max;
while($r = mysqli_fetch_assoc($result)) {
  $max = $r["alias"];
}

// poprzedni zespół
if (($currentTeamCnt-1)<0){
  $prevId=$max;
}else{
  $prevId = $teamsTab[$currentTeamCnt-1];
}
// nastepny zespół
if (($currentTeamCnt+1)>=$maxTeamCnt){
  $nextId=$min;
}else{
  $nextId = $teamsTab[$currentTeamCnt+1];
}

// pobranie id
$query="SELECT lower(team) id FROM teams WHERE alias_name='$id'";
$result = mysqli_query($dbhandle,$query);
$id;
while($r = mysqli_fetch_assoc($result)) {
  $id = $r["id"];
}

// ostatni sezon zespolu
$query="SELECT max(season) maxYear FROM drivers_gp_involvements WHERE team='$id'";
$result = mysqli_query($dbhandle,$query);
$teamMaxYear;
while($r = mysqli_fetch_assoc($result)) {
  $teamMaxYear = $r["maxYear"];
}
if ($selectedSeason==null) $selectedSeason=$teamMaxYear;

// kierowcy
$teamDrivers= array();
if ($year) {
  $query="SELECT DISTINCT drivers.alias, drivers.id_driver id, CONCAT(drivers.name, ' ', drivers.surname) driver, drivers.country, drivers.country_code countryCode,
  (SELECT dc.points_class FROM drivers_class dc WHERE dc.id_driver=drivers.id_driver AND dc.season=drivers_class.season ORDER BY dc.season DESC LIMIT 1) points,
  (SELECT dc.place FROM drivers_class dc WHERE dc.id_driver=drivers.id_driver AND dc.season=drivers_class.season ORDER BY dc.season DESC LIMIT 1) place,
  drivers_gp_involvements.season
  FROM drivers_gp_involvements
  LEFT JOIN drivers_class ON drivers_gp_involvements.season=drivers_class.season
  AND drivers_gp_involvements.id_driver=drivers_class.id_driver AND drivers_class.season = '$year'
  LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
  WHERE drivers_gp_involvements.team = '$id' 
  AND drivers_gp_involvements.season = '$year'
  ORDER BY drivers_class.season DESC, drivers_class.place ASC";
  $result = mysqli_query($dbhandle,$query);
  while($r = mysqli_fetch_assoc($result)) {
    $teamDrivers[] = $r;
  }
}

// informacje o zespole
$query="SELECT alias_name alias,lower(team) id, name, group_concat(distinct coalesce(engine,name) SEPARATOR ', ') engineNames, fullname,";
if ($lang=='pl') {
  $query.="GROUP_CONCAT(distinct country ORDER BY id_team desc SEPARATOR ', ') country";
}else{
  $query.="GROUP_CONCAT(distinct country_en ORDER BY id_team desc SEPARATOR ', ') country";
} 
$query.=",GROUP_CONCAT(distinct country_code ORDER BY id_team desc SEPARATOR ',') countryCode, lower(country_short) countryShort,
location, internet, picture, banner, '' prevId, '' nextId
FROM teams WHERE teams.team='$id'
order by id_team desc";
$result = mysqli_query($dbhandle,$query);
$teamItems;
while($r = mysqli_fetch_assoc($result)) {
   $r["prevId"]=$prevId;
   $r["nextId"]=$nextId;
   $teamItems = $r;
}
$teamItems["teamMaxYear"] = $teamMaxYear;
$teamItems["selectedSeason"] = $selectedSeason;
$teamItems["currentDrivers"] = $teamDrivers;
$teamItems["[1] Time - info"]=microtime(true)-$startTime1;
$startTime2 = microtime(true);

/**
* Statystyki
**/
$statsItems;

//sezony
$query="SELECT count(items) as seasons FROM (
SELECT season, group_concat(distinct season) as items
FROM drivers_gp_involvements 
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.season,drivers_gp_involvements.season
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["seasons"] = $r["seasons"];
}

//liczba gp
$query="SELECT count(items) as gp FROM (
SELECT season, group_concat(distinct id_gp) as items
FROM drivers_gp_involvements 
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.season,drivers_gp_involvements.id_gp
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["gp"] = $r["gp"];
}

//liczba kwalifikacji
$query="SELECT count(items) as qual FROM (
SELECT season, group_concat(distinct qual_date) as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_pp_results.qual_date,teams.team
ORDER BY drivers_pp_results.qual_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["qual"] = $r["qual"];
}

//liczba wyscigow
$query="SELECT count(items) as starts FROM (
SELECT season, group_concat(distinct race_date) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_gp_results.race_date,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["starts"] = $r["starts"];
}

//liczba sprintow
$query="SELECT count(items) as sprints FROM (
SELECT season, group_concat(distinct sprint_date) as items
FROM drivers_sprint_results,teams 
WHERE drivers_sprint_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_sprint_results.sprint_date,teams.team
ORDER BY drivers_sprint_results.sprint_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["sprints"] = $r["sprints"];
}

//zwyciestwa
$query="SELECT  count(items) as wins FROM (
SELECT season, group_concat(distinct race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos=1  
AND teams.team='$id'
group by drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["wins"] = $r["wins"];
}

//punkty
$query="SELECT COALESCE(SUM(points),0) points,teams.name,teams.team,teams.country_short 
FROM teams_class,teams 
WHERE teams_class.id_team=teams.id_team 
AND teams.team='$id'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["points"] = $r["points"];
}

//podium
$query="SELECT count(items) as podium FROM (
SELECT season, group_concat(distinct race_pos) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos < 4  
AND teams.team='$id'
group by drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["podium"] = $r["podium"];
}

//pole positions
$query="SELECT count(items) as polepos FROM (
SELECT season, group_concat(distinct id_starting_grid) as items
FROM drivers_gp_starting_grid,teams 
WHERE drivers_gp_starting_grid.id_team=teams.id_team
AND drivers_gp_starting_grid.is_pp=1  
AND teams.team='$id'
group by drivers_gp_starting_grid.id_starting_grid,teams.team,drivers_gp_starting_grid.grid_pos
ORDER BY drivers_gp_starting_grid.season ASC, drivers_gp_starting_grid.grid_pos ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["polepos"] = $r["polepos"];
}

//najlepsze okrazenia
$query="SELECT count(items) as bestlaps FROM (
SELECT season, group_concat(distinct race_best_lap) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_best_lap=1
AND teams.team='$id'
group by drivers_gp_results.race_date,teams.team,drivers_gp_results.race_best_lap
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_best_lap ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["bestlaps"] = $r["bestlaps"];
}

//z punktami
$query="SELECT count(items) as pointPlaces FROM (
SELECT drivers_gp_involvements.season, group_concat(distinct race_date) as items
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957
GROUP BY drivers_gp_results.race_date,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["pointPlaces"] = $r["pointPlaces"];
}

//bez punktow
$query="SELECT count(items) as noPointPlaces FROM (
SELECT drivers_gp_involvements.season, group_concat(distinct race_date) as items
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957
AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND d.team=drivers_gp_results.team)
GROUP BY drivers_gp_results.race_date,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["noPointPlaces"] = $r["noPointPlaces"];
}

//niezakwalifikowany
$query="SELECT count(items) as notQualified FROM (
SELECT season, group_concat(distinct id_drivers_pp) as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND teams.team='$id'
AND drivers_pp_results.not_qualified=1
GROUP BY drivers_pp_results.id_drivers_pp,teams.team
ORDER BY drivers_pp_results.qual_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["notQualified"] = $r["notQualified"];
}

//nie wystartowal
$query="SELECT count(items) as notStarted FROM (
SELECT season, group_concat(distinct id_drivers_pp) as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND teams.team='$id'
AND drivers_pp_results.not_started=1
GROUP BY drivers_pp_results.id_drivers_pp,teams.team
ORDER BY drivers_pp_results.qual_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["notStarted"] = $r["notStarted"];
}

// z kara
$query="SELECT count(items) as penalty FROM (
SELECT season, group_concat(distinct id_drivers_gp) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND penalty=1
GROUP BY drivers_gp_results.id_drivers_gp,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["penalty"] = $r["penalty"];
}

//sklasyfikowani
$query="SELECT count(items) as completed FROM (
SELECT season, group_concat(distinct id_drivers_gp) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_completed=1
GROUP BY drivers_gp_results.id_drivers_gp,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["completed"] = $r["completed"];
}

//niesklasyfikowani
$query="SELECT count(items) as incomplete FROM (
SELECT season, group_concat(distinct id_drivers_gp) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_completed=0
GROUP BY drivers_gp_results.id_drivers_gp,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["incomplete"] = $r["incomplete"];
}

//ukonczone
$query="SELECT count(items) as finished FROM (
SELECT season, group_concat(distinct id_drivers_gp) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_finished=1
GROUP BY drivers_gp_results.id_drivers_gp,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["finished"] = $r["finished"];
}

//nieukonczone
$query="SELECT count(items) as retirement FROM (
SELECT season, group_concat(distinct id_drivers_gp) as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_finished=0 AND drivers_gp_results.disq=0
GROUP BY drivers_gp_results.id_drivers_gp,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["retirement"] = $r["retirement"];
}

//dyskwalifikacje
$query="SELECT name, count(items) as disq FROM (
SELECT season, name, drivers_gp_results.id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND disq=1
ORDER BY drivers_gp_results.race_date ASC, name
) as t";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $statsItems["disq"] = $r["disq"];
}

$teamItems["stats"]=$statsItems;

$teamItems["[2] Time - stats"]=microtime(true)-$startTime2;
$startTime3 = microtime(true);

/**
* Ranking - miejsca
**/
$rankingItems;

//sezony
$query="SELECT team, name, count(items) as seasons FROM (
SELECT lower(teams.team) team, teams.name, drivers_gp_involvements.season as items
FROM drivers_gp_involvements,teams 
WHERE drivers_gp_involvements.id_team=teams.id_team
GROUP BY drivers_gp_involvements.season, drivers_gp_involvements.team
ORDER BY drivers_gp_involvements.season, drivers_gp_involvements.team
) as t GROUP BY team ORDER BY seasons desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["seasons"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba gp
$query="SELECT team, name, count(items) as gp FROM (
SELECT lower(teams.team) team, teams.name, id_gp as items
FROM drivers_gp_involvements,teams 
WHERE drivers_gp_involvements.id_team=teams.id_team
GROUP BY drivers_gp_involvements.id_gp, drivers_gp_involvements.season, drivers_gp_involvements.team
ORDER BY drivers_gp_involvements.season, drivers_gp_involvements.id_gp
) as t GROUP BY team ORDER BY gp desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["gp"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba kwalifikacji
$query="SELECT team, name, count(items) as qual FROM (
SELECT lower(teams.team) team, teams.name, qual_date as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
GROUP BY drivers_pp_results.qual_date,drivers_pp_results.team
ORDER BY drivers_pp_results.qual_date ASC
) as t GROUP BY team ORDER BY qual desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["qual"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba wyscigow
$query="SELECT team, name, count(items) as starts FROM (
SELECT lower(teams.team) team, teams.name, race_date as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
GROUP BY drivers_gp_results.race_date,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY starts desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["starts"] = $pos;
    break;
  }
  $pos+=1;
}

//liczba sprintow
$query="SELECT team, name, count(items) as sprints FROM (
SELECT lower(teams.team) team, teams.name, sprint_date as items
FROM drivers_sprint_results,teams 
WHERE drivers_sprint_results.id_team=teams.id_team
GROUP BY drivers_sprint_results.sprint_date,drivers_sprint_results.team
ORDER BY drivers_sprint_results.sprint_date ASC
) as t GROUP BY team ORDER BY sprints desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["sprints"] = $pos;
    break;
  }
  $pos+=1;
}

//zwyciestwa
$query="SELECT team, name, count(items) as wins FROM (
SELECT lower(teams.team) team, teams.name, race_pos as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos=1  
group by drivers_gp_results.race_date, drivers_gp_results.team, drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t GROUP BY team ORDER BY wins desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["wins"] = $pos;
    break;
  }
  $pos+=1;
}

//punkty
$query="SELECT SUM(points) points, lower(teams.team) team, name 
FROM teams_class,teams WHERE teams_class.id_team=teams.id_team 
group by teams.team 
order by 1 desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["points"] = $pos;
    break;
  }
  $pos+=1;
}

//podium
$query="SELECT team, name, count(items) as podium FROM (
SELECT lower(teams.team) team, teams.name, race_pos as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos < 4  
group by drivers_gp_results.race_date, drivers_gp_results.team, drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t GROUP BY team ORDER BY podium desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["podium"] = $pos;
    break;
  }
  $pos+=1;
}

//pole positions
$query="SELECT team, name, count(items) as polepos FROM (
SELECT lower(teams.team) team, teams.name, id_starting_grid as items
FROM drivers_gp_starting_grid,teams 
WHERE drivers_gp_starting_grid.id_team=teams.id_team
AND drivers_gp_starting_grid.is_pp=1  
group by drivers_gp_starting_grid.id_starting_grid,drivers_gp_starting_grid.team,drivers_gp_starting_grid.grid_pos
ORDER BY drivers_gp_starting_grid.season ASC, drivers_gp_starting_grid.grid_pos ASC
) as t GROUP BY team ORDER BY polepos desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["polepos"] = $pos;
    break;
  }
  $pos+=1;
}

//najlepsze okrazenia
$query="SELECT team, name, count(items) as bestlaps FROM (
SELECT lower(teams.team) team, teams.name, race_best_lap as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_best_lap=1
group by drivers_gp_results.race_date,drivers_gp_results.team,drivers_gp_results.race_best_lap
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_best_lap ASC
) as t GROUP BY team ORDER BY bestlaps desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["bestlaps"] = $pos;
    break;
  }
  $pos+=1;
}

//z punktami
$query="SELECT team, name, count(items) as pointPlaces FROM (
SELECT lower(teams.team) team, teams.name, drivers_gp_results.id_drivers_gp as items
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_results.id_team=teams.id_team
AND (drivers_gp_results.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND drivers_gp_results.season>1957
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY pointPlaces desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["pointPlaces"] = $pos;
    break;
  }
  $pos+=1;
}

//bez punktow
$query="SELECT team, name, count(items) as noPointPlaces FROM (
SELECT lower(teams.team) team, teams.name, drivers_gp_results.id_drivers_gp as items
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_results.id_team=teams.id_team
AND (drivers_gp_results.race_team_points=0 AND (drivers_sprint_results.sprint_points=0 OR drivers_sprint_results.sprint_points IS NULL)) AND drivers_gp_results.season>1957
AND drivers_gp_results.race_date NOT IN (SELECT DISTINCT d.race_date FROM drivers_gp_involvements LEFT JOIN drivers_gp_results d ON d.id_drivers_gp=drivers_gp_involvements.id_drivers_gp LEFT JOIN drivers_sprint_results s ON s.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint WHERE (d.race_team_points>0 OR drivers_sprint_results.sprint_points>0) AND d.team=drivers_gp_results.team)
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY noPointPlaces desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["noPointPlaces"] = $pos;
    break;
  }
  $pos+=1;
}

//z kara
$query="SELECT team, name, count(items) as penalty FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND penalty=1
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY penalty desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["penalty"] = $pos;
    break;
  }
  $pos+=1;
}

//sklasyfikowany
$query="SELECT team, name, count(items) as completed FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND race_completed=1
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY completed desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["completed"] = $pos;
    break;
  }
  $pos+=1;
}

//niesklasyfikowny
$query="SELECT team, name, count(items) as incomplete FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND race_completed=0
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY incomplete desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["incomplete"] = $pos;
    break;
  }
  $pos+=1;
}

//wyscigi ukonczone
$query="SELECT team, name, count(items) as finished FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND race_finished=1
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY finished desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["finished"] = $pos;
    break;
  }
  $pos+=1;
}

//wys nieukonczone
$query="SELECT team, name, count(items) as retirement FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND race_finished=0 AND drivers_gp_results.disq=0
GROUP BY drivers_gp_results.id_drivers_gp,drivers_gp_results.team
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY retirement desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["retirement"] = $pos;
    break;
  }
  $pos+=1;
}

//niezakwalifikowany
$query="SELECT team, name, count(items) as notQualified FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_pp as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND not_qualified=1
GROUP BY drivers_pp_results.id_drivers_pp,drivers_pp_results.team
ORDER BY drivers_pp_results.qual_date ASC
) as t GROUP BY team ORDER BY notQualified desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["notQualified"] = $pos;
    break;
  }
  $pos+=1;
}

//nie wystartowal
$query="SELECT team, name, count(items) as notStarted FROM (
SELECT lower(teams.team) team, teams.name, id_drivers_pp as items
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND not_started=1
GROUP BY drivers_pp_results.id_drivers_pp,drivers_pp_results.team
ORDER BY drivers_pp_results.qual_date ASC
) as t GROUP BY team ORDER BY notStarted desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["notStarted"] = $pos;
    break;
  }
  $pos+=1;
}

//dyskwalifikacje
$query="SELECT team, name, count(items) as disq FROM (
SELECT lower(teams.team) team, teams.name, drivers_gp_results.id_drivers_gp as items
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND disq=1
ORDER BY drivers_gp_results.race_date ASC
) as t GROUP BY team ORDER BY disq desc, name";
$result = mysqli_query($dbhandle,$query);
$pos=1;
while($r = mysqli_fetch_assoc($result)) {
  if ($r["team"] === $id){
    $rankingItems["disq"] = $pos;
    break;
  }
  $pos+=1;
}

$teamItems["ranking"]=$rankingItems;

$teamItems["[3] Time - ranking"]=microtime(true)-$startTime3;
$startTime4 = microtime(true);

/**
* Kariera
**/
$careerItems;

//sezony
$query="SELECT MAX(season) maxYear,MIN(season) minYear  FROM drivers_gp_involvements,teams WHERE drivers_gp_involvements.id_team=teams.id_team and teams.team='$id'";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["seasons"] = $r["minYear"]." - ".$r["maxYear"];
}

//najlepszy wynik
$query="SELECT count(place) amount, MIN(place) as bestResult, GROUP_CONCAT(DISTINCT season ORDER BY season DESC SEPARATOR ', ') seasons,
MAX(season) bestResultSeason
FROM teams_class where teams_class.team='$id' and season>1957 and season<=$currentYear and is_classified=1 group by place LIMIT 1";
$resultM = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($resultM)) {
  if ($r["bestResult"] === '1'){
    // $careerItems["bestResult"] = "MŚ (".$r["amount"]."x)";
    $careerItems["bestResult"] = "MŚ";
    $careerItems["champCount"] = $r["amount"];
  }else {
    $careerItems["bestResult"] = $r["bestResult"].".";
    $careerItems["bestResultSeason"] = $r["bestResultSeason"];
  }
}

// silniki wg lat
$query="SELECT distinct group_concat(distinct coalesce(teams.engine,teams.name) SEPARATOR ', ') engine, drivers_gp_involvements.season year
FROM drivers_gp_involvements,teams,drivers WHERE drivers_gp_involvements.id_team=teams.id_team
AND drivers_gp_involvements.id_driver=drivers.id_driver AND teams.team='$id' GROUP BY year ORDER BY year desc";
$result = mysqli_query($dbhandle,$query);
$enginesByYear = array();
while($r = mysqli_fetch_assoc($result)) {
  $enginesByYear[$r["year"]] = $r["engine"];
}

//pierwsza grand prix
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstGP"] = $r;
}

//ostatnia grand prix
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' ORDER BY race_date DESC LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastGP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastGP"] = $r;
}

//pierwsze zwyciestwo
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_pos=1 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstWin"] = $r;
}

//ostatnie zwyciestwo
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_pos=1 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastWin"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastWin"] = $r;
}

//pierwsze podium
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_pos<4 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstPodium"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstPodium"] = $r;
}

//ostatnie podium
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_pos<4 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastPodium"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastPodium"] = $r;
}

//pierwsze kwalifikacje
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,qual_date,SUBSTRING(qual_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_pp_results,gp,teams,drivers WHERE drivers_pp_results.id_gp=gp.id_gp AND drivers_pp_results.id_team=teams.id_team AND drivers_pp_results.id_driver=drivers.id_driver AND teams.team='$id' ORDER BY qual_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstQual"] = $r;
}

//ostatnie kwalifikacje
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,qual_date,SUBSTRING(qual_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_pp_results,gp,teams,drivers WHERE drivers_pp_results.id_gp=gp.id_gp AND drivers_pp_results.id_team=teams.id_team AND drivers_pp_results.id_driver=drivers.id_driver AND teams.team='$id' ORDER BY qual_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastQual"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastQual"] = $r;
}

//pierwsze pole position
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,drivers_gp_starting_grid.season year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_starting_grid,gp,teams,drivers WHERE drivers_gp_starting_grid.id_gp=gp.id_gp AND drivers_gp_starting_grid.id_team=teams.id_team AND drivers_gp_starting_grid.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_starting_grid.is_pp=1 ORDER BY drivers_gp_starting_grid.season LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstPP"] = $r;
}

//ostatnie pole position
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,drivers_gp_starting_grid.season year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_starting_grid,gp,teams,drivers WHERE drivers_gp_starting_grid.id_gp=gp.id_gp AND drivers_gp_starting_grid.id_team=teams.id_team AND drivers_gp_starting_grid.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_starting_grid.is_pp=1 ORDER BY drivers_gp_starting_grid.season desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastPP"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastPP"] = $r;
}

//pierwsze najlepsze okrazenie
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_best_lap=1 ORDER BY race_date LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["firstBL"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["firstBL"] = $r;
}

//ostatnie najlepsze okrazenie
if ($lang=='pl') {
  $query="SELECT gp.name gp";
}else{
  $query="SELECT gp.name_en as gp";
}
$query.=",gp.name_alias alias, gp.id_gp id,race_date,SUBSTRING(race_date,1,4) year,drivers.alias driverAlias,CONCAT(drivers.name,' ',drivers.surname) driver FROM drivers_gp_results,gp,teams,drivers WHERE drivers_gp_results.id_gp=gp.id_gp AND drivers_gp_results.id_team=teams.id_team AND drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' AND drivers_gp_results.race_best_lap=1 ORDER BY race_date desc LIMIT 1";
$result = mysqli_query($dbhandle,$query);
$careerItems["lastBL"]="";
while($r = mysqli_fetch_assoc($result)) {
  $careerItems["lastBL"] = $r;
}

// miejsca w wyscigach - wg sezonow
$query="SELECT season name, MIN(place) y, points_class FROM teams_class WHERE teams_class.team='$id' AND season>1957 AND is_classified=1 GROUP BY season  ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPlacesBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesBySeasons[] = $r;
}
$careerItems["gpPlacesBySeasons"] = $gpPlacesBySeasons;

// punkty w wyscigach - wg sezonow
$query="SELECT season name, SUM(points_class) y FROM teams_class WHERE teams_class.team='$id' AND season>1957  GROUP BY season ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPointsClassBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPointsClassBySeasons[] = $r;
}
$careerItems["gpPointsClassBySeasons"] = $gpPointsClassBySeasons;

// punkty w wyscigach - wg sezonow (wszystkie)
$query="SELECT season name, SUM(points) y, SUM(points) - SUM(points_class) sy FROM teams_class WHERE teams_class.team='$id' AND season>1957  GROUP BY season ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPointsBySeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPointsBySeasons[] = $r;
}
$careerItems["gpPointsBySeasons"] = $gpPointsBySeasons;

// miejsca w wyscigach - wszystkie sezony
$query="SELECT gp.race_pos name,(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos=gp.race_pos AND team='$id') y
FROM drivers_gp_results gp WHERE gp.race_pos<21 AND team='$id' GROUP BY gp.race_pos ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$gpPlacesTotal = array();
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesTotal[] = $r;
}

$query="SELECT '>20' name,(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos>20 AND team='$id') y
FROM drivers_gp_results gp WHERE gp.race_pos=21 AND team='$id' GROUP BY gp.race_pos";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $gpPlacesTotal[] = $r;
}
$careerItems["gpPlacesTotal"] = $gpPlacesTotal;

// miejsca w kwalifikacjach - wszystkie sezony
$query="SELECT pp.grid_pos name,(SELECT COUNT(*) FROM drivers_gp_starting_grid  WHERE grid_pos=pp.grid_pos AND team='$id') y
FROM drivers_gp_starting_grid pp WHERE pp.grid_pos<21 AND team='$id' GROUP BY pp.grid_pos ORDER BY 1";
$result = mysqli_query($dbhandle,$query);
$ppPlacesTotal = array();
while($r = mysqli_fetch_assoc($result)) {
  $ppPlacesTotal[] = $r;
}

$query="SELECT '>20' name,(SELECT COUNT(*) FROM drivers_gp_starting_grid  WHERE grid_pos>20 AND team='$id') y
FROM drivers_gp_starting_grid pp WHERE pp.grid_pos=21 AND team='$id' GROUP BY pp.grid_pos";
$result = mysqli_query($dbhandle,$query);
while($r = mysqli_fetch_assoc($result)) {
  $ppPlacesTotal[] = $r;
}
$careerItems["ppPlacesTotal"] = $ppPlacesTotal;

$teamItems["career"]=$careerItems;

$teamItems["[4] Time - kariera"]=microtime(true)-$startTime4;
$startTime5 = microtime(true);

/**
* Sezony
**/
//gp w sezonach
$query="SELECT season, count(amount) as gp FROM (
SELECT season, group_concat(distinct id_gp) as amount
FROM drivers_gp_involvements 
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.id_gp, drivers_gp_involvements.season, drivers_gp_involvements.team
ORDER BY drivers_gp_involvements.season, drivers_gp_involvements.id_gp
  ) as t group by season";
  $result = mysqli_query($dbhandle,$query);
  $teamsGPTab = array();
  while($r = mysqli_fetch_assoc($result)) {
    $tmp_season = $r["season"];
    $teamsGPTab[$tmp_season] = $r["gp"];
  }
//wyscigi w sezonach
$query="SELECT season, count(amount) as starts FROM (
SELECT season, group_concat(distinct race_date) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_gp_results.race_date,teams.team
ORDER BY drivers_gp_results.race_date ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsStartsTab[$tmp_season] = $r["starts"];
}
//starty w kwalifikacjach sezonach
$query="SELECT season, count(amount) as qual FROM (
SELECT season, group_concat(distinct qual_date) as amount
FROM drivers_pp_results,teams 
WHERE drivers_pp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_pp_results.qual_date,teams.team
ORDER BY drivers_pp_results.qual_date ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsQualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsQualTab[$tmp_season] = $r["qual"];
}
//zwyciestwa w sezonach
$query="SELECT season, count(amount) as wins FROM (
SELECT season, group_concat(distinct drivers_gp_results.race_pos) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=1
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsWinsTab[$tmp_season] = $r["wins"];
}
//drugie miejsca w sezonach
$query="SELECT season, count(amount) as second FROM (
SELECT season, group_concat(distinct drivers_gp_results.race_pos) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=2
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsSecondTab[$tmp_season] = $r["second"];
}
//trzecie miejsca w sezonach
$query="SELECT season, count(amount) as third FROM (
SELECT season, group_concat(distinct drivers_gp_results.race_pos) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=3
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsThirdTab[$tmp_season] = $r["third"];
}
//podium w sezonach
$query="SELECT season, count(amount) as podiums FROM (
SELECT season, group_concat(distinct race_pos) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND drivers_gp_results.race_pos < 4  
AND teams.team='$id'
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_pos
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsPodiumsTab[$tmp_season] = $r["podiums"];
}
//pole position w sezonach
$query="SELECT season, count(amount) as pp FROM (
SELECT season, group_concat(distinct id_starting_grid) as amount
FROM drivers_gp_starting_grid,teams 
WHERE drivers_gp_starting_grid.id_team=teams.id_team
AND drivers_gp_starting_grid.is_pp=1 
AND teams.team='$id'
GROUP BY drivers_gp_starting_grid.id_starting_grid,teams.team,drivers_gp_starting_grid.grid_pos
ORDER BY drivers_gp_starting_grid.season ASC, drivers_gp_starting_grid.grid_pos ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsPolePosTab[$tmp_season] = $r["pp"];
}
//best laps w sezonach
$query="SELECT season, count(amount) as bestlaps FROM (
SELECT season, group_concat(distinct race_best_lap) as amount
FROM drivers_gp_results,teams 
WHERE drivers_gp_results.id_team=teams.id_team
AND race_best_lap=1  
AND teams.team='$id'
GROUP BY drivers_gp_results.race_date,teams.team,drivers_gp_results.race_best_lap
ORDER BY drivers_gp_results.race_date ASC, drivers_gp_results.race_pos ASC
) as t group by season";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsBLTab[$tmp_season] = $r["bestlaps"];
}
//gp z punktami w sezonach
// $query="SELECT SUBSTRING(drivers_gp_results.race_date,1,4) season,count(id_drivers_gp) pointsPlaces from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' AND race_team_points>0 group by 1";
// $result = mysqli_query($dbhandle,$query);
// $teamsPointsPlacesTab = array();
// while($r = mysqli_fetch_assoc($result)) {
//   $tmp_season = $r["season"];
//   $teamsPointsPlacesTab[$tmp_season] = $r["pointsPlaces"];
// }
//ukonczone gp w sezonach
$query="SELECT SUBSTRING(drivers_gp_results.race_date,1,4) season,count(DISTINCT id_drivers_gp) completed from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' AND race_completed=1 group by 1";
$result = mysqli_query($dbhandle,$query);
$teamsCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsCompletedTab[$tmp_season] = $r["completed"];
}
//nieukonczone gp w sezonach
$query="SELECT SUBSTRING(drivers_gp_results.race_date,1,4) season,count(DISTINCT id_drivers_gp) incomplete from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' AND race_completed=0 group by 1";
$result = mysqli_query($dbhandle,$query);
$teamsIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsIncompleteTab[$tmp_season] = $r["incomplete"];
}
//dyskwalifikacje w sezonach
$query="SELECT SUBSTRING(drivers_gp_results.race_date,1,4) season,count(id_drivers_gp) disq from drivers_gp_results,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' AND disq=1 group by 1";
$result = mysqli_query($dbhandle,$query);
$teamsDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $teamsDisqTab[$tmp_season] = $r["disq"];
}

/**
 * Kierowcy w zespole w sezonach
 */
//gp w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, id_gp as amount
FROM drivers_gp_involvements
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.id_driver, drivers_gp_involvements.season, drivers_gp_involvements.id_gp
ORDER BY drivers_gp_involvements.season, drivers_gp_involvements.id_driver,  drivers_gp_involvements.id_gp
 ) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverGPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverGPTab[$r["idx"]] = $r["amount"];
}
//starty w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverStartsTab[$r["idx"]] = $r["amount"];
}
//kwalifikacje w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct qual_date) as amount
FROM drivers_pp_results
WHERE drivers_pp_results.team='$id'
GROUP BY drivers_pp_results.id_driver,drivers_pp_results.qual_date
ORDER BY drivers_pp_results.qual_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverQualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverQualTab[$r["idx"]] = $r["amount"];
}
//p1 w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_pos=1
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverP1Tab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverP1Tab[$r["idx"]] = $r["amount"];
}
//p2 w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_pos=2
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverP2Tab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverP2Tab[$r["idx"]] = $r["amount"];
}
//p3 w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_pos=3
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverP3Tab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverP3Tab[$r["idx"]] = $r["amount"];
}
//podium w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_pos<=3
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverPdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverPdTab[$r["idx"]] = $r["amount"];
}
//pp w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct id_starting_grid) as amount
FROM drivers_gp_starting_grid
WHERE drivers_gp_starting_grid.team='$id'
AND drivers_gp_starting_grid.is_pp=1
GROUP BY drivers_gp_starting_grid.id_driver,drivers_gp_starting_grid.id_starting_grid
ORDER BY drivers_gp_starting_grid.id_starting_grid ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverPpTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverPpTab[$r["idx"]] = $r["amount"];
}
//naj okraz. w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_best_lap=1
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverBlTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverBlTab[$r["idx"]] = $r["amount"];
}
//ukonczone wyscigi w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_completed=1
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverComplTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverComplTab[$r["idx"]] = $r["amount"];
}
//nie ukonczone w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.race_completed=0
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverInComplTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverInComplTab[$r["idx"]] = $r["amount"];
}
//dyskw w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, count(amount) as amount FROM (
SELECT season, id_driver, group_concat(distinct race_date) as amount
FROM drivers_gp_results
WHERE drivers_gp_results.team='$id'
AND drivers_gp_results.disq=1
GROUP BY drivers_gp_results.id_driver,drivers_gp_results.race_date
ORDER BY drivers_gp_results.race_date ASC
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverDsqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverDsqTab[$r["idx"]] = $r["amount"];
}
//punkty w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, amount FROM (
SELECT drivers_gp_involvements.season, drivers_gp_involvements.id_driver, 
COALESCE(SUM(drivers_gp_results.race_points),0) + 
COALESCE(SUM(drivers_sprint_results.sprint_points),0) as amount
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_points>0
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.id_driver,drivers_gp_involvements.season
ORDER BY drivers_gp_involvements.season ASC, drivers_gp_involvements.id_driver
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverPointsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverPointsTab[$r["idx"]] = $r["amount"];
}
//punkty do klasyfikacji w sezonach
$query="SELECT CONCAT(season, '_', id_driver) idx, amount FROM (
SELECT drivers_gp_involvements.season, drivers_gp_involvements.id_driver, 
COALESCE(SUM(drivers_gp_results.race_points),0) + 
COALESCE(SUM(drivers_sprint_results.sprint_points),0) as amount
FROM drivers_gp_involvements
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=drivers_gp_involvements.id_drivers_gp AND drivers_gp_results.race_points>0 AND drivers_gp_results.excluded_from_class=0
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint=drivers_gp_involvements.id_drivers_sprint
WHERE drivers_gp_involvements.team='$id'
GROUP BY drivers_gp_involvements.id_driver,drivers_gp_involvements.season
ORDER BY drivers_gp_involvements.season ASC, drivers_gp_involvements.id_driver
) as t group by season, id_driver";
$result = mysqli_query($dbhandle,$query);
$seasonDriverPointsClassTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $seasonDriverPointsClassTab[$r["idx"]] = $r["amount"];
}

$teamItems["[5] Time - statystyki w sezonach"]=microtime(true)-$startTime5;
$startTime6 = microtime(true);

//bolidy w sezonach
$query="SELECT DISTINCT drivers_gp_involvements.season,
lower(teams.team) teams,
GROUP_CONCAT(DISTINCT COALESCE(teams.engine,teams.name) SEPARATOR ', ') as engines,
model teamsModelNames,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ')  teamsNames,
teams_models.id_team_model idTeamModel    
from drivers_gp_involvements,teams,teams_models 
where teams_models.team='$id' 
and drivers_gp_involvements.id_team=teams.id_team
and teams_models.id_team_model=drivers_gp_involvements.id_team_model
group by model,season 
order by 1,3";
$result = mysqli_query($dbhandle,$query);
$teamsCarModelsTab = array();
$teamsCarModelsSubTab = array();
$currSeason = "";
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  if ($currSeason=="") {
    $currSeason==$tmp_season;
  }
  
  $item = array();    
  $item["team"] = $r["teams"];
  $item["model"] = $r["teamsModelNames"];
  $item["teamsNames"] = $r["teamsNames"];
  $item["engines"] = $r["engines"];
  $item["idTeamModel"] = $r["idTeamModel"];
  if ($currSeason==$tmp_season) {
    $teamsCarModelsSubTab[]=$item;
  }else{
    $currSeason=$tmp_season;
    $teamsCarModelsSubTab = array();
    $teamsCarModelsSubTab[] = $item;
  }
  $teamsCarModelsTab[$tmp_season] = $teamsCarModelsSubTab;  
}

//bolidy we wszystkich sezonach
$query="SELECT DISTINCT drivers_gp_involvements.season,
lower(teams.team) team,
teams.alias_name alias,
teams.name,
teams.engine,
teams_models.model,    
teams_models.id_team_model idTeamModel,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamModelName,
lower(teams.country_code) countryCode,
count(DISTINCT drivers_gp_involvements.id_gp) starts
from drivers_gp_involvements,teams,teams_models 
where drivers_gp_involvements.team='$id' 
and drivers_gp_involvements.id_team=teams.id_team
and teams_models.id_team_model=drivers_gp_involvements.id_team_model 
GROUP BY drivers_gp_involvements.season, teams_models.model
order by 1,3";
$result = mysqli_query($dbhandle,$query);
$teamCarModelsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $teamCarModelsTab[] = $r;
}

//silniki we wszystkich sezonach
$query="SELECT DISTINCT drivers_gp_involvements.season,
lower(teams.team) team,
teams.alias_name alias,
teams.name,
COALESCE(teams.engine,teams.name) engine,
lower(teams.country_code) countryCode,
count(DISTINCT drivers_gp_involvements.id_gp) starts
from drivers_gp_involvements,teams
where drivers_gp_involvements.team='$id' 
and drivers_gp_involvements.id_team=teams.id_team
GROUP BY drivers_gp_involvements.season, teams.engine
order by 1,3";
$result = mysqli_query($dbhandle,$query);
$teamEnginesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $teamEnginesTab[] = $r;
}

$teamItems["[6] Time - bolidy i silniki w sezonach"]=microtime(true)-$startTime6;
$startTime7 = microtime(true);

// kierowcy we wszystkich sezonach
$query="SELECT drivers_gp_involvements.season, 
drivers.id_driver id,
drivers.name,
drivers.surname, 
drivers.alias,
drivers.country_code countryCode,";
if ($lang=='pl') {
  $query.="(SELECT COALESCE(team_name,'Prywatnie') FROM teams_models WHERE teams_models.id_team_model=drivers_gp_involvements.id_team_model) teamName,";
}else{
  $query.="(SELECT COALESCE(team_name,'Private') FROM teams_models WHERE teams_models.id_team_model=drivers_gp_involvements.id_team_model) teamName,";
}
$query.="(SELECT place FROM drivers_class WHERE drivers_class.id_driver=drivers_gp_involvements.id_driver AND drivers_class.season=drivers_gp_involvements.season AND drivers_class.is_classified=1) place,
COUNT(drivers_gp_involvements.id_drivers_gp) starts
FROM drivers_gp_involvements
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
WHERE drivers_gp_involvements.team='$id' 
GROUP BY drivers_gp_involvements.season, drivers_gp_involvements.id_driver
ORDER BY drivers_gp_involvements.season, ISNULL(place) ASC,place ASC, 9 DESC, drivers.surname";
$result = mysqli_query($dbhandle,$query);
$teamDriversTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $teamDriversTab[] = $r;
}

$teamItems["[7.1] Time - kierowcy we wszystkich sezonach"]=microtime(true)-$startTime7;
$startTime7_1 = microtime(true);

// miejsca w gp we wszystkich sezonach
$query="SELECT
gp_season.season,
gp.id_gp,
gp.country_code name,
LOWER(gp.name_short) nameShort,
gp.name_alias alias,";
if ($lang=='pl') {
  $query.="gp.name gp";
}else{
  $query.="gp.name_en as gp";
}
$query.=",gp.circuit,gp.circuit_alias circuitAlias,
SUBSTRING(gp_season.date,1,10) raceDate,
SUM(COALESCE(drivers_gp_results.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
SUM(COALESCE(dgr.race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) classPoints,
-- (SELECT SUM(COALESCE(race_team_points,0)) + SUM(COALESCE(drivers_sprint_results.sprint_points,0)) FROM drivers_gp_results WHERE drivers_gp_results.team=drivers_gp_involvements.team
-- AND drivers_gp_results.season=gp_season.season and drivers_gp_results.id_gp=gp_season.id_gp  and drivers_gp_results.excluded_from_team_class=0) classPoints,
CASE WHEN GROUP_CONCAT(drivers_gp_involvements.id_drivers_gp) IS NOT NULL THEN 1 ELSE 0 END as raceStarted,
CASE WHEN GROUP_CONCAT(drivers_gp_involvements.id_drivers_pp) IS NOT NULL THEN 1 ELSE 0 END as qualStarted,
CASE WHEN GROUP_CONCAT(drivers_gp_involvements.id_drivers_sprint) IS NOT NULL THEN 1 ELSE 0 END as sprintStarted
FROM gp_season
LEFT JOIN gp ON gp.id_gp = gp_season.id_gp
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_gp=gp_season.id_gp AND drivers_gp_involvements.season = gp_season.season
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_gp_results dgr ON dgr.id_drivers_gp = drivers_gp_involvements.id_drivers_gp AND dgr.excluded_from_team_class=0
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model and teams_models.season=gp_season.season
LEFT JOIN teams ON teams.id_team=drivers_gp_involvements.id_team
WHERE drivers_gp_involvements.team='$id'
GROUP BY gp_season.season, gp.id_gp
ORDER BY gp_season.season, gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$teamGpResultsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $teamGpResultsTab[] = $r;
}

$teamItems["[7.2] Time - miejsca w gp we wszystkich sezonach"]=microtime(true)-$startTime7_1;
$startTime7_2 = microtime(true);


// sezony
$query="SELECT drivers_gp_involvements.season, teams.alias_name alias,teams.team id, CONCAT(teams.name,' ',COALESCE(teams.engine,'')) name,"; 
if ($lang=='pl') {
  $query.="GROUP_CONCAT(DISTINCT COALESCE(team_name, 'Prywatnie') SEPARATOR ', ') fullname,";
}else{
  $query.="GROUP_CONCAT(DISTINCT COALESCE(team_name, 'Private') SEPARATOR ', ') fullname,";
}
$query.="teams.country_code country,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' teams, 
COALESCE((SELECT SUM(points) FROM teams_class WHERE team=teams.team AND season=drivers_gp_involvements.season),0) points,
COALESCE((SELECT SUM(points_class) FROM teams_class WHERE team=teams.team AND season=drivers_gp_involvements.season),0) pointsClass,
teams_class.penalty, 
teams_class.info,
CASE WHEN teams_class.is_classified=0 THEN 'NK' ELSE teams_class.place END place
FROM drivers_gp_involvements
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN teams_class ON teams_class.team = teams.team AND teams_class.season = drivers_gp_involvements.season
LEFT JOIN teams_models ON teams_models.team=teams.team AND teams_models.season=drivers_gp_involvements.season AND teams_models.id_team_model=drivers_gp_involvements.id_team_model
WHERE teams.team = '$id' 
GROUP BY 1  ORDER BY 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamSeasonsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  $r["gp"]=!isset($teamsGPTab[$r["season"]]) ? 0 : $teamsGPTab[$r["season"]];
  $r["starts"]=!isset($teamsStartsTab[$r["season"]]) ? 0 : $teamsStartsTab[$r["season"]];
  $r["qual"]=!isset($teamsQualTab[$r["season"]]) ? 0 : $teamsQualTab[$r["season"]];
  $r["wins"]=!isset($teamsWinsTab[$r["season"]]) ? 0 : $teamsWinsTab[$r["season"]];
  $r["second"]=!isset($teamsSecondTab[$r["season"]]) ? 0 : $teamsSecondTab[$r["season"]];
  $r["third"]=!isset($teamsThirdTab[$r["season"]]) ? 0 : $teamsThirdTab[$r["season"]];
  $r["podium"]=!isset($teamsPodiumsTab[$r["season"]]) ? 0 : $teamsPodiumsTab[$r["season"]];
  $r["polepos"]=!isset($teamsPolePosTab[$r["season"]]) ? 0 : $teamsPolePosTab[$r["season"]];
  $r["bestlaps"]=!isset($teamsBLTab[$r["season"]]) ? 0 : $teamsBLTab[$r["season"]];
  $r["pointsPlaces"]=!isset($teamsPointsPlacesTab[$r["season"]]) ? 0 : $teamsPointsPlacesTab[$r["season"]];
  $r["completed"]=!isset($teamsCompletedTab[$r["season"]]) ? 0 : $teamsCompletedTab[$r["season"]];
  $r["incomplete"]=!isset($teamsIncompleteTab[$r["season"]]) ? 0 : $teamsIncompleteTab[$r["season"]];
  $r["disq"]=!isset($teamsDisqTab[$r["season"]]) ? 0 : $teamsDisqTab[$r["season"]];
  $r["engine"]=$enginesByYear[$r["season"]];

  // kierowcy w sezonie
  $drivers = array();
  foreach ($teamDriversTab as $driver) {
    if ($driver["season"]!=$tmp_season){
      continue;
    }
    array_push($drivers, $driver);
  }
  $r["drivers"]=$drivers;

  //bolidy w sezonie
  $carModels = array();
  foreach ($teamCarModelsTab as $carModel) {
    if ($carModel["season"]!=$tmp_season){
      continue;
    }
    array_push($carModels, $carModel);
  }
  $r["carModels"]=$carModels;

  //silniki w sezonie
  $engines = array();
  foreach ($teamEnginesTab as $engine) {
    if ($engine["season"]!=$tmp_season){
      continue;
    }
    array_push($engines, $engine);
  }
  $r["engines"]=$engines;

  // wyniki gp w sezonie
  $gpResults = array();
  foreach ($teamGpResultsTab as $gpResult) {
    if ($gpResult["season"]!=$tmp_season){
      continue;
    }
    array_push($gpResults, $gpResult);
  }
  $r["gpResults"]=$gpResults;

  $teamSeasonsItems[] = $r;
}
$teamItems["seasons"]=$teamSeasonsItems;

$teamItems["[7] Time - sezony"]=microtime(true)-$startTime7_2;
$startTime8 = microtime(true);

/**
 * Wybrany sezon
 */
//miejsca w gp w sezonach
$query="SELECT gp.season, gp.race_pos name FROM drivers_gp_results gp WHERE gp.season=$selectedSeason GROUP BY gp.season, gp.race_pos ORDER BY 1,2";
$result = mysqli_query($dbhandle,$query);
$racePlacesInSeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $racePlacesInSeasons[] = $r;
}

//miejsca kierowcow w gp w wybranym sezonie
$query="SELECT CONCAT(gp.id_driver,'_',gp.season,'_',gp.race_pos) idx,gp.id_driver,gp.season,gp.race_pos,
(SELECT COUNT(*) FROM drivers_gp_results WHERE race_completed=1 AND race_pos=gp.race_pos AND team='$id' AND id_driver=gp.id_driver AND season=gp.season) y 
FROM drivers_gp_results gp WHERE gp.season=$selectedSeason AND gp.team='$id'
GROUP BY gp.season, gp.id_driver, gp.race_pos ORDER BY 1,2";
$result = mysqli_query($dbhandle,$query);
$driversGpPlacesInSeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $driversGpPlacesInSeasons[$r["idx"]] = $r["y"];
}

//pola startowe kierowcow w wybranym sezonie
$query="SELECT CONCAT(gp.id_driver,'_',gp.season,'_',gp.grid_pos) idx,gp.id_driver,gp.season,gp.grid_pos,
(SELECT COUNT(*) FROM drivers_gp_starting_grid WHERE grid_pos=gp.grid_pos AND team='$id' AND id_driver=gp.id_driver AND season=gp.season) y 
FROM drivers_gp_starting_grid gp WHERE gp.season=$selectedSeason AND gp.team='$id'
GROUP BY gp.season, gp.id_driver, gp.grid_pos ORDER BY 1,2";
$result = mysqli_query($dbhandle,$query);
$driversPpPlacesInSeasons = array();
while($r = mysqli_fetch_assoc($result)) {
  $driversPpPlacesInSeasons[$r["idx"]] = $r["y"];
}

//kierowcy w wybranym sezonie
$query="SELECT drivers_gp_involvements.season, 
drivers.id_driver id,
drivers.name,
drivers.surname, 
drivers.alias,
drivers.country_code countryCode,";
if ($lang=='pl') {
  $query.="(SELECT COALESCE(team_name,'Prywatnie') FROM teams_models WHERE teams_models.id_team_model=drivers_gp_involvements.id_team_model) teamName,";
}else{
  $query.="(SELECT COALESCE(team_name,'Private') FROM teams_models WHERE teams_models.id_team_model=drivers_gp_involvements.id_team_model) teamName,";
}
$query.="(SELECT place FROM drivers_class WHERE drivers_class.id_driver=drivers_gp_involvements.id_driver AND drivers_class.season=drivers_gp_involvements.season AND drivers_class.is_classified=1) place,
COUNT(drivers_gp_involvements.id_drivers_gp) starts
FROM drivers_gp_involvements
LEFT JOIN drivers ON drivers.id_driver=drivers_gp_involvements.id_driver
WHERE drivers_gp_involvements.team='$id' AND drivers_gp_involvements.season=$selectedSeason
GROUP BY drivers_gp_involvements.season, drivers_gp_involvements.id_driver
ORDER BY drivers_gp_involvements.season, ISNULL(place) ASC,place ASC, 9 DESC, drivers.surname";
$result = mysqli_query($dbhandle,$query);
$teamsDriversTab = array();
$teamsDriversSubTab = array();
$currSeason = "";
while($r = mysqli_fetch_assoc($result)) {
  $tmp_season = $r["season"];
  if ($currSeason=="") {
    $currSeason==$tmp_season;
  }
  $tmp_idx = $r["season"]."_".$r["id"];
  $tmp_id_driver = $r["id"];

  $item = array();    
  $item["id"] = $r["id"];
  $item["name"] = $r["name"];
  $item["surname"] = $r["surname"];
  $item["alias"] = $r["alias"];
  $item["countryCode"] = $r["countryCode"];
  $item["teamName"] = $r["teamName"];
  $item["gp"] = !isset($seasonDriverGPTab[$tmp_idx]) ? 0 : $seasonDriverGPTab[$tmp_idx]; 
  $item["starts"] = !isset($seasonDriverStartsTab[$tmp_idx]) ? 0 : $seasonDriverStartsTab[$tmp_idx]; 
  $item["qual"] = !isset($seasonDriverQualTab[$tmp_idx]) ? 0 : $seasonDriverQualTab[$tmp_idx]; 
  $item["p1"] = !isset($seasonDriverP1Tab[$tmp_idx]) ? 0 : $seasonDriverP1Tab[$tmp_idx]; 
  $item["p2"] = !isset($seasonDriverP2Tab[$tmp_idx]) ? 0 : $seasonDriverP2Tab[$tmp_idx]; 
  $item["p3"] = !isset($seasonDriverP3Tab[$tmp_idx]) ? 0 : $seasonDriverP3Tab[$tmp_idx]; 
  $item["pd"] = !isset($seasonDriverPdTab[$tmp_idx]) ? 0 : $seasonDriverPdTab[$tmp_idx]; 
  $item["pp"] = !isset($seasonDriverPpTab[$tmp_idx]) ? 0 : $seasonDriverPpTab[$tmp_idx]; 
  $item["bl"] = !isset($seasonDriverBlTab[$tmp_idx]) ? 0 : $seasonDriverBlTab[$tmp_idx]; 
  $item["compl"] = !isset($seasonDriverComplTab[$tmp_idx]) ? 0 : $seasonDriverComplTab[$tmp_idx]; 
  $item["incompl"] = !isset($seasonDriverInComplTab[$tmp_idx]) ? 0 : $seasonDriverInComplTab[$tmp_idx]; 
  $item["dsq"] = !isset($seasonDriverDsqTab[$tmp_idx]) ? 0 : $seasonDriverDsqTab[$tmp_idx]; 
  $item["points"] = $seasonDriverPointsTab[$tmp_idx]=="0" ? 0 : $seasonDriverPointsTab[$tmp_idx]; 
  $item["pointsClass"] = $seasonDriverPointsClassTab[$tmp_idx]=="0" ? 0 : $seasonDriverPointsClassTab[$tmp_idx];
  $item["place"] = $r["place"];

  // miejsca kierowcy w gp 
  $query2="SELECT
  gp.country_code name,
  LOWER(gp.name_short) nameShort,
  gp.name_alias alias,";
  if ($lang=='pl') {
    $query2.="gp.name gp";
  }else{
    $query2.="gp.name_en as gp";
  }
  $query2.=",SUBSTRING(gp_season.date,1,10) raceDate,
  gp_season.sprint,
  race_pos place,
  race_completed completed,
  race_time time,
  race_best_lap bestLap,
  race_laps laps,
  disq,";
  if ($lang=='pl') {
    $query2.="drivers_gp_results.race_add_info info,
    drivers_pp_results.qual_add_info qualInfo,
    drivers_gp_starting_grid.grid_add_info gridInfo,";
  }else{
    $query2.="drivers_gp_results.race_add_info_en info,
    drivers_pp_results.qual_add_info_en qualInfo,
    drivers_gp_starting_grid.grid_add_info_en gridInfo,";
  }
  $query2.="excluded_from_class excluded,
  race_points racePoints,
  drivers_sprint_results.sprint_points sprintPoints,
  drivers_sprint_results.sprint_pos sprintPos,
  drivers_pp_results.qual_pos qual,
  drivers_pp_results.not_qualified notQualified,
  drivers_pp_results.not_started notStarted,
  drivers_gp_starting_grid.grid_pos grid,
  SUM(COALESCE(race_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
  COALESCE((SELECT SUM(COALESCE(race_points, 0) + COALESCE(drivers_sprint_results.sprint_points,0))
  FROM drivers_gp_involvements di
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=di.id_drivers_gp AND drivers_gp_results.excluded_from_class = 0
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = di.id_drivers_sprint
  WHERE di.id_driver = drivers_gp_involvements.id_driver AND di.season = gp_season.season AND di.id_gp = gp_season.id_gp ),0) classPoints,
  lower(drivers_gp_involvements.team) team,
  teams_models.model modelName,
  teams_models.team_name teamName
  FROM
      gp_season
  LEFT JOIN gp ON gp.id_gp = gp_season.id_gp
  LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_gp=gp_season.id_gp 
  AND drivers_gp_involvements.id_driver = $tmp_id_driver AND drivers_gp_involvements.team='$id'   
  AND drivers_gp_involvements.season = gp_season.season
  LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
  LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
  LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
  LEFT JOIN drivers_gp_starting_grid ON drivers_gp_starting_grid.id_gp = drivers_gp_involvements.id_gp AND drivers_gp_starting_grid.id_driver = drivers_gp_involvements.id_driver AND drivers_gp_starting_grid.season =gp_season.season
  LEFT JOIN teams_models ON teams_models.id_team_model=drivers_gp_involvements.id_team_model and teams_models.season=gp_season.season
  WHERE
      gp_season.season = $tmp_season
  GROUP BY
      gp.id_gp
  ORDER BY
      gp_season.sort,
      race_pos";
  $result2 = mysqli_query($dbhandle,$query2);
  
  $gpResults=array();
  while($r2 = mysqli_fetch_assoc($result2)) {
    if($r2["completed"]=='0') {
        if ($r2["time"]=='DNQ') $r2["place"]="NQ";
        else {
            if ($r2["disq"] === 0){
              $r2["place"]="-";
            }else{
              $r2["place"]="DS";
            }
        }
        if ($r2["time"]=='DNS') $r2["place"]="";
    }else if($r2["completed"]=="1") {
      $r2["place"]=$r2["place"];
    }else{
      $r2["place"]="";
    }
    $gpResults[] = $r2;
  }
  $item["gpResults"] = $gpResults;

  // miejsca w wyscigach
  $gpPlacesBySeason = array();
  foreach ($racePlacesInSeasons as $racePlacesInSeason) {
    if ($racePlacesInSeason["season"]!=$tmp_season){
      continue;
    }
    $placeCount = isset($driversGpPlacesInSeasons[$r["id"]."_".$racePlacesInSeason["season"]."_".$racePlacesInSeason["name"]]) ? $driversGpPlacesInSeasons[$r["id"]."_".$racePlacesInSeason["season"]."_".$racePlacesInSeason["name"]] : 0;
    $object = new stdClass();
    $object->name = $racePlacesInSeason["name"];
    $object->season = $racePlacesInSeason["season"];
    $object->y = $placeCount == null ? 0 : $placeCount;
    $gpPlacesBySeason[] = $object;
  }
  $item["gpPlaces"] = $gpPlacesBySeason;

  // miejsca w kwalifikacjach
  $ppPlacesBySeason = array();
  foreach ($racePlacesInSeasons as $racePlacesInSeason) {
    if ($racePlacesInSeason["season"]!=$tmp_season){
      continue;
    }
    $placeCount = isset($driversPpPlacesInSeasons[$r["id"]."_".$racePlacesInSeason["season"]."_".$racePlacesInSeason["name"]]) ? $driversPpPlacesInSeasons[$r["id"]."_".$racePlacesInSeason["season"]."_".$racePlacesInSeason["name"]] : 0;
    $object = new stdClass();
    $object->name = $racePlacesInSeason["name"];
    $object->season = $racePlacesInSeason["season"];
    $object->y = $placeCount == null ? 0 : $placeCount;
    $ppPlacesBySeason[] = $object;
  }
  $item["ppPlaces"] = $ppPlacesBySeason;

  if ($currSeason==$tmp_season) {
    $teamsDriversSubTab[]=$item;
  }else{
    $currSeason=$tmp_season;
    $teamsDriversSubTab = array();
    $teamsDriversSubTab[] = $item;
  }
  $teamsDriversTab[$tmp_season] = $teamsDriversSubTab;  
}

// punkty w wybranym sezonie wg gp 
$query="SELECT
gp_season.season,
gp.country_code name,
LOWER(gp.name_short) nameShort,
gp.name_alias alias,";
if ($lang=='pl') {
  $query.="gp.name gp";
}else{
  $query.="gp.name_en as gp";
}
$query.=",SUBSTRING(gp_season.date,1,10) raceDate,
gp_season.sprint,
(SELECT COUNT(race_best_lap) FROM drivers_gp_results d WHERE d.team='$id' AND d.season=gp_season.season AND d.id_gp=gp_season.id_gp AND d.race_best_lap=1) bestLap,
SUM(COALESCE(race_team_points,0)) racePoints,
SUM(COALESCE(drivers_sprint_results.sprint_points,0)) sprintPoints,
SUM(COALESCE(race_team_points,0) + COALESCE(drivers_sprint_results.sprint_points,0)) points,
COALESCE((SELECT SUM(COALESCE(race_team_points, 0) + COALESCE(drivers_sprint_results.sprint_points,0))
FROM drivers_gp_involvements di
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp=di.id_drivers_gp AND drivers_gp_results.excluded_from_team_class = 0
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = di.id_drivers_sprint
WHERE di.team = drivers_gp_involvements.team AND di.season = gp_season.season AND di.id_gp = gp_season.id_gp ),0) classPoints
FROM gp_season
LEFT JOIN gp ON gp.id_gp = gp_season.id_gp
LEFT JOIN drivers_gp_involvements ON drivers_gp_involvements.id_gp=gp_season.id_gp 
AND drivers_gp_involvements.team = '$id'
AND drivers_gp_involvements.season = gp_season.season
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
WHERE gp_season.season=$selectedSeason
GROUP BY gp_season.season, gp_season.id_gp
ORDER BY gp_season.season, gp_season.sort";
$result = mysqli_query($dbhandle,$query);
$gpTeamResultsBySeasons=array();
while($r = mysqli_fetch_assoc($result)) {
  $gpTeamResultsBySeasons[] = $r;
}

// szczegoly wybranego sezonu
$query="SELECT gp.season, teams.alias_name alias,teams.team id, teams.name name,";
if ($lang=='pl') {
  $query.="(SELECT GROUP_CONCAT(DISTINCT COALESCE(team_name, 'Prywatnie') SEPARATOR ', ') FROM teams_models WHERE teams_models.team = teams.team AND teams_models.season = gp.season) fullname,";
}else{
  $query.="(SELECT GROUP_CONCAT(DISTINCT COALESCE(team_name, 'Private') SEPARATOR ', ') FROM teams_models WHERE teams_models.team = teams.team AND teams_models.season = gp.season) fullname,";
}
$query.="teams.country_code country,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' teams, 
COALESCE((SELECT place FROM teams_class WHERE id_team=gp.id_team AND season=gp.season AND teams_class.is_classified=1),'NK') place,
COALESCE((SELECT SUM(points) FROM teams_class WHERE team=teams.team AND season=gp.season),0) points,
COALESCE((SELECT SUM(points_class) FROM teams_class WHERE team=teams.team AND season=gp.season),0) pointsClass,
teams_class.penalty, 
teams_class.info
FROM drivers_gp_involvements gp
LEFT JOIN teams ON teams.id_team = gp.id_team
LEFT JOIN teams_class ON teams_class.team = teams.team AND teams_class.season = gp.season
WHERE teams.team = '$id' AND gp.season=$selectedSeason
GROUP BY 1 ORDER BY 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamSeasonDetailsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($teamsGPTab[$r["season"]]) ? 0 : $teamsGPTab[$r["season"]];
  $r["starts"]=!isset($teamsStartsTab[$r["season"]]) ? 0 : $teamsStartsTab[$r["season"]];
  $r["qual"]=!isset($teamsQualTab[$r["season"]]) ? 0 : $teamsQualTab[$r["season"]];
  $r["wins"]=!isset($teamsWinsTab[$r["season"]]) ? 0 : $teamsWinsTab[$r["season"]];
  $r["second"]=!isset($teamsSecondTab[$r["season"]]) ? 0 : $teamsSecondTab[$r["season"]];
  $r["third"]=!isset($teamsThirdTab[$r["season"]]) ? 0 : $teamsThirdTab[$r["season"]];
  $r["podium"]=!isset($teamsPodiumsTab[$r["season"]]) ? 0 : $teamsPodiumsTab[$r["season"]];
  $r["polepos"]=!isset($teamsPolePosTab[$r["season"]]) ? 0 : $teamsPolePosTab[$r["season"]];
  $r["bestlaps"]=!isset($teamsBLTab[$r["season"]]) ? 0 : $teamsBLTab[$r["season"]];
  $r["pointsPlaces"]=!isset($teamsPointsPlacesTab[$r["season"]]) ? 0 : $teamsPointsPlacesTab[$r["season"]];
  $r["completed"]=!isset($teamsCompletedTab[$r["season"]]) ? 0 : $teamsCompletedTab[$r["season"]];
  $r["incomplete"]=!isset($teamsIncompleteTab[$r["season"]]) ? 0 : $teamsIncompleteTab[$r["season"]];
  $r["disq"]=!isset($teamsDisqTab[$r["season"]]) ? 0 : $teamsDisqTab[$r["season"]];
  $r["engine"]=$enginesByYear[$r["season"]];
  $r["teamDrivers"]=$teamsDriversTab[$r["season"]];
  $r["carModels"]=$teamsCarModelsTab[$r["season"]];
  $r["gpResults"] = $gpTeamResultsBySeasons;

  $teamSeasonDetailsItems[] = $r;
}
$teamItems["seasonDetails"]=$teamSeasonDetailsItems;

$teamItems["[8] Time - sezon szczegoly"]=microtime(true)-$startTime8;
$startTime9 = microtime(true);

/**
* Kierowcy
**/
//gp kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT CONCAT(season,id_gp)) as gp from drivers_gp_involvements,teams,drivers where drivers_gp_involvements.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_involvements.id_team group by 1 desc order by 2";
$result = mysqli_query($dbhandle,$query);
$teamsGPTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsGPTab[$tmp_alias] = $r["gp"];
}
//wyscigi kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT race_date) as starts from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsStartsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsStartsTab[$tmp_alias] = $r["starts"];
}
//kwalifikacje kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT qual_date) as qual from drivers_pp_results,teams,drivers where drivers_pp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_pp_results.id_team group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsQualTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsQualTab[$tmp_alias] = $r["qual"];
}
//zwyciestwa kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT race_date) as wins
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_pos=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsWinsTab[$tmp_alias] = $r["wins"];
}
//drugie miejsca kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as second
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_pos=2 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsSecondTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsSecondTab[$tmp_alias] = $r["second"];
}
//trzecie miejsca kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as third
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_pos=3 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsThirdTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsThirdTab[$tmp_alias] = $r["third"];
}
//podium kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as podiums
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_pos<4 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPodiumsTab[$tmp_alias] = $r["podiums"];
}
//pole position kierowców
$query="SELECT drivers.alias,COUNT(id_starting_grid) as pp
from drivers_gp_starting_grid,teams,drivers where drivers_gp_starting_grid.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_starting_grid.id_team and is_pp=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPolePosTab[$tmp_alias] = $r["pp"];
}
//best laps kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as bestlaps
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_best_lap=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsBLTab[$tmp_alias] = $r["bestlaps"];
}
//gp z punktami kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as pointsPlaces
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_team_points>0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPointsPlacesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsPlacesTab[$tmp_alias] = $r["pointsPlaces"];
}
//ukonczone gp kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT race_date) as completed
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_completed=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsCompletedTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsCompletedTab[$tmp_alias] = $r["completed"];
}
//nieukonczone gp kierowców
$query="SELECT drivers.alias,COUNT(DISTINCT race_date) as incomplete
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and race_completed=0 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsIncompleteTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsIncompleteTab[$tmp_alias] = $r["incomplete"];
}
//dyskwalifikacje kierowców
$query="SELECT drivers.alias,COUNT(id_drivers_gp) as disq
from drivers_gp_results,teams,drivers where drivers_gp_results.id_driver=drivers.id_driver AND teams.team='$id' and teams.id_team=drivers_gp_results.id_team and disq=1 group by 1 desc";
$result = mysqli_query($dbhandle,$query);
$teamsDisqTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsDisqTab[$tmp_alias] = $r["disq"];
}
//punkty kierowców
$query="SELECT COALESCE(SUM(drivers_gp_results.race_team_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) points, drivers.alias
FROM drivers_gp_involvements
LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp = drivers_gp_involvements.id_gp AND gp_season.season = drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)
AND teams.team = '$id'
GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short
HAVING points > 0 
ORDER BY points DESC";
$result = mysqli_query($dbhandle,$query);
$teamsPointsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsTab[$tmp_alias] = $r["points"];
}
//punkty kierowców brane do klasyfikacji
$query="SELECT COALESCE(SUM(drivers_gp_results.race_team_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) points, drivers.alias
FROM drivers_gp_involvements
LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp = drivers_gp_involvements.id_gp AND gp_season.season = drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp AND excluded_from_team_class=0
WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL)
AND teams.team = '$id'
GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short
HAVING points > 0 
ORDER BY points DESC";
$result = mysqli_query($dbhandle,$query);
$teamsPointsClassTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsClassTab[$tmp_alias] = $r["points"];
}

// kierowcy
$query="SELECT distinct drivers.id_driver id, drivers.alias,drivers.country_code country,drivers.name,drivers.surname,CONCAT(drivers.name,' ',drivers.surname) driver, 
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' points, '' pointsClass,
MIN(drivers_gp_involvements.season) seasonMin,
MAX(drivers_gp_involvements.season) seasonMax,
COUNT(DISTINCT drivers_gp_involvements.season) seasons
from drivers_gp_involvements,teams,drivers
where drivers_gp_involvements.id_team=teams.id_team AND drivers_gp_involvements.id_driver=drivers.id_driver AND teams.team='$id'
GROUP BY drivers_gp_involvements.id_driver    
ORDER BY seasonMax DESC, seasonMin DESC, drivers.surname";
$result = mysqli_query($dbhandle,$query);
$teamDriversItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($teamsGPTab[$r["alias"]]) ? 0 : $teamsGPTab[$r["alias"]];
  $r["starts"]=!isset($teamsStartsTab[$r["alias"]]) ? 0 : $teamsStartsTab[$r["alias"]];
  $r["qual"]=!isset($teamsQualTab[$r["alias"]]) ? 0 : $teamsQualTab[$r["alias"]];
  $r["wins"]=!isset($teamsWinsTab[$r["alias"]]) ? 0 : $teamsWinsTab[$r["alias"]];
  $r["second"]=!isset($teamsSecondTab[$r["alias"]]) ? 0 : $teamsSecondTab[$r["alias"]];
  $r["third"]=!isset($teamsThirdTab[$r["alias"]]) ? 0 : $teamsThirdTab[$r["alias"]];
  $r["podium"]=!isset($teamsPodiumsTab[$r["alias"]]) ? 0 : $teamsPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($teamsPolePosTab[$r["alias"]]) ? 0 : $teamsPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($teamsBLTab[$r["alias"]]) ? 0 : $teamsBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($teamsPointsPlacesTab[$r["alias"]]) ? 0 : $teamsPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($teamsCompletedTab[$r["alias"]]) ? 0 : $teamsCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($teamsIncompleteTab[$r["alias"]]) ? 0 : $teamsIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($teamsDisqTab[$r["alias"]]) ? 0 : $teamsDisqTab[$r["alias"]];
  $r["points"]=!isset($teamsPointsTab[$r["alias"]]) ? 0 : $teamsPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($teamsPointsClassTab[$r["alias"]]) ? 0 : $teamsPointsClassTab[$r["alias"]];
  $teamDriversItems[] = $r;
}
$teamItems["drivers"]=$teamDriversItems;


/**
* Najlepsi kierowcy w historii
*/
$driversStatsItems;

// grand prix
$query="SELECT COUNT(drivers_gp_involvements.id_involvement) amount,COUNT(drivers_gp_involvements.id_involvement) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_involvements,drivers,teams
WHERE drivers_gp_involvements.id_driver=drivers.id_driver
AND drivers_gp_involvements.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_gp_involvements.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByGP"]=$statsItems;

// wyscigi
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByStarts"]=$statsItems;

// kwalifikacje
$query="SELECT COUNT(drivers_pp_results.id_drivers_pp) amount,COUNT(drivers_pp_results.id_drivers_pp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_pp_results,drivers,teams
WHERE drivers_pp_results.id_driver=drivers.id_driver
AND drivers_pp_results.id_team=teams.id_team
AND teams.team='$id'
GROUP BY drivers_pp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByQualStarts"]=$statsItems;

// zwyciestwa
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByWins"]=$statsItems;

// 2 miejsca
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=2
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverBySecondPlaces"]=$statsItems;

// 3 miejsca
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos=3
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByThirdPlaces"]=$statsItems;

// podium
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_pos>0 AND race_pos<4
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByPodium"]=$statsItems;

// pole positions
$query="SELECT COUNT(drivers_gp_starting_grid.id_starting_grid) amount,COUNT(drivers_gp_starting_grid.id_starting_grid) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_starting_grid,drivers,teams
WHERE drivers_gp_starting_grid.id_driver=drivers.id_driver
AND drivers_gp_starting_grid.id_team=teams.id_team
AND teams.team='$id'
AND is_pp=1
GROUP BY drivers_gp_starting_grid.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByPolepos"]=$statsItems;

// naj. okrążenia
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_best_lap=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByBestlaps"]=$statsItems;

// wys ukonczone
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_completed=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByCompleted"]=$statsItems;

// wys nieukonczone
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND race_completed=0
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByIncompleted"]=$statsItems;

// dyskwalifikacje
$query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_results,drivers,teams
WHERE drivers_gp_results.id_driver=drivers.id_driver
AND drivers_gp_results.id_team=teams.id_team
AND teams.team='$id'
AND disq=1
GROUP BY drivers_gp_results.id_driver
ORDER BY 1 DESC LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByDisq"]=$statsItems;

// wys punktowane
// $query="SELECT COUNT(drivers_gp_results.id_drivers_gp) amount,COUNT(drivers_gp_results.id_drivers_gp) y,drivers.id_driver idDriver, drivers.surname driver,drivers.alias, UPPER(drivers.surname) name
// FROM drivers_gp_results,drivers,teams
// WHERE drivers_gp_results.id_driver=drivers.id_driver
// AND drivers_gp_results.id_team=teams.id_team
// AND teams.team='$id'
// AND race_team_points>0
// GROUP BY drivers_gp_results.id_driver
// ORDER BY 1 DESC LIMIT 10";
// $result = mysqli_query($dbhandle,$query);
// $statsItems= array();
// while($r = mysqli_fetch_assoc($result)) {
//   $statsItems[] = $r;
// }
// $driversStatsItems["bestDriverByPointRaces"]=$statsItems;

// punkty
$query="SELECT COALESCE(SUM(drivers_gp_results.race_team_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) amount,
COALESCE(SUM(drivers_gp_results.race_team_points),0) + COALESCE(SUM(drivers_sprint_results.sprint_points),0) y,
drivers.id_driver idDriver, drivers.surname driver, drivers.alias, UPPER(drivers.surname) name
FROM drivers_gp_involvements
LEFT JOIN gp ON gp.id_gp = drivers_gp_involvements.id_gp
LEFT JOIN gp_season ON gp_season.id_gp = drivers_gp_involvements.id_gp AND gp_season.season = drivers_gp_involvements.season
LEFT JOIN drivers ON drivers.id_driver = drivers_gp_involvements.id_driver
LEFT JOIN teams ON teams.id_team = drivers_gp_involvements.id_team
LEFT JOIN drivers_pp_results ON drivers_pp_results.id_drivers_pp = drivers_gp_involvements.id_drivers_pp
LEFT JOIN drivers_sprint_results ON drivers_sprint_results.id_drivers_sprint = drivers_gp_involvements.id_drivers_sprint
LEFT JOIN drivers_gp_results ON drivers_gp_results.id_drivers_gp = drivers_gp_involvements.id_drivers_gp
WHERE (drivers_gp_involvements.id_drivers_gp IS NOT NULL OR drivers_gp_involvements.id_drivers_pp IS NOT NULL) 
AND teams.team = '$id'
GROUP BY drivers.id_driver, drivers.name, drivers.surname, drivers.country_short 
HAVING amount > 0
ORDER BY amount DESC, surname LIMIT 10";
$result = mysqli_query($dbhandle,$query);
$statsItems= array();
while($r = mysqli_fetch_assoc($result)) {
  $statsItems[] = $r;
}
$driversStatsItems["bestDriverByPoints"]=$statsItems;

$teamItems["driversStats"]=$driversStatsItems;

$teamItems["[9] Time - kierowcy"]=microtime(true)-$startTime9;
$startTime10 = microtime(true);

/**
* Grand Prix
**/
//udzialy w gp
$query="SELECT gp.name_alias alias,COUNT(DISTINCT CONCAT(drivers_gp_involvements.id_gp, drivers_gp_involvements.season)) amount from drivers_gp_involvements,gp,teams 
where drivers_gp_involvements.id_team=teams.id_team 
AND teams.team='$id' 
and gp.id_gp=drivers_gp_involvements.id_gp 
group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsGPTab = array();
$teamGPStatsGPItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsGPTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsGPItems = $r;
  }
  $cnt+=1;
}
//starty w gp
$query="SELECT gp.name_alias alias,COUNT(DISTINCT race_date) amount from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsStartsTab = array();
$teamGPStatsStartsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsStartsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsStartsItems = $r;
  }
  $cnt+=1;
}
//starty w kwalifikacjach
$query="SELECT gp.name_alias alias,COUNT(DISTINCT qual_date) amount from drivers_pp_results,gp,teams where drivers_pp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_pp_results.id_gp group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsQualTab = array();
$teamGPStatsQualItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsQualTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsQualItems = $r;
  }
  $cnt+=1;
}
//zwyciestwa w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,COUNT(DISTINCT race_date) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_pos=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
$teamGPStatsWinsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsWinsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsWinsItems = $r;
  }
  $cnt+=1;
}
//drugie miejsca w gp
$query="SELECT gp.name_alias alias,COUNT(id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_pos=2 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsSecondTab = array();
$teamGPStatsSecondItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsSecondTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsSecondItems = $r;
  }
  $cnt+=1;
}
//trzecie miejsca w gp
$query="SELECT gp.name_alias alias,COUNT(id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_pos=3 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsThirdTab = array();
$teamGPStatsThirdItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsThirdTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsThirdItems = $r;
  }
  $cnt+=1;
}
//podium w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,COUNT(id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_pos<4 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
$teamGPStatsPodiumItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPodiumsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsPodiumItems = $r;
  }
  $cnt+=1;
}
//pole position w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,COUNT(id_starting_grid) amount
from drivers_gp_starting_grid,gp,teams where drivers_gp_starting_grid.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_starting_grid.id_gp and is_pp=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
$teamGPStatsPoleposItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPolePosTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsPoleposItems = $r;
  }
  $cnt+=1;
}
//best laps w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,COUNT(id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_best_lap=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
$teamGPStatsBLItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsBLTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsBLItems = $r;
  }
  $cnt+=1;
}
//gp z punktami w gp
// $query="SELECT gp.name_alias alias,COUNT(id_drivers_gp) amount
// from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_team_points>0 group by 1 order by 2 desc";
// $result = mysqli_query($dbhandle,$query);
// $teamsPointsPlacesTab = array();
// $teamGPStatsPointsPlacesItems;
// $cnt = 0;
// while($r = mysqli_fetch_assoc($result)) {
//   $tmp_alias = $r["alias"];
//   $teamsPointsPlacesTab[$tmp_alias] = $r["amount"];
//   if ($cnt == 0 ){
//     $teamGPStatsPointsPlacesItems = $r;
//   }
//   $cnt+=1;
// }
//ukonczone gp w gp
$query="SELECT gp.name_alias alias,COUNT(DISTINCT id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_completed=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsCompletedTab = array();
$teamGPStatsCompletedItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsCompletedTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsCompletedItems = $r;
  }
  $cnt+=1;
}
//nieukonczone gp w gp
$query="SELECT gp.name_alias alias,COUNT(DISTINCT id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and race_completed=0 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsIncompleteTab = array();
$teamGPStatsIncompleteItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsIncompleteTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsIncompleteItems = $r;
  }
  $cnt+=1;
}
//dyskwalifikacje w gp
$query="SELECT gp.name_alias alias,COUNT(id_drivers_gp) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and disq=1 group by 1 order by 2 desc";
$result = mysqli_query($dbhandle,$query);
$teamsDisqTab = array();
$teamGPStatsDisqItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsDisqTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsDisqItems = $r;
  }
  $cnt+=1;
}
//punkty w gp
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,SUM(race_team_points) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp
and drivers_gp_results.race_date>='1958-01-01'
and race_team_points>0 group by 1  order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPointsTab = array();
$teamGPStatsPointsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamGPStatsPointsItems = $r;
  }
  $cnt+=1;
}
//punkty w gp do klasyfikacji
if ($lang=='pl') {
  $query="SELECT gp.name";
}else{
  $query="SELECT gp.name_en as name";
}
$query.=",gp.name_alias alias,SUM(race_team_points) amount
from drivers_gp_results,gp,teams where drivers_gp_results.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp
and drivers_gp_results.race_date>='1958-01-01'
and race_team_points>0 AND excluded_from_team_class=0 group by 1  order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPointsClassTab = array();
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsClassTab[$tmp_alias] = $r["amount"];
  $cnt+=1;
}
// grand prix
if ($lang=='pl') {
  $query="SELECT distinct gp.name";
}else{
  $query="SELECT distinct gp.name_en as name";
}
$query.=",gp.name_alias alias,gp.country_code country, gp.name_short nameShort, GROUP_CONCAT(distinct gp.circuit separator ', ') circuits,
'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' points, '' pointsClass
from drivers_gp_involvements,gp,teams
where drivers_gp_involvements.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_involvements.id_gp 
group by name_short
order by starts DESC, name";
$result = mysqli_query($dbhandle,$query);
$teamGPItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($teamsGPTab[$r["alias"]]) ? 0 : $teamsGPTab[$r["alias"]];
  $r["starts"]=!isset($teamsStartsTab[$r["alias"]]) ? 0 : $teamsStartsTab[$r["alias"]];
  $r["qual"]=!isset($teamsQualTab[$r["alias"]]) ? 0 : $teamsQualTab[$r["alias"]];
  $r["wins"]=!isset($teamsWinsTab[$r["alias"]]) ? 0 : $teamsWinsTab[$r["alias"]];
  $r["second"]=!isset($teamsSecondTab[$r["alias"]]) ? 0 : $teamsSecondTab[$r["alias"]];
  $r["third"]=!isset($teamsThirdTab[$r["alias"]]) ? 0 : $teamsThirdTab[$r["alias"]];
  $r["podium"]=!isset($teamsPodiumsTab[$r["alias"]]) ? 0 : $teamsPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($teamsPolePosTab[$r["alias"]]) ? 0 : $teamsPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($teamsBLTab[$r["alias"]]) ? 0 : $teamsBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($teamsPointsPlacesTab[$r["alias"]]) ? 0 : $teamsPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($teamsCompletedTab[$r["alias"]]) ? 0 : $teamsCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($teamsIncompleteTab[$r["alias"]]) ? 0 : $teamsIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($teamsDisqTab[$r["alias"]]) ? 0 : $teamsDisqTab[$r["alias"]];
  $r["points"]=!isset($teamsPointsTab[$r["alias"]]) ? 0 : $teamsPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($teamsPointsClassTab[$r["alias"]]) ? 0 : $teamsPointsClassTab[$r["alias"]];
  $teamGPItems[] = $r;
}
$teamItems["gp"]=$teamGPItems;

// grand prix - statystyki
$teamGPStatsItems=array();
$teamGPStatsItems['gp']=isset($teamGPStatsGPItems) ? $teamGPStatsGPItems : 0;
$teamGPStatsItems['starts']=isset($teamGPStatsStartsItems) ? $teamGPStatsStartsItems : 0;
$teamGPStatsItems['qual']=isset($teamGPStatsQualItems) ? $teamGPStatsQualItems : 0;
$teamGPStatsItems['wins']=isset($teamGPStatsWinsItems) ? $teamGPStatsWinsItems : 0;
$teamGPStatsItems['second']=isset($teamGPStatsSecondItems) ? $teamGPStatsSecondItems : 0;
$teamGPStatsItems['third']=isset($teamGPStatsThirdItems) ? $teamGPStatsThirdItems : 0;
$teamGPStatsItems['podium']=isset($teamGPStatsPodiumItems) ? $teamGPStatsPodiumItems : 0;
$teamGPStatsItems['polepos']=isset($teamGPStatsPoleposItems) ? $teamGPStatsPoleposItems : 0;
$teamGPStatsItems['bestlaps']=isset($teamGPStatsBLItems) ? $teamGPStatsBLItems : 0;
$teamGPStatsItems['pointsPlaces']=isset($teamGPStatsPointsPlacesItems) ? $teamGPStatsPointsPlacesItems : 0;
$teamGPStatsItems['completed']=isset($teamGPStatsCompletedItems) ? $teamGPStatsCompletedItems : 0;
$teamGPStatsItems['incomplete']=isset($teamGPStatsIncompleteItems) ? $teamGPStatsIncompleteItems : 0;
$teamGPStatsItems['disq']=isset($teamGPStatsDisqItems) ? $teamGPStatsDisqItems : 0;
$teamGPStatsItems['points']=isset($teamGPStatsPointsItems) ? $teamGPStatsPointsItems : 0;
$teamItems["gpStats"]=$teamGPStatsItems;

$teamItems["[10] Time - GP"]=microtime(true)-$startTime10;
$startTime11 = microtime(true);

/**
* Tory
**/
//gp na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT CONCAT(drivers_gp_involvements.season,drivers_gp_involvements.id_gp)) amount from drivers_gp_involvements,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_involvements.id_gp and teams.id_team=drivers_gp_involvements.id_team group by 1 desc order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsGPTab = array();
$teamCircuitsStatsGPItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsGPTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsGPItems = $r;
  }
  $cnt+=1;
}
//wyscigi na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT race_date) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team group by 1 desc order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsStartsTab = array();
$teamCircuitsStatsStartsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsStartsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsStartsItems = $r;
  }
  $cnt+=1;
}
//kwalifikacje na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT qual_date) amount from drivers_pp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_pp_results.id_gp and teams.id_team=drivers_pp_results.id_team group by 1 desc order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsQualTab = array();
$teamCircuitsStatsQualItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsQualTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsQualItems = $r;
  }
  $cnt+=1;
}
//zwyciestwa na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT race_date) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_pos=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsWinsTab = array();
$teamCircuitsStatsWinsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsWinsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsWinsItems = $r;
  }
  $cnt+=1;
}
//drugie miejsca na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_pos=2 group by 1 desc order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsSecondTab = array();
$teamCircuitsStatsSecondItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsSecondTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsSecondItems = $r;
  }
  $cnt+=1;
}
//trzecie miejsca na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_pos=3 group by 1 desc order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsThirdTab = array();
$teamCircuitsStatsThirdItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsThirdTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsThirdItems = $r;
  }
  $cnt+=1;
}
//podium na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_pos<4 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPodiumsTab = array();
$teamCircuitsStatsPodiumItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPodiumsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsPodiumItems = $r;
  }
  $cnt+=1;
}
//pole position na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_starting_grid) amount from drivers_gp_starting_grid,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_starting_grid.id_gp and teams.id_team=drivers_gp_starting_grid.id_team and is_pp=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPolePosTab = array();
$teamCircuitsStatsPoleposItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPolePosTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsPoleposItems = $r;
  }
  $cnt+=1;
}
//best laps na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_best_lap=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsBLTab = array();
$teamCircuitsStatsBLItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsBLTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsBLItems = $r;
  }
  $cnt+=1;
}
//gp z punktami na torach
// $query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_team_points>0 group by 1 order by 3 desc";
// $result = mysqli_query($dbhandle,$query);
// $teamsPointsPlacesTab = array();
// $teamCircuitsStatsPointsPlacesItems;
// $cnt = 0;
// while($r = mysqli_fetch_assoc($result)) {
//   $tmp_alias = $r["alias"];
//   $teamsPointsPlacesTab[$tmp_alias] = $r["amount"];
//   if ($cnt == 0 ){
//     $teamCircuitsStatsPointsPlacesItems = $r;
//   }
//   $cnt+=1;
// }
//ukonczone gp na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_completed=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsCompletedTab = array();
$teamCircuitsStatsCompletedItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsCompletedTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsCompletedItems = $r;
  }
  $cnt+=1;
}
//nieukonczone gp na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(DISTINCT id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_completed=0 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsIncompleteTab = array();
$teamCircuitsStatsIncompleteItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsIncompleteTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsIncompleteItems = $r;
  }
  $cnt+=1;
}
//dyskwalifikacje na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,COUNT(id_drivers_gp) amount from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and disq=1 group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsDisqTab = array();
$teamCircuitsStatsDisqItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsDisqTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsDisqItems = $r;
  }
  $cnt+=1;
}
//punkty na torach
$query="SELECT gp.circuit_alias alias,gp.circuit,SUM(race_team_points) amount
from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_team_points>0
and drivers_gp_results.race_date>='1958-01-01'
group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPointsTab = array();
$teamCircuitsStatsPointsItems;
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsTab[$tmp_alias] = $r["amount"];
  if ($cnt == 0 ){
    $teamCircuitsStatsPointsItems = $r;
  }
  $cnt+=1;
}
//punkty na torach do klasyfikacji
$query="SELECT gp.circuit_alias alias,gp.circuit,SUM(race_team_points) amount
from drivers_gp_results,gp,teams where teams.team='$id' and gp.id_gp=drivers_gp_results.id_gp and teams.id_team=drivers_gp_results.id_team and race_team_points>0
and drivers_gp_results.race_date>='1958-01-01' AND excluded_from_team_class=0
group by 1 order by 3 desc";
$result = mysqli_query($dbhandle,$query);
$teamsPointsClassTab = array();
$cnt = 0;
while($r = mysqli_fetch_assoc($result)) {
  $tmp_alias = $r["alias"];
  $teamsPointsClassTab[$tmp_alias] = $r["amount"];
  $cnt+=1;
}

// tory
$query="SELECT distinct gp.circuit_alias alias,gp.circuit name, 
GROUP_CONCAT(DISTINCT gp.country_code ORDER BY gp.country_code SEPARATOR ', ') countries,";
if ($lang=='pl') {
  $query.="GROUP_CONCAT(DISTINCT gp.name ORDER BY gp.country_code SEPARATOR ', ') grandPrix";
}else{
  $query.="GROUP_CONCAT(DISTINCT gp.name_en ORDER BY gp.country_code SEPARATOR ', ') grandPrix";
} 
$query.=",'' gp, '' starts, '' qual, '' wins, '' second, '' third, '' podium, '' polepos, '' bestlaps, '' pointsPlaces, '' completed, '' incomplete, '' disq, '' points, '' pointsClass
from drivers_gp_involvements,gp,teams where drivers_gp_involvements.id_team=teams.id_team AND teams.team='$id' and gp.id_gp=drivers_gp_involvements.id_gp 
group by 1
order by starts DESC, name";
$result = mysqli_query($dbhandle,$query);
$teamCircuitsItems=array();
while($r = mysqli_fetch_assoc($result)) {
  $r["gp"]=!isset($teamsGPTab[$r["alias"]]) ? 0 : $teamsGPTab[$r["alias"]];
  $r["starts"]=!isset($teamsStartsTab[$r["alias"]]) ? 0 : $teamsStartsTab[$r["alias"]];
  $r["qual"]=!isset($teamsQualTab[$r["alias"]]) ? 0 : $teamsQualTab[$r["alias"]];
  $r["wins"]=!isset($teamsWinsTab[$r["alias"]]) ? 0 : $teamsWinsTab[$r["alias"]];
  $r["second"]=!isset($teamsSecondTab[$r["alias"]]) ? 0 : $teamsSecondTab[$r["alias"]];
  $r["third"]=!isset($teamsThirdTab[$r["alias"]]) ? 0 : $teamsThirdTab[$r["alias"]];
  $r["podium"]=!isset($teamsPodiumsTab[$r["alias"]]) ? 0 : $teamsPodiumsTab[$r["alias"]];
  $r["polepos"]=!isset($teamsPolePosTab[$r["alias"]]) ? 0 : $teamsPolePosTab[$r["alias"]];
  $r["bestlaps"]=!isset($teamsBLTab[$r["alias"]]) ? 0 : $teamsBLTab[$r["alias"]];
  $r["pointsPlaces"]=!isset($teamsPointsPlacesTab[$r["alias"]]) ? 0 : $teamsPointsPlacesTab[$r["alias"]];
  $r["completed"]=!isset($teamsCompletedTab[$r["alias"]]) ? 0 : $teamsCompletedTab[$r["alias"]];
  $r["incomplete"]=!isset($teamsIncompleteTab[$r["alias"]]) ? 0 : $teamsIncompleteTab[$r["alias"]];
  $r["disq"]=!isset($teamsDisqTab[$r["alias"]]) ? 0 : $teamsDisqTab[$r["alias"]];
  $r["points"]=!isset($teamsPointsTab[$r["alias"]]) ? 0 : $teamsPointsTab[$r["alias"]];
  $r["pointsClass"]=!isset($teamsPointsClassTab[$r["alias"]]) ? 0 : $teamsPointsClassTab[$r["alias"]];
  $teamCircuitsItems[] = $r;
}
$teamItems["circuits"]=$teamCircuitsItems;

// tory - statystyki
$teamCircuitsStatsItems=array();
$teamCircuitsStatsItems['gp']=isset($teamCircuitsStatsGPItems) ? $teamCircuitsStatsGPItems : 0;
$teamCircuitsStatsItems['starts']=isset($teamCircuitsStatsStartsItems) ? $teamCircuitsStatsStartsItems : 0; 
$teamCircuitsStatsItems['qual']=isset($teamCircuitsStatsQualItems) ? $teamCircuitsStatsQualItems : 0;
$teamCircuitsStatsItems['wins']=isset($teamCircuitsStatsWinsItems) ? $teamCircuitsStatsWinsItems : 0;
$teamCircuitsStatsItems['second']=isset($teamCircuitsStatsSecondItems) ? $teamCircuitsStatsSecondItems : 0;
$teamCircuitsStatsItems['third']=isset($teamCircuitsStatsThirdItems) ? $teamCircuitsStatsThirdItems : 0;
$teamCircuitsStatsItems['podium']=isset($teamCircuitsStatsPodiumItems) ? $teamCircuitsStatsPodiumItems : 0;
$teamCircuitsStatsItems['polepos']=isset($teamCircuitsStatsPoleposItems) ? $teamCircuitsStatsPoleposItems : 0;
$teamCircuitsStatsItems['bestlaps']=isset($teamCircuitsStatsBLItems) ? $teamCircuitsStatsBLItems : 0;
$teamCircuitsStatsItems['pointsPlaces']=isset($teamCircuitsStatsPointsPlacesItems) ? $teamCircuitsStatsPointsPlacesItems : 0;
$teamCircuitsStatsItems['completed']=isset($teamCircuitsStatsCompletedItems) ? $teamCircuitsStatsCompletedItems : 0;
$teamCircuitsStatsItems['incomplete']=isset($teamCircuitsStatsIncompleteItems) ? $teamCircuitsStatsIncompleteItems : 0;
$teamCircuitsStatsItems['disq']=isset($teamCircuitsStatsDisqItems) ? $teamCircuitsStatsDisqItems : 0;
$teamCircuitsStatsItems['points']=isset($teamCircuitsStatsPointsItems) ? $teamCircuitsStatsPointsItems : 0;
$teamItems["circuitsStats"]=$teamCircuitsStatsItems;

$teamItems["[11] Time - tory"]=microtime(true)-$startTime11;

//bolidy wszystkie sezony
$query="SELECT DISTINCT drivers_gp_involvements.season,
lower(teams.team) team,
teams.alias_name alias,
teams.name,
teams.engine,
teams_models.model,    
teams_models.id_team_model idTeamModel,
GROUP_CONCAT(DISTINCT teams_models.team_name SEPARATOR ', ') teamModelName,
lower(teams.country_code) countryCode,
count(DISTINCT CONCAT(drivers_gp_involvements.id_gp,drivers_gp_involvements.season)) starts,
MIN(drivers_gp_involvements.season) seasonMin,
MAX(drivers_gp_involvements.season) seasonMax
from drivers_gp_involvements,teams,teams_models 
where drivers_gp_involvements.team='$id' 
and drivers_gp_involvements.id_team=teams.id_team
and teams_models.id_team_model=drivers_gp_involvements.id_team_model 
GROUP BY teams_models.model
order by 1 DESC,3";
$result = mysqli_query($dbhandle,$query);
$carModelsTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $carModelsTab[] = $r;
}
$teamItems["carModels"]=$carModelsTab;

//silniki wszystkie sezony
$query="SELECT DISTINCT 
lower(teams.team) team,
teams.alias_name alias,
teams.name,
COALESCE(teams.engine,teams.name) engine,
lower(teams.country_code) countryCode,
count(DISTINCT CONCAT(drivers_gp_involvements.id_gp,drivers_gp_involvements.season)) starts,
MIN(drivers_gp_involvements.season) seasonMin,
MAX(drivers_gp_involvements.season) seasonMax
from drivers_gp_involvements,teams
where drivers_gp_involvements.team='$id' 
and drivers_gp_involvements.id_team=teams.id_team
GROUP BY teams.engine
order by 8 DESC,1";
$result = mysqli_query($dbhandle,$query);
$enginesTab = array();
while($r = mysqli_fetch_assoc($result)) {
  $enginesTab[] = $r;
}
$teamItems["engines"]=$enginesTab;

$teamItems["createTime"]=microtime(true)-$start_time;

// Response
$response = $teamItems;

print json_encode($response);
mysqli_free_result($result);
?>
