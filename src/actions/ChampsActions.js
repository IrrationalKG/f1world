// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_CHAMPS_REQUEST = "FETCH_CHAMPS_REQUEST";
export const FETCH_CHAMPS_SUCCESS = "FETCH_CHAMPS_SUCCESS";
export const FETCH_CHAMPS_ERROR = "FETCH_CHAMPS_ERROR";

function requestData() {
  return {
    type: FETCH_CHAMPS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_CHAMPS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_CHAMPS_ERROR,
    payload: error,
  };
}

export function fetchChamps(lang) {
  const url = `${ROOT_URL}/get_champs.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
