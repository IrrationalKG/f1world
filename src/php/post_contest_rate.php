<?
header ("Access-Control-Allow-Origin: *");
header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
header ("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

// $rateJson = $_GET["rate"];

// if ($rateJson === null){
  $rateContent = file_get_contents('php://input');
  $rate = json_decode($rateContent);
  // $rate = json_decode($rateJson, true);
  // $rateJson = json_encode($rateContent);
  // $rate = json_decode($rateJson, false);
  // $rate = json_encode($rateJson);
  // $data = '{"overwrite":false,"qualRatesChecked":true,"raceRatesChecked":false,"gp":"BAH","season":"2024","player":{"key":"118","value":"118","text":"Głowacki Krystian"},"password":"polska","pp":{"key":"933","value":"HAMI","text":"Hamilton Lewis"},"p1":"","p2":"","p3":"","p4":"","p5":"","p6":"","p7":"","p8":"","p9":"","p10":""}';
  // $rate2 = json_decode($rate, false);
  // $data3 = $rateJson;
  // $rate3 = json_decode(stripslashes($data3), false));
  $method = "POST";
// }else{
//   $rate = json_decode($rateJson, false);
//   $rate2 = json_decode($rateJson, true);
//   $method = "GET";
// }
//mail("kryglow@gmail.com","f1world error","ratejson: ".$_SERVER['REQUEST_METHOD']." ".$rateJson,"From: <f1world@f1world.pl>");

$qualRatesChecked = $rate->qualRatesChecked;
$raceRatesChecked = $rate->raceRatesChecked;
$overwrite = $rate->overwrite;

$gp = $rate->gp;
$season = $rate->season;
$player = $rate->player;
$passwd = $rate->password;

$pp = $rate->pp;
$p1 = $rate->p1;
$p2 = $rate->p2;
$p3 = $rate->p3;
$p4 = $rate->p4;
$p5 = $rate->p5;
$p6 = $rate->p6;
$p7 = $rate->p7;
$p8 = $rate->p8;
$p9 = $rate->p9;
$p10 = $rate->p10;

include("dbinfo.inc.php");

//connection string with database
$dbhandle = mysqli_connect($hostname, $username, $password)
or die("Unable to connect to MySQL");
echo "";
//printf("Initial character set: %s\n", mysqli_character_set_name($dbhandle));
if (!mysqli_set_charset($dbhandle, "utf8")) {
    printf("Error loading character set utf8: %s\n", mysqli_error($dbhandle));
    exit();
} else {
    //printf("Current character set: %s\n", mysqli_character_set_name($dbhandle));
}
// connect with database
$selected = mysqli_select_db($dbhandle, $database)
or die("Could not select examples");

//query fire
$response = array();
$response["method"] = $method;
$response["rateContent"] = $rateContent;
$response["rateJson"] = $rateJson;
$response["rate"] = $rate;
$response["passwd"] = $rate->password;
$response["qualRatesChecked"] = $qualRatesChecked;
$response["raceRatesChecked"] = $raceRatesChecked;
$response["overwrite"] = $overwrite;
$response["gp"] = $gp;
$response["season"] = $season;

$response["player"] = $player;
$response["password"] = $passwd;
$response["pp"] = $pp;
$response["p1"] = $p1;
$response["p2"] = $p2;
$response["p3"] = $p3;
$response["p4"] = $p4;
$response["p5"] = $p5;
$response["p6"] = $p6;
$response["p7"] = $p7;
$response["p8"] = $p8;
$response["p9"] = $p9;
$response["p10"] = $p10;

$votesType = "";
$formValid=true;
if ($qualRatesChecked && $raceRatesChecked){
  if ($player=="" || $pp=="" || $p1=="" || $p2=="" || $p3=="" || $p4=="" || $p5=="" || $p6=="" || $p7=="" || $p8=="" || $p9=="" || $p10==""){
    $response["status"] = "ERROR-RATES-NOT-SAVED-WRONG-DATA";
    $formValid=false;
  }
  $votesType = "all";
}else if ($qualRatesChecked){
  if ($player=="" || $pp==""){
    $response["status"] = "ERROR-RATES-NOT-SAVED-WRONG-DATA";
    $formValid=false;
  }
  $votesType = "pp";
}else if ($raceRatesChecked){
  if ($player=="" || $p1=="" || $p2=="" || $p3=="" || $p4=="" || $p5=="" || $p6=="" || $p7=="" || $p8=="" || $p9=="" || $p10==""){
    $response["status"] = "ERROR-RATES-NOT-SAVED-WRONG-DATA";
    $formValid=false;
  }
  $votesType = "gp";
}
$response["votesType"] = $votesType;

if ($formValid){
  $query = "SELECT * FROM users WHERE password='$passwd' AND id_user='$player'";
  $result = mysqli_query($dbhandle,$query);
  $num = mysqli_num_rows($result);
  if ($num==0) {
    $response["status"] = "ERROR-RATES-WRONG-PASSWORD";
  }else{
    while($r = mysqli_fetch_assoc($result)) {
      $email = $r["email"];
    	$name = $r["name"];
    	$surname = $r["surname"];
    }
    // sprawdzenie czy typy istnieja
  	$query1 = "SELECT * FROM typy WHERE gp='$gp' AND uczestnik='$player' AND is_deleted=0 AND sezon='$season' LIMIT 1";
    $result1 = mysqli_query($dbhandle,$query1);
  	$num1=mysqli_num_rows($result1);
    if ($num1==0) {
      // dodanie typów
      $query2 = "INSERT INTO typy VALUES ('0','$gp','$player','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$pp','0','0','0','0','$passwd','$email','0','$season','0',NOW(),'$votesType')";
  		mysqli_query($dbhandle,$query2);

  		$subject="[F1WORLD] Potwierdzenie wysłania typów";
  		$body= "Grand Prix: $gp\nUczestnik: $name $surname\n\nPP.$pp\n1m.$p1\n2m.$p2\n3m.$p3\n4m.$p4\n5m.$p5\n6m.$p6\n7m.$p7\n8m.$p8\n9m.$p9\n10m.$p10\n\nPozdrawiam\nKrystian Głowacki\nFormula One World $season\nhttps://www.f1world.pl";

  		$subject2="[F1KONKURS] Typy na GP $gp $season";
  		$body2= "Grand Prix: $gp\nUczestnik: $name $surname ($player)\nHaslo: $passwd\nEmail: $email\n\nPP.$pp\n1m.$p1\n2m.$p2\n3m.$p3\n4m.$p4\n5m.$p5\n6m.$p6\n7m.$p7\n8m.$p8\n9m.$p9\n10m.$p10\nRodzaj typów:$votesType\n\n";

  		$email2="konkurs@f1world.pl";

  		if($email != "") {
  			mail($email,$subject,$body,"From: <f1world@f1world.pl>");
  		}
  		mail($email2,$subject2,$body2,"From: <f1world@f1world.pl>");

      $response["status"] = "SUCCESS";
    }else{
      // typy już dodane - czy zmienić
      if ($overwrite){
        while($r = mysqli_fetch_assoc($result1)) {
          $id_typy=$r["id_typy"];
          $oldpp=$r["pp"];
          $oldp1=$r["m1"];
          $oldp2=$r["m2"];
          $oldp3=$r["m3"];
          $oldp4=$r["m4"];
          $oldp5=$r["m5"];
          $oldp6=$r["m6"];
          $oldp7=$r["m7"];
          $oldp8=$r["m8"];
          $oldp9=$r["m9"];
          $oldp10=$r["m10"];
        }
        $query2 = "UPDATE typy SET is_deleted=1 WHERE id_typy='$id_typy' and gp='$gp' and uczestnik='$player' and sezon='$season'";
        mysqli_query($dbhandle,$query2);

        if ($votesType=='all'){
          $query3 = "INSERT INTO typy VALUES ('0','$gp','$player','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$pp','0','0','0','0','$passwd','$email','0','$season','0',NOW(),'$votesType')";
          mysqli_query($dbhandle,$query3);
        }
        if ($votesType=='pp'){
          $query3 = "INSERT INTO typy VALUES ('0','$gp','$player','$oldp1','$oldp2','$oldp3','$oldp4','$oldp5','$oldp6','$oldp7','$oldp8','$oldp9','$oldp10','$pp','0','0','0','0','$passwd','$email','0','$season','0',NOW(),'$votesType')";
          mysqli_query($dbhandle,$query3);
        }
        if ($votesType=='gp'){
          $query3 = "INSERT INTO typy VALUES ('0','$gp','$player','$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$oldpp','0','0','0','0','$passwd','$email','0','$season','0',NOW(),'$votesType')";
          mysqli_query($dbhandle,$query3);
        }

        $subject="[F1WORLD] Potwierdzenie wysłania typów";
        $body= "Grand Prix: $gp\nUczestnik: $name $surname\n\nPP.$pp\n1m.$p1\n2m.$p2\n3m.$p3\n4m.$p4\n5m.$p5\n6m.$p6\n7m.$p7\n8m.$p8\n9m.$p9\n10m.$p10\n\nPozdrawiam\nKrystian Głowacki\nFormula One World $season\nhttps://www.f1world.pl";

        $subject2="[F1KONKURS] Typy na GP $gp $season";
        $body2= "$query3\nGrand Prix: $gp\nUczestnik: $name $surname ($player)\nHaslo: $passwd\nEmail: $email\n\nPP.$pp\n1m.$p1\n2m.$p2\n3m.$p3\n4m.$p4\n5m.$p5\n6m.$p6\n7m.$p7\n8m.$p8\n9m.$p9\n10m.$p10\nRodzaj typów:$votesType\n\n";

        $email2="konkurs@f1world.pl";

        if($email != "") {
          mail($email,$subject,$body,"From: <f1world@f1world.pl>");
        }
        mail($email2,$subject2,$body2,"From: <f1world@f1world.pl>");

        $response["status"] = "SUCCESS";
      }else{
        $response["status"] = "ERROR-RATES-ALREADY-SENT";
      }
    }
    mysqli_free_result($result1);
  }
}
mysqli_free_result($result);
print json_encode($response);
?>
