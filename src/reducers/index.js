import { loadingBarReducer } from "react-redux-loading-bar";
import ParamsReducer from "./ParamsReducer";
import ComboSeasonsReducer from "./ComboSeasonsReducer";
import ComboDriversReducer from "./ComboDriversReducer";
import ComboTeamsReducer from "./ComboTeamsReducer";
import ComboGPReducer from "./ComboGPReducer";
import ComboCircuitsReducer from "./ComboCircuitsReducer";
import ComboEnginesReducer from "./ComboEnginesReducer";
import ComboModelsReducer from "./ComboModelsReducer";
import ComboTiresReducer from "./ComboTiresReducer";
import ComboContestSeasonsReducer from "./ComboContestSeasonsReducer";
import ComboContestPlayerSeasonsReducer from "./ComboContestPlayerSeasonsReducer";
import ComboContestGPReducer from "./ComboContestGPReducer";
import ComboPlayersReducer from "./ComboPlayersReducer";
import ComboContestDriversReducer from "./ComboContestDriversReducer";
import MenuReducer from "./MenuReducer";
import NewsReducer from "./NewsReducer";
import TriviaReducer from "./TriviaReducer";
import LastGPReducer from "./LastGPReducer";
import NextGPReducer from "./NextGPReducer";
import SeasonClassReducer from "./SeasonClassReducer";
import ContestReducer from "./ContestReducer";
import ContestAwardsReducer from "./ContestAwardsReducer";
import GPInfoReducer from "./GPInfoReducer";
import NewsDetailsReducer from "./NewsDetailsReducer";
import DriverReducer from "./DriverReducer";
import DriverModelsReducer from "./DriverModelsReducer";
import DriverTeammatesReducer from "./DriverTeammatesReducer";
import TeamReducer from "./TeamReducer";
import TeamModelsReducer from "./TeamModelsReducer";
import CircuitReducer from "./CircuitReducer";
import GPResultReducer from "./GPResultReducer";
import ClassDriversReducer from "./ClassDriversReducer";
import ClassTeamsReducer from "./ClassTeamsReducer";
import ChampsReducer from "./ChampsReducer";
import SeasonsReducer from "./SeasonsReducer";
import GPSeasonReducer from "./GPSeasonReducer";
import GPSeasonPointsReducer from "./GPSeasonPointsReducer";
import GPSeasonPlacesReducer from "./GPSeasonPlacesReducer";
import GPSeasonQualReducer from "./GPSeasonQualReducer";
import GPSeasonSummaryReducer from "./GPSeasonSummaryReducer";
import GPSeasonDriversReducer from "./GPSeasonDriversReducer";
import GPSeasonTeamsReducer from "./GPSeasonTeamsReducer";
import GrandPrixReducer from "./GrandPrixReducer";
import GPStatsReducer from "./GPStatsReducer";
import CircuitsStatsReducer from "./CircuitsStatsReducer";
import DriversStatsReducer from "./DriversStatsReducer";
import TeamsStatsReducer from "./TeamsStatsReducer";
import DriversRecordsReducer from "./DriversRecordsReducer";
import TeamsRecordsReducer from "./TeamsRecordsReducer";
import DriverEventsReducer from "./DriverEventsReducer";
import TeamEventsReducer from "./TeamEventsReducer";
import TeamModelEventsReducer from "./TeamModelEventsReducer";
import ContestTopReducer from "./ContestTopReducer";
import ContestClassReducer from "./ContestClassReducer";
import ContestStatsReducer from "./ContestStatsReducer";
import ContestPlacesStatsReducer from "./ContestPlacesStatsReducer";
import ContestGPRatesReducer from "./ContestGPRatesReducer";
import ContestRegisterReducer from "./ContestRegisterReducer";
import ContestRateReducer from "./ContestRateReducer";
import ContestPlayerReducer from "./ContestPlayerReducer";
import DriversCompareReducer from "./DriversCompareReducer";
import DriversCompareTeammatesReducer from "./DriversCompareTeammatesReducer";
import StatsReducer from "./StatsReducer";
import StatsTriviaReducer from "./StatsTriviaReducer";

export default {
  loadingBar: loadingBarReducer,
  params: ParamsReducer,
  comboSeasons: ComboSeasonsReducer,
  comboDrivers: ComboDriversReducer,
  comboTeams: ComboTeamsReducer,
  comboGPS: ComboGPReducer,
  comboCircuits: ComboCircuitsReducer,
  comboEngines: ComboEnginesReducer,
  comboModels: ComboModelsReducer,
  comboTires: ComboTiresReducer,
  comboContestSeasons: ComboContestSeasonsReducer,
  comboContestPlayerSeasons: ComboContestPlayerSeasonsReducer,
  comboContestGP: ComboContestGPReducer,
  comboPlayers: ComboPlayersReducer,
  comboContestDrivers: ComboContestDriversReducer,
  menu: MenuReducer,
  news: NewsReducer,
  trivia: TriviaReducer,
  lastGP: LastGPReducer,
  nextGP: NextGPReducer,
  seasonClass: SeasonClassReducer,
  contest: ContestReducer,
  awards: ContestAwardsReducer,
  gpinfo: GPInfoReducer,
  newsDetails: NewsDetailsReducer,
  driver: DriverReducer,
  driverModels: DriverModelsReducer,
  driverTeammates: DriverTeammatesReducer,
  team: TeamReducer,
  teamModels: TeamModelsReducer,
  circuit: CircuitReducer,
  gpresult: GPResultReducer,
  classDrivers: ClassDriversReducer,
  classTeams: ClassTeamsReducer,
  champs: ChampsReducer,
  seasons: SeasonsReducer,
  gpseason: GPSeasonReducer,
  gpseasonPoints: GPSeasonPointsReducer,
  gpseasonPlaces: GPSeasonPlacesReducer,
  gpseasonQual: GPSeasonQualReducer,
  gpseasonSummary: GPSeasonSummaryReducer,
  gpseasonDrivers: GPSeasonDriversReducer,
  gpseasonTeams: GPSeasonTeamsReducer,
  gp: GrandPrixReducer,
  gpstats: GPStatsReducer,
  circuitsStats: CircuitsStatsReducer,
  driversStats: DriversStatsReducer,
  teamsStats: TeamsStatsReducer,
  driversRecords: DriversRecordsReducer,
  teamsRecords: TeamsRecordsReducer,
  driverEvents: DriverEventsReducer,
  teamEvents: TeamEventsReducer,
  teamModelEvents: TeamModelEventsReducer,
  contestTop: ContestTopReducer,
  contestClass: ContestClassReducer,
  contestStats: ContestStatsReducer,
  contestPlacesStats: ContestPlacesStatsReducer,
  contestGPRates: ContestGPRatesReducer,
  contestRegister: ContestRegisterReducer,
  contestRate: ContestRateReducer,
  contestPlayer: ContestPlayerReducer,
  driversCompare: DriversCompareReducer,
  driversCompareTeammates: DriversCompareTeammatesReducer,
  stats: StatsReducer,
  statsTrivia: StatsTriviaReducer,
};
