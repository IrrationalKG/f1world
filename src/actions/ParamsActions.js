// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_PARAMS_REQUEST = "FETCH_PARAMS_REQUEST";
export const FETCH_PARAMS_SUCCESS = "FETCH_PARAMS_SUCCESS";
export const FETCH_PARAMS_ERROR = "FETCH_PARAMS_ERROR";

function requestData() {
  return {
    type: FETCH_PARAMS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_PARAMS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_PARAMS_ERROR,
    payload: error,
  };
}

export function fetchParams(lang) {
  const url = `${ROOT_URL}/get_params.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
