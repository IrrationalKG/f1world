/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Loader,
  Header,
  Segment,
  Flag,
  Icon,
  Table,
  Item,
  Message,
  Image,
  Statistic,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import SectionPageHeader from "../../components/SectionPageHeader/SectionPageHeader";
import { fetchGPSeasonPoints } from "../../actions/GPSeasonPointsActions";
import { fetchComboSeasons } from "../../actions/ComboSeasonsActions";
import Regulations from "../Regulations/Regulations";
import { FormattedMessage, injectIntl } from "react-intl";
import Cookies from "js-cookie";
import styles from "./GPSeasonPoints.less";

class GPSeasonPoints extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.year },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchGPSeasonPoints(props.match.params.year, Cookies.get("lang"));
    props.fetchComboSeasons();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { year } = props.match.params;
    const nextYear = nextProps.match.params.year;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextYear,
        label: nextProps.comboSeasons.data?.find((e) => e.value == nextYear)
          ?.label,
      },
    });

    if (year !== nextYear) {
      props.fetchGPSeasonPoints(nextYear, Cookies.get("lang"));
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/gp-season/points/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderClassDriversPoints() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classDriversPoints } = props.gpseasonPoints.data;
    classDriversPoints.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce" className="no-wrap">
            {item.place}.
          </Table.Cell>
          <Table.Cell data-title="Kierowca" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/driver/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={driverPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src =
                        "/build/images/drivers/driver_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.teamsNames?.split(", ").map((teamName, index) => {
                      return (
                        <span key={teamName}>
                          {index != 0 && ", "}

                          {teamName}
                        </span>
                      );
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPoints.map((e) => (
            <Table.Cell data-title={e.gp} key={e.gp}>
              {e.points ? (
                e.points != "0" ? (
                  e.points
                ) : (
                  <>&mdash;</>
                )
              ) : (
                String.fromCharCode(160)
              )}
            </Table.Cell>
          ))}
          <Table.Cell data-title="Punkty" className="cell-body-points">
            <NavLink
              to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.pointsClass === null
                ? String.fromCharCode(160)
                : item.pointsClass}
              <small>
                {item.pointsClass !== item.points && ` (${item.points})`}
              </small>
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassDriversPointsMobile() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classDriversPoints, gp } = props.gpseasonPoints.data;
    classDriversPoints.forEach((item) => {
      const driverPic = `/build/images/drivers/driver_${item.id}_profile.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/driver/${item.alias}`}>
            <Image
              size="small"
              src={driverPic}
              alt={item.name}
              className="cell-photo"
              onError={(e) => {
                e.target.src = "/build/images/drivers/driver_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/driver/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.teamsNames?.split(", ").map((teamName, index) => {
                return (
                  <div key={teamName}>
                    <NavLink
                      to={`/team/${item.teamsAliases?.split(", ")[index]}`}
                    >
                      {teamName}
                    </NavLink>
                  </div>
                );
              })}
            </Item.Description>
            <Item.Description>
              {item.gpPoints.map((e) => (
                <span key={e.gp} className="key-value-box">
                  <div className="key-value-box-header">
                    {e.nameShort.toUpperCase()}
                  </div>
                  <div className="key-value-box-value">
                    {e.points === null ? (
                      String.fromCharCode(160)
                    ) : e.points != "0" ? (
                      e.points
                    ) : (
                      <>&mdash;</>
                    )}
                  </div>
                </span>
              ))}
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                <NavLink
                  to={`/driver-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : item.pointsClass}
                  <div>
                    <small>
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </div>
                </NavLink>
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsPoints() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classTeamsPoints } = props.gpseasonPoints.data;
    classTeamsPoints.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Table.Row key={item.id}>
          <Table.Cell data-title="Miejsce">{item.place}.</Table.Cell>
          <Table.Cell data-title="Zespół" className="no-wrap left">
            <div className="box-image-name">
              <div>
                <NavLink to={`/team/${item.alias}`}>
                  <Image
                    size="tiny"
                    src={teamPic}
                    alt={item.name}
                    onError={(e) => {
                      e.target.src = "/build/images/teams/team_no_profile.jpg";
                    }}
                  />
                </NavLink>
              </div>
              <div>
                <div>
                  <Flag name={item.country} />
                  <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
                </div>
                <div>
                  <small>
                    {item.modelName?.split(", ").map((modelName) => {
                      return <div key={modelName}>{modelName}</div>;
                    })}
                  </small>
                </div>
              </div>
            </div>
          </Table.Cell>
          {item.gpPoints.map((e) => (
            <Table.Cell data-title={e.gp} key={e.gp}>
              {e.points ? (
                e.points != "0" ? (
                  e.points
                ) : (
                  <>&mdash;</>
                )
              ) : (
                String.fromCharCode(160)
              )}
            </Table.Cell>
          ))}
          <Table.Cell data-title="Punkty" className="cell-body-points">
            <NavLink
              to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
            >
              {item.pointsClass === null
                ? String.fromCharCode(160)
                : item.pointsClass}
              <small>
                {item.pointsClass !== item.points && ` (${item.points})`}
              </small>
            </NavLink>
          </Table.Cell>
        </Table.Row>
      );
      elements.push(element);
    });
    return elements;
  }

  renderClassTeamsPointsMobile() {
    const elements = [];
    const { props } = this;
    const { year } = props.match.params;
    const { classTeamsPoints, gp } = props.gpseasonPoints.data;
    classTeamsPoints.forEach((item) => {
      const teamPic = `/build/images/teams/team_${item.team}_profile_logo.jpg`;
      const element = (
        <Item key={`${item.id}`}>
          <NavLink to={`/team/${item.alias}`}>
            <Image
              size="small"
              src={teamPic}
              alt={item.name}
              onError={(e) => {
                e.target.src = "/build/images/teams/team_no_profile.jpg";
              }}
            />
          </NavLink>
          <Item.Content verticalAlign="middle">
            <Item.Header>
              {item.place}. <Flag name={item.country} />
              <NavLink to={`/team/${item.alias}`}>{item.name}</NavLink>
            </Item.Header>
            <Item.Description>
              {item.modelName?.split(", ").map((modelName) => {
                return <div key={modelName}>{modelName}</div>;
              })}
            </Item.Description>
            <Item.Description>
              {item.gpPoints.map((e) => (
                <span key={e.gp} className="key-value-box">
                  <div className="key-value-box-header">
                    {e.nameShort.toUpperCase()}
                  </div>
                  <div className="key-value-box-value">
                    {e.points === null ? (
                      String.fromCharCode(160)
                    ) : e.points != "0" ? (
                      e.points
                    ) : (
                      <>&mdash;</>
                    )}
                  </div>
                </span>
              ))}
            </Item.Description>
          </Item.Content>
          <Item.Content verticalAlign="middle">
            <Statistic floated="right">
              <Statistic.Value>
                <NavLink
                  to={`/team-events/points/${item.alias}/-/-/-/-/-/-/${year}/1/`}
                >
                  {item.pointsClass === null
                    ? String.fromCharCode(160)
                    : item.pointsClass}
                  <div>
                    <small>
                      {item.pointsClass !== item.points && ` (${item.points})`}
                    </small>
                  </div>
                </NavLink>
              </Statistic.Value>
              <Statistic.Label>
                <FormattedMessage id={"app.stats.pts"} />
              </Statistic.Label>
            </Statistic>
          </Item.Content>
        </Item>
      );
      elements.push(element);
    });
    return elements;
  }

  render() {
    const { props, state } = this;
    const { formatMessage } = this.props.intl;

    if (!props.gpseasonPoints.data || props.gpseasonPoints.loading) {
      return (
        <section
          id="gpseason-points-details"
          name="gpseason-points-details"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      state;
    const { year } = props.match.params;
    const prevYear = parseInt(year, 10) - 1;
    const nextYear = parseInt(year, 10) + 1;
    const { gp, regulations } = props.gpseasonPoints.data;

    const linkPrev = `/gp-season/points/${prevYear}`;
    const linkNext = `/gp-season/points/${nextYear}`;
    if (gp.length === 0) {
      return (
        <section
          id="gpseason-points-details"
          name="gpseason-points-details"
          className="section-page"
        >
          <SectionPageBanner
            title={
              <FormattedMessage
                id={"app.page.gpseason.points.title"}
                values={{ season: year }}
              />
            }
            subtitle={
              <FormattedMessage id={"app.page.gpseason.points.subtitle"} />
            }
            linkPrev={linkPrev}
            linkNext={linkNext}
          />
          <Segment padded basic>
            <Header as="h2" icon textAlign="center">
              <Icon name="info" circular />
              <Header.Content>
                <FormattedMessage id={"app.page.gpseason.points.info"} />
              </Header.Content>
            </Header>
          </Segment>
        </section>
      );
    }

    return (
      <section
        id="gpseason-points-details"
        name="gpseason-points-details"
        className="section-page"
      >
        <SectionPageBanner
          title={
            <FormattedMessage
              id={"app.page.gpseason.points.title"}
              values={{ season: year }}
            />
          }
          subtitle={
            <FormattedMessage id={"app.page.gpseason.points.subtitle"} />
          }
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic padded>
            <div className="buttons">
              <NavLink to={`/gp-season/${year}`} className="secondary">
                <FormattedMessage id={"app.button.summary"} />
              </NavLink>
              <NavLink to={`/gp-season/points/${year}`} className="primary">
                <FormattedMessage id={"app.button.points"} />
              </NavLink>
              <NavLink to={`/gp-season/places/${year}`} className="secondary">
                <FormattedMessage id={"app.button.places"} />
              </NavLink>
              <NavLink to={`/gp-season/qual/${year}`} className="secondary">
                <FormattedMessage id={"app.button.grid"} />
              </NavLink>
              <NavLink to={`/gp-season/summary/${year}`} className="secondary">
                <FormattedMessage id={"app.button.statistics"} />
              </NavLink>
              <NavLink to={`/gp-season/drivers/${year}`} className="secondary">
                <FormattedMessage id={"app.button.drivers"} />
              </NavLink>
              <NavLink to={`/gp-season/teams/${year}`} className="secondary">
                <FormattedMessage id={"app.button.teams"} />
              </NavLink>
            </div>
            <div className="gp-season-points-filters">
              <Select
                ref={(ref) => {
                  this.comboSeason = ref;
                }}
                isLoading={isLoadingExternally}
                name="seasons"
                multi={multi}
                ignoreCase={ignoreCase}
                ignoreAccents={ignoreAccents}
                value={state.selectValue}
                onChange={this.onInputChange}
                options={props.comboSeasons.data}
                clearable={clearable}
                labelKey="label"
                valueKey="value"
                placeholder={formatMessage({
                  id: "app.placeholder.select.season",
                })}
                noResultsText={
                  <FormattedMessage id={"app.placeholder.no.results"} />
                }
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>
            <div className="class-drivers-points">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.drivers"} />}
                type="secondary"
              />
              <div className="hideForDesktop">
                <Segment basic>
                  <Item.Group divided unstackable>
                    {this.renderClassDriversPointsMobile()}
                  </Item.Group>
                </Segment>
              </div>
              <div className="hideForMobile">
                <Segment basic className="overflow">
                  <Table
                    basic="very"
                    celled
                    className="responsive-table center-aligned"
                  >
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell className="cell-header-number">
                          #
                        </Table.HeaderCell>
                        <Table.HeaderCell className="left">
                          <FormattedMessage id={"app.table.header.driver"} />
                        </Table.HeaderCell>
                        {gp.map((item) => (
                          <Table.HeaderCell
                            data-title={item.gp}
                            key={item.alias}
                            className="cell-default-size"
                          >
                            <NavLink to={`/gp-result/${item.alias}/${year}`}>
                              {item.nameShort}
                            </NavLink>
                            <br />
                            <Flag name={item.name} />
                          </Table.HeaderCell>
                        ))}
                        <Table.HeaderCell className="cell-header-points">
                          <FormattedMessage id={"app.table.header.points"} />
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>{this.renderClassDriversPoints()}</Table.Body>
                  </Table>
                </Segment>
              </div>
            </div>
            <div className="class-teams-points">
              <SectionPageHeader
                title={<FormattedMessage id={"app.stats.teams"} />}
                type="secondary"
              />
              {year > 1957 ? (
                <>
                  <div className="hideForDesktop">
                    <Segment basic>
                      <Item.Group divided unstackable>
                        {this.renderClassTeamsPointsMobile()}
                      </Item.Group>
                    </Segment>
                  </div>
                  <div className="hideForMobile">
                    <Segment basic className="overflow">
                      <Table
                        basic="very"
                        celled
                        className="responsive-table center-aligned"
                      >
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell className="cell-header-number">
                              #
                            </Table.HeaderCell>
                            <Table.HeaderCell className="left">
                              <FormattedMessage id={"app.table.header.team"} />
                            </Table.HeaderCell>
                            {gp.map((item) => (
                              <Table.HeaderCell
                                data-title={item.gp}
                                key={item.alias}
                                className="cell-default-size"
                              >
                                <NavLink
                                  to={`/gp-result/${item.alias}/${year}`}
                                >
                                  {item.nameShort}
                                </NavLink>
                                <br />
                                <Flag name={item.name} />
                              </Table.HeaderCell>
                            ))}
                            <Table.HeaderCell className="cell-header-points">
                              <FormattedMessage
                                id={"app.table.header.points"}
                              />
                            </Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>{this.renderClassTeamsPoints()}</Table.Body>
                      </Table>
                    </Segment>
                  </div>
                </>
              ) : (
                <Message icon>
                  <Icon name="warning circle" />
                  <Message.Content>
                    <Message.Header>
                      <FormattedMessage id={"app.message.header"} />
                    </Message.Header>
                    <p>
                      <FormattedMessage
                        id={"app.page.gpseason.points.info2"}
                        values={{ season: year }}
                      />
                    </p>
                  </Message.Content>
                </Message>
              )}
            </div>
            <Segment padded basic>
              <Regulations regulations={regulations}></Regulations>
            </Segment>
          </Segment>
        </div>
      </section>
    );
  }
}

GPSeasonPoints.propTypes = {
  fetchGPSeasonPoints: PropTypes.func.isRequired,
  gpseasonPoints: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboSeasons: PropTypes.func.isRequired,
  comboSeasons: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  year: PropTypes.string,
};

function mapStateToProps({ gpseasonPoints, comboSeasons }) {
  return { gpseasonPoints, comboSeasons };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { fetchGPSeasonPoints, fetchComboSeasons },
    dispatch
  );
}

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(GPSeasonPoints)
);
