// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_ENGINES_REQUEST = "FETCH_COMBO_ENGINES_REQUEST";
export const FETCH_COMBO_ENGINES_SUCCESS = "FETCH_COMBO_ENGINES_SUCCESS";
export const FETCH_COMBO_ENGINES_ERROR = "FETCH_COMBO_ENGINES_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_ENGINES_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_ENGINES_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_ENGINES_ERROR,
    payload: error,
  };
}

export function fetchComboEngines(
  empty,
  driver,
  team,
  gp,
  circuit,
  model,
  tyre,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_engines.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          driver: driver,
          team: team,
          gp: gp,
          circuit: circuit,
          model: model,
          tyre: tyre,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
