// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_TEAMS_RECORDS_REQUEST = "FETCH_TEAMS_RECORDS_REQUEST";
export const FETCH_TEAMS_RECORDS_SUCCESS = "FETCH_TEAMS_RECORDS_SUCCESS";
export const FETCH_TEAMS_RECORDS_ERROR = "FETCH_TEAMS_RECORDS_ERROR";

function requestData() {
  return {
    type: FETCH_TEAMS_RECORDS_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_TEAMS_RECORDS_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_TEAMS_RECORDS_ERROR,
    payload: error,
  };
}

export function fetchTeamsRecords(
  page,
  stats,
  pos,
  gp,
  circuit,
  beginYear,
  endYear,
  status,
  lang
) {
  const url = `${ROOT_URL}/get_teams_records.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          page: page,
          stats: stats,
          pos: pos,
          gp: gp,
          circuit: circuit,
          beginYear: beginYear,
          endYear: endYear,
          status: status,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
