// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_COMBO_TIRES_REQUEST = "FETCH_COMBO_TIRES_REQUEST";
export const FETCH_COMBO_TIRES_SUCCESS = "FETCH_COMBO_TIRES_SUCCESS";
export const FETCH_COMBO_TIRES_ERROR = "FETCH_COMBO_TIRES_ERROR";

function requestData() {
  return {
    type: FETCH_COMBO_TIRES_REQUEST,
  };
}

function receiveData(data) {
  return {
    type: FETCH_COMBO_TIRES_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_COMBO_TIRES_ERROR,
    payload: error,
  };
}

export function fetchComboTires(
  empty,
  driver,
  team,
  gp,
  circuit,
  engine,
  model,
  season,
  lang
) {
  const url = `${ROOT_URL}/get_combo_tires.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          empty: empty,
          driver: driver,
          team: team,
          gp: gp,
          circuit: circuit,
          engine: engine,
          model: model,
          season: season,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
