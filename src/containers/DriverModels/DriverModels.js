/* eslint no-unused-vars: ["error", {"varsIgnorePattern": "styles"}] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["renderCountriesFlags"] }] */

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  Image,
  Grid,
  Loader,
  Segment,
  Statistic,
  Label,
  Popup,
} from "semantic-ui-react";
import Select from "react-select";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import SectionPageBanner from "../../components/SectionPageBanner/SectionPageBanner";
import { fetchDriverModels } from "../../actions/DriverModelsActions";
import { fetchComboDrivers } from "../../actions/ComboDriversActions";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import styles from "./DriverModels.less";

class TeamModels extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multi: false,
      clearable: false,
      ignoreCase: true,
      ignoreAccents: false,
      isLoadingExternally: false,
      selectValue: { value: props.match.params.driverId },
    };

    this.onInputChange = this.onInputChange.bind(this);
  }

  UNSAFE_componentWillMount() {
    const { props } = this;
    props.fetchDriverModels(props.match.params.driverId);
    props.fetchComboDrivers(
      0,
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      "-",
      Cookies.get("lang")
    );
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { props } = this;

    const { driverId } = props.match.params;
    const nextDriverId = nextProps.match.params.driverId;

    this.setState({
      isLoadingExternally: false,
      selectValue: {
        value: nextDriverId,
        label: nextProps.comboDrivers.data?.find((e) => e.value == nextDriverId)
          ?.label,
      },
    });

    if (driverId !== nextDriverId) {
      props.fetchDriverModels(nextDriverId);
    }
  }

  onInputChange(event) {
    const { props } = this;
    props.history.push({ pathname: `/driver-models/${event.value}` });
    this.setState({
      selectValue: event.value,
    });
  }

  renderDriverModels() {
    const elements = [];
    const { props } = this;
    const { models, alias } = props.driverModels.data;

    models.forEach((item) => {
      const picCar = `/build/images/teams/models/team_${item.team.toLowerCase()}_${item.model
        .toLowerCase()
        .replace("???", "3")
        .replace("??", "2")
        .replace("?", "1")}.jpg`;

      const element = (
        <Grid.Column key={item.id_team_model} textAlign="center">
          <Segment padded basic>
            <Segment basic>
              <Popup
                content={
                  item.teamName ? (
                    item.teamName.split(", ").map((t) => {
                      return <div key={t}>{t}</div>;
                    })
                  ) : (
                    <FormattedMessage id={"app.stats.private"} />
                  )
                }
                key={item.id_team_model}
                position="bottom center"
                wide="very"
                trigger={
                  <NavLink
                    to={`/team-model-events/gp/${item.id_team_model}/-/${alias}`}
                  >
                    {/* <NavLink
                     to={`/driver-events/gp/${alias}/-/-/-/-/${item.model}/-/-/1`}
                   > */}
                    <Image
                      centered
                      src={picCar}
                      alt={`${item.name} ${item.engine} ${item.model.replaceAll(
                        "???",
                        "3"
                      )} ${item.season}`}
                      onError={(e) => {
                        e.target.src = "/build/images/teams/team_no_car.jpg";
                      }}
                    />
                    <div className="driver-models-box-model">
                      {item.name} {item.engine ? item.engine : ""}{" "}
                      {item.model
                        ? item.model.replace("_", "/").replaceAll("?", "")
                        : ""}
                    </div>
                  </NavLink>
                }
              />
              <Label attached="top left" color="red">
                &nbsp;{item.season}&nbsp;
              </Label>
            </Segment>
          </Segment>
        </Grid.Column>
      );
      elements.push(element);
    });

    return elements;
  }

  render() {
    const { props, state } = this;
    if (!props.driverModels.data || props.driverModels.loading) {
      return (
        <section
          id="driver-models"
          name="driver-models"
          className="section-page"
        >
          <div className="full-height">
            <Loader active inline="centered">
              <FormattedMessage id={"app.loading"} />
            </Loader>
          </div>
        </section>
      );
    }
    const { isLoadingExternally, multi, ignoreCase, ignoreAccents, clearable } =
      this.state;
    const { name, id, alias, prevId, nextId, models } = props.driverModels.data;
    const picProfile = `/build/images/drivers/driver_${id}_profile.jpg`;

    const linkPrev = `/driver-models/${prevId}`;
    const linkNext = `/driver-models/${nextId}`;

    return (
      <section id="driver-models" name="driver-models" className="section-page">
        <SectionPageBanner
          title={name}
          subtitle={<FormattedMessage id={"app.page.driver.models.subtitle"} />}
          linkPrev={linkPrev}
          linkNext={linkNext}
        />
        <div className="section-page-content">
          <Segment basic>
            <Grid stackable centered>
              <Grid.Column
                mobile={16}
                tablet={4}
                computer={3}
                className="left-sidebar"
              >
                <div className="section-page-filters">
                  <Select
                    ref={(ref) => {
                      this.comboTeam = ref;
                    }}
                    isLoading={isLoadingExternally}
                    name="drivers"
                    multi={multi}
                    ignoreCase={ignoreCase}
                    ignoreAccents={ignoreAccents}
                    value={state.selectValue}
                    onChange={this.onInputChange}
                    options={props.comboDrivers.data}
                    clearable={clearable}
                    labelKey="label"
                    valueKey="value"
                    placeholder={
                      <FormattedMessage id={"app.placeholder.select.driver"} />
                    }
                    noResultsText={
                      <FormattedMessage id={"app.placeholder.no.results"} />
                    }
                    className="react-select-container"
                    classNamePrefix="react-select"
                  />
                </div>
                <div>
                  <div>
                    <Grid centered>
                      <Grid.Column mobile={8} tablet={16} computer={16}>
                        <Segment basic textAlign="center">
                          <Image
                            className="driver-photo"
                            centered
                            src={picProfile}
                            alt={name}
                            onError={(e) => {
                              e.target.src =
                                "/build/images/drivers/driver_no_profile.jpg";
                            }}
                          />
                        </Segment>
                      </Grid.Column>
                      <Grid.Column
                        mobile={8}
                        tablet={16}
                        computer={16}
                        className="info-box"
                      >
                        <div className="vertical-middle-aligned">
                          <Segment basic textAlign="center">
                            <Statistic size="large" inverted>
                              <Statistic.Value>{models.length}</Statistic.Value>
                              <Statistic.Label>
                                <FormattedMessage id={"app.stats.models"} />
                              </Statistic.Label>
                            </Statistic>
                            <div className="buttons">
                              <NavLink
                                className="primary"
                                to={`/driver/${alias}`}
                              >
                                <FormattedMessage id={"app.button.profile"} />
                              </NavLink>
                            </div>
                          </Segment>
                        </div>
                      </Grid.Column>
                    </Grid>
                  </div>
                </div>
              </Grid.Column>
              <Grid.Column mobile={16} tablet={12} computer={13}>
                <Segment basic>
                  <Grid stackable>
                    <Grid.Row textAlign="center" columns={1} only="mobile">
                      {this.renderDriverModels()}
                    </Grid.Row>
                    <Grid.Row textAlign="center" columns={2} only="tablet">
                      {this.renderDriverModels()}
                    </Grid.Row>
                    <Grid.Row textAlign="center" columns={3} only="computer">
                      {this.renderDriverModels()}
                    </Grid.Row>
                  </Grid>
                </Segment>
              </Grid.Column>
            </Grid>
          </Segment>
        </div>
      </section>
    );
  }
}

TeamModels.propTypes = {
  fetchDriverModels: PropTypes.func.isRequired,
  driverModels: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  fetchComboDrivers: PropTypes.func.isRequired,
  comboTeams: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ])
  ),
  history: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
      PropTypes.number,
      PropTypes.func,
    ])
  ),
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.bool, PropTypes.string])
  ),
  driverId: PropTypes.string,
};

function mapStateToProps({ driverModels, comboDrivers }) {
  return { driverModels, comboDrivers };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchDriverModels, fetchComboDrivers }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamModels);
