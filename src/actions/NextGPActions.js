// PHP
import axios from "axios";
import { ROOT_URL } from "../config/config";

export const FETCH_NEXTGP = "FETCH_NEXTGP";
export const FETCH_NEXTGP_SUCCESS = "FETCH_NEXTGP_SUCCESS";
export const FETCH_NEXTGP_ERROR = "FETCH_NEXTGP_ERROR";

function requestData() {
  return {
    type: FETCH_NEXTGP,
  };
}

function receiveData(data) {
  return {
    type: FETCH_NEXTGP_SUCCESS,
    payload: data,
  };
}

function receiveError(error) {
  return {
    type: FETCH_NEXTGP_ERROR,
    payload: error,
  };
}

export function fetchNextGP(year, id, lang) {
  const url = `${ROOT_URL}/get_next_gp.php`;
  return (dispatch) => {
    dispatch(requestData());
    axios
      .get(url, {
        params: {
          id: id,
          year: year,
          lang: lang,
        },
      })
      .then((response) => {
        dispatch(receiveData(response.data));
      })
      .catch((error) => {
        dispatch(receiveError(error));
      });
  };
}
